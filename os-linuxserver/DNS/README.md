# DNS

dns 既 Domain Name System 的 縮寫 負責將 域名 解析到 ip地址 DNS 使用 TCP/UDP 53 端口  
(通常是 使用 UDP 查詢 目前只有一些 特例比如 代理工具 可能會使用 TCP 查詢)

對於每級域名長度 限度是 63個字符 總長度 不能超過 253

# dnsutils

linux 提供 了 dnsutils 套件 來支持 dns 查詢 排錯

```sh
#info=false
sudo apt-get install dnsutils
```

```sh
#info=false
yum install bind-utils
```

## nslookup

nslookup 可以 使用 系統 默認的 dns 服務器 進行 dns 查詢

```sh
$ nslookup google.com
Server:		127.0.1.1
Address:	127.0.1.1#53

Non-authoritative answer:
Name:	google.com
Address: 172.217.161.174
```

```sh
$ nslookup
> google.com
Server:		127.0.1.1
Address:	127.0.1.1#53

Non-authoritative answer:
Name:	google.com
Address: 216.58.221.238
> facebook.com
Server:		127.0.1.1
Address:	127.0.1.1#53

Non-authoritative answer:
Name:	facebook.com
Address: 69.63.184.14
>
```

## dig
dig 可以 指定 一個 dns 服務器 進行 查詢
```sh
$ dig google.com @192.168.16.1 -p 53

; <<>> DiG 9.10.3-P4-Ubuntu <<>> google.com @192.168.16.1 -p 53
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53250
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		197	IN	A	172.217.24.46

;; Query time: 4 msec
;; SERVER: 192.168.16.1#53(192.168.16.1)
;; WHEN: Wed Oct 17 09:19:48 CST 2018
;; MSG SIZE  rcvd: 55
```

## host
```sh
$ host google.com
google.com has address 216.58.221.238
google.com has IPv6 address 2404:6800:4005:80d::200e
google.com mail is handled by 30 alt2.aspmx.l.google.com.
google.com mail is handled by 50 alt4.aspmx.l.google.com.
google.com mail is handled by 10 aspmx.l.google.com.
google.com mail is handled by 20 alt1.aspmx.l.google.com.
google.com mail is handled by 40 alt3.aspmx.l.google.com.
```