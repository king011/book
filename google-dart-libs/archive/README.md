# [archive](https://pub.dev/packages/archive)

MIT 協議的庫，提供了檔案打包和壓縮解壓

```
dart pub add archive
```

```
flutter pub add archive
```

有兩種 import 方法:

* **import 'package:archive/archive.dart';** 可以在 web 平臺運行因爲它不依賴 dart:io
* **import 'package:archive/archive_io.dart';** 非 web 平臺都應該使用這個，它使用 dart:io 直接讀寫檔案效率更高內存佔用更少

# zip

zip 很適合作爲遊戲打包資源，因爲它解壓快速，並且檔案可以分別選擇是否進行壓縮。通常對於圖像音頻等資源已經經過算法壓縮則在打包時不應該再壓縮(降低效率，且沒什麼效果)
而對於腳本和語言文本的文字內容應該啓用壓縮。

> 注意雖然可以設置密碼，但目前版本(^3.4.10)在 linux 下測試設置密碼後 decodeBuffer 會拋出異常


```
import 'dart:convert';
import 'dart:io';

import 'package:archive/archive_io.dart';

void main(List<String> arguments) async {
  final zipPath = "a.zip";
  String? password;
  // password = "testpwd";
  await encoder(zipPath, password, ".");
  await decoder(zipPath, password);
}

Future<void> decoder(String zipPath, String? password) async {
  final inputStream = InputFileStream(zipPath);
  final archive = ZipDecoder().decodeBuffer(
    inputStream,
    verify: true,
    password: password,
  );
  for (var file in archive.files) {
    if (!file.isFile) {
      continue;
    }
    print(" * ${file.name} ${file.isCompressed}");
    final outputStream = OutputStream();
    file.writeContent(
      outputStream,
    );
    print(utf8.decode(outputStream.getBytes()));
  }
}

Future<void> encoder(String zipPath, String? password, String source) async {
  final zip = ZipFileEncoder(password: password)..open(zipPath);
  await for (var entity in Directory(source).list(recursive: true)) {
    final stat = await entity.stat();
    if (stat.type != FileSystemEntityType.file) {
      continue;
    }
    final path = entity.path;
    if (path.endsWith(".yaml")) {
      await zip.addFile(
        File(path),
        path.startsWith('.') ? path.substring(1) : path,
        1, // 使用 gzip 壓縮
      );
    } else if (path.endsWith(".dart")) {
      await zip.addFile(
        File(path),
        path.startsWith('.') ? path.substring(1) : path,
        0, // 不想要壓縮
      );
    }
  }

  // add from memory
  final content = utf8.encode("測試 ArchiveFile");
  final archive = ArchiveFile.noCompress(
    "/README.md",
    content.length,
    content,
  );
  zip.addArchiveFile(archive);

  zip.close();
}
```