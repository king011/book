# 準備

* 一臺 linux，本例子使用 ubuntu 20.04
* 每臺機器 RAM 高於 2GB
* 每臺機器至少 2 CPU 核
* 集羣中的所有機器的網路彼此能夠互相連接(公網 內網 都可以)
* 集羣節點中不可用有重複的 主機名 MAC地址 或 product_uuid

	```
	# 查詢 MAC
	ifconfig -a
	```

	```
	# 查詢 product_uuid
	sudo cat /sys/class/dmi/id/product_uuid
	```

* 禁用交換分區。爲了包裝 kubelet 正常工作，**必須**禁用交換分區

	下述命令可以歷時禁用交換分區
	
	```
	# 禁用交換分區
	sudo swapoff -a
	#  啓用交換分區
	sudo swapon -a
	# 查看交換分區
	free -h |egrep 'Swap|total' 
	```
	
	如果要永久禁用交換分區需要編輯 **/etc/fstab** 檔案將以 **swap** 格式掛接的分區 刪除
	
# 允許 iptables 檢查橋接流量
	
確保 br_netfilter 模塊被加載

```
lsmod | grep br_netfilter
```

執行下述指令 顯示加載 br_netfilter 模塊
```
sudo modprobe br_netfilter
```

爲了讓 iptables 能夠正確的查看橋接流量，需要確保 sysctrl 配置中將 net.bridge.bridge-nf-call-iptables 設置爲 1

```
#info="/etc/modules-load.d/k8s.conf"
br_netfilter
```
```
#info="/etc/sysctl.d/k8s.conf"
net.bridge.bridge-nf-call-ip6tables=1
net.bridge.bridge-nf-call-iptables=1
net.ipv4.ip_forward=1
```
```
sudo sysctl --system
```

# 檢查所需要端口

確保 Kubernetes 各組件可以通過端口互通，你可以使用 telnet 來測試

```
telnet 127.0.0.1 6443
```

# 安裝 runtime

爲了在 Pod 中允許容器，Kubernetes使用容器運行時。

Kubernetes 支持 docker containerd CRI-O

```
sudo apt install docker.io
```

如果使用 docker 從 1.24 開始需要自己手動安裝 [cri-dockerd](https://github.com/Mirantis/cri-dockerd)

```
git clone https://github.com/Mirantis/cri-dockerd.git

# Run these commands as root
###Install GO###
wget https://storage.googleapis.com/golang/getgo/installer_linux
chmod +x ./installer_linux
./installer_linux
source ~/.bash_profile

cd cri-dockerd
mkdir bin
go build -o bin/cri-dockerd
mkdir -p /usr/local/bin
install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd
cp -a packaging/systemd/* /etc/systemd/system
sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
systemctl daemon-reload
systemctl enable cri-docker.service
systemctl enable --now cri-docker.socket
```

# 安裝 kubeadm kubelet  kubectl

需要在每臺機器上都安裝：
* kubeadm 用來初始化集羣的指令
* kubelet 在集羣中每個節點用來啓動 Pod 和 容器等
* kubectl 用來與集羣通信的命令行工具

需要確保 kubeadm kubelet  kubect 的版本匹配，否則可能會出現一些意料之外的錯誤

1. 更新 apt 包索引並安裝使用 Kubernetes 所以需要的包

	```
	sudo apt-get update
	sudo apt-get install -y apt-transport-https ca-certificates curl
	```
	
2. 下載 Google Cloud  公開簽名的密鑰

	```
	sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
	```
	
3. 添加 Kubernetes 到 apt 倉庫

	```
	echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
	```
	
4. 更新 apt 索引，之後安裝 kubeadm kubelet  kubectl 並鎖定其版本

	```
	sudo apt-get update
	sudo apt-get install -y kubelet kubeadm kubectl
	sudo apt-mark hold kubelet kubeadm kubectl
	```
	
kubelet 現在每個幾秒就會重啓，因爲它陷入了一個等待 kubeadm 指令的死循環


將 kubelet.service 加入到開機啓動項
```
sudo systemctl enable kubelet.service
```

# 配置 cgroup 驅動程序

容器運行時和 kubelet 都具有名字爲 "cgroup driver" 的屬性，該屬性對於在 linux 機器上管理 CGroups 而言非常重要。

需要確保 容器運行時和 kubelet 所使用的是相同的 cgroup driver，目前推薦使用 systemd 驅動

對於[docker](https://kubernetes.io/zh/docs/setup/production-environment/container-runtimes/) 需要設置 **native.cgroupdriver=systemd** 選項

```
#info="/etc/docker/daemon.json"
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
```

對於 [Kubernetes](https://kubernetes.io/zh/docs/tasks/administer-cluster/kubeadm/configure-cgroup-driver/) 可以在創建集羣節點時傳遞一個 KubeletConfiguration 結構體，並指定 cgroupDriver 屬性

```
#info="kubeadm-config.yaml"
kind: ClusterConfiguration
apiVersion: kubeadm.k8s.io/v1beta3
kubernetesVersion: v1.21.0
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: systemd
```
```
kubeadm init --config kubeadm-config.yaml
```

> 在版本 1.22 中如果用戶沒有在 KubeletConfiguration 中設置 cgroupDriver，則 kubeadm init 會將它設置爲默認的 systemd