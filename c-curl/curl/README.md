# 命令行工具

curl 提供了一個命令行工具

```
# 簡單模式
curl http://127.0.0.1:9000

# 詳細模式
curl --verbose http://127.0.0.1:9000
curl -v http://127.0.0.1:9000

# 下載
curl --output output.html http://127.0.0.1:9000/
curl -o output.html http://127.0.0.1:9000/

#  設置 header
curl  http://127.0.0.1:9000/api/v1/version -H "Accept: application/xml"
# post json
curl  http://127.0.0.1:9000/api/v1/sum -X POST -d '[1,2,3,4]'  -H "Content-type: application/json" -H "Accept: application/xml"
```