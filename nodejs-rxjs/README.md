# rxjs

RxJS 是一套藉由 Observable sequences 來組合非同步行為和事件基礎程序的 Library

RxJS 是 **Functional Programming** 和 **Reactive Programming** 兩個編程思想的結合

官網 [http://reactivex.io/](http://reactivex.io/)


# Functional Programming

以 function 來思考問題 以及撰寫程式

# Reactive Programming

當資源變化時 由資源自動 告知發送了變動

