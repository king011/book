# [Window](https://wails.io/docs/reference/runtime/window)

Window 系列函數提供來對應用程序窗口操作的方法

# WindowSetTitle

設置窗口標題文本

```
Go: WindowSetTitle(ctx context.Context, title string)
JS: WindowSetTitle(title: string)
```
# WindowFullscreen

使窗口全屏顯示

```
Go: WindowFullscreen(ctx context.Context)
JS: WindowFullscreen()
```

# WindowUnfullscreen

使用窗口退出全屏顯示

```
Go: WindowUnfullscreen(ctx context.Context)
JS: WindowUnfullscreen()
```

# WindowIsFullscreen

返回窗口是否全屏顯示

```
Go: WindowIsFullscreen(ctx context.Context) bool
JS: WindowIsFullscreen() bool
```

# WindowCenter

使窗口位於當前屏幕中心顯示

```
Go: WindowCenter(ctx context.Context)
JS: WindowCenter()
```

# WindowExecJS

在窗口中執行任意 JS 代碼

此方法異步運行瀏覽器中的代碼並立刻返回。如果腳本導致任何錯誤，它將僅在瀏覽器控制檯中可用

```
Go: WindowExecJS(ctx context.Context, js string)
```

# WindowReload

執行 reload(重新加載當前頁面)

```
Go: WindowReload(ctx context.Context)
JS: WindowReload()
```

# WindowReloadApp

重新加載應用程序前端

```
Go: WindowReloadApp(ctx context.Context)
JS: WindowReloadApp()
```

# WindowSetSystemDefaultTheme

將窗口設置爲系統默認主題 (dark/light)

* Windows only

```
Go: WindowSetSystemDefaultTheme(ctx context.Context)
JS: WindowSetSystemDefaultTheme()
```

# WindowSetLightTheme

設置窗口 light 主題

Windows only.
```
Go: WindowSetLightTheme(ctx context.Context)
JS: WindowSetLightTheme()
```

# WindowSetDarkTheme

設置窗口 dark 主題

* widows only

```
Go: WindowSetDarkTheme(ctx context.Context)
JS: WindowSetDarkTheme()
```

# WindowShow

如果窗口隱藏則顯示窗口

```
Go: WindowShow(ctx context.Context)
JS: WindowShow()
```

# WindowHide

如果窗口可見則隱藏窗口

```
Go: WindowHide(ctx context.Context)
JS: WindowHide()
```

# WindowIsNormal

如果窗口沒有 最小化/最大化/全屏 則返回 true
```
Go: WindowIsNormal(ctx context.Context) bool
JS: WindowIsNormal() bool
```

# WindowSetSize

設置窗口寬度和高度

```
Go: WindowSetSize(ctx context.Context, width int, height int)
JS: WindowSetSize(size: Size)
```

# WindowGetSize

返回窗口寬度和高度

```
Go: WindowGetSize(ctx context.Context) (width int, height int)
JS: WindowGetSize() : Size
```

# WindowSetMinSize

設置窗口最小尺寸，如果窗口當前小於給定尺寸將調整窗口尺寸

將尺寸設置爲 0,0 將禁用此約束

```
Go: WindowSetMinSize(ctx context.Context, width int, height int)
JS: WindowSetMinSize(size: Size)
```

# WindowSetMaxSize
設置窗口最大尺寸，如果窗口當前大於給定尺寸將調整窗口尺寸

將尺寸設置爲 0,0 將禁用此約束

```
Go: WindowSetMaxSize(ctx context.Context, width int, height int)
JS: WindowSetMaxSize(size: Size)
```

# WindowSetAlwaysOnTop

設置窗口是否始終在頂部

```
Go: WindowSetAlwaysOnTop(ctx context.Context, b bool)
JS: WindowSetAlwaysOnTop(b: Boolen)
```

# WindowSetPosition

設置窗口在所在屏幕的位置

```
Go: WindowSetPosition(ctx context.Context, x int, y int)
JS: WindowSetPosition(position: Position)
```

# WindowGetPosition

返回窗口在所在屏幕的位置

```
Go: WindowGetPosition(ctx context.Context) (x int, y int)
JS: WindowGetPosition() : Position
```

# WindowMaximise

最大化窗口以填滿屏幕

```
Go: WindowMaximise(ctx context.Context)
JS: WindowMaximise()
```
# WindowUnmaximise
將窗口恢復到最大化之前的尺寸和位置

```
Go: WindowUnmaximise(ctx context.Context)
JS: WindowUnmaximise()
```

# WindowIsMaximised

返回窗口是否最大化

```
Go: WindowIsMaximised(ctx context.Context) bool
JS: WindowIsMaximised() bool
```

# WindowToggleMaximise

切換窗口最大化狀態

```
Go: WindowToggleMaximise(ctx context.Context)
JS: WindowToggleMaximise()
```

# WindowMinimise
最小化窗口
```
Go: WindowMinimise(ctx context.Context)
JS: WindowMinimise()
```

# WindowUnminimise

恢復窗口最小化前的尺寸和位置

```
Go: WindowUnminimise(ctx context.Context)
JS: WindowUnminimise()
```

# WindowIsMinimised
返回窗口是否最小化

```
Go: WindowIsMinimised(ctx context.Context) bool
JS: WindowIsMinimised() bool
```

# WindowSetBackgroundColour

設置窗口背景顏色

```
Go: WindowSetBackgroundColour(ctx context.Context, R, G, B, A uint8)
JS: WindowSetBackgroundColour(R, G, B, A)
```

> 在 Windows 上，alpha 僅支持 0 或 255。任何不爲 0 的值都將被視 255

# Position

```
interface Position {
  x: number;
  y: number;
}
```

# Size
```
interface Size {
  w: number;
  h: number;
}
```