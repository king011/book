# windows

1. 從 [https://tightvnc.com/download.php](https://tightvnc.com/download.php) 官網下載 windows 軟件包，並且安裝

2. 安裝完成後會彈出設置框，需要設置遠程訪問密碼以及修改 vnc 設定時要驗證的管理員密碼

	![](assets/password.png)
	
	
## DFMirage Driver

如果在 windows7 或更早的系統中使用 TightVNC，應該安裝 DFMirage Driver。Windows 及更高的版本不需要。

DFMirage 鏡像驅動使 TightVNC 在舊版本的 Windows 下獲得最佳的性能，DFMirage 使 TightVNC 可以以非常有效的方式檢查屏幕更新並獲取像素數據。