# 安裝運行

docker 官方提供了 registry 的 docker 鏡像 可以直接使用 docker 運行

1. 運行 registry

	```
	docker run -d -p 5000:5000 --name registry registry:2
	```

1. 標記 images 使其指向 私有的 registry

	```
	docker image tag ubuntu localhost:5000/myfirstimage
	```

1. 上傳鏡像

	```
	docker push localhost:5000/myfirstimage
	```
	
1. 拉回鏡像

	```
	docker pull localhost:5000/myfirstimage
	```

1. 停止 registry 並刪除私有 數據

	```
	docker container stop registry && docker container rm -v registry
	```
	
# 部署

* -p 80:5000 將本機 80 端口 映射給 registry
* --restart=always 設置重啓策略
* -v /mnt/registry:/var/lib/registry -> 將主機**/mnt/registry**掛接給 registry 用來存儲鏡像

```
docker run -d \
  -p 80:5000 \
  --restart=always \
  --name registry \
  -v /mnt/registry:/var/lib/registry \
  registry:2
```

如果 使用 http 協議 registry 不會允許 push images， 使用 nginx 對內使用 http 連接 registry 可以加上 X-Forwarded-Proto 告訴 registry 是安全的 https 

```
proxy_set_header  Host              $http_host;   # required for docker client's sake
proxy_set_header  X-Real-IP         $remote_addr; # pass on real client's IP
proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto https;
proxy_pass http://127.0.0.1:5000/;
```

# 密碼
1. 創建 密碼檔案

	```
	mkdir auth
	```
	
	```
	docker run --rm \
			--entrypoint htpasswd \
			httpd:alpine \
			-Bbn username password > auth/htpasswd
	```
	
	需要將 username password 改成自己的 用戶名和密碼

1. 啓動 registry 並設置使用 htpasswd 驗證
	```
	docker run -d \
		-p 5000:5000 \
		--restart=always \
		--name registry \
		-v "$(pwd)"/auth:/auth \
		-e "REGISTRY_AUTH=htpasswd" \
		-e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
		-e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
		registry:2
	```
	
	如果要允許刪除 鏡像加上 -e "REGISTRY\_STORAGE\_DELETE\_ENABLED=true"

1. 登入自己的私有倉庫

	```
	docker login myregistrydomain.com:5000
	```
	
	會在 $HOME/.docker/config.json 寫入 登入信息
	
1. 登出私有倉庫

	```
	docker logout myregistrydomain.com:5000
	```
	
# config.yml

**/etc/docker/registry/config.yml** 是 配置檔案 默認內容如下

```
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
```

# 注意

registry 不支持 docker search 

```
# 列出所有鏡像
curl -X GET https://<registry_ip>:<registry_port>/v2/_catalog -H "Authorization: Basic XXXXX"

# 查詢鏡像支持的標籤
curl -X GET https://<registry_ip>:<registry_port>/v2/<image_name>/tags/list  -H "Authorization: Basic XXXXX"

# 刪除 遠程鏡像
curl  -X DELETE https://<registry_ip>:<registry_port>/v2/<image_name>/manifests/<tag>
```