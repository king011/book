# 模塊
ES2015 開始 引入了 模塊概念 ts 使用了此 定義 使用 import 導入 模塊 export 導出

**animal.ts**
```ts
export interface IAnimal {
    Speak(): void
}
export class Cat implements IAnimal {
    Speak() {
        console.log("喵~")
    }
}
export class Dog implements IAnimal {
    Speak() {
        console.log("汪~")
    }
}
```
**main.ts**
```ts
import { IAnimal, Cat, Dog } from './animal'
function speak(a: IAnimal) {
    a.Speak()
}
speak(new Cat())
speak(new Dog())
```