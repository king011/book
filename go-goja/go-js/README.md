# Runtime

goja.New 會創建一個 Runtime 作爲 js運行的 虛擬機 Runtime


# go 調用 js

* Runtime 提供了 GlobalObject 函數 返回一個 goja.Object 來返回 js的全局對象
* Runtime 同時 提供了 Get Set 函數來 代理對 js的全局對象 的訪問
* Runtime 提供了 ToValue 函數 將 go 型別轉換爲 可供js使用的 goja.Value 接口
* goja.AssertFunction 可以返回一個 goja.Callable 以及 bool 來判斷 goja.Value 是否是可調用的函數

```
type Callable func(this Value, args ...Value) (Value, error)
```

Callable 函數 是一個 可調用的 js 函數 如果 js 拋出異常 將被 返回值 error 捕獲 js函數的返回值由 Value 捕獲

```go
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
)

func main() {
	var (
		vm     *goja.Runtime
		e      error
		result goja.Value
		ok     bool
		call   goja.Callable
	)

	// 創建 虛擬機
	vm = goja.New()
	_, e = vm.RunString(`function max(a,b){
		throw 123
		return a>b?a:b
}
`)
	if e != nil {
		log.Fatalln(e)
	}

	// 查找 函數
	call, ok = goja.AssertFunction(vm.Get(`max`))
	if !ok {
		log.Fatalln(`not found function max`)
	}
	result, e = call(
		goja.Undefined(),             // this
		vm.ToValue(1), vm.ToValue(2), // other args
	)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(result)
}
```

# js 調用 go

通過 goja.Object 提供的 Set 函數 可以 將 go 型別 或 函數 設置 給js 調用

對於 函數 支持 兩種寫法

1. go 寫法 就是普通的 go函數 goja 會自動翻譯
2. goja 寫法 'func(call goja.FunctionCall) goja.Value' call 傳入了 調用 goja.FunctionCall 和 參數  goja.Value 是 函數返回值

```
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
)

type nativeGlobal struct {
	runtime *goja.Runtime
}

func (n *nativeGlobal) init() {
	vm := n.runtime
	vm.Set(`println`, n.nativePrintln)
	vm.Set(`testReturn`, n.nativeTestReturn)
	vm.Set(`testError`, n.nativeTestError)
	vm.Set(`testInterrupt`, n.nativeTestInterrupt)
}
func (n *nativeGlobal) nativePrintln(args ...interface{}) {
	fmt.Println(args...)
}
func (n *nativeGlobal) nativeTestReturn() (int, int) {
	// 多個返回值 或作爲 數組 返回
	return 1, 2
}
func (n *nativeGlobal) nativeTestError(call goja.FunctionCall) goja.Value {
	// panic 引發的 goja.Value 恐慌將 作爲 js 異常 可以被js捕獲
	panic(n.runtime.ToValue(`abc`))
}
func (n *nativeGlobal) nativeTestInterrupt(call goja.FunctionCall) goja.Value {
	// 中斷js 並 拋出異常
	n.runtime.Interrupt(`test interrupt`)

	// Interrupt 後 的代碼會繼續執行 要終止需要手動調用 return
	fmt.Println(`will run`)
	// n.runtime.ClearInterrupt() // ClearInterrupt 可以撤銷 中斷
	return n.runtime.ToValue(`ok`) // 會返回 但 js 不會接收
}

func main() {
	var (
		vm *goja.Runtime
		e  error
	)

	// 創建 虛擬機
	vm = goja.New()
	native := &nativeGlobal{
		runtime: vm,
	}
	native.init()
	_, e = vm.RunString(`println('cerberus is an idea')
var arrs=testReturn()
println(arrs.length) // 2
try{
	testError()
}catch(e){
	println('exception',e)
}
try{
	println(testInterrupt(1,2,3)) // not println
}catch(e){
	// 中斷無法被 捕獲
	println(e) // not println
}
`)
	if e != nil {
		if _, ok := e.(*goja.InterruptedError); ok {
			log.Fatalln(`InterruptedError`, e)
		} else {
			log.Fatalln(e)
		}
	}
}
```