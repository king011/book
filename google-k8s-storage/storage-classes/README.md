# [存儲類](https://kubernetes.io/docs/concepts/storage/storage-classes/)

StorageClass 爲管理員提供了描述存儲類的方法。

每個 StorageClass 都包含 provisioner、parameters 和 reclaimPolicy 字段，這些字段會在 StorageClass 需要動態分配 PV 時使用到。

StorageClass 對象的命名很重要，用戶使用這個命名來請求生成一個特定的類。當創建 StorageClass 對象時，管理員設置 StorageClass 對象的命名和其它參數，一旦創建類對象就不能再對其更新

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: standard
provisioner: kubernetes.io/aws-ebs
parameters:
  type: gp2
reclaimPolicy: Retain
allowVolumeExpansion: true
mountOptions:
  - debug
volumeBindingMode: Immediate
```

# Provisioner

每個 StorageClass 都有個 一個 Provisioner 用來決定使用哪個插件 制備 PV 該字段必須指定

除類 internal provisioner，還可以使用外部 Provisioner，你可以訪問 [kubernetes-sigs/sig-storage-lib-external-provisioner](https://github.com/kubernetes-sigs/sig-storage-lib-external-provisioner) 包含一個用於 external provisioner 編寫功能的實現類庫 ，還可以訪問 [ kubernetes-sigs/sig-storage-lib-external-provisioner](https://github.com/kubernetes-sigs/sig-storage-lib-external-provisioner) 了解外部驅動列表

# 允許卷擴展

allowVolumeExpansion 爲 true 時 允許  用戶通過編輯 PVC 來調整 卷大小

# 掛載選項

mountOptions 指定 附加的 掛載選項

當設置類卷插件不支持掛掛載選項 會掛載失敗

# 卷綁定模式
volumeBindingMode 字段控制類 卷綁定和動態制備 應該發送在什麼時候

默認清空下 Immediate 模式標識 一旦創建類  PVC 也就完成了卷綁定和動態制備。對於由拓撲限制而非集羣所有節點可達的存儲後端 PV 會在不知道 Pod 調度要求的情況下綁定或制備。

可以設置 WaitForFirstConsumer 模式。該模式將延遲 PV 綁定和制備，直到使用該 PVC 的 Pod 被創建。PV 會根據 Pod 調度約定指定的拓撲來選擇和制備。這些包括但不限於 資源需求 節點篩選器 Pod親和性和互斥性 污點和容忍度 ...

# 允許的拓撲結構

# 參數

parameters 描述類 制備器可接收的參數，一個 StorageClass 可定義 512 個參數 。這些參數總長度不能超過 256Kib

## NFS

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: example-nfs
provisioner: example.com/external-nfs
parameters:
  server: nfs-server.example.com
  path: /share
  readOnly: "false"
```

* server：NFS 服務器主機名稱或 ip 地址
* path：NFS 服務器導出路徑。
* readOnly：是否將存儲掛載爲只讀的標誌（默認爲 false）

Kubernetes 未提供 nfs 驅動，需要自行安裝第三方驅動，後續都以 [nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner) 爲例

1. 爲 nfs 驅動創建必要的 rabc 權限

    ```
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: nfs-client-provisioner
      namespace: default # 這裏要替換爲和部署使用 nfs 的資源相同名字空間
    ---
    kind: ClusterRole
    apiVersion: rbac.authorization.k8s.io/v1
    metadata:
      name: nfs-client-provisioner-runner
    rules:
      - apiGroups: [""]
        resources: ["nodes"]
        verbs: ["get", "list", "watch"]
      - apiGroups: [""]
        resources: ["persistentvolumes"]
        verbs: ["get", "list", "watch", "create", "delete"]
      - apiGroups: [""]
        resources: ["persistentvolumeclaims"]
        verbs: ["get", "list", "watch", "update"]
      - apiGroups: ["storage.k8s.io"]
        resources: ["storageclasses"]
        verbs: ["get", "list", "watch"]
      - apiGroups: [""]
        resources: ["events"]
        verbs: ["create", "update", "patch"]
    ---
    kind: ClusterRoleBinding
    apiVersion: rbac.authorization.k8s.io/v1
    metadata:
      name: run-nfs-client-provisioner
    subjects:
      - kind: ServiceAccount
        name: nfs-client-provisioner
        namespace: default # 這裏要替換爲和部署使用 nfs 的資源相同名字空間
    roleRef:
      kind: ClusterRole
      name: nfs-client-provisioner-runner
      apiGroup: rbac.authorization.k8s.io
    ---
    kind: Role
    apiVersion: rbac.authorization.k8s.io/v1
    metadata:
      name: leader-locking-nfs-client-provisioner
      namespace: default # 這裏要替換爲和部署使用 nfs 的資源相同名字空間
    rules:
      - apiGroups: [""]
        resources: ["endpoints"]
        verbs: ["get", "list", "watch", "create", "update", "patch"]
    ---
    kind: RoleBinding
    apiVersion: rbac.authorization.k8s.io/v1
    metadata:
      name: leader-locking-nfs-client-provisioner
      namespace: default # 這裏要替換爲和部署使用 nfs 的資源相同名字空間
    subjects:
      - kind: ServiceAccount
        name: nfs-client-provisioner
        namespace: default # 這裏要替換爲和部署使用 nfs 的資源相同名字空間
    roleRef:
      kind: Role
      name: leader-locking-nfs-client-provisioner
      apiGroup: rbac.authorization.k8s.io
    ```

1. 運行驅動

    ```
    #info="provisioner.yaml"
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      namespace: default # 這裏要替換爲和部署使用 nfs 的資源相同名字空間
      name: nfs-client-provisioner
      labels:
        app: nfs-client-provisioner
    spec:
      replicas: 1
      strategy:
        type: Recreate
      selector:
        matchLabels:
          app: nfs-client-provisioner
      template:
        metadata:
          labels:
            app: nfs-client-provisioner
        spec:
          serviceAccountName: nfs-client-provisioner
          containers:
            - name: nfs-client-provisioner
              image: k8s.gcr.io/sig-storage/nfs-subdir-external-provisioner:v4.0.2
              volumeMounts:
                - name: nfs-client-root
                  mountPath: /persistentvolumes
              env:
                - name: PROVISIONER_NAME
                  value: nfs-external-provisioner # 這裏替換爲你自定義的一個 pvc 名稱
                - name: NFS_SERVER
                  value: 192.168.251.80 # 這裏替換爲 nfs 服務器主機名或 ip 地址
                - name: NFS_PATH
                  value: /opt/nfs # 這裏替換爲 nfs 服務器提供的掛載路徑
          volumes:
            - name: nfs-client-root
              nfs:
                server: 192.168.251.80 # 這裏替換爲 nfs 服務器主機名或 ip 地址
                path: /opt/nfs # 這裏替換爲 nfs 服務器提供的掛載路徑
    ```
		
	```
	kubectl apply -f provisioner.yaml
	```

2. 定義 存儲類

	```
	#info="class.yaml"
	apiVersion: storage.k8s.io/v1
	kind: StorageClass
	metadata:
		name: nfs-client
	provisioner: nfs-external-provisioner # 供應商名稱必須和 provisioner.yaml 中定義的一致
	parameters:
		pathPattern: "${.PVC.namespace}/${.PVC.annotations.nfs.io/storage-path}" # waits for nfs.io/storage-path annotation, if not specified will accept as empty string.
		onDelete: delete
	```