# teal

Teal 是 lua 的一個開源(MIT)方言，源碼以 **.tl** 作爲後綴，其目的在於爲 lua 提供類型檢查支持

Teal 編譯器由 tl.lua 提供，沒有任何額外的依賴所以，你也可以直接將 tl.lua 加載到 lua，這樣將可以直接運行 .tl 檔案不過並不建議

目前 teal 可以工作在 lua5.1-5.4 包括 LuaJIT

* [源碼](https://github.com/teal-language/tl)