# 虚拟栈
duktape 參照 lua 模擬了一個 虛擬的棧 js 和 c++ 通過棧 使用 類似彙編的工作模式進行交互

# duk\_push\_context\_dump
此api可以將 當前棧信息 以字符串形式 入棧 對於調試c代碼 很有幫助
```c++
void duktape_context_dump(duk_context *ctx)
{
    duk_push_context_dump(ctx);
    printf("%s\n", duk_to_string(ctx, -1));
    duk_pop(ctx);
}
```

# c 調用 js
将 函数 参数 从左到右 入栈 之后 调用 duk_pcall 即可
```c++
#include "duktape.h"
#include <iostream>
#include <cassert>
 
int main(int argc, char* argv[])
{
	//初始化一個默認的 js 執行環境
	duk_context* ctx = duk_create_heap_default();
	//duk_context* ctx = duk_create_heap(NULL, NULL, NULL, NULL, NULL);
	assert(ctx);
	{
		//執行js
		if(0 != duk_peval_string(ctx, "var x=2;var y=3;function add (l,r) {print(l+r)}")){
			printf("eval failed: %s\n", duk_safe_to_string(ctx, -1));
		}
		else{
			//入栈 js 全局对象
			duk_push_global_object(ctx);
			//在指定 index 栈对象 中 查找 指定属性 并且入栈(不存在 入栈undefined)
			duk_get_prop_string(ctx, -1 /*index*/, "add");
			duk_get_prop_string(ctx, -2 , "x");
			duk_get_prop_string(ctx, -3 , "y");
 
			//打印当前栈情况
			duk_dump_context_stdout(ctx);
 
			//调用函数
			if(duk_pcall(ctx,2)){
				printf("error: %s\n", duk_to_string(ctx, -1));
			}
			//js所有函数 都有返回值 不定义 返回 undefined
			duk_dump_context_stdout(ctx);
			duk_pop(ctx);
 
			//pop global_object
			duk_pop(ctx);
		}
		duk_pop(ctx);
	}
	//釋放 js 環境
	duk_destroy_heap(ctx);
 
	return 0;
}
```

# js调用c
duk\_push\_*

将值 设置到 栈中 便可供js调用

```c++
#include "duktape.h"
#include <iostream>
#include <cassert>
 
//c扩展 js 函数
duk_ret_t my_native_func(duk_context *ctx) {
    double arg = duk_require_number(ctx, 0 /*index*/);
    duk_push_number(ctx, arg * arg);
    return 1;
}
 
int main(int argc, char* argv[])
{
	//初始化一個默認的 js 執行環境
	duk_context* ctx = duk_create_heap_default();
	//duk_context* ctx = duk_create_heap(NULL, NULL, NULL, NULL, NULL);
	assert(ctx);
	{
		//入栈 js 全局对象
		duk_push_global_object(ctx);
 
		//入栈 函数
		duk_push_c_function(ctx, my_native_func, 1 /*函数参数数量*/);
		//为函数 设置 名字 (其实是设置 全局对象 设置属性名 被设置的是栈顶位置的 属性名)
		//设置之后 此元素 出栈
		duk_put_prop_string(ctx, -2, "my_native_func");
		
		//打印当前栈情况
		duk_dump_context_stdout(ctx);
		duk_pop(ctx);
 
		assert(!duk_peval_string(ctx, "my_native_func(2)"));
		printf("value = %d\n",duk_get_int(ctx,-1));
		duk_pop(ctx);
	}
	//釋放 js 環境
	duk_destroy_heap(ctx);
 
	return 0;
}
```

> 在 c api 中 與duk交互 使用 一個 虛擬的棧   
> 在 c 擴展的 函數中 默認大小 參考值為 DUK\_API\_ENTRY\_STACK 64
> 
> //返回 棧大小 是否 足夠 extra  
> //如果  extra 爲 5 最大棧索引爲 4  
> duk\_bool\_t duk\_check\_stack(duk_context *ctx, duk\_idx\_t extra);
> 
> //同 duk\_check\_stack 然 如果 棧 大小不夠 自動 擴容  
> //如果 擴容失敗 返回 false  
> duk\_bool\_t duk\_check\_stack\_top(duk\_context *ctx, duk\_idx\_t top);
> 

duk\_require\_stack duk\_require\_stack\_top 類似 check 不過在失敗時 會自動 duk_throw 一個錯誤信息

