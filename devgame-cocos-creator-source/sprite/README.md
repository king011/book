# [貼圖資源](https://docs.cocos.com/creator/manual/zh/asset-workflow/sprite.html)

creator 目前支持 jpg 和 png 貼圖

# Texture2D SpriteFrame 

所有 導入到 creator 中的 貼圖 都是 [cc.Texture2D](https://docs.cocos.com/creator/api/zh/classes/Texture2D.html)

creator 會在 資源管理器 下 爲 Texture2D 創建一個 [cc.SpriteFrame](https://docs.cocos.com/creator/api/zh/classes/SpriteFrame.html)


修改 Sprite 組件的 spriteFrame 屬性 即可 實現 顯示圖像的 更改
