# [硬體鍵盤](https://docs.flame-engine.org/1.16.0/flame/inputs/hardware_keyboard_detector.html)

HardwareKeyboardDetector 元件可讓您直接監聽硬體鍵盤的事件，繞過 Flutter 中的 Focus 小工具。 它不會偵聽任何螢幕（軟體）鍵盤的事件。

該組件可以放置在組件樹中的任何位置。 例如，它可以附加到 Game 類別的根級別，或附加到被控制的玩家。 多個 HardwareKeyboardDetector 元件可以在同一個遊戲中共存，並且它們都會接收按鍵事件

此元件提供 onKeyEvent 事件處理程序，可以覆寫該事件處理程序，也可以將其作為建構函數中的參數傳遞。 每當使用者按下或釋放鍵盤上的任何按鍵以及按住某個鍵時，都會觸發此事件處理程序

按鍵事件流將被 Flutter 標準化，這意味著對於每個 KeyDownEvent 總是會有相應的 KeyUpEvent，中間可能會有一些 KeyRepeatEvent。 根據平台的不同，其中一些事件可能是“合成的”，即由框架人為創建，以保留正確的事件序列。 有關更多詳細信息，請參閱 Flutter 的 [HardwareKeyboard](https://api.flutter.dev/flutter/services/HardwareKeyboard-class.html)

當將此組件新增至組件樹或從組件樹中刪除時，存在類似的規範化保證。 如果在安裝 HardwareKeyboardDetector 時使用者按住任何按鍵，則會觸發人工 KeyDownEvents； 如果移除此元件時使用者按住按鍵，則將合成 KeyUpEvents

使用pauseKeyEvents屬性暫時停止/恢復onKeyEvents的傳遞。 當組件從組件樹中刪除時，事件也會停止傳遞

# [Constructors](https://docs.flame-engine.org/1.16.0/flame/inputs/hardware_keyboard_detector.html#constructors)

```
HardwareKeyboardDetector({void Function(KeyEvent)? onKeyEvent})
```

# [Properties](https://docs.flame-engine.org/1.16.0/flame/inputs/hardware_keyboard_detector.html#properties)

* **physicalKeysPressed&nbsp;→&nbsp;List&lt;PhysicalKeyboardKey&gt;** 目前在鍵盤（或類似鍵盤的裝置）上按下的按鍵清單。 這些鍵按按下的順序列出，但修改鍵除外，在某些系統上可能會無序列出
* **logicalKeysPressed&nbsp;→&nbsp;Set&lt;LogicalKeyboardKey&gt;** 目前在鍵盤上按下的一組邏輯鍵。 此集合對應於physicalKeysPressed列表，並可用於以與鍵盤佈局無關的方式搜尋按鍵
* **isControlPressed → bool** 如果目前正在按下 Ctrl 鍵，則為 True
* **isShiftPressed → bool** 如果目前正在按下 Shift 鍵，則為 True
* **isAltPressed → bool** 如果目前正在按下 Alt 鍵，則為 True
* **isNumLockOn → bool** 如果 Num Lock 目前打開，則為 true
* **isCapsLockOn → bool** 如果大寫鎖定目前打開，則為 True
* **isScrollLockOn → bool** 如果目前開啟 Scroll Lock，則為 true
* **pauseKeyEvents ←→ bool** 如果為真，鍵盤事件的傳遞將暫停

當 pauseKeyEvents 屬性設為 true 時，系統會為目前持有的所有按鍵產生 KeyUp 事件，就好像使用者已釋放它們一樣。 相反，當此屬性切換回 false，並且用戶當時按住某些鍵時，系統將產生 KeyDown 事件，就好像用戶剛開始按下這些按鈕一樣

# [Methods](https://docs.flame-engine.org/1.16.0/flame/inputs/hardware_keyboard_detector.html#methods)

* **onKeyEvent(KeyEvent event)** 如果您希望在按下、按住或釋放鍵盤上的任何按鍵時收到通知，請重寫此事件處理程序。 該事件將分別是 KeyDownEvent、KeyRepeatEvent 或 KeyUpEvent 之一
