# Brotli

Brotli 是 google 工程師 在 2015年9月 發佈的開源(MIT) 壓縮算法 側重與 http壓縮 目前已被 大部分 現代瀏覽器支持 以 **br** 作爲 內容編碼類型

* 源碼 [https://github.com/google/brotli](https://github.com/google/brotli)