# rsync

rsync 是 linux 下的一個開源(GPL3)的 資料傳輸工具 

rsync 即可作爲本地替代 cp 的指令，也可以在本地與遠程計算機間同步 檔案/檔案夾

* 官網 [https://rsync.samba.org/](https://rsync.samba.org/)
* 源碼 [https://git.samba.org/rsync.git](https://git.samba.org/rsync.git)