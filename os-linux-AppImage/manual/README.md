# [手動打包](https://docs.appimage.org/packaging-guide/manual.html)

下述指令可以將 app 檔案夾打包爲 app.AppImage
```
appimagetool-x86_64.AppImage app app.AppImage
```

app 檔案夾下通常包含下述檔案

* **AppRun** 這是被執行運行環境執行的檔案，建議下載[官方](https://github.com/AppImage/AppImageKit/releases)提供的此檔案即可，它會正確的設置各種環境並最終執行真實的打包程式
* **myapp.desktop** 是 AppImage 的設定檔案名稱固定爲 myapp.desktop
* **myapp.png** 桌面圖標
* **usr/bin/myapp** 真實的可執行程序，名稱不一定爲 myapp，將你的真實可執行程式複製到 usr/bin/ 檔案夾下即可
* **/usr/lib/libfoo.so.0** 將依賴庫複製到 /usr/lib 檔案夾下即可

注意因爲 AppImage 會被加載到一個隨機檔案夾下運行，所以程序中不要包含硬編碼的路徑否則可能會運行錯誤

# myapp.desktop

myapp.desktop 下應該至少包含下述內容

```
[Desktop Entry]
Name=MyApp
Exec=myapp
Icon=myapp
Type=Application
Categories=Utility;
```

* **Name** 指定程式名稱
* **Exec** 可執行檔案名稱
* **Icon** 可執行檔圖標，不帶後綴名，只會加載 png 作爲圖標
* **Type** 打包類型，目前來說通常都是 **Application**
* **Categories** [註冊類別](https://specifications.freedesktop.org/menu-spec/latest/category-registry.html#main-category-registry)

對於命令行程序通常應該加上 `Terminal=true`