# 創建 bash-completion 腳本

flutter bash-completion

# 創建項目
flutter create &lt;output directory&gt;
* -t, --template=&lt;type&gt;
    * app 創建應用
    * package 創建包
    * plugin 創建插件
* --org 域名 作爲 android/ios package name
    * 默認值爲 com.example
* --project-name 項目名稱 必須是 合法的 dart 包名
* -i, --ios-language ios開發語言
    * objc
    * swift (default)
* -a, --android-language android開發語言
    * java
    * kotlin (default)
...
# 運行項目
flutter run \[arguments\]
* --debug 默認選項
* --profile
* --release
...
