# Example
***.jsonet**
```js
//支持 單行註解

//支持 變量 定義
local version = "1.0.0";
local show = false;
local obj = {
    //使用 變量
    Version:version,
};
{
    /*
        支持多行註解
    */
    Person0:{
        //屬性名 可選擇 使用 ' " 擴起來 或 忽略
        Name:"Kate",
        "Full Name": self.Name +" beckinsale", //self 可調用 當前 屬性 別進行簡單的 運算
        'Lv':10,
        Array:[1,2,3],
        
        //最後的 , 可以選擇是否添加
        Who:"my love",
    },
    //複製一份配置
    Person1:self.Person0,
    //複製一份配置 並且 客製化
    Person2:self.Person0{
        //覆蓋 屬性 
        Name:"林青霞", //Full Name 也會變化
 
        //擴展屬性
        New:"新屬性",
    },
    Obj:obj,
}
```
**output**
```js
king@king-company ~/project/go/src/test/jsonnet $ jsonnet test.jsonnet 
{
   "Obj": {
      "Version": "1.0.0"
   },
   "Person0": {
      "Array": [
         1,
         2,
         3
      ],
      "Full Name": "Kate beckinsale",
      "Lv": 10,
      "Name": "Kate",
      "Who": "my love"
   },
   "Person1": {
      "Array": [
         1,
         2,
         3
      ],
      "Full Name": "Kate beckinsale",
      "Lv": 10,
      "Name": "Kate",
      "Who": "my love"
   },
   "Person2": {
      "Array": [
         1,
         2,
         3
      ],
      "Full Name": "林青霞 beckinsale",
      "Lv": 10,
      "Name": "林青霞",
      "New": "新屬性",
      "Who": "my love"
   }
}
```

# build on linux
## ubuntu
1. git clone https://github.com/google/jsonnet.git
2. cd jsonnet && mkdir jsonnet && cd jsonnet
3. export CFLAGS=-m64 CXXFLAGS=-m64
4. cmake ../
5. make
6. sudo make install
