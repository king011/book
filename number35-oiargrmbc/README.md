# 倉頡輸入法

倉頡輸入法 是由 朱邦復 於1976 創建的中文輸入法 方案 1978年由蔣緯國命名爲 倉頡輸入法

是應用最廣的 中文 形碼輸入法

最新版本爲 倉頡五代

* wiki [https://zh.wikipedia.org/wiki/%E5%80%89%E9%A0%A1%E8%BC%B8%E5%85%A5%E6%B3%95](https://zh.wikipedia.org/wiki/%E5%80%89%E9%A0%A1%E8%BC%B8%E5%85%A5%E6%B3%95)
* 手冊 [http://www.pascal-man.com/5cjbook/index.htm](http://www.pascal-man.com/5cjbook/index.htm)