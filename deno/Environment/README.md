# [環境變量](https://deno.land/manual@v1.28.1/basics/env_variables)

deno 提供了多種與環境變量相關的操作方法

# Built-in Deno.env

[Deno.env](https://deno.land/api@v1.25.3?s=Deno.env) 提供了 get/set 方法用於 獲取/設置 環境變量

```
Deno.env.set("FIREBASE_API_KEY", "examplekey123");
Deno.env.set("FIREBASE_AUTH_DOMAIN", "firebasedomain.com");

console.log(Deno.env.get("FIREBASE_API_KEY")); // examplekey123
console.log(Deno.env.get("FIREBASE_AUTH_DOMAIN")); // firebasedomain.com
```

# .env file

你還可以將環境變量放在 .env 檔案中，並使用標準庫 [dotenv](https://deno.land/std@0.165.0/dotenv/mod.ts) 來訪問

```
PASSWORD=Geheimnis
```


```
import { config } from "https://deno.land/std/dotenv/mod.ts";

const configData = await config();
const password = configData["PASSWORD"];

console.log(password);
// "Geheimnis"
```

# std/flags

標準庫 [std/flags](https://deno.land/std@0.165.0/flags/mod.ts) 提供了 命令行參數解析的功能
