# shadowsocks-libev

shadowsocks-libev 是用 c語言 實現的 shadowsocks 開源(GPL3.0)套件

源碼 [https://github.com/shadowsocks/shadowsocks-libev](https://github.com/shadowsocks/shadowsocks-libev)

# 安裝
## ubuntu

Debian 8 or higher, including oldstable (jessie), stable (stretch), testing (buster) and unstable (sid)  
Ubuntu 16.10 or higher

```sh
sudo apt update
sudo apt install shadowsocks-libev
```

Ubuntu 14.04 and 16.04

```sh
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:max-c-lv/shadowsocks-libev -y
sudo apt-get update
sudo apt install shadowsocks-libev
```

# ss-server
ss-server 是 shadowsocks 服務器 程式

以 king 用戶 運行 shadowsocks 服務器 在 0.0.0.0:1911 地址上 連接密碼爲 123  加密方式爲 rc4-md5
ss-server -a king -p 1911 -k 123 -m rc4-md5

```sh
$ ss-server -h
 
shadowsocks-libev 3.1.2
 
  maintained by Max Lv <max.c.lv@gmail.com> and Linus Yang <laokongzi@gmail.com>
 
  usage:
 
    ss-server
 
       -s <server_host>           Host name or IP address of your remote server.
       -p <server_port>           Port number of your remote server.
       -l <local_port>            Port number of your local server.
       -k <password>              Password of your remote server.
       -m <encrypt_method>        Encrypt method: rc4-md5, 
                                  aes-128-gcm, aes-192-gcm, aes-256-gcm,
                                  aes-128-cfb, aes-192-cfb, aes-256-cfb,
                                  aes-128-ctr, aes-192-ctr, aes-256-ctr,
                                  camellia-128-cfb, camellia-192-cfb,
                                  camellia-256-cfb, bf-cfb,
                                  chacha20-ietf-poly1305,
                                  salsa20, chacha20 and chacha20-ietf.
                                  The default cipher is rc4-md5.
 
       [-a <user>]                Run as another user.
       [-f <pid_file>]            The file path to store pid.
       [-t <timeout>]             Socket timeout in seconds.
       [-c <config_file>]         The path to config file.
       [-n <number>]              Max number of open files.
       [-i <interface>]           Network interface to bind.
       [-b <local_address>]       Local address to bind.
 
       [-u]                       Enable UDP relay.
       [-U]                       Enable UDP relay and disable TCP relay.
       [-6]                       Resovle hostname to IPv6 address first.
 
       [-d <addr>]                Name servers for internal DNS resolver.
       [--reuse-port]             Enable port reuse.
       [--fast-open]              Enable TCP fast open.
                                  with Linux kernel > 3.7.0.
       [--acl <acl_file>]         Path to ACL (Access Control List).
       [--manager-address <addr>] UNIX domain socket address.
       [--mtu <MTU>]              MTU of your network interface.
       [--mptcp]                  Enable Multipath TCP on MPTCP Kernel.
       [--no-delay]               Enable TCP_NODELAY.
       [--key <key_in_base64>]    Key of your remote server.
       [--plugin <name>]          Enable SIP003 plugin. (Experimental)
       [--plugin-opts <options>]  Set SIP003 plugin options. (Experimental)
 
       [-v]                       Verbose mode.
       [-h, --help]               Print this message.
```

# ss-local
ss-local 是 shadowsocks 客戶端 程式

以 在本地 1080 創建 sock5 代理 到 domain:1911  
ss-local -l 1080 -s domain -p 1911  -k 123 m rc4-md5 --reuse-port --fast-open
```json
{
	"local_address": "127.0.0.1",
	"local_port": 1080,
	"server": "domain",
	"server_port": 1911,
	"password": "123",
	"method": "rc4-md5",
	"fast_open": true,
	"reuse_port": true
}
```
```sh
$ ss-local -h
 
shadowsocks-libev 3.1.2
 
  maintained by Max Lv <max.c.lv@gmail.com> and Linus Yang <laokongzi@gmail.com>
 
  usage:
 
    ss-local
 
       -s <server_host>           Host name or IP address of your remote server.
       -p <server_port>           Port number of your remote server.
       -l <local_port>            Port number of your local server.
       -k <password>              Password of your remote server.
       -m <encrypt_method>        Encrypt method: rc4-md5, 
                                  aes-128-gcm, aes-192-gcm, aes-256-gcm,
                                  aes-128-cfb, aes-192-cfb, aes-256-cfb,
                                  aes-128-ctr, aes-192-ctr, aes-256-ctr,
                                  camellia-128-cfb, camellia-192-cfb,
                                  camellia-256-cfb, bf-cfb,
                                  chacha20-ietf-poly1305,
                                  salsa20, chacha20 and chacha20-ietf.
                                  The default cipher is rc4-md5.
 
       [-a <user>]                Run as another user.
       [-f <pid_file>]            The file path to store pid.
       [-t <timeout>]             Socket timeout in seconds.
       [-c <config_file>]         The path to config file.
       [-n <number>]              Max number of open files.
       [-i <interface>]           Network interface to bind.
       [-b <local_address>]       Local address to bind.
 
       [-u]                       Enable UDP relay.
       [-U]                       Enable UDP relay and disable TCP relay.
 
       [--reuse-port]             Enable port reuse.
       [--fast-open]              Enable TCP fast open.
                                  with Linux kernel > 3.7.0.
       [--acl <acl_file>]         Path to ACL (Access Control List).
       [--mtu <MTU>]              MTU of your network interface.
       [--mptcp]                  Enable Multipath TCP on MPTCP Kernel.
       [--no-delay]               Enable TCP_NODELAY.
       [--key <key_in_base64>]    Key of your remote server.
       [--plugin <name>]          Enable SIP003 plugin. (Experimental)
       [--plugin-opts <options>]  Set SIP003 plugin options. (Experimental)
 
       [-v]                       Verbose mode.
       [-h, --help]               Print this message.
```

# ss-redir
ss-redir 可以配合 iptables 實現 透明代理

1. 使用 ss-redir 連接 shadowsocks 服務器

	```sh
	ss-redir -b 0.0.0.0 -l 1911 -s ssh.xxx.com -p 1911  -k passwd  m rc4-md5 --reuse-port --fast-open
	```

1. 開啓網卡轉發功能

	```sh
	# 檢查 是否啓用 網卡轉發 如果爲1 則以開啓
	cat /proc/sys/net/ipv4/ip_forward

	# 如果爲0 編輯 /etc/sysctl.conf 添加如下設置
	net.ipv4.ip_forward=1
	# 執行 如下指令使用 修改立刻生效
	sysctl -p
	```

1. 修改 iptables 規則

	```sh
	# 放行 到 ss-redir 的tcp連接
	iptables -A INPUT -p tcp -m state --state NEW -m tcp --dport 1911 -j ACCEPT
	```
	
	```sh
	# 創建 nat/tcp 轉發鏈 用於 轉發 tcp流
	iptables -t nat -N SS-TCP

	# 放行 環回地址 保留地址 特殊地址
	iptables -t nat -A SS-TCP -d 0/8 -j RETURN
	iptables -t nat -A SS-TCP -d 127/8 -j RETURN
	iptables -t nat -A SS-TCP -d 10/8 -j RETURN
	iptables -t nat -A SS-TCP -d 169.254/16 -j RETURN
	iptables -t nat -A SS-TCP -d 172.16/12 -j RETURN
	iptables -t nat -A SS-TCP -d 192.168/16 -j RETURN
	iptables -t nat -A SS-TCP -d 224/4 -j RETURN
	iptables -t nat -A SS-TCP -d 240/4 -j RETURN

	# 放行 發往 ss-server 服務器的 數據 需要將 -d 地址 改爲服務器 ip
	iptables -t nat -A SS-TCP -d x.x.x.x -j RETURN

	# 重定向 tcp 數據包到 ss-redir 監聽端口
	iptables -t nat -A SS-TCP -p tcp -j REDIRECT --to-ports 1911

	# 本機 tcp 數據包流向 SS-TCP 鏈
	iptables -t nat -A OUTPUT -p tcp -j SS-TCP
	# 內網 tcp 數據包流向 SS-TCP 鏈
	iptables -t nat -A PREROUTING -p tcp -s 192.168/16 -j SS-TCP

	# 內網數據包源 NAT
	iptables -t nat -A POSTROUTING -s 192.168/16 -j MASQUERADE
	```

## pptpd

配合 pptpd 可以將 ss-redir 轉換爲一個 vpn

1. 安裝 pptpd

	```sh
	sudo apt-get install pptpd
	```
	
1. 編輯 /etc/pptpd.conf 設置

	```txt
	#
	# (Recommended)
	# 服務器 id 
	localip 192.168.0.1
	# 客戶端ip
	remoteip 192.168.0.234-238,192.168.0.245
	# or
	#localip 192.168.0.234-238,192.168.0.245
	#remoteip 192.168.1.234-238,192.168.1.245
	```

1. 編輯 /etc/ppp/chap-secrets 設置 客戶端連接 用戶名 密碼 ip

	```txt
	# Secrets for authentication using CHAP
	# client        server  secret                  IP addresses

	# 用戶名        pptpd   密碼            ip (* 自動分配)
	king            pptpd   XXXXXXX     *
	```

1. 編輯 etc/ppp/pptpd-options 設置dns 地址

	```txt
	ms-dns 8.8.8.8
	ms-dns 8.8.4.4
	```

1. 運行 pptpd

	```sh
	sudo systemctl start pptpd.service
	```

1. 設置 iptables 將 pptpd 轉發到 ss-redir

	```sh
	# 內網數據包源 NAT
	iptables -t nat -A POSTROUTING -s 192.168/16 -j MASQUERADE
	iptables -t nat -A POSTROUTING -s 192.168/16 -o wlp4s0b1 -j MASQUERADE
	```
	
# ss-tunnel
ss-tunnel 用來 解決 dns 問題 和 ss-local 用法 類似 只是 多了 -L 參數 指定 dns 服務器

ss-tunnel 獲取到 dns 請求 \-&gt; ss-server \-&gt; \-&gt; -L指定的dns服務器 \-&gt; ss-server \-&gt; ss-tunnel \-&gt; 響應 dns

> ss-server 和 ss-tunnel 需要 加上 -u 轉發 udp 因爲 dns 是通過 udp 發送數據的
> 


