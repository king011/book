# angular2-toaster

angular2 toaster 是一個 開源(MIT) 頁面通知 組件

效果 如下圖右下腳

![](assets/toaster.png)


* npm [https://www.npmjs.com/package/angular2-toaster](https://www.npmjs.com/package/angular2-toaster)
* 源碼 [https://github.com/stabzs/Angular2-Toaster](https://github.com/stabzs/Angular2-Toaster)
