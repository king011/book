# docker

gitlab 提供了 docker 發佈包 可以快速 使用  

[官方使用說明](https://docs.gitlab.com/omnibus/docker/)

1. 安裝好 docker 環境
1. 獲取  gitlab 映像檔

	```sh
	docker pull gitlab/gitlab-ce
	```
	
	> 映像檔 目前有 1.48 G 請在網路快速的地方下載
	
1. 啓動

	```sh
	docker run --detach \
	--publish 22443:443 --publish 2280:80  --publish 2222:22 \
	--name gitlab \
	--memory 4g \
	--restart always \
	--volume /srv/gitlab/config:/etc/gitlab \
	--volume /srv/gitlab/logs:/var/log/gitlab \
	--volume /srv/gitlab/data:/var/opt/gitlab \
	gitlab/gitlab-ce
	```
	
	> * --publish 指定了 將 主機的 三個 端口 映射到 容器 分別 用於 https http ssh
	> * --memory 4g 限定了 容器最大內存 佔用 4g 是官方 推薦配置
	> * --volume 指定 掛接 目錄 用於  配置gitlab和數據備份

# 持久數據
volume 安裝了 三個 資料卷 用來 存儲 持久化數據

| 主機檔案夾 | 容器檔案夾 | 用途 |
| -------- | -------- | -------- |
| /srv/gitlab/config     | /etc/gitlab     | 存儲gitlab配置     |
| /srv/gitlab/logs     | /var/log/gitlab      | 存儲日誌     |
| /srv/gitlab/data     | /var/opt/gitlab     | 存儲應用數據     |

# 配置 gitlab
gitlab 的配置檔案是 /etc/gitlab/gitlab.rb 
```sh
# 在 gitlab 容器中 啓動一個 bash shell
sudo docker exec -it gitlab /bin/bash
```
```sh
vi /etc/gitlab/gitlab.rb 
```

> 因爲已經 建立了 資料卷 所以 也可以直接修改 主機上的配置檔案
>
> ```sh
> sudo vi /srv/gitlab/config/gitlab.rb
> ```
> 

修改配置後 需要重啓 gitlab 以便配置生效

```sh
sudo docker restart gitlab
```

