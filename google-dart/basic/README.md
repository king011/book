# 基本概念

1. dart 變量中所有內容都是 一個對象
 
  每個對象都是一個 class的 instance，function 和 null 都是 對象
	
	所有對象都 派生自 **Object**
	
1. dart 是強型別語言 

  使用 var 定義 則可以 自動推導型別
	
	使用 dynamic 定義 類似 boost::any
	
1. dart 支持 泛型 List&lt;int&gt; List&lt;dynamic&gt;

1. dart 沒有 public protected private 關鍵字 命名 以 **\_** 開始 的爲私有

# Keywords

<table class="table table-striped nowrap">
  <tbody>
    <tr>
      <td>
<a href="#abstract-classes">abstract</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#important-concepts">dynamic</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#implicit-interfaces">implements</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#importing-only-part-of-a-library">show</a>&nbsp;<sup title="contextual keyword" alt="contextual keyword">1</sup>
</td>
    </tr>
    <tr>
      <td>
<a href="#type-test-operators">as</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="#if-and-else">else</a></td>
      <td>
<a href="#using-libraries">import</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#class-variables-and-methods">static</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
    </tr>
    <tr>
      <td><a href="#assert">assert</a></td>
      <td><a href="#enumerated-types">enum</a></td>
      <td><a href="#for-loops">in</a></td>
      <td><a href="#extending-a-class">super</a></td>
    </tr>
    <tr>
      <td>
<a href="#asynchrony-support">async</a>&nbsp;<sup title="contextual keyword" alt="contextual keyword">1</sup>
</td>
      <td>
<a href="/guides/libraries/create-library-packages">export</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="https://stackoverflow.com/questions/28595501/was-the-interface-keyword-removed-from-dart" class="external">interface</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="#switch-and-case">switch</a></td>
    </tr>
    <tr>
      <td>
<a href="#asynchrony-support">await</a>&nbsp;<sup title="limited reserved word" alt="limited reserved word">3</sup>
</td>
      <td><a href="#extending-a-class">extends</a></td>
      <td><a href="#type-test-operators">is</a></td>
      <td>
<a href="#generators">sync</a>&nbsp;<sup title="contextual keyword" alt="contextual keyword">1</sup>
</td>
    </tr>
    <tr>
      <td><a href="#break-and-continue">break</a></td>
      <td>
<a href="https://stackoverflow.com/questions/24929659/what-does-external-mean-in-dart" class="external">external</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#libraries-and-visibility">library</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="#constructors">this</a></td>
    </tr>
    <tr>
      <td><a href="#switch-and-case">case</a></td>
      <td>
<a href="#factory-constructors">factory</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#adding-features-to-a-class-mixins">mixin</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="#throw">throw</a></td>
    </tr>
    <tr>
      <td><a href="#catch">catch</a></td>
      <td><a href="#booleans">false</a></td>
      <td><a href="#using-constructors">new</a></td>
      <td><a href="#booleans">true</a></td>
    </tr>
    <tr>
      <td><a href="#instance-variables">class</a></td>
      <td><a href="#final-and-const">final</a></td>
      <td><a href="#default-value">null</a></td>
      <td><a href="#catch">try</a></td>
    </tr>
    <tr>
      <td><a href="#final-and-const">const</a></td>
      <td><a href="#finally">finally</a></td>
      <td>
<a href="#catch">on</a>&nbsp;<sup title="contextual keyword" alt="contextual keyword">1</sup>
</td>
      <td>
<a href="#typedefs">typedef</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
    </tr>
    <tr>
      <td><a href="#break-and-continue">continue</a></td>
      <td><a href="#for-loops">for</a></td>
      <td>
<a href="#overridable-operators">operator</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="#variables">var</a></td>
    </tr>
    <tr>
      <td>
<a href="/guides/language/sound-problems#the-covariant-keyword">covariant</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#functions">Function</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="/guides/libraries/create-library-packages#organizing-a-library-package">part</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="https://medium.com/dartlang/dart-2-legacy-of-the-void-e7afb5f44df0" class="external">void</a></td>
    </tr>
    <tr>
      <td><a href="#switch-and-case">default</a></td>
      <td>
<a href="#getters-and-setters">get</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td><a href="#catch">rethrow</a></td>
      <td><a href="#while-and-do-while">while</a></td>
    </tr>
    <tr>
      <td>
<a href="#lazily-loading-a-library">deferred</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#importing-only-part-of-a-library">hide</a>&nbsp;<sup title="contextual keyword" alt="contextual keyword">1</sup>
</td>
      <td><a href="#functions">return</a></td>
      <td><a href="#adding-features-to-a-class-mixins">with</a></td>
    </tr>
    <tr>
      <td><a href="#while-and-do-while">do</a></td>
      <td><a href="#if-and-else">if</a></td>
      <td>
<a href="https://api.dartlang.org/stable/dart-core/Set-class.html" class="external">set</a>&nbsp;<sup title="built-in-identifier" alt="built-in-identifier">2</sup>
</td>
      <td>
<a href="#generators">yield</a>&nbsp;<sup title="limited reserved word" alt="limited reserved word">3</sup>
</td>
    </tr>
  </tbody>
</table>