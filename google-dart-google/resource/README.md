# resource

resource 是 dart 官方團隊維護的一個庫 可以在運行時 加載一個 package 的 資源

[https://pub.dartlang.org/packages/resource#-installing-tab-](https://pub.dartlang.org/packages/resource#-installing-tab-)

```
import 'package:resource/resource.dart' show Resource;
import 'dart:convert' show utf8;

main() async {
  var resource = new Resource("package:foo/foo_data.txt");
  var string = await resource.readAsString(encoding: utf8);
  print(string);
}
```