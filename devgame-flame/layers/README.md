# [Layers and Snapshots](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html)

圖層和快照具有一些共同的功能，包括預先渲染和快取物件以提高效能的能力。 然而，它們也具有獨特的功能，使它們更適合不同的用例。

Snapshot 是一個可以加入任何 PositionComponent 的 mixin。 將此用於：

* 混合到現有的遊戲物件（即 PositionComponents）中。
* 快取渲染起來很複雜的遊戲對象，例如精靈。
* 多次繪製同一個對象，而不是每次都渲染它。
* 擷取影像快照以另存為螢幕截圖（例如）。

Layer 是一個類別。 使用或擴展此類用於：
* 使用邏輯層（例如 UI、前景、主要、背景）建立遊戲。
* 將物件分組以形成複雜的場景，然後對其進行快取（例如背景圖層）。
* 處理器支援。 圖層允許使用者定義的處理器運行預渲染和後渲染。

# [Layers](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#layers)

圖層可讓您按上下文對渲染進行分組，並允許您預先渲染事物。 例如，這使得渲染遊戲中在記憶體中不會發生太大變化的部分（例如背景）成為可能。 透過這樣做，您將釋放處理能力來處理每個遊戲週期需要渲染的更多動態內容。

Flame 上有兩種類型的圖層：

* **DynamicLayer**: 對於正在移動或變化的事物。
* **PreRenderedLayer**: 對於靜態的東西。

## [DynamicLayer](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#dynamiclayer)

動態圖層是每次在畫布上繪製時都會渲染的圖層。 顧名思義，它適用於動態內容，對於對具有相同上下文的物件進行分組渲染最有用。

使用範例：

```
class GameLayer extends DynamicLayer {
  final MyGame game;

  GameLayer(this.game);

  @override
  void drawLayer() {
    game.playerSprite.render(
      canvas,
      position: game.playerPosition,
    );
    game.enemySprite.render(
      canvas,
      position: game.enemyPosition,
    );
  }
}

class MyGame extends Game {
  // Other methods omitted...

  @override
  void render(Canvas canvas) {
    gameLayer.render(canvas); // x and y can be provided as optional position arguments
  }
}
```

## [PreRenderedLayer](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#prerenderedlayer)

預渲染圖層僅渲染一次，快取在記憶體中，然後複製到遊戲畫布上。 它們對於快取遊戲過程中不會改變的內容非常有用，例如背景。

使用範例：

```
class BackgroundLayer extends PreRenderedLayer {
  final Sprite sprite;

  BackgroundLayer(this.sprite);

  @override
  void drawLayer() {
    sprite.render(
      canvas,
      position: Vector2(50, 200),
    );
  }
}

class MyGame extends Game {
  // Other methods omitted...

  @override
  void render(Canvas canvas) {
    // x and y can be provided as optional position arguments.
    backgroundLayer.render(canvas);
  }
}
```

## [Layer Processors](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#layer-processors)

Flame還提供了一種在圖層上新增處理器的方法，這是在整個圖層上添加效果的方法。 目前，只有 ShadowProcessor 開箱即用，該處理器在您的圖層上渲染背景陰影。

要將處理器新增至圖層，只需將它們新增至層預處理器或後處理器清單即可。 例如：

```
// Works the same for both DynamicLayer and PreRenderedLayer
class BackgroundLayer extends PreRenderedLayer {
  final Sprite sprite;

  BackgroundLayer(this.sprite) {
    preProcessors.add(ShadowProcessor());
  }

  @override
  void drawLayer() { /* omitted */ }

  // ...
```

可以透過擴充 LayerProcessor 類別來建立自訂處理器。

您可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/layers_example.dart)查看圖層的工作範例。

# [Snapshots](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#snapshots)

快照是圖層的替代方案。 Snapshot mixin 可以套用於任何 PositionComponent。

```
class SnapshotComponent extends PositionComponent with Snapshot {}

class MyGame extends FlameGame {
  late final SnapshotComponent root;

  @override
  Future<void> onLoad() async {
    // Add a snapshot component.
    root = SnapshotComponent();
    add(root);
  }
}
```

## [Render as a snapshot](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#render-as-a-snapshot)

在啟用快照的元件上將 renderSnapshot 設為 true（預設值）的行為與 PreRenderedLayer 類似。 此元件僅渲染一次，快取在記憶體中，然後複製到遊戲畫布上。 它們對於快取遊戲過程中不會改變的內容非常有用，例如背景。

```
class SnapshotComponent extends PositionComponent with Snapshot {}

class MyGame extends FlameGame {
  late final SnapshotComponent root;
  late final SpriteComponent background1;
  late final SpriteComponent background2;

  @override
  Future<void> onLoad() async {
    // Add a snapshot component.
    root = SnapshotComponent();
    add(root);

    // Add some children.
    final background1Sprite = Sprite(await images.load('background1.png'));
    background1 = SpriteComponent(sprite: background1Sprite);
    root.add(background1);

    final background2Sprite = Sprite(await images.load('background2.png'));
    background2 = SpriteComponent(sprite: background2Sprite);
    root.add(background2);

    // root will now render once (itself and all its children) and then cache
    // the result. On subsequent render calls, root itself, nor any of its
    // children, will be rendered. The snapshot will be used instead for
    // improved performance.
  }
}
```

# [Regenerating a snapshot](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#regenerating-a-snapshot)

啟用快照的元件將產生其整個樹狀圖（包括其子級）的快照。 如果任何子項發生變化（例如，它們的位置發生變化，或它們被動畫化），則呼叫 takeSnapshot 來更新快取的快照。 如果它們變化非常頻繁，最好不要使用快照，因為不會有任何效能優勢。

渲染快照的元件仍然可以進行轉換，而不會產生任何效能成本。 拍攝快照後，組件仍可縮放、移動和旋轉。 但是，如果元件的內容發生變化（正在渲染的內容），則必須透過呼叫 takeSnapshot 重新產生快照。

## [Taking a snapshot](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#taking-a-snapshot)

啟用快照的元件可用於隨時產生快照，即使 renderSnapshot 設定為 false 也是如此。 當獲取全部或部分遊戲的靜態快照可能有用時，這對於進行螢幕擷取或任何其他目的很有用。

快照始終在不套用變換的情況下產生 - 即，就好像啟用快照的元件位於位置 (0,0) 並且未套用縮放或旋轉一樣。

快照儲存為 Picture，但可以使用 snapshotToImage 將其轉換為 Image。

```
class SnapshotComponent extends PositionComponent with Snapshot {}

class MyGame extends FlameGame {
  late final SnapshotComponent root;

  @override
  Future<void> onLoad() async {
    // Add a snapshot component, but don't use its render mode.
    root = SnapshotComponent()..renderSnapshot = false;
    add(root);

    // Other code omitted.
  }

  // Call something like this to take an image snapshot at any time.
  void takeSnapshot() {
    root.takeSnapshot();
    final image = root.snapshotToImage(200, 200);
  }
}
```

## [Snapshots that are cropped or off-center](https://docs.flame-engine.org/1.16.0/flame/rendering/layers.html#snapshots-that-are-cropped-or-off-center)

有時，您的快照影像可能會被裁剪，或不在預期的位置。

這是因為 Picture 的內容可以定位在相對於原點的任何位置，但當它轉換為 Image 時，影像始終從 0,0 開始。 這意味著任何具有 -ve 位置的內容都將被裁剪。

處理這個問題的最佳方法是確保您的快照組件相對於您的遊戲始終位於位置 0,0 並且您永遠不會移動它。 這意味著圖像通常會包含您期望的內容。

然而，這並不總是可能的。 若要在將快照轉換為影像之前移動（或旋轉或縮放等）快照，請將變換矩陣傳遞給 snapshotToImage。

```
// Call something like this to take an image snapshot at any time.
void takeSnapshot() {
  // Prepare a matrix to move the snapshot by 200,50.
  final matrix = Matrix4.identity()..translate(200.0,50.0);

  root.takeSnapshot();
  final image = root.snapshotToImage(200, 200, transform: matrix);
}
```