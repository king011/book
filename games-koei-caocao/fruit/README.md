# 水果

將 lv3 的 武器或護俱 賣給商人即可獲取神奇的水果 這些水果各具功效 能夠 強化角色的基本能力

| 裝備種類 | 水果 | 效果 |
| -------- | -------- | -------- |
| 槍類     | 武力果     | +2     |
| 弓類     | 好運果     | +2     |
| 劍類     | 經驗果     | +2     |
| 棍類     | 好運果     | +2     |
| 投石     | 武力果     | +2     |
| 扇類     | 智力果     | +2     |
| 寶劍     | 智力果     | +2     |
| 衣服     | 敏捷果     | +2     |
| 鎧甲     | 統率果     | +2     |


新武將加入時會依據他她們加入等級帶入不同等級的裝備，控制好加入武將等級可以白票武器賣掉換果子:



| 武將等級 | 裝備等級 |
| -------- | -------- |
| 7 級以下     | 1     |
| 8 到 11 級     | 2     |
| 12 到 15 級     | 3     |
| 16 到 19 級     | 1     |
| 20 到 23 級     | 2     |
| 24 到 27 級     | 3     |
| ...     | ...     |


# 水果消耗
* <font color="green">綠色</font> -> 推薦(效果好或容易達成)
* <font color="red">紅色</font> -> 強力推薦(效果好並且容易達成)
* <font color="blue">藍色</font> -> 無消耗，自帶加成


| 武將 | 智力果(精神) | 武力果(攻擊) | 好運果(士氣) | 敏捷果(爆發) | 統率果(防禦) |
| -------- | -------- | -------- | -------- | -------- | -------- |
| * 君主     | A      | A      | A      | A      | A      |
| 1. 曹操     |      |      |      |      |      |
| * 騎兵     | B    | S      | B      | B      | A      |
| 1. 夏侯惇     | B->A 13    |      | B->A 12    | <font color="blue">B->A</font>   |      |
| 2. 張遼     | <font color="red">B->A 2</font>     |      | <font color="blue">B->A</font>     | <font color="green">B->A 6</font>     |      |
| 3. 曹仁     | B->A 12     | <font color="red">A -> S 1</font>     | B->A 14     | B->A 10     |      |
| 4. 曹彰     | B->A 16     |      | B->A 15     | B->A 13     |      |
| 5. 關羽     | <font color="blue">B->A</font>      |      | B->A 14     | B->A 11     |      |
| * 步兵     | A      | B      | B      | B      | S      |
| 1. 李典     | <font color="red">B->A 1</font>     | <font color="red">B->A 4</font>     | B->A 17     | B->A 8     |      |
| 2. 樂進     |      | <font color="green">B->A 6</font>      | <font color="green">B->A 2</font>      | B->A 18     |      |
| 3. 曹洪     |      | B->A 13     | B->A 10     | B->A 12     | <font color="red">A->S 6</font>     |
| 4. 龐德     |      |      | B->A 15    | B->A 14     |      |
| * 弓騎     | B      | S      | A      | B      | B      |
| 1. 夏侯淵     | B->A 14     |      |      | B->A 12     | <font color="green">B->A 5</font>     |
| 2. 張郃     | B->A 12     |      |      | B->A 8     | B->A 11     |
| 3. 曹丕     | B->A 11     | A->S 10     |      | B->A 16     |      |
| * 弓兵     | B      | A      | S      | B      | B      |
| 1. 徐晃     | B->A 17     |      |      | <font color="green">B->A 6</font>     | B->A      |
| 2. 于禁     | B->A 15     |      |      | B->A      | <font color="green">B->A 7</font>     |
| * 炮兵     | B      | S      | A      | C      | A      |
| 1. 劉曄     | <font color="green">B->A 3</font>     | <font color="green">A->S 9</font>     |      | C->B 9      |      |
| * 武術家     | C      | A      | B      | S      | A      |
| 1. 典韋     | C->B 9     |      | B->A 11     |      |      |
| * 賊兵     | C      | S      | S      | B      | B      |
| 1. 許褚     | C->B 17     |      |      | B->A 11     | B->A      |
| * 舞娘     | B      | A      | B      | S      | B      |
| 1. 貂嬋     | B->A 11     |      | <font color="green">B->A 4</font>     |      | B->A 13     |
| * 策士     | S      | B      | B      | B      | B      |
| 1. 荀彧     |      | B->A 18     | B->A 14     | B->A 17     | <font color="red">B->A 4</font>     |
| 2. 程昱     |      | B->A 16     | B->A 7     | B->A 13     | B->A 14     |
| * 道士     | S      | C      | B      | A      | B      |
| 1. 郭嘉     |      | C->B 24     | B->A      |      | B->A 13     |
| 2. 賈詡     |      | C->B 9     | B->A 6     |      | B->A 11     |
| * 風水士     | S      | C      | A      | A      | C      |
| 1. 荀攸     |      | C->B 18     |      |      | C->B      |
| 2. 滿寵     |      | C->B 8     |      |      | C->B 1     |
| * 騎馬策士     | S      | A      | C      | B      | B      |
| 1. 司馬懿     |      |      | C->B 14     | C->B 2 or B->A 22     | B->A 11     |


