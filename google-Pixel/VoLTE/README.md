# 678 VoLTE

pixel 6 7 8 可以在未 root 的情況下啓用 volte 功能

# 查看 VoLTE 狀態

1. 在撥號頁面輸入 **\*#\*#4636#\*#\***
2. 選擇 **手機資訊**
3. 左上角菜單選擇 **IMS 服務狀態**

下面是無法使用 VoLTE 的狀態:  
![](assets/no.jpg)

下圖是可以使用 VoLTE 的狀態:  
![](assets/yes.jpg)

# 安裝 Shizuku

在 gogole 商店安裝 [Shizuku](https://play.google.com/store/apps/details?id=moe.shizuku.privileged.api) 或從[官網](https://shizuku.rikka.app/download/) 下載安裝

這是一個使用 adb 與 android 通信來執行特權 api 的平臺

因爲是使用 adb 所以

1. 連接 wifi
2. Shizuku 選擇配對
2. 打開手機開發者選項，啓用無線調試(點擊它 選擇使用配對碼配對)
3. Shizuku 輸入配對碼
4. Shizuku 選擇啓動

# pixel-volte-patch
在 Shizuku 啓動成功後，安裝 [pixel-volte-patch](https://github.com/kyujin-cho/pixel-volte-patch/releases/tag/1.2.8) 並運行

1. 首次運行選擇，允許 Shizuku 授權
2. 選擇右下角的 SIM 卡
3. 啓用 **Enable VoLTE**(第一個) 和 **Enable Enhanced 4G LTE(LTE+)** (通常默認勾選)

完成此時可以在朝鮮使用完整的 4g 功能和 4g 通話。即使關閉或卸載 pixel-volte-patch/Shizuku 都行但不建議，因爲如果 更新了 android 系統 可能需要重複此步驟 再次啓用 VoLTE