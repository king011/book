# [net/http](https://pkg.go.dev/net/http)
http 包 提供了 http 服務器 和 客戶端 相關 工具

# Cookie

struct Cookie 表示在 Response Header 中的 Set-Cookie 或者 Request Header 中的 Cookie

```
type Cookie struct {
	Name  string
	Value string

	Path       string    // optional
	Domain     string    // optional
	Expires    time.Time // optional
	RawExpires string    // for reading cookies only

	// MaxAge=0 means no 'Max-Age' attribute specified.
	// MaxAge<0 means delete cookie now, equivalently 'Max-Age: 0'
	// MaxAge>0 means Max-Age attribute present and given in seconds
	MaxAge   int
	Secure   bool
	HttpOnly bool
	SameSite SameSite
	Raw      string
	Unparsed []string // Raw text of unparsed attribute-value pairs
}

// 返回 cookie 在 header 中的字符串，如果 僅設置了 name/value 或 name 無效,將返回空字符串
// 如果設置了 name/value 之外的值，將返回 set-cookie 的值
func (c *Cookie) String() string

// 爲 http 響應設置 Set-Cookie header
func SetCookie(w ResponseWriter, cookie *Cookie)

// 讀取響應中所有的 Set-Cookie header
func (r *Response) Cookies() []*Cookie {
	return readSetCookies(r.Header)
}


// 讀取請求中所有的 Cookie
func (r *Request) Cookies() []*Cookie {
	return readCookies(r.Header, "")
}

// 返回請求中指定名稱的 cookie 如果指定了多個同名的只返回 第一個找到的
func (r *Request) Cookie(name string) (*Cookie, error) 

// 向請求中添加一個 cookie
func (r *Request) AddCookie(c *Cookie) 
```



# 服務器

* ServeMux 爲服務器 提供了 路由
* ResponseWriter 提供了響應
* Request 保存了 請求信息

```
package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"time"
)

// Addr .
const Addr = `:8080`

func main() {
	// 創建一個路由
	mux := http.NewServeMux()
	// 註冊主路由 所有 未被匹配到的 都會被路由到此
	mux.HandleFunc(`/`, home)

	// /json 會匹配 /json /json/
	mux.HandleFunc(`/json`, json)
	// mux 會安裝 最長匹配原則 匹配路由
	mux.HandleFunc(`/json/xml`, xml)
	mux.HandleFunc(`/json/xml/`, xml2) // 也支持分別 註冊 以 / 結尾的路由

	// 返回 檔案
	mux.HandleFunc(`/main.go`, source)
	// 使用 gzip
	mux.HandleFunc(`/gzip`, gzipHandler)
	// 分塊傳輸
	mux.HandleFunc(`/chunked`, chunked)

	// 支持 以主機名 開始 按照 主機名 分流 路由
	mux.HandleFunc(`localhost/`, localhost)

	// 監聽 tcp
	l, e := net.Listen(`tcp`, Addr)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`work at`, Addr)
	// 運行 http 服務
	http.Serve(l, mux)
}

func home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(`Content-Type`, `text/plain; charset=utf-8`)
	w.Write([]byte(`cerberus is an idea`))
}
func json(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(`Content-Type`, `application/json`)
	w.Write([]byte(`{"name":"cerberus","lv":1}`))
}

func xml(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(`Content-Type`, `application/xml`)
	w.Write([]byte(`<root><name>cerberus</name><lv>1</lv></root>`))
}
func xml2(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(`Content-Type`, `application/xml`)
	w.Write([]byte(`<root><name>kate</name><lv>2</lv></root>`))
}
func source(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, `main.go`)
}
func gzipHandler(w http.ResponseWriter, r *http.Request) {
	f, e := os.Open(`main.go`)
	if e != nil {
		w.Header().Set(`Content-Type`, `text/plain; charset=utf-8`)
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("not found"))
		return
	}
	defer f.Close()

	header := w.Header()
	header.Set(`Content-Type`, `text/plain; charset=utf-8`)
	header.Set(`Content-Encoding`, `gzip`)

	gw := gzip.NewWriter(w)
	//	golang 會 主動設置 Content-Length 無需額外操作
	io.Copy(gw, f)
	// 對於大檔案 應該使用 chunked 傳輸 既 http.Flusher
	gw.Close()
}
func chunked(w http.ResponseWriter, r *http.Request) {
	flusher, ok := w.(http.Flusher)
	if !ok {
		panic("expected http.ResponseWriter to be an http.Flusher")
	}

	// flusher 會主動設置 Transfer-Encoding: chunked 無需額外操作
	header := w.Header()
	header.Set(`Content-Type`, `application/json`)

	// 傳輸塊
	strs := []string{
		`{"name":`,
		`"cerberus`,
		`"`,
		`,"lv":1}`,
	}
	for _, str := range strs {
		// 傳輸塊數據
		w.Write([]byte(str))
		flusher.Flush()
		time.Sleep(time.Second)
	}
}
func localhost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(`Content-Type`, `text/plain; charset=utf-8`)
	w.Write([]byte(`this is cerberus's localhost`))
}
```