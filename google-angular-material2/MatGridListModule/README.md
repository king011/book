# 網格

Grid List 是一種以網格呈現資料的方式 也可以直接將 Grid List 當作 網格佈局系統使用 (angular 團隊有維護一個 [angular/flex-layout](https://github.com/angular/flex-layout) 佈局系統 但到目前爲止 還一直處於 beta 狀態)

```
import {MatGridListModule} from '@angular/material/grid-list';
```

![](assets/01-grid-list-basic-concept.png)

* Grid List 使用 網格的概念 每個 格子 稱爲 **Cell**
* **Tile** 則代表用來放置內容的 容器 且一個 Tile 可以橫跨多個 Cells

![](assets/02-grid-list-cell-tile.png)

# 使用 Grid List

* &lt;mat\-grid\-list&gt; 用來 設定 Grid List 基本狀態
* &lt;mat\-grid\-tile&gt; 用來在 mat-grid-list 中 放置 tile

mat-grid-list 會自動鋪滿寬度 但至少要指定 cols 然後 將寬度 平均分爲 cols 個 cell

```html
<mat-grid-list cols="3">
  <mat-grid-tile style="background: red">
    Tile 1
  </mat-grid-tile>
  <mat-grid-tile style="background: green">
    Tile 2
  </mat-grid-tile>
  <mat-grid-tile style="background: blue">
    Tile 3
  </mat-grid-tile>
  <mat-grid-tile style="background: yellow">
    Tile 4
  </mat-grid-tile>
</mat-grid-list>
```

![](assets/03-grid-list-basic.png)

# rowHeight 指定 高度

mat-grid-list 會依照 col 來 切割 cell 每個 cell 的寬度會平均 並且 默認情況下 cell 的 寬度等於高度

可以 通過 rowHeight 屬性 設定 高度

```html
<mat-grid-list cols="3" rowHeight="100px">
  ...
</mat-grid-list>
```

![](assets/04-grid-list-row-height.png)

此外 還可以添一個 比值 字符串 tile 會 依據 寬度去 自動計算

```html
<mat-grid-list cols="3" rowHeight="4:1">
  ...
</mat-grid-list>
```

# 指定 tile 橫跨 cell

使用 colspan/rowspan  可以指定 tile 水平/ 垂直 佔用 多少 cell

```html
<mat-grid-list cols="3">
  <mat-grid-tile style="background: red" colspan="2">
    Tile 1
  </mat-grid-tile>
  <mat-grid-tile style="background: green" rowspan="5">
    Tile 2
  </mat-grid-tile>
  <mat-grid-tile style="background: blue">
    Tile 3
  </mat-grid-tile>
  <mat-grid-tile style="background: yellow" colspan="2">
    Tile 4
  </mat-grid-tile>
</mat-grid-list>
```

![](assets/07-tile4-colspan.png)

```html
<mat-grid-list cols="3">
  <mat-grid-tile style="background: red" colspan="2">
    Tile 1
  </mat-grid-tile>
  <mat-grid-tile style="background: green" rowspan="5">
    Tile 2
  </mat-grid-tile>
  <mat-grid-tile style="background: blue" colspan="3">
    Tile 3
  </mat-grid-tile>
  <mat-grid-tile style="background: yellow" colspan="2">
    Tile 4
  </mat-grid-tile>
</mat-grid-list>
```

![](assets/08-bad-set-tile.png)

# cell 間距

使用 gutterSize 可以 調整 cell 間距 可以使用 px em rem 作爲單位

```html
<mat-grid-list cols="3" rowHeight="100px" gutterSize="20px">
```

# header/footer

使用 mat-grid-tile-header/mat-grid-tile-footer 可以爲 tile 加上 header/footer

```html
<mat-grid-tile style="background: green" rowspan="5">
  <mat-grid-tile-header>
    <h3 mat-line>功能清單</h3>
    <span mat-line>選擇你要的</span>
    <mat-icon>list</mat-icon>
  </mat-grid-tile-header>

  <mat-grid-tile-footer>
    <span mat-line>生是IT人</span>
    <span mat-line>死是IT魂</span>
    <span mat-line>但我不想死</span>
    <mat-icon>thumb_up</mat-icon>
  </mat-grid-tile-footer>
  Tile 2(右邊清單資訊)
</mat-grid-tile>
```

![](assets/12-mat-grid-tile-header-footer.png)
