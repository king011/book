# [config.listener.v3.Listener](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener.proto#envoy-v3-api-msg-config-listener-v3-listener)

```typescript
interface Listener {
	/**
	 * 監聽器的唯一名稱
	 * 
	 * @remarks
	 * 如果未提供則 envoy 自動創建一個 uuid作爲名稱。
	 * 如果要使用 {@link https://www.envoyproxy.io/docs/envoy/latest/configuration/listeners/lds#config-listeners-lds LDS} 更新或刪除監聽器則必須指定一個唯一的名稱
	 */
	name?: string
	/**
	 * 監聽地址，除非設置了 api_listener/listener_specifier 否則必須設置此值
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-address config.core.v3.Address}
	 */
	address?: config.core.v3.Address
	/**
	 * 偵聽器應偵聽的其他地址
	 * 
	 * @remarks
	 * 在單個偵聽器中使用多個地址時，所有地址都使用相同的協議，並且不支持多個內部地址
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener.proto#envoy-v3-api-msg-config-listener-v3-additionaladdress config.listener.v3.AdditionalAddress}
	 */
	additional_addresses?: Array<AdditionalAddress>
	/**
	 * 統計信息前綴
	 * 
	 * @remarks
	 * 如果為空，統計信息將以 listener.<address as string>.. 如果非空，統計信息將以 listener.<stat_prefix>..
	 */
	stat_prefix?: string
	/**
	 * 監聽器上的過濾鏈
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filterchain config.listener.v3.FilterChain}
	 */
	filter_chains?: Array<FilterChain>
	/**
	 * @alpha
	 * 從網路獲取過濾鏈名稱
	 */
	filter_chain_matcher?: any
	/**
	 * 如果使用 iptables 重定向連接，則代理接收它的端口可能與原始目標地址不同。
	 * 當此標誌設置為 true 時，偵聽器將重定向連接移交給與原始目標地址關聯的偵聽器。 
	 * 如果沒有與原始目標地址關聯的偵聽器，則連接由接收它的偵聽器處理
	 * 
	 * @defaultValue false
	 */
	use_original_dst?: boolean,
	/**
	 * 如果沒有匹配的過濾器鏈，則使用默認過濾器鏈。
	 * 如果沒有提供默認的過濾器鏈，連接將被關閉。 
	 * 過濾器鏈匹配在此字段中被忽略。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filterchain config.listener.v3.FilterChain}
	 */
	default_filter_chain?: FilterChain
	/**
	 * 對偵聽器的新連接讀寫緩衝區大小的軟限制
	 * 
	 * @defaultValue 1024*1024 = 1M
	 */
	per_connection_buffer_limit_bytes?: number
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-metadata config.core.v3.Metadata}
	 */
	metadata?: config.core.v3.Metadata
	/**
	 * 在偵聽器級別執行的耗盡類型
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener.proto#envoy-v3-api-enum-config-listener-v3-listener-draintype config.listener.v3.Listener.DrainType}
	 */
	drain_type?: 'DEFAULT' | 'MODIFY_ONLY'
	/**
	 * 監聽器過濾器，用於指定監聽器的類型
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-listenerfilter config.listener.v3.ListenerFilter}
	 */
	listener_filters?: Array<ListenerFilter>
	/**
	 * 等待所有偵聽器過濾器完成操作的超時時間
	 * 
	 * @defaultValue 5s
	 * 
	 * @remarks
	 * 如果達到超時，除非將 continue_on_listener_filters_timeout 設置為 true，否則將關閉接受的套接字而不創建連接。 
	 * 指定 0 以禁用超時。
	 * 如果未指定，則使用默認超時 15 秒。
	 * 
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	listener_filters_timeout?: string
	/**
	 * 當偵聽器過濾器超時時是否應創建連接
	 * 
	 * @defaultValue false
	 */
	continue_on_listener_filters_timeout?: boolean
	/**
	 * 是否應將偵聽器設置為透明套接字 TPROXY
	 */
	transparent?: boolean
	/**
	 * 是否啓用 IP_FREEBIND 選項
	 */
	freebind?: boolean
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/socket_option.proto#envoy-v3-api-msg-config-core-v3-socketoption config.core.v3.SocketOption}
	 */
	socket_options?: Array<config.core.v3.SocketOption>
	/**
	 * 偵聽器是否應接受 TCP 快速打開 (TFO) 連接。 當此標誌設置為大於 0 的值時，選項 TCP_FASTOPEN 在套接字上啟用，隊列長度為指定大小（請參閱 {@link https://tools.ietf.org/html/rfc7413#section-5.1 RFC7413} 中的詳細信息）
	 */
	tcp_fast_open_queue_length?: number
	/**
	 * 指定流量相對於本地 Envoy 的預期方向。 對於使用原始目標篩選器的偵聽器，此屬性在 Windows 上是必需的，請參閱{@link https://www.envoyproxy.io/docs/envoy/latest/configuration/listeners/listener_filters/original_dst_filter#config-listener-filters-original-dst 原始目標}
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-enum-config-core-v3-trafficdirection config.core.v3.TrafficDirection}
	 */
	traffic_direction?: 'UNSPECIFIED' | 'INBOUND' | 'OUTBOUND'
	/**
	 * 對於 udp 協議，指定udp設定
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/udp_listener_config.proto#envoy-v3-api-msg-config-listener-v3-udplistenerconfig config.listener.v3.UdpListenerConfig}
	 */
	udp_listener_config?: UdpListenerConfig
	/**
	 * 用於表示一個 API 偵聽器，用於非代理客戶端。
	 * @remarks
	 * 向非代理應用程序公開的 API 類型取決於 API 偵聽器的類型。 
	 * 設置此字段後，不應設置除名稱以外的其他字段。
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/api_listener.proto#envoy-v3-api-msg-config-listener-v3-apilistener config.listener.v3.ApiListener}
	 */
	api_listener?: ApiListener
	/**
	 * 偵聽器的連接平衡器配置，目前僅適用於 TCP 偵聽器。
	 * @remarks
	 * 如果未指定配置，Envoy 將不會嘗試平衡工作線程之間的活動連接
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener.proto#envoy-v3-api-msg-config-listener-v3-listener-connectionbalanceconfig config.listener.v3.Listener.ConnectionBalanceConfig}
	 */
	connection_balance_config?: config.listener.v3.Listener.ConnectionBalanceConfig
	/**
	 * 使用 {@link enable_reuse_port} 替代
	 */
	reuse_port?: boolean
	/**
	 * 當此標誌設置為 true 時，偵聽器設置 SO_REUSEPORT 套接字選項並為每個工作線程創建一個套接字。
	 *  這使得入站連接在存在大量連接的情況下大致均勻地分佈在工作線程中。 
	 * 當此標誌設置為 false 時，所有工作線程共享一個套接字
	 * 
	 * @defaultValue true
	 */
	enable_reuse_port?: boolean
	/**
	 * 爲監聽器配置日誌
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#envoy-v3-api-msg-config-accesslog-v3-accesslog config.accesslog.v3.AccessLog}
	 */
	access_log?: Array<config.accesslog.v3.AccessLog>
	/**
	 * uint32 tcp 偵聽器的掛起連接隊列可以增長到的最大長度。 
	 * 如果未提供任何值， 在 Linux 上使用 net.core.somaxconn，否則為 128。
	 */
	tcp_backlog_size?: number
	/**
	 * 偵聽器是否應綁定到端口。 未綁定的偵聽器只能接收從其他將 use_original_dst 設置為 true 的偵聽器重定向的連接
	 * 
	 * @defaultValue true
	 */
	bind_to_port?: boolean
	/**
	 * 用於表示不在 OSI L4 地址上偵聽但可由特使集群用來創建用戶空間連接的內部偵聽器。
	 * 內部偵聽器充當 TCP 偵聽器。 它支持偵聽器過濾器和網絡過濾器鏈。 
	 * 上游集群通過名稱引用內部偵聽器。 不得在內部偵聽器上設置地址。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener.proto#envoy-v3-api-msg-config-listener-v3-listener-internallistenerconfig config.listener.v3.Listener.InternalListenerConfig}
	 */
	internal_listener?: config.listener.v3.Listener.InternalListenerConfig
	/**
	 * 在此偵聽器上啟用 MPTCP（多路徑 TCP）。
	 * 客戶端將被允許建立 MPTCP 連接。 非 MPTCP 客戶端將回退到常規 TCP。
	 */
	nable_mptcp?: boolean
	/**
	 * 偵聽器是否應根據 global_downstream_max_connections 的值限制連接
	 */
	ignore_global_conn_limit?: boolean
}
```

## filter\_chains
(repeated [config.listener.v3.FilterChain](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filterchain)) 是過濾鏈，可以用於在一個端口上部署多個虛擬主機。

例如你有 a.example.com 與 b.example.com 要使用tls部署到同一個監聽器上，你可以創建兩個 FilterChain，使用 filter\_chain\_match.server\_names 來匹配用戶到達請求的 a.example.com/b.example.com 哪個虛擬主機，以便爲它們返回不同的 tls 證書


```typescript
interface FilterChain {
		/**
		 * 定義如何匹配此鏈
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filterchainmatch config.listener.v3.FilterChainMatch}
		 */
		filter_chain_match?: FilterChainMatch
		/**
		 * 過濾器
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filter config.listener.v3.Filter}
		 */
		filters?: Array<Filter>
		/**
		 * 偵聽器是否應在新連接上期望 PROXY 協議 V1 標頭
		 * 
		 * @deprecated 應該顯示增加一個 PROXY 協議的監聽器
		 */
		use_proxy_proto?: boolean
		/**
		 * 用於下游連接的可選自定義傳輸套接字實現，例如啓用 tls
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-transportsocket config.core.v3.TransportSocket}
		 */
		transport_socket?: config.core.v3.TransportSocket
		/**
		 * 如果存在且非零，則為允許傳入連接完成任何傳輸套接字協商的時間量。 
		 * 如果這在傳輸報告連接建立之前過期，連接將立即關閉。
		 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
		 */
		transport_socket_connect_timeout?: string
		/**
		 * 過濾器在監聽器中的唯一名稱
		 */
		name?: string
}
```

### filter\_chain\_match
([config.listener.v3.FilterChainMatch](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filterchainmatch)) 定義如何匹配此鏈

```typescript
interface FilterChainMatch {
	/**
	 * uint32 匹配目的端口
	 */
	destination_port?: number
	/**
	 * 如果非空，當偵聽器綁定到 0.0.0.0/:: 或指定 use_original_dst 時，IP 地址和前綴長度匹配地址
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-cidrrange config.core.v3.CidrRange}
	 */
	prefix_ranges?: Array<config.core.v3.CidrRange>
	/**
	 * 如果下游連接的直接連接的源 IP 地址包含在至少一個指定的子網中，則滿足條件。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-cidrrange config.core.v3.CidrRange}
	 */
	direct_source_prefix_ranges?: Array<config.core.v3.CidrRange>
	/**
	 * 指定連接源 IP 匹配類型。 可以是任何本地或外部網絡。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-enum-config-listener-v3-filterchainmatch-connectionsourcetype config.listener.v3.FilterChainMatch.ConnectionSourceType}
	 */
	source_type?: config.listener.v3.FilterChainMatch.ConnectionSourceType
	/**
	 * 如果下游連接的源 IP 地址至少包含在指定子網之一中，則滿足條件
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-cidrrange config.core.v3.CidrRange}
	 */
	source_prefix_ranges?: Array<config.core.v3.CidrRange>
	/**
	 * uint32 如果下游連接的源端口包含在至少一個指定端口中，則滿足條件
	 */
	source_ports?: Array<number>
	/**
	 * 如果非空，則在確定過濾器鏈匹配時要考慮的服務器名稱列表（例如 TLS 協議的 SNI）。 
	 * 
	 * @remarks
	 * 服務器名稱將與所有通配符域匹配，即 www.example.com 將首先與 www.example.com 匹配，然後是 *.example.com，然後是 *.com
	 */
	server_names?: Array<string>
	/**
	 * 如果非空，則在確定過濾器鏈匹配時要考慮的傳輸協議
	 */
	transport_protocol?: 'raw_buffer' | 'tls'
	/**
	 * 如果非空，則在確定過濾器鏈匹配時要考慮的應用程序協議列表（例如 TLS 協議的 ALPN）
	 * 
	 * - http/1.1 - set by envoy.filters.listener.tls_inspector,
	 * - h2 - set by envoy.filters.listener.tls_inspector
	 */
	application_protocols?: Array<string>
}
```

### filters
[repeated config.listener.v3.Filter](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filter) 過濾器

```
interface Filter {
	/**
	 * 選用的過濾器名稱
	 */
	name: string
	/**
	 * 過濾器特定配置取決於正在實例化的過濾器
	 */
	typed_config?: any
}
```

## default\_filter\_chain
([config.listener.v3.FilterChain](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filterchain))

所有沒有匹配到 filter\_chains 的請求都會調用 default\_filter\_chain(default\_filter\_chain.filter\_chain\_match 會被忽略) 進行處理

如果沒設置 default\_filter\_chain 則會直接關閉未匹配的請求


## [config.listener.v3.Filter](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-filter)


## listener\_filters

(repeated [config.listener.v3.ListenerFilter](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-listenerfilter))

```typescript
interface ListenerFilter {
	/**
	 * 選擇的過濾器名稱
	 */
	name: string
	/**
	 * 過濾器特定配置取決於正在實例化的過濾器
	 */
	typed_config?: any

	/**
	 * 擴展配置發現服務的配置源說明符。 如果發生故障且沒有默認配置，偵聽器將關閉連接
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-extensionconfigsource config.core.v3.ExtensionConfigSource}
	 */
	config_discovery?: config.core.v3.ExtensionConfigSource

	/**
	 * 用於禁用過濾器的可選匹配謂詞
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener_components.proto#envoy-v3-api-msg-config-listener-v3-listenerfilterchainmatchpredicate config.listener.v3.ListenerFilterChainMatchPredicate}
	 */
	filter_disabled?: config.listener.v3.ListenerFilterChainMatchPredicate
}
```