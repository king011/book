# 元表
在 lua 中 table 的符號操作是由元表定義的，元表也是一個 table，稱爲 metatable

lua 提供了兩個函數用於操作 metatable

* setmetatable(table,metatable) 爲一個 table 設置元表，metatable 傳入 nil 則刪除元表
* getmetatable(table) 返回 table 的元表或 nil

當對一個 table 進行操作時，lua會查詢是否存在 metatable，如果存在 metatable，則依據不同的操作類型查找 metatable 中是否存在指定函數如果存在則調用此函數，對於多個操作數的操作比如 **a+b** lua會先查找 a 中是否存在 metatable.\_\_add 函數存在則調用之，不存在再查找 b 中是否存在存在則調用之，都不存在則拋出錯誤

# 元方法

爲元表添加元方法可以使 table 獲得 運算符操作的能力

| 簽名 | 運算符 |
| -------- | -------- |
| __add(l,r)     | +     |
| __sub(l,r)      | -     |
| __mul(l,r)      | *     |
| __div(l,r)      | /     |
| __unm(v)      | 取相反數     |
| __mod(l,r)      | %     |
| __pow(l,r)      | ~     |
| __concat(l,r)      | ..     |
| __tostring(v)      | print 輸出字符串     |

```
local function point(x,y)
    -- 創建 點
    local t = {
        x=x,
        y=y
    }
    -- 設置元表
    setmetatable(t,{
        __add=function (l,r)
            return point(l.x+r.x,l.y+r.y)
        end,
        __unm=function (v)
            return point(-v.x,-v.y)
        end,
        __tostring=function (v)
            return string.format("point(%d,%d)",v.x,v.y)
        end
    })
    return t
end

local p0=point(1,2)
print(p0) -- point(1,2)
local p1=-p0
print(p1) -- point(-1,-2)
print(p0+p1) -- point(0,0)
```

# 關係元方法

關係元方法用以重新定義比較操作

| 簽名 | 運算符 |
| -------- | -------- |
| __eq(l,r)     | ==     |
| __lt(l,r)     | <     |
| __le(l,r)     | <=     |

* 其它比較操作由上述三種定義自動推導
* 比較操作需要兩個 table 有相同的元表 否則拋出錯誤
* == 和 ~= 是例外，當元表不同時直接認爲 不等

```
local __point
local function point(x,y)
    -- 創建 點
    local t = {
        x=x,
        y=y
    }
    -- 比較需要元素擁有同一個元表，所以定義了個模塊私有變量
    __point=__point or {
        __lt=function (l,r)
            return l.x<r.x and l.y<r.y
        end,
    }
    -- 設置元表
    setmetatable(t,__point)
    return t
end
local p0=point(1,2)
print(p0<point(2,3)) -- true
print(p0<point(1,2)) -- false
```

# 讀寫鍵
lua 提供了兩個元方法可以自定義如何讀取不存在的 key，以及創建新的 key

 簽名 | 描述 |
| -------- | -------- |
| __index(table,key)     | 當讀取不存在的 key 時，若存在此元方法調用否則返回 nil     |
| __newindex(table,key,value)     | 當創建新 key 時，若存在此方法調用，否則創建值爲 value 的 key     |

此外 lua 提供了兩個函數用於在 獲取key 或 設置key 時跳過 對 \_\_index \_\_newindex 元方法的查找與調用

* rawget(table,key)
* rawset(table,kye,value)

```
local function Cat()
    local t = {
    }
    -- 設置元表
    setmetatable(t,{
        __index=function (table,key)
            print('key not exists:'..key)
            return nil
        end,
        __newindex=function (table,key,value)
            print(string.format('cat[%s]=%s',key,value))
            -- 避免 __newindex 被無限遞歸
            rawset(table,key,value)
        end
    })
    return t
end
local cat=Cat()
print(cat.name) -- print, nil
cat.name='cat' -- print
cat.name='dog' -- not print
print(cat.name) -- not print, dog
```

# call
**\_\_call(table,args...)** 元方法用於 table 被當作函數調用時執行，如果不存在 \_\_call 的元方法則不可被調用

```
local function Cat()
    local t = {
    }
    setmetatable(t,{
        __call=function (table,food)
            print("I'm cat,i love "..food)
        end
    })
    return t
end
local cat=Cat()
cat('fish')
```