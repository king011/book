# Noto
Noto 是 google 開發的 開源(OFL) 字型  
Noto 既 No Tofu 的簡寫 意在以 消除 無法顯示的字元 爲目標

Noto Sans 屬於 無襯線黑體  
Noto Serif 屬於 襯線體

官網 [https://www.google.com/get/noto/](https://www.google.com/get/noto/)