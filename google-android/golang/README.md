# go 生成 aar 庫

1. 安裝好 go git jdk

1. 安裝好 android sdk ndk

1. 設置 環境變量 

	```
	#info=false
	export ANDROID_HOME=~/Android/Sdk
	```

1. 安裝 gomobile

	```sh
	#info=false
	# 下載 gomobile
	go get -u -v golang.org/x/mobile/cmd/gomobile

	# 初始化 gomobile
	gomobile init -ndk=$ANDROID_HOME/ndk-bundle
	```
	
1. 寫一個 golang 庫

	```
	#info="$GOPATH/utils/utils.go"
	package utils

	import (
		"crypto/rand"
		"encoding/hex"
	)

	func Get() string {
		b := make([]byte, 128)
		rand.Read(b)
		return hex.EncodeToString(b)
	}
	```
	
1. 生成庫

	```
	#info=false
	gomobile bind -o utils.aar -target=android utils
	```
	
# java 調用 aar

1. 創建一個 app/libaars 檔案夾

1. 將 utils.aar 檔案 複製到 app/libaars 下

1. 修改 app/build.gradle 添加 配置

	```
	#info="指定 aar 檔案夾位置"
	repositories {
			flatDir {
					dirs './libaars'
			}
	}
	```

	```
	#info="指定加載的 aar 檔案"
	dependencies {
			implementation fileTree(dir: 'libs', include: ['*.jar'])
			implementation (name:'utils', ext:'aar')
	}
	```
	
	```
	#info="完整配置"
	apply plugin: 'com.android.application'

	repositories {
			flatDir {
					dirs './libaars'
			}
	}

	android {
			compileSdkVersion 28
			defaultConfig {
					applicationId "com.king011.example"
					minSdkVersion 19
					targetSdkVersion 28
					versionCode 1
					versionName "1.0"
					testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
			}
			buildTypes {
					release {
							minifyEnabled false
							proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
					}
			}
	}

	dependencies {
			implementation fileTree(dir: 'libs', include: ['*.jar'])
			implementation 'com.android.support:appcompat-v7:28.0.0'
			implementation 'com.android.support.constraint:constraint-layout:1.1.3'
			testImplementation 'junit:junit:4.12'
			androidTestImplementation 'com.android.support.test:runner:1.0.2'
			androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'
			implementation (name:'utils', ext:'aar')
	}
	```
	
1. 在 java 中 import 並調用 代碼

	```
	package com.king011.example;

	import android.support.v7.app.AppCompatActivity;
	import android.os.Bundle;
	import android.view.View;
	import android.widget.TextView;
	import utils.Utils;

	public class MainActivity extends AppCompatActivity {
			static final private String TAG="MainActivity";
			@Override
			protected void onCreate(Bundle savedInstanceState) {
					super.onCreate(savedInstanceState);
					setContentView(R.layout.activity_main);
			}
			public void onClick(View view){
					TextView text = findViewById(R.id.textView);
					text.setText(Utils.get());
			}

	}
	```
