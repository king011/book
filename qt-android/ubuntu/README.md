# ubuntu 環境配置 qt5.11.1
[http://doc.qt.io/qt-5/androidgs.html](http://doc.qt.io/qt-5/androidgs.html)

1. 安裝 Android SDK Tools 	下載 (sdk-tools-linux-xxx.zip) 解壓 的 ANDROID_SDK_ROOT 檔案夾下

```
# 更新 sdk
$ANDROID_SDK_ROOT/tools/android update sdk

# 安裝 android-10 platforms
$ANDROID_SDK_ROOT/tools/bin/sdkmanager "platforms;android-10"
```

3. 安裝 Android NDK (qt5.11.1 recommended version 10e) 

5. 安裝 openjdk 

  ```sh
	#info=false
  sudo apt install openjdk-8-jdk
  ```
