# [Elevation](https://www.primefaces.org/primeflex/elevation)

box-shadow 用於指定元素的邊框陰影

| Class | Properties |
| -------- | -------- |
| shadow-none     | box-shadow: none;     |
| shadow-1     | box-shadow: 0px 3px 5px rgba(0, 0, 0, 0.02), 0px 0px 2px rgba(0, 0, 0, 0.05), 0px 1px 4px rgba(0, 0, 0, 0.08);     |
| shadow-2     | box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.03), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 2px 6px rgba(0, 0, 0, 0.12);     |
| shadow-3     | box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.08), 0px 3px 4px rgba(0, 0, 0, 0.1), 0px 1px 4px -1px rgba(0, 0, 0, 0.1);     |
| shadow-4     | box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.12), 0px 4px 5px rgba(0, 0, 0, 0.14), 0px 2px 4px -1px rgba(0, 0, 0, 0.2);     |
| shadow-5     | box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.1), 0px 4px 5px -2px rgba(0, 0, 0, 0.12), 0px 10px 15px -5px rgba(0, 0, 0, 0.2);     |
| shadow-6     | box-shadow: 0px 3px 5px rgba(0, 0, 0, 0.06), 0px 7px 9px rgba(0, 0, 0, 0.12), 0px 20px 25px -8px rgba(0, 0, 0, 0.18);     |
| shadow-7     | box-shadow: 0px 7px 30px rgba(0, 0, 0, 0.08), 0px 22px 30px 2px rgba(0, 0, 0, 0.15), 0px 8px 10px rgba(0, 0, 0, 0.15);     |
| shadow-8     | box-shadow: 0px 9px 46px 8px rgba(0, 0, 0, 0.12), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 11px 15px rgba(0, 0, 0, 0.2);     |

![](assets/shadow.png)


```
<div class="card">
    <div class="card-container blue-container">
        <div class="flex flex-wrap align-items-center justify-content-center">
            <div class="shadow-none m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-none</div>
            <div class="shadow-1 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-1</div>
            <div class="shadow-2 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-2</div>
        </div>
        <div class="flex flex-wrap align-items-center justify-content-center">
            <div class="shadow-3 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-3</div>
            <div class="shadow-4 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-4</div>
            <div class="shadow-5 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-5</div>
        </div>
        <div class="flex flex-wrap align-items-center justify-content-center">
            <div class="shadow-6 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-6</div>
            <div class="shadow-7 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-7</div>
            <div class="shadow-8 m-3 surface-card text-center p-3 border-round-sm h-6rem w-9rem flex align-items-center justify-content-center font-semibold">shadow-8</div>
        </div>
    </div>
</div>
```

## 響應式佈局

box-shadow 和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同

## Pseudo States
你可以爲僞狀態在 class shadow-XXX 名稱前添加同名的前綴例如 focus: hover: active: 來爲這些狀態指定邊框陰影

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <div class="shadow-2 hover:shadow-8 text-center border-round-sm h-6rem surface-overlay p-3 m-3 flex align-items-center justify-content-center font-bold">
            shadow-8 on hover
        </div>
    </div>
</div>
```

# [Opacity](https://www.primefaces.org/primeflex/opacity)

opacity 用於設定元素的不透明度

![](assets/opacity.png)

```
<div class="card">
    <div class="flex align-items-center justify-content-center card-container blue-container">
        <div class="opacity-10 flex align-items-center justify-content-center border-round bg-blue-500 text-white p-3 m-3 font-bold">opacity-10</div>
        <div class="opacity-40 flex align-items-center justify-content-center border-round bg-blue-500 text-white p-3 m-3 font-bold">opacity-40</div>
        <div class="opacity-70 flex align-items-center justify-content-center border-round bg-blue-500 text-white p-3 m-3 font-bold">opacity-70</div>
        <div class="opacity-100 flex align-items-center justify-content-center border-round bg-blue-500 text-white p-3 m-3 font-bold">opacity-100</div>
    </div>
</div>
```
