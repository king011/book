# angular

第一步是創建一個 button 標籤，data-clipboard-text 屬性記錄要複製的內容

```
<button #btnClipboard class="hide" data-clipboard-text="">copy</button>
```

之後在 ngAfterViewInit 中 創建實例，並且註冊上要處理的事件
```
import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import ClipboardJS from 'clipboard';

export class XXXComponent implements OnDestroy, AfterViewInit {
	private _clipboard?: ClipboardJS
	@ViewChild("btnClipboard")
	private _btnClipboard?: ElementRef
	ngAfterViewInit() {
		this._clipboard = new ClipboardJS(this._btnClipboard!.nativeElement).on('success', () => {
			console.log('success')
		}).on('error', (evt) => {
			console.error('Action:', evt.action)
			console.error('Trigger:', evt.trigger)
		});
	}
	ngOnDestroy() {
		this._clipboard?.destroy()
	}
	onClickCopy(s: string) {
		// 設置要拷貝的內容
		const btn = this._btnClipboard!.nativeElement
		btn.setAttribute("data-clipboard-text", s)
		// 觸發拷貝
		btn.click()
	}
}
```


