# [vertical-align](https://www.primefaces.org/primeflex/verticalalign)

vertical-align 設置 inline inline-block table-cell 如何在垂直方向對齊

| Class | Properties | 
| -------- | -------- |
| vertical-align-baseline     | vertical-align: baseline;     |
| vertical-align-top     | vertical-align: top;     |
| vertical-align-middle     | vertical-align: middle;     |
| vertical-align-bottom     | vertical-align: bottom;     |
| vertical-align-text-top     | vertical-align: text-top;     |
| vertical-align-text-bottom     | vertical-align: text-bottom;     |
| vertical-align-sub     | vertical-align: sub;     |
| vertical-align-super     | vertical-align: super;     |

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <div class="surface-overlay border-round border-1 shadow-1 p-5 py-0 m-3">
            <span class="vertical-align-baseline">baseline</span>
            <span class="vertical-align-bottom">top</span>
            <span class="vertical-align-middle">middle</span>
            <span class="vertical-align-top">bottom</span>
            <span class="vertical-align-text-top">text-top</span>
            <span class="vertical-align-text-bottom">text-bottom</span>
            <span class="vertical-align-sub">sub</span>
            <span class="vertical-align-super">super</span>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同