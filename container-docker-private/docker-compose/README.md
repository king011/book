# docker-compose

registry 沒有提供一個 ui 用於查詢 鏡像和 tags，並且其 http 配置也沒有 ngix 方便，故可以使用 docker-compose 將 ngix 和 registry 組合起來並且可以嵌入一段 js 代碼來提供查詢ui

```
#info="registry/docker-compose.yml"
version: '1'
services:
  main:
    image: registry:2
    restart: always
    volumes:
      - ./data:/var/lib/registry
  nginx:
    image: nginx:1.23
    restart: always
    ports:
      - 3443:80
    volumes:
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf:ro
      - ./nginx/snippets:/etc/nginx/snippets:ro
      - ./nginx/conf.d:/etc/nginx/conf.d:ro
      - ./www:/var/www:ro
```

[完整代碼](assets/registry.tar.gz)

# basic_password
**nginx/conf\.d/basic\_password** 檔案記錄了 訪問 registry 的 basic_password 你可以使用下述命令增加訪問用戶與密碼

```
echo -n 'username:' >> basic_password
openssl passwd -apr1 >> basic_password
```
