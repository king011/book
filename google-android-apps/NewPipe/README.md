# NewPipe

NewPipe 是第三方的開源(GPL 3) Youtube 客戶端，可以屏蔽 youtube 廣告播放。

但是 NewPipe 無需 google 框架和帳號，它們通過網頁解析播放地址，這意味着當 youtube 更新後 NewPipe 可能會暫時無法工作必須等待官方更新以兼容最新的 youtube

* 官網 [https://newpipe.net/](https://newpipe.net/)
* 源碼 [https://github.com/TeamNewPipe/NewPipe](https://github.com/TeamNewPipe/NewPipe)
* 安裝 [https://github.com/TeamNewPipe/NewPipe/releases](https://github.com/TeamNewPipe/NewPipe/releases)