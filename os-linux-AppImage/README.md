# AppImage

linux 各發現版附帶的庫完全不兼容，並且即使同個系列的發現版，本身版本號不同也可能因爲附帶庫不同而不兼容。AppImage 正式解決此問題的一個方案

* 官網 [https://appimage.org/](https://appimage.org/)
* 源碼 [https://github.com/AppImage/appimagekit](https://github.com/AppImage/appimagekit)

# 工作原理

首先需要將程序的所有資源打包到同個 AppImage 檔案中，之後在支持 AppImage 的系統中運行時，它會被以只讀的形式掛載都 /tmp 下的一個隨機檔案夾中以供系統運行

# glibc

需要注意的是 AppImage 不會打包 glibc 環境，並且這還牽扯到了 ld-linux.so.2，如果 glibc 或者 ld-linux.so.2 不兼容打包的程式依然無法正常運行

可以嘗試打包 ld-linux.so.2 和 glibc 並使用 ld-linux.so.2 來執行程式，本喵測試一些程式可以這樣打包成功但不是絕對的，例如本喵測試 flutter 這樣無法正常運行會返回 AOT 錯誤