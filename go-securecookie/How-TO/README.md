# How-TO

```
package main

import (
	"encoding/gob"
	"fmt"
	"github.com/gorilla/securecookie"
	"log"
)

type User struct {
	Name string
	Lv   int
}

func main() {
	// 必須的 32/64 字節 用來 使用 HMAC 爲 cookie 簽名 以防止 篡改
	var hashKey = securecookie.GenerateRandomKey(32) // 創建 32字節的 隨機key
	// 如果 爲 nil 則 不加密 數據 否則 依據 key 長度 進程 不同的 加密
	// 16字節 AES-128
	// 24 字節 AES-196
	// 32 字節 AES-256
	// Block keys should be 16 bytes (AES-128) or 32 bytes (AES-256) long.
	// Shorter keys may weaken the encryption used.
	var blockKey = securecookie.GenerateRandomKey(32)
	var s = securecookie.New(hashKey, blockKey)

	// 默認 使用 securecookie.GobEncoder 序列化
	// s.SetSerializer(securecookie.GobEncoder)
	// s.SetSerializer(securecookie.JSONEncoder)

	// 模擬 cookie 數據
	u := &User{
		Name: "king",
		Lv:   10,
	}
	// 官方說 必須使用 gob.Register 註冊 型別 但好像 不調用 也沒什麼影響
	// 不過 既然 官方 說了 還是 調用下
	gob.Register(&u)

	// 創建 cookie
	// 簽名 加密 base64.URLEncoding
	str, e := s.Encode("session", &u)
	if e != nil {
		log.Fatalln(e)
	}

	// 解開 cookie
	var ud User
	e = s.Decode("session", str, &ud)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(ud)
}
```

> Encode/Decode 應該是 goroutine 安全的 因爲 在源碼中 適合 都之訪問了 局部變量 沒有訪問 全局或成員變量
> 

# MaxLength
MaxLength 函數 設置 產生的 cookie 值 最大長度 默認 是 4096
```
func (s *SecureCookie) MaxLength(value int) *SecureCookie 
```

# MaxAge
MaxAge 函數 設置 cookie 最大過期時間 默認 86400 * 30 秒 == 720 小時 == 30 天
MinAge 函數 設置 cookie 最小過期時間 默認 0 秒

如果 過期 Decode 會返回 errTimestampExpired 錯誤

```
func (s *SecureCookie) MaxAge(value int) *SecureCookie
func (s *SecureCookie) MinAge(value int) *SecureCookie
```
> 傳入單位是 秒
> 

# securecookie.Error 接口

securecookie 返回的 所有 error 都實現了 securecookie.Error 接口

```
// Error is the interface of all errors returned by functions in this library.
type Error interface {
	error

	// IsUsage returns true for errors indicating the client code probably
	// uses this library incorrectly.  For example, the client may have
	// failed to provide a valid hash key, or may have failed to configure
	// the Serializer adequately for encoding value.
	IsUsage() bool

	// IsDecode returns true for errors indicating that a cookie could not
	// be decoded and validated.  Since cookies are usually untrusted
	// user-provided input, errors of this type should be expected.
	// Usually, the proper action is simply to reject the request.
	IsDecode() bool

	// IsInternal returns true for unexpected errors occurring in the
	// securecookie implementation.
	IsInternal() bool

	// Cause, if it returns a non-nil value, indicates that this error was
	// propagated from some underlying library.  If this method returns nil,
	// this error was raised directly by this library.
	//
	// Cause is provided principally for debugging/logging purposes; it is
	// rare that application logic should perform meaningfully different
	// logic based on Cause.  See, for example, the caveats described on
	// (MultiError).Cause().
	Cause() error
}
```
 