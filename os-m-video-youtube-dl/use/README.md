# 查詢視頻可用格式

通常可以使用 **-F** 或 **--list-formats** 指令來查詢下視頻所有可用格式

```
youtube-dl -F "$URL"
```

如果存在播放列表 youtube-dl 會查詢所有列表內容，可以使用 **--no-playlist** 參數設置不需要列表內容

```
youtube-dl -F --no-playlist  "$URL"
```

# 下載視頻

```
youtube-dl  "$URL"
```

通常 youtube-dl 會自動下載質量最高格式的視頻，但你可能需要特定格式可以使用 **-f** 參數指定， -f 參數支持多種指定方式，最簡單的用法是傳入 檔案後綴名(也可以傳入 -F 查詢出來的 format code 數值來指定具體哪個源)

```
youtube-dl --no-playlist -f mp4  "$URL"
```