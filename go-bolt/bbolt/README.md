# bbolt

```
go get go.etcd.io/bbolt/...
```

bbolt 是 bolt 的一個 fork 其 api 和用法與 bolt 一致，目的是提供一份 高可用 穩定 持續更新 bug 修復 性能增強 且 api 兼容 bolt 的底層存儲引擎

作爲 etcd 的底層存儲引擎 被廣泛用在 k8s 和 其它微服務架構中


* 源碼 [https://github.com/etcd-io/bbolt](https://github.com/etcd-io/bbolt)