# [CONFLICT](https://www.sqlite.org/lang_conflict.html)

ON CONFLICT 是 SQLite 的非標準擴展，可以出現在許多 SQL 命令中

ON CONFLICT 子句在 CREATE TABLE 之後，對於 INSERT 或 UPDATE 命令則替換爲 OR 例如 "INSERT OR IGNORE"，使用 OR 關鍵字是因爲這樣語法讀起來更自然

ON CONFLICT 子句適用於 UNIQUE，NOT NULL，CHECK，PRIMARY KEY 約束。不適用與 FOREIGN KEY 約束。有五種衝突解決算法可供選擇，默認爲 ABORT

* **ROLLBACK** 當約束衝突時，ROLLBACK 算法會中止當前 SQL 語句，並返回 SQLITE\_CONSTRAINT 錯誤並回滾當前事務。如果沒有事務處於獲得狀態(除了在每個命名上創建的隱含事務)，那麼 ROLLBACK 算法與 ABORT 相同
* **ABORT** 當約束衝突時，ABORT 算法會中止當前 SQL 語句，返回 SQLITE\_CONSTRAINT 錯誤，並撤銷當前 SQL 語句所做出的任何更改；但由同一事務先前的 SQL 語句引起的更改將保留並且事務保持活動狀態。這是默認行爲，也是 SQL 標準規定的行爲
* **FAIL** 當約束衝突時，FAIL 算法會中止當前 SQL 語句，返回 SQLITE\_CONSTRAINT 錯誤。但是 FAIL 解決方案不會取消先前對失敗的 SQL 語句的更改，也不會結束事務。例如 UPDATE 語句在嘗試更改的第 100 行遇到約束衝突，則前 99 行更改將保留，但它嘗試對第 100 行以及以後的行的更改永遠不會發生
* **IGNORE** 當約束衝突時，IGNORE 算法會跳過包含約束違規的一行，並繼續處理 SQL 語句的後續行，就好像沒有發生任何錯誤一樣。包含約束違規的行之前和之後的其它行將正常插入或更新。當使用 IGNORE 算法時，不會爲唯一性 NOT NULL 和 UNIQUE 約束返回錯誤，但是對於外鍵約束，IGNORE工作類似 ABORT
* **REPLACE** 當發生 UNIQUE 或 PRIMARY KEY 約束違規時，REPLACE 算法會在插入或更新行之前刪除導致約束違規的預先存在行，並且命令繼續正常執行。如果發生 NOT NULL 約束違規，REPLACE 算法將 NULL 值替換爲該列的默認值，或者如果沒有默認值則使用 ABORT 算法。如果發生 CHECK 約束或外鍵約束衝突，REPLACE將像 ABORT 一樣工作

	當 REPLACE 算法刪除行以滿足約束時，當且僅當啓用遞歸觸發器時，刪除觸發器才會觸發
	
	不會爲 REPLACE 算法刪除的行調用 update hook。REPLACE 也不增加更改計數器。本段中定義的異常行爲可能會在未來的版本中發生變化