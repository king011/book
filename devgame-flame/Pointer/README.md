# [指針事件](https://docs.flame-engine.org/1.16.0/flame/inputs/pointer_events.html)

本文檔描述了新的事件 API。 [手勢輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html)中描述了仍受支援的舊（傳統）方法

指標事件是 Flutter 廣義的「滑鼠移動」類型事件（適用於桌面或 Web）

如果您想與組件或遊戲中的滑鼠移動事件進行交互，可以使用 PointerMoveCallbacks mixin

For example:

```
class MyComponent extends PositionComponent with PointerMoveCallbacks {
  MyComponent() : super(size: Vector2(80, 60));

  @override
  void onPointerMove(PointerMoveEvent event) {
    // Do something in response to the mouse move (e.g. update coordinates)
  }
}
```

mixin 在您的元件中新增了兩個可重寫的方法：

* **onPointerMove**：當滑鼠在元件內移動時調用
* **onPointerMoveStop**：如果元件懸停且滑鼠離開，則呼叫一次

預設情況下，這些方法中的每一個都不執行任何操作，需要重寫它們才能執行任何功能

另外，元件必須實作 containsLocalPoint() 方法（已經在 PositionComponent 中實現，所以大多數時候你不需要在這裡做任何事情）——這個方法允許 Flame 知道事件是否發生在元件內

請注意，只有元件內發生的滑鼠事件才會被代理。 但是， onPointerMoveStop 將在第一次滑鼠移動離開元件時觸發一次，因此您可以在那裡處理任何退出條件

# [HoverCallbacks](https://docs.flame-engine.org/1.16.0/flame/inputs/pointer_events.html#hovercallbacks)

如果你想具體知道你的元件是否正在懸停，或者如果你想掛鉤懸停進入和存在事件，你可以使用一個更專用的 mixin，稱為 HoverCallbacks

For example:

```
class MyComponent extends PositionComponent with HoverCallbacks {

  MyComponent() : super(size: Vector2(80, 60));

  @override
  void update(double dt) {
    // use `isHovered` to know if the component is being hovered
  }

  @override
  void onHoverEnter() {
    // Do something in response to the mouse entering the component
  }

  @override
  void onHoverExit() {
    // Do something in response to the mouse leaving the component
  }
}
```

請注意，您仍然可以監聽「原始」onPointerMove 方法來取得附加功能，只需確保呼叫 super 版本以啟用 HoverCallbacks 行為即可