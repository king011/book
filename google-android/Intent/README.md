# Intent

android 程式 有 三種組件 組成 

[android.content.Intent](https://developer.android.com/reference/android/content/Intent) 提供了 這些組件 協同工作的 機制

* Activity 將Intent傳入  startActivity/startActivityForResult 來啓動一個Activity/使一個已經啓動的Activity去做新的事
* Service 將Intent傳入 startService 初始化一個Service/傳送新的指令給Service , 傳入 bindService 建立調用組件和Service間的 連接
* BroadcastReceiver 將Intent傳入 傳入 sendBroadcast/sendOrderedBroadcast 傳遞廣播

# 組件名

* 顯示 Intent 是指定了 組件名的 Intent 其只會被發送到 指定的 組件 通常都是app內的 組件協同
* 隱式 Intent 是沒有指定 組件名的 Intent android 會 根據 app 定義的 Intent過濾器 來找到合適的 組件 進行響應 通常 用於 與外部app中的 組件 協同

Intent 提供了多個 方法 設置 組件名
```
// 構造 並使用 cls 設置 組件名
// packageContext 是調用者的 上下文 信息
public Intent(Context packageContext, Class<?> cls);
public Intent (String action, 
                Uri uri, 
                Context packageContext, 
                Class<?> cls);

// 設置 組件名
public Intent setComponent (ComponentName component);

// 設置 class 並使用 cls 設置 組件名
public Intent setClass (Context packageContext, Class<?> cls);
public Intent setClassName (String packageName, String className);

// 返回設置的 組件名/null
public ComponentName getComponent ();
```

# Action

Action 指定了 組件啓動後 要完成的 任務 可以使用如下方法 設置

```
// 構造
public Intent (String action);
public Intent (String action, Uri uri);
public Intent (String action, 
                Uri uri, 
                Context packageContext, 
                Class<?> cls);

// 設置 Action
public Intent setAction (String action);

// 返回 Action字符串/null
public String getAction ();
```

android 定義了一些標準的 動作 以及其對於的 常量 位於 Intent 中 此外 也可以自定義Action

## 標準 Activity Action
| 常量 | 含義 | 
| -------- | -------- | 
| ACTION\_MAIN     | 作爲初始的 Activity 啓動 沒有 數據 輸入/輸出     | 
| ACTION\_VIEW     | 將數據顯示給用戶     | 
| ACTION\_ATTACH\_DATA     | 指示一些數據應該附屬與其它地方     | 
| ACTION\_EDIT     | 將數據顯示給用戶用於編輯     | 
| ACTION\_PICK     | 從數據中選擇一項並返回該項     | 
| ACTION\_CHOOSER     | 顯示 Activity 選擇器 允許用戶在繼續前景前 按需選擇     | 
| ACTION\_GET\_CONTENT     | 允許用戶選擇特定類型的數據並將其返回     | 
| ACTION\_DIAL     | 使用給定的數字撥打電話     | 
| ACTION\_CALL     | 使用給定的數據給某人撥打電話     | 
| ACTION\_SEND     | 向某人發送消息 接收者未指定     | 
| ACTION\_SENDTO     | 向某人發送消息 接收者已指定     | 
| ACTION\_ANSWER     | 接聽電話     | 
| ACTION\_INSERT     | 將項目插入指定容器     | 
| ACTION\_DELETE     | 從容器中刪除數據     | 
| ACTION\_RUN     | 無條件運行數據     | 
| ACTION\_SYNC     | 執行數據同步     | 
| ACTION\_PICK\_ACTIVITY     | 挑選給定 Intent 的 Activity 並返回選擇的類    | 
| ACTION\_SEARCH     | 執行查詢     | 
| ACTION\_WEB\_SEARCH     | 執行聯機查詢     | 
| ACTION\_FACTORY\_TEST     | 工廠測試的主入口點     | 

> 其對應的字符串值爲 android.intent.action. + 沒有前綴的常量名 (如 ACTION_EDIT 的字符串值爲 android.intent.action.EDIT)
> 

## 標準 廣播 Action

| 常量 | 含義 | 
| -------- | -------- |
| ACTION\_TIME\_TICK     | 每分鐘通知一次當前時間改變     | 
| ACTION\_TIME\_CHANGED     | 通知時間被修改     | 
| ACTION\_TIMEZONE\_CHANGED     | 通知時區被修改     | 
| ACTION\_BOOT\_COMPLETED     | 在系統啓動完成後 發出一次通知     | 
| ACTION\_PACKAGE\_ADDED     | 通知新程式包已經安裝到設備上     | 
| ACTION\_PACKAGE\_CHANGED     | 通知安裝的程式包已經被修改     | 
| ACTION\_PACKAGE\_REMOVED     | 通知從設備刪除程式包     | 
| ACTION\_PACKAGE\_RESTARTED     | 通知用戶重啓程序包 其所有進程都被關閉被     | 
| ACTION\_DATE\_CHANGED     | 通知用戶程式包中的數據改變     | 
| ACTION\_UID\_REMOVED     | 通知從系統中刪除用戶ID     | 
| ACTION\_BATTERY\_CHANGED     | 充電狀態 等級 其它一些 電池信息 改變     | 
| ACTION\_POWER\_CONNECTED     | 通知設備已經連接外置電源    | 
| ACTION\_POWER\_DISCONNECTED     | 通知設備已經移除電源     | 
| ACTION\_SHUTDOWN     | 通知設備已經關閉 | 


# Data

data 是作用與 Intent 上的數據的URI和數據的 MIME 類型 不同的Action有不同的 data

通常 MIME 可由 URI 推測 但 Intent 亦提供了 api 來 明確指定

```
// 構造
public Intent (String action, Uri uri);
public Intent (String action, 
                Uri uri, 
                Context packageContext, 
                Class<?> cls);

// 操作 data
public Intent setData (Uri data);
public Uri getData ();

// 操作 MIME
public Intent setType (String type);
public String getType ();

// 設置 data 和 MIME
public Intent setDataAndType (Uri data, String type);
```
## 撥號示例
```
#info="AndroidManifest.xml"
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.king011.example">

    <!--增加撥號權限-->
    <uses-permission android:name="android.permission.CALL_PHONE"/>

    <application
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
        <activity android:name=".MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>

</manifest>
```
```
public void onClick(View view){
		// 撥號示例
		Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:12345678"));
		startActivity(intent);
}
```

## 短信示例
```
#info="AndroidManifest.xml"
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.king011.example">

    <!--增加發短信權限-->
    <uses-permission android:name="android.permission.SEND_SMS"/>

    <application
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
        <activity android:name=".MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>

</manifest>
```

```
public void onClick(View view){
		// 發短信示例
		Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:12345678"));
		intent.putExtra("sms_body","cerberus is an idea");
		startActivity(intent);
}
```

# Category

Intent 還提供了 Category 作爲被執行動作的 附加信息 並提供了如下 常量

| 常量 | 含義 |
| -------- | -------- | 
| CATEGORY_DEFAULT     | 應該作爲執行數據的默認動作的選項     |
| CATEGORY_BROWSABLE     | 能夠安全的從瀏覽器中調用     |
| CATEGORY_TAB     | 需要作爲TabActivity的選項卡     |
| CATEGORY_ALTERNATIVE     | 作爲用戶正在查看數據的備用動作     |
| CATEGORY_SELECTED_ALTERNATIVE     | 作爲用戶正在選擇數據的備用動作     |
| CATEGORY_LAUNCHER     | 應該在頂層啓動器中顯示     |
| CATEGORY_INFO     | 需要提供其所在包信息     |
| CATEGORY_HOME     | 是 Home Activity     |
| CATEGORY_PREFERENCE     | 是一個偏好面板     |
| CATEGORY_TEST     | 用於測試     |
| CATEGORY_CAR_DOCK     | 設備插到 car dock 時 運行     |
| CATEGORY_DESK_DOCK     | 設備插到 desk dock 時 運行      |
| CATEGORY_LE_DESK_DOCK     | 設備插到 模擬(低端) dock 時 運行     |
| CATEGORY_HE_DESK_DOCK     | 設備插到 模擬(高端) dock 時 運行     |
| CATEGORY_CAR_MODE     | 可以用於汽車環境     |
| CATEGORY_APP_MARKET     | 允許用戶瀏覽和下載新應用     |

```
public Intent addCategory (String category);
public void removeCategory (String category);
public Set<String> getCategories ();
public boolean hasCategory (String category);
```

# Extras

Intent 提供了 一系列 put/get 函數 用來 設置/獲取 附加數據

```
public void onClick(View view){
		Intent intent = new Intent();

		// put
		intent.putExtra("name","kate beckinsale");
		intent.putExtra("lv",10);

		Bundle bundle = new Bundle();
		bundle.putString("who","king");
		intent.putExtras(bundle);

		// get
		Log.i(TAG,intent.getStringExtra("name") + " " + Integer.toString(intent.getIntExtra("lv",0)));
		bundle = intent.getExtras();
		Log.i(TAG,bundle.getString("who"));
}
```

# Flag

flag 用來 指示 如何 啓動一個 動作 並且在 啓動後 如何 對待它

| 常量 | 含義 |
| -------- | -------- |
| FLAG\_GRANT\_READ\_URI\_PERMISSION     | 對 Intent 有 讀取 權限     |
| FLAG\_GRANT\_WRITE\_URI\_PERMISSION     | 對 Intent 有 寫入 權限      |
| FLAG\_ACTIVITY\_CLEAR\_TOP     | 如果當前task中有啓動的 Activity 那麼把該 Activity 之前的 所有 Activity 關閉 並且將此Activity置前以避免創建Activity實例     |
| FLAG\_ACTIVITY\_NEW\_DOCUMENT     |      |
| FLAG\_ACTIVITY\_EXCLUDE\_FROM\_RECENTS     | 如果設置 新的 Activity 不會在最近啓動列表中     |
| FLAG\_ACTIVITY\_FORWARD\_RESULT     | 如果設置 並且誒這個 Intent 從一個存在的 Activity 啓動，則這個作爲答覆目標的Activity將會傳到新的Activity中，新的Activity可以調用setResult 將結果答覆給目標Activity    |
| FLAG\_ACTIVITY\_LAUNCHED\_FROM\_HISTORY     | 通常有系統設定 當通過home的歷史列表啓動時 系統會自動設定此值     |
| FLAG\_ACTIVITY\_NEW\_TASK     | 系統檢查已創建的Task中是否有需要啓動 Activity的Task，有則在Task上創建Activity，沒有則創建有該Activity屬性的Task並在此Task上創建Activity    |
| FLAG\_ACTIVITY\_MULTIPLE\_TASK     | 與FLAG_ACTIVITY_NEW_TASK一起使用時，創建新的Task來處理Intent     |
| FLAG\_ACTIVITY\_NO\_HISTORY     | Activity 不會保存到歷史Stack中，用戶一旦離開 這個 Activity 就自動關閉     |
| FLAG\_ACTIVITY\_NO\_USER\_ACTION     | 如果設置 作爲新啓動的 Activity 進入前臺時，將在Activity暫停前阻止從最前方的Activity回調onUserLeaveHint()     |

```
public Intent setFlags (int flags);
public Intent addFlags (int flags)
public void removeFlags (int flags);
public int getFlags ();
```

# Intent 過濾器

對於 隱式 Intent android 比對 Intent 的過濾器 來 找到 最合適的 組件 進行 處理

Intent 會從 三個 方面 進行 測試

* Action
* Data
* Category

過濾器 需要 在 AndroidManifest.xml 中的 intent-filter 標籤下 進行 配置

```
<intent-filter>
		<!--配置 Action 過濾器 -->
		<action android:name="android.intent.action.MAIN" />
		<!--配置 Data 過濾器-->
		<data android:mimeType="audio/mpeg" android:scheme="http"/>
		<!--配置 Category 過濾器-->
		<category android:name="android.intent.category.LAUNCHER" />
</intent-filter>
```

## Action 測試


1. 如果 過濾器中沒有指定 Action 則任何 Intent 都無法通過測試

1. 如果 過濾器 指定了 Action 而 Intent 沒有指定 Action 則 此 Intent 通過測試

1. 都指定了 Action 則 Intent 中定義的 Action 必須和過濾器中的 某個 Action 匹配 才能 通過測試

## Data 測試

data 測試 分爲 兩部分 MIME 和 Uri 兩者都允許使用 通配符指定過濾器

使用 android:mimeType 定義 MIME  

```
#info=false
<data android:mimeType="video/*"/>
<data android:mimeType="audio/mpeg"/>
```

Uri 分爲 四個 部分

```
#info=false
https://www.google.com/chrome/
```

* android:scheme (https)
* android:host (www.google.com)
* android:port (443)
* android:path (/chrome/)

```
#info=false
<data android:scheme="https" android:host="google.com" android:port="443" android:path="/chrome/">
```

| Intent Uri | Intent MIME | 過濾器 Uri | 過濾器 MIME | 是否通過測試 |
| -------- | -------- | -------- | -------- | -------- |
| 未指定     | 未指定     | 未指定     | 未指定     | 通過     |
| 指定     | 未指定     | 指定     | 未指定     | Uri匹配時通過     |
| 未指定     | 指定     | 未指定     | 指定     | 數據匹配時通過     |
| 指定     | 指定     | 指定     | 指定     | 數據和Uri都匹配時通過     |



## Category 測試

1. Intent 中的所有 Category 都必須在 過濾器中 存在定義 才通過 測試

1. 沒有定義 Category 的Intent 始終可以通過 Category 測試

 
**startActivity** 啓動 隱式 Intent 時會自動 爲其 添加上 **Intent.CATEGORY_DEFAULT** 故需要 配置 
```
#info=false
<category android:name="android.intent.category.DEFAULT" />
```

但是 對於設置了 **android.intent.action.MAIN** 和 **android.intent.category.LAUNCHER** 時

代表 Activity 作爲新任務 啓動並顯示到屏幕 上 此時 無需設置 **android.intent.category.DEFAULT**
```
<intent-filter>
		<action android:name="android.intent.action.MAIN" />
		
		<category android:name="android.intent.category.LAUNCHER" />
</intent-filter>
```
 
# BroadcastReceiver
 
 廣播時一種同時通多個對象的事件通知機制
 
 廣播 來源 可以是 系統事件 也可以時程式發出的
 
 從 BroadcastReceiver 派生 用來收聽 廣播 當收到廣播時 onReceive 會被回調
 
 * 使用 sendBroadcast 發起異步廣播 同時向所有接收者進行廣播 接收者 的順序時 隨機的  無法對廣播內容進行 修改/終止
 * 使用 sendOrderedBroadcast 發起 順序廣播 intent-filter中 android:priority 屬性高的 先收到 且在接收者 返回後 才向下一接收者 進行廣播

> 通常 廣播 不要設置 組件名 從而可向所有 有興趣的(此時依據 Intent 過濾器 尋找 接收者) 接收者 進行 廣播(當然 設置 了 組件名 就只有 此組件會收到 廣播)
> 

```
public class MainActivity extends AppCompatActivity {
    public void onClick(View view){
        Intent intent = new Intent(this,MyReceiver.class);

        sendBroadcast(intent);
    }
}
```

```
package com.king011.example;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    static final private String TAG = "MyReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG,"yes");
    }
}
```