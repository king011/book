# [log](https://deno.land/std@0.166.0/log/mod.ts)

log 庫提供了日誌功能可以輸出到控制檯或者檔案，是寫工具腳本不可或缺的助力


# 快速開始

log 的日誌分爲了 Logger 和 Handler，Logger 上設置要輸出的日誌等級，Handler 則負責日誌的具體寫入，這個設計原本沒什麼，但目前(0.166.0)默認 Handler 自己記錄了輸入日誌等級並且判斷是否輸出，導致 logger 的設置根本沒用，必須將 Logger 和 Handler 都設置到要輸出的日誌等級才能正確輸出


```
import * as log from "https://deno.land/std@0.166.0/log/mod.ts";
```

```
import * as log from "std/log/mod.ts";

const l = log.getLogger();
// 調整日誌輸出等級(默認爲 INFO)
l.level = log.LogLevels.DEBUG;

// 將 handlers 的輸出等級設置到和 logger 一致
l.handlers.forEach((h) => {
  h.level = l.level;
  h.levelName = l.levelName;
});

// 輸出各種等級的日誌
log.debug("this is debug log");
log.info("this is info log");
log.warning("this is warning log");
log.error("this is error log");
log.critical("this is critical log");
```