# OWASP ZAP

OWASP ZAP 是一個開源(Apache-2.0)的 網頁漏洞掃描工具

* 官網 [https://www.zaproxy.org/](https://www.zaproxy.org/)
* 源碼 [https://github.com/zaproxy/zaproxy](https://github.com/zaproxy/zaproxy)