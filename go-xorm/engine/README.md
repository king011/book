# 單個 Engine 引擎

單個 ORM 引擎使用 Engine 表示。一個 app 可以同時存在多個 Engine，一個 Engine 通常只對應一個數據庫，Engine 是 goroutine safe 的。使用 xorm.NewEngine 創建

```
import (
    _ "github.com/go-sql-driver/mysql"
    "xorm.io/xorm"
)

var engine *xorm.Engine

func main() {
    var err error
    engine, err = xorm.NewEngine("mysql", "root:123@/test?charset=utf8")
}
```

創建 Engine 後並沒有連接數據庫，Engine會在需要時自動管理連接，使用 Ping 或 PingContext 可以來測試數據庫是否連通

對於大量需要分區的應用，需要創建多個 Engine 分別使用

```
var err error
for i:=0;i<5;i++ {
    engines[i], err = xorm.NewEngine("sqlite3", fmt.Sprintf("./test%d.db", i))
}
```

通常 程式退出時會關閉 Engine 此外 也可以使用 Close 函數 手動關閉 Engine 

## 日誌

* engine.ShowSQL(true) 會在控制檯打印生成的 SQL 語句
* engine.Logger() 返回 ContextLogger 接口
* engine.SetLogger engine.SetLogLevel 設置自定義日誌 或 調整日誌等級(默認INFO)

```
// SQLLogger represents an interface to log SQL
type SQLLogger interface {
	BeforeSQL(context LogContext) // only invoked when IsShowSQL is true
	AfterSQL(context LogContext)  // only invoked when IsShowSQL is true
}

// ContextLogger represents a logger interface with context
type ContextLogger interface {
	SQLLogger

	Debugf(format string, v ...interface{})
	Errorf(format string, v ...interface{})
	Infof(format string, v ...interface{})
	Warnf(format string, v ...interface{})

	Level() LogLevel
	SetLevel(l LogLevel)

	ShowSQL(show ...bool)
	IsShowSQL() bool
}
```

## 連接池

Engine 提供了幾個函數來設置內部的連接池

* engine.SetMaxIdleConns(2) 連接池空閒數大小
* engine.SetMaxOpenConns(0) 設置到數據庫最大連接數 如果 <= 0 不限制 (如果 SetMaxIdleConns 大於 SetMaxOpenConns 會被自動縮小到匹配 SetMaxOpenConns)
* engine.SetConnMaxLifetime(0) 設置連接可重用時間長度超過此時間的連接將延遲關閉
# 讀寫分離 EngineGroup 引擎

* Engine 提供的 方法 EngineGroup 基本都提供了 
* EngineGroup 通常用於 讀寫分離 EngineGroup 自動將 寫入發送到 master 將 讀取 發送到 slave

```
package main

import (
	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm"
)

func checkEror(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	master, e := xorm.NewEngine("mysql", "root:123@/test?charset=utf8")
	checkEror(e)
	slave0, e := xorm.NewEngine("mysql", "root:123@tcp(slave-prod-0:3306)/test?charset=utf8")
	checkEror(e)
	slave1, e := xorm.NewEngine("mysql", "root:123@tcp(slave-prod-1:3306)/test?charset=utf8")
	checkEror(e)
	engines, e := xorm.NewEngineGroup(master, []*xorm.Engine{slave0, slave1})
	checkEror(e)

	engines.Master()
	engines.Slave()
}
```

如果 master 和 slave 是同一種驅動(通常情況下都是) xorm 提供了 下面的語法糖來簡便創建 EngineGroup

```
package main

import (
	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm"
)

func checkEror(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	engines, e := xorm.NewEngineGroup("mysql",
		[]string{"root:123@/test?charset=utf8", "root:123@tcp(slave-prod-0:3306)/test?charset=utf8", "root:123@tcp(slave-prod-1:3306)/test?charset=utf8"},
	)
	checkEror(e)

	engines.Master()
	engines.Slave()
}
```

* Master 返回主庫 Engine

	```
	func (eg *EngineGroup) Master() *Engine
	```

* Slave 依據負載策略返回一個 從庫 Engine

	```
	func (eg *EngineGroup) Slave() *Engine
	```

* Slaves 返回 從庫 切片

	```
	func (eg *EngineGroup) Slaves() []*Engine
	```

* SetPolicy 設置 EngineGroup 負載策略

	```
	func (eg *EngineGroup) SetPolicy(policy GroupPolicy) *EngineGroup
	```
	
# 負載策略

xorm 提供了多種負載策略 由 NewEngineGroup 的第三個參數傳入 默認爲 xorm.RoundRobinPolicy()

* xorm.RandomPolicy() 隨機訪問
* xorm.WeightRandomPolicy([]int{2, 3}) 隨機權重 slave0和slave1 權重分別爲 2 和 3
* xorm.RoundRobinPolicy() 輪詢訪問
* xorm.WeightRoundRobinPolicy([]int{2, 3}) 輪詢權重 slave0和slave1 權重分別爲 2 和 3
* xorm.LeastConnPolicy() 最小負載


要自定義策略 實現 GroupPolicy 接口即可

```
type GroupPolicy interface {
	Slave(*EngineGroup) *Engine
}
```
