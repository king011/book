# 交叉編譯

go 從版本1.3 開始支持 交叉編譯

设置环境变量 GOARCH GOOS 即可

| OS | ARCH | OS Version |
| -------- | -------- | -------- |
| linux     | 386/amd64/arm     | >= Linux 2.6     |
| darwin     | 386/amd64     | OS X (Snow Leopard + Lion)     |
| freebsd     | 386/amd64     | >= FreeBSD 7     |
| windows     | 386/amd64     | >= Windows 2000     |

當 go version >=  1.7 使用 `go tool dist list` 指令即可查看支持的平臺

```bash
go tool dist list 
```

# cgo

cgo 使用 g++ gcc 編譯 c/c++ 代碼 只需c/c++編譯器支持 跨平臺的交叉編譯即可使 cgo 程式交叉編譯

注意如果啓用 CGO\_ENABLED=1 則，通常編譯的程序會使用少量的動態庫，在大多數情況下沒有什麼問題，但如果要將程式拷貝到嵌入式linux中 建議顯示設置 CGO\_ENABLED=0 以使程式使用靜態的 c 庫。如果要使用 cgo 且想靜態連接可以嘗試傳入 -static 參數，但不同的 golang 版本可能還需要加入額外參數才會完全靜態鏈接庫。

在 linux 可以使用 **ldd path** 指令來查看程式動態鏈接的依賴項目


以 ubuntu 爲例

## windows

要編譯cgo程式到 windows 安裝 **mingw-w64** 即可

```
sudo apt install mingw-w64 -y
```

```
# 交叉編譯到 windows 64bit
#!/bin/bash
export GOOS=windows
export GOARCH=amd64
export CGO_ENABLED=1
export CC="x86_64-w64-mingw32-gcc-posix"
export CXX="x86_64-w64-mingw32-g++-posix"

go build -ldflags "-s -w -linkmode external -extldflags -static"   -o 1.exe
```

```
# 交叉編譯到 windows 32bit
#!/bin/bash
export GOOS=windows
export GOARCH=386
export CGO_ENABLED=1
export CC="i686-w64-mingw32-gcc-posix"
export CXX="i686-w64-mingw32-g++-posix"

go build -ldflags "-s -w -linkmode external -extldflags -static"   -o 1.exe
```

## darwin

[https://github.com/tpoechtrager/osxcross](https://github.com/tpoechtrager/osxcross) 爲 darwin 提供了交叉編譯環境

1. 安裝 [osxcross](https://tenbaht.github.io/sduino/developer/cross-compile-for-osx/) 依賴 

	```
	sudo apt install clang llvm-dev libxml2-dev uuid-dev libssl-dev bash patch make  tar xz-utils bzip2 gzip sed cpio libbz2-dev cmake
	```
	
1. 下載 [https://github.com/tpoechtrager/osxcross](https://github.com/tpoechtrager/osxcross) 源碼 到 ~/osxcross

	```
	git clone https://github.com/tpoechtrager/osxcross.git
	```
	
1. 下 MacOSX SDK 到 ~/osxcross/tarballs 目錄下

	[https://github.com/phracker/MacOSX-SDKs](https://github.com/phracker/MacOSX-SDKs) 項目提供了 打包的 SDK
	
	```
	export sdkname=MacOSX11.3.sdk.tar.xz && \
		curl -#Lo ~/osxcross/tarballs/$sdkname https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/$sdkname
	```
	
1. 編譯 osxcross

	```
	cd ~/osxcross && \
			sudo UNATTENDED=yes TARGET_DIR=/opt/osxcross ./build.sh
	```

安裝好 osxcross 後就可以交叉編譯到 darwin

```
# 交叉編譯到 darwin 64bit
#!/bin/bash
export GOOS=darwin
export GOARCH=amd64
export CGO_ENABLED=1
export CC="/opt/osxcross/bin/o64-clang"
export CXX="/opt/osxcross/bin/o64-clang++"

go build -ldflags "-s -w -linkmode external"   -o 1
```

```
# 交叉編譯到 darwin 32bit
#!/bin/bash
#!/bin/bash
export GOOS=darwin
export GOARCH=386
export CGO_ENABLED=1
export CC="/opt/osxcross/bin/o32-clang"
export CXX="/opt/osxcross/bin/o32-clang++"

go build -ldflags "-s -w -linkmode external"   -o 1
```

## arm

要編譯cgo程式到 arm 首先需要安裝 arm 交叉編譯環境

```
sudo apt install gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf
```

```
# 交叉編譯到 arm 64bit
#!/bin/bash
export GOOS=linux
export GOARCH=arm64
export CGO_ENABLED=1
export CC="arm-linux-gnueabihf-gcc"
export CXX="arm-linux-gnueabihf-g++"

go build -ldflags "-s -w -linkmode external -extldflags -static"   -o 1
```

```
# 交叉編譯到 arm 32bit
#!/bin/bash
export GOOS=linux
export GOARCH=arm
export CGO_ENABLED=1
export CC="arm-linux-gnueabihf-gcc"
export CXX="arm-linux-gnueabihf-g++"

go build -ldflags "-s -w -linkmode external -extldflags -static"   -o 1
```