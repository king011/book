# noVNC

noVNC 是開源的 HTML VNC Client，同時也可以作爲 js 庫使用。

noVNC 使用 html5 渲染畫面，使用 websocket 連接 vnc 服務器，因爲 vnc 服務器不支持 websocket 協議故，noVNC 提供了一個協議轉發適配器 websockify。

* 源碼 [https://github.com/novnc/noVNC](https://github.com/novnc/noVNC)