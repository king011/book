# 基礎動作類型

* [cc.Action](https://docs.cocos.com/creator/api/zh/classes/Action.html) 所有動作類型的基類
* [cc.FiniteTimeAction](https://docs.cocos.com/creator/api/zh/classes/FiniteTimeAction.html) 有限事件動作 此動作擁有時長 duration 屬性
* [cc.ActionInstant](https://docs.cocos.com/creator/api/zh/classes/ActionInstant.html) 即時動作 會立刻執行 派生自 [cc.FiniteTimeAction](https://docs.cocos.com/creator/api/zh/classes/FiniteTimeAction.html)
* [cc.ActionInterval](https://docs.cocos.com/creator/api/zh/classes/ActionInterval.html) 時間間隔動作 在已定事件內完成 派生自  [cc.FiniteTimeAction](https://docs.cocos.com/creator/api/zh/classes/FiniteTimeAction.html)
* [cc.ActionEase](https://docs.cocos.com/creator/api/zh/classes/ActionEase.html) 所有緩動作的基類 用於修飾 [cc.ActionInterval](https://docs.cocos.com/creator/api/zh/classes/ActionInterval.html)
* [cc.EaseRateAction](https://docs.cocos.com/creator/api/zh/classes/EaseRateAction.html) 用於速率屬性的緩動作基類
* [cc.EaseElastic](https://docs.cocos.com/creator/api/zh/classes/EaseElastic.html) 彈性緩動作基類
* [cc.EaseBounce](https://docs.cocos.com/creator/api/zh/classes/EaseBounce.html) 反彈緩動作基類

# 容器動作

* [cc.sequence](https://docs.cocos.com/creator/api/zh/modules/cc.html#sequence) 順序執行
* [cc.spawn](https://docs.cocos.com/creator/api/zh/modules/cc.html#spawn) 同步執行
* [cc.repeat](https://docs.cocos.com/creator/api/zh/modules/cc.html#repeat) 重複執行
* [cc.repeatForever](https://docs.cocos.com/creator/api/zh/modules/cc.html#repeatForever) 無限重複執行
* [cc.speed](https://docs.cocos.com/creator/api/zh/modules/cc.html#speed) 修改動作速率

# 即時動作

* [cc.show](https://docs.cocos.com/creator/api/zh/modules/cc.html#show) 立刻顯示
* [cc.hide](https://docs.cocos.com/creator/api/zh/modules/cc.html#hide) 立刻隱藏
* [cc.toggleVisibility](https://docs.cocos.com/creator/api/zh/modules/cc.html#toggleVisibility) 切換顯示狀態 
* [cc.removeSelf](https://docs.cocos.com/creator/api/zh/modules/cc.html#removeSelf) 從父節點移除自身 
* [cc.flipX](https://docs.cocos.com/creator/api/zh/modules/cc.html#flipX) x軸翻轉 
* [cc.flipY](https://docs.cocos.com/creator/api/zh/modules/cc.html#flipY) y軸翻轉
* [cc.place](https://docs.cocos.com/creator/api/zh/modules/cc.html#place) 放置在目標位置 
* [cc.callFunc](https://docs.cocos.com/creator/api/zh/modules/cc.html#callFunc) 執行回調函數 
* [cc.targetedAction](https://docs.cocos.com/creator/api/zh/modules/cc.html#targetedAction) 用已有動作和一個新的目標節點創建動作

# 時間間隔動作

* [cc.moveTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#moveTo) 移動到指定目標
* [cc.moveBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#moveBy) 移動指定距離
* [cc.rotateTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#rotateTo) 旋轉到目標角度
* [cc.rotateBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#rotateBy) 旋轉指定角度
* [cc.scaleTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#scaleTo) 縮放到指定倍數
* [cc.scaleBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#scaleBy) 縮放指定倍數
* [cc.skewTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#skewTo) 偏移到目標角度
* [cc.skewBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#skewBy) 偏移指定角度
* [cc.jumpBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#jumpBy) 以跳動方式移動指定距離
* [cc.jumpTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#jumpTo) 以跳動方式移動到目標位置
* [cc.follow](https://docs.cocos.com/creator/api/zh/modules/cc.html#follow) 追蹤目標節點位置
* [cc.bezierTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#bezierTo) 按貝賽爾曲線移動到目標位置
* [cc.bezierBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#bezierBy) 按貝賽爾曲線移動指定距離
* [cc.blink](https://docs.cocos.com/creator/api/zh/modules/cc.html#blink) 閃爍(基於透明度)
* [cc.fadeTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#fadeTo) 修改透明度到指定值
* [cc.fadeIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#fadeIn) 漸顯
* [cc.fadeOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#fadeOut) 漸隱
* [cc.tintTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#tintTo) 修改顏色到指定值
* [cc.tintBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#tintBy) 按指定增量修改顏色
* [cc.delayTime](https://docs.cocos.com/creator/api/zh/modules/cc.html#delayTime) 延遲指定的時間量
* [cc.reverseTime](https://docs.cocos.com/creator/api/zh/modules/cc.html#reverseTime) 反轉目標動作的時間軸
* [cc.cardinalSplineTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#cardinalSplineTo) 按基數樣條缺陷軌跡移動到目標位置
* [cc.cardinalSplineBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#cardinalSplineBy) 按基數樣條缺陷軌跡移動指定距離
* [cc.catmullRomTo](https://docs.cocos.com/creator/api/zh/modules/cc.html#catmullRomTo) 按 Catmull Rom 樣條曲線軌跡移動到目標位置
* [cc.catmullRomBy](https://docs.cocos.com/creator/api/zh/modules/cc.html#catmullRomBy) 按 Catmull Rom 樣條曲線軌跡移動指定距離

# 緩動作

* [cc.easeIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeIn)
* [cc.easeOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeOut)
* [cc.easeInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeInOut)
* [cc.easeExponentialIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeExponentialIn)
* [cc.easeExponentialOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeExponentialOut)
* [cc.easeExponentialInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeExponentialInOut)
* [cc.easeSineIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeSineIn)
* [cc.easeSineOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeSineOut)
* [cc.easeSineInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeSineInOut)
* [cc.easeElasticIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeElasticIn)
* [cc.easeElasticOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeElasticOut)
* [cc.easeElasticInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeElasticInOut)
* [cc.easeBounceIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBounceIn)
* [cc.easeBounceOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBounceOut)
* [cc.easeBounceInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBounceInOut)
* [cc.easeBackIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBackIn)
* [cc.easeBackOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBackOut)
* [cc.easeBackInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBackInOut)
* [cc.easeBezierAction](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeBezierAction)
* [cc.easeQuadraticActionIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuadraticActionIn)
* [cc.easeQuadraticActionOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuadraticActionOut)
* [cc.easeQuadraticActionInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuadraticActionInOut)
* [cc.easeQuarticActionIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuarticActionIn)
* [cc.easeQuarticActionOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuarticActionOut)
* [cc.easeQuarticActionInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuarticActionInOut)
* [cc.easeQuinticActionIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuinticActionIn)
* [cc.easeQuinticActionOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuinticActionOut)
* [cc.easeQuinticActionInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeQuinticActionInOut)
* [cc.easeCircleActionIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeCircleActionIn)
* [cc.easeCircleActionOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeCircleActionOut)
* [cc.easeCircleActionInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeCircleActionInOut)
* [cc.easeCubicActionIn](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeCubicActionIn)
* [cc.easeCubicActionOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeCubicActionOut)
* [cc.easeCubicActionInOut](https://docs.cocos.com/creator/api/zh/modules/cc.html#easeCubicActionInOut)
