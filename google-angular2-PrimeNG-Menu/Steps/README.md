# [Steps](https://primeng.org/steps/personal)

```
import {StepsModule} from 'primeng/steps';
import {MenuItem} from 'primeng/api';
```

步驟組件是嚮導工作流中的步驟指標

```
<p-steps [model]="items"></p-steps>
```

```
export class MenuDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {label: 'Step 1'},
            {label: 'Step 2'},
            {label: 'Step 3'}
        ];
    }
}
```

## Readonly

默認情況下 Steps 組件是只讀的，要使它可交互設置 **readonly** 屬性

```
<p-steps [model]="items" [readonly]="false"></p-steps>
```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| activeIndexChange     | index: Index of the active step item	     | 選擇新項目時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 步驟數組     |
| activeIndex     | number     | 0     | 當前活躍項目索引     |
| readonly     | boolean     | true     | ui 是否可交互     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |

# Styling

| Name | Element |
| -------- | -------- |
| p-steps     | Container element.     |
| p-steps-item     | Menuitem element.     |
| p-steps-number     | Number of menuitem.     |
| p-steps-title     | Label of menuitem.     |
