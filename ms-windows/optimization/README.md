# 關閉 診斷跟蹤

診斷跟蹤(DiagTrack) 是 win8.1開始增加的新功能，用於收集PC信息，以供微軟對其產品進行跟蹤與完善。但則會消耗系統資源，如果不是腦子有問題是微軟的擁護者建議關閉

1. win+r 輸入 `services.msc` 進入服務面板
2. 搜索4個以 **Diagnostic** 爲前綴顯示的服務名稱，全部關閉並禁用
	* Diagnostic Execution Service -> diagsvc
	* Diagnostic Policy Service -> DPS
	* Diagnostic Service Host -> WdiServiceHost
	* Diagnostic System Host -> WdiSystemHost
3. 另外 Connected User Experiences and Telemetry 也是DiagTrack的服務，記得關閉並禁用

	* Connected User Experiences and Telemetry -> DiagTrack

# 關閉 超級預讀

SysMain 服務在 Win10 1809 之前的名稱是 Superfetch，其目的是推測用戶常用的程序將它們預先加載到內存，所以當用戶打開這些程序時就可以快速開啓(已經預先加載到內存無需要讀取緩慢的磁盤)，但代價就是消耗額外的內存，並且微軟總是會猜錯了的使用習慣而白白浪費內存，但反過來如果微軟大部分都猜對你的習慣那麼你在微軟面前已經是裸體了更加可怕，故建議關閉這個服務

1. win+r 輸入 `services.msc` 進入服務面板
2. 找到 SysMain 服務關閉並禁用

# 關閉 Windows Search 服務

Windows Search 服務用於爲磁盤上的檔案建立索引，這樣當搜索時就能更快的得到搜索結果，然而這很消耗資源，且對於固態硬盤來說快不了多少。

並且通常你用不到這個索引，只有微軟內置的搜索功能會使用這個索引，你寫個bash腳本或第三方軟體根本不會用到這個索引，並且通常你不會搜索整個系統的檔案，而是用第三方程式搜索某個小返回目錄下的檔案，所以 Search 服務很雞肋，強烈建議關閉

1. win+r 輸入 `services.msc` 進入服務面板
2. 找到 Windows Search 服務關閉並禁用

# 關閉 Connected User Experiences and Telemetry 服務

這個服務是微軟用於診斷和收集用戶體驗數據的，如果不是腦子有問題是微軟的擁護者建議關閉

1. win+r 輸入 `services.msc` 進入服務面板
2. 找到 Connected User Experiences and Telemetry 服務關閉並禁用