# libev

libev 是一個 c 實現的 開源(GPL2) 高性能的 事件循環庫

* 官網 [http://software.schmorp.de/pkg/libev.html](http://software.schmorp.de/pkg/libev.html)
* 文檔 [http://pod.tst.eu/http://cvs.schmorp.de/libev/ev.pod](http://pod.tst.eu/http://cvs.schmorp.de/libev/ev.pod)

# Exmaple

```
// libev 對外提供的接口 都在 ev.h 中
#include <ev.h>

#include <stdio.h>

// 每種觀察者 都由自己的型別 使用 ev_TYPE 命名
ev_io stdin_watcher;
ev_timer timeout_watcher;

// 所有觀察者 使用相同的 回調簽名
// 本示例當 stdin 由數據可讀時 會被回調
static void
stdin_cb(EV_P_ ev_io *w, int revents)
{
    puts("stdin ready");
    // 對於一次性的 觀察者 必須手動 停止觀察程序
    ev_io_stop(EV_A_ w);

    // 使所有嵌套執行的 ev_run 都停止
    ev_break(EV_A_ EVBREAK_ALL);
}

// 另外一個回調，本示例是一個 超時回調
static void
timeout_cb(EV_P_ ev_timer *w, int revents)
{
    puts("timeout");
    // 使用最內層的 ev_run 停止
    ev_break(EV_A_ EVBREAK_ONE);
}

int main(void)
{
    // 除非由特殊需要否則應該使用 默認的事件循環
    struct ev_loop *loop = EV_DEFAULT;

    // 初始化 io 觀察者 之後啓動 她
    // this one will watch for stdin to become readable
    ev_io_init(&stdin_watcher, stdin_cb, /*STDIN_FILENO*/ 0, EV_READ);
    ev_io_start(loop, &stdin_watcher);

    // 啓動一個定時器 觀察者 之後啓動 她
    // 5.5秒後 超時 回調 不會重複
    ev_timer_init(&timeout_watcher, timeout_cb, 5.5, 0.);
    ev_timer_start(loop, &timeout_watcher);

    // 執行事件循環
    ev_run(loop, 0);

    // 所有事件結束 退出主程式
    return 0;
}
```