# [os/exec](https://pkg.go.dev/os/exec)

os/exec 對 os 中的 Process 提供了 封裝 提供了 更方便 通用 的 子進程 管理功能

# type Cmd

Cmd 是一個 子進程 全局的 Command 函數 可以 創建一個 Cmd

```txt
// 創建一個 子進程
func Command(name string, arg ...string) *Cmd

// 運行 進程
func (c *Cmd) Start() error

// 等待進程 並釋放資源 (如果進程返回 非0 將返回 error 否則 返回 nil)
func (c *Cmd) Wait() error

// Start and Wait 的語法糖
func (c *Cmd) Run() error

...
```

```go
package main
 
import (
	"log"
	"os"
	"os/exec"
)
 
func main() {
	//創建進程
	cmd := exec.Command("bash", "-c", "echo $PATH")
	//重定位 io 流
	cmd.Stdout = os.Stdout
 
	//運行 進程 並等待其結束
	e := cmd.Run()
	if e != nil {
		//無法啓動進程 或進程 返回非0
		log.Fatal(e)
	}
}
```

> Command 的 name 只能是 進程路徑 進程的每個 參數 需要 傳入到 arg 中
> 
> 調用了 Start 必須 調用 Wait 否則 可能會 泄漏資源
> 

# CommandContext
CommandContext 函數 類似 Command 不過 其 支持 context 可以 使用 context.WithCancel 創建返回一個 cancel 函數

使用 此 cancel 函數 可以 將 子進程 強制殺死

```go
package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

func main() {
	m := make(map[string]bool)
	for i := 0; i < len(os.Args); i++ {
		m[os.Args[i]] = true
	}
	if m["run"] {
		for {
			fmt.Println("run", time.Now())
			time.Sleep(time.Second)
		}
	} else {
		ctx, cancel := context.WithCancel(context.Background())
		exe := "./console"
		if runtime.GOOS == "windows" {
			exe += ".exe"
		}
		cmd := exec.CommandContext(ctx, exe, "run")
		cmd.Stdout = os.Stdout
		e := cmd.Start()
		if e != nil {
			log.Fatalln(e)
		}

		time.Sleep(10 * time.Second)
		fmt.Println("killing...", cmd.Process.Pid)
		cancel()

		cmd.Wait()
	}
}
```

> 如果 使用了 WithCancel 在正常 退出(沒有因爲 主動調用 cancel 退出) 後 應該在 cmd.Wait 之後 調用 cancel 以便 和 cancel 管理的 資源能夠被釋放
> 

cancel 只是 調用了 Process.Kill 直接使用 Cmd.Process.Kill 也能完成 同樣的 效果

```go
package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

func main() {
	m := make(map[string]bool)
	for i := 0; i < len(os.Args); i++ {
		m[os.Args[i]] = true
	}
	if m["run"] {
		for {
			fmt.Println("run", time.Now())
			time.Sleep(time.Second)
		}
	} else {
		exe := "./console"
		if runtime.GOOS == "windows" {
			exe += ".exe"
		}
		cmd := exec.Command(exe, "run")
		cmd.Stdout = os.Stdout
		e := cmd.Start()
		if e != nil {
			log.Fatalln(e)
		}

		time.Sleep(10 * time.Second)
		fmt.Println("killing ...", cmd.Process.Pid)
		cmd.Process.Kill()

		cmd.Wait()
	}
}
```

## 超時 WithTimeout WithDeadline
使用 WithDeadline 可以讓 進程在指定 時間 被kill

WithTimeout 則是 WithDeadline(parent, time.Now().Add(timeout) 的語法糖

```go
package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

func main() {
	m := make(map[string]bool)
	for i := 0; i < len(os.Args); i++ {
		m[os.Args[i]] = true
	}
	if m["run"] {
		for {
			fmt.Println("run", time.Now())
			time.Sleep(time.Second)
		}
	} else {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		// 釋放 cancel 資源
		defer cancel()
		
		exe := "./console"
		if runtime.GOOS == "windows" {
			exe += ".exe"
		}
		cmd := exec.CommandContext(ctx, exe, "run")
		cmd.Stdout = os.Stdout
		e := cmd.Start()
		if e != nil {
			log.Fatalln(e)
		}

		cmd.Wait()
	}
}
```

