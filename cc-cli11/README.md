# CLI11

CLI11 是一個 功能強大的 開源(BSD) c++ 命令行 解析庫(支持子命令) 只依賴 c++11

* 源碼 [https://github.com/CLIUtils/CLI11](https://github.com/CLIUtils/CLI11)
* api [https://cliutils.github.io/CLI11/index.html](https://cliutils.github.io/CLI11/index.html)

# Example

```
#include "CLI11.hpp"
#include <boost/lexical_cast.hpp>
void subcommand_calculator(CLI::App &app);
void subcommand_listen(CLI::App &app);
int main(int argc, char *argv[])
{
    // 定義 命令
    CLI::App app("console");

    // 設置 命令 處理 回調
    app.callback([&cmd = app] {
        // 獲取 參數
        const CLI::Option &v = *cmd.get_option("--version");
        if (v)
        {
            std::cout << "version 0.0.1" << std::endl;
        }
        else
        {
            // 打印 命令使用 說明
            // std::cerr << cmd.help() << std::endl;

            // 通知 CLI11_PARSE 異常
            // CallForHelp 被捕獲 會自動 調用 help()
            throw CLI::CallForHelp();
        }
    });
    // 允許輸入 額外參數
    app.allow_extras();

    // 爲命令 定義 參數
    app.add_flag("-v,--version", "display version");

    // 添加子命令
    subcommand_calculator(app);
    subcommand_listen(app);

    // 解析 命令 並 執行 同時 捕獲 CLI::ParseError 異常
    CLI11_PARSE(app, argc, argv);
    return 0;
}
void subcommand_calculator(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("calculator", "calculator for number");

    cmd->callback([&cmd = *cmd] {
        // 獲取 額外 參數
        std::vector<std::string> items = cmd.remaining();
        std::int64_t num = 0;
        for (const std::string &str : items)
        {
            try
            {
                num += boost::lexical_cast<std::int64_t>(str);
            }
            catch (const boost::bad_lexical_cast &e)
            {
                throw CLI::InvalidError(str);
            }
        }
        const CLI::Option &sub = *cmd.get_option("--sub");
        if (sub)
        {
            std::cout << -num << std::endl;
        }
        else
        {
            std::cout << num << std::endl;
        }
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });

    // 設置 參數
    cmd->add_flag("-s,--sub", "sub");
}
void subcommand_listen(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("listen", "tcp/udp server listen");

    cmd->callback([&cmd = *cmd] {
        const CLI::Option &udp = *cmd.get_option("--udp");
        const std::string addr = cmd.get_option("--addr")->as<std::string>();
        const std::uint32_t port = cmd.get_option("--port")->as<std::uint32_t>();
        if (udp)
        {
            std::cout << "udp work at " << addr << ":" << port << std::endl;
        }
        else
        {
            std::cout << "tcp work at " << addr << ":" << port << std::endl;
        }
        // 通知 CLI11_PARSE 成功
        throw CLI::Success();
    });
    // 設置 參數
    cmd->add_flag("--udp", "work as udp");
    std::string addr = "127.0.0.1";
    cmd->add_option("-a,--addr",
                    addr, // 默認 值
                    "listen addr",
                    true // true 使用 默認值
    );
    std::uint32_t port = 1911;
    cmd->add_option("-p,--port",
                    port,
                    "listen port",
                    true);
}
```

* 在 子命令 callback 中 需要 通過 throw CLI::ParseError 來 讓 CLI11_PARSE 捕獲異常 不然 會繼續執行 父命令 的 callback
* 需要 調用 allow_extras 運行 輸入額外 未定義的 參數 並且 使用 remaining() 來 返回 這些 未定義參數組成的 字符串 數組
* CLI 提供了 多個 CLI::ParseError 的子類來表示 錯誤 通常在 程式 出現意外時 應該 throw 響應的 錯誤 CLI11_PARSE 會合理的處理這些 異常

