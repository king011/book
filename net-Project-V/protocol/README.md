# 協議
v2ray 不是 cs 架構 沒有 v2ray 都是 一個 網路中的 相同 節點

v2ray 使用 入站協議 來 接收 傳入 數據

v2ray 使用 出站協議 來 傳出 數據

v2ray 提供了 多種的 協議來 完成 數據傳輸工作 將這些不同的協議 配置到 不同的 v2ray 中 就可以 組成 傳統的 代理 客戶端/服務器

```
#info="server.json"
{
  // 爲服務器 添加入站規則
  "inbounds": [
    {
      // 傳輸協議
      "protocol": "vmess",
      // socket 監聽 端口
      "port": 22582,
      "settings": {
        // 客戶端 定義
        "clients": [
          {
            "id": "af1879cc-1ca4-4f36-87b2-a75c2bf1684a",
            "level": 1,
            "alterId": 64
          }
        ]
      }
    }
  ],
  // 爲服務器 添加 出站 規則
  "outbounds": [
    {
      // freedom 直接 訪問 網路
      "protocol": "freedom",
      "settings": {}
    }
  ]
}
```

```
#info="client.json"
{
  // 爲客戶端 添加入站規則
  "inbounds": [
    {
      // socks 開啓一個 socks5 代理服務器
      "protocol": "socks",
      // socket 監聽端口
      "port": 1081,
      "settings": {
        // noauth socks5 不驗證 用戶名/密碼
        "auth": "noauth"
      }
    }
  ],
  // 爲客戶端 添加 出站 規則
  "outbounds": [
    {
      // 使用 vmess 連接 服務器
      "protocol": "vmess",
      "settings": {
        "vnext": [
          {
            // 服務器 域名
            "address": "127.0.0.1",
            // 服務器 端口
            "port": 22582,
            "users": [
              // 服務器 驗證 id/alterId 需要和 服務器一致
              {
                "id": "af1879cc-1ca4-4f36-87b2-a75c2bf1684a",
                "alterId": 64,
                "security": "auto",
                "level": 1
              }
            ]
          }
        ]
      }
    }
  ]
}
```

# inbounds 入站配置
inbounds 是 入站配置 接收 [InboundObject](https://www.v2ray.com/chapter_02/01_overview.html#inboundobject) 數組來定義 多個 入站 規則

```
#info="InboundObject"
{
  "port": 1080,
  "listen": "127.0.0.1",
  "protocol": "协议名称",
  "settings": {},
  "streamSettings": {},
  "tag": "标识",
  "sniffing": {
    "enabled": false,
    "destOverride": ["http", "tls"]
  },
  "allocate": {
    "strategy": "always",
    "refresh": 5,
    "concurrency": 3
  }
}
```

# outbounds 出站規則
outbounds 是 出站配置 接收 [OutboundObject](https://www.v2ray.com/chapter_02/01_overview.html#outboundobject) 數組來定義 多個 出站 規則

當存在 多個 出站規則時 第一個 作爲 主出站 規則 

如果 出站數據 沒有被任何路由匹配 則使用 主出站 規則 傳出數據  

```
#info="OutboundObject"
{
  "sendThrough": "0.0.0.0",
  "protocol": "协议名称",
  "settings": {},
  "tag": "标识",
  "streamSettings": {},
  "proxySettings": {
    "tag": "another-outbound-tag"
  },
  "mux": {}
}
```
