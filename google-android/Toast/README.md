# Toast

[android.widget.Toast](https://developer.android.com/reference/android/widget/Toast) 用來顯示一個 簡單的消息提示框 該框不會獲取焦點 並且一定時間後 自動消失

1. 創建一個 Toast 實例
2. 設置 Toast 顯示細節
3. 調用 show 顯示 Toast

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if(toast == null) {
            // 創建 Toast
            toast = Toast.makeText(this,"Cerberus is an idea",Toast.LENGTH_SHORT);


            // 設置 Toast
            toast.setGravity(Gravity.BOTTOM|Gravity.LEFT,0,0);
        }

        // 顯示 Toast
        toast.show();
    }
}
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if(toast == null) {
            // 創建 Toast
            toast = Toast.makeText(this,null,Toast.LENGTH_SHORT);


            // 設置 Toast
            toast.setGravity(Gravity.CENTER,0,0);

            toast.setView(View.inflate(this,R.layout.about,null));
        }

        // 顯示 Toast
        toast.show();
    }
}
```



| 方法 | 功能 |
| -------- | -------- |
| setDuration     | 設置消息框 持續時間     |
| setGravity     | 設置消息框 顯示位置     |
| setMargin     | 設置消息框 頁邊距     |
| setText     | 設置消息框 顯示文本     |
| setView     | 設置消息框 顯示視圖     |

setText 和 setView 只有一個 生效 同時設置 只有 setView 會生效

# AlertDialog

[android.app.AlertDialog](https://developer.android.com/reference/android/app/AlertDialog) 提供了 常用的模式對話框 使用方式 類似  Toast

AlertDialog 的 構造函數 被聲明爲 **protected** 需要通過 AlertDialog.Builder 來創建

```
package com.king011.example;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if (alertDialog ==null) {
            // 創建 AlertDialog
            alertDialog = new AlertDialog.Builder(MainActivity.this).create();

            // 設置 基本 信息
            alertDialog.setTitle("測試");
            alertDialog.setMessage("Cerberus is an idea");
            // alertDialog.setView(View.inflate(MainActivity.this,R.layout.about,null));

            // 增加按鈕
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"取消");
                }
            });
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "確認", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"確認");
                }
            });
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "中立", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"中立");
                }
            });
        }

        alertDialog.show();
    }
}
```

| 方法 | 功能 |
| -------- | -------- |
| setTitle     | 設置 標題     |
| setMessage     | 設置 顯示文本     |
| setIcon     | 設置 圖標     |
| setButton     | 設置 可選的 按鈕 DialogInterface.BUTTON_POSITIVE(確定) DialogInterface.BUTTON_NEGATIVE(取消) DialogInterface.BUTTON_NEUTRAL(中立)     |

# AlertDialog.Builder

[android.app.AlertDialog.Builder](https://developer.android.com/reference/android/app/AlertDialog.Builder) 提供了 AlertDialog 的全部功能 並且定製化了一些常用的 模式 對話框 



| 方法 | 功能 |
| -------- | -------- |
| setTitle     | 設置 標題     |
| setMessage     | 設置 顯示文本     |
| setIcon     | 設置 圖標     |
| setPositiveButton     | 設置 確定按鈕     |
| setNegativeButton     | 設置 取消按鈕     |
| setNeutralButton     | 設置 中立按鈕     |
| setItems     | 爲對話框 添加列表     |
| setSingleChoiceItems     | 爲對話框 添加單選列表     |
| setMultiChoiceItems     | 爲對話框 添加複選列表     |

```
package com.king011.example;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if (builder ==null) {
            // 創建 AlertDialog.Builder
            builder = new AlertDialog.Builder(MainActivity.this);

            // 設置 基本 信息
            builder.setTitle("測試");
            builder.setMessage("Cerberus is an idea");
            //builder.setView(View.inflate(MainActivity.this,R.layout.about,null));

            // 增加按鈕
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"取消");
                }
            });
            builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"確認");
                }
            });
            builder.setNeutralButton("中立", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"中立");
                }
            });
        }

        builder.show();
    }
}
```

```
#info="列表示例"
package com.king011.example;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if (builder ==null) {
            // 創建 AlertDialog.Builder
            builder = new AlertDialog.Builder(MainActivity.this);

            // 設置 基本 信息
            builder.setTitle("測試");
            final String[] items = new String[]{"kate","anita","anna","jolin"};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"you love " + items[which]);
                }
            });

            // 增加按鈕
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"取消");
                }
            });
            builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"確認");
                }
            });
            builder.setNeutralButton("中立", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"中立");
                }
            });
        }

        builder.show();
    }
}
```

```
#info="單選列表示例"
package com.king011.example;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if (builder ==null) {
            // 創建 AlertDialog.Builder
            builder = new AlertDialog.Builder(MainActivity.this);

            // 設置 基本 信息
            builder.setTitle("測試");
            final String[] items = new String[]{"kate","anita","anna","jolin"};
            builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"you love " + items[which]);
                }
            });
            
            // 增加按鈕
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"取消");
                }
            });
            builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"確認");
                }
            });
            builder.setNeutralButton("中立", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"中立");
                }
            });
        }

        builder.show();
    }
}
```

```
#info="複選列表示例"
package com.king011.example;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        if (builder ==null) {
            // 創建 AlertDialog.Builder
            builder = new AlertDialog.Builder(MainActivity.this);

            // 設置 基本 信息
            builder.setTitle("測試");
            final String[] items = new String[]{"kate","anita","anna","jolin"};
            final boolean []checked = new boolean[]{false,true,false,false};
            builder.setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    if(isChecked){
                        Log.i(TAG,"you love " + items[which]);
                    }
                }
            });

            // 增加按鈕
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"取消");
                }
            });
            builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"確認");
                    for (int i=0;i<checked.length;i++){
                        if(checked[i]){
                            Log.i(TAG,"you love " + items[i]);
                        }
                    }
                }
            });
            builder.setNeutralButton("中立", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG,"中立");
                }
            });
        }

        builder.show();
    }
}
```