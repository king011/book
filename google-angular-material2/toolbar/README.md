# MatToolbarModule

MatToolbar 通常作為 頁面的 header 或 footer

## Example

```typescript
import {MatToolbarModule} from '@angular/material/toolbar';
```

```html
<mat-toolbar>
  <span>Angular Material Demo</span>
  <span style="flex: 1 1 auto;"></span>
  <span>right</span>
</mat-toolbar>
```

## top

如果 需要 mat-toolbar 始終 顯示在 頁面上 需要 自己定義 css

```css
.header{
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 40;
}
```

```html
<mat-toolbar class="header">
  <span>Angular Material Demo</span>
  <span style="flex: 1 1 auto;"></span>
  <span>right</span>
</mat-toolbar>
```

# 多行

mat-toolbar 默認 只在一行 顯示 元件 使用 mat-toolbar-row 可以在 多行顯示

```html
<mat-toolbar color="primary">
  <mat-toolbar-row>
    <span>第一行Toolbar</span>
    <span class="toolbar-seprator"></span>
    <mat-icon>favorite</mat-icon>
  </mat-toolbar-row>
  <mat-toolbar-row>
    第二行Toolbar
    <span class="toolbar-seprator"></span>
    <mat-icon>delete</mat-icon>
  </mat-toolbar-row>
</mat-toolbar>
```

