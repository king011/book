# Scheduler

Scheduler 控制一個 observable 的訂閱什麼時候開始 以及元素什麼時候送達

* queue
* asap
* async
* animationFrame

rxjs 的 observable 都設置了適合的默認 Scheduler

# queue

queue 很適合在 會有回調的 operator 且大量數據時 可以避免 無線回調

# asap

asap  在 瀏覽器中是 setTimeout(0) 

nodejs 是 process.nextTick()

# async 
async 使用 setInterval  運作

# animationFrame

animationFrame 利用瀏覽器的 Window.requestAnimationFrame  運作