# [OverlayPanel](https://primeng.org/overlaypanel)

```
import {OverlayPanelModule} from 'primeng/overlaypanel';
```

OverlayPanel 是一個容器組件，定位爲 connected 到 target

toggle 函數用於切換顯示狀態

```
<p-overlayPanel #op>
    <ng-template pTemplate>
        Content
    </ng-template>
</p-overlayPanel>

<button type="text" pButton label="Basic" (click)="op.toggle($event)"></button>
```

## show hide

show 用於顯示，它第一個參數是必須的 $event，第二個參數是可選的用來指定它與誰對齊，下面的例子將與 div 標籤對齊。(hide 與 show 類似但它用於隱藏)

```
<p-overlayPanel #op>
    <ng-template pTemplate>
        Content
    </ng-template>
</p-overlayPanel>

<button type="text" pButton label="Custom Target" (click)="op.show($event, actualTarget)"></button>
<div #actualTarget></div>
```

## dismissable showCloseIcon

* dismissable 默認爲 true，表示單擊 OverlayPanel 之外自動關閉 OverlayPanel
* showCloseIcon 默認爲 true，表示在 OverlayPanel 右上角添加一個關閉圖標按鈕

```
<p-overlayPanel #op [dismissable]="true" [showCloseIcon]="true">
    <ng-template pTemplate>
        Content
    </ng-template>
</p-overlayPanel>
```

## Animation Configuration

**showTransitionOptions/hideTransitionOptions** 屬性用於指定 顯示/隱藏 的過渡動畫，下面例子將完全禁用過渡動畫

```
<p-overlayPanel [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'" #op [dismissable]="true" [showCloseIcon]="true">
    <ng-template pTemplate>
        Content
    </ng-template>
</p-overlayPanel>
```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onShow     | -     | 顯示時回調     |
| onHide     | -     | 隱藏時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| dismissable     | boolean     | true     | 是否在單擊外部時隱藏     |
| showCloseIcon     | boolean     | false     | 啓用後，在右上角顯示一個關閉圖標     |
| ariaCloseLabel     | string     | close     | 關閉圖標的 aria label     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| appendTo     | any     | null     | Target element to attach the panel, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| baseZIndex     | number     | 0     | 分層的基礎 zIndex 值     |
| autoZIndex     | boolean     | true     | 自動管理分層     |
| showTransitionOptions     | string     | 0.12s cubic-bezier(0, 0, 0.2, 1)     | 顯示的過渡動畫     |
| hideTransitionOptions     | string     | 0.1s linear     | 隱藏的過渡動畫     |
| focusOnShow     | boolean     | true     | 第一個按鈕自動獲取焦點     |

# Methods

| Name | Parameters | Description |
| -------- | -------- | -------- |
| toggle     | event: browser event
target?: target element to align the panel, defaults to event.target     | 切換顯示狀態     |
| show     | event: browser event
target?: target element to align the panel to     | 顯示 panel     |
| hide     | -     | 隱藏 panel     |

# Templates

| Name | Parameters |
| -------- | -------- |
| content     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-overlaypanel     | Container element.     |
| p-overlaypanel-content     | Content of the panel.     |
| p-overlaypanel-close     | Close icon.     |


