# MatInputModule

MatInputModule 在 input/textarea 之上 提供了 material 風格的 輸入框 通常會和 FormField 一起使用

```
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
```

```
<div>
  <mat-form-field>
    <input type="text" name="nickname" matInput placeholder="暱稱" />
  </mat-form-field>
</div>
<div>
  <mat-form-field>
    <textarea name="intro_self" matInput placeholder="自我介紹"></textarea>
  </mat-form-field>
</div>
```

![](assets/01-mat-input-basic.gif)

**placeholder** 在此 有着 label 的作用

# input type

matInput 支持 多種 input type

* date
* datetime-local
* email
* month
* number
* password
* search
* tel
* text
* time
* url
* week

比如使用 如下代碼 生成一個 日期 文字輸入框

```html
<mat-form-field>
  <input type="date" name="birthday" matInput placeholder="生日" />
</mat-form-field>
```

![](assets/02-mat-input-type-date.png)

不同瀏覽器 對於 date 有不同效果 有些甚至不支持 

Material 中 有提供一個 強大的 Datepicker 元件 通常應該使用 此元件

# mat-hint 提示 說明

如果 placeholder 無法 說明欄位意義 可以 使用 mat-hint 加上 說明

```html
<mat-form-field>
  <textarea name="intro_self" matInput placeholder="自我介紹"></textarea>
  <mat-hint>簡單介紹一下你的興趣吧！</mat-hint>
</mat-form-field>
```

![](assets/03-mat-hint.png)

# mat-error 錯誤提示

當欄位數據有問題時 可以使用 mat-error 進行提示

```html
<mat-form-field>
  <textarea name="intro_self" matInput placeholder="自我介紹" required></textarea>
  <mat-hint>簡單介紹一下你的興趣吧！</mat-hint>
  <mat-error>請記得輸入自我介紹喔！</mat-error>
</mat-form-field>
```

![](assets/04-mat-error.gif)

## 控制 不過錯誤

只要同個 mat-form-field 裏的欄位有 錯誤 就會顯示 錯誤信息

如果 需要依據不同 錯誤 顯示不同 錯誤 提示 可以使用 **ngIf/ngSwitch** 來區分錯誤

```html
<mat-form-field>
  <textarea name="intro_self" matInput placeholder="自我介紹" formControlName="intro" required></textarea>
  <mat-hint>簡單介紹一下你的興趣吧！</mat-hint>
  <mat-error *ngIf="surveyForm.get('basicQuestions').get('intro').hasError('required')">請記得輸入自我介紹喔！</mat-error>
  <mat-error *ngIf="surveyForm.get('basicQuestions').get('intro').hasError('minlength')">至少輸入10個字吧！</mat-error>
</mat-form-field>
```

![](assets/05-mat-error-with-ngif.gif)


## 自己控制錯誤顯示 時機

默認情況下 錯誤顯示必須符合 **dirty** **touched** 和 **invalid** 狀態 才會顯示

在第一次 輸入時 不處於 **touched** 所以即時 有錯 也要等到離開 欄位 才會標記 **touched** 後顯示 錯誤

如果 希望自己決定 錯誤顯示 時機 可以 implements **ErrorStateMatcher** 同時爲 matInput 指定 自定義的 macher 即可

```
#info="component.ts"
// 調整時機為invalid + dirty即顯示錯誤訊息
export class EarlyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && control.dirty);
  }
}
export class SurveyComponent {
  surveyForm: FormGroup;
  earlyErrorStateMacher = new EarlyErrorStateMatcher();
}
```

爲 matInput 指定 errorStateMatcher

```html
#info="*.html"
<mat-form-field>
  <textarea name="intro_self" matInput placeholder="自我介紹" formControlName="intro" required [errorStateMatcher]="earlyErrorStateMacher"></textarea>
  <mat-hint>簡單介紹一下你的興趣吧！</mat-hint>
  <mat-error *ngIf="surveyForm.get('basicQuestions').get('intro').hasError('required')">請記得輸入自我介紹喔！</mat-error>
  <mat-error *ngIf="surveyForm.get('basicQuestions').get('intro').hasError('minlength')">至少輸入10個字吧！</mat-error>
</mat-form-field>
```

![](assets/06-custom-error-state-macher.gif)


如果 要在 全域範圍使用這個規則 可以在 providers 中 注入此macher

```ts
providers: [
  {provide: ErrorStateMatcher, useClass: EarlyErrorStateMatcher}
]
```

# matTextareaAutosize 自動調整大小的 textarea

設置 **matTextareaAutosize** 即可使 textarea 自動調整大小

```
<textarea name="intro_self" matInput placeholder="自我介紹" formControlName="intro" required matTextareaAutosize></textarea>
```

![](assets/07-autosize-textarea.gif)
