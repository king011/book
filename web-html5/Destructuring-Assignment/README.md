# 解構賦值

es6 支持了解構賦值它是一種語法糖，可以把 Array 或 Object 解開爲獨立的變量，

解構代碼最好都寫上 **;** ，否則可能導致語法解析錯誤，爲了語法一致故可以選擇始終書寫 **;**

# Array

```
// 定義三個變 接收數組前三個元素
let [a, b, c] = [1, 2, 3, 4, 5, 6];
console.log(a, b, c); // 1 2 3

// 多餘的使用 undefined 賦值，tsc 不允許直接手寫明確的不匹配解構
[a, b, c] = [7, 8] as any;
console.log(a, b, c); // 7 8 undefined

// 爲 undefined 設置默認值
[a, b, c = 3] = [1, 2] as any
console.log(a, b, c) // 1 2 3
// null 不會被設置爲默認值
const [a1 = 1, b1 = 2, c1 = 3] = [null, undefined];
console.log(a1, b1, c1); // null 2 3

// 交互值
[a, b] = [b, a];
console.log(a, b); // 2 1

// 忽略某些值
[a, , b] = [1, 2, 3];
console.log(a, b); // 1 3

// 將其它值設置到數組
const arrs0 = [0];
let arrs1 = arrs0;
[a, , b, ...arrs1] = [1, 2, 3, 4, 5];
console.log(arrs0 != arrs1) // true
console.log(arrs0) // [0]
console.log(a, b, arrs1) // 1 3 [ 4, 5 ]

let vals: any = null;
[a, b] = vals;
console.log(a, b);
```

# Object

Object 解構類似 Array 但使用 {} 來獲取解構值

```
// 定義三個變 接收數組前三個元素
const o = { a: 1, b: 2 } as any;
let { a, b, c } = o;
console.log(a, b, c); // 1 2 undefined

// 沒有宣告式需要使用 () 括起來
let d: any
({ a: c, b, d=0 } = o); // 將 o.a 輸出到變量 c, 爲 d 設置 默認值
console.log(b, c, d); // 2 1 0

// 將其它值設置到 object
const { a1, ...b1 } = { a1: 1, b: 2, c: 3 };
console.log(a1, b1); // 1 { b: 2, c: 3 }
```

使用解構可以很容易實現類似 dart 的命名參數
```
interface GetOptions {
    network?: 'tcp' | 'udp' | 1 | 2,
    keepalive?: boolean,
    timeout?: number,
}
function get(addr: string, opts?: GetOptions) {
    const { network = 'tcp', keepalive = true, timeout = 10 } = opts ?? {};
    console.log(`get ${addr}: network=${network} keepalive=${keepalive} timeout=${timeout}`)
}
const addr = "localhost"
get(addr);
get(addr, {
    network: "udp",
    keepalive: false,
});
get(addr, {
    timeout: 5,
});
```