# aac格式

* ADIF
* ADTS

# ADIF 

ADIF 既 Audio Data Interchange Format 的簡寫 

ADIF 不能在音頻數據流中解碼 必須從頭部開始解碼 通常存儲在磁盤檔案中

![](assets/adif.gif)

最開始是頭部信息 之後是 各幀數據
# ADTS
ADTS 既 Audio Data Transport Stream 音頻數據傳輸流的 簡寫

此格式每幀都有一個 同步字的比特流(12 bits 的 1111 1111 1111) 可從流中任意位置開始解碼

![](assets/ADTS.png)

ADTS 頭分爲
* adts\_fixed\_header 固定頭
* adts\_variable\_header 可變頭

## 固定頭

| 名稱 | 長度 | 含義 |
| -------- | -------- | -------- |
| syncword     | 12 bits     | 固定爲 **0xFFF** 用於確定 ADTS 開始位置     |
| id     | 1 bit     | MPEG 版本 **0**&lt;MPEG\-4&gt; **1**&lt;MPEG\-2&gt;     |
| layer     | 2 bits     | 固定爲 0     |
| protection_absent     | 1 bit     | **0**&lt;包含crc效驗&gt; **1**&lt;沒有crc效驗&gt;     |
| profile     | 2 bit     | 使用哪個級別的 AAC 取值爲**Audio Object Type -1** 比如 **0**&lt;代表AAC&nbsp;MAIN&gt; **1**&lt;代表&nbsp;AAC&nbsp;LC&gt;     |
| sampling\_frequency\_index     | 4 bit     | 採樣率 0x0~0xF     |
| private_bit     | 1 bit     | 編碼時設置爲0 解碼時忽略     |
| channel_configuration     | 3 bits     | 聲道數     |
| original\_copy     | 1 bit     | 編碼時設置爲0 解碼時忽略     |
| home     | 1 bit     | 編碼時設置爲0 解碼時忽略     |

![](assets/profile.png)



| sampling\_frequency\_index | 採樣率 |
| -------- | -------- |
| 0x0     | 96000     |
| 0x1     | 88200    |
| 0x2     | 64000    |
| 0x3     | 48000    |
| 0x4     | 44100     |
| 0x5     | 32000     |
| 0x6     | 24000     |
| 0x7     | 22050     |
| 0x8     | 16000     |
| 0x9     | 12000     |
| 0xa     | 11025     |
| 0xb     | 8000     |
| 0xc     | 7350     |
| 0xd     | reserved     |
| 0xe     | reserved     |
| 0xf     | escape value     |

![](assets/channel_configuration.png)

# 可變頭

| 名稱 | 長度 | 含義 |
| -------- | -------- | -------- |
| copyrighted\_id\_bit     | 1 bit     | 編碼時設置爲0 解碼時忽略     |
| copyrighted\_id\_start     | 1 bit     | 編碼時設置爲0 解碼時忽略     |
| aac\_frame\_length     | 13 bits     | 一個 ADTS 幀總 長度 包含了 頭部和數據     |
| adts\_buffer\_fullness     | 11 bits     | 固定爲 0x7ff 說明是可變碼流     |
| number\_of\_raw\_data\_blocks\_in\_frame     | 2 bits     | 表示當前有 number\_of\_raw\_data\_blocks\_in\_frame + 1个 原始幀     |

