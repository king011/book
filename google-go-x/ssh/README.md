# crypto/ssh

[golang.org/x/crypto/ssh](https://godoc.org/golang.org/x/crypto/ssh) 實現了 ssh 服務器/客戶端

# Dial

Dial 函數 實現了 到 ssh 的 連接 如果成功會 創建一個 ssh 客戶端

ClientConfig 定義了 客戶端 配置
```
type ClientConfig struct {
    // Config contains configuration that is shared between clients and
    // servers.
    Config

    // 登入用戶名
    User string

    // 認證方式
    // ssh.Password 函數返回 一個 密碼認證 實現
    // ssh.ParsePrivateKey 函數返回 密鑰認證 實現
    Auth []AuthMethod

    // 在握手時被回調 客戶端 需要再次 驗證 密鑰是否正確
    // InsecureIgnoreHostKey 函數 直接返回 nil 相當於 忽略 密鑰驗證
    // FixedHostKey 則只驗證 是否是某個 密鑰
    HostKeyCallback HostKeyCallback

    // BannerCallback is called during the SSH dance to display a custom
    // server's message. The client configuration can supply this callback to
    // handle it as wished. The function BannerDisplayStderr can be used for
    // simplistic display on Stderr.
    BannerCallback BannerCallback

    // 包含要連接的版本 字符串
    // 如果爲空 則使用一個默認的 合理值
    ClientVersion string

    // 客戶端 將要使用的 密鑰類型
    // 安順序從服務器接收 密鑰
    //
    // 如果爲空 則使用一個 默認的 合理值
    HostKeyAlgorithms []string

    // tcp建立超時時間
    //
    // 如果爲0則永不超時
    Timeout time.Duration
}
```

```
#info="密碼登入"
package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

func main() {
	// 由 known_hosts1 檔案 創建 密鑰認證函數
	hosts := os.Getenv("HOME") + "/.ssh/known_hosts"
	hostKeyCallback, e := knownhosts.New(hosts)
	if e != nil {
		if os.IsNotExist(e) {
			// 直接忽略 認證
			hostKeyCallback = ssh.InsecureIgnoreHostKey()
		} else {
			log.Fatalln(e)
		}
	}
	cnf := &ssh.ClientConfig{
		User: "king",
		Auth: []ssh.AuthMethod{
			ssh.Password(pwd),
		},
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) (e error) {
			e = hostKeyCallback(hostname, remote, key)
			fmt.Println(e)
			if e == nil {
				return
			} else if e.Error() != "knownhosts: key is unknown" {
				// 密鑰 不匹配 返回 錯誤
				return
			}
			e = nil
			// hostname 密鑰 不存在 創建密鑰
			f, _ := os.OpenFile(hosts, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
			if e == nil {
				f.WriteString(knownhosts.HashHostname(knownhosts.Normalize(hostname)) + knownhosts.Line(nil, key) + "\n")
				f.Close()
				return
			}
			return nil
		},
		HostKeyAlgorithms: []string{
			ssh.KeyAlgoRSA,
			ssh.KeyAlgoDSA,
			ssh.KeyAlgoECDSA256,
			ssh.KeyAlgoECDSA384,
			ssh.KeyAlgoECDSA521,
			ssh.KeyAlgoED25519,
		},
		Timeout: 5 * time.Second,
	}
	client, e := ssh.Dial("tcp", fmt.Sprintf("%s:22", host), cnf)
	if e != nil {
		log.Fatalln(e)
	}
	defer client.Close()

	log.Println("ok")
}
```

```
#info="密鑰登入"
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

func main() {
	// 由 known_hosts1 檔案 創建 密鑰認證函數
	var authMethod []ssh.AuthMethod
	{
		filanme := os.Getenv("HOME") + "/.ssh/id_rsa"
		b, e := ioutil.ReadFile(filanme)
		if e != nil {
			log.Fatalln(e)
		}
		s, e := ssh.ParsePrivateKey(b)
		if e != nil {
			log.Fatalln(e)
		}
		authMethod = []ssh.AuthMethod{
			ssh.PublicKeys(s),
		}
	}
	hosts := os.Getenv("HOME") + "/.ssh/known_hosts"
	hostKeyCallback, e := knownhosts.New(hosts)
	if e != nil {
		if os.IsNotExist(e) {
			// 直接忽略 認證
			hostKeyCallback = ssh.InsecureIgnoreHostKey()
		} else {
			log.Fatalln(e)
		}
	}
	cnf := &ssh.ClientConfig{
		User: "king",
		Auth: authMethod,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) (e error) {
			e = hostKeyCallback(hostname, remote, key)
			fmt.Println(e)
			if e == nil {
				return
			} else if e.Error() != "knownhosts: key is unknown" {
				// 密鑰 不匹配 返回 錯誤
				return
			}
			e = nil
			// hostname 密鑰 不存在 創建密鑰
			f, _ := os.OpenFile(hosts, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
			if e == nil {
				f.WriteString(knownhosts.HashHostname(knownhosts.Normalize(hostname)) + knownhosts.Line(nil, key) + "\n")
				f.Close()
				return
			}
			return nil
		},
		HostKeyAlgorithms: []string{
			ssh.KeyAlgoRSA,
			ssh.KeyAlgoDSA,
			ssh.KeyAlgoECDSA256,
			ssh.KeyAlgoECDSA384,
			ssh.KeyAlgoECDSA521,
			ssh.KeyAlgoED25519,
		},
		Timeout: 5 * time.Second,
	}
	client, e := ssh.Dial("tcp", fmt.Sprintf("%s:22", host), cnf)
	if e != nil {
		log.Fatalln(e)
	}
	defer client.Close()

	log.Println("ok")
}
```