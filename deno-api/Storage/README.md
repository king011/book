# Storage
deno 實現了 web 的 Storage interface

```
interface Storage {
  /**
   * Returns the number of key/value pairs currently present in the list associated with the object.
   */
  readonly length: number;
  /**
   * Empties the list associated with the object of all key/value pairs, if there are any.
   */
  clear(): void;
  /**
   * Returns the current value associated with the given key, or null if the given key does not exist in the list associated with the object.
   */
  getItem(key: string): string | null;
  /**
   * Returns the name of the nth key in the list, or null if n is greater than or equal to the number of key/value pairs in the object.
   */
  key(index: number): string | null;
  /**
   * Removes the key/value pair with the given key from the list associated with the object, if a key/value pair with the given key exists.
   */
  removeItem(key: string): void;
  /**
   * Sets the value of the pair identified by key to value, creating a new key/value pair if none existed for key previously.
   *
   * Throws a "QuotaExceededError" DOMException exception if the new value couldn't be set. (Setting could fail if, e.g., the user has disabled storage for the site, or if the quota has been exceeded.)
   */
  setItem(key: string, value: string): void;
}
```

# sessionStorage/localStorage

* sessionStorage 在內存中存儲數據
* localStorage 將數據存儲在 **$HOME/.cache/deno/location_data/${HASH}/local_storage** 檔案中

localStorage 需要操作檔案，但是 Storage 的api 不是異步的所以執行 localStorage 函數時，cpu會等待磁盤完成，故若考慮效率不要使用 localStorage