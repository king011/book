# sqflite
[sqflite](https://pub.dartlang.org/packages/sqflite) 是一個 flutter 插件 爲 android/ios/mac 提供了 SQLite 支持

```
dependencies:
  sqflite: ^2.0.2
  sqflite_common_ffi: ^2.1.0+2
  sqlite3_flutter_libs: ^0.5.4
```

1. sqflite\_common\_ffi 可以使 sqflite 以 ffi 工作，並支持 linux 和 windows
2. 對於 android 要使用 ffi 需要依賴 sqlite3\_flutter\_libs

# linux windows

[using\_ffi\_instead\_of\_sqflite](https://github.com/tekartik/sqflite/blob/master/sqflite_common_ffi/doc/using_ffi_instead_of_sqflite.md)

[sqflite\_common\_ffi](https://pub.dev/packages/sqflite_common_ffi)

# Database

class Database 提供了數據庫的所有操作，通常建議創建一個 static class 用於管理 Database 的創建和初始化

```
#info="lib/db/db.dart"
import 'dart:async';
import 'dart:io';

import 'package:flutter/rendering.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'data/helpers.dart';

/// 定義 static class 用於管理數據庫創建
class DB {
  DB._();
  static const name = 'my.db';
  static const version = 3;

  /// 初始化 ffi
  static Future<void> _ffiInit() async {
    if (databaseFactory == databaseFactoryFfi) {
      return;
    }
    if (Platform.isWindows || Platform.isLinux) {
      // Initialize FFI
      sqfliteFfiInit();
    } else {
      databaseFactoryFfi.setDatabasesPath(await getDatabasesPath());
    }
    databaseFactory = databaseFactoryFfi;
  }

  static Completer<Helpers>? _completer;

  /// 返回數據庫操作 helper
  static Future<Helpers> get helpers async {
    if (_completer == null) {
      final completer = Completer<Helpers>();
      _completer = completer; // 保證同時只有一 future 執行數據庫初始化

      try {
        await _ffiInit(); // 初始化 ffi

        final db = await openDatabase(
          name,
          version: version,
          onCreate: (Database db, int version) => Helpers.onCreate(db, version),
          onUpgrade: (Database db, int oldVersion, int newVersion) =>
              Helpers.onUpgrade(db, oldVersion, newVersion),
        );
        debugPrint("db helper ready");
        completer.complete(Helpers(db));
      } catch (e) {
        debugPrint("db init error : $e");
        _completer = null; // 重置 _completer 以便可以再次執行數據庫初始化
        completer.completeError(e);
        return completer.future;
      }
    }
    return _completer!.future;
  }
}
```

## getDatabasesPath

getDatabasesPath 函數返回了數據庫存儲檔案夾路徑，在使用 ffi 時，其值可能並不合理，你可以使用 setDatabasesPath 函數來設置一個合適的存儲路徑

* android 下 getDatabasesPath 爲 **/.dart\_tool/sqflite\_common\_ffi/databases**，這顯然無法正常工作故需調用 setDatabasesPath
* linux 下 getDatabasesPath 返回可執行檔案所在位置下的 **.dart\_tool/sqflite\_common\_ffi/databases**

# Helper
對於每張表推薦創建一個 class data 與 class helper，data 用於映射數據 helper 用於提供操作函數，例如下面是一個 datetime 的 class 可用於存儲緩存數據

```
#info="lib/db/data/datetime.dart"
import 'package:sqflite/sqflite.dart';

///  映射表結構到 dart
class Datetime {
  int? id;

  /// 創建時間
  DateTime? created;

  /// 最後修改時間
  DateTime? last;

  Datetime({
    this.id,
    this.created,
    this.last,
  });

  Map<String, dynamic> toMap() => <String, dynamic>{
        DatetimeHelper.columnID: id,
        DatetimeHelper.columnCreated:
            created?.toUtc().millisecondsSinceEpoch ?? 0,
        DatetimeHelper.columnLast: last?.toUtc().millisecondsSinceEpoch ?? 0,
      };

  Datetime.fromMap(Map<String, dynamic> map) {
    id = map[DatetimeHelper.columnID];
    var v = map[DatetimeHelper.columnCreated];
    if (v is int && v > 0) {
      created = DateTime.fromMillisecondsSinceEpoch(v, isUtc: true).toLocal();
    }
    v = map[DatetimeHelper.columnLast];
    if (v is int && v > 0) {
      last = DateTime.fromMillisecondsSinceEpoch(v, isUtc: true).toLocal();
    }
  }
}

/// helper 提供對表操作函數
class DatetimeHelper {
  static const table = 'date_time';
  static const columnID = 'id';
  static const columnCreated = 'created';
  static const columnLast = 'last';
  static const columns = [
    columnID,
    columnCreated,
    columnLast,
  ];
  static Future<void> onCreate(Database db, int version) async {
    await db.execute('''CREATE TABLE IF NOT EXISTS 
$table (
$columnID INTEGER PRIMARY KEY, 
$columnCreated INTEGER DEFAULT 0,
$columnLast INTEGER DEFAULT 0
)''');
  }

  static Future<void> onUpgrade(
      Database db, int oldVersion, int newVersion) async {
    await onCreate(db, newVersion);
  }

  static Future<int> update<T extends DatabaseExecutor>(
    DatabaseExecutor db,
    Datetime datetime,
  ) {
    return db.insert(
      table,
      datetime.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<Datetime?> getByID(DatabaseExecutor db, int id) async {
    final list = await db.query(
      table,
      columns: columns,
      where: '$columnID = ?',
      whereArgs: [id],
      limit: 1,
    );
    if (list.isNotEmpty) {
      return Datetime.fromMap(list.first);
    }
    return null;
  }

  static Future<int> deleteByID(DatabaseExecutor db, int id) {
    return db.delete(
      table,
      where: '$columnID = ?',
      whereArgs: [id],
    );
  }
}
```

下面是一個 person 的 class 你可以參考它爲自己的表創建 映射與helper
```
#info="lib/db/data/helper.dart"
import 'dart:async';

import 'package:sqflite/sqflite.dart';

_exception(String func) => Exception('Helper function not override: $func');

abstract class Helper<T> {
  Helper(this.db);
  final Database db;

  String get tableName => throw _exception('get tableName');
  T fromMap(Map<String, dynamic> map) => throw _exception('fromMap');
  Map<String, dynamic> toMap(T data) => throw _exception('toMap');
}

mixin HasId {
  String get byId => 'id';
}
mixin HasName {
  String get byName => 'name';
}

/// 爲 helper 提供一些通用的 方法
mixin Executor<T> on Helper<T> {
  /// 查詢數據
  Future<List<T>> query({
    bool? distinct,
    List<String>? columns,
    String? where,
    List<Object?>? whereArgs,
    String? groupBy,
    String? having,
    String? orderBy,
    int? limit,
    int? offset,
  }) async {
    final result = <T>[];
    final list = await db.query(
      tableName,
      distinct: distinct,
      columns: columns,
      where: where,
      whereArgs: whereArgs,
      groupBy: groupBy,
      having: having,
      limit: limit,
      offset: offset,
    );
    for (var m in list) {
      result.add(fromMap(m));
    }
    return result;
  }

  /// 查詢第一條數據
  Future<T?> first({
    bool? distinct,
    List<String>? columns,
    String? where,
    List<Object?>? whereArgs,
    String? groupBy,
    String? having,
    String? orderBy,
    int? limit,
    int? offset,
  }) async {
    final list = await db.query(
      tableName,
      distinct: distinct,
      columns: columns,
      where: where,
      whereArgs: whereArgs,
      groupBy: groupBy,
      having: having,
      limit: 1,
    );
    return list.isEmpty ? null : fromMap(list.first);
  }

  /// 添加一條記錄
  Future<int> add(T data) => db.insert(tableName, toMap(data));

  /// 添加多條記錄
  FutureOr<List<int>> addAll(Iterable<T> iterable) {
    if (iterable.isNotEmpty) {
      return <int>[];
    }

    return db.transaction((txn) async {
      final result = <int>[];
      for (var data in iterable) {
        result.add(await txn.insert(tableName, toMap(data)));
      }
      return result;
    });
  }

  /// 刪除記錄
  Future<int> delete({String? where, List<Object?>? whereArgs}) => db.delete(
        tableName,
        where: where,
        whereArgs: whereArgs,
      );

  /// 修改記錄
  Future<int> update(
    Map<String, Object?> values, {
    String? where,
    List<Object?>? whereArgs,
    ConflictAlgorithm? conflictAlgorithm,
  }) =>
      db.update(
        tableName,
        values,
        where: where,
        whereArgs: whereArgs,
        conflictAlgorithm: conflictAlgorithm,
      );
}

/// 提供 xxxById 函數
mixin ById<T, TypeID> on Helper<T>, HasId {
  /// 返回 id 爲指定值的數據
  Future<T?> getById(
    TypeID id, {
    bool? distinct,
    List<String>? columns,
    String? groupBy,
    String? having,
    String? orderBy,
    int? offset,
  }) async {
    final list = await db.query(
      tableName,
      distinct: distinct,
      columns: columns,
      where: '$byId = ?',
      whereArgs: [id],
      groupBy: groupBy,
      having: having,
      limit: 1,
    );
    return list.isEmpty ? null : fromMap(list.first);
  }

  /// 刪除 id 爲指定值的 數據
  Future<int> deleteById(TypeID id) => db.delete(
        tableName,
        where: '$byId = ?',
        whereArgs: [id],
      );

  /// 更新 id 爲指定值的 數據
  Future<int> updateById(
    TypeID id,
    Map<String, Object?> values, {
    ConflictAlgorithm? conflictAlgorithm,
  }) =>
      db.update(
        tableName,
        values,
        where: '$byId = ?',
        whereArgs: [id],
        conflictAlgorithm: conflictAlgorithm,
      );
}

/// 提供 xxxByName 函數
mixin ByName<T, TypeName> on Helper<T>, HasName {
  /// 返回 name 爲指定值的數據
  Future<T?> getByName(
    TypeName name, {
    bool? distinct,
    List<String>? columns,
    String? groupBy,
    String? having,
    String? orderBy,
    int? offset,
  }) async {
    final list = await db.query(
      tableName,
      distinct: distinct,
      columns: columns,
      where: '$byName = ?',
      whereArgs: [name],
      groupBy: groupBy,
      having: having,
      limit: 1,
    );
    return list.isEmpty ? null : fromMap(list.first);
  }

  /// 刪除 name 爲指定值的 數據
  Future<int> deleteByName(TypeName name) => db.delete(
        tableName,
        where: '$byName = ?',
        whereArgs: [name],
      );

  /// 更新 name 爲指定值的 數據
  Future<int> updateByName(
    TypeName name,
    Map<String, Object?> values, {
    ConflictAlgorithm? conflictAlgorithm,
  }) =>
      db.update(
        tableName,
        values,
        where: '$byName = ?',
        whereArgs: [name],
        conflictAlgorithm: conflictAlgorithm,
      );
}
```

```
#info="lib/db/data/person.dart"
import 'package:sqflite/sqflite.dart';
import './helper.dart';

class Person {
  int id;
  String name;
  int level;
  Person({
    required this.id,
    required this.name,
    required this.level,
  });
  Map<String, dynamic> toMap() => <String, dynamic>{
        PersonHelper.columnID: id,
        PersonHelper.columnName: name,
        PersonHelper.columnLevel: level,
      };
  Person.fromMap(Map<String, dynamic> map)
      : id = map['id'] ?? 0,
        name = map['name'] ?? '',
        level = map['level'] ?? 0;
}

class PersonHelper extends Helper<Person>
    with Executor, HasId, ById<Person, int>, HasName, ByName<Person, String> {
  static const table = 'person';
  static const columnID = 'id';
  static const columnName = 'name';
  static const columnLevel = 'level';
  static const columns = [
    columnID,
    columnName,
    columnLevel,
  ];
  static Future<void> onCreate(Database db, int version) async {
    await db.execute('''CREATE TABLE IF NOT EXISTS 
$table (
$columnID INTEGER PRIMARY KEY AUTOINCREMENT, 
$columnName TEXT DEFAULT '',
$columnLevel INTEGER DEFAULT 0
)''');
    await db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 
unique_$columnName
ON $table ($columnName);
''');
    await db.execute('''CREATE INDEX IF NOT EXISTS 
index_$columnLevel
ON $table ($columnLevel);
''');
  }

  static Future<void> onUpgrade(
      Database db, int oldVersion, int newVersion) async {
    await onCreate(db, newVersion);
  }

  PersonHelper(Database db) : super(db);
  @override
  String get tableName => table;
  @override
  Person fromMap(Map<String, dynamic> map) => Person.fromMap(map);
  @override
  Map<String, dynamic> toMap(Person data) => data.toMap();
}
```

## Helpers

建議創建一個 helpers 用於保存對每張表的 helper 實例，同時實現 onCreate 與 onUpgrade 函數供 openDatabase 調用

```
#info="lib/db/data/helpers.dart"
import 'dart:async';

import 'package:flutter/rendering.dart';
import 'package:sqflite/sqflite.dart';
import './datetime.dart';
import './person.dart';

class Helpers {
  final PersonHelper person;
  Helpers(Database db) : person = PersonHelper(db);
  static FutureOr<void> onCreate(Database db, int version) async {
    debugPrint('onCreate: $version');
    await DatetimeHelper.onCreate(db, version);
    await PersonHelper.onCreate(db, version);
  }

  static FutureOr<void> onUpgrade(
    Database db,
    int oldVersion,
    int newVersion,
  ) async {
    debugPrint('onUpgrade: $oldVersion -> $newVersion');
    await DatetimeHelper.onUpgrade(db, oldVersion, newVersion);
    await PersonHelper.onUpgrade(db, oldVersion, newVersion);
  }
}
```