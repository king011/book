# 下載 http 檔案

```
#info="下載檔案"
aria2c \
 -o "ubuntu-20.04.3-live-server-arm64.iso" \
 -j 5 \
 --header "User-Agent: aria2c" \
 --header "Cookie: XXX" \
 "https://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04.3-live-server-arm64.iso" 
```
* -o 下載到本地的目標檔案名稱
* -j 設置每個等待下載的資源 最大使用多少併發 默認爲 5
* --header 設置 http 頭

```
#info="從多個源下載"
aria2c \
 "https://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04.3-live-server-arm64.iso"  \
 "https://127.0.0.1/releases/20.04/release/ubuntu-20.04.3-live-server-arm64.iso"
```

# 批量下載

```
aria2c -i list.txt
```

```
#info="list.txt"
http://127.0.0.1/tools/HD%20%E4%B8%AD%E6%96%87%E5%AD%97%E5%B9%95%20Mass%20Effect%202_Cinematic%20Trailer%20%E8%B3%AA%E9%87%8F%E6%95%88%E6%87%892%20CG%E9%A0%90%E5%91%8A%E7%89%87.mp4
	out=ok.mp4
	dir=video
http://127.0.0.1/tools/ok.apk
```

[-i, --input-file=<FILE>](https://aria2.github.io/manual/en/html/aria2c.html#id2)