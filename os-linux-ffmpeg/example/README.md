# ffmpeg

ffmpeg 提供了 常用的 轉碼 和 音視頻 剪輯功能

```
# 轉字幕文件
ffmpeg -i a.ass a.vtt

# 轉換媒體容器
ffmpeg -i a.mkv -codec copy a.mp4

# 以 h265 編碼視頻
ffmpeg -i a.mp4 -c:v libx265 265.mp4

# 以 vp9 編碼視頻
ffmpeg -i a.mp4 -c:v libvpx-vp9 vp9.webm
ffmpeg -i a.mp4 -c:v vp9_vaapi vp9.webm


# 以 av1 編碼
ffmpeg -i a.mp4 -c:v libaom-av1 -strict -2 a.mkv

# 以 vp9 編碼
ffmpeg -i a.mp4 -c:v libvpx-vp9 a.webm

# 爲視頻 嵌入字幕
ffmpeg -i a.mkv -i a.ass -codec copy out.mkv

# 提前視頻中的字幕( -map s:0 代表提前第一個字幕)
ffmpeg -i a.mkv -map s:0 a.sc.vtt
ffmpeg -i a.mkv -map s:1 a.tc.vtt

# 爲 mp4 之類不支持字幕的 視頻 強制 嵌入 字幕
ffmpeg -i a.mp4 -vf subtitles=a.ass out.mp4

# 剪切視頻 從 30秒 到 1小時59分30秒
ffmpeg -i input.mp4 -c copy -ss 30 -to 1:59:30 output.mp4
```


```
# h265 轉 h264
ffmpeg -i a.mkv -map 0 -c:v libx264 -c:a copy b.mp4
```

# threads
默認 ffmpeg 會儘量使用 cpu 即啓動和 cpu 數一樣多的線程可以使用 threads 參數明確指定線程數

```
ffmpeg -threads 5
```

# m3u8

很多在線視頻都使用 m3u8 格式，m3u8實際上是一個索引檔案，裏面記錄了視頻每個段的下載地址，使用 ffmpeg 可以輕鬆的下載

```
ffmpeg -i "m3u8_file_uri" "save_video.mp4" 
```

如果只想要下載音頻
```
ffmpeg -i "m3u8_file_uri" -c copy -map a "output.ts"
```