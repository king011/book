# [FlameGame](https://docs.flame-engine.org/1.16.0/flame/game.html)

FlameGame 是實現了 Game 的基礎組件。它有一個組件樹，並調用所有已添加組件的 update 和 render 函數

我們將這種基於 component-based 的系統稱為 Flame 元件系統 (FCS)。 在整個文件中，FCS 用於引用該系統。

可以使用命名的children參數直接在建構函式中將元件加入FlameGame中，也可以使用add/addAll方法從其他任何地方加入元件。 然而，大多數時候，您希望將您的孩子添加到一個 world 中，預設 world 存在於 FlameGame.world 下，並且您可以向其中添加組件，就像向任何其他組件添加組件一樣。

一個簡單的 FlameGame 實作加入了兩個元件，一個在 onLoad 中，一個直接在建構函式中，如下所示：

```
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flutter/widgets.dart';

/// A component that renders the crate sprite, with a 16 x 16 size.
class MyCrate extends SpriteComponent {
  MyCrate() : super(size: Vector2.all(16));

  @override
  Future<void> onLoad() async {
    sprite = await Sprite.load('crate.png');
  }
}

class MyWorld extends World {
  @override
  Future<void> onLoad() async {
    await add(MyCrate());
  }
}

void main() {
  final myGame = FlameGame(world: MyWorld());
  runApp(
    GameWidget(game: myGame),
  );
}
```

若要從 FlameGame 的清單中移除元件，可以使用remove 或removeAll 方法。 如果您只想刪除一個元件，則可以使用第一個，而當您想要刪除一系列元件時，可以使用第二個。 這些方法存在於所有元件上，包括 world。

# [Game Loop](https://docs.flame-engine.org/1.16.0/flame/game.html#game-loop)

GameLoop 模組是遊戲循環概念的簡單抽象。 基本上，大多數遊戲都是基於兩種方法構建的：

* **render** 方法使用畫布來繪製遊戲的當前狀態。
* **update** 方法接收自上次更新以來的增量時間（以微秒為單位），並允許您移至下一個狀態。

所有 Flame 的遊戲實作都使用 GameLoop

# [Resizing](https://docs.flame-engine.org/1.16.0/flame/game.html#resizing)

每次需要調整遊戲大小時，例如更改方向時，FlameGame 將呼叫所有組件的 onGameResize 方法，並將此資訊傳遞給相機和視窗。

FlameGame.camera 控制座標空間中的哪個點應位於取景器的錨點處，預設 [0,0] 位於視口的中心 (Anchor.center)。

# [Lifecycle](https://docs.flame-engine.org/1.16.0/flame/game.html#lifecycle)

FlameGame 生命週期依照以下順序呼叫：

![](assets/lifecycle.png)

當 FlameGame 首次新增至 GameWidget 時，將依序呼叫生命週期方法 onGameResize、onLoad 和 onMount。 然後，每個遊戲週期都會依序調用 update 和 render。 如果從 GameWidget 中刪除 FlameGame，則呼叫 onRemove。 如果將 FlameGame 新增至新的 GameWidget，則該序列將從 onGameResize 開始重複。

> onGameResize和onLoad的順序與其他元件相反。 這是為了允許在載入或生成資源之前計算遊戲元素的大小。

onRemove 回呼可用於清理子項目和快取資料：

```
@override
void onRemove() {
	// Optional based on your game needs.
	removeAll(children);
	processLifecycleEvents();
	Flame.images.clearCache();
	Flame.assets.clearCache();
	// Any other code that you want to run when the game is removed.
}
```

> FlameGame 中子項和資源的清理不會自動完成，必須明確地加入 onRemove 呼叫中。

# [Debug mode](https://docs.flame-engine.org/1.16.0/flame/game.html#debug-mode)
Flame 的 FlameGame 類別提供了一個名為 debugMode 的變量，預設為 false。 但是，可以將其設為 true 以啟用遊戲組件的調試功能。 請注意，當元件新增至遊戲時，該變數的值會傳遞給其元件，因此如果您在執行時間變更 debugMode，預設不會影響已新增的元件。

要了解有關 Flame 上的 debugMode 的更多信息，請參閱[調試文檔](https://docs.flame-engine.org/1.16.0/flame/other/debug.html)

# [Change background color](https://docs.flame-engine.org/1.16.0/flame/game.html#change-background-color)

若要變更 FlameGame 的背景顏色，您必須重寫 backgroundColor()。

在以下範例中，背景顏色設定為完全透明，以便您可以看到 GameWidget 後面的 widget。 預設為不透明黑色。

```
class MyGame extends FlameGame {
  @override
  Color backgroundColor() => const Color(0x00000000);
}
```

請注意，背景顏色在遊戲運行時無法動態更改，但如果您希望背景顏色動態更改，則可以繪製覆蓋整個畫布的背景

# [SingleGameInstance mixin](https://docs.flame-engine.org/1.16.0/flame/game.html#singlegameinstance-mixin)

如果您正在製作單遊戲應用程序，則可以將可選的 mixin SingleGameInstance 應用於您的遊戲。 這是建立遊戲時的常見場景：有一個全螢幕 GameWidget 託管單一 Game 實例。

新增此 mixin 可在某些場景中提供效能優勢。 特別是，當元件新增至其父元件時，即使父元件尚未安裝，元件的 onLoad 方法也保證啟動。 因此，對parent.add(component) 的await-ing 保證始終完成元件的載入。

```
class MyGame extends FlameGame with SingleGameInstance {
  // ...
}
```

# [Low-level Game API](https://docs.flame-engine.org/1.16.0/flame/game.html#low-level-game-api)
![](assets/low.png)

抽象 Game 類別是一個低階 API，當您想要實作遊戲引擎的結構功能時可以使用它。 例如，遊戲不實現任何更新或渲染功能。

該類別還包含生命週期方法 onLoad、onMount 和 onRemove，當遊戲載入+安裝或刪除時，這些方法會從 GameWidget（或另一個父級）呼叫。 onLoad 僅在類別第一次添加到父級時調用，但 onMount（在 onLoad 之後調用）每次添加到新父級時都會調用。 當類別從父類別中刪除時，將呼叫 onRemove。

> Game 類別允許更自由地實現事物，但如果您使用它，您也會錯過 Flame 中的所有內建功能


Game 實現的範例如下:

```
class MyGameSubClass extends Game {
  @override
  void render(Canvas canvas) {
    // ...
  }

  @override
  void update(double dt) {
    // ...
  }
}

void main() {
  final myGame = MyGameSubClass();
  runApp(
    GameWidget(
      game: myGame,
    )
  );
}
```

# [Pause/Resuming/Stepping game execution](https://docs.flame-engine.org/1.16.0/flame/game.html#pause-resuming-stepping-game-execution)

Flame Game 可以透過兩種方式暫停和恢復：

* 調用 pauseEngine/resumeEngine
* 修改 paused 實現

暫停遊戲時，GameLoop 實際上已暫停，這意味著在恢復之前不會發生任何 update 或 render

當遊戲暫停時，可以使用 stepEngine 方法逐幀推進遊戲。 它在最終遊戲中可能沒有多大用處，但對於在開發週期中逐步檢查遊戲狀態非常有幫助

# [Backgrounding](https://docs.flame-engine.org/1.16.0/flame/game.html#backgrounding)

當應用程式發送到後台時，遊戲將自動暫停，當應用程式返回前台時，遊戲將自動恢復。 可以透過將pauseWhenBackgrounded 設為 false 來停用此行為

```
class MyGame extends FlameGame {
  MyGame() {
    pauseWhenBackgrounded = false;
  }
}
```
