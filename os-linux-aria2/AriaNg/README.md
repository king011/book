# AriaNg

AriaNg 是使用 angularjs 爲 aria2 寫的一個 開源(MIT) web ui

* 官網 [https://ariang.mayswind.net/zh_Hans/3rd-extensions.html](https://ariang.mayswind.net/zh_Hans/3rd-extensions.html)
* 源碼 [https://github.com/mayswind/AriaNg](https://github.com/mayswind/AriaNg)