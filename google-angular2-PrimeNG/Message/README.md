# [Message](https://primeng.org/messages)

```
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
```

Message 通常用來顯示內聯的警告，使用 **p-messages** 標籤來創建信息顯示的載體

```
<p-messages [(value)]="msgs"></p-messages>
```

value 是雙向綁定的數組，數組每個元素是一個顯示的消息

```
<p-messages [(value)]="msgs"></p-messages>

<button type="button" (click)="show()">Show</button>
<button type="button" (click)="clear()">Hide</button>
```

```
msgs: Array<Message> = []
show() {
	this.msgs = [
		...this.msgs,
		{ severity: 'info', summary: 'Info Message', detail: 'PrimeNG rocks' },
	]
}

hide() {
	this.msgs = []
}
```

## MessageService

你可以不用將 msg 值注入 p-messages，取而代之的是使用 MessageService

```
@Component({
  providers: [MessageService],
})
export class MessageDemoComponent {
  addSingle(key?: string) {
    this.messageService.add(
      {
        // 指定要在哪個 p-messages 中顯示
        key: key,
        // 消息嚴重性
        severity: 'success',
        // 消息概括文本
        summary: 'Service Message',
        // 消息詳細文本 
        detail: 'Via MessageService',
      }
    )
  }

  addMultiple(key?: string) {
    this.messageService.addAll([
      { key: key, severity: 'success', summary: 'Service Message', detail: 'Via MessageService' },
      { key: key, severity: 'info', summary: 'Info Message', detail: 'Via MessageService' },
    ])
  }
  hide(key?: string) {
    // 要清除哪個 p-messages
    // 這裏與 add 表現有點不一致，如果 key 爲 undefined 會清空所有 p-messages
    this.messageService.clear(key)
  }
}
```

## closable

默認消息有個關閉圖標按鈕，數組 closable 爲 false 可以隱藏這個按鈕

```
<p-messages [closable]="false"></p-messages>
```

## Severities

存在四種不同嚴重性的消息

* success
* info
* warn
* error

## message

**p-message** 組件在需要顯示與表單等元素相關的消息時很有用，它有兩個實現，消息嚴重性 和 文本

```
<p-message severity="info" text="Message Content"></p-message>
<p-message severity="success" text="Message Content"></p-message>  
<p-message severity="warn" text="Message Content"></p-message>  
<p-message severity="error" text="Message Content"></p-message>
```


## Animation Configuration

**showTransitionOptions/hideTransitionOptions** 用於指定顯示與隱藏消息的過渡動畫，下面例子將完全禁用動畫

```
<p-message [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'" severity="info" text="PrimeNG Rocks"></p-message>
```

# Properties of Messages Component

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| value     | array     | null     | 要顯示的消息數組     |
| closable     | boolean     | true     | 消息是否可關閉     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| enableService     | boolean     | true     | 是否顯示服務推送的消息     |
| escape     | boolean     | true     | 顯示消息是否被轉義     |
| key     | string     | null     | 一個 key 用於匹配消息服務     |
| showTransitionOptions     | string     | 300ms ease-out     | 顯示過渡動畫     |
| hideTransitionOptions     | string     | 200ms cubic-bezier(0.86, 0, 0.07, 1)     | 隱藏過渡動畫     |

# Templates

| Name | Parameters |
| -------- | -------- |
| content     | -     |

# Properties of Message Component

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| severity     | string     | null     | 消息嚴重性     |
| text     | string     | null     | 消息文本     |
| escape     | boolean     | true     | 消息是否轉義     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |


# Styling for Messages Component

| Name | Element |
| -------- | -------- |
| p-messages     | Container element.     |
| p-message     | Message element.     |
| p-message-info     | Message element when displaying info messages.     |
| p-message-warn     | Message element when displaying warning messages.     |
| p-message-error     | Message element when displaying error messages.     |
| p-message-success     | Message element when displaying success messages.     |
| p-message-close     | Close button.     |
| p-message-close-icon     | 	Close icon.     |
| p-message-icon     | Severity icon.     |
| p-message-summary     | Summary of a message.     |
| p-message-detail     | Detail of a message.     |

# Styling for Message Component

| Name | Element |
| -------- | -------- |
| p-inline-message     | Message element.     |
| p-inline-message-info     | Message element when displaying info messages.     |
| p-inline-message-warn     | Message element when displaying warning messages.     |
| p-inline-message-error     | Message element when displaying error messages.     |
| p-inline-message-success     | Message element when displaying success messages.     |
| p-inline-message-icon     | Severity icon.     |
| p-inline-message-text     | Text of a message.     |

