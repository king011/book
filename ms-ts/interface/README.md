# 定義接口
* 使用 關鍵字 interface 定義接口 
* 不同其它語言只針對函數 ts中 接口頁可以 定義屬性
* 可以使用 ? 定義可選的 接口 方法/屬性

```ts
// 定義一個 IAnimal 接口
interface IAnimal {
    // 定義 接口方法
    Eat(): void
    // 定義 可選的 接口方法
    Speak?(str: string): void

    // 定義一個 只讀 屬性
    readonly Ethnicity: string

    // 定義一個 可選屬性
    Name?: string
}
```

# 實現接口
* ts 使用 鴨子類型 實現接口 不需要明確只讀 要實現的 接口 只要包含 接口 需要的 定義 就是 實現了接口
* ts 提供了 關鍵字 implements 來明確指定要 實現 某接口(多個接口以 , 分割)的全部 定義

```ts
// 定義一個 IAnimal 接口
interface IAnimal {
    // 定義 接口方法
    Eat(): void
    // 定義 可選的 接口方法
    Speak?(str: string): void

    // 定義一個 只讀 屬性
    readonly Ethnicity: string

    // 定義一個 可選屬性
    Name?: string
}
// 使用 接口 作爲參數
function go(a: IAnimal): void {
    if (a.Name != undefined) {
        console.log("this is", a.Name);
    }
    a.Eat();
    if (a.Speak) {
        a.Speak("cerberus is an idea")
    }
}
// 實現 接口
class Cat {
    readonly Ethnicity: string = "Cat"
    Name: string = "Garfield"
    Eat() {
        console.log(this.Ethnicity, "eat")
    }
}
// 顯示 實現 接口
class Dog implements IAnimal {
    // 對於 只讀 屬性 只要存在 就算實現 不要求 也是只讀
    Ethnicity:string = "Dog"
    Eat() {
        console.log(this.Ethnicity, "eat")
    }
}
go(new Cat())
let d = new Dog();
d.Ethnicity = "dog"
go(d)

// 直接以 Object 實現接口
go({
    Ethnicity: "Hare",
    Eat() {
        console.log(this.Ethnicity, "eat")
    },
})
```

## 額外屬性檢查
當 使用 \{\} 形式 傳遞 接口時 ts 會檢查 不允許 \{\} 存在接口定義 之外的 屬性 如果你剛好需要這麼做 有 兩種 解決方案

將 \{\} 存儲到一個 變量 之後 傳遞
```ts
interface IAnimal {
    Name: string
}
function Info(a: IAnimal) {
    console.log(a.Name)
}
let cat = {
    Name: "Cat",
    Lv: 10,
    Eat() {
    }
}
Info(cat)
```

在接口中 定義 `[propName: string]: any` 允許存在 任意 屬性

```ts
interface IAnimal {
    Name: string
    [propName: string]: any
}
function Info(a: IAnimal) {
    console.log(a.Name)
}
Info({
    Name: "Cat",
    Lv: 10,
    Eat() {
    }
})
```
# 接口繼承

接口可以使用 關鍵字 **extends** 繼承其它 接口

```ts
interface IWrite{
    Write(str:string):void
}
interface IRead{
    Read():string
}
interface IReadWrite extends IWrite,IRead{
}
function RW(rw:IReadWrite){
    rw.Write("cerberus")
    console.log(rw.Read())
}
let _str:string = "";
RW({
    Write(str:string):void{
        _str = str;    
    },
    Read():string{
        return _str;
    }
})
```

# 函數簽名
js 沒有一個 好的 方法 確定一個 函數的簽名 ts 的接口 提供了一個 定義與 要求 函數 簽名的 方案

定義一個 interface 且 只包含 沒有名稱的 函數 簽名 則此 interface 在傳遞時 ts會 檢查傳遞是函數 是否符合簽名

```ts
interface LessFunc {
    (l: any, r: any): boolean
    // 重載
    (l: any): boolean
}

function IsLess(l: any, r: any, less: LessFunc): boolean {
    return less(l, r)
}
console.log(
    IsLess(
        1, 2,
        function (l: any, r?: any): boolean {
            if (r == undefined) {
                return true
            }
            return l < r
        }
    )
)
```
## 混合類型
如果 使用 inteface 定義 函數簽名 時 包含了 其它屬性 和 函數 則 接口 即可作爲 函數 又可作爲 對象 使用
```ts
interface Counter {
    // 定義 函數 簽名
    (start: number): string;

    // 定義 對象 接口
    interval: number;
    reset(): void;
}

function getCounter(): Counter {
    //const counter = <Counter>function (start: number) { console.log(start) };
    const counter = function (start: number) { console.log(start) } as Counter;
    counter.interval = 123;
    counter.reset = function () {
        console.log("reset")
    };
    return counter;
}

let c = getCounter();
c(10);
c.reset();
c.interval = 5.0;
```

# 可引用 類型
ts 提供了 \[index:&nbsp;number\] 和 \[index:&nbsp;string\] 類指定 型別 可以 像 數組 或 map 一樣 使用 \[\] 進行 操作
```ts
interface StringArray {
    // 同樣 可選的 readonly 可以讓 數據只讀 而無法修改 數組
    readonly [index: number]: string;
    length: number
}
interface StringMap {
    readonly [index: string]: number;
}
const arrs: StringArray = ["kate", "anita"]
const keys: StringMap = {
    "kate": 9,
    "anita": 10,
}
for (let i = 0; i < arrs.length; i++) {
    const name = arrs[i];
    const lv = keys[name];
    console.log(name, lv)
}
```

# 接口繼承類
ts 運行 接口 從 一個 類 繼承

類 的 private protect 屬性 也會被繼承 這會導致 只有 此類和其子類 能夠實現此接口

```ts
class Animal {
    private name: string = "Animal"
    protected get Name(): string {
        return this.name;
    }
    Eat() {
        console.log(this.name, "eat")
    }
}

interface IAnimal extends Animal {
}
function go(a: IAnimal): void {
    a.Eat()
}
class Cat extends Animal {
    Eat() {
        console.log(this.Name, "eat")
    }
}
go(new Cat())
```

> 這個特性 很少會用到 且讓人迷糊  
> 除非你確實知道在幹什麼 否則 不建議使用 這個特性 可能會讓代碼 變得 不宜維護 和 難以理解