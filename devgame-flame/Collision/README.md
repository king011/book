# [碰撞檢測](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html)

大多數遊戲都需要碰撞檢測來檢測兩個相互交叉的組件並對其採取行動。 例如，箭擊中敵人或玩家撿起硬幣

在大多數碰撞偵測系統中，您使用稱為碰撞盒的東西來建立更精確的組件邊界框。 在 Flame 中，碰撞框是組件中可以更準確地對碰撞做出反應（並使手勢輸入）的區域

碰撞檢查系統支持三種不同的形狀，你可以從中構建碰撞盒(hitbox)，這些形狀是 多邊形(Polygon) 矩形(Rectangle) 圓形(Circle)。一個 hitbox 可以用許多形狀來表示形成一個區域，可以用來檢查碰撞或者它是否包含一個點，後者對於準確的手勢檢測非常有用。碰撞檢測不處理當兩個碰撞盒發生碰撞時應該發生的情況，因此由用戶來實現當兩個組件具有相互碰撞時會發生什麼

**注意** 內置的碰撞檢查系統不會考慮兩個互相過衝的碰撞盒之間的碰撞，這可能發生在它們移動太快或更新時間過長的情況下，這種行爲稱爲 tunneling

**注意** 如果縮放可碰撞組件的祖先，碰撞檢測系統將無法正常工作

# [Mixins](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#mixins)

Flame 的碰撞檢測系統使用 Mixins 提供

## [HasCollisionDetection](HasCollisionDetection)

如果您想在遊戲中使用碰撞偵測，則必須將 'mixin HasCollisionDetection' with 至遊戲中，以便它可以追蹤可能發生碰撞的元件

```
class MyGame extends FlameGame with HasCollisionDetection {
  // ...
}
```

現在，當您將 ShapeHitbox 添加到隨後添加到遊戲中的組件時，將自動檢查它們是否發生碰撞

您也可以將 HasCollisionDetection 直接 with 至另一個元件而不是 FlameGame，例如 with 至用於 CameraComponent 的 World。 如果這樣做，添加到該組件樹中的碰撞箱將僅與該子樹中的其他碰撞箱進行比較，這使得在一個 FlameGame 中可以擁有多個具有碰撞檢測的 World

```
class CollisionDetectionWorld extends World with HasCollisionDetection {}
```

> Hitboxes 將僅連接到一個碰撞偵測系統，且該系統是具有 mixin HasCollisionDetection 的最接近的父系統


## [CollisionCallbacks](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#collisioncallbacks)

若要對碰撞做出反應，您應該將 CollisionCallbacks mixin 加入元件中。 例子：

```
class MyCollidable extends PositionComponent with CollisionCallbacks {
  @override
  void onCollision(Set<Vector2> points, PositionComponent other) {
    if (other is ScreenHitbox) {
      //...
    } else if (other is YourOtherComponent) {
      //...
    }
  }

  @override
  void onCollisionEnd(PositionComponent other) {
    if (other is ScreenHitbox) {
      //...
    } else if (other is YourOtherComponent) {
      //...
    }
  }
}
```

在這個例子中，我們使用 Dart 的 is 關鍵字來檢查我們與哪個元件發生了碰撞。 這組點是碰撞盒邊緣相交的地方

請注意，如果兩個 PositionComponent 都實作了 onCollision 方法，並且兩個 hitbox 都實作了 onCollision 方法，則會在兩個 PositionComponent 上呼叫 onCollision 方法。 onCollisionStart 和 onCollisionEnd 方法也是如此，當兩個元件和 hitbox 開始或停止相互碰撞時會呼叫這些方法

當 PositionComponent（和 hitbox）開始與另一個 PositionComponent 碰撞時， onCollisionStart 和 onCollision 都會被調用，因此如果您不需要在碰撞開始時執行特定操作，則只需重寫 onCollision ，反之亦然

如果您想檢查與螢幕邊緣的碰撞，就像我們在上面的範例中所做的那樣，您可以使用預先定義的 [ScreenHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#screenhitbox) 類別

預設情況下，所有碰撞箱都是空心的，這意味著一個碰撞箱可以被另一個碰撞箱完全包圍而不會觸發碰撞。 如果你想將碰撞盒設定為實心，你可以設定 **isSolid = true**。 實心碰撞箱內的空心碰撞箱會觸發碰撞，但反之則不然。 如果實體碰撞盒上的邊緣沒有相交，則返回中心位置

## [Collision order](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#collision-order)

如果 Hitbox 在給定時間步內與多個其他 Hitbox 發生碰撞，則 onCollision 回呼將以基本上隨機的順序呼叫。 在某些情況下，這可能是一個問題，例如在彈跳球遊戲中，球的軌跡可能會根據首先擊中的其他物體而有所不同。 為了幫助解決這個問題，可以使用碰撞完成通知監聽器 - 這會在碰撞偵測過程結束時觸發

使用此方法的範例是在 PositionComponent 中加入一個局部變數來保存與其發生碰撞的其他元件：`List<PositionComponent> CollisionComponents = [];`。 然後使用 onCollision 回呼將所有其他 PositionComponent 儲存到此清單中：

```
@override
void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
  collisionComponents.add(other);
  super.onCollision(intersectionPoints, other);
}
```

最後，在 PositionComponent 的 onLoad 方法中新增一個監聽器來呼叫函數，該函數將解決如何處理碰撞：

```
(game as HasCollisionDetection)
    .collisionDetection
    .collisionsCompletedNotifier
    .addListener(() {
  resolveCollisions();
});
```

> 每次呼叫 update 時都需要清除 collisionComponents


# [ShapeHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#shapehitbox)

ShapeHitbox 是普通元件，因此您可以將它們新增至要新增 Hitbox 的元件中，就像任何其他元件一樣：

```
class MyComponent extends PositionComponent {
  @override
  void onLoad() {
    add(RectangleHitbox());
  }
}
```

如果您沒有向 hitbox 添加任何參數（如上面所示），hitbox 將嘗試盡可能多地填充其父級。 除了讓 hitbox 嘗試填充其父級之外，還有兩種方法來啟動 hitbox，即使用普通構造函數，您可以自行定義 hitbox，包括 size 和 position 等。另一種方法是使用 **relative** 構造函數它定義了與其目標父級大小相關的碰撞盒

在某些特定情況下，您可能只想處理 hitbox 之間的碰撞，而不會將 onCollision\* 事件傳播到 hitbox 的父元件。 例如，車輛可以有一個車身碰撞箱來控制碰撞，並有一個側面碰撞箱來檢查左轉或右轉的可能性。 因此，與主體碰撞盒碰撞意味著與組件本身碰撞，而與側面碰撞盒碰撞並不意味著真正的碰撞，不應傳播到碰撞盒的父級。 對於這種情況，您可以將 triggersParentCollision 變數設為 false：

```
class MyComponent extends PositionComponent {

  late final MySpecialHitbox utilityHitbox;

  @override
  void onLoad() {
    utilityHitbox = MySpecialHitbox();
    add(utilityHitbox);
  }

  void update(double dt) {
    if (utilityHitbox.isColliding) {
      // do some specific things if hitbox is colliding
    }
  }
// component's onCollision* functions, ignoring MySpecialHitbox collisions.
}

class MySpecialHitbox extends RectangleHitbox {
  MySpecialHitbox() {
    triggersParentCollision = false;
  }

// hitbox specific onCollision* functions

}
```

您可以在 [ShapeComponents](https://docs.flame-engine.org/1.16.0/flame/components.html#shapecomponents)部分中閱讀有關如何定義不同形狀的更多資訊

請記住，您可以根據需要在 PositionComponent 中添加任意數量的 ShapeHitbox，以構成更複雜的區域。 例如，戴著帽子的雪人可以用三個 CircleHitbox 和兩個 RectangleHitbox 作為其帽子來表示

hitbox 可用於碰撞偵測或使元件頂部的手勢偵測更加準確，請在 [GestureHitboxes](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#gesturehitboxes) mixin 的部分中查看有關後者的更多資訊


## [CollisionType](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#collisiontype)

碰撞盒有一個名為「collisionType」的字段，它定義碰撞盒何時應與另一個碰撞盒發生碰撞。 通常，您希望為 **CollisionType.passive** 設定盡可能多的碰撞箱，以使碰撞偵測更有效率。 預設情況下，CollisionType 處於 **active** 狀態。

CollisionType 枚舉包含以下值：

* **active** 與其 **active/passive** 類型的 Hitbox 發生碰撞
* **passive** 與其 **active** 類型的 Hitbox 發生碰撞
* **inactive** 不會與任何其他 Hitbox 碰撞

因此，如果您有不需要檢查彼此碰撞的碰撞箱，您可以透過在建構函數中設定碰撞類型：**CollisionType.passive** 將它們標記為被動，這可能是地面組件，或者您的敵人可能不需要檢查彼此之間的衝突，那麼它們也可以被標記為 passive。

想像一下，一個遊戲中有很多子彈，它們不能互相碰撞，飛向玩家，那麼玩家將被設定為 **CollisionType.active**，子彈將被設定為 **CollisionType.passive**

然後我們有 **inactive**(非活動)類型，它在碰撞檢測中根本不會被檢查。 例如，如果您在螢幕之外有一些目前不關心的組件，但稍後可能會回來查看，因此它們不會完全從遊戲中刪除，則可以使用此功能。

這些只是如何使用這些類型的範例，它們還會有更多用例，因此即使此處未列出您的用例，也不要懷疑使用它們


## [PolygonHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#polygonhitbox)

需要注意的是，如果要在Polygon上使用碰撞偵測或containsPoint，則多邊形需要是凸的。 因此，請始終使用凸多邊形，否則如果您不真正知道自己在做什麼，您很可能會遇到問題。 還應該注意的是，您應該始終以逆時針順序定義多邊形中的頂點。

其他碰撞盒形狀沒有任何強制構造函數，這是因為它們可以有一個根據它們所附加的可碰撞體的大小計算出的預設值，但由於多邊形可以在邊界內以無限多種方式製作框，您必須在該形狀的建構函式中加入定義。

PolygonHitbox 具有與 [PolygonComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#polygoncomponent) 相同的建構函數，請參閱該部分以取得有關這些內容的文件。


## [RectangleHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#rectanglehitbox)

RectangleHitbox 具有與 [RectangleComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#rectanglecomponent) 相同的建構函數，請參閱該部分以取得這些內容的文件。

## [CircleHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#circlehitbox)

CircleHitbox 具有與 [CircleComponent](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#circlehitbox) 相同的建構函數，請參閱該部分以取得這些內容的文件。

# [ScreenHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#screenhitbox)

ScreenHitbox 是一個代表視窗/螢幕邊緣的元件。 如果您將 ScreenHitbox 添加到遊戲中，則帶有 hitbox 的其他組件在與邊緣碰撞時將收到通知。 它不需要任何參數，僅取決於它所添加到的遊戲的大小。 要新增它，如果您不希望 ScreenHitbox 本身在與某些東西發生碰撞時收到通知，您可以在遊戲中執行 add(ScreenHitbox()) 操作。 由於 ScreenHitbox 具有 CollisionCallbacks mixin，因此您可以根據需要向該物件新增自己的 onCollisionCallback、onStartCollisionCallback 和 onEndCollisionCallback 函數

# [CompositeHitbox](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#compositehitbox)

在 CompositeHitbox 中，您可以新增多個 hitbox，以便它們模擬為一個連接的 hitbox。

例如，如果您想形成一頂帽子，您可能需要使用兩個RectangleHitbox 來正確遵循該帽子的邊緣，那麼您可以將這些碰撞盒添加到此類的實例中，並對整個帽子的碰撞做出反應，而不是僅針對每個碰撞盒分別地處理

# [Broad phase](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#broad-phase)

如果您的遊戲場不大且沒有很多可碰撞組件 - 您不必擔心所使用的 broad phase系統，因此如果標準實現對您來說性能足夠，您可能不需要必須閱讀本節

broad phase 是碰撞檢測的第一步，其中計算潛在的碰撞。 計算這些潛在的碰撞比準確檢查交點要快，並且無需相互檢查所有碰撞盒，從而避免 O(n²)

broad phase 產生一組潛在碰撞（一組 CollisionProspects）。 然後使用該集合來檢查命中框之間的確切交叉點（有時稱為'narrow phase'）

預設情況下，Flame 的碰撞偵測使用掃描和修剪 broadphase 步驟。 如果您的遊戲需要另一種類型的 Broadphase，您可以透過擴充 Broadphase 並手動設定應使用的碰撞偵測系統來編寫自己的 Broadphase。

例如，如果您實作了基於魔術演算法而不是標準掃描和剪枝的 Broadphase，那麼您將執行以下操作：

```
class MyGame extends FlameGame with HasCollisionDetection {
  MyGame() : super() {
    collisionDetection =
        StandardCollisionDetection(broadphase: MagicAlgorithmBroadphase());
  }
}
```

# [Quad Tree broad phase](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#quad-tree-broad-phase)

如果您的遊戲區域很大且遊戲包含大量可碰撞組件（超過一百個），則標準掃描和修剪可能會變得低效。 如果是這樣，您可以嘗試使用四叉樹 broad phase。

為此，請將 HasQuadTreeCollisionDetection mixin 新增至您的遊戲中，而不是 HasCollisionDetection 並在遊戲載入時呼叫initializeCollisionDetection 函數：

```
class MyGame extends FlameGame with HasQuadTreeCollisionDetection {
  @override
  void onLoad() {
    initializeCollisionDetection(
      mapDimensions: const Rect.fromLTWH(0, 0, mapWidth, mapHeight),
      minimumDistance: 10,
    );
  }
}
```

當呼叫initializeCollisionDetection時，您應該向其傳遞正確的地圖尺寸，以使四叉樹演算法正常運作。 還有一些額外的參數可以讓系統更有效率：

* **minimumDistance**: 認為它們可能發生碰撞的物體之間的最小距離。 如果為 null - 檢查被停用，這是預設行為
* **maxObjects**: 一個象限內的最大物體數。 預設為 25。
* **maxDepth**: 象限內的最大嵌套等級。 預設為 10

如果您使用四叉樹系統，則可以透過在元件中實作 CollisionCallbacks mixin 的 onComponentTypeCheck 函數來使其更有效率。 如果您需要防止不同類型的項目發生碰撞，它會很有用。 計算結果被緩存，因此您不應在此處檢查任何動態參數，該函數旨在用作純類型檢查器：

```
class Bullet extends PositionComponent with CollisionCallbacks {

  @override
  bool onComponentTypeCheck(PositionComponent other) {
    if (other is Player || other is Water) {
      // do NOT collide with Player or Water
      return false;
    }
    // Just return true if you're not interested in the parent's type check result.
    // Or call super and you will be able to override the result with the parent's
    // result.
    return super.onComponentTypeCheck(other);
  }

  @override
  void onCollisionStart(
    Set<Vector2> intersectionPoints,
    PositionComponent other,
  ) {
    // Removes the component when it comes in contact with a Brick.
    // Neither Player nor Water would be passed to this function
    // because these classes are filtered out by [onComponentTypeCheck]
    // in an earlier stage.
    if (other is Brick) {
      removeFromParent();
    }
    super.onCollisionStart(intersectionPoints, other);
  }
}
```

經過密集的遊戲後，地圖可能會變得太擁擠，並出現許多空的象限。 運行 QuadTree.optimize() 來清理空象限：

```
class QuadTreeExample extends FlameGame
        with HasQuadTreeCollisionDetection {

  /// A function called when intensive gameplay session is over
  /// It also might be scheduled, but no need to run it on every update.
  /// Use right interval depending on your game circumstances
  onGameIdle() {
    (collisionDetection as QuadTreeCollisionDetection)
            .quadBroadphase
            .tree
            .optimize();
  }
}
```

> 始終嘗試不同的碰撞檢測方法並檢查它們在您的遊戲中的表現。 QuadTreeBroadphase 明顯慢於預設值的情況並非聞所未聞。 不要認為更複雜的方法總是更快

# [Ray casting and Ray tracing](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#ray-casting-and-ray-tracing)

光線投射和光線追蹤是從遊戲中的一個點發出光線並能夠看到這些光線與什麼物體發生碰撞以及它們在撞擊某物後如何反射的方法。

對於以下所有方法，如果您希望忽略任何命中框，則可以新增ignoreHitboxes 參數，該參數是您希望在呼叫時忽略的命中框的清單。 這可能非常有用，例如，如果您從碰撞箱內投射光線，則該碰撞箱可能位於您的玩家或 NPC 上； 或者如果您不希望光線從 ScreenHitbox 反彈。

## [Ray casting](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#ray-casting)

光線投射是從一個點投射出一條或多條光線的操作，看看它們是否會擊中任何物體，在 Flame 的例子中，就是hitboxes。

我們提供了兩種方法來執行此操作：raycast 和 raycastAll。 第一個只是投射出一條光線，並返回一個結果，其中包含有關光線擊中的內容和位置的信息，以及一些額外的信息，例如距離、法線和反射光線。 第二個是 raycastAll，其工作原理類似，但圍繞原點均勻發出多條光線，或在以原點為中心的角度內發出多條光線。

預設情況下，raycast 和 raycastAll 會掃描最近的命中，無論它距離射線原點有多遠。 但在某些用例中，僅在特定範圍內查找命中可能會很有趣。 對於這種情況，可以提供可選的 maxDistance。

若要使用光線投射功能，您的遊戲中必須有 HasCollisionDetection mixin。 新增後，您可以在遊戲類別上呼叫碰撞偵測.raycast(...)。

```
class MyGame extends FlameGame with HasCollisionDetection {
  @override
  void update(double dt) {
    super.update(dt);
    final ray = Ray2(
        origin: Vector2(0, 100),
        direction: Vector2(1, 0),
    );
    final result = collisionDetection.raycast(ray);
  }
}
```

在此範例中，我們可以看到正在使用 Ray2 類，該類別定義了一條來自原點位置和方向（均由 Vector2s 定義）的射線。 該特定射線從 0, 100 開始，直接向右射出一條射線。

如果光線沒有撞擊任何物體，則此操作的結果將為 null，或包含以下內容的 RaycastResult：

* 射線撞擊了哪個 hitbox
* 碰撞交點
* 反射光線，即光線如何反射到它所在的碰撞盒上
* 碰撞的法線，即垂直於碰撞盒表面的向量

如果您擔心效能，可以預先建立一個 RaycastResult 對象，並使用 out 參數將其傳送到方法中，這將使該方法可以重複使用該對象，而不是為每次迭代建立一個新物件。 如果您在更新方法中進行大量光線投射，這可能會很好

## [raycastAll](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#raycastall)

有時您希望從原點向所有方向或有限範圍的方向發出光線。 這可以有很多應用，例如您可以計算玩家或敵人的視野，或者它也可以用於創建光源

Example:

```
class MyGame extends FlameGame with HasCollisionDetection {
  @override
  void update(double dt) {
    super.update(dt);
    final origin = Vector2(200, 200);
    final result = collisionDetection.raycastAll(
      origin,
      numberOfRays: 100,
    );
  }
}
```

在此範例中，我們將從 (200, 200) 發出 100 條光線，均勻地向各個方向傳播。

如果你想限制方向，你可以使用 startAngle 和 swingAngle 參數。 其中 startAngle（從直線向上計數）是光線開始的位置，然後光線將在 startAngle + swingAngle 處結束

如果您擔心效能，可以透過將函數建立的 RaycastResult 物件作為帶有 out 參數的清單傳送來重複使用。

## [Ray tracing](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#ray-tracing)

光線追蹤與光線投射類似，但您不僅可以檢查光線擊中的內容，還可以繼續追蹤光線並查看其反射光線（從命中框彈回的光線）將擊中的內容，然後查看投射的反射光線的反射光線將擊中的內容擊打等等，直到您確定您已經追蹤光線足夠長的時間。 例如，如果您想像撞球如何在撞球桌上彈跳，則可以藉助光線追蹤來檢索該資訊

Example:

```
class MyGame extends FlameGame with HasCollisionDetection {
  @override
  void update(double dt) {
    super.update(dt);
    final ray = Ray2(
        origin: Vector2(0, 100),
        direction: Vector2(1, 1)..normalize()
    );
    final results = collisionDetection.raytrace(
      ray,
      maxDepth: 100,
    );
    for (final result in results) {
      if (result.intersectionPoint.distanceTo(ray.origin) > 300) {
        break;
      }
    }
  }
}
```

在上面的例子中，我們從 (0, 100) 向右對角線發出一條射線，我們希望它在最多 100 個碰撞箱上反彈，它不一定必須得到 100 個結果，因為在某個時刻反射光線可能不會擊中碰撞箱，然後該方法就完成了。

該方法是惰性的，這意味著它只會執行您要求的計算，因此您必須循環遍歷它返回的可迭代物件才能獲取結果，或者執行 toList() 直接計算所有結果

在 for 循環中，可以看到如何使用它，在該循環中，我們檢查當前反射光線交點（前一條光線擊中命中框的位置）距離起始光線原點的距離是否超過 300 像素，如果是的話，我們不關心其餘的結果（然後也不必計算它們）

如果您擔心效能，可以透過將函數建立的 RaycastResult 物件作為帶有 out 參數的清單傳送來重複使用。

# [與 Forge2D 的比較](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#comparison-to-forge2d)

如果想在遊戲中擁有成熟的物理引擎，應該使用 [Forge2D](https://pub.dev/packages/flame_forge2d)。但是如果只需要一個簡單的組件碰撞檢測以及提高手勢的準確性，Flame 內置的碰撞檢測將更加適合

如果有以下需求，應該考慮使用 Forge2D：

* 相互作用的 力學
* 可以與其它物體相互作用的粒子系統
* 身體關節
* 多個實例同時(~50+ 取決於平臺)

如果只需要以下一些東西(因爲不涉及 Forge2D 會稍微簡單一些)，那麼使用 Flame 碰撞檢查系統是個好主意：

* 當你的某些組件發生碰撞時採取行動
* 當你的組件與屏幕邊界碰撞時採取行動
* 複雜的形狀充當組件的碰撞盒，以便手勢更準確
* 可以判斷組件的哪個部分與某物體發生碰撞的 hitbox