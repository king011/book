# [kubectl](https://kubernetes.io/zh/docs/tasks/tools/install-kubectl/)

kubectl 是 k8s 的命令行工具，使用 kubectl 可以對 k8s 集羣運行命令，你可以使用 kubectl 來 部署應用 檢測 和 管理集羣資源 以及查看日誌

```
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```

```
# 創建  bash completion 自動補齊腳本
echo 'source <(kubectl completion bash)' >>~/.bashrc
```