# MTD

MTD 是 Memory Technology Device 的簡寫，是用於處理大多數原始閃存設備(例如 NOR NAND 數據閃存 和 SPI 閃存)的 Linux 子系統名稱。它提供對這些設備的字符和塊訪問，以及許多專門的檔案系統

# 磨損均衡

在處理閃存芯片時，磨損均衡是一個重要的考慮因素。閃存芯片由大量存儲單元或門組成。在這些門中的每一個都被額定爲有限數量的擦除/編程週期。這意味著它們最終會磨損變成“壞塊”。如果沒有磨損均衡一些塊會比其它塊磨損的更快。

MTD 本身不提供磨損均衡，但許多閃存檔案系統提供了磨損均衡

# 用戶空間實用程序

mtd-utils 包提供了許多用戶空間使用程序，用於與 MTD 閃存芯片進行交互。這些使用程序通常在給定 MTD 芯片的字符設備 (/dev/mtdN) 上運行

## flash\_erase

flash\_erase 使用程序用於從給定設備中擦除任意數量的塊。他將定義範圍內的所有存儲單元設置爲擦除值。在大多數類型的閃存中通常爲 0xFF

```
flash_erase MTD-device [start] [cnt (# erase blocks)] [lock]
```
此命令擦除 MTD 設備商店 cnt 個塊，從塊 start 開始


下面的命令從 .dev.mtd1 的第一個塊開始擦除 20個塊
```
flash_erase /dev/mtd1 0 20
```

## flash\_eraseall

flash\_eraseall 用於擦除給定設備上的所有塊。它將所有存儲單元設置爲擦除值，在大多數類型的閃存中通常是 0xFF。它的命令形式如下：

```
Usage: flash_eraseall [OPTION] MTD_DEVICE
Erases all of the specified MTD device.

  -j, --jffs2    format the device for jffs2
  -q, --quiet    don't display progress messages
      --silent   same as --quiet
      --help     display this help and exit
      --version  output version information and exit
```

-j 參數還將爲 JFFS2 檔案系統設置帶外(OOB) 區域。由於 NAND 閃存不使用 JFFS2 的 OOB 數據，因此在該類型的閃存上沒由必要


以下命令將清除 /dev/mtd1 上的所有數據：

```
flash_eraseall /dev/mtd1
```

## flashcp

flashcp 會將檔案寫入給定的閃存設備。與 dd 命令不同，它對於有壞塊的設備是安全的，並提供許多錯誤檢測屬性

```
usage: flashcp [ -v | --verbose ] <filename> <device>
       flashcp -h | --help

   -h | --help      Show this help message
   -v | --verbose   Show progress reports
   <filename>       File which you want to copy to flash
   <device>         Flash device to write to (e.g. /dev/mtd0, /dev/mtd1, etc.)
```

下面命令會將 /sample.img 複製到設備 /dev/mtd1:

```
flashcp /sample.img /dev/mtd1
```

## nandwrite

nandwrite專爲將檔案寫入 NAND 閃存而設計。它提供了許多專門適用與 NAND 的附加選項

> 注意：大多時候，flashcp足於將圖像寫入 NAND。它幾乎總是“做正確的事”。如果你需要確保以非常特定的方式寫入信息，例如對閃存檔案系統遺留的支持，或者某個應用程序正在以非常特定的方式讀取閃存，則你應該只需要使用此實用程序

```
Usage: nandwrite [OPTION] MTD_DEVICE [INPUTFILE|-]
Writes to the specified MTD device.

  -a, --autoplace         Use auto oob layout
  -j, --jffs2             Force jffs2 oob layout (legacy support)
  -y, --yaffs             Force yaffs oob layout (legacy support)
  -f, --forcelegacy       Force legacy support on autoplacement-enabled mtd device
  -m, --markbad           Mark blocks bad if write fails
  -n, --noecc             Write without ecc
  -o, --oob               Image contains oob data
  -s addr, --start=addr   Set start address (default is 0)
  -p, --pad               Pad to page size
  -b, --blockalign=1|2|4  Set multiple of eraseblocks to align to
  -q, --quiet             Don't display progress messages
      --help              Display this help and exit
      --version           Output version information and exit
```

下面命令會將 /sample.img 複製到設備 /dev/mtd1:
```
nandwrite /dev/mtd1 /sample.img
```

# 構建閃存設備

在開發與測試期間，可以使用 linux 的 NAND 模擬器來構建一個閃存設備，該模塊可讓你重新利用部分 RAM，就好像它是閃存設備一樣。

NAND 模擬器可以通過將 nandsim 模塊加載到內核來激活。例如使用 root 執行下面的命令:

```
sudo modprobe nandsim first_id_byte=0x01 \
                        second_id_byte=0xf1 \
                        third_id_byte=0x80 \
                        fourth_id_byte=0x1d \
                        parts=4,32,816,4,160,8
```

如果失敗可以執行 `dmesg` 命令查看錯誤原因

模擬器根據設備 id 字節確定要模擬的內容。這些字節描述了設備的製造商 特性 擦除塊大小 和 總內存大小。這些值應該從閃存數據表中獲得。在例子中，我們正在模擬 Cypress (AKA Spansion) S43ML01G2 1Gbit NAND 閃存，具有 128KB 擦除塊。 這為我們提供了 128MB 的工作空間。 這些 id 字節在傳遞給 nandsim 的 xxx_id_byte 參數中可以看到。

parts 參數告訴模擬器將設備劃分爲多個 /dev/mtdN 設備。使用真正的閃存設備，此分區數據將在啓動時傳遞給 linux 內核。對閃存設備進行分區有助於隔離設備的不同部分，從而最大限度的降低風險。parts 參數列舉了每個分區的大小。如果還有剩餘空間，則將這些塊分配給最終分區。例子中將得到如下分區:

| MTD | function | Size(Eraseblocks) | Size |
| -------- | -------- | -------- | -------- |
| mtd0     | System Boot     | 4     | 512KB     |
| mtd1     | Linux Kernel     | 32     | 4MB     |
| mtd2     | Linux Root/ROM Filesystem     | 816     | 102MB     |
| mtd3     | Recovery Bootloader     | 4     | 512KB     |
| mtd4     | Recovery     | 160     | 20MB     |
| mtd5     | NVRAM/Settings     | 8     | 1MB     |


加載模擬器(或真正的閃存驅動程序)後，我們可以讀取 /proc/mtd 以查看 MTD 狀態

```
$ cat /proc/mtd
dev:    size   erasesize  name
mtd0: 00080000 00020000 "NAND simulator partition 0"
mtd1: 00400000 00020000 "NAND simulator partition 1"
mtd2: 06600000 00020000 "NAND simulator partition 2"
mtd3: 00080000 00020000 "NAND simulator partition 3"
mtd4: 01400000 00020000 "NAND simulator partition 4"
mtd5: 00100000 00020000 "NAND simulator partition 5"
```


| 字段 | 含義 | 例子 |
| -------- | -------- | -------- |
| dev     | 設備     | mtd0  的字符串設備路徑是 /dev/mtd0     |
| size     | 設備空間大小(十六進制表示的 bytes)     | 00080000 表示 512KB     |
| erasesize     | 擦除塊大小(十六進制表示的 bytes)     | 00020000 表示 128kb     |
| name     | 人類友好的設備名稱     | NAND simulator partition 0     |


# c example

```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int example_mtd(int fd)
{
    // mtd_info_t 記錄了一些 設備相關的信息
    mtd_info_t mtd_info;
    if (ioctl(fd, MEMGETINFO, &mtd_info) == -1)
    {
        printf("ioctl MEMGETINFO: %s\n", strerror(errno));
        return -1;
    }
    printf("MTD Type: %x\nMTD total size: %x bytes\nMTD erase size: %x bytes\n",
           mtd_info.type, mtd_info.size, mtd_info.erasesize);

    // 擦除數據，每次寫入數據前需要對塊進行擦除否則可能會寫入錯誤的數據
    erase_info_t ei;
    ei.length = mtd_info.erasesize; // set the erase block size
    for (ei.start = 0; ei.start < mtd_info.size; ei.start += ei.length)
    {
        if (ioctl(fd, MEMERASE, &ei) == -1)
        {
            printf("ioctl MEMERASE: %s\n", strerror(errno));
            return -1;
        }
    }

    // 檢測壞塊，擦除數據後塊內容應該全部是 0xff 通過檢測是否全 0xff 來判斷是否是一個損壞的塊
    if (lseek(fd, 0, SEEK_SET) == -1)
    {
        printf("lseek: %s\n", strerror(errno));
        return -1;
    }
    char *block = malloc(ei.length * 2);
    memset(block + ei.length, 0xff, ei.length);
    for (ei.start = 0; ei.start < mtd_info.size; ei.start += ei.length)
    {
        if (read(fd, block, ei.length) == -1)
        {
            printf("read: %s\n", strerror(errno));
            return -1;
        }
        if (memcmp(block, block + ei.length, ei.length))
        {
            printf("BAD block %x %x\n", ei.start, ei.length);
            return -1;
        }
    }

    // 寫入數據
    if (lseek(fd, 0, SEEK_SET) == -1)
    {
        printf("lseek: %s\n", strerror(errno));
        return -1;
    }
    const char *str = "cerberus is an idea";
    memcpy(block, str, strlen(str));
    if (write(fd, block, ei.length) == -1) // 必須寫入一個完整的塊
    {
        printf("write: %s\n", strerror(errno));
        return -1;
    }

    // 讀取數據
    if (lseek(fd, 0, SEEK_SET) == -1)
    {
        printf("lseek: %s\n", strerror(errno));
        return -1;
    }
    ssize_t n = read(fd, block + ei.length, strlen(str));
    if (n == -1)
    {
        printf("read: %s\n", strerror(errno));
        return -1;
    }
    printf("read: %.*s\n", n, block + ei.length);
    /* code */
    return 0;
}

int main(int argc, char *argv[])
{
    // 打開設備進行讀寫
    int fd = open("/dev/mtd0", O_RDWR);
    if (fd == -1)
    {
        puts(strerror(errno));
        return -1;
    }
    int ok = example_mtd(fd);

    // 關閉設備
    close(fd);
    return ok;
}
```
