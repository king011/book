# PrimeFlex

PrimeFlex 是一個開源的(MIT) CSS 實用庫

* 官網 [https://www.primefaces.org/primeflex/](https://www.primefaces.org/primeflex/)
* 源碼 [https://github.com/primefaces/primeflex](https://github.com/primefaces/primeflex)

此系列文章主要是參考 PrimeFlex 官網的英文教學寫的類翻譯資料(有點像翻譯，因爲和英文資料內容差不多，但並不是語言的翻譯本喵只是用中文記錄了自己的理解故語意並不和官網英文資料完全一致)

因爲 PrimeFlex 內容太多，爲了方便查看，此頁面集合只包含了 **安裝** 和 **佈局系統** 相關的內容其它內容你可以訪問下述 鏈接獲取

* [外觀](web-css-PrimeFlex-look/0)
* [特效](web-css-PrimeFlex-special/0)