# net/http2

```bash
go get -u -v golang.org/x/net/http2
```

golang.org/x/net/http2 庫實現了 h2

http2 提供了很多優化效率的 特性 二進制的header 多路復用 socket \.\.\.

* 通常 加密傳輸的 http2 稱為 h2
* 非加密 傳輸 稱為 h2c

# Example

```go
#info="main.go"
package main

import "flag"

func main() {
	var help, h2c, client bool
	var address string
	var certFile, keyFile string
	flag.BoolVar(&help, `help`, false, `display help`)
	flag.BoolVar(&h2c, `h2c`, false, `use h2c mode`)
	flag.BoolVar(&client, `client`, false, `if true run as client,else run as server`)
	flag.StringVar(&address, `address`, `:3016`, `listen address or dial address`)
	flag.StringVar(&certFile, `cert`, `test.pem`, `h2 cert file`)
	flag.StringVar(&keyFile, `key`, `test.key`, `h2 key file`)
	flag.Parse()
	if help {
		flag.PrintDefaults()
		return
	}
	if client {
		runClient(h2c, address)
	} else {
		runServer(h2c, address, certFile, keyFile)
	}
}
```

```
#info="server.go"
package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

func runServer(useH2C bool, address, certFile, keyFile string) {
	// 創建 路由
	mux := http.NewServeMux()
	mux.HandleFunc(`/`, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "other rs")
	})
	mux.HandleFunc(`/1`, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "1 rs")
	})
	mux.HandleFunc(`/2`, func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(time.Second)
		fmt.Fprint(w, "2 rs")
	})

	// 配置 服務器
	httpServer := http.Server{
		Addr: address,
	}
	var http2Server http2.Server
	if useH2C {
		httpServer.Handler = h2c.NewHandler(mux, &http2Server)
	} else {
		httpServer.Handler = mux
	}
	e := http2.ConfigureServer(&httpServer, &http2Server)
	if e != nil {
		log.Fatalln(e)
	}

	// 運行 服務
	if useH2C {
		log.Println("h2c work at", address)
		e = httpServer.ListenAndServe()
	} else {
		log.Println("h2 work at", address)
		e = httpServer.ListenAndServeTLS(certFile, keyFile)
	}
	if e != nil {
		log.Fatalln(e)
	}
}
```

```
#info="client.go"
package main

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"

	"golang.org/x/net/http2"
)

func runClient(h2c bool, address string) {
	var transport http.RoundTripper
	if h2c {
		transport = &http2.Transport{
			DialTLS: func(network, addr string, cfg *tls.Config) (net.Conn, error) {
				fmt.Println("只會被調用一次 DialTLS 因為同個服務器 後面的 請求都會 復用")
				return net.Dial(network, addr)
			},
			AllowHTTP: true,
		}
	} else {
		transport = &http2.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}
	}

	//創建 http2 客戶端
	client := http.Client{
		Transport: transport,
	}
	//多路復用 發送 多個 請求 只會使用 同個 socket
	ch := make(chan (int))
	go getURL(ch, &client, h2c, address, "1")
	go getURL(ch, &client, h2c, address, "2")
	sum := 0
	for sum < 2 {
		<-ch
		sum++
	}
}
func getURL(ch chan (int), client *http.Client, h2c bool, address, u string) {
	var url string
	if h2c {
		url = fmt.Sprintf(`http://%s/%s`, address, u)
	} else {
		url = fmt.Sprintf(`https://%s/%s`, address, u)
	}

	resp, err := client.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(body), body)
	if u == "1" {
		getURL(ch, client, h2c, address, "3")
	} else {
		ch <- 1
	}
}
```
