# [Native 支付](https://pay.weixin.qq.com/doc/v3/merchant/4012791874)

Native 支付是大部分網站會使用的支付方式，它會生成一個二維碼供用戶使用微信掃碼進行支付。支付金額由商家預先設置用戶不可修改，很適合用於在線銷售商品或服務

[接入前的準備](https://pay.weixin.qq.com/doc/v3/merchant/4012791875) 是官方指引，需要商戶管理員按照指引在騰訊官網上啓用相應服務並將各種加密密鑰下載來說提供給程序使用

# [下單](https://pay.weixin.qq.com/doc/v3/merchant/4012791877)

支付的第一步是創建一個訂單，依據官網填充參加發送請求即可，比較關鍵的參數是

* **out\_trade\_no** 商户订单号，需要保證每個訂單唯一，用於標識這個訂單
* **time\_expire** 訂單失效時間，(目前 2025-01-13 如果低於當前時間，不是返回錯誤而是創建或刷新一個無過期時間的訂單，這顯然是給bug fucking)
* **notify\_url** 通知 url，微信會將支付信息異步通知到這個 url

如果成功 接口會返回 200 狀態碼和付款 url，將它生成 二維碼 供微信用戶掃碼支付

```
{
  "code_url" : "weixin://wxpay/bizpayurl/up?pr=NwY5Mz9&groupid=00"
}
```

> code\_url 有效期爲2小時，過期後可以使用相同的參數繼續調用來生成新的二維碼

```
package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	client, e := getClient()
	if e != nil {
		log.Fatalln(`get client fail:`, e)
	}

	// 創建一個支付訂單
	// https://pay.weixin.qq.com/doc/v3/merchant/4012791877
	result, e := client.Post(context.Background(),
		BaseURL+`/v3/pay/transactions/native`,
		map[string]any{
			//  string(32) 商戶關聯的 appid
			`appid`: Appid,
			//  string(32) 商戶號
			`mchid`: MchID,
			// string(127) 商品描述
			`description`: `微信支付接口測試`,
			// string(32) 商戶內部訂單號 6到32 字符, 同一商戶下必須唯一, 只能由 數組字母和 _-|* 四個符號組成
			`out_trade_no`: OutTradeNo,
			// 訂單金額
			`amount`: map[string]any{
				// 總金額 單位:分
				`total`: 1,
				// 可選的貨幣類型 目前只能是 CNY 表示人民幣
				`currency`: `CNY`,
			},

			// string(64) 可選的支付截止時間
			`time_expire`: time.Now().Add(time.Hour).Format(time.RFC3339),
			// string(128) 可選的附加數據 用戶不會看到 會在查詢api和支付回調中返回給商戶
			`attach`: `cerberus is an idea`,
			// string(255) 支付成功的通知 URL
			`notify_url`: NotifyURL,
			// string(32) 可選的優惠價代碼
			`goods_tag`: `cerberus`,
			// boolean 如果爲 true 在支付詳情頁面提供開電子發票功能 需要商戶在平臺開頭電子發票功能
			`support_fapiao`: false,
			// // 優惠功能 請查看官方文檔
			// `detail`: map[string]any{},
			// // 場景信息 請查看官方文檔
			// `scene_info`: map[string]any{},
			// // 結算信息 請查看官方文檔
			// `settle_info`: map[string]any{},
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	resp := result.Response
	defer resp.Body.Close()
	fmt.Println(resp.StatusCode, resp.Status)
	for k, v := range resp.Header {
		fmt.Println(k, v)
	}
	b, e := io.ReadAll(resp.Body)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`-------------------`)
	fmt.Println(string(b))
}
```

# [支付成功回調](https://pay.weixin.qq.com/doc/v3/merchant/4012791882)

當用戶付款成功後，微信會向創建訂單時 **notify\_url** 指定的網頁發送一個 POST 請求，商戶在得到此請求後應該首先驗證請求來自微信(通過簽名)，然後通知前端客戶端用戶付款進行付款後的邏輯。
1. 這個通知不是100%保證收到的，但大部分時候可以收到並且微信在通知失敗後會進行嘗試新通知，直到這個接口返回 http 200，或達到騰訊的通知闊值
2. 這個通知有可能會重複收到，商戶需要自己處理，避免重複爲用戶發貨

body 是類似下述 json

```
{
    "id": "EV-2018022511223320873",
    "create_time": "2015-05-20T13:29:35+08:00",
    "resource_type": "encrypt-resource",
    "event_type": "TRANSACTION.SUCCESS",
    "summary": "支付成功",
    "resource": {
        "original_type": "transaction",
        "algorithm": "AEAD_AES_256_GCM",
        "ciphertext": "",
        "associated_data": "",
        "nonce": ""
    }
}
```

對於支付成功通知來說 **event\_type** 固定爲 **TRANSACTION.SUCCESS**

**resource.ciphertext** 是使用 AES-256-GCM 加密的數據

```
package main

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

func (s *Server) wxpay(w http.ResponseWriter, r *http.Request) {
	b, e := io.ReadAll(io.LimitReader(r.Body, 1024*1024))
	if e != nil {
		log.Fatalln(e)
	}
	var o struct {
		Resource struct {
			Ciphertext     string `json:"ciphertext"`
			AssociatedData string `json:"associated_data"`
			Nonce          string `json:"nonce"`
		} `json:"resource"`
	}
	e = json.Unmarshal(b, &o)
	if e != nil {
		log.Fatalln(e)
	}

	// 獲取密文
	ciphertext, e := base64.StdEncoding.DecodeString(o.Resource.Ciphertext)
	if e != nil {
		log.Fatalln(e)
	}

	// 創建 gcm 解密工廠
	block, e := aes.NewCipher([]byte(V3Key))
	if e != nil {
		log.Fatalln(e)
	}
	gcm, e := cipher.NewGCM(block)
	if e != nil {
		log.Fatalln(e)
	}

	// 解密
	dst, e := gcm.Open(nil, []byte(o.Resource.Nonce), ciphertext, []byte(o.Resource.AssociatedData))
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(string(dst))
}
```

## [驗證簽名](https://pay.weixin.qq.com/doc/v3/merchant/4013053249#4.-%E9%AA%8C%E8%AF%81%E7%AD%BE%E5%90%8D)

POST 過來的 header 會包含下述4個請求頭:

* **Wechatpay-Serial**: 微信支付的公鑰 id
* **Wechatpay-Signature**: 簽名
* **Wechatpay-Timestamp**: 簽名時間戳
* **Wechatpay-Nonce**: 簽名用到的隨機字符串
* **Wechatpay-Signature-Type**: 簽名算法，可能不存在則爲默認值 **WECHATPAY2-SHA256-RSA2048**

```
package main

import (
	"bytes"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/wechatpay-apiv3/wechatpay-go/utils"
)

func (s *Server) wxpay(w http.ResponseWriter, r *http.Request) {
	header := r.Header
	var content bytes.Buffer

	// 構造被簽名的內容
	content.WriteString(header.Get(`Wechatpay-Timestamp`) + "\n" +
		header.Get(`Wechatpay-Nonce`) + "\n")
	_, e := io.Copy(&content, io.LimitReader(r.Body, 1024*1024))
	if e != nil {
		log.Fatalln(e)
	}
	content.WriteString("\n")

	// 獲取簽名
	signature := header.Get(`Wechatpay-Signature`)
	sig, e := base64.StdEncoding.DecodeString(signature)
	if e != nil {
		log.Fatalln(e)
	}

	pub, e := utils.LoadPublicKeyWithPath(PublicKey)
	if e != nil {
		log.Fatalln(e)
	}
	r.Header.Get(`Wechatpay-Timestamp`)

	// 驗證簽名
	hashed := sha256.Sum256(content.Bytes())
	e = rsa.VerifyPKCS1v15(pub, crypto.SHA256, hashed[:], sig)
	if e != nil {
		log.Fatalln(e)
	}

	// 執行驗簽成功邏輯
	fmt.Println(`Verify ok`)
}
```

# [使用商戶訂單號進行查詢](https://pay.weixin.qq.com/doc/v3/merchant/4012791880)

可以通過商戶訂單號 查詢訂單的支付等狀態，如果成功會返回 200 狀態碼和下述 json 響應
```
{
  // 創建訂單時使用的 appid
  "appid" : "wxd678efh567hg6787",
  // 商戶訂單號
  "mchid" : "1230000109",
  // 訂單號
  "out_trade_no" : "1217752501201407033233368018",
  "transaction_id" : "1217752501201407033233368018",
  "trade_type" : "APP",
  // 支付狀態
  "trade_state" : "SUCCESS",
  // 支付狀態的中文描述
  "trade_state_desc" : "支付成功",
  "bank_type" : "CMC",
  "attach" : "自定义数据",
  "success_time" : "2018-06-08T10:34:56+08:00",
  "payer" : {
    "openid" : "oUpF8uMuAJO_M2pxb1Q9zNjWeS6o\t"
  },
  "amount" : {
    "total" : 100,
    "payer_total" : 90,
    "currency" : "CNY",
    "payer_currency" : "CNY"
  },
  "scene_info" : {
    "device_id" : "013467007045764"
  },
  "promotion_detail" : [
    {
      "coupon_id" : "109519",
      "name" : "单品惠-6",
      "scope" : "SINGLE",
      "type" : "CASH",
      "amount" : 10,
      "stock_id" : "931386",
      "wechatpay_contribute" : 0,
      "merchant_contribute" : 10,
      "other_contribute" : 0,
      "currency" : "CNY",
      "goods_detail" : [
        {
          "goods_id" : "M1006",
          "quantity" : 1,
          "unit_price" : 100,
          "discount_amount" : 10,
          "goods_remark" : "商品备注信息"
        }
      ]
    }
  ]
}
```

其中最重要的就是 **trade\_state** 記錄了支付狀態，可能是如下值：
* **SUCCESS**：支付成功
* **REFUND**：转入退款
* **NOTPAY**：未支付
* **CLOSED**：已关闭
* **REVOKED**：已撤销（仅付款码支付会返回）
* **USERPAYING**：用户支付中（仅付款码支付会返回）
* **PAYERROR**：支付失败（仅付款码支付会返回）


```
package main

import (
	"context"
	"fmt"
	"io"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	client, e := getClient()
	if e != nil {
		log.Fatalln(`get client fail:`, e)
	}

	// 使用商戶訂單號 查詢訂單
	// https://pay.weixin.qq.com/doc/v3/merchant/4012791880
	result, e := client.Get(context.Background(),
		BaseURL+`/v3/pay/transactions/out-trade-no/`+OutTradeNo+
			// 必須將商戶號 作爲查詢參數
			`?mchid=`+MchID,
	)
	if e != nil {
		log.Fatalln(e)
	}
	resp := result.Response
	defer resp.Body.Close()
	fmt.Println(resp.StatusCode, resp.Status)
	for k, v := range resp.Header {
		fmt.Println(k, v)
	}
	b, e := io.ReadAll(resp.Body)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`-------------------`)
	fmt.Println(string(b))
}
```

# [關閉訂單](https://pay.weixin.qq.com/doc/v3/merchant/4012791881)

未支付的訂單可以調用關閉接口 關閉訂單 這通常發生在
* 用戶在商戶系統中關閉了訂單
* 訂單超時，商戶需要關閉訂單

如果接口成功會返回 204 狀態碼，並且不包含任何響應

> 對於不存在的訂單可以直接調用此接口進行關閉，wxpay 也會返回 204並創建一個被關閉的記錄，這簡化了創建訂單失敗時的情況，商戶可以直接請求wxpay 進行關閉而不需要清除訂單是否創建成功

```
package main

import (
	"context"
	"fmt"
	"io"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	client, e := getClient()
	if e != nil {
		log.Fatalln(`get client fail:`, e)
	}

	// 使用商戶訂單號 關閉未支付的訂單
	// https://pay.weixin.qq.com/doc/v3/merchant/4012791881
	result, e := client.Post(context.Background(),
		BaseURL+`/v3/pay/transactions/out-trade-no/`+OutTradeNo+`/close`,
		map[string]any{
			"mchid": MchID,
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	resp := result.Response
	defer resp.Body.Close()
	fmt.Println(resp.StatusCode, resp.Status)
	for k, v := range resp.Header {
		fmt.Println(k, v)
	}
	b, e := io.ReadAll(resp.Body)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`-------------------`)
	fmt.Println(string(b))

}
```

# [退款](https://pay.weixin.qq.com/doc/v3/merchant/4012791883)

在付款一年內，商戶可以通過調用此接口進行退款

```
package main

import (
	"context"
	_ "crypto/rsa"
	"fmt"
	"io"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	client, e := getClient()
	if e != nil {
		log.Fatalln(`get client fail:`, e)
	}

	// 申請退款
	// https://pay.weixin.qq.com/doc/v3/merchant/4012791883
	result, e := client.Post(context.Background(),
		BaseURL+`/v3/refund/domestic/refunds`,
		map[string]any{
			// string(32) 商戶內部訂單號
			`out_trade_no`: OutTradeNo,
			// string(64) 商戶內部退款單號 必須唯一
			`out_refund_no`: `refund_` + OutTradeNo,
			// string(80) 顯示給客戶的的退款原因
			`reason`: `退款測試`,
			// string(256) 退款通知 URL
			`notify_url`: NotifyURL,
			// 可選的退款金額信息
			`amount`: map[string]any{
				// 原訂單金額
				`total`: 1,
				// 必須爲 CNY
				`currency`: `CNY`,
				// 要退多少錢
				`refund`: 1,
			},
		},
	)
	if e != nil {
		log.Fatalln(e)
	}
	resp := result.Response
	defer resp.Body.Close()
	fmt.Println(resp.StatusCode, resp.Status)
	for k, v := range resp.Header {
		fmt.Println(k, v)
	}
	b, e := io.ReadAll(resp.Body)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`-------------------`)
	fmt.Println(string(b))
}
```

成功時會返回 200和退款信息

```
{
  "refund_id" : "50000000382019052709732678859",
  "out_refund_no" : "1217752501201407033233368018",
  "transaction_id" : "1217752501201407033233368018",
  "out_trade_no" : "1217752501201407033233368018",
  "channel" : "ORIGINAL",
  "user_received_account" : "招商银行信用卡0403",
  "success_time" : "2020-12-01T16:18:12+08:00",
  "create_time" : "2020-12-01T16:18:12+08:00",
  "status" : "SUCCESS",
  "funds_account" : "UNSETTLED",
  "amount" : {
    "total" : 100,
    "refund" : 100,
    "from" : [
      {
        "account" : "AVAILABLE",
        "amount" : 444
      }
    ],
    "payer_total" : 90,
    "payer_refund" : 90,
    "settlement_refund" : 100,
    "settlement_total" : 100,
    "discount_refund" : 10,
    "currency" : "CNY",
    "refund_fee" : 100
  },
  "promotion_detail" : [
    {
      "promotion_id" : "109519",
      "scope" : "GLOBAL",
      "type" : "COUPON",
      "amount" : 5,
      "refund_amount" : 100,
      "goods_detail" : [
        {
          "merchant_goods_id" : "1217752501201407033233368018",
          "wechatpay_goods_id" : "1001",
          "goods_name" : "iPhone6s 16G",
          "unit_price" : 528800,
          "refund_amount" : 528800,
          "refund_quantity" : 1
        }
      ]
    }
  ]
}
```

# [退款成功通知](https://pay.weixin.qq.com/doc/v3/merchant/4012791886)

類似與支付通知，只是它最終解密會得到下述數據

```
{
    "mchid": "1900000100",
    "transaction_id": "1008450740201411110005820873",
    "out_trade_no": "20150806125346",
    "refund_id": "50200207182018070300011301001",
    "out_refund_no": "7752501201407033233368018",
    "refund_status": "SUCCESS",
    "success_time": "2018-06-08T10:34:56+08:00",
    "user_received_account": "招商银行信用卡0403",
    "amount" : {
        "total": 999,
        "refund": 999,
        "payer_total": 999,
        "payer_refund": 999
    }
}
```

# [查看退款詳情](https://pay.weixin.qq.com/doc/v3/merchant/4012791884)

你可以調用此接口來查看退款詳情

```
{
  "refund_id" : "50000000382019052709732678859",
  "out_refund_no" : "1217752501201407033233368018",
  "transaction_id" : "1217752501201407033233368018",
  "out_trade_no" : "1217752501201407033233368018",
  "channel" : "ORIGINAL",
  "user_received_account" : "招商银行信用卡0403",
  "success_time" : "2020-12-01T16:18:12+08:00",
  "create_time" : "2020-12-01T16:18:12+08:00",
  "status" : "SUCCESS",
  "funds_account" : "UNSETTLED",
  "amount" : {
    "total" : 100,
    "refund" : 100,
    "from" : [
      {
        "account" : "AVAILABLE",
        "amount" : 444
      }
    ],
    "payer_total" : 90,
    "payer_refund" : 90,
    "settlement_refund" : 100,
    "settlement_total" : 100,
    "discount_refund" : 10,
    "currency" : "CNY",
    "refund_fee" : 100
  },
  "promotion_detail" : [
    {
      "promotion_id" : "109519",
      "scope" : "GLOBAL",
      "type" : "COUPON",
      "amount" : 5,
      "refund_amount" : 100,
      "goods_detail" : [
        {
          "merchant_goods_id" : "1217752501201407033233368018",
          "wechatpay_goods_id" : "1001",
          "goods_name" : "iPhone6s 16G",
          "unit_price" : 528800,
          "refund_amount" : 528800,
          "refund_quantity" : 1
        }
      ]
    }
  ]
}
```