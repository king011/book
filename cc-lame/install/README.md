# install

1. sudo apt install libmp3lame-dev
2. #include &lt;lame/lame\.h&gt;
3. libmp3lame.so

# 解碼

```
#include <iostream>
#include <lame/lame.h>
#define BUF_SIZE 512
#define INBUF_SIZE 4096
#define MP3BUF_SIZE (int)(1.25 * BUF_SIZE) + 7200
class decode_t
{
private:
    hip_t gfp_;

public:
    decode_t()
    {
        // 初始化 解碼器
        gfp_ = hip_decode_init();
        if (!gfp_)
        {
            throw std::string("hip_decode_init failed");
        }
    }
    ~decode_t()
    {
        // 關閉解碼器 釋放資源
        hip_decode_exit(gfp_);
    }

    void done(const char *input)
    {
        FILE *input_file = fopen(input, "rb");
        if (!input_file)
        {
            throw std::string("open input file error");
        }

        short pcm_l[INBUF_SIZE];
        short pcm_r[INBUF_SIZE];
        int mp3_bytes;
        unsigned char mp3_buf[MP3BUF_SIZE];
        mp3data_struct mp3data;
        int total = 0;
        double duration = 0;
        while ((mp3_bytes = fread(mp3_buf, 1, 210, input_file)) > 0)
        {
            int nout = hip_decode_headers(gfp_, mp3_buf, 210, pcm_l, pcm_r, &mp3data);
            if (nout < 0)
            {
                throw std::string("hip_decode error");
            }
            total += nout;
            if (nout > 0)
            {
                // 達到一幀
                // 時長 = 數據長度 / 採樣率
                duration += double(total) / double(mp3data.samplerate);
            }
            // else
            // {
            //     // 未滿一幀 等待數據
            //     continue;
            // }
        }
        std::cout << duration << "\n";
    }
};

int main(int argc, char *argv[])
{
    try
    {
        decode_t decode;
        // 解碼
        decode.done("/home/king/a.mp3");
    }
    catch (const std::string &e)
    {
        std::cout << "error : " << e << std::endl;
    }
    return 0;
}
```