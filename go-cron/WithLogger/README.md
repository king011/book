# 日誌

可以使用 WithLogger 將任務調度打印出來

```
package main

import (
	"fmt"
	"log"
	"os"

	"github.com/robfig/cron/v3"
)

func main() {
	c := cron.New(
		cron.WithLogger(
			cron.VerbosePrintfLogger(log.New(os.Stdout, "[cron] ", log.LstdFlags)),
		),
	)
	c.AddFunc("@every 1s", func() {
		fmt.Println("hello world")
	})
	c.Start()

	select {}
}
```