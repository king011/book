# event\_base

```
#include <event2/event.h>
```

event\_base 是一個不透明的 struct，每個程序都至少需要一個 event\_base。event\_base 與所有你關心的事件關聯，並在事件變得活躍時通知你。

你可以使用 [event\_base\_new()](https://libevent.org/doc/event_8h.html#af34c025430d445427a2a5661082405c3) 或 [event\_base\_new\_with\_config\(cfg\)](https://libevent.org/doc/event_8h.html#a864798e5b3c6aa8d2f9e9c5035a3e6da) 創建一個 event\_base，如果函數成功會返回一個實例指針，否則將返回 NULL 指針

當你不在需要 event\_base 時，調用 [event\_base\_free(eb)](https://libevent.org/doc/event_8h.html#a91f9deb12c37f2ff586e65e8b9a46641) 來 event\_base，注意這個函數不會釋放任何 fd 句柄以及 傳遞給 event\_new 的內存

# 事件循環

你可以調用 [event\_base\_dispatch(eb)](https://libevent.org/doc/event_8h.html#a19d60cb72a1af398247f40e92cf07056) 來進入事件循環，它會一直執行直到沒有事件或者 [event\_base\_loopbreak()](https://libevent.org/doc/event_8h.html#a28e0be91488e8a0ed57a582f3343eaf6) 或 [event\_base\_loopexit()](https://libevent.org/doc/event_8h.html#a67ba4db4b6e7467b3412219530603e60) 被調用

它的返回值有三種可能:
* **-1** 發生了錯誤
* **0** 成功
* **1** 沒有事件

你也可以調用 [event\_base\_loop(eb, flags)](https://libevent.org/doc/event_8h.html#acd7da32086d1e37008e7c76c9ea54fc4) 來進入事件循環，它多接收一個 flags 參數用於進行一些行爲定義，flags 可以是下列值的任意組合
* EVLOOP\_ONCE 阻塞直到有一個活動事件，然後在所有活動事件都運行完回調後退出
* EVLOOP\_NONBLOCK 不要阻塞：查看哪些事件現在準備就緒，運行最高優先級事件的回調，然後退出。
* EVLOOP\_NO\_EXIT\_ON\_EMPTY 不要退出循環，因為我們有未決事件(可能有來自其它線程的事件)

# event\_new

你可以使用 [event\_new(eb, fd, events, callback, callback_arg )](https://libevent.org/doc/event_8h.html#a6cd300e5cd15601c5e4570b221b20397) 來創建一個關心的時間，如果失敗將返回 NULL，否則你需要在不再使用時調用 [event\_free(ev)](https://libevent.org/doc/event_8h.html#a1f326019b65d319b2221b7e57d56c273) 來釋放內存

1. 如果 events 包含 EV\_READ EV\_WRITE，則 fd 是一個文件描述符或套接字，應監視其是否 準備好讀取 準備寫
2. 如果 events 包含 EV\_SIGNAL，則 fd 是要等待的信號編號
3. 如果 events 不包含上述 1 2 中的值則只能通過 超時 或使用 [event\_active(ev, res, ncall)](https://libevent.org/doc/event_8h.html#a31d9440cf2b7010e32a05a2142a91525) 手動激活來觸發事件，在這種情況下，fd 必須為 -1
4. 你可以在 events 中設置 EV\_PERSIST 這使用 event\_add 添加的事件變得持久化直到調用 event\_del 爲止
5. EV\_ET 是與 EV\_READ EV\_WRITE 兼容的標記，用於告訴 Libevent 使用 edge-triggered(邊緣觸發) 模式，某些後端並不支持，默認使用 level-triggered(水平觸發)
6. EV\_TIMEOUT 這個標記沒有作用，只是可以在使用定時器時設置上讓代碼看起來更人類友好
7. 可以讓多個事件關聯同一個 fd，但它們不能混用 edge-triggered level-triggered 模式，既必須全是 edge-triggered 模式 或全是 level-triggered 模式
8. 當事件激活時會自動調用回調函數 callback(fd, events, callback\_arg)，events 可能被設置上 EV\_READ EV\_WRITE EV\_SIGNAL，EV\_TIMEOUT 表示超時， EV\_ET 表示發生邊緣觸發通知
> edge-triggered 當條件發生變化時，觸發一次條件沒有變化不會重複觸發，level-triggered 當滿足條件時一直觸發，以 socket 讀取爲例，網路發來 100 字節，兩種觸發方式都會觸發回調，但只讀取了50字節，edge-triggered 不會繼續觸發因爲可讀狀態沒有變化之前已經觸發過通知而 level-triggered 將一直觸發通知直到沒有數據可讀。理論上 edge-triggered 更高效但編程複雜度更高

## event\_base\_once

[event\_base\_once(eb, fd, events, callback, callback\_arg, tv)](https://libevent.org/doc/event_8h.html#ae4d40e572ad8310897ce06f9fe0f9062) 是一個語法糖，它將在內部使用 event\_new 準備一個一次性事件，然後使用 event\_add 來監聽這個事件，並在事件回調完成後自動釋放 event\_new 創建的事件

如果函數成功返回0，否則返回-1，對於只需要處理一次的事件，使用這個函數會比較容易書寫。但是注意 events 只能是 EV\_READ or EV\_WRITE or EV\_TIMEOUT

> 在 2.0 以及更早的版本中這個函數存在bug，如果事件在  event\_base\_free 調用前都未觸發過，則event\_new 申請的內存將永久泄漏。2.1 修復來這個bug，event\_base\_free 會爲未觸發且由 event\_base\_once 創建的事件進行清理

## event\_active

[event\_active(ev, res, ncall)](https://libevent.org/doc/event_8h.html#a31d9440cf2b7010e32a05a2142a91525) 用於手動觸發某個事件，這對於自定義事件系統很有幫助，

* **ev** 是要 event\_new 創建的事件 
* **res** 是傳遞給回調函數的第二個參數(events)，指示是發生來哪個關心的事件
* **ncall** 是歷史遺留的一個無效參數，固定傳入 0 即可

## event\_assign

[event\_assign(ev, fd, events, callback, callback\_arg, tv)](https://libevent.org/doc/event_8h.html#a3e49a8172e00ae82959dfe64684eda11) 類似 event\_new 用於初始化事件，但它不自動申請內存，而是由調用者傳入已申請好的內存，可以用它來做 events 池之類的優化，但是 struct event 是一個不透明結構並且其大小可能會變化如果直接使用臨時變量或者 sizeof 它可能會和動態連接的未來版本不兼容，正確做法是調用 [event\_get\_struct\_event\_size()](https://libevent.org/doc/event_8h.html#a0f93db0db7e74f9ae173e1766415dd56) 函數來返回 struct event 的大小

# event\_add
[event\_add(ev, tv)](https://libevent.org/doc/event_8h.html#ab0c85ebe9cf057be1aa17724c701b0c8) 向 event\_base 添加一個關心的事件，tv 是一個可選的超時時間，如果爲NULL則永不超時。如果成功返回 0，否則返回 -1

# event\_del
[event\_del(ev)](https://libevent.org/doc/event_8h.html#a2ffc10a652c67bea16b0a71f7d5a44dc) 用於從 event\_base 中刪除不再關心的事件，如果成功返回 0 否則返回 -1


# Example

```
#include <event2/event.h>
#include <stdio.h>

void timeout_handler(evutil_socket_t fd, short events, void *args)
{
    printf("timeout_handler %d %d\n", events, (int)(uintptr_t)args);
}
int main(int argc, char *argv[])
{
    int ret = 0;
    struct event_base *eb = NULL;
    struct event *ev = NULL;

    eb = event_base_new();
    if (!eb)
    {
        puts("event_base_new error");
        goto END;
    }
    {
        struct timeval tv = {
            .tv_sec = 1,
            .tv_usec = 0,
        };
        ev = event_new(eb, -1, EV_TIMEOUT | EV_PERSIST, timeout_handler, (void *)1);
        if (!ev)
        {
            puts("event_new error");
            goto END;
        }
        event_add(ev, &tv);

        ret = event_base_dispatch(eb);
        printf("%d\n", ret);
    }
END:
    if (ev)
    {
        event_free(ev);
    }
    if (eb)
    {
        event_base_free(eb);
    }
    return ret;
}
```
