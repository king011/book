# [debug](https://docs.flame-engine.org/1.16.0/flame/other/debug.html)

Flame 為 FlameGame 類別提供了一些偵錯功能。 當 debugMode 屬性設為 true（或覆蓋為 true）時，將啟用這些功能。 當啟用 debugMode 時，每個 PositionComponent 將以其邊界大小進行渲染，並將其位置寫入螢幕上。 這樣，您可以直觀地驗證組件的邊界和位置。

若要查看 FlameGame 偵錯功能的工作範例，請查看此[範例](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/components/debug_example.dart)。

# [FPS](https://docs.flame-engine.org/1.16.0/flame/other/debug.html#fps)

Flame 報告的 FPS 可能會比 Flutter DevTools 等報告的稍低，具體取決於您的目標平台。 您的遊戲運行的 FPS 的真實來源應該是我們報告的 FPS，因為這是我們的遊戲循環所受的約束

## [FpsComponent](https://docs.flame-engine.org/1.16.0/flame/other/debug.html#fpscomponent)

FpsComponent 可以新增到元件樹中的任何位置，並將追蹤遊戲目前渲染的 FPS。如果您想在遊戲中將其顯示為文本，請使用 FpsTextComponent。

## [FpsTextComponent](https://docs.flame-engine.org/1.16.0/flame/other/debug.html#fpstextcomponent)

FpsTextComponent 只是一個包裝 FpsComponent 的 TextComponent，因為您通常希望在使用 FpsComponent 時在某處顯示當前 FPS

## [ChildCounterComponent](https://docs.flame-engine.org/1.16.0/flame/other/debug.html#childcountercomponent)

ChildCounterComponent 是一個每秒從元件（目標）渲染類型 T 的子級數量的元件。 例如：

例如，以下程式碼將渲染遊戲世界子級 SpriteAnimationComponent 的數量：

```
add(
  ChildCounterComponent<SpriteAnimationComponent>(
    target: world,
  ),
);
```

## [TimeTrackComponent](https://docs.flame-engine.org/1.16.0/flame/other/debug.html#timetrackcomponent)

該組件允許開發人員追蹤其程式碼中花費的時間。 這對於在程式碼的某些部分花費的效能偵錯時間非常有用。

要使用它，請將其添加到遊戲中的某個位置（因為這是一個調試功能，我們建議僅在調試版本/風格中添加該組件）：

```
add(TimeTrackComponent());
```

然後在要追蹤時間的程式碼部分中，執行以下操作：

```
void update(double dt) {
  TimeTrackComponent.start('MyComponent.update');
  // ...
  TimeTrackComponent.end('MyComponent.update');
}
```

透過上面的調用，新增的 TimeTrackComponent 將以微秒為單位呈現經過的時間。