# ubuntu

```
sudo apt update
sudo apt install xmake
```

如果 ubuntu 版本較舊可能還沒有收錄 xmake，可以添加 xmake-io 官方源

```
sudo add-apt-repository ppa:xmake-io/xmake
sudo apt update
sudo apt install xmake
```
