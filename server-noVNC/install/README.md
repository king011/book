# install

1. 下載源碼並解壓
2. 使用靜態服務器將源碼中的 html 以 web 服務提供
3. 訪問 vnc.html 使用 noVNC

```
server {
    listen 8000;
    location / {
        root /home/king/noVNC-1.2.0;
    }
}
```