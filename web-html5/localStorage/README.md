# localStorage

localStorage 是html5提供的 key/val 資料庫 用於 存儲前端本地數據 通常默認最大尺寸爲 5m

* localStorage 通常可以存儲至少5m數據但 以瀏覽器實現不同 大小限制可能由差異
* localStorage 存儲的內容 不會過期 除非 調用api手動清除
* localStorage 的 key/val 目前都只支持 字符串 不要存儲其它型別 否則不同瀏覽器 行爲可能不一致

```ts
// 設置 數據 
localStorage.setItem("name", "kate")
// 返回 數據
console.log(localStorage.getItem("name"))
// 返回已經存在的 key
for (let i = 0; i < localStorage.length; i++) {
	console.log(i, localStorage.key(i))
}

// 移除數據
localStorage.removeItem("name")
// 清空 資料庫
localStorage.clear()
```

# sessionStorage

sessionStorage api 和限制 類似 localStorage 唯一區別是 sessionStorage 只在當前頁面有效 頁面消失 會自動清空

