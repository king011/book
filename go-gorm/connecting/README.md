# 連接數據庫

gorm 官方支持的數據庫有

* MySQL
* PostgreSQL
* SQlite
* SQL Server

# MySql

```
import (
  "gorm.io/driver/mysql"
  "gorm.io/gorm"
)

func main() {
  // 參考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 獲取詳情
  dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
  db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
}
```

> 想要正確處理 time.Time 需要帶上 parseTime 參數， ([更多參數](https://github.com/go-sql-driver/mysql#parameters))要支持完整的 UTF-8編碼 需要 將 charset=utf8 更改爲 charset=utf8mb4 查看 [此文章](https://mathiasbynens.be/notes/mysql-utf8mb4) 獲取詳情

MySql 驅動 提供列一些 高級配置 可在初始化過程中 使用,例如

```
db, err := gorm.Open(mysql.New(mysql.Config{
  DSN: "gorm:gorm@tcp(127.0.0.1:3306)/gorm?charset=utf8&parseTime=True&loc=Local", // DSN data source name
  DefaultStringSize: 256, // string 型別 默認長度
  DisableDatetimePrecision: true, // 禁用 datetime 精度，MySQL 5.6 之前的數據庫不支持
  DontSupportRenameIndex: true, // 重命名索引時採用刪除並新建的方式，MySQL 5.7 之前的數據庫和 MariaDB 不支持重命名索引
  DontSupportRenameColumn: true, // 用 `change` 重命名列時，MySQL 8 之前的數據庫和 MariaDB 不支持重命名列
  SkipInitializeWithVersion: false, // 根據當前 MySQL 版本自動配置
}), &gorm.Config{})
```

## 自定義驅動

gorm 允許通過 DriverName 選項 自定義 MySql 驅動

```
import (
  _ "example.com/my_mysql_driver"
  "gorm.io/gorm"
)

db, err := gorm.Open(mysql.New(mysql.Config{
  DriverName: "my_mysql_driver",
  DSN: "gorm:gorm@tcp(localhost:9910)/gorm?charset=utf8&parseTime=True&loc=Local", // Data Source Name，參考 https://github.com/go-sql-driver/mysql#dsn-data-source-name
}), &gorm.Config{})
```

## 現有數據庫連接

gorm 允許通過一個現有數據庫連接來初始化

```
import (
  "database/sql"
  "gorm.io/gorm"
)

sqlDB, err := sql.Open("mysql", "mydb_dsn")
gormDB, err := gorm.Open(mysql.New(mysql.Config{
  Conn: sqlDB,
}), &gorm.Config{})
```

# PostgreSQL

```
import (
  "gorm.io/driver/postgres"
  "gorm.io/gorm"
)

dsn := "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai"
db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
```
gorm 使用 [pgx](https://github.com/jackc/pgx) 作爲 postgres 的驅動 默認情況下會啓用 prepared statement  可以這樣禁用它：
```
// https://github.com/go-gorm/postgres
db, err := gorm.Open(postgres.New(postgres.Config{
  DSN: "user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai",
  PreferSimpleProtocol: true, // disables implicit prepared statement usage
}), &gorm.Config{})
```

## 自定義驅動
```
import (
  _ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
  "gorm.io/gorm"
)

db, err := gorm.Open(postgres.New(postgres.Config{
  DriverName: "cloudsqlpostgres",
  DSN: "host=project:region:instance user=postgres dbname=postgres password=password sslmode=disable",
})
```
## 現有數據庫連接

```
import (
  "database/sql"
  "gorm.io/gorm"
)

sqlDB, err := sql.Open("postgres", "mydb_dsn")
gormDB, err := gorm.Open(postgres.New(postgres.Config{
  Conn: sqlDB,
}), &gorm.Config{})
```

# SQLite

```
import (
  "gorm.io/driver/sqlite"
  "gorm.io/gorm"
)

// github.com/mattn/go-sqlite3
db, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
```

> 可以使用 file::memory:?cache=shared 替代檔案路徑。這會告訴 SQLite在系統內存中使用一個臨時數據庫。(查看 [SQLite 文檔](https://www.sqlite.org/inmemorydb.html) 獲取詳情)

# SQL Server
```
import (
  "gorm.io/driver/sqlserver"
  "gorm.io/gorm"
)

// github.com/denisenkom/go-mssqldb
dsn := "sqlserver://gorm:LoremIpsum86@localhost:9930?database=gorm"
db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
```

# Clickhouse
[https://github.com/go-gorm/clickhouse](https://github.com/go-gorm/clickhouse)
```
import (
  "gorm.io/driver/clickhouse"
  "gorm.io/gorm"
)

func main() {
  dsn := "tcp://localhost:9000?database=gorm&username=gorm&password=gorm&read_timeout=10&write_timeout=20"
  db, err := gorm.Open(clickhouse.Open(dsn), &gorm.Config{})

  // Auto Migrate
  db.AutoMigrate(&User{})
  // Set table options
  db.Set("gorm:table_options", "ENGINE=Distributed(cluster, default, hits)").AutoMigrate(&User{})

  // 插入
  db.Create(&user)

  // 查詢
  db.Find(&user, "id = ?", 10)

  // 批量插入
  var users = []User{user1, user2, user3}
  db.Create(&users)
  // ...
}
```

# 連接池
```
sqlDB, err := db.DB()

// SetMaxIdleConns 設置空閒連接池中連接最大數量
sqlDB.SetMaxIdleConns(10)

// SetMaxOpenConns 設置打開數據庫連接最大數量
sqlDB.SetMaxOpenConns(100)

// SetConnMaxLifetime 設置連接可復用的最大時間
sqlDB.SetConnMaxLifetime(time.Hour)
```