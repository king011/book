# 有序集合

* sorted set 是 string 類型的 集合 並且不允許重複成員
* 不同的每個元素都會關聯一個 double 的 分數，redis 通過分數來爲集合成員按照從小到大排序
* 有序集合成員是唯一的，但分數可以重複
* 集合通過 hash map 實現，所以添加 刪除 查找複雜度都是 O(1)。集合中最大成員數爲 pow(2,32-1) 個 (4294967295,超過40億)

| 命令 | 含義 |
| -------- | -------- |
| ZADD key score1 member1 [score2 member2]     | 向集合添加一個或多個成員，或者更新已存在成員的分數     |
| ZCARD key     | 獲取有序集合的成員數     |
| ZCOUNT key min max     | 計算在有序集合中指定區間分數的成員數     |
| ZINCRBY key increment member     | 爲集合指定成員的分數加上 increment    |
| ZINTERSTORE destination numkeys key [key ...]     | 計算給定的一個或多個有序集的交集並將結果集存儲在新的有序集合 key 中     |
| ZLEXCOUNT key min max     | 在有序集合中計算指定字典區間內成員數量     |
| ZRANGE key start stop [WITHSCORES]     | 通過索引區間返回有序集合指定區間內的成員     |
| ZRANGEBYLEX key min max [LIMIT offset count]     | 通過字典區間返回有序集合的成員     |
| ZRANGEBYSCORE key min max [WITHSCORES] [LIMIT]     | 通過分數返回有序集合指定區間內的成員     |
| ZRANK key member     | 返回有序集合中指定成員的索引     |
| ZREM key member [member ...]     | 移除集合成員     |
| ZREMRANGEBYLEX key min max     | 移除集合中給定的字典區間的所有成員     |
| ZREMRANGEBYRANK key start stop     | 移除集合中給定排名區間的所有成員     |
| ZREMRANGEBYSCORE key min max     | 移除有序集合中給定的分數區間的所有成員     |
| ZREVRANGE key start stop [WITHSCORES]     | 返回有序集中指定區間內成員，通過索引，分數從高到低     |
| ZREVRANGEBYSCORE key max min [WITHSCORES]     | 返回有序集中指定分數區間內的成員，分數從高到低     |
| ZREVRANK key member     | 返回有序集合中指定成員的排名，有序集合成員按分數值遞減排序     |
| ZSCORE key member     | 返回有序集合中，成員分數值     |
| ZUNIONSTORE destination numkeys key [key ...]    | 所有給定集合的並集存儲到 destination 集合中     |
| 	ZSCAN key cursor [MATCH pattern] [COUNT count]     | 迭代集合中的元素     |


