# LSPatch

* 源碼 [https://github.com/LSPosed/LSPatch](https://github.com/LSPosed/LSPatch)

LSPatch 是一個無需 root 的 Xposed 框架，它由 LSPosed 的作者派生創建

你可以在 [https://modules.lsposed.org/](https://modules.lsposed.org/) 去搜尋可用到 Xposed 模塊


## [企微小打手](https://github.com/Xposed-Modules-Repo/com.rong862.fqywx)

一個 Xposed 插件，用於企業微信打卡時虛擬定位