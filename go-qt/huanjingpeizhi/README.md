# linux

1. 安裝最新的 qt c++ 環境

1. 設置 qt 版本 信息 詳見 [https://github.com/therecipe/qt/wiki/Installation](https://github.com/therecipe/qt/wiki/Installation)

   ```sh
   export QT_VERSION=5.9.3
   export QT_DIR=/opt/Qt
   export QT_QMAKE_DIR=/opt/Qt/5.9.3/gcc_64/bin
   ```

1. 獲取 therecipe/qt 源碼 和 工具

   ```sh
   #info=false
   go get -v -u github.com/therecipe/qt/cmd/...
   ```
	 
1. 設置 cgo 環境變量

   ```sh
	 #info=false
   export CGO_CXXFLAGS_ALLOW=".*" 
   export CGO_LDFLAGS_ALLOW=".*" 
   export CGO_CFLAGS_ALLOW=".*" 
   ```

1. 安裝依賴工具

   ```sh
	 #info={"noline":true,"name":"Debian/Ubuntu (apt-get)"}
   sudo apt-get -y install build-essential libglu1-mesa-dev libpulse-dev libglib2.0-dev
   ```
	 
   ```sh
   #info={"noline":true,"name":"Fedora/RHEL/CentOS (yum)"}
   sudo yum -y groupinstall "C Development Tools and Libraries"
   sudo yum -y install mesa-libGLU-devel gstreamer-plugins-base pulseaudio-libs-devel glib2-devel
   ```

3. 部署 qt go 環境

   ```sh
   #info=false
   qtsetup
   ```
	 
# linux for android
1. 設置環境變量 ANDROID\_SDK\_DIR

   下載 [sdk tools](https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip) 解壓到 ANDROID\_SDK\_DIR 中
	 
1. 安裝依賴

  ```sh
	#info=false
	$ANDROID_SDK_DIR/tools/bin/sdkmanager "platform-tools" "build-tools;28.0.2" "platforms;android-28"
	```
	
1. 設置 環境變量 ANDROID\_NDK\_DIR

   下載 [ndk](https://dl.google.com/android/repository/android-ndk-r14b-linux-x86_64.zip) 解壓到 ANDROID\_NDK\_DIR 中

1. 設置 環境變量 JDK\_DIR

  下載 [jdk](https://download.java.net/java/jdk8u192/archive/b04/binaries/jdk-8u192-ea-bin-b04-linux-x64-01_aug_2018.tar.gz) 並且解壓到JDK_DIR中

1. 安裝好 qt android 環境

1. 設置 qt 版本 信息 詳見 [https://github.com/therecipe/qt/wiki/Installation](https://github.com/therecipe/qt/wiki/Installation)

   ```sh
   export QT_VERSION=5.9.3
   export QT_DIR=/opt/Qt
   export QT_QMAKE_DIR=/opt/Qt/5.9.3/gcc_64/bin
   ```
	 
	 