# 記錄器

記錄器 定義了 如何 記錄 日誌 zap 提供了 多種 獲取 記錄器的 方法

```txt
#info=false
// 返回 全局的 記錄器
func L() *Logger

// 創建一個 無操作 的 記錄器 此記錄器 不會進行 任何操作 失敗 調用 用戶 鉤子
func NewNop() *Logger

// 最靈活的 記錄器 創建 方法 如果 core 爲 nil 則 相當於 NewNop
func New(core zapcore.Core, options ...Option) *Logger

// 創建一個 用於 開發 便於人類觀看的 的 記錄器 輸出到 stderr
// NewDevelopmentConfig().Build(...Option) 的語法糖
func NewDevelopment(options ...Option) (*Logger, error)

// 創建一個 用於 生產的 將info以上日誌以 json 輸出到 stderr 的 記錄器
// NewProductionConfig().Build(...Option) 的語法糖
func NewProduction(options ...Option) (*Logger, error)
```

# 記錄到檔案

配合 gopkg.in/natefinch/lumberjack.v2 可以輕易的 將 日誌記錄到 檔案

```go
package main

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
	"runtime"
)

// 實現一個 zapcore.WriteSyncer 接口 將 數據同時 寫入 檔案和 控制檯
type _WriteSyncer struct {
	ws zapcore.WriteSyncer
}

func (w _WriteSyncer) Write(b []byte) (n int, e error) {
	os.Stderr.Write(b)
	n, e = w.ws.Write(b)
	return
}
func (w _WriteSyncer) Sync() (e error) {
	os.Stderr.Sync()
	e = w.ws.Sync()
	return
}

// CallerEncoder 爲 caller 增加 函數名稱 默認只有檔案名行號
//
// filename:lineNum:funcName
func CallerEncoder(caller zapcore.EntryCaller, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(caller.TrimmedPath() + ":" + runtime.FuncForPC(caller.PC).Name())
}

func main() {
	// 創建 日誌 輸出 檔案
	w := zapcore.AddSync(&lumberjack.Logger{
		Filename:   "/home/king/project/go/src/test/zap/log/a.log",
		MaxSize:    500, // megabytes
		MaxBackups: 3,
		MaxAge:     28, // days
	})
	// 創建 core
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeCaller = CallerEncoder
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		//zapcore.NewConsoleEncoder(encoderCfg),
		_WriteSyncer{w}, //合併 日誌 到 控制檯
		zap.InfoLevel,
	)

	// 創建 記錄器
	logger := zap.New(core,
		zap.AddCaller(), // 需要 輸出 代碼 位置
	)

	defer logger.Sync()
	// 寫入 日誌
	for index := 0; index < 10000; index++ {
		logger.Info("file test",
			zap.Int("index", index),
		)
	}
}
```