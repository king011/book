# 基本原理

android 4.0 (API LEVEL 15)開始 提供了 VpnService 來實現 本地 vpn

android 會 創建 一個 tun0 設置 並且通過iptables使用NAT將 所有 數據 發送到 tun0 vpn 可在此 對數據包 進行 修改

如果 vpn 要使用 socket 必須 先 明確的將 socket 綁定到 真實 網路設備上去 才不會被 傳入 tun0

# VpnService
1. 從 VpnService 派生 實現一個 vpn 服務

1. 需要在 AndroidManifest.xml 中 爲 服務 顯示 聲明 使用 android.permission.BIND\_VPN\_SERVICE 權限

	```
	<?xml version="1.0" encoding="utf-8"?>
	<manifest xmlns:android="http://schemas.android.com/apk/res/android"
			package="com.king011.testvpn">
			<application
					android:allowBackup="true"
					android:icon="@mipmap/ic_launcher"
					android:label="@string/app_name"
					android:roundIcon="@mipmap/ic_launcher_round"
					android:supportsRtl="true"
					android:theme="@style/AppTheme">
					<service
							android:name=".Socks5Service"
							android:enabled="true"
							android:exported="true"
							android:permission="android.permission.BIND_VPN_SERVICE"></service>
					<activity android:name=".MainActivity">
							<intent-filter>
									<action android:name="android.intent.action.MAIN" />
									<category android:name="android.intent.category.LAUNCHER" />
							</intent-filter>
					</activity>
			</application>
	</manifest>
	```

1. 調用 VpnService.prepare 函數 prepare 會檢查系統是否已經存在一個 本程式建立的 vpn 如果 存在 返回 null 否則 返回一個 Intent 啓動此 Intent 會彈出一個 用戶授權框 如果 用戶 確認 則會 爲 本程式 創建 vpn 連接

	```
	package com.king011.testvpn;

	import android.content.Intent;
	import android.net.VpnService;
	import android.support.v7.app.AppCompatActivity;
	import android.os.Bundle;
	import android.util.Log;
	import android.view.View;

	public class MainActivity extends AppCompatActivity {
			static public final String TAG = "MainActivity";
			@Override
			protected void onCreate(Bundle savedInstanceState) {
					super.onCreate(savedInstanceState);
					setContentView(R.layout.activity_main);
			}
			public void onStart(View view){
					Intent intent = VpnService.prepare(this);
					if (intent != null) {
							startActivityForResult(intent, 0);
					} else {
							onActivityResult(0, RESULT_OK, null);
					}
			}
			protected void onActivityResult(int request, int result, Intent data) {
					if (result == RESULT_OK) {
							// 啓動 vpn 服務
							Intent intent = new Intent(this, Socks5Service.class);
							startService(intent);
					}
			}
			public void onStop(View view){
					Intent intent = new Intent(this, Socks5Service.class);
					stopService(intent);
			}
	}
	```