# [重要的類](https://docs.unity3d.com/Manual/ScriptingImportantClasses.html)

在編寫腳本時 unity 提供了幾個常用且重要的內置類，它們爲腳本編寫提供了基礎的支持

* GameObject
* MonoBehaviour
* Object
* Transform
* Vectors
* Quaternion
* ScriptableObject 可用於保存大量數據的數據容器
* Time
* Mathf
* Random
* Debug
* Gizmos 和 Handles