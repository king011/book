# 代理網關

在 linux 下很容易利用 docker 來創建一個代理網關，這樣的好處是 docker 內的網路設置不會影響到宿主機

下文都假設網卡設備名稱是 **enp3s0** 網關地址 192.168.251.1/24

首先必須打開網卡的混雜模式(重启后会失效，可以将命令添加到 **/etc/rc.local**)

```
ip link set enp3s0 promisc on
```

使用下述命令查看是否打開混雜模式(存在 PROMISC 字符串)

```
ip link |egrep enp3s0 | egrep PROMISC
```

然後創建一個 macvlan 網路，macvlan 網路會創建一個虛擬網卡，局域網內的其它設備和網關會把它當作一個真實的物理網卡。然後創建一個使用此網路的容器來運行代理環境即可(這裏的容器 選擇了 [xray-webui](https://github.com/zuiwuchang/xray-webui))

```
networks:
  vlan:
    driver: macvlan
    driver_opts:
      parent: enp3s0
    ipam:
      config:
        - subnet: "192.168.251.0/24"
          gateway: "192.168.251.1"
services:
  xray:
    image: king011/xray-webui:v0.0.4
    restart: always
    cap_add:
      - NET_ADMIN
      - NET_RAW
    volumes:
      - ./conf/xray.json:/own/xray.json:ro
      - /opt/xray-webui/xray:/opt/xray-webui/xray:ro
    environment:
      - XRAY_IMPORT=/own/xray.json
      - XRAY_ADDR=:80
    networks:
      vlan:
        ipv4_address: 192.168.251.20
```

最後局域網內的設備將網關設置爲容器ip(這裏的例子是 192.168.251.20),即可通過容器的透明代理訪問網路

# 注意

macvlan 可能無法在無線網卡中工作，因爲 IEEE 802.11 不允許。 IEEE 802.11 不喜歡單一客戶端上有多個 MAC 位址。macvlan 子介面可能會被您的無線介面驅動程式、AP 或兩者阻止