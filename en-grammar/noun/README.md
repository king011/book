# 名詞
nuon 簡寫 n 通常用作

* 主詞
* 補語
* 受詞

通常 人 事 地 物的名字用 名稱表示

# 名詞種類
* 普通名詞 => book penal dog spaceship
* 集合名詞 => class family audience
* 專有名詞 => 人名 地名 月份
    *  Bob, Smith, April, Lond... 
    * 開頭字母都用大寫
    * 前面不能加 冠詞 a/an 因為沒有單複數
    * the Unite States, the United Nations
    * 一些特殊專有名詞(組織名 國家名) 前面加上 The 
* 物質名詞 
    * glass, wood, paper, butter, fruit
    * 通常是一種材質材料 沒有固定形狀
    * 物質名詞不能有 單複數的表現 通常加上 s 會變成新的意思 glasses(眼鏡) woods(森林) papers(報紙)
    * a loaf of bread, loafs of bread
    * a cup of coffee, two cups of coffee
    * a sheet of paper, two sheets of paper
    * a spoonful of sugar(一勺糖). spoon(勺子) 通常可以 名詞加上ful 變成一個度量詞
    * 使用 `數字 + 容器(度量) + of + 物質名詞` 表示物質名詞的數量
* 抽象名詞
    * beauty honesty love patience
    * happinese music
    * 通常看不見 摸不到的 也無法計量 
# 名詞複數
可數名詞都有複數形式
## 規則變化
複數形式 都是以s結尾的
* 名詞字尾直接加**s** : dog/dogs book/books girl/girls
* 名詞以 s sh ch x o 結尾 加 es : class/classes bus/buses dish/dishes bench/benches box/boxes
* 名字以 子音+o結尾 加 es 但 發s但音 : tomatoes 但有例外(photos pianos)
* 名詞以 子音+y結尾 將y改為 ies : baby/babies story/stories city/cities lady/ladies
* 名詞以 f或fe結尾 將f/fe 改為 ves : leaf/leaves wife/wives knife/knives 例外 handkerchiefs chiefs roofs

## 不規則變化
* 結尾加en ren : ox/oxen child/children
* 改變母音 : man/men woman/women goose/geese tooth/teeth mouse/mice
* 單 複數 同形 : fish deer sheep Chinese Japanese
# 名詞所有格






















