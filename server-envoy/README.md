# envoy

envoy 一個 開源(Apache-2.0) 使用c++實現的 專爲大型現代SOA(面向服務) 架構設計的L7代理 和 通信總線

* 官網 [https://www.envoyproxy.io/](https://www.envoyproxy.io/)
* 源碼 [https://github.com/envoyproxy/envoy](https://github.com/envoyproxy/envoy)
* 文檔 [https://www.envoyproxy.io/docs/envoy/latest/](https://www.envoyproxy.io/docs/envoy/latest/)
* 一份網友維護的中文入門文檔 [https://icloudnative.io/envoy-handbook/docs/practice/migrating-from-nginx-to-envoy/](https://icloudnative.io/envoy-handbook/docs/practice/migrating-from-nginx-to-envoy/)
* [root](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto.html)
