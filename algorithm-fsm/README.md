# 有限狀態機

有限狀態機即 FSM 是 finite state machine 的簡寫，用於表示有限個狀態以及在這些狀態之間的轉義和動作等行爲的數學模型

# 特點

1. 在一個時刻，只會有一個狀態
2. 某種條件(事件)下，會總一種狀態轉變(transition)到另外一種狀態
3. 狀態有限(finite)

# 要素

有限狀態機有4個要素

1. 現態，指當前所處的狀態
2. 條件/事件，當，一個條件被滿足，將會觸發一個動作，或者執行一次狀態的遷移
3. 動作，條件滿足後執行的動作。動作執行完畢後，可以遷移到新的狀態，也可以依舊保持原狀態。動作不是必須的，當條件滿足後，也可以不執行任何動作，直接遷移到新狀態
4. 次態，條件滿足後要遷往的新狀態。次態是相對與現態而言的，次態一旦被激活，就轉變成新的現態了。