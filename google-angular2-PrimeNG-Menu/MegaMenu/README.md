# [MegaMenu](https://primeng.org/megamenu)

```
import {MegaMenuModule} from 'primeng/megamenu';
import { MegaMenuItem } from 'primeng/api';
```

MegaMenu 會將子孫菜單一起顯示

![](assets/0.png)

```
<p-megaMenu [model]="items"></p-megaMenu>
```

```
import {MegaMenuItem,MenuItem} from 'primeng/api';

export class MegaMenuDemo {

    items: MegaMenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'Videos', icon: 'pi pi-fw pi-video',
                items: [
                    [
                        {
                            label: 'Video 1',
                            items: [{label: 'Video 1.1'}, {label: 'Video 1.2'}]
                        },
                        {
                            label: 'Video 2',
                            items: [{label: 'Video 2.1'}, {label: 'Video 2.2'}]
                        }
                    ],
                    [
                        {
                            label: 'Video 3',
                            items: [{label: 'Video 3.1'}, {label: 'Video 3.2'}]
                        },
                        {
                            label: 'Video 4',
                            items: [{label: 'Video 4.1'}, {label: 'Video 4.2'}]
                        }
                    ]
                ]
            },
            {
                label: 'Users', icon: 'pi pi-fw pi-users',
                items: [
                    [
                        {
                            label: 'User 1',
                            items: [{label: 'User 1.1'}, {label: 'User 1.2'}]
                        },
                        {
                            label: 'User 2',
                            items: [{label: 'User 2.1'}, {label: 'User 2.2'}]
                        },
                    ],
                    [
                        {
                            label: 'User 3',
                            items: [{label: 'User 3.1'}, {label: 'User 3.2'}]
                        },
                        {
                            label: 'User 4',
                            items: [{label: 'User 4.1'}, {label: 'User 4.2'}]
                        }
                    ],
                    [
                        {
                            label: 'User 5',
                            items: [{label: 'User 5.1'}, {label: 'User 5.2'}]
                        },
                        {
                            label: 'User 6',
                            items: [{label: 'User 6.1'}, {label: 'User 6.2'}]
                        }
                    ]
                ]
            },
            {
                label: 'Events', icon: 'pi pi-fw pi-calendar',
                items: [
                    [
                        {
                            label: 'Event 1',
                            items: [{label: 'Event 1.1'}, {label: 'Event 1.2'}]
                        },
                        {
                            label: 'Event 2',
                            items: [{label: 'Event 2.1'}, {label: 'Event 2.2'}]
                        }
                    ],
                    [
                        {
                            label: 'Event 3',
                            items: [{label: 'Event 3.1'}, {label: 'Event 3.2'}]
                        },
                        {
                            label: 'Event 4',
                            items: [{label: 'Event 4.1'}, {label: 'Event 4.2'}]
                        }
                    ]
                ]
            },
            {
                label: 'Settings', icon: 'pi pi-fw pi-cog',
                items: [
                    [
                        {
                            label: 'Setting 1',
                            items: [{label: 'Setting 1.1'}, {label: 'Setting 1.2'}]
                        },
                        {
                            label: 'Setting 2',
                            items: [{label: 'Setting 2.1'}, {label: 'Setting 2.2'}]
                        },
                        {
                            label: 'Setting 3',
                            items: [{label: 'Setting 3.1'}, {label: 'Setting 3.2'}]
                        }
                    ],
                    [
                        {
                            label: 'Technology 4',
                            items: [{label: 'Setting 4.1'}, {label: 'Setting 4.2'}]
                        }
                    ]
                ]
            }
        ]
    }
}
```

## Custom Content

自定義內容可以放在 **p-megaMenu** 標籤之間。MegaMenu 對於自定義內容應該是水平的

```
<p-megaMenu [model]="items">
    <input type="text" pInputText placeholder="Search">
    <button pButton label="Logout" icon="pi pi-sign-out"></button>
</p-megaMenu>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| orientation     | string     | horizontal     | 定義方向: horizontal vertical     |
| style     | object     | null     | 組件的內聯 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| baseZIndex     | number     | 0     | 分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |

# Templates

| Name | Parameters |
| -------- | -------- |
| start     | -     |
| end     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-megamenu     | Container element.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
| p-submenu-icon     | Arrow icon of a submenu.     |

