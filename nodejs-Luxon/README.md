# Luxon

Luxon 是一個開源(MIT) 的 js 時間日期處理庫

* 官網 [https://moment.github.io/luxon/#/?id=luxon-3x](https://moment.github.io/luxon/#/?id=luxon-3x)
* 源碼 [https://github.com/moment/luxon/](https://github.com/moment/luxon/)


```
npm install --save luxon
npm install --save-dev @types/luxon
```

or

```
yarn add luxon
yarn add --dev @types/luxon
```