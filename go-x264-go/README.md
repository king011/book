# x264-go

x264-go 是一個 cgo 實現的 h264 編碼器

以後端使用 c 代碼的 [x264](https://www.videolan.org/developers/x264.html)

* 源碼 [github.com/gen2brain/x264-go](https://github.com/gen2brain/x264-go)