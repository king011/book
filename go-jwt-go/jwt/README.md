# jwt

jwt Json-Web-Token 的簡寫 由三部分由.連接組成

* header 用來指定使用的算法 (HMAC SHA256 RSA) 和 token類型 (JWT)
* payload 包含聲明 通常是用戶信息或其它數據 比如 id 名稱 昵稱等 聲明可分爲三種 registered public private
* signature 用來保證jwt無法篡改的簽名

# header
```
{
    "alg": "HS256",
    "typ": "JWT"
}
```

header 需要使用 urlbase64 no padding 編碼 

* alg 指定 簽名算法
* typ 指定token類型 jwt 固定爲 JWT

# payload 

payload 是信息載體 可以自定義 另外 由 七個預定義的 屬性



| 名稱 | 含義 |
| -------- | -------- |
| Audience     | jwt受衆     |
| ExpiresAt     | 生效時間     |
| Id     | 簽發編號     |
| IssuedAt     | 簽發時間     |
| Issuer     | 簽發人     |
| NotBefore     | 生效時間     |
| Subject     | 主題     |


# signature
