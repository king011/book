# 基本型別

kotlin 中 所有東西都是對象 可以在任意變量上調用成員函數與屬性

# 整數

支持多種數字表示形式
* 十進制 123
* 十六進制 0x0f
* 二進制 0b0011

| 型別 | 內存佔用 | 最小值 | 最大值 |
| -------- | -------- | -------- | -------- |
| Byte     | 1     | -128     | 127     |
| Short     | 2     | -32768     | 32767     |
| Int     | 4     | -2,147,483,648     | 2,147,483,647     |
| Long     | 8     | -9,223,372,036,854,775,808     | 9,223,372,036,854,775,807     |

所有 未超過 Int 最大值的 整型 會被自動推導爲 Int 如果超過了 則推導爲 Long 也可以在數字後加上 L 顯示指定爲 Long

```
fun main() {
    println(123.javaClass)//int
    println(123L.javaClass)//long
    println(3000000000.javaClass)//long
    val b: Byte = 1
    println(b.javaClass)//Byte
    var s: Short = 1
    println(s.javaClass)//Short
}
```

# 浮點

* 浮點默認爲 double 123.5、123.5e10
* 使用 f 或 F 標記 float 123.5 

| 型別 | 內存佔用 |
| -------- | -------- |
| Float     | 4     |
| Double     | 8     |

```
fun main() {
    println(3.14.javaClass)//double
    val f = 2.7182818284f
    println(f) // 超過 float 範圍 所以值爲 2.7182817
    println(f.javaClass)//float
}
```

無論 浮點 還是整數 可以 用 _ 連接數字 使用 人類更容易觀看

```
fun main() {
    println(3.14_15927)
    println(0xFFFF_0000)
}
```

# 位運算
* shl 有符號左移
* shr 有符號右移
* ushr 無符號右移
* and 與
* or 或
* xor 異或
* inv 位非

```
fun main() {
    println((1 shl 2) and 0x000FF000)
}
```

# 比較
*  相等 a == b ， a != b
* 大小 a < b ， a > b ，a <= b a >= b
* 區間檢查 a..b ， x in a..b ， x !in a..b

1. NaN 與自身相等
2. NaN 比包括正無窮(POSITIVE_INFINITY) 在內的所有素數都大
3.  -0.0 小於 0.0

# 字符

字符用 Char 表示 不可字節和 數字比較

# 布爾

布爾用 Boolean 表示 只有 true 和 false

* || 或
* && 與
* ! 非

# 數組

數字 使用 Array 表示 實現了多種 有用方法 比如 get set 對於 運算符 \[\] 會轉到 對 get set 的調用

kotlin 提供了無裝箱開銷的 原生數組 ByteArray ShortArray IntArray 等 它們和 Array沒有關係 但提供了 相同的 屬性 和 方法

```
fun main() {
    val x: IntArray = intArrayOf(1, 2, 3)
    x[0] = x[1] + x[2]
    // 5,2,3
    println(x.joinToString(","))

    // 0,0,0
    println(IntArray(3).joinToString(","))

    // 42,42,42
    println(IntArray(3) { 42 }.joinToString(","))

    // 0,2,4
    println(IntArray(3) { it * 2 }.joinToString(","))
}
```

# 無符號整數

kotlin 1.3 版本 提供了試驗性的 無符號整數

* UByte
* UShort
* UInt
* ULong

# 字符串

字符串 同樣是不可變常量 使用 " 定義 使用 + 連接

同時 支持 """ 定義 原始字符 和 字符串模板

```
fun main() {
    val name = "cerberus"
    println("""
        $name is an idea
        i love anita
""".trimIndent())
}
```