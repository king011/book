# flutter_sound

[flutter_sound](https://pub.dev/packages/flutter_sound) 提供了 原生的 音頻錄製 和 播放

```
#info="AndroidManifest.xml"
<manifest>
    <application>
    </application>
		
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
</manifest>
```

```
#info="example"
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:kc_microservice_flutter/pages/component/dialog.dart';
import 'package:intl/intl.dart';

class MyKingPage extends StatefulWidget {
  MyKingPage({Key key}) : super(key: key);
  @override
  _MyKingPageState createState() => _MyKingPageState();
}

class _MyKingPageState extends State<MyKingPage> {
  FlutterSound _recorder = new FlutterSound();
  FlutterSound _player = new FlutterSound();
  bool _isRecording = false;
  StreamSubscription<RecordStatus> _recordSubscription;
  String _recordStatus;
  StreamSubscription<PlayStatus> _playSubscription;
  String _playStatus;
  String _file;
  t_AUDIO_STATE _state = t_AUDIO_STATE.IS_STOPPED;
  @override
  void initState() {
    super.initState();

    //_player.setDbLevelEnabled(true);
    // _recorder.audioState; // 需要設置 setDbLevelEnabled 爲 trrue 才會更新 _recorder.audioState
  }

  @override
  void dispose() {
    _recordSubscription?.cancel();
    if (_isRecording) {
      _recorder.stopRecorder();
    }
    _stopPlayer();
    super.dispose();
  }

  _startRecorder() async {
    try {
      final str = await _recorder.startRecorder(
        codec: t_CODEC.CODEC_AAC,
      );
      _recordSubscription = _recorder.onRecorderStateChanged.listen((status) {
        setState(() {
          DateTime date = new DateTime.fromMillisecondsSinceEpoch(
              status.currentPosition.toInt(),
              isUtc: true);
          String txt = DateFormat('mm:ss:SS', 'en_GB').format(date);
          this._recordStatus = txt.substring(0, 8);
        });
      });
      _file = str;
      debugPrint("record to : $str");
      setState(() {
        _isRecording = true;
      });
    } on RecorderRunningException catch (e) {
      showMessageDialog(context, e.message);
    }
  }

  _stopRecorder() async {
    try {
      if (_recordSubscription != null) {
        _recordSubscription.cancel();
        _recordSubscription = null;
      }
      _recorder.stopRecorder();
      setState(() {
        _isRecording = false;
      });
    } on RecorderStoppedException catch (e) {
      showErrorMessageDialog(context, e.message);
    }
  }

  _start() async {
    if (_file == null) {
      return;
    }
    debugPrint("_start $_state");
    switch (_state) {
      case t_AUDIO_STATE.IS_STOPPED:
        _startPlayer();
        break;
      case t_AUDIO_STATE.IS_PAUSED:
        _resumePlayer();
        break;
      default:
    }
  }

  _resumePlayer() async {
    try {
      final str = await _player.resumePlayer();
      _state = t_AUDIO_STATE.IS_PLAYING;
      debugPrint("resume $str");
    } on PlayerRunningException catch (e) {
      showErrorMessageDialog(context, e.message);
    }
  }

  _startPlayer() async {
    try {
      final str = await _player.startPlayer(_file);
      _playSubscription = _player.onPlayerStateChanged.listen((status) {
        if (status == null) {
          _state = t_AUDIO_STATE.IS_STOPPED;
          if (_playSubscription != null) {
            _playSubscription.cancel();
            _playSubscription = null;
          }
        } else {
          DateTime date = new DateTime.fromMillisecondsSinceEpoch(
            status.currentPosition.toInt(),
            isUtc: true,
          );
          String txt = DateFormat('mm:ss:SS', 'en_GB').format(date);
          this.setState(() {
            //this._isPlaying = true;
            this._playStatus = txt.substring(0, 8);
          });
        }
      });
      _state = t_AUDIO_STATE.IS_PLAYING;
      debugPrint("play $str");
    } catch (e) {
      showErrorMessageDialog(context, e.message);
    }
  }

  _pausePlayer() async {
    if (_state != t_AUDIO_STATE.IS_PLAYING) {
      debugPrint("_pausePlayer ${_player.audioState}");
      return;
    }
    try {
      final str = await _player.pausePlayer();
      _state = t_AUDIO_STATE.IS_PAUSED;
      debugPrint("pause $str");
    } on PlayerRunningException catch (e) {
      showErrorMessageDialog(context, e.message);
    }
  }

  _stopPlayer() async {
    if (_state != t_AUDIO_STATE.IS_PAUSED &&
        _state != t_AUDIO_STATE.IS_PLAYING) {
      debugPrint("_stopPlayer ${_player.audioState}");
      return;
    }
    try {
      final str = await _player.stopPlayer();
      if (_playSubscription != null) {
        _playSubscription.cancel();
        _playSubscription = null;
      }
      debugPrint("stop $str");
      _state = t_AUDIO_STATE.IS_STOPPED;
    } on PlayerRunningException catch (e) {
      showErrorMessageDialog(context, e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("king 測試"),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Text("${_recordStatus ?? '00:00:00'}"),
          ),
          IconButton(
            icon: Icon(_isRecording ? Icons.stop : Icons.mic),
            onPressed: _isRecording ? _stopRecorder : _startRecorder,
          ),
          Container(
            alignment: Alignment.center,
            child: Text("${_playStatus ?? '00:00:00'}"),
          ),
          Container(
            alignment: Alignment.center,
            child: IntrinsicWidth(
              child: Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.play_circle_filled),
                    onPressed: _start,
                  ),
                  IconButton(
                    icon: Icon(Icons.pause_circle_filled),
                    onPressed: _pausePlayer,
                  ),
                  IconButton(
                    icon: Icon(Icons.stop),
                    onPressed: _stopPlayer,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
```