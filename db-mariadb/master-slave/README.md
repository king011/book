# 主從複製

* 讓一台服務器的數據和另外的服務器數據保存同步
* 一台主服務器可以和多台從服務器保持同步，並且從服務器也可以反過來作為主服務器
* 主服務器和從服務器可以位於不同的網絡拓撲中，還能對整台服務器，特定數據庫，甚至特定表進行複製

# 工作原理

* master 將變化記錄到二進制日誌(binary log)中(這些記錄叫做二進制日誌事件 binary log events )
* slave 將 master 的 binary log events 拷貝到自己的中繼日誌(relay log)
* slave 重做中繼日誌，修改 slave 上的數據


1. master 記錄中繼日誌。在每個事物更新數據完成前，master 在 binary log 中記錄這些改變。mariadb 將事物寫進 binary log，即使事物中的語句是交叉執行的。在事件寫入 binary log 完成後，master 通知存儲引擎提交事物。
2. slave 將 master 的 binary log events 拷貝到自己的 relay log。首先 slave 開啟一個io線程,io線程在 master 上打開一個普通連接，然後開始 binlog dump process。binlog dump process 從 master 的 binary log 中讀取事件，如果執行完 master 產生 所有檔案，binlog dump process 會睡眠並等待 master 產生新的事件。io線程將這些事件寫入 relay log。
3. SQL slave thread 處理該過程的最後一步。SQL slave thread 從 relay log 中讀取事件，並重新執行其中的事件來更新 slave 的數據，使其與 master 中的數據一致。

# Example
本示例假設有一個 master 和一個 slave，設置過程中應該保證 master 和 slave 不要在中途發生變化。

## master 配置
1. 首先為 master 添加如下配置

	```
	[mysqld]
	server-id=1
	log_bin=master-bin
	binlog-ignore-db=mysql
	binlog-ignore-db=information_schema
	binlog-ignore-db=performance_schema
	binlog-ignore-db=test
	innodb_flush_log_at_trx_commit=1
	binlog_format=mixed
	#expire_logs_days = 10
	#max_binlog_size = 100M 
	```
	
	>  * server-id 需要保證唯一
	> * binlog-ignore-db 指定了 不用複製的 數據庫
	> * expire\_logs\_days 二進制日誌保存多少天 默認爲0 一直保存
	> * max\_binlog\_size 設置單個日誌檔案最尺寸 超過此值\[4096,1GB\] 默認1GB

	```
	MariaDB [(none)]> show global variables like 'max_binlog_size';
	+-----------------+------------+
	| Variable_name   | Value      |
	+-----------------+------------+
	| max_binlog_size | 1073741824 |
	+-----------------+------------+
	1 row in set (0.002 sec)
	```

1. 之後在 master 上添加一個備份用的帳號

	```
	grant replication slave on *.* to 'backup'@'%' identified by 'backupmima';
	flush privileges;
	```

1. 重啟 master 

	```
	sudo systemctl restart mariadb
	```

1. 查看 log_bin 檔案

	```
	show master status\G
	```

	![](assets/mariadb.png)
	
	記錄下 File 和 Position 屬性 以便後續使用
	
1. 將 master 庫所有數據導出

	```
	mysqldump -u root -p  -A > master-all.sql
	```

## slave 配置

1. 導入 master 內所有數據

	```
	mysql -uroot -p < master-all.sql
	```
	
1. 為 slave 添加配置

	```
	[mysqld]
	server-id=2
	relay-log-index=slave-relay-bin.index
	relay-log=slave-relay-bin
	relay_log_recovery=1
	```
	
1. 重啟 slave

	```
	sudo systemctl restart mariadb
	```
	
1. 在 slave 中執行 

	```
	stop slave;
	change master to 
				 master_host='master IP',
				 master_port=3306,
				 master_user='backup',
				 master_password='backupmima',
				 master_log_file='master-bin.000002',
				 master_log_pos=43147982;
	```
	
	> master\_log\_file 和 master\_log\_pos 對應於 master 中的 File 和 Position 屬性

1. 啟動 slave

	```
	slave start;
	```

1. 查看是否配置成功

	```
	show slave status\G
	```
	
	![](assets/slave.png)