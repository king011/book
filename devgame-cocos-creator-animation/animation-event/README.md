# 動畫事件

![](assets/button.png)

在時間軸上 添加 插入事件 即可 添加事件 會出現一個 白點

![](assets/delete.jpg)

雙擊白帶 即可 編輯 事件回調函數

# 觸發函數 參數

觸發的函數 會自動 查找掛接的 各個組件中 的同名函數 (所有組件的同名函數都會被調用)

參數 目前只 支持 boolean string number

# 動畫回調

[cc.Animation](https://docs.cocos.com/creator/api/zh/classes/Animation.html) 還提供了 動畫回調 供腳本註冊

* play 開始播放時
* stop 停止播放時
* pause 暫停播放時
* resume 恢復播放時
* lastframe 加入動畫循環次數大於1 當動畫播放到最後一幀時
* finished 動畫播放完成時

當在 [cc.Animation](https://docs.cocos.com/creator/api/zh/classes/Animation.html)  註冊一個回調後 會在播放動畫時 對相應的 [cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html) 註冊回調 在 [cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html)  停止播放時 對 [cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html)  取消註冊

[cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html) 其實才是動畫回調的發送方 如果希望對 單個 [cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html) 註冊回調 可以 獲取 [cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html)  後再單獨 對它註冊


```
const animation = this.node.getComponent(cc.Animation)

// 註冊回調
animation.on('play',      this.onPlay,        this)
animation.on('stop',      this.onStop,        this)
animation.on('lastframe', this.onLastFrame,   this)
animation.on('finished',  this.onFinished,    this)
animation.on('pause',     this.onPause,       this)
animation.on('resume',    this.onResume,      this)

// 取消註冊
animation.off('play',      this.onPlay,        this)
animation.off('stop',      this.onStop,        this)
animation.off('lastframe', this.onLastFrame,   this)
animation.off('finished',  this.onFinished,    this)
animation.off('pause',     this.onPause,       this)
animation.off('resume',    this.onResume,      this)

// 短單個 cc.AnimationState 註冊回調
const anim1 = animation.getAnimationState('anim1')
anim1.on('lastframe',    this.onLastFrame,      this)
```
