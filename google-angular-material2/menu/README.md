# MatMenuModule
MatMenuModule 提供了菜單 功能
```ts
import {MatMenuModule} from '@angular/material/menu';
```

```html
<button mat-button [matMenuTriggerFor]="menu">Menu</button>
<mat-menu #menu="matMenu">
  <button mat-menu-item>Item 1</button>
  <button mat-menu-item [matMenuTriggerFor]="matMenuChild">Item 2</button>

  <mat-menu #matMenuChild="matMenu">
    <button mat-menu-item>Child 1</button>
    <button mat-menu-item>Child 2</button>
  </mat-menu>
</mat-menu>
```
>  * **matMenuTriggerFor** 指定 按鈕對應的菜單 
>  * **mat-menu** 定義一個菜單
>  * **mat-menu-item** 定義一個菜單項

## matMenuTrigger

使用 matMenuTrigger 來創建 一個 按鈕的 引用 其提供了 openMenu toggleMenu closeMenu 等方法 來在程式中 動態 打開 關閉 按鈕

```html
<button mat-button [matMenuTriggerFor]="menu" #menuTrigger="matMenuTrigger">Menu</button>
<mat-menu #menu="matMenu">
  <button mat-menu-item>Item 1</button>
  <button mat-menu-item [matMenuTriggerFor]="matMenuChild">Item 2</button>

  <mat-menu #matMenuChild="matMenu">
    <button mat-menu-item>Child 1</button>
    <button mat-menu-item>Child 2</button>
  </mat-menu>
</mat-menu>
<button mat-button (click)="menuTrigger.toggleMenu()">click me</button>
```

# xPosition yPosition
在 **mat-menu** 中 使用 xPosition yPosition 可以調整菜單出現位置

## xPosition
* after 預設值 從start往end的方向長 通常是從左到右
* before 從end往start的方向長 通常是從右到左

## yPosition
* below 預設值 從上往下長
* above 從下往上長

```html
<button mat-button [matMenuTriggerFor]="menu">Menu</button>
<mat-menu #menu="matMenu" xPosition="before" yPosition="above">
  <button mat-menu-item>Item 1</button>
  <button mat-menu-item>Item 2</button>
</mat-menu>
```
## overlapTrigger
有時 菜單項會 擋住 菜單按鈕 可以設置 overlapTrigger 爲false 以告訴 material  不要 擋住 菜單按鈕
```html
<button mat-button [matMenuTriggerFor]="menu">Menu</button>
<mat-menu #menu="matMenu" xPosition="before" yPosition="above" [overlapTrigger]="false">
  <button mat-menu-item>Item 1</button>
  <button mat-menu-item>Item 2</button>
</mat-menu>
```

# mat-divider
同樣 使用 `<mat-divider></mat-divider>` 可以創建分割線
```html
<button mat-button [matMenuTriggerFor]="menu">Menu</button>
<mat-menu #menu="matMenu">
  <button mat-menu-item>Item 1</button>
  <mat-divider></mat-divider>
  <button mat-menu-item>Item 2</button>
</mat-menu>
```

# no-outline
按鈕 mat-menu-item 獲取焦點時 會有一個 外邊框線 看起來 很醜 可以 自定義一個 css 去掉
```css
.no-outline{
    outline: 0 none !important;
    border: none;
}
```
```html
<button class="no-outline" mat-button [matMenuTriggerFor]="menu">Menu</button>
<mat-menu #menu="matMenu">
  <button class="no-outline" mat-menu-item>Item 1</button>
  <mat-divider></mat-divider>
  <button class="no-outline" mat-menu-item>Item 2</button>
</mat-menu>
```