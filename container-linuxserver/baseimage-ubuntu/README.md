# [baseimage-ubuntu](https://github.com/linuxserver/docker-baseimage-ubuntu)

baseimage-ubuntu 是一個基礎鏡像包含了 [ubuntu](https://cloud-images.ubuntu.com/) 和 [s6-overlay](https://github.com/just-containers/s6-overlay)，通常你可以使用它作爲基礎鏡像來構建你自己的容器


```
#info="Dockerfile"
FROM ghcr.io/linuxserver/baseimage-ubuntu:focal

# 安裝應用
COPY http /usr/bin/

# 安裝服務器
COPY root/ /

# 端口 和 卷
EXPOSE 80 443
VOLUME /config
```

```
#info="root/etc/services.d/http/run"
#!/usr/bin/with-contenv bash

exec \
    s6-setuidgid abc \
    /usr/bin/http
```

```
#info="docker-compose.yml"
version: '1'
services:
  main:
    build: ./
    restart: always
    ports:
      - 9000:80
    environment:
      - PUID=33 # www-data
      - PGID=33 # www-data
      - TZ=Asia/Shanghai
```

# cont-init.d

你可以在 **/etc/cont-init.d** 創建一些腳本用於執行初始化設定，例如爲檔案中設置好正確的權限

```
#info="/etc/cont-init.d/30-config"
#!/usr/bin/with-contenv bash

# create directories
mkdir -p \
    /config/{log,data/transcodes,cache} \
    /data \
    /transcode

# permissions
chown abc:abc \
    /config \
    /config/* \
    /config/data/transcodes \
    /transcode
```

```
#info="/etc/cont-init.d/40-gid-video"
#!/usr/bin/with-contenv bash

FILES=$(find /dev/dri /dev/dvb /dev/vchiq /dev/vc-mem /dev/video1? -type c -print 2>/dev/null)

for i in $FILES
do
    VIDEO_GID=$(stat -c '%g' "$i")
    if ! id -G abc | grep -qw "$VIDEO_GID"; then
        VIDEO_NAME=$(getent group "${VIDEO_GID}" | awk -F: '{print $1}')
        if [ -z "${VIDEO_NAME}" ]; then
            VIDEO_NAME="video$(head /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c8)"
            groupadd "$VIDEO_NAME"
            groupmod -g "$VIDEO_GID" "$VIDEO_NAME"
        fi
        usermod -a -G "$VIDEO_NAME" abc
        if [ $(stat -c '%A' "${i}" | cut -b 5,6) != "rw" ]; then
            echo -e "**** The device ${i} does not have group read/write permissions, which might prevent hardware transcode from functioning correctly. To fix it, you can run the following on your docker host: ****\nsudo chmod g+rw ${i}\n"
        fi
    fi
done

# openmax lib loading
if [ -e "/opt/vc/lib" ] && [ ! -e "/etc/ld.so.conf.d/00-vmcs.conf" ]; then
    echo "[jellyfin-init] Pi Libs detected loading"
    echo "/opt/vc/lib" > "/etc/ld.so.conf.d/00-vmcs.conf"
    ldconfig
fi
```