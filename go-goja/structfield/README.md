# 屬性名映射

默認情況下 goja 將按照原樣 將 go struct 映射到 js 可以調用SetFieldNameMapper 函數 設置 屬性映射方式

goja 默認提供了 幾種標準的 映射實現

* goja.TagFieldNameMapper
* goja.UncapFieldNameMapper

```
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
)

func main() {
	// 創建 虛擬機
	vm := goja.New()
	vm.SetFieldNameMapper(goja.TagFieldNameMapper("json", true))
	vm.Set(`println`, func(args ...interface{}) {
		fmt.Println(args...)
	})
	vm.Set(`obj`, struct {
		Name string `json:"name"`
	}{
		Name: "king",
	})
	_, e := vm.RunScript(`main.js`, `
println(JSON.stringify(obj))
	`)
	if e != nil {
		log.Fatalln(e)
	}
}
```