# [網路接口](http://docs.cocos.com/creator/manual/zh/scripting/network.html)

creator 提供了 和 web 平臺 兼容的 通信 api

* XMLHttpRequest 
* WebSocket

# XMLHttpRequest

[XMLHttpRequest](https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest) 是瀏覽器中 實現和 服務器 短連接的 標準組件

```ts
 var xhr = new XMLHttpRequest();
 xhr.onreadystatechange = function () {
     if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
         var response = xhr.responseText;
         console.log(response);
     }
 };
 xhr.open("GET", url, true);
 xhr.send();
```

# WebSocket

[WebSocket](https://developer.mozilla.org/zh-CN/docs/Web/API/WebSocket) 是 html5 後瀏覽器提供的 服務器 雙向 通信組件 可以實現 長連接 請求

```
ws = new WebSocket("ws://echo.websocket.org");
ws.onopen = function (event) {
	 console.log("Send Text WS was opened.");
};
ws.onmessage = function (event) {
	 console.log("response text msg: " + event.data);
};
ws.onerror = function (event) {
	 console.log("Send Text fired an error");
};
ws.onclose = function (event) {
	 console.log("WebSocket instance closed.");
};

setTimeout(function () {
	 if (ws.readyState === WebSocket.OPEN) {
			 ws.send("Hello WebSocket, I'm a text message.");
	 }
	 else {
			 console.log("WebSocket instance wasn't ready...");
	 }
}, 3);
```