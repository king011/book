# bash completion

bash completion 是 linux 提供的強大的 命令補齊功能

bash completion 默認為linux提供了 標準的 補齊功能 並且允許 用戶 編輯腳本以為第三方命令 提供補齊擴展

通常 補齊腳本都 存在在 **/etc/bash_completion.d** 中

## bash-completion 套件

bash-completion 是爲 常用 程式 提供的 bash completion 腳本 通常 桌面系統都默認安裝了 服務器 則需要自行安裝

```sh
#info=false
sudo apt install bash-completion
```

## Example
```sh
#!/bin/bash
#Program:
#       zap-cli for bash completion
#History:
#       2018-10-13 king011 first release
#       2018-10-16 king011 fix style
#Email:
#       zuiwuchang@gmail.com

function __king011_zap_cli_watch()
{
    local opts='-f --filename -a --auto -u --url -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -f|--filename)
            _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;
        -u|--url)
            COMPREPLY=()
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_zap_cli_analyze()
{
    # 參數定義
    local opts='-f --filename -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -f|--filename)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_zap_cli()
{
    # 獲取 正在輸入的 參數
    local cur=${COMP_WORDS[COMP_CWORD]}
    
    #  輸入 第1個 參數
    if [ 1 == $COMP_CWORD ];then
        local opts="watch analyze help --help -h -v --version"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
        # switch 子命令
        case ${COMP_WORDS[1]} in
            watch)
                __king011_zap_cli_watch
            ;;

            analyze)
               __king011_zap_cli_analyze
            ;;
        esac
    fi
}

complete -F __king011_zap_cli zap-cli
```

# complete

complete 命令用來 定義一個 補齊項  
complete XXX 命令

## -W wordlist
-W wordlist 分割 wordlist中的單詞 作為 候選結果

```sh
# 為 ng 添加 help version new serve 4 個 補齊候選
complete -W 'help version new serve' ng
```

## -F function

-F function 最小一個 shell函數 將 COMPREPLY 數組 作為 候選結果  
在 function 提供了 幾個 內置變量 用於和 bash completion 交互

| 變量 | 含義 |
| -------- | -------- |
| COMP_WORDS     | 數組 存放了當前命令中 所有輸入的單詞     |
| COMP_CWORD     | 整型 當前輸入單詞 在 COMP_WORDS 中的索引     |
| COMPREPLY     | 數組 輸出的 候選補齊結果     |
| COMP_WORDBREAKS     | 字符串 單詞分隔符     |
| COMP_LINE     | 字符串 當前命令行輸入的字符串     |
| COMP_POINT     | 整型 光標在當前命令行 的位置     |

```sh
#!/bin/bash
function _my_ng_()
{
    # 獲取 正在輸入的 參數
    local word=${COMP_WORDS[COMP_CWORD]}
 
    # switch 輸入第 幾個參數
    case $COMP_CWORD in
        1)
            # 返回 候選
            COMPREPLY=($(compgen -W 'new help' -- "$word"))
        ;;
 
        2)
            # 返回 候選
            COMPREPLY=($(compgen -W 'abc def' -- "$word"))
        ;;
 
        *)
            COMPREPLY=()
        ;;
    esac 
}
complete -F _my_ng_ ng
```

# compgen
compgen 通常用來 篩選 補齊結果 其參數 類似 complete

```sh
# compgen -W 'word1 word2 test'   
word1  
word2  
test  
# compgen -W 'word1 word2 test' word   
word1  
word2  
```

# 模板
寫一個完善的 bash completion 不是一個容易的事情，要考慮許多還要反覆測試這相當耗費心神並且還容易出錯

不過不要擔心本喵經過多年的實踐後寫了一個模板，只要按照這個模板的規則去替換掉部分代碼就可以實現一個功能強大且完善的 bash completion 腳本

首先是使用模板完成的腳本可以提供如下功能

1. 自動補全子命令
2. 自動補全短參數和長參數
3. 爲參數補全可選值
4. 爲參數補全檔案路徑
5. 爲 args(沒有被子命令以及參數匹配的內容) 執行補全
6. 參數補全 和 args 補全支持動態調用命令生成補全內容

**注意** 只要在當前輸入的單詞，首字母是 **-** 時才會進行參數補全，其它時候會執行 子命令補全 或者 args 補全。這樣設計是因爲參數必須一字符 **-** 爲前綴所以在沒有輸入 - 之前本喵假設用戶當前並不想輸入參數所以將補全內容中刪除參數以爲用戶提供更高的補全命中率

下面先看下腳本的模板代碼，這時本喵從自己寫的 docker-conpose 補全腳本中抽取的關鍵代碼：
```
#!/bin/bash
#
# bash completion for docker-compose
#
# This script provides completion of:
#  - commands and their options
#  - service names
#  - filepaths
#
# To enable the completions either:
#  - place this file in /etc/bash_completion.d
#  or
#  - copy this file to e.g. ~/.docker-compose-completion.sh and add the line
#    below to your .bashrc after bash completion features are loaded
#    . ~/.docker-compose-completion.sh


### BEGIN helper functions ###
_docker_compose_args_func(){
    eval "_docker_compose_args(){
    $1
}"
}
_docker_compose_args_filedir(){
    eval "_docker_compose_args(){
    COMPREPLY=( \$(compgen -o plusdirs -f \${cur}) )
}"
}
_docker_compose_args_opts(){
    eval "_docker_compose_args(){
    COMPREPLY=( \$(compgen -W '$1' -- \${cur}) )
}"
}
_docker_compose_args(){
    local _x=0
}
_docker_compose_rest(){
    eval '_docker_compose_args(){
    local _x=0
}'
    commands=()
    flags=()
    flags_file=()
    flags_str=()
    flags_vals=()
    flags_eval=()
}

_docker_compose_flags(){
    local opts="${flags[@]} ${flags_file[@]} ${flags_str[@]} ${flags_vals[@]} ${flags_eval[@]} ${flags_eval[@]}"
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
}
### END helper functions ###

### BEGIN define ###
# val list for root command
_docker_compose_vals--ansi(){
    docker_compose_vals=(
        never
        always
        auto
    )
}
# val list for sub command
_docker_compose-exec_vals--user(){
    docker_compose_vals=(
        root bin daemon adm lp sync shutdown halt
        mail news uucp operator man postmaster
        cron ftp sshd at squid xfs games cyrus
        vpopmail ntp smmsp guest nobody
    )
}
_docker_compose-exec_vals-u(){
    _docker_compose-exec_vals--user
}
# val list by eval
_docker_compose-exec_eval--index(){
    COMPREPLY=( $(compgen -W "1 2 3 4 5 6 7 8 9" -- ${cur}) )
}
_docker_compose-exec(){
    _docker_compose_rest
    # flags
    flags=(
        --help
        -d --detach 
        -T --no-TTY
        --privileged
    )
    flags_str=(
        -e --env
        -w --workdir
    )
    flags_vals=(
        -u --user
    )
    flags_eval=(
        --index
    )

    # args
    # _docker_compose_args_filedir
    # _docker_compose_args_opts "xl bu xx"
    _docker_compose_args_func "_docker_compose_services"
}
_docker_compose_services(){
    if [[ -f compose.yaml || -f compose.yml || -f docker-compose.ayml || -f docker-compose.yml ]];then
        local opts=$(docker-compose ps --services)
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    fi
}

### END define ###

_docker_compose_foreach(){
    local word="${COMP_WORDS[counter]}"
    local val
    # input file or dir
    for val in "${flags_file[@]}"; do
        if [[ "$val" == "$word" ]];then
            input=1
            return
        fi
    done
    # input file or dir
    for val in "${flags_str[@]}"; do
        if [[ "$val" == "$word" ]];then
            input=2
            return
        fi
    done
    # input vals list
    for val in "${flags_vals[@]}"; do
        if [[ "$val" == "$word" ]];then
            input=3
            docker_compose_flags_vals="$val"
            return
        fi
    done
    # input eval
    for val in "${flags_eval[@]}"; do
        if [[ "$val" == "$word" ]];then
            input=4
            docker_compose_flags_eval="$val"
            return
        fi
    done
    # sub command
    for val in "${commands[@]}"; do
        if [[ "$val" == "$word" ]];then
            docker_compose_cmd="$docker_compose_cmd-$val"
            eval "$docker_compose_cmd"
            if [[ $? == 0 ]];then
                return
            else
                # eval error
                exit 0
            fi
        fi
    done
}
_docker_compose(){
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[COMP_CWORD-1]}

    local commands=(
        build convert create down events exec 
        help images kill logs pause port ps 
        pull push restart rm run scale start stop 
        top unpause up version
    )
    local flags=(
        --compatibility
        --help
    )
    local flags_file=(
        --env-file
        -f --file
        --profile
        --project-directory
    )
    local flags_str=(
        -p --project-name
    )
    local flags_vals=(
        --ansi
    )
    local flags_eval=()
    local input=0
    local counter=1
    local docker_compose_cmd="_docker_compose"
    local docker_compose_flags_vals=""
    local docker_compose_flags_eval=""
    for ((;counter<COMP_CWORD;counter=counter+1));do
        if [[ $input == 0 ]];then
            _docker_compose_foreach
        else
            input=0
        fi        
    done

    case $input in
        0)
            case "$cur" in
                -*)
                    _docker_compose_flags
                ;;
                *)
                    if [[ ${#commands[@]} == 0 ]];then
                        # args
                        _docker_compose_args
                    else
                        # sub command
                        local opts="${commands[@]}"
                        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
                    fi
                ;;
            esac
        ;;
        # input file
        1)
            COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;
        # input str
        # 2)
        # ;;
        # input vals list
        3)
            local docker_compose_vals=()
            eval "${docker_compose_cmd}_vals${docker_compose_flags_vals}"
            local opts="${docker_compose_vals[@]}"
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
        # input eval
        4)
            eval "${docker_compose_cmd}_eval${docker_compose_flags_eval}"
        ;;
    esac
}

complete -F _docker_compose docker-compose docker-compose.exe
```

我們逐步進行分析

1. 首先所有函數都使用 **\_docker\_compose** 前綴，你需要自己更換成爲另外一個不會與其它腳本重名的前綴，因爲 bash 腳本函數都是全局的，使用一個獨特的前綴避免與別人腳本中函數重名而引發 bug

2. 第 240 行爲 docker-compose docker-compose.exe 兩個命令指定了補全函數 **\_docker\_compose**，函數定義在 161 行

	這個函數是入口點，它使用比較通用的方式對輸入進行分析和補全指令
	
	在 165 行定義了數組 commands 裏面包含了支持的子命令，如果不支持子命令將它設置爲空數組即可 `local commands=()`
	
	171到186行定義了多個 flagsXXX 數組，它們用於記錄支持的各種不同的參數，因爲不同的參數類型需要使用不同的方式補全指令，所以需要將它們定義到不同的數組，同樣如果沒有這種類型的參數將它設置爲空數組即可。先看下各變量的含義
	* **flags** 這種參數後面不會接受一個對應的值
	* **flags\_file** 這種參數後面需要接受一個檔案系統的路徑，腳本會補全路徑
	* **flags\_str** 這種參數後面要接受一個未知字符串，因爲字符串值未知所以腳本不會對值進行自動補全
	* **flags\_vals** 這種參數後面要接受一個已經字符串，腳本會對這些值進行自動補全
	* **flags\_eval** 這種參數後面要接受一個字符串，這個字符串的可選內容由動態執行的命令生成，腳本會調用指定命令來獲取候選值
3. 188行到199行，腳本遍歷了輸入的命令，從而獲取到當前的輸入狀態，同時如果存在子命令將補齊內容也切換到子命令

	實現代碼就是第 114 行定義的 **\_docker\_compose\_foreach** 函數，切換到子命令是通過 150 行調用 **\_docker\_compose${-Child\_1}${-Child\_2}...** 完成的
4.  201行到237行依據獲取到的狀態，調用不同狀態下的補全功能即可

## 適配模板
下面是如何修改模板來適配自己的代碼

1. 先確定了每次前綴後文稱爲 prefix，把所有函數都改爲以 prefix 開頭，把190行的變量 **docker\_compose\_cmd** 初始化爲 $prefix，第53行  END helper functions 之前的代碼都保留不要修改，那是一些通用的輔助函數
2. 在入口函數例子中的 \_docker\_compose 函數中修改變量 commands flags flags\_file flags\_str flags\_vals flags\_eval 到的指令支持的值
3. 爲 flags\_vals 的所有參數定義一個函數來提供候選值

	例子中 57，65，74 行爲根命令的參數提供了候選值，在函數中將候選值設置到 **docker\_compose\_vals** 變量
	
	函數命名規則是 $prefix +[-$sub]...+ \_vals + $flag 
	* prefix 是你定義的名稱前綴
	* sub 是子命令名，嵌套了多少層子命令就依次寫多少個
	* flag 是參數的名稱
4. 爲 flags\_eval 的所有參數定義一個函數來提供補齊

	例子中 77 行，命名規則是 $prefix + [-$sub]... + \_eval +$flag

5. 如果根命令要補全 args ，則修改第 34 行的 **\_docker\_compose\_args** 函數，在裏面補全指令
6. 爲所有在命令定義初始化函數，例子中第 80 行，函數命名規則是 $prefix + [-$sub]...

	函數必須實現調用 \_docker\_compose\_rest，然後修改 commands flags flags\_file flags\_str flags\_vals flags\_eval 的值到子命令支持的內容
	
	子命令可以在最後調用如下三個函數中的一個來設置如何補全 args
	
	* **\_docker\_compose\_args\_filedir** 對 args 執行路徑補全
	* **\_docker\_compose\_args\_opts** 將傳遞給此函數的第一個參數作爲補全內容
	* **\_docker\_compose\_args\_func** 此函數的第一個參數指定來一個函數名稱，調用這個函數進行補全

	你可以將 **\_docker\_compose\_flags** 傳遞給 **\_docker\_compose\_args\_func** 函數 `_docker_compose_args_func "_docker_compose_flags"`，來告訴模板使用參數補全來補全 args 內容

	