# 場景間傳遞數據

創建一個 GameData 節點，並且掛接上 GameData 腳本，在 GameData 腳本中調用 DontDestroyOnLoad 函數以告訴引擎場景銷毀時不要銷毀節點

```
#info="GameData.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    // 單例
    public static GameData Instance { get; private set; }
    void Awake()
    {
        if (Instance == null)
        {
            // 首次運行設置單件實例 並告訴引擎不要銷毀對象
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            // 單件已經存在 銷毀新的對象
            Destroy(gameObject);
        }
    }
    // 要傳遞的任意參數
    public int param;
}
```

之後就可以通過 GameData.Instance.XXX 來設置和獲取要傳遞的數據

# 普通單件

此外也可以直接創建一個普通的單件，不過普通的單件無法和 unity 可視化開發界面交互

```
#info="SingletonData.cs"
public class SingletonData
{
    static SingletonData instance;
    public static SingletonData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new SingletonData();
            }
            return instance;
        }
    }
    public int param;
}
```