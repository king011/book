# 安裝

Docker Desktop for Mac/Windows 自帶了 docker-compose 對於 linux 從 [github](https://github.com/docker/compose/releases) 下載最新版本即可

```
sudo curl -o /usr/local/bin/docker-compose -L https://github.com/docker/compose/releases/download/v2.7.0/docker-compose-linux-x86_64

sudo chmod a+x /usr/local/bin/docker-compose
```

# 命令補齊

```
sudo curl -o /etc/bash_completion.d/docker-compose -L https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose
```

# 卸載

```
sudo rm /usr/local/bin/docker-compose
```