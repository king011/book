# 顯示的 virtual 重載 final 避免重載繼承

* 現在在子類中 重載 virtual 函數 可以 加上 override 這樣 編譯器會檢查確定這的確是一個 在父類中定義的 virtual 函數，應該 使用這個特性 可以避免我們錯誤的 重載根本不存在的 virtual 函數
* c++11 還提供了 final 關鍵字 可以指定一個類型 不能在被繼承 或指定 一個函數無法在被重載

```
#include <iostream>
class anima_t
{
public:
    virtual void speak() = 0;
    virtual const char *name() = 0;
};
class cat_t : public anima_t
{
public:
    virtual void speak() override final
    {
        std::cout << "i'm " << name() << ", 喵~" << std::endl;
    }
    virtual const char *name() override
    {
        return "cat";
    }
};

class doll_t final : public cat_t
{
    virtual const char *name() override
    {
        return "doll";
    }
};

int main(int argc, char *argv[])
{
    doll_t doll;
    doll.speak();
    return 0;
}
```

# 強型別枚舉
c++11 之前 枚舉被視爲整數 這使得兩個不同枚舉間可以比較 而且枚舉的名稱全暴露在枚舉型別的作用域中 因此不同枚舉也不可有相同的列舉名

c++11 提供了新的 強型別枚舉來 解決這些問題

```
#include <iostream>
enum class animta_t
{
    cat,
    dog = 100,
    fish //101
};

int main(int argc, char *argv[])
{
    std::cout << animta_t::dog << std::endl;
    return 0;
}
```

枚舉 默認使用 int 但也可以指定類型
```
#include <iostream>
enum class animta_t : short
{
    cat,
    dog = 100,
    fish //101
};

int main(int argc, char *argv[])
{
    std::cout << (short)(animta_t::dog) << std::endl;
    return 0;
}
```
# noexcept

c++ 允許拋出異常 但有些函數 不會拋出 現在可以在 函數後 加上 關鍵字 noexcept 代表函數不會拋出異常

表明 noexcept 編譯器可以知道不會拋出異常 能夠有更好的優化區間 如果函數拋出了異常 將會導出程式退出 因爲異常無法被捕獲到(捕獲相關代碼已經被優化掉)

```
#include <iostream>
void speak() noexcept
{
    std::cout << "cerberus is an idea" << std::endl;
}
int main(int argc, char *argv[])
{
    speak();
    return 0;
}
```

此外 noexcept 還能指定只有在滿足一定條件時 才不拋出異常

```
#include <iostream>
#include <vector>
template <typename T>
void swap(T &x, T &y) noexcept(noexcept(x.swap(y))) //如果 x.swap(y) 不拋出異常 則函數不會拋出異常
{
    x.swap(y);
}
int main(int argc, char *argv[])
{
    std::vector<int> x, y;
    swap(x, y);
    return 0;
}
```

noexcept 的優化並不能帶來顯著的 效率提升 錯誤的使用 可能導致編譯器意外的優化引起 奇怪的bug 通常只在下述情況下使用
1. 移動構造與賦值
2. 析構函數(一些編譯器會默認 加上)
3. 葉子函數(內部不分配內存也不調用其它函數 不存儲非易失性寄存器 不處理異常的函數)

不過 noexcept 很難把握 本喵建議不要使用 一些 簡單的 move 構造與賦值 厲害的編譯器可以自動分析出是否需要優化異常代碼