# [discovery server](https://docs.syncthing.net/users/stdiscosrv.html)

syncthing 依賴 discovery server 在 internet 中查找對等方，任何人都可以運行發現服務器並將 syncthing 的 discovery server 指向該服務器

syncthing 官網維護了一個公共使用的全局服務器，使用 default 配置爲 syncthing 的默認設定

# stdiscosrv

stdiscosrv 是 官方提供的一個 discovery server 以供用戶運行並使用自己的 discovery server

```
#info="ubuntu"
sudo apt install syncthing-discosrv -y
```

stdiscosrv 默認以 https 工作在 8443 端口

```
https://disco.example.com:8443/
```

stdiscosrv 啓動時會打印設備 ID。如果使用非 CA 簽名的證書，則必須在 URL 中 設置此 id 給客戶端

```
https://disco.example.com:8443/?id=7DDRT7J-UICR4PM-PBIZYL3-MZOJ7X7-EX56JP6-IK6HHMW-S7EK32W-G3EUPQA
```




