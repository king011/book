# SharedPreferences

SharedPreferences 是 android 以 xml 形式 提供給 用戶 用來存儲小量數據的 工具

flutter 插件 [SharedPreferences](https://pub.dartlang.org/packages/shared_preferences) 提供了 這個功能 並且 兼容 ios平臺

```
#info=false
dependencies:
  shared_preferences: ^0.5.1
```

```
#info=false
import 'package:shared_preferences/shared_preferences.dart';
```
```dart
class _MyHomePageState extends State<MyHomePage> {
  final _key = TextEditingController();
  final _val = TextEditingController();
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "key : ",
              ),
              TextField(
                decoration: InputDecoration(
                  hintText: 'Input Key',
                ),
                controller: _key,
              ),
              Text(
                "key : ",
              ),
              TextField(
                decoration: InputDecoration(
                  hintText: 'Input Value',
                ),
                controller: _val,
              ),
              MaterialButton(
                child: Text("read"),
                onPressed: () async {
                  var key = _key.text;
                  // 打開 SharedPreferences
                  var prefs = await SharedPreferences.getInstance();
                  // 讀取值
                  var val = prefs.getString(key);
                  _val.text = val;
                },
              ),
              MaterialButton(
                child: Text("write"),
                onPressed: () async {
                  var key = _key.text;
                  var val = _val.text;
                  // 打開 SharedPreferences
                  var prefs = await SharedPreferences.getInstance();
                  // 設置值
                  var ok = await prefs.setString(key, val);
                  debugPrint("write $key=$val $ok");
                },
              ),
            ],
          ),
        ),
      );
}
```