# SQLite

SQLite 是遵守 ACID 的[開源(Public Domain)](https://sqlite.org/copyright.html)跨平臺嵌入式關係型數據庫

* [官網](https://sqlite.org/index.html)
* [源碼](https://www.sqlite.org/src/doc/trunk/README.md)