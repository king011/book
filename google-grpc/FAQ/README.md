# 包含 new 生成 grpc 代碼失敗

不要使用 特殊的 關鍵字 作爲 grpc service/method/request/response 名稱

比如 使用 new/New 作爲 一個 方法名

生成 grpc 代碼時 一些語言插件 會創建一個 method new 之類的 代碼 這在一些語言中 通常是 錯誤的語法 而一些語言中 允許這樣的 代碼

即時 是 New 也可能參數問題 因爲 grpc 代碼 創建插件 會以 語言的 風格 改變 你的命名 大小寫

合理的方案是 使用 另外的 單詞 來替換掉 這些可能成爲 語言 保留字 的單詞

# nginx

```
server {
    listen       6666 http2;
    server_name  localhost;

    #charset koi8-r;
    access_log  /var/log/nginx/host.access.log  main;

    location / {
        grpc_pass grpc://grpcservers;
    }
}

upstream grpcservers {
    server 10.161.11.181:8090;
    server 10.161.11.180:8090;
    server 10.161.11.179:8090;
    server 10.161.11.178:8090;
    server 10.161.11.177:8090;
    keepalive 2000;
}
```