# Flame

flame 是開源(MIT)組件化的 flutter 遊戲引擎，基於 flutter 提供的強大與舒適的基礎架構，爲遊戲提供了一套完整的解決方案

```
flutter pub add flame
```

```
dependencies:
  flame: ^1.16.0
```

* pub 倉庫 [https://pub.dev/packages/flame](https://pub.dev/packages/flame)
* 源碼 [https://github.com/flame-engine/flame](https://github.com/flame-engine/flame)
* 官網 [https://flame-engine.org/](https://flame-engine.org/)
* api [https://pub.dev/documentation/flame/latest/](https://pub.dev/documentation/flame/latest/)
* example [https://examples.flame-engine.org/](https://examples.flame-engine.org/)