# ota

android 使用 ota 技術自動 提供 方便快捷的 自動更新

# 手動更新 ota
在無恥的社會 是無法連接 google服務的 手動更新 成爲了 唯一的出路和永恆的 疼

1. 打開手機的 usb調試功能
2. 安裝好 adb 工具 [https://developer.android.com/studio/releases/platform-tools](https://developer.android.com/studio/releases/platform-tools)
3. 下載 最新的 ota ota_XXX.zip [https://developers.google.com/android/ota](https://developers.google.com/android/ota)
4. 將手機連接到 pc
5. 執行 **adb devices** 查看是否識別到 手機
6. 執行 **adb reboot recovery** 讓手機 重啓並進入 recovery 模式
7. 按住手機電源鍵不放 同時 按下音量\+ 進入 維護選單
8. 使用 手機 (音量鍵 選擇 電源 確定) 選中 **Apply update from ADB**
9. 執行 **adb sideload ota_XXX.zip** 等待 執行完成 後手機出現** Install from ADB complete**
10. 手機 選擇 **Reboot system now** 重啓 手機 完成 更新

# ubuntu 安裝 adb
```sh
sudo apt install android-tools-adb
```
## adb devices : no permissions
linux 執行 adb devices 會出現 no permissions 問題 這是 因爲 需要爲 usb 設置 權限

1. 將 手機 連到 pc 並執行 lsusb 獲取 設備 id

	```sh
	$ lsusb 
	Bus 002 Device 002: ID 8087:8000 Intel Corp. 
	Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
	Bus 001 Device 002: ID 8087:8008 Intel Corp. 
	Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
	Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
	Bus 003 Device 003: ID 046d:c05a Logitech, Inc. M90/M100 Optical Mouse
	Bus 003 Device 002: ID 05af:1031 Jing-Mold Enterprise Co., Ltd 
	Bus 003 Device 014: ID 18d1:4ee7 Google Inc. 
	Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
	```

1. 爲 usb 添加 權限 設置 

	```sh
	sudo echo 'SUBSYSTEM=="usb",ATTRS{idVendor}=="18d1",ATTRS{idProduct}=="4ee7",MODE="0666"' >> /etc/udev/rules.d/70-android.rules
	```
	
	> idVendor idProduct 需要修改爲 自己的 usb 設備id

1. 重啓 udev 服務 和 adb server

	```sh
	sudo systemctl restart udev
	sudo adb kill-server
	sudo adb start-server
	```

1. 重新 插入手機