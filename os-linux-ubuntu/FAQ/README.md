# 刪除 舊 kernel

查詢 所有 kernel
```
dpkg --get-selections | grep linux-image
```

查詢當前 使用的 kernel

```
uname -r
```

刪除 舊 kernel
```
sudo apt purge linux-image-4.4.0-67-generic
```

#  .bashrc 無效

ubuntu 在 用戶 登入時不會 加載 **~/.bashrc** 而是加載 **~/.profile**

可以自己 在 **.profile** 中 添加 如下代碼 來加載 **.bashrc**

```
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi
```

# 靜態 ip

編輯配置檔案
```
sudo vi /etc/netplan/XXX.yaml
```

配置靜態ip
```
# This is the network config written by 'subiquity'
network:
  ethernets:
    eno1:
      addresses:
      - 192.168.251.40/24
      gateway4: 192.168.251.1
      nameservers:
        addresses:
        - 114.114.114.114
        search: []
    enp2s0:
      dhcp4: true
  version: 2
```

使用配置生效
```
sudo netplan apply
```

確認網路設置
```
ip addr
```