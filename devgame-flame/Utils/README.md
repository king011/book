# [Utils](https://docs.flame-engine.org/1.16.0/flame/other/util.html)

在此頁面上，您可以找到一些實用程式類別和方法的文件。

# [Device Class](https://docs.flame-engine.org/1.16.0/flame/other/util.html#device-class)

可以從 Flame.device 存取此類，它有一些可用於控制裝置狀態的方法，例如您可以更改螢幕方向並設定應用程式是否應全螢幕。

## [Flame.device.fullScreen()](https://docs.flame-engine.org/1.16.0/flame/other/util.html#flame-device-fullscreen)

呼叫時，這會停用所有 SystemUiOverlay，使應用程式全螢幕顯示。 當在主方法中呼叫時，它會使您的應用程式全螢幕顯示（沒有頂部或底部欄）。

**注意**：在網頁上呼叫沒有效果。

## [Flame.device.setLandscape()](https://docs.flame-engine.org/1.16.0/flame/other/util.html#flame-device-setlandscape)

此方法將整個應用程式（實際上也是遊戲）的方向設置為橫向，並且根據作業系統和裝置設置，應允許左橫向和右橫向。 若要將應用程式方向設定為特定方向的橫向，請使用 Flame.device.setLandscapeLeftOnly 或 Flame.device.setLandscapeRightOnly。

**注意**：在網頁上呼叫沒有效果。

## [Flame.device.setPortrait()](https://docs.flame-engine.org/1.16.0/flame/other/util.html#flame-device-setportrait)

此方法將整個應用程式（實際上也是遊戲）的方向設置為縱向，並且根據作業系統和裝置設置，它應該允許向上和向下縱向方向。 若要將應用程式方向設定為特定方向的縱向，請使用 Flame.device.setPortraitUpOnly 或 Flame.device.setPortraitDownOnly。

**注意**：在網頁上呼叫沒有效果。

## [Flame.device.setOrientation() and Flame.device.setOrientations()](https://docs.flame-engine.org/1.16.0/flame/other/util.html#flame-device-setorientation-and-flame-device-setorientations)

如果需要對允許的方向進行更精細的控制（無需直接處理 SystemChrome），可以使用 setOrientation（接受單一 DeviceOrientation 作為參數）和 setOrientations（接受 List&lt;DeviceOrientation&gt; 作為可能的方向）

**注意**：在網頁上呼叫沒有效果。

# [Timer](https://docs.flame-engine.org/1.16.0/flame/other/util.html#timer)

Flame 提供了一個簡單的實用程式類別來幫助您處理倒數計時和計時器狀態變更（例如事件）。

Countdown example:

```
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class MyGame extends Game {
  final TextPaint textPaint = TextPaint(
    style: const TextStyle(color: Colors.white, fontSize: 20),
  );

  final countdown = Timer(2);

  @override
  void update(double dt) {
    countdown.update(dt);
    if (countdown.finished) {
      // Prefer the timer callback, but this is better in some cases
    }
  }

  @override
  void render(Canvas canvas) {
    textPaint.render(
      canvas,
      "Countdown: ${countdown.current.toString()}",
      Vector2(10, 100),
    );
  }
}
```

Interval example:

```
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class MyGame extends Game {
  final TextPaint textPaint = TextPaint(
    style: const TextStyle(color: Colors.white, fontSize: 20),
  );
  Timer interval;

  int elapsedSecs = 0;

  MyGame() {
    interval = Timer(
      1,
      onTick: () => elapsedSecs += 1,
      repeat: true,
    );
  }

  @override
  void update(double dt) {
    interval.update(dt);
  }

  @override
  void render(Canvas canvas) {
    textPaint.render(canvas, "Elapsed time: $elapsedSecs", Vector2(10, 150));
  }
}
```

透過使用 TimerComponent 類，也可以在 FlameGame 遊戲中使用 Timer 實例。

TimerComponent example:

```
import 'package:flame/timer.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';

class MyFlameGame extends FlameGame {
  MyFlameGame() {
    add(
      TimerComponent(
        period: 10,
        repeat: true,
        onTick: () => print('10 seconds elapsed'),
      )
    );
  }
}
```

# [Time Scale](https://docs.flame-engine.org/1.16.0/flame/other/util.html#time-scale)

在許多遊戲中，通常需要根據某些遊戲事件創建慢動作或快轉效果。 實現這些結果的一個非常常見的方法是操縱遊戲時間或滴答率。

為了讓這種操作更容易，Flame 提供了 HasTimeScale mixin。 該 mixin 可以附加到任何 Flame 元件，並公開一個簡單的 timeScale 獲取/設定 API。 timeScale 的預設值為 1，表示組件的遊戲時間以與現實生活時間相同的速度運作。 將其設為 2 將使組件的滴答速度加快兩倍，將其設為 0.5 將使組件的滴答速度減半。

由於 FlameGame 也是一個元件，因此該 mixin 也可以附加到 FlameGame。 這樣做將允許從一個地方控制遊戲所有組件的時間尺度。

```
import 'package:flame/components.dart';
import 'package:flame/game.dart';

class MyFlameGame extends FlameGame with HasTimeScale {
  void speedUp(){
    timeScale = 2.0;
  }

  void slowDown(){
    timeScale = 1.0;
  }
}
```

# [Extensions](https://docs.flame-engine.org/1.16.0/flame/other/util.html#extensions)

Flame 捆綁了一系列實用擴展，這些擴展旨在幫助開發人員使用快捷方式和轉換方法，在這裡您可以找到這些擴展的摘要。

它們都可以從 **package:flame/extensions.dart** 導入

## [Canvas](https://docs.flame-engine.org/1.16.0/flame/other/util.html#canvas)

Methods:

* **scaleVector**: 就像 canvas scale 方法一樣，但採用 Vector2 作為參數
* **translateVector**: 就像 canvas translate 方法一樣，但採用 Vector2 作為參數
* **renderPoint**: 在 canvas 上渲染單點（主要用於調試目的）
* **renderAt** 和 **renderRotated**: 如果您直接渲染到 Canvas，則可以使用這些函數輕鬆操作座標以將內容渲染到正確的位置。 他們更改 Canvas 變換矩陣，但隨後重置

## [Color](https://docs.flame-engine.org/1.16.0/flame/other/util.html#color)

Methods:

* **darken**: 將顏色的陰影變暗 0 到 1 之間的量
* **brighten**: 將顏色的陰影增亮 0 到 1 之間的量

Factories:

* **ColorExtension.fromRGBHexString**: 從有效的十六進位字串（例如#1C1C1C）解析RGB顏色
* **ColorExtension.fromARGBHexString**: 從有效的十六進位字串（例如#FF1C1C1C）中解析ARGB顏色

## [Image](https://docs.flame-engine.org/1.16.0/flame/other/util.html#image)

Methods:
* **PixelsInUint8**: 以 Uint8List 形式擷取影像的像素資料（採用 ImageByteFormat.rawRgba 像素格式）
* **getBoundingRect**: 取得影像的邊界矩形作為矩形
* **size**: 作為 Vector2 的圖像的大小
* **darken**: 將影像的每個像素變暗 0 到 1 之間的量
* **brighten**: 將影像的每個像素增亮 0 到 1 之間的量

## [Offset](https://docs.flame-engine.org/1.16.0/flame/other/util.html#offset)

Methods:
* **toVector2**: 從 Offset 建立 Vector2
* **toSize**: 從偏移量建立 Size
* **toPoint**: 從偏移創建 Point
* **toRect**: 建立一個從(0,0)開始的 Rect，其右下角是 Offset

## [Rect](https://docs.flame-engine.org/1.16.0/flame/other/util.html#rect)

Methods:

* **toOffset**: 由 Rect 創建一個 Offset
* **toVector2**: 建立一個從 (0,0) 開始的 Vector2，並達到 Rect 的大小
* **containsPoint** 返回這個 Rect 是否包含 Vector2 指定的點
* **intersectsSegment**: 返回兩個 Vector2 指定的線段是否與這個 Rect 相交
* **intersectsLineSegment**: 返回線段是否與 Rect 相交
* **toVertices**: 返回 Rect 四個角落的 Vector2 數組
* **toFlameRectangle**: 將 Rect 轉換為爲 Flame Rectangle
* **toMathRectangle**: 將此 Rect 轉換為 math.Rectangle
* **toGeometryRectangle**: 將此 矩形 轉換為來自 Flame-geom 的矩形
* **transform**: 使用 Matrix4 變換 Rect

Factories:

* **RectExtension.getBounds**: 創建一個 Rect，它的四個點由 Vector2 數組組成
* **RectExtension.fromCenter**: 創建一個 Rect，它的中心點由 Vector2 指定

## [math.Rectangle](https://docs.flame-engine.org/1.16.0/flame/other/util.html#math-rectangle)

Methods:

* **toRect**: 將此 math 矩形轉換為 ui 矩形

## [Size](https://docs.flame-engine.org/1.16.0/flame/other/util.html#size)

Methods:

* **toVector2**: 由 Size 創建一個 Vector2
* **toOffset**: 由 Size 創建一個 Offset
* **toPoint**: 由 Size 創建一個 Point
* **toRect**: 創建一個從 (0,0) 開始大小爲 Size 的 Rect

## [Vector2](https://docs.flame-engine.org/1.16.0/flame/other/util.html#vector2)

這個類別來自 vector\_math 套件，除了該套件提供的功能之外，我們還有一些有用的擴充方法

Methods:

* **toOffset**: 由 Vector2 創建一個 Offset
* **toPoint**: 由 Vector2 創建一個 Point
* **toRect**: 創建一個從 (0,0) 開始大小爲 Vector2 的 Rect
* **toPositionedRect**: 在 Vector2 中建立一個從 (x, y) 開始的 Rect，其大小為 Vector2 參數
* **lerp**: 將 Vector2 線性內插到另一個 Vector2
* **rotate**: 以弧度指定的角度旋轉 Vector2，它圍繞可選定義的 Vector2 旋轉，否則圍繞中心旋轉
* **scaleTo**: 將 Vector2 的長度改為提供的長度，而不會改變方向

Factories:

* **Vector2Extension.fromInts**: 建立一個以整數作為輸入的 Vector2

Operators:

* **&amp;**: 將兩個Vector2組合起來形成一個長方形，原點應該在左邊，尺寸在右邊
* **%**: 兩個 Vector2 分別對 x 和 y 求模/求餘


## [Matrix4](https://docs.flame-engine.org/1.16.0/flame/other/util.html#matrix4)

該類別來自 vector\_math 套件。 我們在 vector\_math 已經提供的基礎上建立了一些擴充方法

Methods:

* **translate2**: 透過給定的 Vector2 平移 Matrix4
* **transform2**: 透過使用 Matrix4 轉換給定的 Vector2 來建立新的 Vector2
* **transformed2**: 將輸入 Vector2 轉換為輸出 Vector2

Getters:

* **m11**: 第一行和第一列
* **m12**: 第一行第二列
* **m13**: 第一行第三列
* **m14**: 第一行第四列
* **m21**: 第二行和第一列
* **m22**: 第二行和第二列
* **m23**: 第二行和第三列
* **m24**: 第二行和第四列
* **m31**: 第三行和第一列
* **m32**: 第三行和第二列
* **m33**: 第三行和第三列
* **m34**: 第三行和第四列
* **m41**: 第四行和第一列
* **m42**: 第四行和第二列
* **m43**: 第四行和第三列
* **m44**: 第四行和第四列

Factories:

* **Matrix4Extension.scale**: 建立一個縮放後的 Matrix4。 透過傳遞 Vector4 或 Vector2 作為第一個參數，或傳遞 x y z 雙精度浮點數
