# gopsutil

github.com/shirou/gopsutil 是移植自 [psutil](https://github.com/giampaolo/psutil) 的開源(BSD)項目

這個項目主要用於獲取各種硬件信息，並且所有代碼都沒有使用到 cgo 所以交叉編譯變得很方便

* 源碼 [https://github.com/shirou/gopsutil](https://github.com/shirou/gopsutil)

```
go get github.com/shirou/gopsutil/v3
```

```
package main

import (
    "fmt"

    "github.com/shirou/gopsutil/v3/mem"
    // "github.com/shirou/gopsutil/mem"  // to use v2
)

func main() {
    v, _ := mem.VirtualMemory()

    // almost every return value is a struct
    fmt.Printf("Total: %v, Free:%v, UsedPercent:%f%%\n", v.Total, v.Free, v.UsedPercent)

    // convert to JSON. String() is also implemented
    fmt.Println(v)
}
```

# 子包

gopsutil 將不同的功能劃分到不同的子包中

* cpu
* disk
* docker
* host
* mem
* net
* process
* winservices