# 信號

在 bash 環境中 進程 可以通過 signal 來相互 通信 以完成 一些 合作

使用 kill signal pid/job 來向一個 進程 發送 信號

kill -l 指令 列出來所有的 可用 信號

```
kill -9 %1
kill -SIGKILL %1
```

```
kill -l
 1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
 6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1
11) SIGSEGV	12) SIGUSR2	13) SIGPIPE	14) SIGALRM	15) SIGTERM
16) SIGSTKFLT	17) SIGCHLD	18) SIGCONT	19) SIGSTOP	20) SIGTSTP
21) SIGTTIN	22) SIGTTOU	23) SIGURG	24) SIGXCPU	25) SIGXFSZ
26) SIGVTALRM	27) SIGPROF	28) SIGWINCH	29) SIGIO	30) SIGPWR
31) SIGSYS	34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
63) SIGRTMAX-1	64) SIGRTMAX
```

# trap

在 bash 中 使用 trap 指令 來 捕獲信號 腳本可以 自定義 如何 處理信號

trap 'command_list'  signals

但 下面幾個 信號 無法被捕獲

* SIGKILL (9)
* SIGSTOP (17)
* SIGCONT (19)

下面信號是 通常 會被腳本處理的

* SIGHUP (1) 從終端終止或退出正在前臺運行的進程
* SIGINT (2) 按下鍵盤的 ctrl-c
* SIGQUIT(3)  按下鍵盤的 ctrl-\
* SIGTERM(15) 軟件終止信號


# 忽略信號

使用 `trap ''  signals` 來 忽略 信號

使用 `trap - signals` 來 重置 信號

```
#!/bin/bash
trap '' SIGINT SIGQUIT

for ((x=0;x<10;x=x+1))
do
    sleep 1
    echo $x
done
```

```
#!/bin/bash
function Exit(){
    echo exit script
    exit $1
}
#trap 'echo exit script;exit 1' SIGINT SIGQUIT SIGTERM SIGHUP
trap 'Exit 1' SIGINT SIGQUIT SIGTERM SIGHUP

for ((x=0;x<10;x=x+1))
do
    sleep 1
    echo $x
done
```
