# nodejs
node.js 是一個 開源(MIT) 快速(v8) 跨平臺 的服務器解決方案  
對 google 的 v8 進行包裝 爲js 提供了 服務器 等環境支持

* 官網 [https://nodejs.org/en/](https://nodejs.org/en/)
* wiki [https://zh.wikipedia.org/wiki/Node.js](https://zh.wikipedia.org/wiki/Node.js)
* 源碼 [https://github.com/nodejs/node](https://github.com/nodejs/node)

# ubuntu 安裝
1. sudo apt install nodejs-legacy
2. sudo apt install npm
3. sudo npm install -g n
4. sudo n stable

> ubuntu 自帶的 node 版本很久 n 是 nodejs 自帶的一個 nodejs 版本 工具
> 
> n stable 可以 自動更新 nodejs 到最新穩定版本
> 
> n latest 可以 自動更新 nodejs 到最新 版本