# shadowsocksr-libev

shadowsocksr-libev 同樣 提供了 c 語言 實現的 shadowsocksrr 開源(GPL3.0)套件

git clone [https://github.com/shadowsocksrr/shadowsocksr-libev.git](https://github.com/shadowsocksrr/shadowsocksr-libev.git)

# 安裝
## ubuntu
1. sudo apt install gcc make libssl-dev libpcre3-dev zlib1g-dev -y
1. git clone https://github.com/shadowsocksrr/shadowsocksr-libev.git
1. cd shadowsocksr-libev
1. ./configure --disable-documentation
1. make
1. sudo make install

# tools
shadowsocksr-libev 提供了 類似 shadowsocks-libev 的工具包

使用 方式 也基本 相同 只是 現在 增加了 額外的 參數 指定 混淆方式 和 協議

ss-local -c xxx.json
```json
{
	"_comment":"本地 socks5 地址",
	"local_address":"127.0.0.1",
	"_comment":"本地 socks5 端口",
	"local_port": 1080,
 
	"_comment":"服務器地址",
	"server":"xxxx",
	"_comment":"服務器端口",
	"server_port": 14871,
 
	"_comment":"服務器驗證",
	"password":"kate is beauty",
 
	"_comment":"加密方式",
	"method":"chacha20",
 
	"_comment":"協議",
	"protocol":"auth_chain_a",
	"_comment":"協議參數",
	"protocol_param":"",
 
	"_comment":"混淆",
	"obfs":"tls1.2_ticket_auth",
	"_comment":"混淆參數",
	"obfs_param":"",
 
	"fast_open":true
}
```

> ssr 沒有 --reuse-port 參數 默認既 全部 使用 tcp 復用
> 

# ss-manager ss-server and ss-tunnel

自從
```
#info=fasle
commit f208f92f954128acd6c20064defba8920385e8ad
Author: noisyfox <timemanager.rick@gmail.com>
Date:   Fri Jan 27 20:44:01 2017 +1100

    ss_local works on windows
```
開始 shadowsocksr-libev 就 無法 編譯 之後 master 編譯中 除去了 ss-manager ss-server and ss-tunnel 三個組件

要 得到這三個 組件 可以 `git checkout e4e3245` 到前一個 commit 去 編譯
