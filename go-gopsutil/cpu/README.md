# cpu

cpu 子包用於獲取 cpu 相關數據

# Counts

Counts/CountsWithContext 用於獲取 cpu 數量，即可獲取物理 cpu 數(實際 cpu)，也可以獲取邏輯 cpu 數(核心數)

```
package main

import (
	"fmt"
	"log"

	"github.com/shirou/gopsutil/v3/cpu"
)

func main() {
	physical, e := cpu.Counts(false)
	if e != nil {
		log.Fatalln(e)
	}
	logical, e := cpu.Counts(true)
	if e != nil {
		log.Fatalln(e)
	}

	fmt.Println(`物理 cpu:`, physical)
	fmt.Println(`邏輯 cpu:`, logical)
}
```

# Percent
Percent/PercentWithContext 用於統計 cpu 使用率

```
func Percent(interval time.Duration, percpu bool) ([]float64, error) 
func PercentWithContext(ctx context.Context, interval time.Duration, percpu bool) ([]float64, error)
```

* 如果 interval 設置爲0，則返回的是自上次調用以來統計的 cpu 使用率，否則則阻塞 interval 指定的時間來統計 cpu 使用率
* 如果 percpu 爲 true 則數組值只返回一個值爲總的 cpu 使用率，否則返回每個邏輯 cpu 使用率

```
package main

import (
	"fmt"
	"log"
	"time"

	"github.com/shirou/gopsutil/v3/cpu"
)

func main() {
	for {
		vals, e := cpu.Percent(time.Second, true)
		if e != nil {
			log.Fatalln(e)
		}
		fmt.Println(`---------`)
		for i, val := range vals {
			fmt.Printf("%d : %v%%\n", i, val)
		}
	}
}
```

# Info

Info/InfoWithContext 用於返回每個邏輯 cpu 的詳細信息 InfoStat

```
type InfoStat struct {
	// 索引
	CPU        int32    `json:"cpu"`
	// 廠商
	VendorID   string   `json:"vendorId"`
	Family     string   `json:"family"`
	Model      string   `json:"model"`
	Stepping   int32    `json:"stepping"`
	PhysicalID string   `json:"physicalId"`
	// 所屬物理 cpu 索引
	CoreID     string   `json:"coreId"`
	Cores      int32    `json:"cores"`
	// 名稱
	ModelName  string   `json:"modelName"`
	Mhz        float64  `json:"mhz"`
	// 緩存大小
	CacheSize  int32    `json:"cacheSize"`
	Flags      []string `json:"flags"`
	Microcode  string   `json:"microcode"`
}
```

```
package main

import (
	"fmt"
	"log"

	"github.com/shirou/gopsutil/v3/cpu"
)

func main() {
	stats, e := cpu.Info()
	if e != nil {
		log.Fatalln(e)
	}
	for i := 0; i < len(stats); i++ {
		fmt.Println(i, stats[i])
	}
}
```