# path_provider

[path_provider](https://pub.dev/packages/path_provider) 用於查找 android/iso 上 檔案常用位置

```
flutter pub add path_provider
```

```
import 'package:path_provider/path_provider.dart';

final Directory tempDir = await getTemporaryDirectory();

final Directory appDocumentsDir = await getApplicationDocumentsDirectory();

final Directory? downloadsDir = await getDownloadsDirectory();
```