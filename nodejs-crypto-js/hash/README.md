# hash

```
import sha256 from 'crypto-js/sha256';
import hex from 'crypto-js/enc-hex';
import base64 from 'crypto-js/enc-base64';
import base64url from 'crypto-js/enc-base64url';

const digest = sha256("cerberus is an idea")
console.log('hex', hex.stringify(digest))
console.log('base64', base64.stringify(digest))
console.log('base64url', base64url.stringify(digest))
```