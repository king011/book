# unity

1. unity 是一款 專有的 2d/3d 遊戲引擎 
2. 核心使用c++開發 邏輯使用 c\# 開發
3. 可以發佈到多個 平臺

* 官網 [https://unity3d.com/](https://unity3d.com/) 
* 手冊 [https://docs.unity3d.com/Manual/index.html](https://docs.unity3d.com/Manual/index.html)
* API [https://docs.unity3d.com/ScriptReference/index.html](https://docs.unity3d.com/ScriptReference/index.html)
* 中文官網 [https://unity.cn/](https://unity.cn/)
* 中文手冊 [https://docs.unity.cn/cn/current/Manual/index.html](https://docs.unity.cn/cn/current/Manual/index.html)
* 中文 API [https://docs.unity.cn/cn/current/ScriptReference/index.html](https://docs.unity.cn/cn/current/ScriptReference/index.html)