# TiledLayer

[cc.TiledLayer](http://docs.cocos.com/creator/api/zh/classes/TiledLayer.html) 封裝了 每個圖層的 圖像信息

> 同個圖層的 TiledTile 只能來自同一張圖像資源 
> 

# 常用方法

```
#info=false
// 返回 指定座標的 gid 如果返回 0 代表此座標 沒有 TiledTile
getTileGIDAt(pos: Vec2|number, y?: number): number

```