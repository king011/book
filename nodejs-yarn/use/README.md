# 初始化一個新項目

```
yarn init
```

# 安裝依賴包

```
yarn add [package]
yarn add [package]@[version]
yarn add [package]@[tag]
```

分別添加到 devDependencies peerDependencies optionalDependencies

```
yarn add [package] --dev
yarn add [package] --peer
yarn add [package] --optional
```

# 移除依賴包

```
yarn remove [package]
```

# 安裝項目全部依賴

```
yarn
```

```
yarn install
```