# 家庭版

windows 家庭版 閹割了許多功能 然其中一些功能可以手動強制打開

# 組策略

1. 創建一個批處理檔案 並寫入如下代碼

	```
	@echo off
	pushd "%~dp0"
	dir /b C:\Windows\servicing\Packages\Microsoft-Windows-GroupPolicy-ClientExtensions-Package~3*.mum >List.txt
	dir /b C:\Windows\servicing\Packages\Microsoft-Windows-GroupPolicy-ClientTools-Package~3*.mum >>List.txt
	for /f %%i in ('findstr /i . List.txt 2^>nul') do dism /online /norestart /add-package:"C:\Windows\servicing\Packages\%%i"
	pause
	```

1. 以管理員身份執行上述批處理

	![](assets/1.png)

1. 執行完畢後即可使用 **gpedit.msc** 打開組策略