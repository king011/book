# net

net 庫 提供了 tcp 和 ipc 的 服務器與客戶端 支持

# class Server

* class Server 提供了 tcp 服務器功能
* class Socket 提供了 tcp 客戶端功能

```
import { promises } from "fs"
import { AddressInfo, Server, Socket } from "net"

// 服務器
const server = new Server()
server.listen(9000,
    'localhost',
    function () {
        const address = server.address() as AddressInfo
        console.log('server listening :', address)
    })
    .on('error', (e) => {
        console.log('server error :', e)
    }).on('connection', (s) => {
        onRead(s)
    }).on('close', () => {
        console.log('server closed', server.address())
    })

function onRead(s: Socket) {
    const address = s.address()
    console.log(`one in`, address)
    s.on('data', (data) => {
        s.write(data)
    }).on('error', (e) => {
        console.log('error', e)
    }).on('close', () => {
        console.log(`one out`, address)
    })
}

class Completer<T> {
    private promise_: Promise<T> = null
    private resolve_: (value?: T | PromiseLike<T>) => void
    private reject_: (reason?: any) => void
    constructor() {
        this.promise_ = new Promise<T>((resolve, reject) => {
            this.resolve_ = resolve
            this.reject_ = reject
        })
    }
    resolve(value?: T | PromiseLike<T>) {
        this.resolve_(value)
    }
    reject(reason?: any) {
        this.reject_(reason)
    }
    get promise(): Promise<T> {
        return this.promise_
    }
}
class Wait {
    private read_: Completer<undefined>
    private drain_: Completer<undefined>
    constructor() {

    }
    waitRead(): Promise<undefined> {
        this.read_ = new Completer<undefined>()
        return this.read_.promise
    }
    doneRead() {
        if (this.read_) {
            this.read_.resolve()
        }
    }
    waitDrain(): Promise<undefined> {
        if (!this.drain_) {
            this.drain_ = new Completer<undefined>()
        }
        return this.drain_.promise
    }
    doneDrain() {
        if (this.drain_) {
            this.drain_.resolve()
            this.drain_ = null
        }
    }
    close() {
        const read = this.read_
        if (read) {
            read.promise.catch(() => { })
            read.reject('closed')
            this.read_ = null
        }
        const drain = this.drain_
        if (drain) {
            drain.promise.catch(() => { })
            drain.reject('closed')
            this.drain_ = null
        }
    }
}

// 客戶端
const wait = new Wait()
const s = new Socket()
s.setEncoding("utf8") //設置 on data 使用 utf8 編碼
s.connect(9000, 'localhost', async () => {
    console.log('connect success')
    try {
        for (let i = 0; i < 10; i++) {
            // 發送消息
            const readOk = wait.waitRead()
            while (true) {
                if (s.destroyed) {
                    return
                }
                const writeReady = wait.waitDrain()
                if (s.write(`echo ${i}`)) {
                    break
                }
                // 發送緩衝區已滿 等待可發送
                await writeReady
            }
            // 等待 接收消息
            await readOk
        }
        console.log('all success')
        s.destroy() // 手動銷毀 socket
    } catch (e) {
        console.log(e)
    }
    console.log('write exit')
}).on('error', (e) => {
    // 在 error 調用前 會自動 destroy 無需手動調用
    console.log('client error :', e)
}).on('data', (data) => {
    console.log(data)
    wait.doneRead() // 通知接收成功
}).on('drain', () => {
    wait.doneDrain()
}).on('close', () => {
    // 在 close 調用前 會自動 destroy 無需手動調用
    wait.close()
})
```