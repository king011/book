# [Ingress](https://kubernetes.io/zh/docs/concepts/services-networking/ingress/)

Ingress 作爲系統的入口可以爲 接入系統提供路由 負載均衡等

# Example

一些路由例子

## 內部網關

使用 Ingress 可以作爲網關爲外部訪問提供服務支持，對於集羣內部如果也想通過相同的 Ingress 作爲內部網關，可以直接在集羣內部訪問集羣 ingress 地址即可將流量導入到 Ingress

```
$ kubectl get service -n ingress-nginx 
NAME                                 TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.100.42.200    <pending>     80:30603/TCP,443:30125/TCP   37d
ingress-nginx-controller-admission   ClusterIP      10.100.241.130   <none>        443/TCP                      37d
```

如果 Ingress 使用了 host 用於識別後端服務則在內部訪問時需要使用相應的 host 域名而不可以使用集羣網關的 ip，目前 kubernetes 使用 coredns 作爲集羣內部的 dns 服務器並且創建了一個 configmap 作爲 coredns 的配置，修改 configmap 在其中將域名設置到 hosts 記錄即可

```
kubectl edit configmaps --namespace kube-system coredns
```

```
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        hosts {
            10.100.42.200  demo-internal
            fallthrough
        }
        prometheus :9153
        forward . /etc/resolv.conf {
           max_concurrent 1000
        }
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
```


## path

這個例子使用 path 爲 http/grpc 進行路由，將創建兩個 不同的 http/grpc 服務 v1 和 v2，使用 ingress-nginx 將 v1 請求發送到 v1 服務器，將 v2 請求發送到 v2 服務器

1. 創建一個 namespace 用於演示

	```
	#info="namespace.yaml"
	apiVersion: v1
	kind: Namespace
	metadata:
		name: demo-ingress
		labels:
			app: demo-ingress
	```
	
	```
	kubectl apply -f namespace.yaml
	```

2. 創建 v1 v2 應用以提供具體的功能

	```
	#info="deployment.yaml"
	# 創建 v1 應用
	apiVersion: apps/v1
	kind: Deployment
	metadata:
			namespace: demo-ingress
			name: demo-v1
			labels:
					app: demo-v1
	spec:
			replicas: 2
			selector:
					matchLabels:
							app: demo-v1
			template: # 容器模板
					metadata:
							labels:
									app: demo-v1
					spec:
							containers:
									- image: king011/k8s-ingress-nginx-example-server:0.0.2
										name: demo-v1
										env:
												- name: ExampleAddr
													value: ":9000"
	---
	# 創建 v2 應用
	apiVersion: apps/v1
	kind: Deployment
	metadata:
			namespace: demo-ingress
			name: demo-v2
			labels:
					app: demo-v2
	spec:
			replicas: 2
			selector:
					matchLabels:
							app: demo-v2
			template: # 容器模板
					metadata:
							labels:
									app: demo-v2
					spec:
							containers:
									- image: king011/k8s-ingress-nginx-example-server:0.0.2
										name: demo-v2
										env:
												- name: ExampleAddr
													value: ":9000"
	```
	
	```
	kubectl apply -f deployment.yaml
	```

3. 創建 v1 v2 服務，以暴露 v1 v2 提供的功能

	```
	#info="service.yaml"
	# 爲 v1 創建服務
	apiVersion: v1
	kind: Service
	metadata:
		namespace: demo-ingress
		name: demo-v1
		labels:
			app: demo-v1
	spec:
		selector:
			app: demo-v1
		ports:
			- name: service
				port: 9000
				targetPort: 9000
	---
	# 爲 v2 創建服務
	apiVersion: v1
	kind: Service
	metadata:
		namespace: demo-ingress
		name: demo-v2
		labels:
			app: demo-v2
	spec:
		selector:
			app: demo-v2
		ports:
			- name: service
				port: 9000
				targetPort: 9000
	```

	```
	kubectl apply -f service.yaml
	```

4. 創建一個 ingress 爲 v1 v2 服務提供反向代理

	```
	#info="ingress.yaml"
	# 爲 http 創建的 ingress
	apiVersion: networking.k8s.io/v1
	kind: Ingress
	metadata:
		namespace: demo-ingress
		name: demo-http-ingress
		labels:
			app.kubernetes.io/component: controller
		annotations:
			nginx.ingress.kubernetes.io/rewrite-target: /api/$1/$2 # $參數來自正則替換
	spec:
		ingressClassName: nginx
		rules:
			- http:
					paths:
						- pathType: Prefix
							path: /api/(v1)/(.*)
							backend:
								service:
									name: demo-v1
									port:
										number: 9000
						- pathType: Prefix
							path: /api/(v2)/(.*)
							backend:
								service:
									name: demo-v2
									port:
										number: 9000
	---
	# 爲 grpc 創建的 ingress
	apiVersion: networking.k8s.io/v1
	kind: Ingress
	metadata:
		namespace: demo-ingress
		name: demo-grpc-ingress
		labels:
			app.kubernetes.io/component: controller
		annotations:
			nginx.ingress.kubernetes.io/rewrite-target: /jsgenerate_server.api.$1/$2 # $參數來自正則替換
			nginx.ingress.kubernetes.io/grpc-backend: "true"
			nginx.ingress.kubernetes.io/backend-protocol: "GRPC"
			nginx.ingress.kubernetes.io/proxy-body-size: 0
	spec:
		ingressClassName: nginx
		rules:
			- http:
					paths:
						- pathType: Prefix
							path: /jsgenerate_server.api.(V1)/(.*)
							backend:
								service:
									name: demo-v1
									port:
										number: 9000
						- pathType: Prefix
							path: /jsgenerate_server.api.(V2)/(.*)
							backend:
								service:
									name: demo-v2
									port:
										number: 9000
	```
	
	> 即時是 grpc stream 其傳輸 body 總量依然會受到 client_max_body_size 影響，可以設置 nginx.ingress.kubernetes.io/proxy-body-size 註解將 client_max_body_size 設置爲0(不限制)
	
	```
	kubectl apply -f ingress.yaml
	```
	
5. 現在將 ingress-nginx-controller 代理到本地9000來測試(**注意** nginx無法對外提供 h2c 服務所以只能通過 443 使用 h2 協議訪問 grpc，nginx 支持 h2c 後端所以 grpc 服務本身可以是 h2c 的)

	```
	kubectl port-forward --namespace=ingress-nginx service/ingress-nginx-controller 9000:443
	```
	
	另外也可以通過 ingress-nginx-controller 對外暴露的 ip 端進行服務訪問
	```
	kubectl get service --namespace=ingress-nginx  ingress-nginx-controller
	```