# [Images, Sprites and Animations](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html)

首先，您必須具有適當的資料夾結構並將文件新增至 pubspec.yaml 檔案中，如下所示：

```
flutter:
  assets:
    - assets/images/player.png
    - assets/images/enemy.png
```

圖片可以是 Flutter 支援的任何格式，包括：JPEG、WebP、PNG、GIF、動畫 GIF、動畫 WebP、BMP 和 WBMP。 其他格式需要額外的庫。 例如，SVG 映像可以透過 Flame\_svg 庫載入

# [Loading images](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#loading-images)

Flame 捆綁了一個名為 Images 的實用程式類，它允許您輕鬆地將映像從資源目錄載入並快取到記憶體中。

Flutter 有一些與圖像相關的類型，將所有內容從本地資產正確轉換為可以在 Canvas 上繪製的圖像有點複雜。 此類別可讓您取得可使用drawImageRect 方法在Canvas 上繪製的影像。

它會自動快取按檔案名稱載入的任何圖像，因此您可以安全地多次呼叫它。

載入和清除快取的方法有：load、loadAll、clear、clearCache。 他們返回 Futures 以加載圖像。 在以任何方式使用圖像之前，必須等待這些 Futures。 如果您不想立即等待這些 future，您可以啟動多個 load() 操作，然後使用 Images.ready() 方法一次等待所有這些操作。

若要同步檢索先前快取的映像，可以使用 fromCache 方法。 如果之前未載入具有該鍵的圖像，則會拋出異常。

若要將已經載入的映像新增至快取中，可以使用 add 方法，並且可以設定該映像在快取中應具有的鍵。

您也可以使用 ImageExtension.fromPixels() 在遊戲過程中動態建立影像。

對於clear和clearCache，請注意，對於從快取中刪除的每個映像都會呼叫dispose，因此請確保之後不再使用該映像。

## [Standalone usage](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#standalone-usage)

可以透過實例化來手動使用它：

```
import 'package:flame/cache.dart';
final imagesLoader = Images();
Image image = await imagesLoader.load('yourImage.png');
```

但是 Flame 還提供了兩種使用此類的方法，而無需您自己實例化它。

## [Flame.images](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#flame-images)

Flame 類別提供了一個單例，可以用作全域影像快取。

Example:

```
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';

// inside an async context
Image image = await Flame.images.load('player.png');

final playerSprite = Sprite(image);
```

## [Game.images](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#game-images)

Game 類別也提供了一些用於處理映像載入的實用方法。 它捆綁了 Images 類別的一個實例，可用於載入遊戲期間使用的圖像資源。 當遊戲小部件從小部件樹中刪除時，遊戲將自動釋放快取。

Game 類別中的 onLoad 方法是載入初始資源的好地方。

Example:

```
class MyGame extends Game {

  Sprite player;

  @override
  Future<void> onLoad() async {
    // Note that you could also use Sprite.load for this.
    final playerImage = await images.load('player.png');
    player = Sprite(playerImage);
  }
}
```

遊戲運行時也可以透過 images.fromCache 檢索載入的資源，例如：

```
class MyGame extends Game {

  // attributes omitted

  @override
  Future<void> onLoad() async {
    // other loads omitted
    await images.load('bullet.png');
  }

  void shoot() {
    // This is just an example, in your game you probably don't want to
    // instantiate new [Sprite] objects every time you shoot.
    final bulletSprite = Sprite(images.fromCache('bullet.png'));
    _bullets.add(bulletSprite);
  }
}
```

# [Loading images over the network](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#loading-images-over-the-network)

Flame 核心包不提供從網路載入圖片的內建方法。

原因是 Flutter/Dart 沒有內建的 http 用戶端，這需要使用一個包，而且由於有幾個可用的包，我們不會強迫使用者使用特定的包。

話雖如此，一旦用戶選擇了 http 用戶端包，從網頁加載圖像就非常簡單了。 以下程式碼片段顯示如何使用 [http](https://pub.dev/packages/http) 套件從網路取得圖像。

```
import 'package:http/http.dart' as http;
import 'package:flutter/painting.dart';

final response = await http.get('https://url.com/image.png');
final image = await decodeImageFromList(response.bytes);
```

> 檢查 [Flame\_network\_assets](https://pub.dev/packages/flame_network_assets) 以獲得可立即使用的網路資產解決方案，該解決方案提供內建快取

# [Sprite](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#sprite)

Flame 提供了一個代表圖像或圖像區域的 Sprite 類別。

您可以透過提供 Image 和定義該精靈所代表的圖像片段的座標來建立精靈。

例如，這將創建一個代表所傳遞文件的整個圖像的精靈：

```
final image = await images.load('player.png');
Sprite player = Sprite(image);
```

您也可以指定精靈在原始影像中所在的座標。 這允許您使用精靈表並減少記憶體中的圖像數量，例如：

```
final image = await images.load('player.png');
final playerFrame = Sprite(
  image,
  srcPosition: Vector2(32.0, 0),
  srcSize: Vector2(16.0, 16.0),
);
```

srcPosition 的預設值為 (0.0, 0.0)，srcSize 的預設值為 null（表示它將使用來源影像的完整寬度/高度）。

Sprite 類別有一個 render 方法，可讓您將 sprite 渲染到 Canvas 上：

```
final image = await images.load('block.png');
Sprite block = Sprite(image);

// in your render method
block.render(canvas, 16.0, 16.0); //canvas, width, height
```

您必須將大小傳遞給渲染方法，並且圖像將相應地調整大小。

Sprite 類別中的所有渲染方法都可以接收 Paint 實例作為可選的命名參數 overridePaint，該參數將覆寫該渲染呼叫的目前 Sprite 繪製實例。

Sprite 也可以用作小部件，只需使用 SpriteWidget 類別。

可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/widgets/sprite_widget_example.dart)找到使用精靈作為小部件的完整範例。

# [SpriteBatch](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#spritebatch)

如果您有一個精靈表（也稱為圖像圖集，它是內部包含較小圖像的圖像），並且希望有效地渲染它 - SpriteBatch 會為您處理該工作。

為其指定影像的檔案名，然後新增描述影像各個部分的矩形，以及變換（位置、縮放和旋轉）和可選顏色。

您可以使用 Canvas 和可選的 Paint、BlendMode 和 CullRect 來渲染它。

為了方便起見，也可以使用 SpriteBatchComponent。

請參閱[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/sprites/sprite_batch_example.dart)的範例。

# [ImageComposition](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#imagecomposition)

在某些情況下，您可能希望將多個影像合併為單一影像； 這稱為合成。 例如，當使用 SpriteBatch API 來優化繪圖呼叫時，這可能很有用。

對於此類用例，Flame 附帶了 ImageComposition 類別。 這允許您將多個圖像添加到新圖像上，每個圖像都有自己的位置：

```
final composition = ImageComposition()
  ..add(image1, Vector2(0, 0))
  ..add(image2, Vector2(64, 0));
  ..add(image3,
    Vector2(128, 0),
    source: Rect.fromLTWH(32, 32, 64, 64),
  );
  
Image image = await composition.compose();
Image imageSync = composition.composeSync();
```

如您所見，有兩個版本的合成圖像可用。 使用 ImageComposition.compose() 作為非同步方法。 或使用新的 ImageComposition.composeSync() 函數，利用 Picture.toImageSync 函數的優勢將影像光柵化到 GPU 上下文中。

**注意**：合成影像的成本很高，我們不建議您每次都執行此操作，因為它會嚴重影響效能。 相反，我們建議您預先渲染您的作品，以便您可以重複使用輸出影像。

# [Animation](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#animation)

Animation 類別可協助您建立精靈的循環動畫。

您可以透過傳遞大小相等的精靈清單和stepTime（即移動到下一幀需要多少秒）來創建它：

```
final a = SpriteAnimationTicker(SpriteAnimation.spriteList(sprites, stepTime: 0.02));
```

建立動畫後，您需要呼叫其更新方法並在遊戲實例上渲染目前幀的精靈。

Example:

```
class MyGame extends Game {
  SpriteAnimationTicker a;

  MyGame() {
    a = SpriteAnimationTicker(SpriteAnimation(...));
  }

  void update(double dt) {
    a.update(dt);
  }

  void render(Canvas c) {
    a.getSprite().render(c);
  }
}
```

產生精靈清單的更好替代方法是使用 fromFrameData 建構子：

```
const amountOfFrames = 8;
final a = SpriteAnimation.fromFrameData(
    imageInstance,
    SpriteAnimationFrame.sequenced(
      amount: amountOfFrames,
      textureSize: Vector2(16.0, 16.0),
      stepTime: 0.1,
    ),
);
```

使用精靈表時，此建構函式可以非常輕鬆地建立動畫。

在建構函式中，您傳遞影像實例和幀數據，其中包含一些可用於描述動畫的參數。 檢查 SpriteAnimationFrameData 類別上可用建構函式的文件以查看所有參數。

如果您使用 Aseprite 製作動畫，Flame 確實為 Aseprite 動畫的 JSON 資料提供一些支援。 要使用此功能，您需要匯出 Sprite Sheet 的 JSON 數據，並使用類似以下程式碼片段的內容：

```
final image = await images.load('chopper.png');
final jsonData = await assets.readJson('chopper.json');
final animation = SpriteAnimation.fromAsepriteData(image, jsonData);
```

**注意**：Flame 不支援修剪後的精靈表，因此如果您以這種方式匯出精靈表，它將具有修剪後的尺寸，而不是精靈的原始尺寸。

動畫創建後有 update 和 render 方法；render 渲染當前幀，update 滴答內部時鐘以更新幀。

動畫通常在 SpriteAnimationComponents 內部使用，但也可以建立具有多個動畫的自訂元件。

可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/widgets/sprite_animation_widget_example.dart)找到使用動畫作為小部件的完整範例。

# [SpriteSheet](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#spritesheet)

精靈表是大圖像，上面有同一精靈的多個幀，是組織和儲存動畫的好方法。 Flame 提供了一個非常簡單的實用程式類別來處理 SpriteSheets，使用它您可以載入 Sprite Sheet 圖像並從中提取動畫。 以下是如何使用它的一個非常簡單的範例：

```
import 'package:flame/sprite.dart';

final spriteSheet = SpriteSheet(
  image: imageInstance,
  srcSize: Vector2.all(16.0),
);

final animation = spriteSheet.createAnimation(0, stepTime: 0.1);
```

現在您可以直接使用動畫或在動畫組件中使用它。

您也可以透過使用 SpriteSheet.createFrameData 或 SpriteSheet.createFrameDataFromId 來擷取單一 SpriteAnimationFrameData 來建立自訂動畫：

```
final animation = SpriteAnimation.fromFrameData(
  imageInstance, 
  SpriteAnimationData([
    spriteSheet.createFrameDataFromId(1, stepTime: 0.1), // by id
    spriteSheet.createFrameData(2, 3, stepTime: 0.3), // row, column
    spriteSheet.createFrameDataFromId(4, stepTime: 0.1), // by id
  ]),
);
```

如果您不需要任何類型的動畫，而只需要 SpriteSheet 上的 Sprite 實例，則可以使用 getSprite 或 getSpriteById 方法：

```
spriteSheet.getSpriteById(2); // by id
spriteSheet.getSprite(0, 0); // row, column
```

您可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/sprites/sprite_sheet_example.dart)查看 SpriteSheet 類別的完整範例