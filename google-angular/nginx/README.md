# nginx

使用 nginx 可以 方便的 將 Angular 和 後端服務器 路由到一起

## production

```
server{
    listen 80 default;

    location / {
        try_files $uri $uri/ /index.html;

        alias /var/www/angular;
    }
}
```

## dev
**/etc/hosts**
```text
127.0.0.1	dev.my.web
```

**/etc/nginx/nginx.conf**
```
	# 配置一個開發環境
	server {
		listen  80;
		server_name     dev.my.web;
 
		# 轉發 angular
		location / {
			proxy_http_version 1.1;
			proxy_set_header Host $http_host;

			# Allow websocket connections
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection $http_connection;

			proxy_set_header X-Real-IP              $remote_addr;                         

			proxy_set_header X-Forwarded-For        $remote_addr;

			proxy_set_header X-Forwarded-Host       $http_host;                      
			proxy_set_header X-Forwarded-Port       $server_port;                           
			proxy_set_header X-Forwarded-Proto      $scheme;                                  
			proxy_set_header X-Forwarded-Scheme     $scheme;                  

			proxy_set_header X-Scheme               $scheme;  

			# Pass the original X-Forwarded-For
			proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;

			proxy_pass http://localhost:4200;
		}
		# 轉發到後端請求 
		location /api/ {
			proxy_http_version 1.1;
			proxy_set_header Host $http_host;

			# Allow websocket connections
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection $http_connection;

			proxy_set_header X-Real-IP              $remote_addr;                         

			proxy_set_header X-Forwarded-For        $remote_addr;

			proxy_set_header X-Forwarded-Host       $http_host;                      
			proxy_set_header X-Forwarded-Port       $server_port;                           
			proxy_set_header X-Forwarded-Proto      $scheme;                                  
			proxy_set_header X-Forwarded-Scheme     $scheme;                  

			proxy_set_header X-Scheme               $scheme;  

			# Pass the original X-Forwarded-For
			proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;
			
			proxy_pass http://localhost:9000;
		}
	}
```