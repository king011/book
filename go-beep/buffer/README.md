# buffer

been.Buffer 類似 bytes.Buffer 但它不存放字節 而是存放 樣本，這對於小檔案或要大量重複或同時播放的音效很有效果

```
package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func main() {
	// 打開源
	f, e := os.Open("a.mp3")
	if e != nil {
		log.Fatal(e)
	}
	defer f.Close()
	// 調用解碼器創建 streamer
	streamer, format, e := mp3.Decode(f)
	if e != nil {
		log.Fatal(e)
	}
	fmt.Println(format)

	// 創建緩衝區
	buffer := beep.NewBuffer(format)
	// 添加 流 可以多次調用以將多個流加入緩衝區
	buffer.Append(streamer)
	// 一旦加入緩衝區 就可以關閉 流
	streamer.Close()

	// 初始化揚聲器
	// 更大的緩衝區意味着更低的 cpu 使用率和更可靠的播放，更少的緩衝區意味着更快的響應和更少的延遲
	e = speaker.Init(
		format.SampleRate,                   // 採樣率
		format.SampleRate.N(time.Second/10), // N 計算 持續時間內的 樣本數，此處傳入了 100ms 的樣本數
	)
	if e != nil {
		log.Fatalln(e)
	}

	for {
		fmt.Println("Press [ENTER] to play! ")
		fmt.Scanln()

		// 從流中 取出指定 範圍的 樣本
		streamer := buffer.Streamer(0, buffer.Len())
		speaker.Play(streamer)
	}
}
```