# gitlab

gitlab 是一個 使用 ruby 和 golang 實現的 linux下 開源(MIT)的 git web 解決方案

同時 官方爲開源社區 提供了 免費的 項目管理 功能

* 官網 [https://about.gitlab.com/](https://about.gitlab.com/)
* 源碼 [https://gitlab.com/gitlab-org/gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce)