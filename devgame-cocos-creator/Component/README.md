# [腳本組件](https://docs.cocos.com/creator/manual/zh/scripting/)

* creator 使用 ts 創建 腳本組件 新建一個 腳本 創建一個 [cc.Component](http://docs.cocos.com/creator/api/zh/classes/Component.html) 的子類 即實現一個 組件 
* 腳本 通常 存儲在 Scripts 檔案夾下

```
#info="默認腳本"
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    // update (dt) {}
}
```

1. 第11行 引入了 cocos 實現的 tag
2. @ccclass 標明了 這是一個 cocos class 
3. @property 標明了 這是cocos class 的屬性 (可以在 creator 中 編輯)

* [詳細介紹](devgame-cocos-creator-ts/0)
