# [errors](https://deno.land/api@v1.28.1?s=Deno.errors)

namespace Deno.errors 下定義了一些 class 用來描述 deno 遇到的各種異常

```
try {
  const file = await Deno.open("./some/file.txt");
} catch (error) {
  if (error instanceof Deno.errors.NotFound) {
    console.error("the file was not found");
  } else {
    // otherwise re-throw
    throw error;
  }
}
```



| class | 描述 |
| -------- | -------- |
| AddrInUse     | 嘗試在已經監聽的地址和端口上創建新的監聽器     |
| AddrNotAvailable     | 底層操作系統報告 EADDRNOTAVAIL(地址不可用) 錯誤     |
| AlreadyExists     | 創建的資源已經存在     |
| BadResource     | 底層 IO 資源無效或已經關閉，因此無法執行操作     |
| BrokenPipe     | 嘗試寫入資源並發生管道損壞錯誤時。嘗試直接寫入 stdout/stderr 並且操作系統由於 deno 運行時外部的原因無法通過管道傳輸時可能發生這種情況     |
| Busy     | 底層 IO 資源不可用時，因爲它只在另外一個代碼塊中等待     |
| ConnectionAborted     | 底層操作系統報告 ECONNABORTED(連接中斷) 時     |
| ConnectionRefused     | 底層操作系統報告與資源的連接被拒絕是引發     |
| ConnectionReset     | 底層操作系統報告連接已重置時引發。對於網路服務器，客戶端終止而不是正確關閉連接是很正常的情況     |
| Http     | 嘗試加載動態導入時遇到太多重定向的情況下引發     |
| Interrupted     | 底層操作系統報告 EINTR 錯誤時引發。在許多情況下，這個底層 IO 錯誤將在 deno 內部處理，或者導致 BadResource 錯誤     |
| InvalidData     | 當操作返回對正在執行的操作無效的數據時引發     |
| NotConnected     | 當底層操作系統報告 ENOTCONN 錯誤時引發     |
| NotFound     | 當底層操作系統報告指定的資源找不到時引發     |
| NotSupported     | 當要求底層 deno api 執行不支持的功能時引發     |
| PermissionDenied     | 當底層操作系統指示運行 deno 進程的當前用戶對資源沒有適當權限時引發     |
| TimedOut     | 當底層操作系統報告 I/O 操作已經超時(ETIMEDOUT)時     |
| UnexpectedEof     | 嘗試從資源讀取字節但意外遇到 EOF 時     |
| WriteZero     | 當期望寫入 IO 緩衝區導致寫入 0 字節時引發     |



