# python

1989 研發 1991年開始發布的 一個 跨平台 弱型別 自動內存 腳本語言

目前出現 2.X 3.X 版本分裂

* 官網 [https://www.python.org/](https://www.python.org/)
* wiki [https://zh.wikipedia.org/wiki/Python](https://zh.wikipedia.org/wiki/Python)

```py
#!/usr/bin/env python3
 
import sys
print(sys.stdout.encoding)
print(sys.getdefaultencoding())
 
print("hellow word")
```

> 3.X 以前python使用 ascii編碼  
> 3.X開始 使用 unicode (4 字節編碼)編碼  
> **python 的源碼 通常使用 utf-8**
> 
> python 使用 # 註釋單行  
> python 沒有多行註釋
> 
> 通常 python 控制檯程序/模塊 使用 *.py  後綴  
> gui程序 使用 *.pyw 後綴
> 

# 執行 python 腳本
```sh
# 以 python2 執行 腳本
python main.py
python2 main.py

# 以 python3 執行 腳本
python3 main.py

# 以腳本中的 shebang 指定的 解析器 執行腳本
./main.py
```

## shebang

因爲 python2 python3 的同時 存在 大多shell 支持 shebang (windows32 不支持)

可以在 python腳本 第一行 以 **#!** 指定 解析器版本

```txt
#info=false
# 使用 python3 
#!/usr/bin/env python3

# 使用 python2
#!/usr/bin/env python

# 使用 指定路徑的 可執行 程式 執行腳本
#!/usr/bin/python3
#!/usr/bin/python2
#!/usr/bin/python
```

