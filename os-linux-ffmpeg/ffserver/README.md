# ffserver

ffserver 用來 啓動一個 視頻流服務器 可以支持 http rstp 視頻

ffserver 默認 讀取 **/etc/ffserver.conf** 的配置 也可以 通過 -f 指令 指定一個 配置檔案

ffserver -f ffserver.conf

```
#info="ffserver.conf"
# http 工作 地址
HTTPPort 8090
HTTPBindAddress 0.0.0.0
MaxHTTPConnections 2000
# 其它配置
MaxClients 1000
MaxBandwidth 30000
CustomLog -
NoDefaults

#配置 視頻推送 地址
<Feed play.ffm>
    File /home/king/Win32/tmps/play.ffm # 存儲的 臨時檔案 
    FileMaxSize 50M # 臨時檔案 大小

    ACL allow localhost
</Feed>

# 視頻流 播發地址
<Stream play.flv>
    Feed play.ffm # 視頻源
    Format flv

    AudioCodec aac
    Strict -2
    AudioBitRate 128
    AudioChannels 2
    AudioSampleRate 44100
    AVOptionAudio flags +global_header

    VideoCodec libx264
    VideoBitRate 800
    VideoFrameRate 25
    VideoSize 1280x720
    VideoGopSize 25
    AVOptionVideo flags +global_header
</Stream>


# # 配置一個 點播 節目
# <Stream a.flv>
#     File "/home/king/test/a.flv" # 視頻源
#     Format flv
# </Stream>

# 配置 status 頁面 用於顯示 ffserver 狀態
<Stream status.html>
    Format status
    # 配置 運行訪問的 ip 範圍
    ACL allow localhost
    ACL allow 192.168.0.0 192.168.255.255
</Stream>
```

# ffmpeg 推流

使用 ffmpeg 可以 將 視頻 推送到 ffserver

```
# 推送一個 檔案
ffmpeg -i a.mp4 http://localhost:8090/play.ffm


```