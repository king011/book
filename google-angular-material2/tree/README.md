# 樹

material 提供了 兩種樹結構 

* Flat tree 扁平樹
* Nested tree 嵌入樹

```
import {MatTreeModule} from '@angular/material/tree';
```

# Flat tree

1. Flat tree 中 層次結構是扁平的 
2. 節點都是按順序呈現爲兄弟節點 
3. TreeFlattener 實例 用來生成 平面列表
4. TreeControl 通過 getLevel 來獲取 每個節點的 級別 以此級別來設置樣式 以使其縮進到適當的級別

Flat tree 的最終結構如下

```html
<mat-tree>
  <mat-tree-node> parent node </mat-tree-node>
  <mat-tree-node> -- child node1 </mat-tree-node>
  <mat-tree-node> -- child node2 </mat-tree-node>
</mat-tree>
```

這樣的結構 通常可以 獲取 比 Nested tree 更高效的 渲染 和 滾動效果

## Eample

```html
#info="html"
<!-- 使用 mat-tree 定義 一個 樹，dataSource 指定 數據源，treeControl 指定 控制器 -->
<mat-tree [dataSource]="dataSource" [treeControl]="treeControl">
  <!-- 使用 mat-tree-node 定義 葉子節點 模板， let node for leaf nodes -->
  <mat-tree-node *matTreeNodeDef="let node" matTreeNodePadding>
    <!-- use a disabled button to provide padding for tree leaf -->
    <button mat-icon-button disabled></button>
    {{node.name}}
    <button mat-icon-button (click)="onAdd(node,'new')">
      <mat-icon>add</mat-icon>
    </button>
  </mat-tree-node>
  <!-- 使用 mat-nested-tree-node 定義 擴展節點 模板， let node for expandable nodes -->
  <mat-tree-node *matTreeNodeDef="let node;when: hasChild" matTreeNodePadding>
    <button mat-icon-button matTreeNodeToggle [attr.aria-label]="'toggle ' + node.name">
      <mat-icon class="mat-icon-rtl-mirror">
        {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}
      </mat-icon>
    </button>
    {{node.name}}
    <button mat-icon-button (click)="onAdd(node,'new')">
      <mat-icon>add</mat-icon>
    </button>
  </mat-tree-node>
</mat-tree>
```

```ts
#info="*.ts"
import { Component, OnInit, Injectable } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { isNullOrUndefined } from 'util';

/**
 * 定義嵌套 數據源
 */
class NestedNode {
  /**
   * 顯示名稱
   */
  name: string;
  /**
   * 子節點
   */
  children?: NestedNode[];
}
/**
 * 定義 扁平結構
 */
class FlatNode {
  /**
   * 顯示名稱
   */
  name: string
  /**
   * 是否是擴展節點
   */
  expandable: boolean
  /**
   * 級別
   */
  level: number
  /**
   * 是否需要更新節點
   */
  update: boolean
}

/**
 * 定義 測試數據
 */
const TREE_DATA: NestedNode[] = [
  {
    name: 'Fruit',
    children: [
      { name: 'Apple' },
      { name: 'Banana' },
      { name: 'Fruit loops' },
    ],
  },
  {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          { name: 'Broccoli' },
          { name: 'Brussel sprouts' },
        ],
      },
      {
        name: 'Orange',
        children: [
          { name: 'Pumpkins' },
          { name: 'Carrots' },
        ],
      },
    ],
  },
]

/**
 * 定義一個 helper 來 操作 數據源
 */
@Injectable()
export class Helper {
  /**
   * 數據源 變化
   */
  dataChange = new BehaviorSubject<NestedNode[]>([])
  get data(): NestedNode[] { return this.dataChange.value }

  constructor() {
    const data = TREE_DATA
    this.dataChange.next(data)
  }


  /**
   * 增加節點
   * @param parent 父節點
   * @param name 新節點名稱
   */
  add(parent: NestedNode, name: string) {
    if (isNullOrUndefined(parent.children)) {
      parent.children = new Array<NestedNode>()
    }
    parent.children.push({ name: name })
    this.dataChange.next(this.data)
  }
  /**
   * 更新節點
   * @param node 目標節點
   * @param name 節點名稱
   */
  update(node: NestedNode, name: string) {
    node.name = name
    this.dataChange.next(this.data)
  }
}

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  providers: [Helper],
})
export class AboutComponent implements OnInit {
  /**
   * 爲 NestedNode 創建 索引
   */
  flatNodeMap = new Map<FlatNode, NestedNode>()
  /**
   * 爲 FlatNode 創建 索引
   */
  nestedNodeMap = new Map<NestedNode, FlatNode>()

  /**
   * 定義 輔助 函數 將 NestedNode 轉到 FlatNode
   */
  private _transformer = (node: NestedNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node)
    if (existingNode) {
      if (existingNode.update) {
        this.flatNodeMap.delete(existingNode)
      } else {
        return existingNode
      }
    }

    const flatNode = new FlatNode()

    flatNode.name = node.name
    flatNode.level = level
    flatNode.expandable = !!node.children && node.children.length > 0

    this.flatNodeMap.set(flatNode, node)
    this.nestedNodeMap.set(node, flatNode)

    return flatNode
  }
  /**
   * 定義控制器
   * 
   * 需要 告訴 FlatTreeControl 如何 獲取 級別/是否是擴展節點
   */
  treeControl = new FlatTreeControl<FlatNode>(node => node.level, node => node.expandable)
  /**
   * 定義 MatTreeFlattener 爲樹 生成 平面 列表
   */
  treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children)
  /**
   * 定義 數據源
   */
  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener)
  constructor(private helper: Helper) {
  }
  ngOnInit() {
    // 訂閱 數據源 改變通知
    this.helper.dataChange.subscribe(
      (data) => {
        this.dataSource.data = data
      }
    )
  }
  /**
   * 返回 節點是否有子
   */
  hasChild = (_: number, node: FlatNode) => node.expandable

  onAdd(node: FlatNode, name: string) {
    const nestedNode = this.flatNodeMap.get(node)
    if (!node.expandable) {
      node.expandable = true
      if (!node.update) {
        node.update = true
      }
    }
    this.helper.add(nestedNode, name)
  }
}

```



# Nested tree

1. Nested tree 的結構是嵌套的
2. 子節點嵌套在父節點內

Nested tree 的最終結構如下

```html
<mat-tree>
   <mat-nested-tree-node>
     parent node
     <mat-nested-tree-node> -- child node1 </mat-nested-tree-node>
     <mat-nested-tree-node> -- child node2 </mat-nested-tree-node>
   </mat-nested-tree-node>
</mat-tree>
```

Nested tree 更容易使用 但 效率通常 比 Flat tree 低下 因爲 難以直觀的表示層次關係 並且 父節點的改變 可能影響到子節點 導致 整個 節點和子節點都被重構

## Example 

```
#info="*.css"
.example-tree-invisible {
    display: none;
  }
  
  .example-tree ul,
  .example-tree li {
    margin-top: 0;
    margin-bottom: 0;
    list-style-type: none;
  }
```

```html
#info="*.html"
<!-- 使用 mat-tree 定義 一個 樹，dataSource 指定 數據源，treeControl 指定 控制器 -->
<mat-tree [dataSource]="dataSource" [treeControl]="treeControl" class="example-tree">
  <!-- 使用 mat-tree-node 定義 葉子節點 模板，let node for leaf nodes -->
  <mat-tree-node *matTreeNodeDef="let node" matTreeNodeToggle>
    <li class="mat-tree-node">
      <!-- use a disabled button to provide padding for tree leaf -->
      <button mat-icon-button disabled></button>
      {{node.name}}
    </li>
  </mat-tree-node>

  <!-- 使用 mat-nested-tree-node 定義 擴展節點 模板，let node for expandable nodes -->
  <mat-nested-tree-node *matTreeNodeDef="let node; when: hasChild">
    <li>
      <div class="mat-tree-node">
        <button mat-icon-button matTreeNodeToggle [attr.aria-label]="'toggle ' + node.name">
          <mat-icon class="mat-icon-rtl-mirror">
            {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}
          </mat-icon>
        </button>
        {{node.name}}
      </div>
      <ul [class.example-tree-invisible]="!treeControl.isExpanded(node)">
        <ng-container matTreeNodeOutlet></ng-container>
      </ul>
    </li>
  </mat-nested-tree-node>
</mat-tree>
```

```
#info="*ts"
import { Component, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';

/**
 * 定義 節點 接口
 */
interface Element {
  /**
   * 節點名稱
   */
  name: string
  /**
   * 可選的子節點
   */
  children?: Element[]
}

/**
 * 具有層次 結構的 測試數據
 */
const TREE_DATA: Element[] = [
  {
    name: 'Fruit',
    children: [
      { name: 'Apple' },
      { name: 'Banana' },
      { name: 'Fruit loops' },
    ]
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          { name: 'Broccoli' },
          { name: 'Brussel sprouts' },
        ]
      }, {
        name: 'Orange',
        children: [
          { name: 'Pumpkins' },
          { name: 'Carrots' },
        ]
      },
    ]
  },
]

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  /**
   * 定義 控制器
   */
  treeControl = new NestedTreeControl<Element>(node => node.children)
  /**
   * 定義 數據源
   */
  dataSource = new MatTreeNestedDataSource<Element>()
  constructor() {
    // 設置 數據 到 數據源
    this.dataSource.data = TREE_DATA
  }
  /**
   * 返回 節點是否有子節點
   */
  hasChild = (_: number, node: Element) => !!node.children && node.children.length > 0
  ngOnInit() {
  }
}
```
