# 腳本控制動畫

[cc.Animation](https://docs.cocos.com/creator/api/zh/classes/Animation.html) 提供了一些 動畫控制函數

```
const anim = this.getComponent(cc.Animation)

// 如果沒有指定播放 哪個動畫 並且由設置defaultClip 則播放 defaultClip 動畫
anim.play()

// 指定播放 test 動畫
anim.play('test')

// 指定從 1s 開始 播放 test 動畫
anim.play('test', 1)

// 使用 play 播放動畫時 如果還有其它正在播放的動畫 會先停止其它動畫
anim.play('test2')
```

Animation 對一個動畫進行播放時 會先判斷 動畫之前的 狀態 來進行 下一步操作 如果動畫

* 停止 則會直接重新播放這個動畫
* 暫停 則會恢復動畫播放
* 播放 則會線停止動畫後重新播放

使用 playAdditive 可以同時播放 多個動畫
```
const anim = this.getComponent(cc.Animation)

// 播放第一個動畫
anim.playAdditive('position-anim')

// 播放第二個動畫
anim.playAdditive('rotation-anim')
```

## 暫停 恢復 停止

```
var anim = this.getComponent(cc.Animation);

anim.play('test')

// 暫停 test 動畫
anim.pause('test')

// 暫停所有動畫
// anim.pause()

// 恢復 test 動畫
anim.resume('test')

// 恢復所有動畫
// anim.resume()

// 停止 test 動畫
anim.stop('test')

// 停止所有動畫
// anim.stop()
```

## 設置動畫當前時間

可以在任何事件設置動畫當前時間 但動畫不會立刻根據設置的時間進行狀態更改 需要在下一個 動畫的 update 中 才會根據這個時間重新計算播放狀態

```
var anim = this.getComponent(cc.Animation);

anim.play('test')

// 設置 test 動畫的當前播放事件爲 1s
anim.setCurrentTime(1, 'test')

// 設置 所有 動畫的當前播放事件爲 1s
// anim.setCurrentTime(1)
```

# AnimationState

[cc.AnimationState](https://docs.cocos.com/creator/api/zh/classes/AnimationState.html) 提供了更加完整的 動畫控制 是AnimationClip 數據的實例

```
#info="獲取 AnimationState"
const anim = this.getComponent(cc.Animation)
// play 會返回關聯的 AnimationState
const animState = anim.play('test')

// 也可以直接獲取
const animState = anim.getAnimationState('test')
```

```
#info="獲取動畫信息"
const anim = this.getComponent(cc.Animation)
const animState = anim.play('test')

// 獲取關聯的 clip
const clip = animState.clip

// 獲取動畫名字
const name = animState.name

// 獲取動畫播放速度
const speed = animState.speed

// 獲取動畫播放總時長
const duration = animState.duration

// 獲取動畫播放時間
const time = animState.time

// 獲取動畫重複次數
const repeatCount = animState.repeatCount

// 獲取動循環模式
const wrapMode = animState.wrapMode

// 獲取動畫是否 正在播放
const playing = animState.isPlaying

// 獲取動畫是否 已經暫停
const paused = animState.isPaused

// 獲取動畫幀率
const frameRate = animState.frameRate
```

## 設置動畫 播放速度

```
var anim = this.getComponent(cc.Animation)
var animState = anim.play('test')

// 使用動畫 播放速度 加速
animState.speed = 2

// 使用動畫 播放速度 減速
animState.speed = 0.5
```

## 設置動畫 循環模式 和 循環次數

```
const anim = this.getComponent(cc.Animation);
const animState = anim.play('test')

// 設置循環模式爲 Normal
animState.wrapMode = cc.WrapMode.Normal

// 設置循環模式爲 Loop
animState.wrapMode = cc.WrapMode.Loop

// 設置動畫播放次數爲 2次
animState.repeatCount = 2

// 設置動畫播放次數爲 無限循環
animState.repeatCount = Infinity
```

# 動態 創建 [cc.AnimationClip](https://docs.cocos.com/creator/api/zh/classes/AnimationClip.html)

```
const animation = this.node.getComponent(cc.Animation)
// frames 這時一個 SpriteFrame 數組
const clip = cc.AnimationClip.createWithSpriteFrames(frames, 17)
clip.name = "anim_run"
clip.wrapMode = cc.WrapMode.Loop

// 添加幀事件
clip.events.push({
		frame: 1,               // 準確事件 以秒爲單位 。此處表示將在動畫播放到 1s 時 觸發
		func: "frameEvent",     // 回調函數名
		params: [1, "hello"]    // 回調參數
})

animation.addClip(clip)
animation.play('anim_run')
```