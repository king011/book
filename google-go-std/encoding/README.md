# [encoding](https://pkg.go.dev/encoding)

encoding 定義了 數據 文本 \[\]byte 間 轉換的共享接口 其子包 提供了 一些具體編碼的 實現

# encoding/gob

encoding/gob 是go特有的 編碼 方式 是 encoding 所有實現中 最快的 通常用來作爲 go rpc 的 序列化工具

* NewEncoder 創建一個 數據流 用於 將 數據 寫入
* NewDecoder 創建一個 讀取流 用於 將 編碼 數據 讀出
* 流中的 數據是 自述的 故 在同個流中 可以 寫入 不同型別的數據
* 讀取的型別 可以和寫入不同 但 必須兼容 詳見 godoc

```go
package main

import (
	"bytes"
	"encoding/gob"
	"log"
)

// Point2D .
type Point2D struct {
	X, Y int
}

// Point3D .
type Point3D struct {
	X, Y, Z *int
}

// Role3D .
type Role3D struct {
	Name  string
	Point *Point3D
}

// Role2D .
type Role2D struct {
	Name  string
	Point Point2D
}

func main() {
	// 創建一個 buffer 模擬 網路 傳輸 數據
	var network bytes.Buffer
	enc := gob.NewEncoder(&network) // 創建 編碼器
	dec := gob.NewDecoder(&network) // 創建 解碼機

	if e := Encode(enc); e != nil {
		log.Fatalln(e)
	}
	if e := Decode(dec); e != nil {
		return
	}
}

// Decode .
func Decode(dec *gob.Decoder) (e error) {
	// 讀取基本 型別
	{
		// 型別必須 兼容 可以是 int/int8/int16 ... 但如果實際值超過型別上限將 返回錯誤
		// 型別可以是 指針 或 值
		var i *int8
		e = dec.Decode(&i)
		if e != nil {
			log.Fatalln(e)
		}
		log.Println("i =", *i)

		var str string
		e = dec.Decode(&str)
		if e != nil {
			log.Fatalln(e)
		}
		log.Println("str =", str)
	}
	// 讀取 結構
	{
		// Point2D Point3D 是兼容 型別 故 可以 用 p3 讀 p2 p2 讀 p3
		var p3 Point3D
		e = dec.Decode(&p3)
		if e != nil {
			log.Fatalln(e)
		}
		// 1 2 nil
		log.Println(*p3.X, *p3.Y, p3.Z)

		var p2 Point2D
		e = dec.Decode(&p2)
		if e != nil {
			log.Fatalln(e)
		}
		// 10 11
		log.Println(p2.X, p2.Y)
	}
	// 讀取 複合 結構
	{
		var r3 Role3D
		e = dec.Decode(&r3)
		if e != nil {
			log.Fatalln(e)
		}
		// r2 1 2 nil
		log.Println(r3.Name, *r3.Point.X, *r3.Point.Y, r3.Point.Z)

		var r2 Role2D
		e = dec.Decode(&r2)
		if e != nil {
			log.Fatalln(e)
		}
		// r3 10 11
		log.Println(r2.Name, r2.Point.X, r2.Point.Y)
	}

	// 讀取 slice
	var arrs []string
	e = dec.Decode(&arrs)
	if e != nil {
		log.Fatalln(e)
	}
	// [kate anita anna]
	log.Println(arrs)
	// 讀取 map
	var m map[string]int
	e = dec.Decode(&m)
	if e != nil {
		log.Fatalln(e)
	}
	// map[kate:10 anita:100 anna:1]
	log.Println(m)

	return
}

// Encode .
func Encode(enc *gob.Encoder) (e error) {
	// 寫入基本 型別
	{
		i := 1
		str := "cerberus is an idea"
		e = enc.Encode(i)
		if e != nil {
			log.Fatalln(e)
		}
		e = enc.Encode(str)
		if e != nil {
			log.Fatalln(e)
		}
	}
	// 寫入結構
	var p2 Point2D
	var p3 Point3D
	{
		// 寫入 2d 點
		p2.X = 1
		p2.Y = 2
		e = enc.Encode(p2)
		if e != nil {
			log.Fatalln(e)
		}
		// 寫入 2d 點
		x := 10
		y := 11
		z := 12
		p3.X = &x
		p3.Y = &y
		p3.Z = &z
		e = enc.Encode(p3)
		if e != nil {
			log.Fatalln(e)
		}
	}
	// 寫入 複合 結構
	r2 := &Role2D{
		Name:  "r2",
		Point: p2,
	}
	r3 := &Role3D{
		Name:  "r3",
		Point: &p3,
	}
	{
		e = enc.Encode(r2)
		if e != nil {
			log.Fatalln(e)
		}
		e = enc.Encode(r3)
		if e != nil {
			log.Fatalln(e)
		}
	}

	// 寫入 slice
	arrs := []string{"kate", "anita", "anna"}
	e = enc.Encode(arrs)
	if e != nil {
		log.Fatalln(e)
	}
	// 寫入 map
	m := make(map[string]int)
	m["kate"] = 10
	m["anita"] = 100
	m["anna"] = 1
	e = enc.Encode(m)
	if e != nil {
		log.Fatalln(e)
	}

	return
}
```

> 對於 複合結構 讀取型別 和 寫入型別 相交的 字段名型別兼容則是兼容型別 未相交的字段會被忽略 或寫入默認值(通常是 0 false nil 空字符串)
>  

# encoding/hex
encoding/hex 提供了將 二進制數據 和 16進制字符串 間的 轉換函數

```go
package main

import (
	"bytes"
	"encoding/hex"
	"io/ioutil"
	"log"
)

func main() {
	var network bytes.Buffer
	str := "cerberus is an idea"
	src := []byte(str)
	enc := hex.NewEncoder(&network)
	dec := hex.NewDecoder(&network)

	// hex
	log.Println(hex.EncodeToString(src))

	// hex 到 指定 內存
	n := hex.EncodedLen(len(src))
	dst := make([]byte, n)
	// 如果 dst 長度不夠 將 產生 異常
	n = hex.Encode(dst, src)
	//log.Println(string(dst[:n]))
	log.Println(string(dst))

	// hex writer
	_, e := enc.Write(src)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(network.String())

	src = dst
	// dec
	dst, e = hex.DecodeString(string(src))
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(string(dst))

	// dec 到 指定 內存
	n = hex.DecodedLen(len(src))
	dst = make([]byte, n)
	n, e = hex.Decode(dst, src) // 如果 dst 長度 不夠 不會 返回錯誤 而是產生 異常
	if e != nil {
		log.Fatalln(e)
	}
	//	log.Println(string(dst[:n]))
	log.Println(string(dst))

	// hex reader
	dst, e = ioutil.ReadAll(dec)
	log.Println(string(dst))
}
```

Dump/Dumper 函數 進行 dump格式 的 編碼 或創建編碼器 

```go
package main

import (
	"encoding/hex"
	"fmt"
)

func main() {
	src := []byte("cerberus is an idea")
	fmt.Println(hex.Dump(src))
}
```
```txt
00000000  63 65 72 62 65 72 75 73  20 69 73 20 61 6e 20 69  |cerberus is an i|
00000010  64 65 61                                          |dea|
```
# encoding/xml

```txt
#info=false
encoding/xml 包 實現了 xml 到 Go struct之間的 轉換 
轉化關係 由於 struct 屬性(屬性必須倒出) 後的 `` tag指定

//由字節數據 轉換到 Go struct 
func Unmarshal(data []byte, v interface{}) error

//由Go struct 轉換到 byte字節 默認不會 輸出 header
func Marshal(v interface{}) ([]byte, error)		//不會換行 縮進
//同上 不過爲每行加上 prefix前綴 且 使用indent縮進
func MarshalIndent(v interface{}, prefix, indent string) ([]byte, error)
```

## tag規則

### 解析xml

* 如果struct的一个字段是string或者[]byte类型且它的tag含有",innerxml"，Unmarshal将会将此字段所对应的元素内所有内嵌的原始xml累加到此字段上
* 如果struct中有一个叫做XMLName，且类型为xml.Name字段，那么在解析的时候就会保存这个element的名字到该字段
* 如果某个struct字段的tag定义中含有XML结构中element的名称，那么解析的时候就会把相应的element值赋值给该字段
* 如果某个struct字段的tag定义了中含有",attr"，那么解析的时候就会将该结构所对应的element的与字段同名的属性的值赋值给该字段，如上version定义。
* 如果某个struct字段的tag定义 型如"a>b>c",则解析的时候，会将xml结构a下面的b下面的c元素的值赋值给该字段。
* 如果某个struct字段的tag定义了"-",那么不会为该字段解析匹配任何xml数据。
* 如果struct字段后面的tag定义了",any"，如果他的子元素在不满足其他的规则的时候就会匹配到这个字段。
* 如果某个XML元素包含一条或者多条注释，那么这些注释将被累加到第一个tag含有",comments"的字段上，这个字段的类型可能是[]byte或string,如果没有这样的字段存在，那么注释将会被抛弃。

### 生成xml

* 如果v是 array或者slice，那么输出每一个元素，类似value
* 如果v是指针，那么会Marshal指针指向的内容，如果指针为空，什么都不输出
* 如果v是interface，那么就处理interface所包含的数据
* 如果v是其他数据类型，就会输出这个数据类型所拥有的字段信息

元素名按照如下优先级从struct中获取：
* 如果v是struct，XMLName的tag中定义的名称
* 类型为xml.Name的名叫XMLName的字段的值
* 通过struct中字段的tag来获取
* 通过struct的字段名用来获取
* marshall的类型名称

```xml
<?xml version="1.0" encoding="utf-8"?>
<root version='1'>
	<item>
		<url>https://www.google.com</url>
		<note>google search</note>
	</item>
	<item>
		<url>https://mail.google.com/</url>
		<note>google email</note>
	</item>
 
</root>
```

```go
package main
 
import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)
 
type XML struct {
	XMLName xml.Name `xml:"root"`
	Version string   `xml:"version,attr"`
	Note    []Note   `xml:"item"`
}
type Note struct {
	Note string `xml:"note"`
	Url  string `xml:"url"`
}
 
func main() {
	//讀
	file, err := os.Open("my.xml")
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	v := XML{}
	err = xml.Unmarshal(data, &v)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	fmt.Println(v)
 
	//寫
	b, e := xml.Marshal(v)
	if e != nil {
		fmt.Println(e)
		return
	}
	os.Stdout.Write([]byte(xml.Header))
	os.Stdout.Write(b)
	os.Stdout.Write([]byte("\n\n"))
 
	b, e = xml.MarshalIndent(v, "", "	")
	if e != nil {
		fmt.Println(e)
		return
	}
	os.Stdout.Write([]byte(xml.Header))
	os.Stdout.Write(b)
}
```

## stream

goalng 亦提供了 解析 xml stream 的支持

```go
package main
 
import (
	"encoding/xml"
	"io"
	"log"
	"os"
)
 
const (
	// InputFile .
	InputFile = "a.xml"
)
 
func main() {
	// 創建 解碼流
	f, e := os.Open(InputFile)
	if e != nil {
		log.Fatalln(e)
	}
	decoder := xml.NewDecoder(f)
 
	for {
		// 獲取 token
		t, e := decoder.Token()
		if e != nil {
			if e == io.EOF {
				log.Println("Parse XML finished!")
			} else {
				log.Fatalln(e)
			}
			break
		}
 
		// decoder.Token() 返回的 token 只在下次調用 Token() 前有效
		// CopyToken 可以創建一個 一直有效的 副本
		// t = xml.CopyToken(t)
 
		// 處理 token
		switch t := t.(type) {
		case xml.StartElement:
			log.Printf("StartElement: <%v>\n", t.Name.Local)
		case xml.EndElement:
			log.Printf("EndElement: <%v>\n", t.Name.Local)
		case xml.CharData:
			log.Printf("CharData: %v\n", string(t))
		case xml.Comment:
			log.Printf("Comment: <!--%v-->\n", string(t))
		}
	}
}
```

# encoding/json
encoding/json 包提供了對 json解析 的功能 用法 同 encoding/xml

```json
{
	"root":[{"url":"https://www.google.com",
		"note":"google search"},
		{"url":"https://mail.google.com/",
		"note":"google email"}
	]	
}
```

```go
package main
 
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)
 
type JSON struct {
	Data []NOTE `json:"root"`
}
type NOTE struct {
	Url  string
	Note string
}
 
func main() {
	//讀
	file, err := os.Open("my.json")
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	j := JSON{}
	err = json.Unmarshal(data, &j)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	fmt.Println(j)
 
	//寫
	b, e := json.Marshal(j)
	if e != nil {
		fmt.Println(e)
		return
	}
	os.Stdout.Write(b)
	os.Stdout.Write([]byte("\n\n"))
 
	b, e = json.MarshalIndent(j, "", "	")
	if e != nil {
		fmt.Println(e)
		return
	}
	os.Stdout.Write(b)
}
```