# IndexedDB

IndexedDB 是一個 事務型的 NoSQL 數據庫  其作爲 windows.indexedDB 屬性 提供給用戶使用

* 使用 key/val 存儲
* 所有操作都提供了異步API
* 支持事務
* 同源限制 每個數據庫都有對應的建立域名 網頁只能訪問自身域名下的數據庫
* 存儲空間大 通常不會少於250MB
* 支持 二進制存儲

> 在 正式規範 之前的舊瀏覽器版本中 indexedDB 作爲 window的 另外一個屬性名存儲
> * chrome 叫做 webkitIndexedDB
> * firefox 叫做 mozIndexedDB 
> * ie 叫做 msIndexedDB
> 
> 不過這是上古時代的事了 如果 不是爲了考古的無聊興趣 可以直接忽略之

# 基本概念

IndexedDB 主要 有下面 幾個接口

* 數據庫 **IDBDatabase**
* 對象倉庫 **IDBObjectStore**
* 索引 **IDBIndex**
* 事務 **IDBTransaction**

## 數據庫

數據庫 是一系列相關數據的容器 每個 協議+域名+端口 都可以創建任意多個 數據庫

數據庫 存在版本 同一時刻只能存在一個版本的數據庫 如果要修改數據庫結構 只能通過升級版本完成

## 對象倉庫

每個數據庫包含若干個對象倉庫 類似關係型數據庫的表

## 數據記錄

對象倉庫保存的是數據記錄 每條記錄包含 主鍵和數據兩部分

主鍵 用來創建默認索引 必須唯一 主鍵可以是數據中的一個屬性 也可以是一個 自動遞增的 整數

數據可以是任意的 js 對象

## 索引

可以爲數據的 不同屬性 創建 索引 以加快 檢索數據

## 事務

IndexedDB 數據的 讀寫 都必須 通過 事務 完成 事務 提供 **error abort complete** 三個事件來 返回執行結果 

# 打開/創建 數據庫
```ts
// 打開 數據庫
// name 數據庫名稱
// version 版本 如果不添則爲 1
open(name: string, version?: number): IDBOpenDBRequest;
```
IDBOpenDBRequest 對象 包含 下面三個 回調屬性可以設置

* onupgradeneeded 每當 需要升級版本時
* onsuccess 打開成功
* onerror 打開失敗

> 通常 應該 在 onupgradeneeded 中創建 數據倉庫 等操作 onsuccess 會在 onupgradeneeded之後調用
> 
> 對於第一次 創建的數據庫 也會 調用 onupgradeneeded
 

```ts
  private db = null;
  onOpen() {
    if (this.db) {
      return;
    }
    try {
      const request = window.indexedDB.open("test", 11);
      request.onupgradeneeded = (evt) => {
        // const db = evt.target.result;
        const db = request.result;

        // 檢索 倉庫是否存在
        if (db.objectStoreNames.contains('beauty')) {
          // 創建 刪除 倉庫 以便 新建
          // 此 api 只能在 onupgradeneeded 中調用
          db.deleteObjectStore('beauty');
        }
        // 創建倉庫
        const objectStore = db.createObjectStore(
          'beauty', // 倉庫名
          {
            keyPath: 'id', // 指定用 數據中的 id 屬性作爲 主鍵
            autoIncrement: true, // 指定 主鍵 自增
          }
        );
        // 創建 索引
        objectStore.createIndex(
          'name', // 索引名
          'name', // 要創建 索引的 屬性
          {
            unique: true, // 屬性 是否唯一
          }
        );
        objectStore.createIndex('lv', 'lv', { unique: false });
      };
      request.onsuccess = (evt) => {
        this.db = request.result;
        console.log("open success")
      };
      request.onerror = (evt) => {
        console.log("open error");
      };
    } catch (e) {
      console.log(e)
    }
  }
  onClose(){
    if(this.db){
      this.db.close();
      this.db = null;
    }
  }
```
# 事務

對 數據庫的 讀寫 都必須 打開一個 事務 

* readonly 只讀
* readwrite 讀寫

`const transaction = db.transaction(['customers'], 'readwrite');` 如果不寫打開模式 默認 readonly

* add 方法 新建記錄
* put 方法 修改數據
* delete 方法 刪除數據
```ts
// 增加 key是 可以選 的 如果 不傳入 使用 value 中的 keyPath
objectStore.add(value, key?)
// 增加 或修改
objectStore.put(item, key?)
// 刪除
objectStore.delete(Key)
```

## Example 讀寫
```ts
  name: string;
  lv: number;
  id: string;
  onAdd() {
    const db = this.db;
    if (!db || this.name == "" || this.name == null || this.name == undefined) {
      console.log("ignore add");
      return;
    }

    try {
      // 增加數據
      var request = db.transaction(['beauty'], 'readwrite')
        .objectStore('beauty')
        .add({
          name: this.name,
          lv: this.lv
        });
      request.onsuccess = function (evt) {
        console.log('add success');
      };
      request.onerror = function (evt, e) {
        console.log(evt.target.error);
      }
    } catch (e) {
      console.log(e)
    }
  }
  onPut() {
    const db = this.db;
    const id = parseInt(this.id);
    if (!db ||
      this.name == "" || this.name == null || this.name == undefined ||
      isNaN(id) || id == 0
    ) {
      console.log("ignore put");
      return;
    }

    try {
      // 增加數據
      var request = db.transaction(['beauty'], 'readwrite')
        .objectStore('beauty')
        .put({
          id: id,
          name: this.name,
          lv: this.lv
        });
      request.onsuccess = function (evt) {
        console.log('put success');
      };
      request.onerror = function (evt, e) {
        console.log(evt.target.error);
      }
    } catch (e) {
      console.log(e)
    }
  }
  onDelete() {
    const db = this.db;
    const id = parseInt(this.id);
    if (!db ||
      isNaN(id) || id == 0
    ) {
      console.log("ignore delete");
      return;
    }

    try {
      // 增加數據
      var request = db.transaction(['beauty'], 'readwrite')
        .objectStore('beauty')
        .delete(id);
      request.onsuccess = function (evt) {
        console.log('delete success');
      };
      request.onerror = function (evt, e) {
        console.log(evt.target.error);
      }
    } catch (e) {
      console.log(e)
    }
  }
```
> put 的 項目 如果不存在 則會 新增
> 
> delete 的項目 不存在 會直接返回 成功