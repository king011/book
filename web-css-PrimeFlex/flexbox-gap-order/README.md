# [gap](https://www.primefaces.org/primeflex/gap)

gap 是 row-gap 和 column-gap 的簡寫用於定義 flex 中元素的行和列 間距

| Classes | Properties |
| -------- | -------- |
| gap-0     | gap: 0;     |
| gap-1     | gap: 0.25rem;    |
| gap-2     | gap: 0.5rem;    |
| gap-3     | gap: 1rem;    |
| gap-4     | gap: 1.5rem;    |
| gap-5     | gap: 2rem;    |
| gap-6     | gap: 3rem;    |
| gap-7     | gap: 4rem;    |
| gap-8     | gap: 5rem;    |
| row-gap-0     | row-gap: 0;     |
| row-gap-1     | row-gap: 0.25rem;    |
| row-gap-2     | row-gap: 0.5rem;    |
| row-gap-3     | row-gap: 1rem;    |
| row-gap-4     | row-gap: 1.5rem;    |
| row-gap-5     | row-gap: 2rem;    |
| row-gap-6     | row-gap: 3rem;    |
| row-gap-7     | row-gap: 4rem;    |
| row-gap-8     | row-gap: 5rem;    |
| column-gap-0     | column-gap: 0;     |
| column-gap-1     | column-gap: 0.25rem;    |
| column-gap-2     | column-gap: 0.5rem;    |
| column-gap-3     | column-gap: 1rem;    |
| column-gap-4     | column-gap: 1.5rem;    |
| column-gap-5     | column-gap: 2rem;    |
| column-gap-6     | column-gap: 3rem;    |
| column-gap-7     | column-gap: 4rem;    |
| column-gap-8     | column-gap: 5rem;    |


行列間距相同 gap-3
![](assets/gap.png)

```
<div class="card">
    <div class="flex flex-wrap justify-content-center card-container blue-container gap-3">
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">1</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">2</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">3</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">4</div>
    </div>
</div>
```

分別指定行列間距 row-gap-6 column-gap-4
![](assets/gap2.png)
```
<div class="card">
    <div class="flex flex-wrap card-container blue-container column-gap-4 row-gap-6">
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">1</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">2</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">3</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">4</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">5</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">6</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">7</div>
        <div class="border-round w-12rem h-6rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center">8</div>
    </div>
</div>
```

# [order](https://www.primefaces.org/primeflex/order)
order 控制 flex 中元素相對其它元素的順序

| Class | Properties |
| -------- | -------- |
| flex-order-0     | order: 0;     |
| flex-order-1     | order: 1;     |
| flex-order-2     | order: 2;     |
| flex-order-3     | order: 3;     |
| flex-order-4     | order: 4;     |
| flex-order-5     | order: 5;     |
| flex-order-6     | order: 6;     |

![](assets/order.png)

```
<div class="card">
    <div class="flex flex-wrap card-container blue-container">
        <div class="flex-order-2 flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">1</div>
        <div class="flex-order-1 flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">2</div>
        <div class="flex-order-0 flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同