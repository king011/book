# test

go 提供了完善的 單元測試功能

1. 創建 測試文件 XXX_test.go
2. 在測試文件中 import testing
3. 創建 測試 方法 TestXXX(t \*testing.T)
4. 在TestXXX中 如果 出錯 調用 t.FatalXXX t.ErrorXXX ...
5. 執行 `go test -v -cover` 執行 測試

* XXX_test.go 中 多個 TestXXX 將 按照 定義順序 執行
* t.FatalXXX 會讓當前的TestXXX 停止執行 但不會影響其它 TestXXX

```
package main_test
 
import (
	"testing"
)
 
func TestExample(t *testing.T) {
	t.Fatal("example")
}
```

# 可選參數


| 參數名 | 值 | 含義 |
| -------- | -------- | -------- |
| -v     |      | 打印測試詳情     |
| -cover     |      | 打印覆蓋率     |
| -run     | 測試函數名     | 運行指定測試函數     |

# -bench

使用 -bench 參數 可以指定要執行的 基準測試 -bench 後面需要跟上 要測試的 函數名稱 通常可以使用 **.** 匹配所有基準測試

另外 go test 默認會執行 單元測試 如果要只執行 基準測試 可以 使用 -run 指定一個 不存在的 單元測試 函數名

```
go test -run none -bench .
```

基準測試 函數簽名必須如下

```
func BenchmarkXXX(b *testing.B) 
```

```
package main_test

import (
	"testing"
)

func BenchmarkFib1(b *testing.B)  { benchmarkFib(1, b) }
func BenchmarkFib2(b *testing.B)  { benchmarkFib(2, b) }
func BenchmarkFib3(b *testing.B)  { benchmarkFib(3, b) }
func BenchmarkFib10(b *testing.B) { benchmarkFib(10, b) }
func BenchmarkFib20(b *testing.B) { benchmarkFib(20, b) }
func BenchmarkFib40(b *testing.B) { benchmarkFib(40, b) }

func benchmarkFib(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fibonacci(i)
	}
}
func Fibonacci(n int) int {
	if n < 2 {
		return 0
	}
	return Fibonacci(n-1) + Fibonacci(n-2)
}
```

```
$ go test -run none -bench .
goos: linux
goarch: amd64
pkg: test/console
BenchmarkFib1-12     	737286760	         1.60 ns/op
BenchmarkFib2-12     	295187796	         4.01 ns/op
BenchmarkFib3-12     	163133022	         7.46 ns/op
BenchmarkFib10-12    	 4625640	       256 ns/op
BenchmarkFib20-12    	   37849	     31166 ns/op
BenchmarkFib40-12    	       3	 478391707 ns/op
PASS
ok  	test/console	10.721s
```

每個基準測試 至少會執行 1s 如果想執行更長時間 可以傳入 -benchtime 參數

```
go test -run none -bench . -benchtime=10s
```


* 如果要 在測試前 執行執行一些耗時配置 可以調用 b.ResetTimer() 重置 計算器


RunParallel 可以創建 n 個 goroutine 來測試並行效率 通常可以和 -cpu 參數一起指定 GOMAXPROCS 數量

```
package main_test

import (
	"bytes"
	"testing"
	"text/template"
)

func BenchmarkGo(b *testing.B) {
	// 打印 內存使用 清空
	b.ReportAllocs()

	templ := template.Must(template.New("test").Parse("Hello, {{.}}!"))
	// 設置 創建 2*GOMAXPROCS 個 goroutine
	b.SetParallelism(2)
	b.RunParallel(func(pb *testing.PB) {
		// 每個 goroutine 有屬於自己的 bytes.Buffer
		var buf bytes.Buffer
		for pb.Next() {
			// 循環體在所有 goroutine 中總共執行 b.N 次
			buf.Reset()
			templ.Execute(&buf, "World")
		}
	})
}
```

```
$ go test -run none -bench . -cpu 2
goos: linux
goarch: amd64
pkg: test/console
BenchmarkGo-2   	10375189	       122 ns/op	      48 B/op	       1 allocs/op
PASS
ok  	test/console	1.382s
```

* 10375189 : 基準測試迭代次數 b.N
* 122 ns/op : 平均每次迭代所消耗的納秒數
* 48 B/op : 平均每次迭代內存所分配的字節數量
* 1 allocs/op : 平均每次迭代內存所分配次數


## -benchmem

-benchmem 會打印內存分配情況
