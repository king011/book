# runit

runit 是一個具有 服務監督 跨平臺 開源(BSD) 的 Unix 初始化方案

通常 linux 發行版本已經內置了類似方案比如現在流行的 systemd，並不推薦使用 runit 替代發行版提供的服務程式。但對於嵌入式 linux 以及 容器中，runit 是不錯的選擇。

* 官網 [http://smarden.org/runit/](http://smarden.org/runit/)

