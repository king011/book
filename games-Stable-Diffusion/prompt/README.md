# 關鍵詞

在選單了模型後還需要爲模型輸入關鍵詞，來告訴模型需要畫什麼內容

1. 多個關鍵詞使用英文逗號分隔
2. 目前關鍵詞必須是英文
3. 關鍵詞可以是
	*  句子 A beauty who is bathing
	*  單詞 girl
	*  詞組 beauty, girl

關鍵詞通常可以採用 畫質+主體+主體細節+人物服裝+其它(背景 天氣 構圖) 的模式來寫:

```
1girl, very delicate features, very detailed eyes and mouth, long hair, curly hair, delicate skin, big eyes,
```

## 權重

* 你可以爲關鍵詞增加權重使用 () 括起來的關鍵詞權重\*1.1 括多次則 \* 多次 1.1 `((curly hair))`
* 使用 \[\] 括起來的減少權重 `[curly hair]`
* 也可以使用 (:) 直接指定權重 `(curly hair:1.3)`


# 負面關鍵詞
可以填寫負面關鍵詞，這些是不想要出現在畫面中的內容

```
(worst quality:2), (low quality:2), (normal quality:2), lowres, ((monochrome)), ((grayscale)), bad anatomy,DeepNegative, skin spots, acnes, skin blemishes,(fat:1.2),facing away, looking away,tilted head, lowres,bad anatomy,bad hands, missing fingers,extra digit, fewer digits,bad feet,poorly drawn hands,poorly drawn face,mutation,deformed,extra fingers,extra limbs,extra arms,extra legs,malformed limbs,fused fingers,too many fingers,long neck,cross-eyed,mutated hands,polar lowres,bad body,bad proportions,gross proportions,missing arms,missing legs,extra digit, extra arms, extra leg, extra foot,teethcroppe,signature, watermark, username,blurry,cropped,jpeg artifacts,text,error
```

# 質量關鍵詞

通常可以先書寫比較通用的質量關鍵詞

| 含義 | 關鍵詞 |
| -------- | -------- |
| 最高質量	     | the best quality     |
| 傑作     | masterpiece     |
| 極致的細節     | extreme detail     |
| 超高清     | ultra-high definition     |
| 8k     | 8k     |
| 驚人的     | amazing     |
| 原始     | original     |
| 景深     | Depth of field     |
| 非常詳細的壁紙     | extremely detailed wallpaper     |
| 非常詳細的cg     | extremely detailed cg     |
| 超現實圖片     | Surreal Photo     |
| 極高分辨率     | incredibly absurdres     |
| 插畫     | illustration     |
| 壁紙     | wallpaper     |


# 構圖風格

* 透明水彩绘	watercolor\_(medium)
* 水彩铅笔	watercolor pencil
* 官方艺术	official art
* 像素艺术	pixel art
* 游戏CG	game cg
* 高对比度	high contrast
* 漫画	comic
* 速涂风格	pastel color,sketch
* 墨水涂鸦	ink doodle
* 炭笔素描	charcoal sketch
* 素描艺术	sketch art
* 素描纸	sketch paper
* 铅笔	pencils
* 油画	oil painting
* 草图	posing sketch
* 传统媒体风格(蜡笔)	faux traditional media
* 传统媒体（基本上是手绘稿）	traditional_media
* 灰度	greyscale
* 平色	flat color
* 黑白	black and white
* 褪色的边界	fading border
* 浓重的对比(强调颜色)	heavy contrast
* 唯美主义绘画	Aestheticism Painting
* 现实主义绘画	realism
* 逼真,仿真	photorealistic,realistic
* 半写实	semi-realistic
* 现实的阴影	realistic shadows
* 照片真实感	Photorealistic
* 海报/主题聚焦	poster,caustics
* 杂志封面	magazine cover
* 杂志内页	magazine scan
* 动画截图	anime screeshot
* 剪影	silhouette
* 封面	covr
* 专辑	album
* Q版	chibi
* 80年代风格	80s (style)
* 90年代风格	90s (style)
* 新艺术(不支持含背景)	art nouveau
# 內容風格

* 哥特式	gothic
* 日本浮世绘风格	Ukiyoe
* 中国国画	Traditional Chinese painting
* 油画	coil painting
* 现实主义	realism
* 黑暗风格	film noir
* 水彩画	water colour painting
* 浪漫主义	Romanticism
* 文艺复兴风格	Renaissance Art
* 古典主义	Neoclassicism
* 学院派	academic art
* 写实主义	hyperrealism
* 日式风格	Japonism
* 巴洛克式	Baroque
* 民间艺术	folk art
* 水墨画	ink painting
* 古埃及	ancient Egypt
* 手稿	manuscript
* 学院主义	Academicism
* 愁苦主义	Miserablism
* 古希腊艺术	Ancient Greek art
* 迪士尼风格	Disney style
* 皮克斯风格	Picos style
* 插画风格	illustration
* 日本漫画风格	Japanese manga style
* 数字插画	digital illustration
* 黑暗幻想风格	dark fantasy style
* 波普艺术	Pop art
* 印象派	impressionism
* 后印象派	Neo-impressionism
* 野兽派	fauvism
* 表现主义	expressionism
* 抽象表现主义	abstract expressionism
* 立体主义	cubism
* 当代艺术	contemporary art
* 动画风格	anime style
* 卡通	cartoon
* 视觉艺术	visionary art
* 漫画书	comic book
* 蒸汽朋克	streampunk
* 黑暗主题	dark theme
* 微缩模型电影	miniature model film
* "能生成美漫封面
* 的漫画画面"	DC Comics
* 纯二次元风格人像	pixiv
* 威廉森风格	style ofAl Williamson
* 包豪斯风格	Bauhaus Style
* 安迪·沃霍尔风格	Warhol
* 光色主义	Luminsm
* 魔幻现实主义	Magic Realism
* 魔幻现实主义	Fantastic Realism
* 批判现实主义	Classical Realism
* 像素艺术	Pixel art
* 当代写实主义	Contemporary Realism
* 纪实的	Non-Fiction
* 同步性	Synchronism
* 建构主义	Constructivism
* 超现实主义	surreal，hyperrealistc
* 未来主义	futuristic
* ww3风格	ww3 style
* 适合科幻场景	fantasy
* 科幻的	Fiction
* 科幻	Science Fiction
* 科幻风格	sci-fi
* 赛博朋克	cyber punk
* 概念艺术	concept art
* 超现实主义	Hyperrealistic
* 黑暗奇幻	Dark Fantasy
* 飘渺奇幻	Ethereal Fantasy

# 色調
* 彩色的	colorful
* 鲜艳的颜色	vivid colors
* 怀旧的	nostalgia
* 明亮色	bright
* 高对比	high contrast
* 高饱和	High saturation
	
	# 鏡頭
	
* 男性焦点 (女性焦点)	male focus
* 动态姿势	dynamic pose
* 动态角度	dynamic angle
* 从上面拍	from above
* 从下面拍	from below
* 看向观众	looking at viewer
* 专注于脸	focus on face
* 全身拍摄	full-body shot
* 相机平行	camera level/Parallel angles
* 水平视角	horizontal view angle
* 前置焦点	foreground focus
* 鱼眼镜头	fish eye lens
* 色差	chromatic aberration
* 景深	depth of field
* 背光	backlighting
* 电影灯光	cinematic lighting
* 电影角度	cinematic angle
* 光线追踪	ray tracing
* 光线反射	reflection light
* 等距	isometric
* 等距视图	isometric view
	
	
	