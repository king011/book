# SnackBar

SnackBar 用來在 Scaffold 底部顯示一個 會自動隱藏的 提示消息

![](assets/SnackBar.gif)

```dart
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).taskVideoNewTitle),
      ),
      floatingActionButton: Builder(
        builder: (context) => FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text("要顯示的 消息"),
                    action: SnackBarAction(
                      label: "可選操作",
                      onPressed: () => debugPrint("tab"),
                    ),
                  ),
                );
              },
            ),
      ),
    );
```

當 BuildContext 在 Scaffold 正確 Scaffold.of(context) 會返回 null 此時可以將 SnackBar 放到 **Builder** 組件中