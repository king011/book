# libevent

libevent 是一個 c 的開源(BSD) 異步 io 事件循環

* 官網 [https://libevent.org/](https://libevent.org/)
* 源碼 [https://github.com/libevent/libevent](https://github.com/libevent/libevent)
* API [https://libevent.org/doc/](https://libevent.org/doc/)
* 官方教學 [https://libevent.org/libevent-book/](https://libevent.org/libevent-book/)