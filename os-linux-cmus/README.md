# cmus

cmus (C* Music Player) 一個 以 c 實現的 開源(GPL2.0) 純字符終端的 音樂播放器

* 源碼 [https://github.com/cmus/cmus](https://github.com/cmus/cmus)
* 官網 [https://cmus.github.io/](https://cmus.github.io/) 