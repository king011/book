# mame

MAME 是 Multiple Arcade Machine Emulator 的簡寫，一個開源(BSD or GPL 2+)的跨平臺街機模擬器


* 官網 [https://www.mamedev.org/](https://www.mamedev.org/)
* 源碼 [https://github.com/mamedev/mame](https://github.com/mamedev/mame)