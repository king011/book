# [Speed Dial](https://primeng.org/speeddial)

```
import {SpeedDialModule} from 'primeng/speeddial';
```

當點擊按鈕時，在頁面上顯示多個可操作的功能按鈕

![](assets/Linear.png)

![](assets/Circle.png)

![](assets/Tooltip.png)

![](assets/Mask.png)


使用 **p-speedDial** 定義按鈕
```
<p-speedDial [model]="items"></p-speedDial>
```

```
export class SpeedDialDemo implements OnInit {

    items: MenuItem[];

    constructor(private messageService: MessageService) { }

    ngOnInit() {
        this.items = [
            {
                icon: 'pi pi-pencil',
                command: () => {
                    this.messageService.add({ severity: 'info', summary: 'Add', detail: 'Data Added' });
                }
            },
            {
                icon: 'pi pi-refresh',
                command: () => {
                    this.messageService.add({ severity: 'success', summary: 'Update', detail: 'Data Updated' });
                }
            },
            {
                icon: 'pi pi-trash',
                command: () => {
                    this.messageService.add({ severity: 'error', summary: 'Delete', detail: 'Data Deleted' });
                }
            },
            {
                icon: 'pi pi-upload',
                routerLink: ['/fileupload']
            },
            {
                icon: 'pi pi-external-link',
                url: 'http://angular.io'

            }
        ];
    }
}
```

## MenuModel API

SplitButton 使用 menumodel api 來定義其條目，詳細信息請查看 [MenuModel API](https://primeng.org/menumodel)

## Type

SpeedDial 提供了四種不同類型

* **linear** 直線顯示功能按鈕
* **circle** 圓形顯示功能按鈕
* **semi-circle** 半圓顯示功能按鈕
* **quarter-circle** 1/4 圓顯示功能按鈕

## Direction

你可以指定功能按鈕打開的方向

對於 **linear** **semi-circle** 類型的按鈕支持如下值:

* up
* down
* left
* right

對於 **quarter-circle** 類型的按鈕支持如下值:
* up-left
* up-right
* down-left
* down-right

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onVisibleChange     | visible: Whether the actions are visible.     | 當元素可見性變化時回調     |
| onClick     | event: Browser event.     | 當點擊按鈕時回調     |
| onShow     | -     | 當顯示功能菜單時回調     |
| onHide     | -     | 當隱藏功能菜單時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| id     | string     | null     | 元素唯一 id     |
| model     | object     | null     | MenuModel 實例來定義功能按鈕     |
| visible     | boolean     | false     | 指定疊加層是否可見     |
| className     | string     | null     | 組件的 css class     |
| style     | object| null     | 組件的內聯 css style     |
| direction     | string     | up     | 功能菜單顯示方向:  'up', 'down', 'left', 'right', 'up-left', 'up-right', 'down-left' and 'down-right'     |
| transitionDelay     | number     | 30     | 每個操作的轉換延遲步驟     |
| type     | string     | linear     | 按鈕類型: linear circle semi-circle quarter-circle     |
| radius     | number     | 0     | \*circle 的圓角程度      |
| mask     | boolean     | false     | 是否在功能菜單顯示時顯示遮罩層     |
| disabled     | boolean     | false     | 是否禁用菜單     |
| hideOnClickOutside     | boolean     | true     | 單擊組件外部區域自動隱藏功能菜單     |
| buttonClassName     | string     | null     | 按鈕的 css class     |
| buttonStyle     | string     | null     | 按鈕的內聯 css style     |
| buttonTemplate     | any     | null     | button 元素的模板     |
| maskClassName     | string     | null     | 遮罩的 css class      |
| maskStyle     | string     | null     | 遮罩的內聯 css style     |
| showIcon     | string     | pi pi-plus     | 顯示圖標     |
| hideIcon     | string     | null     | 隱藏圖標     |
| rotateAnimation     | boolean     | true     | 圖個爲 true 在 hideIcon 不存在時，旋轉 showIcon 作爲隱藏圖標     |

# Templates

| Name | Parameters |
| -------- | -------- |
| button     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-speeddial     | Container element.     |
| p-speeddial-button     | Button element of speeddial.     |
| p-speeddial-mask     | Mask element of speeddial.     |
| p-speeddial-list     | List of the actions.     |
| p-speeddial-item     | Each action item of list.     |


