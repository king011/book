# 查詢視頻

通常你需要使用 **--info/-i** 參數來查詢可以的視頻質量與格式：

```
$ you-get -i 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
site:                YouTube
title:               Me at the zoo
streams:             # Available quality and codecs
    [ DASH ] ____________________________________
    - itag:          242
      container:     webm
      quality:       320x240
      size:          0.6 MiB (618358 bytes)
    # download-with: you-get --itag=242 [URL]

    - itag:          395
      container:     mp4
      quality:       320x240
      size:          0.5 MiB (550743 bytes)
    # download-with: you-get --itag=395 [URL]

    - itag:          133
      container:     mp4
      quality:       320x240
      size:          0.5 MiB (498558 bytes)
    # download-with: you-get --itag=133 [URL]

    - itag:          278
      container:     webm
      quality:       192x144
      size:          0.4 MiB (392857 bytes)
    # download-with: you-get --itag=278 [URL]

    - itag:          160
      container:     mp4
      quality:       192x144
      size:          0.4 MiB (370882 bytes)
    # download-with: you-get --itag=160 [URL]

    - itag:          394
      container:     mp4
      quality:       192x144
      size:          0.4 MiB (367261 bytes)
    # download-with: you-get --itag=394 [URL]

    [ DEFAULT ] _________________________________
    - itag:          43
      container:     webm
      quality:       medium
      size:          0.5 MiB (568748 bytes)
    # download-with: you-get --itag=43 [URL]

    - itag:          18
      container:     mp4
      quality:       small
    # download-with: you-get --itag=18 [URL]

    - itag:          36
      container:     3gp
      quality:       small
    # download-with: you-get --itag=36 [URL]

    - itag:          17
      container:     3gp
      quality:       small
    # download-with: you-get --itag=17 [URL]
```

# 下載視頻

直接傳入網頁地址 you-get 會下載 -i 參數列出的第一視頻
```
you-get 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
```

此外你可以傳入 -i 參數中返回的 **--itag** 來下載指定格式與質量的視頻

```
you-get --itag=18 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
```

* 目前，大多數站點還沒有實現格式選擇；在這種情況下通常下載的質量最高的視頻
* ffmpeg 是必須的依賴，同樣下載流式傳輸的視頻以及1080p或高分辨率的 youtube 視頻
* 如果不想下載後的視頻被合併，使用 **--no-merge/-n** 參數


## 下載 URL

如果你已經有了資源的確切URL也可以直接下載

> 這是一個試驗性的功能，可能存在一些問題

```
$ you-get https://stallman.org/rms.jpg
Site:       stallman.org
Title:      rms
Type:       JPEG Image (image/jpeg)
Size:       0.06 MiB (66482 Bytes)

Downloading rms.jpg ...
100.0% (  0.1/0.1  MB) ├████████████████████████████████████████┤[1/1]  127 kB/s
```
```
$ you-get http://kopasas.tumblr.com/post/69361932517
Site:       Tumblr.com
Title:      kopasas
Type:       Unknown type (None)
Size:       0.51 MiB (536583 Bytes)

Site:       Tumblr.com
Title:      tumblr_mxhg13jx4n1sftq6do1_1280
Type:       Portable Network Graphics (image/png)
Size:       0.51 MiB (536583 Bytes)

Downloading tumblr_mxhg13jx4n1sftq6do1_1280.png ...
100.0% (  0.5/0.5  MB) ├████████████████████████████████████████┤[1/1]   22 MB/s
```

# 暫停與恢復

你可以使用 **CTRL+C** 中斷下載

一個臨時的 **.download** 檔案會保存在輸出目錄中。下次你可以使用相同的參數運行 you-get，下載進度將從上次會話恢復。如果完全下載(臨時檔案 .download 消失了)，you-get 將跳過下載

要強制重新下載請使用 **--force/-f** 參數(這會覆蓋任何現有的檔案或同名臨時檔案)

# 設置輸出名稱

使用 **--output-dir/-o** 設置輸出檔案夾，使用 **--output-filename/-O** 設置輸出名稱

```
$ you-get -o ~/Videos -O zoo.webm 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
```

默認會在當前路徑下保存與視頻標題相同的檔案，但視頻標題可能含有操作系統不允許的檔案名稱字符，此時設置輸出名稱會有用

# 代理

* 使用 **--http-proxy/-x** 設置使用 http 代理下載
* 使用 **--socks-proxy/-s** 設置使用 socks 代理下載

# 播放視頻

使用 **--player/-p** 參數將視頻輸入媒體播放器而非下載

```
you-get -p vlc 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
```

```
you-get -p chromium 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
```

# cookie

一些視頻是非公開的，此時需要使用 **--cookies/-c** 參數來指定 cookie 檔案，

* 目前支持兩種 cookie 格式： Mozilla cookies.sqlite 和 Netscape cookies.txt.