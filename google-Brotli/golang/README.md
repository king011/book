# golang

[github.com/andybalholm/brotli](https://github.com/andybalholm/brotli) 是將 c 代碼使用 c2go 翻譯 移植的 純golang 版本的 brotli 實現


```
go get -u -v github.com/andybalholm/brotli
```

```
package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/andybalholm/brotli"
)

func main() {
	f, e := os.Open(`main.go`)
	if e != nil {
		log.Fatalln(e)
	}
	defer f.Close()

	// 編碼
	var dst bytes.Buffer
	w := brotli.NewWriter(&dst)
	_, e = io.Copy(w, f)
	if e != nil {
		return
	}
	//	w.Flush()
	w.Close()

	// 解碼
	r := brotli.NewReader(&dst)
	b, e := ioutil.ReadAll(r)
	if e != nil {
		return
	}
	fmt.Println(string(b))
}
```