# vimrc

~/.vimrc 是vim的 用戶配置 檔案

```bash
set nu
set encoding=utf-8
set fileencoding=utf-8
set t_Co=256
```