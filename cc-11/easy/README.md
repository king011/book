# for 

現在 可以 遍歷 一個 範圍(實現了標準 迭代器的 容器)

```
#include <iostream>
#include <vector>
using namespace std;
 
int main()
{
    vector<int> v;
    for(int i=0;i<10;++i)
    {
        v.push_back(i);
    }
    int arrs[] = {0,1,2,3,4,5,6,7,8,9};
 
    for(auto d:v)
    {
        cout<<d<<" ";
    }
    cout<<"\n";
    for(auto d:arrs)
    {
        cout<<d<<" ";
    }
    cout<<"\n";
 
    return 0;
}
```