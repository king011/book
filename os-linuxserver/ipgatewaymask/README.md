# gateway

```
# 查看 gateway 設置
route
route -n

# 添加 默認 gateway
route add default gw ip

# 刪除 默認 gateway
route del default 
```

# ifconfig

```
# 查看所有網卡接口
ifconfig

# 激活網卡接口
ifconfig eth0 up
# 關閉網卡接口
ifconfig eth0 down

# 查看指定名稱的網卡接口
ifconfig eth0

# 爲指定網卡設置 ip 
ifconfig eth0 192.168.251.181
ifconfig eth0 192.168.251.181/24
ifconfig eth0 192.168.251.181 netmask 255.255.255.0
```