# ev_io

ev_io 直接操作 fd 通常應該將 fd 設置爲非阻塞的

在 調用 read 時 沒有數據 此時 一個 非阻塞的 read 會收到 EAGAN 錯誤


```
// 初始化 ev_io
ev_io_init (ev_io *, callback, int fd, int events)
ev_io_set (ev_io *, int fd, int events)

// 類似 ev_io_set 但僅修改 關注的事件，某些後端會更快 libev 可以假定 fd沒有 改變 而 ev_io_set 每次都任務 fd 是全新的
ev_io_modify (ev_io *, int events)

int fd [no-modify]
int events [no-modify]
```

# ev_timer

ev_timer 是一個定時器使用相對時間

```
// 初始化 定時器
ev_timer_init (ev_timer *, callback, ev_tstamp after, ev_tstamp repeat)
ev_timer_set (ev_timer *, ev_tstamp after, ev_tstamp repeat)

ev_timer_start (loop, ev_timer *)
ev_timer_again (loop, ev_timer *)

ev_tstamp ev_timer_remaining (loop, ev_timer *)
ev_tstamp repeat [read-write]
```

after 指定第一次 回調時間 repeat 指定之後的回調事件 如果repeat爲0 則只回調一次

如果要 修改 定時器 時間 需要 stop 後 start

```
ev_timer_init (timer, callback, 60., 0.);
ev_timer_start (loop, timer);

ev_timer_stop (loop, timer);
ev_timer_set (timer, 60., 0.);
ev_timer_start (loop, timer);
```

可以使用 ev_timer_again 直接忽略掉 ev_timer_set 和 ev_timer_start 函數 ev_timer_again 可以在不stop時 修改 定時器事件
```
ev_init (timer, callback);
timer->repeat = 60.;
ev_timer_again (loop, timer);

ev_timer_again (loop, timer);

timer->repeat = 30.;
ev_timer_again (loop, timer);
```

# ev\_periodic

ev\_periodic 類似 ev\_timer 當使用 日曆時間