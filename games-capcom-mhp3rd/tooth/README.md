# 雷狼龍
![](assets/雷狼龍.jpeg)

## 基本抗性
◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>△<br>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>○<br>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头<br>背部(超带电状态)</center></div>
</td><td><div><center>头<br>背部(超带电状态)</center></div>
</td><td><div><center>头<br>后脚(甲壳)(超带电状态)</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">45 (60)  </font></td><td> <font color="#ff0000">50 (65)  </font></td><td> <font color="#ff0000">60 (70)  </font></td><td> 15 (20)  </td><td> 20 (25)  </td><td> 0  </td><td> 20 (30)  </td><td> 5 (15)  </td><td> 100 (120) </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 25 (36) </td><td> 25 (30) </td><td> 35 (40) </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 5 (10) </td><td> 5 </td><td> 0 </td><td> 350
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 40 <font color="#ff0000">(50) </font></td><td> 36 <font color="#ff0000">(50) </font></td><td> 30 (35) </td><td> 10 (15) </td><td> 15 </td><td> 0 </td><td> 15 (20) </td><td> 5 </td><td> 0 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚（甲壳） </td><td> 25 (36) </td><td> 30 (36) </td><td> 25 (35) </td><td> 5 (10) </td><td> 5 (10) </td><td> 0 </td><td> 10 (15) </td><td> 5 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 30 (36) </td><td> 30 (35) </td><td> 30 (40) </td><td> 5 (10) </td><td> 5 (10) </td><td> 0 </td><td> 5 (15) </td><td> 5 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚（甲壳） </td><td> 36 (40) </td><td> 35 (40) </td><td> 35 <font color="#ff0000">(45) </font></td><td> 5 (12) </td><td> 5 (10) </td><td> 0 </td><td> 10 (15) </td><td> 5 </td><td> 0 </td><td> 左220/右220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 30 (36) </td><td> 30 (35) </td><td> 30 (40) </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 5 (10) </td><td> 5 </td><td> 0 </td><td> 左220/右220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 27 </td><td> 25 </td><td> 20 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 21 </td><td> 15 </td><td> 10 (15) </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 15 </td><td> 5 </td><td> 0 </td><td> 180
</td></tr></tbody></table>

## 屬性攻擊

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 150 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (580) </td><td> 180 (900) </td><td> 180 (900) </td><td> 150 (750) </td><td> 150 (850)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 40秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 90秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 200 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>

## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> △ </td><td> 初回15秒（疲劳18秒） → 2回10秒（疲劳13秒） → 3回开始8秒（疲劳8秒） （会增加雷狼龙蓄电情况，且超带电状态下无效）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒　（疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>

## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雷狼龙的甲壳">雷狼龙的甲壳</a> <font color="#008000"><b>40%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E8%93%84%E7%94%B5%E5%A3%B3" title="雷狼龙的蓄电壳">雷狼龙的蓄电壳</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B8%A6%E7%94%B5%E6%AF%9B" title="雷狼龙的带电毛">雷狼龙的带电毛</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%88%AA" title="雷狼龙的爪">雷狼龙的爪</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雷狼龙的坚壳">雷狼龙的坚壳</a> <font color="#008000"><b>38%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%AB%98%E7%94%B5%E6%AF%9B" title="雷狼龙的高电毛">雷狼龙的高电毛</a> <font color="#008000"><b>18%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="雷狼龙的尖爪">雷狼龙的尖爪</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%AB%98%E7%94%B5%E5%A3%B3" title="雷狼龙的高电壳">雷狼龙的高电壳</a> <font color="#008000"><b>12%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B8%A6%E7%94%B5%E6%AF%9B" title="雷狼龙的带电毛">雷狼龙的带电毛</a> <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E8%93%84%E7%94%B5%E5%A3%B3" title="雷狼龙的蓄电壳">雷狼龙的蓄电壳</a> <font color="#008000"><b>6%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%A2%A7%E7%8E%89" title="雷狼龙的碧玉">雷狼龙的碧玉</a> <font color="#008000"><b>3%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雷狼龙的甲壳">雷狼龙的甲壳</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B8%A6%E7%94%B5%E6%AF%9B" title="雷狼龙的带电毛">雷狼龙的带电毛</a> <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="雷狼龙的尾巴">雷狼龙的尾巴</a> <font color="#008000"><b>70%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a> <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="雷狼龙的尾巴">雷狼龙的尾巴</a> <font color="#008000"><b>62%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雷狼龙的坚壳">雷狼龙的坚壳</a> <font color="#008000"><b>18%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a> <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雷狼龙的甲壳">雷狼龙的甲壳</a> <font color="#008000"><b>18%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%A2%A7%E7%8E%89" title="雷狼龙的碧玉">雷狼龙的碧玉</a> <font color="#008000"><b>2%</b></font> </td><td> 断尾需要积累360的伤害值
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E8%B6%85%E7%94%B5%E9%9B%B7%E5%85%89%E8%99%AB" title="超电雷光虫">超电雷光虫</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雷狼龙的甲壳">雷狼龙的甲壳</a> <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a> <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E8%B6%85%E7%94%B5%E9%9B%B7%E5%85%89%E8%99%AB" title="超电雷光虫">超电雷光虫</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雷狼龙的坚壳">雷狼龙的坚壳</a> <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%A2%A7%E7%8E%89" title="雷狼龙的碧玉">雷狼龙的碧玉</a> <font color="#008000"><b>2%</b></font> </td><td> 超带电状态解除时
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部捕虫 </td><td> - </td><td> <a href="/%E8%B6%85%E7%94%B5%E9%9B%B7%E5%85%89%E8%99%AB" title="超电雷光虫">超电雷光虫</a> <font color="#008000"><b>90%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B8%A6%E7%94%B5%E6%AF%9B" title="雷狼龙的带电毛">雷狼龙的带电毛</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E8%B6%85%E7%94%B5%E9%9B%B7%E5%85%89%E8%99%AB" title="超电雷光虫">超电雷光虫</a> <font color="#008000"><b>90%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%AB%98%E7%94%B5%E6%AF%9B" title="雷狼龙的高电毛">雷狼龙的高电毛</a> <font color="#008000"><b>10%</b></font> </td><td> 将其击倒时，去其背部捕虫（需携带捕虫网）
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 角 </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E8%A7%92" title="雷狼龙的角">雷狼龙的角</a>*1 <font color="#008000"><b>80%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雷狼龙的甲壳">雷狼龙的甲壳</a>*1 <font color="#008000"><b>17%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a>*1 <font color="#008000"><b>3%</b></font> </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%96%E8%A7%92" title="雷狼龙的尖角">雷狼龙的尖角</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E8%A7%92" title="雷狼龙的角">雷狼龙的角</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雷狼龙的坚壳">雷狼龙的坚壳</a>*1 <font color="#008000"><b>9%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%A2%A7%E7%8E%89" title="雷狼龙的碧玉">雷狼龙的碧玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 双角全破概率提升
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%88%AA" title="雷狼龙的爪">雷狼龙的爪</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%88%AA" title="雷狼龙的爪">雷狼龙的爪</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E8%93%84%E7%94%B5%E5%A3%B3" title="雷狼龙的蓄电壳">雷狼龙的蓄电壳</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="雷狼龙的尖爪">雷狼龙的尖爪</a>*1 <font color="#008000"><b>67%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="雷狼龙的尖爪">雷狼龙的尖爪</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%88%AA" title="雷狼龙的爪">雷狼龙的爪</a>*3 <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%AB%98%E7%94%B5%E5%A3%B3" title="雷狼龙的高电壳">雷狼龙的高电壳</a>*1 <font color="#008000"><b>8%</b></font> </td><td> 双前脚甲壳击破概率提升
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 无 </td><td> 无 </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B8%A6%E7%94%B5%E6%AF%9B" title="雷狼龙的带电毛">雷狼龙的带电毛</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E8%93%84%E7%94%B5%E5%A3%B3" title="雷狼龙的蓄电壳">雷狼龙的蓄电壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%88%AA" title="雷狼龙的爪">雷狼龙的爪</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雷狼龙的甲壳">雷狼龙的甲壳</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E8%B6%85%E7%94%B5%E9%9B%B7%E5%85%89%E8%99%AB" title="超电雷光虫">超电雷光虫</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a>*1 <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%AB%98%E7%94%B5%E6%AF%9B" title="雷狼龙的高电毛">雷狼龙的高电毛</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%AB%98%E7%94%B5%E5%A3%B3" title="雷狼龙的高电壳">雷狼龙的高电壳</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="雷狼龙的尖爪">雷狼龙的尖爪</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雷狼龙的坚壳">雷狼龙的坚壳</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E8%B6%85%E7%94%B5%E9%9B%B7%E5%85%89%E8%99%AB" title="超电雷光虫">超电雷光虫</a>*4 <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雷狼龙的逆鳞">雷狼龙的逆鳞</a>*1 <font color="#008000"><b>4%</b></font><br><a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99%E7%9A%84%E7%A2%A7%E7%8E%89" title="雷狼龙的碧玉">雷狼龙的碧玉</a>*1 <font color="#008000"><b>2%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>

## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>月下雷鳴
</p>
</td><td>
<p>雷狼龙1头狩猎。同村4星的紧急任务，不过雷狼龙的初始区域换成了5区。
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>縄張りに進入するべからず
</p>
</td><td>
<p>雷狼龙1头狩猎。雷狼龙初始在5区，而后倾向于往6区转移。
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>迅速·雷光·太刀ふさぐ壁
</p>
</td><td>
<p>大型连续狩猎，<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>首先于6区登场，其次是5区的雷狼龙。选择火属性武器较好。
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>狩られる前に狩れ！
</p>
</td><td>
<p>雷狼龙1头狩猎
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>無双の狩人
</p>
</td><td>
<p>雷狼龙1头狩猎
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>月下の渓流に、双雷は轟く
</p>
</td><td>
<p>雷狼龙2头狩猎
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>縄張りに進入するべからず
</p>
</td><td>
<p>雷狼龙1头狩猎
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>緊迫した渓流の中で
</p>
</td><td>
<p>全部大型怪物狩猎 
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%B8%A9%E6%B3%89%E4%BB%BB%E5%8A%A1" title="温泉任务">温泉任务</a>
</p>
</td><td>
<p>竜の訪れる秘泉
</p>
</td><td>
<p>全部大型怪物狩猎
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 农夫三拳 </td><td> 用前爪交替拍地，超带电前拍2次，超带电后拍3次并附加雷属性【小】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 扣尾 </td><td> 背对猎人后，用后空翻将尾巴砸向猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 向猎人突进攻击，超带电时附带雷属性【小】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 猛扑 </td><td> 跃起身子猛地扑向猎人，超带电后可以扑2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头槌 </td><td> 用头撞击攻击，出招比较突然
</td></tr>
<tr style="background:#f9f9f9;">
<td> 阿姆斯特朗大回旋 </td><td> 强力的回旋甩尾攻击，非常帅气，超带电后附加雷属性【大】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 蓄电 </td><td> 积蓄雷光虫来提升自己的力量，积蓄中无任何行动，蓄3次后进入超带电状态（濒死时只需蓄1次）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 雷光球 </td><td> 小跃一下身体放出雷光球，带电前向右放出1颗；带电后向右放出2颗；超带电后先向右放出2颗，再向左放出2颗
</td></tr>
<tr style="background:#f9f9f9;">
<td> 雷击 </td><td> 蓄完电进入超带电状态的一瞬间全身放出雷击，附带雷属性【大】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 落雷 </td><td> （怒状态限定）大范围内的无规律落雷攻击，附带雷属性【大】，落雷后会接雷击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背砸 </td><td> （怒状态限定）跃到高空猛地用背砸向猎人，附带雷属性【大】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食 </td><td> 身体抽搐几下后，扑向猎人，扑中即进入捕食状态
</td></tr></tbody></table>