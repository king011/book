# boost

boost 一個 高效 高質量 開源 的跨平台 c++ 庫

其作者 大多為 c++ 標準委員會成員 故 其風格類似 STL 

且 標準庫 新組件 大多直接從boost中 選取

* 官網 [https://www.boost.org/](https://www.boost.org/)