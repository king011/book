# [Menu](https://primeng.org/menu)

```
import {MenuModule} from 'primeng/menu';
import {MenuItem} from 'primeng/api';
```

Menu 是一個 導航/命令 組件，支持動態和靜態定位

![](assets/primeng.org_menu.png)

```
<p-menu [model]="items"></p-menu>
```

```
export class MenuDemo {
    
    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {label: 'New', icon: 'pi pi-fw pi-plus'},
            {label: 'Open', icon: 'pi pi-fw pi-download'},
            {label: 'Undo', icon: 'pi pi-fw pi-refresh'}
        ];
    }
}
```

## SubMenus

Menu 支持 **1級** 的嵌套子菜單(只允許嵌套一次)

> 此時父菜單相當於一個菜單組分類

```
export class MenuDemo {
    
    items: MenuItem[];

    ngOnInit() {
        this.items = [{
            label: 'File',
            items: [
                {label: 'New', icon: 'pi pi-fw pi-plus'},
                {label: 'Download', icon: 'pi pi-fw pi-download'}
            ]
        },
        {
            label: 'Edit',
            items: [
                {label: 'Add User', icon: 'pi pi-fw pi-user-plus'},
                {label: 'Remove User', icon: 'pi pi-fw pi-user-minus'}
            ]
        }];
    }
}
```

## Popup Mode

默認情況下，菜單是內聯的，設置 **popup** 屬性爲 true 來使用彈出菜單

```
<p-menu #menu [popup]="true" [model]="items"></p-menu>
<button type="button" pButton icon="pi pi-list" label="Show" (click)="menu.toggle($event)"></button>
```

## Animation Configuration

**showTransitionOptions/hideTransitionOptions** 屬性來設置菜單的過渡動畫，下面例子將完全禁用動畫

```
<p-menu [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'" #menu [popup]="true" [model]="items"></p-menu>
<button type="button" pButton icon="pi pi-list" label="Show" (click)="menu.toggle($event)"></button>
```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onShow     | event: Event object     | 顯示時回調     |
| onHide     | event: Event object     | 隱藏時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| popup     | boolean     | false     | 是否作爲彈出菜單     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| appendTo     | any     | null     | Target element to attach the overlay, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| baseZIndex     | number     | 0     | 分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| showTransitionOptions     | string     | 0.12s cubic-bezier(0, 0, 0.2, 1)	     | 顯示過渡動畫     |
| hideTransitionOptions     | string     | 0.1s linear	     | 隱藏過渡動畫     |

# Methods

| Name | Parameters | Description |
| -------- | -------- | -------- |
| toggle     | event: browser event     | 切換菜單顯示狀態     |
| show     | event: browser event     | 顯示菜單     |
| hide     | -     | 隱藏菜單     |

# Styling

| Name | Element |
| -------- | -------- |
| p-menu     | Container element.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |


