# find_path

find_path 定義一個變量 同時 用來 查找一個 檔案
* 如果 查到 變量存儲 路徑 否則 爲 VAR-NOTFOUND
* NAMES 指定 要查找的 檔案
* PATHS 指定 額外的 查找位置
```
find_path(boost_INCLUDE_DIR
  NAMES boost/version.hpp
  PATHS ${INCLUDE_DIR}
)
if(boost_INCLUDE_DIR)
    message("find boost")
    message(${boost_INCLUDE_DIR})
else()
    message(FATAL_ERROR "not found boost")
endif()
```

另外 也會從如下 系統 環境變量中 查找
* CMAKE\_INCLUDE\_PATH

# find_library

find\_library&nbsp;使用&nbsp;類似&nbsp;find\_path 的方式 來查找一個 庫
```cmake
find_library(boost_systtem_LIBRARIE
    libboost_system.so
)
if(boost_systtem_LIBRARIE)
    message("find boost_systtem")
    message(${boost_systtem_LIBRARIE})
else()
    message(FATAL_ERROR "not found asio")
endif()
```
另外 也會從如下 系統 環境變量中 查找
* CMAKE\_LIBRARY\_PATH
