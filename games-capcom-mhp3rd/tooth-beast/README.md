# 野豬王
![](assets/野豬王.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">80 </font></td><td> <font color="#ff0000">70 </font></td><td> <font color="#ff0000">70 </font></td><td> 20 </td><td> 10 </td><td> 30 </td><td> 5 </td><td> 0 </td><td> 100 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头以外 </td><td> <font color="#ff0000">70 </font></td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">60 </font></td><td> 10 </td><td> 5 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 120
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 30 </td><td> 150 </td><td> 30 </td><td> 100 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (330) </td><td> 100 (350) </td><td> 100 (330) </td><td> 150 (400) </td><td> 50 (350)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 90秒 </td><td> 20秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 180 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回15秒（疲劳25秒） → 2回10秒（疲劳20秒） → 3回5秒（疲劳15秒） → 4回开始5秒（疲劳10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场<br>下位</b> </td><td> <b>集会浴场<br>上位</b> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E5%A4%A7%E9%AA%A8" title="大骨">大骨</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%9A%AE" title="大猪的皮">大猪的皮</a> <font color="#008000"><b>50%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%89%99" title="大猪的牙">大猪的牙</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E9%87%8E%E7%8C%AA%E7%9A%84%E5%A4%B4" title="大野猪的头">大野猪的头</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%A1%AC%E7%9A%AE" title="大猪的硬皮">大猪的硬皮</a> <font color="#008000"><b>40%</b></font><br><a href="/%E5%9D%9A%E5%9B%BA%E4%B9%8B%E9%AA%A8" title="坚固之骨">坚固之骨</a> <font color="#008000"><b>25%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%89%99" title="大猪的牙">大猪的牙</a> <font color="#008000"><b>17%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%9A%AE" title="大猪的皮">大猪的皮</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%A4%A7%E9%87%8E%E7%8C%AA%E7%9A%84%E5%A4%B4" title="大野猪的头">大野猪的头</a> <font color="#008000"><b>8%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%89%99" title="大猪的牙">大猪的牙</a> <font color="#008000"><b>20%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%89%99" title="大猪的牙">大猪的牙</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E7%89%99%E5%85%BD%E6%B3%AA" title="大颗牙兽泪">大颗牙兽泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%A1%AC%E7%9A%AE" title="大猪的硬皮">大猪的硬皮</a> <font color="#008000"><b>20%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%89%99" title="大猪的牙">大猪的牙</a> <font color="#008000"><b>5%</b></font> </td><td> 将其击倒时可能会掉落
</td></tr></tbody></table>
## 部位破壞
無
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%9A%AE" title="大猪的皮">大猪的皮</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E5%A4%A7%E9%AA%A8" title="大骨">大骨</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%A4%A7%E9%87%8E%E7%8C%AA%E7%9A%84%E5%A4%B4" title="大野猪的头">大野猪的头</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%89%99" title="大猪的牙">大猪的牙</a>*1 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E5%A4%A7%E7%8C%AA%E7%9A%84%E7%A1%AC%E7%9A%AE" title="大猪的硬皮">大猪的硬皮</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E5%9D%9A%E5%9B%BA%E4%B9%8B%E9%AA%A8" title="坚固之骨">坚固之骨</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E5%A4%A7%E9%87%8E%E7%8C%AA%E7%9A%84%E5%A4%B4" title="大野猪的头">大野猪的头</a>*1 <font color="#008000"><b>15%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2">
<p>体型
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>基础
</p><p>体力
</p>
</td><td style="text-align: center">
<p>攻击力
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>全体
</p><p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td style="text-align: center" rowspan="9"> 村長
</td><td style="text-align: center" rowspan="9"> 下位
</td><td style="text-align: center" rowspan="4"> ★2
</td><td> ざわめく森
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 720
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td> 生下幸福之蛋的丸鸟
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 720
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td> 飞奔的凶猛大猪！
</td><td style="text-align: center">讨伐一头野猪王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 720
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_5">
<td> 制作良药所必须的东西
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 720
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_6">
<td style="text-align: center" rowspan="4"> ★3
</td><td> <font color="#f40"> 踊るクルペッコ </font>
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 810
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_7">
<td> 激走嘉年华!
</td><td style="text-align: center"> 讨伐10头<a href="/%E9%87%8E%E7%8C%AA" title="野猪">野猪</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 810
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_8">
<td> 大猪の猛攻！
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 720<br>720
</td><td style="text-align: right"> 105<br>105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_9">
<td> 催眠的青色集团
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 810
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_10">
<td style="text-align: center"> ★5
</td><td> 白银的抱拥
</td><td style="text-align: center">讨伐所有大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1440
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_11">
<td style="text-align: center" rowspan="11"> 集会浴场
</td><td style="text-align: center" rowspan="4"> 初心者
</td><td style="text-align: center" rowspan="2"> ★1
</td><td> 突进、猛进、大野猪王
</td><td style="text-align: center">讨伐一头野猪王
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_12">
<td> 特产蘑菇盛宴
</td><td style="text-align: center"> 缴纳20个<a href="/index.php?title=%E7%89%B9%E4%BA%A7%E8%98%91%E8%8F%87&amp;action=edit&amp;redlink=1" class="new" title="特产蘑菇（尚未撰写）">特产蘑菇</a>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_13">
<td style="text-align: center" rowspan="2"> ★2
</td><td> 激走嘉年华!
</td><td style="text-align: center"> 讨伐10头<a href="/%E9%87%8E%E7%8C%AA" title="野猪">野猪</a>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1080
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_14">
<td> 极寒之地的采掘委托
</td><td style="text-align: center"> 缴纳10个<a href="/index.php?title=%E8%A1%80%E7%9F%B3&amp;action=edit&amp;redlink=1" class="new" title="血石（尚未撰写）">血石</a>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1080
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_15">
<td style="text-align: center" rowspan="3"> 下位
</td><td style="text-align: center"> ★3
</td><td> 飞奔的凶猛大猪！
</td><td style="text-align: center">讨伐一头野猪王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2070
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_16">
<td style="text-align: center" rowspan="2"> ★5
</td><td> 栖息于森之物
</td><td style="text-align: center">讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2160
</td><td style="text-align: right"> 145
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_17">
<td> 群集的强豪
</td><td style="text-align: center">讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2340
</td><td style="text-align: right"> 140
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_18">
<td style="text-align: center" rowspan="4"> 上位
</td><td style="text-align: center" rowspan="3"> ★6
</td><td> 水没林采集之旅
</td><td style="text-align: center"> 缴纳<a href="/index.php?title=%E7%8C%AB%E5%AE%85%E5%88%B8&amp;action=edit&amp;redlink=1" class="new" title="猫宅券（尚未撰写）">猫宅券</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3600
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_19">
<td> 猪突猛进
</td><td style="text-align: center">讨伐两头野猪王
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2160<br>2160
</td><td style="text-align: right"> 250<br>250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_20">
<td> 飞奔的凶猛大猪！
</td><td style="text-align: center">讨伐一头野猪王
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3600
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_21">
<td style="text-align: center"> ★7
</td><td> 暗夜的迅龙
</td><td style="text-align: center">讨伐一头<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 1620
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_22">
<td style="text-align: center" rowspan="2"> 番台
</td><td style="text-align: center" rowspan="2"> 饮料
</td><td style="text-align: center" rowspan="2"> ★4
</td><td> 中庭的大猪
</td><td style="text-align: center">讨伐两头野猪王
</td><td style="text-align: right"> 91
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 1980<br>1980
</td><td style="text-align: right"> 130<br>130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_23">
<td> 白兔兽与大猪
</td><td style="text-align: center">讨伐一头<a href="/%E7%99%BD%E5%85%94%E5%85%BD" title="白兔兽">白兔兽</a>和一头野猪王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1620
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进（短） </td><td> 无预备动作朝猎人进行短距离突进
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进（长） </td><td> 原地蹭3下猪蹄同时不断修正猎人方位，然后进行长距离突进
</td></tr>
<tr style="background:#f9f9f9;">
<td> 导航猪突 </td><td> （上位怒时限定）先朝猎人突进，然后转为U型突进，转回来再重新锁定猎人突进，追踪性很强
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩牙 </td><td> 先原地甩大牙，然后转身180°再次甩
</td></tr></tbody></table>

# 青熊獸
![](assets/青熊獸.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>头<br>上半身<br>下半身<br>臀部</center></div>
</td><td><div><center>头<br>上半身<br>下半身<br>臀部</center></div>
</td><td><div><center>头<br>上半身</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">60 </font></td><td> 20 </td><td> 5 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 100 </td><td> 130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 上半身 </td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">80 </font></td><td> 25 </td><td> 5 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 33 </td><td> 35 </td><td> 28 </td><td> 30 </td><td> 5 </td><td> 30 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 80
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下半身 </td><td> <font color="#ff0000">56 </font></td><td> <font color="#ff0000">56 </font></td><td> 40 </td><td> 15 </td><td> 5 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 臀部 </td><td> <font color="#ff0000">85 </font></td><td> <font color="#ff0000">85 </font></td><td> 44 </td><td> 15 </td><td> 5 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 150
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 60 </td><td> 80 </td><td> 80 </td><td> 100 </td><td> 100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 80 (460) </td><td> 80 (400) </td><td> 80 (400) </td><td> 80 (420) </td><td> 80 (420)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 12.5秒 </td><td> 60秒 </td><td> 15秒 </td><td> 90秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳20秒） → 2回8秒（疲劳10秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回12秒（疲劳20秒） → 2回7秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒　（疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> △ </td><td> 令其胆怯并出现掉落物，发怒时无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃） 摆脱束缚攻击 同音爆弹使用效果，发怒时有效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> ○ </td><td> 疲劳时就算是发现状态也会吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b> </font></td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b> </font></td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E6%AF%9B" title="青熊兽的毛">青熊兽的毛</a> <font color="#008000"><b>65%</b><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E7%94%B2%E5%A3%B3" title="青熊兽的甲壳">青熊兽的甲壳</a> <font color="#008000"><b>25%</b><br><a href="/%E5%A4%A7%E9%AA%A8" title="大骨">大骨</a> <font color="#008000"><b>10%</b> </font></font></font></td><td> <a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="青熊兽的刚毛">青熊兽的刚毛</a> <font color="#008000"><b>60%</b><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%9D%9A%E5%A3%B3" title="青熊兽的坚壳">青熊兽的坚壳</a> <font color="#008000"><b>20%</b><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E6%AF%9B" title="青熊兽的毛">青熊兽的毛</a> <font color="#008000"><b>10%</b><br><a href="/%E5%9D%9A%E5%9B%BA%E4%B9%8B%E9%AA%A8" title="坚固之骨">坚固之骨</a> <font color="#008000"><b>10%</b> </font></font></font></font></td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> 1 </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>75%</b><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E6%AF%9B" title="青熊兽的毛">青熊兽的毛</a> <font color="#008000"><b>25%</b> </font></font></td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>38%</b><br><a href="/%E5%A4%A7%E9%A2%97%E7%89%99%E5%85%BD%E6%B3%AA" title="大颗牙兽泪">大颗牙兽泪</a> <font color="#008000"><b>37%</b><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="青熊兽的刚毛">青熊兽的刚毛</a> <font color="#008000"><b>25%</b> </font></font></font></td><td> 陷阱成功时、破爪时可能掉落
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落（蜂蜜） </td><td> 1 </td><td> <a href="/%E8%9C%82%E8%9C%9C" title="蜂蜜">蜂蜜</a> <font color="#008000"><b>100%</b> </font></td><td> <a href="/%E8%9C%82%E8%9C%9C" title="蜂蜜">蜂蜜</a> <font color="#008000"><b>100%</b> </font></td><td> 吃蜂蜜时被打翻
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落（鱼） </td><td> 1 </td><td> <a href="/%E5%88%BA%E8%BA%AB%E9%B1%BC" title="刺身鱼">刺身鱼</a> <font color="#008000"><b>85%</b><br><a href="/%E9%BB%84%E9%87%91%E9%B1%BC" title="黄金鱼">黄金鱼</a> <font color="#008000"><b>15%</b> </font></font></td><td> <a href="/%E8%9C%82%E8%9C%9C" title="蜂蜜">蜂蜜</a> <font color="#008000"><b>100%</b><br><a href="/%E5%88%BA%E8%BA%AB%E9%B1%BC" title="刺身鱼">刺身鱼</a> <font color="#008000"><b>80%</b><br><a href="/%E9%BB%84%E9%87%91%E9%B1%BC" title="黄金鱼">黄金鱼</a> <font color="#008000"><b>20%</b> </font></font></font></td><td> 吃鱼时被打翻
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> <a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E8%85%95%E7%94%B2" title="青熊兽的腕甲">青熊兽的腕甲</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E6%AF%9B" title="青熊兽的毛">青熊兽的毛</a>*1 <font color="#008000"><b>30%</b></font> </td><td> <a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E8%85%95%E7%94%B2" title="青熊兽的腕甲">青熊兽的腕甲</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%9D%9A%E8%85%95%E7%94%B2" title="青熊兽的坚腕甲">青熊兽的坚腕甲</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="青熊兽的刚毛">青熊兽的刚毛</a>*1 <font color="#008000"><b>25%</b></font> </td><td> 完全破爪
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E7%94%B2%E5%A3%B3" title="青熊兽的甲壳">青熊兽的甲壳</a>*1 <font color="#008000"><b>45%</b></font><br><a href="/%E5%A4%A7%E9%AA%A8" title="大骨">大骨</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E8%85%95%E7%94%B2" title="青熊兽的腕甲">青熊兽的腕甲</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E6%AF%9B" title="青熊兽的毛">青熊兽的毛</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%9D%9A%E5%A3%B3" title="青熊兽的坚壳">青熊兽的坚壳</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%9D%9A%E8%85%95%E7%94%B2" title="青熊兽的坚腕甲">青熊兽的坚腕甲</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E5%9D%9A%E5%9B%BA%E4%B9%8B%E9%AA%A8" title="坚固之骨">坚固之骨</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9D%92%E7%86%8A%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="青熊兽的刚毛">青熊兽的刚毛</a>*1 <font color="#008000"><b>10%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>

## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2">
<p>体型
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>基础
</p><p>体力
</p>
</td><td style="text-align: center">
<p>攻击力
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>全体
</p><p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td style="text-align: center" rowspan="10"> 村长
</td><td style="text-align: center" rowspan="10"> 下位
</td><td style="text-align: center"> ★1
</td><td> 恐怖的预兆
</td><td style="text-align: center"> 缴纳5个 <a href="/index.php?title=%E7%9A%87%E5%AE%B6%E8%9C%82%E8%9C%9C&amp;action=edit&amp;redlink=1" class="new" title="皇家蜂蜜（尚未撰写）">皇家蜂蜜</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td style="text-align: center" rowspan="4"> ★2
</td><td> 青熊兽
</td><td style="text-align: center">讨伐一头 青熊兽
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td> 某天，太阳底下的森林
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_5">
<td> 洗澡的牙兽
</td><td style="text-align: center">讨伐一头青熊兽
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_6">
<td> 毒之毒狗龙
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_7">
<td style="text-align: center" rowspan="2"> ★3
</td><td> <font color="#f40"> 跳舞的彩鸟 </font>
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_8">
<td> 孤岛的海贼团
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_9">
<td style="text-align: center" rowspan="2"> ★4
</td><td> <font color="#f40"> 狩猎彩鸟库鲁佩科！ </font>
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1120
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_10">
<td> 孤島の相撲大会
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900<br>900
</td><td style="text-align: right"> 110<br>110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_11">
<td style="text-align: center"> ★5
</td><td> 孤島の来訪者
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1800
</td><td style="text-align: right"> 125
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_12">
<td style="text-align: center" rowspan="21"> 集会浴场
</td><td style="text-align: center" rowspan="3"> 初心者
</td><td style="text-align: center" rowspan="2"> ★1
</td><td> 青熊兽的入侵
</td><td style="text-align: center">讨伐一头 青熊兽
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1000
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_13">
<td> ハチミツ集めのお手伝い
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1000
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_14">
<td style="text-align: center"> ★2
</td><td> 孤岛的海贼团
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1200
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_15">
<td style="text-align: center" rowspan="7"> 下位
</td><td style="text-align: center" rowspan="4"> ★3
</td><td> 夢のキノコジュース
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_16">
<td> 黄金の贈り物
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_17">
<td> <font color="#f40"> 狩猎彩鸟库鲁佩科！ </font>
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_18">
<td> 为了主人的孤岛
</td><td style="text-align: center">讨伐一头青熊兽
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_19">
<td style="text-align: center"> ★4
</td><td> <font color="#f40"> 溪流的吵闹小子 </font>
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2400
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_20">
<td style="text-align: center" rowspan="2"> ★5
</td><td> 森に棲むもの
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 150
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_21">
<td> 强豪集结
</td><td style="text-align: center">全部大型怪物狩猎
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 2400
</td><td style="text-align: right"> 150
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_22">
<td style="text-align: center" rowspan="9"> 上位
</td><td style="text-align: center" rowspan="5"> ★6
</td><td> 渓流の採取ツアー
</td><td style="text-align: center"> <font color="#0F0">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 4000
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_23">
<td> アオアシラが大変です×２
</td><td style="text-align: center">
</td><td style="text-align: right"> 92
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2800<br>2000
</td><td style="text-align: right"> 270<br>250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_24">
<td> 洗澡的牙兽
</td><td style="text-align: center">讨伐一头青熊兽
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 4000
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_25">
<td> 强袭的孤岛水流！
</td><td style="text-align: center">讨伐一头<a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2000
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_26">
<td> <font color="#f40"> 狩猎彩鸟库鲁佩科！ </font>
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 4000
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_27">
<td style="text-align: center" rowspan="2"> ★7
</td><td> 狩猎空之王者吧！
</td><td style="text-align: center">讨伐一头<a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2200
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_28">
<td> 集合！孤岛连续大作战
</td><td style="text-align: center"><a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>、青熊兽、<a href="/%E7%BB%BF%E8%BF%85%E9%BE%99" title="绿迅龙">绿迅龙</a>连续讨伐
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2000<br>2000
</td><td style="text-align: right"> 290<br>290
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_29">
<td style="text-align: center" rowspan="2"> ★8
</td><td> 其动如山
</td><td style="text-align: center">讨伐一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2200
</td><td style="text-align: right"> 300
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_30">
<td> 势力范围内禁止进入
</td><td style="text-align: center">讨伐一头<a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99" title="雷狼龙">雷狼龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 300
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_31">
<td style="text-align: center" rowspan="2"> 配信
</td><td style="text-align: center"> ★4
</td><td> 名探偵コナン・連続狩猟事件！
</td><td style="text-align: center">
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 2800
</td><td style="text-align: right"> 150
</td><td style="text-align: right"> 85
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_32">
<td style="text-align: center"> ★8
</td><td> 巨大熊、山中を破壊す
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 140 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 140 </font></b>
</td><td style="text-align: right"> 5000
</td><td style="text-align: right"> 400
</td><td style="text-align: right"> 70
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_33">
<td style="text-align: center" rowspan="2"> 番台
</td><td style="text-align: center"> 温泉
</td><td style="text-align: center"> ★5
</td><td> 腾龙的秘泉
</td><td style="text-align: center">青熊兽、<a href="/%E9%9B%8C%E7%81%AB%E9%BE%99" title="雌火龙">雌火龙</a>、<a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99" title="雷狼龙">雷狼龙</a>连续讨伐
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1520
</td><td style="text-align: right"> 135
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_34">
<td style="text-align: center"> 饮料
</td><td style="text-align: center"> ★3
</td><td> 挑战青熊兽
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_35">
<td style="text-align: center" rowspan="2"> 訓練所
</td><td style="text-align: center"> 集団
</td><td style="text-align: center"> ★1
</td><td> アオアシラ討伐演習
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 2160
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_36">
<td style="text-align: center"> 挑战
</td><td style="text-align: center"> ★4
</td><td> 练习任务02?
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 1200
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 65
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 熊掌挥击 </td><td> 侧身挥一下熊掌，能追加一下
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续挥击 </td><td> 锁定猎人小步向前进行左右掌交替的挥击，平常挥3下，怒时4下，疲劳时2下
</td></tr>
<tr style="background:#f9f9f9;">
<td> 双掌挥击 </td><td> 张开两掌后向前愤力挥击一下
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 伏下身子进行突进
</td></tr>
<tr style="background:#f9f9f9;">
<td> 扑 </td><td> 伏下身子向前跃出进行扑击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食攻击 </td><td> 与扑类似，距离较短，扑中后摇动猎人后扔出
</td></tr>
<tr style="background:#f9f9f9;">
<td> PP撞击 </td><td> （上位限定）用PP朝背后的猎人嘭地顶一下！
</td></tr></tbody></table>

# 白兔獸
![](assets/白兔獸.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>头<br>臀部</center></div>
</td><td><div><center>头<br>臀部</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 40 </td><td> 0 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 100 </td><td> 130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 上半身 </td><td> 44 </td><td> 44 </td><td> 40 </td><td> 30 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 30 </td><td> 30 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下半身 </td><td> 35 </td><td> 35 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 臀部 </td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">50 </font></td><td> 35 </td><td> 20 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 150
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 100 </td><td> 200 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 60 (300) </td><td> 200 (500) </td><td> 150 (600) </td><td> 150 (550) </td><td> 150 (500)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 60秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回20秒（疲劳25秒） → 2回10秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回15秒（疲劳20秒） → 2回10秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒　（疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> △ </td><td> 令其暂时原地眩晕，约3秒束缚，发怒时无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E6%AF%9B" title="白兔兽的毛">白兔兽的毛</a> <font color="#008000"><b>50%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E8%85%B9%E7%94%B2" title="白兔兽的腹甲">白兔兽的腹甲</a> <font color="#008000"><b>30%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%86%B0%E7%88%AA" title="白兔兽的冰爪">白兔兽的冰爪</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E9%AA%A8" title="大骨">大骨</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="白兔兽的刚毛">白兔兽的刚毛</a> <font color="#008000"><b>45%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%9D%9A%E8%85%B9%E7%94%B2" title="白兔兽的坚腹甲">白兔兽的坚腹甲</a> <font color="#008000"><b>28%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%86%BB%E7%88%AA" title="白兔兽的冻爪">白兔兽的冻爪</a> <font color="#008000"><b>12%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E6%AF%9B" title="白兔兽的毛">白兔兽的毛</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%9D%9A%E5%9B%BA%E4%B9%8B%E9%AA%A8" title="坚固之骨">坚固之骨</a> <font color="#008000"><b>5%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> 1 </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E6%AF%9B" title="白兔兽的毛">白兔兽的毛</a> <font color="#008000"><b>15%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E8%85%B9%E7%94%B2" title="白兔兽的腹甲">白兔兽的腹甲</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E7%89%99%E5%85%BD%E6%B3%AA" title="大颗牙兽泪">大颗牙兽泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="白兔兽的刚毛">白兔兽的刚毛</a> <font color="#008000"><b>25%</b></font> </td><td> 耳朵被破坏时
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头（耳） </td><td> <a href="/%E7%99%BD%E5%85%94%E5%85%BD%E4%B9%8B%E8%80%B3" title="白兔兽之耳">白兔兽之耳</a>*1 <font color="#008000"><b>75%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E6%AF%9B" title="白兔兽的毛">白兔兽的毛</a>*1 <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%A4%A7%E8%80%B3" title="白兔兽的大耳">白兔兽的大耳</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="白兔兽的刚毛">白兔兽的刚毛</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E4%B9%8B%E8%80%B3" title="白兔兽之耳">白兔兽之耳</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 完全破耳朵后必定奖励
</td></tr></tbody></table>

## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E8%85%B9%E7%94%B2" title="白兔兽的腹甲">白兔兽的腹甲</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%86%B0%E7%88%AA" title="白兔兽的冰爪">白兔兽的冰爪</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E5%A4%A7%E9%AA%A8" title="大骨">大骨</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E6%AF%9B" title="白兔兽的毛">白兔兽的毛</a>*2 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%9D%9A%E8%85%B9%E7%94%B2" title="白兔兽的坚腹甲">白兔兽的坚腹甲</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%86%BB%E7%88%AA" title="白兔兽的冻爪">白兔兽的冻爪</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%9D%9A%E5%9B%BA%E4%B9%8B%E9%AA%A8" title="坚固之骨">坚固之骨</a>*2 <font color="#008000"><b>17%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%88%9A%E6%AF%9B" title="白兔兽的刚毛">白兔兽的刚毛</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E8%85%B9%E7%94%B2" title="白兔兽的腹甲">白兔兽的腹甲</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E7%99%BD%E5%85%94%E5%85%BD%E7%9A%84%E5%86%B0%E7%88%AA" title="白兔兽的冰爪">白兔兽的冰爪</a>*2 <font color="#008000"><b>5%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%B8%A9%E6%B3%89%E4%BB%BB%E5%8A%A1" title="温泉任务">温泉任务</a>
</p>
</td><td>
<p>白兎獣と大猪
</p>
</td><td>
<p>讨伐一头白兔兽和一头<a href="/%E9%87%8E%E7%8C%AA%E7%8E%8B" title="野猪王">野猪王</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>白銀に抱かれて
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D2%E6%98%9F" title="集会所下位2星">集会所下位2星</a>
</p>
</td><td>
<p>雪のちウルクスス
</p>
</td><td>
<p>讨伐一头白兔兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>雪のちウルクスス
</p>
</td><td>
<p>讨伐一头白兔兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>青と白の挽歌
</p>
</td><td>
<p>讨伐一头<a href="/%E7%9C%A0%E7%8B%97%E9%BE%99" title="眠狗龙">眠狗龙</a>和一头白兔兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>暗闇にうごめく猛毒！
</p>
</td><td>
<p>讨伐一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>地獄の大雪合戦
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>凍土に轟く咆哮
</p>
</td><td>
<p>讨伐一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>
</p>
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 通常滑行 </td><td> 直线滑行攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> U型滑行 </td><td> 连续3次直线滑行，呈U字型
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大回旋 </td><td> 360°的大回旋滑行，范围较大
</td></tr>
<tr style="background:#f9f9f9;">
<td> 折返滑行 </td><td> 直线滑行后折返再接一次滑行
</td></tr>
<tr style="background:#f9f9f9;">
<td> 投冰块 </td><td> 扔出冰块，最多可以扔2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 挥击 </td><td> 侧身使用前爪对150°范围使用挥击，可以连续2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 伏下身子向前飞扑攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> PP攻击 </td><td> 使用PP顶后面的猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续挥击 </td><td> 小步朝猎人前进并左右挥击，怒后挥4次，怒前3次
</td></tr></tbody></table>

# 赤甲獸
![](assets/赤甲獸.jpeg)

## 基本抗性
◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>头<br>上半身<br>上背甲<br>滚动中</center></div>
</td><td><div><center>头<br>上半身<br>上背甲<br>滚动中</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">50 </font></td><td> 0 </td><td> 30 </td><td> 15 </td><td> 20 </td><td> 0 </td><td> 100 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 上半身 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 35 </td><td> 0 </td><td> 20 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 上背甲 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 40 </td><td> 0 </td><td> 20 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 40 </td><td> 30 </td><td> 30 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下半身 </td><td> 36 </td><td> 36 </td><td> 20 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 臀部 </td><td> 30 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 15 </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 110
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滚动中 </td><td> <font color="#ff0000">80 </font></td><td> <font color="#ff0000">80 </font></td><td> 10 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 10
</td></tr></tbody></table>

## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 200 </td><td> 200 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 60 (300) </td><td> 100 (500) </td><td> 100 (400) </td><td> 100 (400) </td><td> 100 (400)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 60秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>

## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回20秒（疲劳25秒） → 2回10秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回15秒（疲劳20秒） → 2回10秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★4（容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>

## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%94%B2%E5%A3%B3" title="赤甲兽的甲壳">赤甲兽的甲壳</a> <font color="#008000"><b>35%</b></font><br><a href="/%E9%BA%BB%E7%97%B9%E8%A2%8B" title="麻痹袋">麻痹袋</a> <font color="#008000"><b>35%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%88%AA" title="赤甲兽的爪">赤甲兽的爪</a> <font color="#008000"><b>20%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E8%9B%87%E8%85%B9%E7%94%B2" title="赤甲兽的蛇腹甲">赤甲兽的蛇腹甲</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E5%9D%9A%E5%A3%B3" title="赤甲兽的坚壳">赤甲兽的坚壳</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%BC%BA%E5%8A%9B%E9%BA%BB%E7%97%B9%E8%A2%8B" title="强力麻痹袋">强力麻痹袋</a> <font color="#008000"><b>30%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E5%B0%96%E7%88%AA" title="赤甲兽的尖爪">赤甲兽的尖爪</a> <font color="#008000"><b>20%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E8%9B%87%E8%85%B9%E7%94%B2" title="赤甲兽的蛇腹甲">赤甲兽的蛇腹甲</a> <font color="#008000"><b>12%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%88%AA" title="赤甲兽的爪">赤甲兽的爪</a> <font color="#008000"><b>8%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> 1 </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E9%BA%BB%E7%97%B9%E8%A2%8B" title="麻痹袋">麻痹袋</a> <font color="#008000"><b>15%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E8%9B%87%E8%85%B9%E7%94%B2" title="赤甲兽的蛇腹甲">赤甲兽的蛇腹甲</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E7%89%99%E5%85%BD%E4%B9%8B%E6%B3%AA" title="牙兽之泪">牙兽之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E7%89%99%E5%85%BD%E6%B3%AA" title="大颗牙兽泪">大颗牙兽泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%BC%BA%E5%8A%9B%E9%BA%BB%E7%97%B9%E8%A2%8B" title="强力麻痹袋">强力麻痹袋</a> <font color="#008000"><b>15%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E8%9B%87%E8%85%B9%E7%94%B2" title="赤甲兽的蛇腹甲">赤甲兽的蛇腹甲</a> <font color="#008000"><b>10%</b></font> </td><td> 背甲被破坏时掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背甲 </td><td> <a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E8%9B%87%E8%85%B9%E7%94%B2" title="赤甲兽的蛇腹甲">赤甲兽的蛇腹甲</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%94%B2%E5%A3%B3" title="赤甲兽的甲壳">赤甲兽的甲壳</a>*1 <font color="#008000"><b>40%</b></font> </td><td> <a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E8%9B%87%E8%85%B9%E7%94%B2" title="赤甲兽的蛇腹甲">赤甲兽的蛇腹甲</a>*2 <font color="#008000"><b>52%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E5%9D%9A%E5%A3%B3" title="赤甲兽的坚壳">赤甲兽的坚壳</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%94%B2%E5%A3%B3" title="赤甲兽的甲壳">赤甲兽的甲壳</a>*2 <font color="#008000"><b>8%</b></font> </td><td> 破坏背部甲壳，战斗胜利后必定奖励
</td></tr></tbody></table>

## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%94%B2%E5%A3%B3" title="赤甲兽的甲壳">赤甲兽的甲壳</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E7%88%AA" title="赤甲兽的爪">赤甲兽的爪</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E9%BA%BB%E7%97%B9%E8%A2%8B" title="麻痹袋">麻痹袋</a>*1 <font color="#008000"><b>40%</b></font> </td><td> <a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E5%9D%9A%E5%A3%B3" title="赤甲兽的坚壳">赤甲兽的坚壳</a>*1 <font color="#008000"><b>42%</b></font><br><a href="/%E5%BC%BA%E5%8A%9B%E9%BA%BB%E7%97%B9%E8%A2%8B" title="强力麻痹袋">强力麻痹袋</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E8%B5%A4%E7%94%B2%E5%85%BD%E7%9A%84%E5%B0%96%E7%88%AA" title="赤甲兽的尖爪">赤甲兽的尖爪</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E9%BA%BB%E7%97%B9%E8%A2%8B" title="麻痹袋">麻痹袋</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>

## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>魔球！火山のナックルボール
</p>
</td><td>
<p>讨伐两头赤甲兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>赤甲激震！潜口猛進！
</p>
</td><td>
<p>讨伐一头赤甲兽和一头<a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99" title="潜口龙">潜口龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D2%E6%98%9F" title="集会所下位2星">集会所下位2星</a>
</p>
</td><td>
<p>落石注意！？
</p>
</td><td>
<p>讨伐一头赤甲兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D2%E6%98%9F" title="集会所下位2星">集会所下位2星</a>
</p>
</td><td>
<p>砂原を泳ぐ珍味！？
</p>
</td><td>
<p>缴纳5个<a href="/%E6%80%AA%E7%89%A9%E7%9A%84%E8%82%9D" title="怪物的肝">怪物的肝</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>落石注意！？
</p>
</td><td>
<p>讨伐一头赤甲兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>赤甲獣ラングロトラ現る！
</p>
</td><td>
<p>讨伐一头赤甲兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>集いし強豪
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>落石注意！？
</p>
</td><td>
<p>讨伐一头赤甲兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>仄暗い火口の中から
</p>
</td><td>
<p>讨伐一头<a href="/%E9%92%A2%E9%94%A4%E9%BE%99" title="钢锤龙">钢锤龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>赤き麻痺、赤き毒
</p>
</td><td>
<p>讨伐一头赤甲兽和一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>今そこにある恐怖
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>砂原の角竜を狩れ！
</p>
</td><td>
<p>讨伐一头<a href="/%E8%A7%92%E9%BE%99" title="角龙">角龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%B8%A9%E6%B3%89%E4%BB%BB%E5%8A%A1" title="温泉任务">温泉任务</a>
</p>
</td><td>
<p>湯けむりと噴煙と
</p>
</td><td>
<p>讨伐一头赤甲兽和一头<a href="/%E6%AF%92%E7%8B%97%E9%BE%99" title="毒狗龙">毒狗龙</a>
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b></td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 长舌伸缩攻击 </td><td> 伸出很恶心的舌头，左右甩，被击中猎人会被强制向赤甲兽靠近
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滚球攻击 </td><td> 身体抱成一团，直线攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滚球三连压 </td><td> 身体抱成一团，进行Z字型上跳下压攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滚球巨压 </td><td> 身体抱成一团，从高空向下压击，攻击完后有大硬直
</td></tr>
<tr style="background:#f9f9f9;">
<td> 臭气攻击 </td><td> 从身体喷出带有恶臭的气体进行360°攻击，攻击附带 <a href="/%E6%81%B6%E8%87%AD" title="恶臭">恶臭</a> 效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 麻痹球 </td><td> 吐出一个带有 <a href="/%E9%BA%BB%E7%97%B9" title="麻痹">麻痹</a> 效果的电球攻击猎人，被击中后猎人会进入 <a href="/%E9%BA%BB%E7%97%B9" title="麻痹">麻痹</a> 状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 挥击 </td><td> 侧身在150°范围使用挥击，可以挥两次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续挥击 </td><td> 使用左右前爪交替挥击，怒时挥4次，怒前3次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 双爪挥击 </td><td> 张开双爪集中向前使用一次挥击
</td></tr></tbody></table>