# 開口說話工具箱

學習英文最好的方式就是使用英文，有一些簡單的萬能短語可以在單詞量匱乏時就開始與人交流，應該先學習這些短語，以便能快速開始使用英文

# 使用英文問關於英語的問題

How do you say this?  
這個怎麼說?

What is this called?  
這個叫什麼?

Please say that agen.  
請再說1次

Please speak more slowly.  
請說慢一點

I don't understand.  
我不明白


# 最常用的詞

常用詞:
* **Please**  請 (英文交流比較文雅，到處都會用到這個詞)
* **Thank You**  謝謝你 (英文交流比較文雅，到處都會用到這個詞)
* **Yes**  是 (表示確定，同意)
* **No**  否/不 (表示 否定/反對)


代詞:
* **I ME** 我
* **You** 你
* **He Him** 他 
* **She her** 她
* **They Them** 他她們
* **It** 它

動詞:
* **Give** 給
* **Want** 想
* **Is Are** 是

位置:
* **This** 這個
* **That** 那個
* **Here** 這裏
* **There** 那裏 


W:
* **What** 什麼
* **Where** 哪裏
* **Who** 誰
* **When** 什麼時候
* **Why** 爲什麼
