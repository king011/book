# 運行

1. 創建一個 自定義網路 private_intranet 以便 wordpress 和 mysql 通信

	```
	docker network create private_intranet
	```

	```
	docker network ls | egrep private_intranet
	```
	
2. wordpress 使用 mysql 存儲數據 故需要先準備一個 mysql 兼容的 數據庫 爲了方便通信 指定網路 使用 private_intranet 網路別名爲 wordpress-db

	```
	docker run \
			--name wordpress-db \
			--network private_intranet \
			--network-alias wordpress-db \
			--restart always \
			-v your_db_save_path:/var/lib/mysql \
			-e MYSQL_ROOT_PASSWORD=123 \
			-d mariadb:10.5.9
	```
	
3. 連接 mysql 創建一個 數據庫供 wordpress 使用

	```
	create database wordpress;
	```
	
4. 創建 wordpress 容器 並指定 數據庫地址

	```
	docker run \
			--name wordpress \
			--network private_intranet \
			--restart always \
			-e WORDPRESS_DB_HOST=wordpress-db \
			-e WORDPRESS_DB_USER=root \
			-e WORDPRESS_DB_PASSWORD=123 \
			-e WORDPRESS_DB_NAME=wordpress \
			-p 8080:80 \
			-d wordpress:5.7
	```