# eventloop

eventloop 提供了一個 簡陋的 事件循環 同時 提供了 定時器 函數 同樣太過簡陋 目前不建議使用


```
package main

import (
	"fmt"
	"time"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/eventloop"
	"github.com/dop251/goja_nodejs/require"
)

// eventloop 提供的事件循環默認只能和 它提供的 setTimeout/setInterval/setImmediate 工作，
// 因爲它沒有提供修改事件計數的方法，故如果沒有 setTimeout/setInterval/setImmediate 事件，
// 事件循環不會等待 Promise 完成就會直接結束，目前解決方案是自己提供一個 事件計數並與 setInterval 進行綁定
type Job struct {
	loop  *eventloop.EventLoop
	flag  *eventloop.Interval
	count uint64
}

func (j *Job) Add() {
	j.count++
	if j.count == 1 {
		j.flag = j.loop.SetInterval(func(r *goja.Runtime) {}, time.Hour*24*365*100)
	}
}
func (j *Job) Done() {
	j.count--
	if j.count == 0 {
		j.loop.ClearInterval(j.flag)
		j.flag = nil
	}
}
func main() {
	// 創建模塊註冊表
	registry := require.NewRegistry()
	// 創建事件循環
	loop := eventloop.NewEventLoop(
		eventloop.WithRegistry(registry), // 指定模塊註冊表
	)

	// 運行事件處理器直到所有事件處理完成
	// 這個只能執行一次
	loop.Run(func(vm *goja.Runtime) {
		vm.Set(`println`, func(args ...any) {
			fmt.Println(args...)
		})
		var job = Job{loop: loop}
		vm.Set(`sleep`, func(call goja.FunctionCall) goja.Value {
			duration := time.Millisecond * time.Duration(call.Argument(0).ToInteger())
			promise, resolve, _ := vm.NewPromise()

			// 增加一個等待事件
			job.Add()
			// 異步方法
			go func(duration time.Duration) {
				time.Sleep(duration)
				// 需要使用 RunOnLoop 將函數投遞到事件 goroutine 中 resolve/reject
				loop.RunOnLoop(func(r *goja.Runtime) {
					// 減少一個等待事件
					job.Done()

					// 完成異步方法
					resolve(time.Now())
				})
			}(duration)
			// 返回 promise
			return vm.ToValue(promise)
		})

		// 腳本入口
		vm.RunScript("main.js", `
println(1)
sleep(1000).then((now)=>{
	println(now)
})
println(2)
`)
	})
}
```