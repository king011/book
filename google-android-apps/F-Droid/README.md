# F-Droid

F-Droid 是一個開源(AGPLv3+)的 andord 商店，它的特色是不需要用戶登入並且上面只收錄了開源軟體。

很多沒有廣告的優秀開源軟體都被淹沒在 play store 中商業軟件的垃圾海中無法被發現，你或許可以從 F-Droid 中更容易找到稱心的應用

* 官網 [https://f-droid.org/](https://f-droid.org/)
* 源碼 [https://gitlab.com/fdroid/](https://gitlab.com/fdroid/)
* 安裝 [https://f-droid.org/F-Droid.apk](https://f-droid.org/F-Droid.apk)