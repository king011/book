# [http\_connection\_manager](https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#extension-envoy-filters-network-http-connection-manager)

```typescript
interface HttpConnectionManager {
	/**
	 * 提供連接器的編碼類型
	 * 
	 * @defaultValue 'AUTO'
	 */
	codec_type?: 'AUTO' | 'HTTP1' | 'HTTP2'
	/**
	 * 統計信息的人類可讀前綴
	 */
	stat_prefix: string
	/**
	 * 連接管理器的路由表將通過 RDS API 動態加載。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-rds extensions.filters.network.http_connection_manager.v3.Rds}
	 */
	rds?: Rds
	/**
	 * 連接管理器的路由表是靜態的，並在此屬性中指定
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route.proto#envoy-v3-api-msg-config-route-v3-routeconfiguration config.route.v3.RouteConfiguration}
	 */
	route_config?: config.route.v3.RouteConfiguration
	/**
	 * 路由表將根據請求屬性（例如，標頭的值）動態分配給每個請求
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-scopedroutes extensions.filters.network.http_connection_manager.v3.ScopedRoutes}
	 */
	scoped_routes?: ScopedRoutes
	/**
	 * 組成對連接管理器的請求的過濾器鏈的單個 HTTP 過濾器的列表。 順序很重要，因為過濾器是在請求事件發生時按順序處理的
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-httpfilter extensions.filters.network.http_connection_manager.v3.HttpFilter}
	 */
	http_filters?: Array<HttpFilter>
	/**
	 * 連接管理器是否操縱 :path 和 x-envoy-downstream-service-cluster 標頭
	 */
	add_user_agent?: boolean
	/**
	 * 對象的存在定義連接管理器是否向配置的跟踪提供程序發出跟踪數據。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-tracing extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.Tracing}
	 */
	tracing?: any
	/**
	 * 連接管理器處理的 HTTP 請求的附加設置。 這些將適用於 HTTP1 和 HTTP2 請求。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/protocol.proto#envoy-v3-api-msg-config-core-v3-httpprotocoloptions config.core.v3.HttpProtocolOptions}
	 */
	common_http_protocol_options?: config.core.v3.HttpProtocolOptions
	/**
	 * 傳遞給 HTTP/1 編解碼器的其他 HTTP/1 設置
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/protocol.proto#envoy-v3-api-msg-config-core-v3-http1protocoloptions config.core.v3.Http1ProtocolOptions}
	 */
	http_protocol_options?: config.core.v3.Http1ProtocolOptions
	/**
	 * 傳遞給 HTTP/2 編解碼器的其他 HTTP/2 設置。
	 * 
	 * @example 對於不信任的下游應該設置如下字段
	 * 
	 * ```
	 * http2_protocol_options: {
	 *   initial_connection_window_size: 1048576,
	 *   initial_stream_window_size: 65536
	 *   max_concurrent_streams: 100
	 * }
	 * ```
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/protocol.proto#envoy-v3-api-msg-config-core-v3-http2protocoloptions config.core.v3.Http2ProtocolOptions}
	 */
	http2_protocol_options?: config.core.v3.Http2ProtocolOptions
	/**
	 * 連接管理器將在響應中寫入服務器標頭的可選覆蓋
	 * @defaultValue 'envoy'
	 */
	server_name?: string
	/**
	 * 定義要應用於響應路徑上的服務器標頭的操作。 
	 * 默認情況下，Envoy 將使用 server_name 中指定的值覆蓋標頭
	 * 
	 * @defaultValue 'OVERWRITE'
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-enum-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-serverheadertransformation extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.ServerHeaderTransformation}
	 */
	server_header_transformation?: 'OVERWRITE' | 'APPEND_IF_ABSENT' | 'PASS_THROUGH'
	/**
	 * 允許顯式轉換請求路徑上的 :scheme 標頭。 如果未設置，則應用 Envoy 的默認方案處理。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/protocol.proto#envoy-v3-api-msg-config-core-v3-schemeheadertransformation config.core.v3.SchemeHeaderTransformation}
	 */
	scheme_header_transformation?: config.core.v3.SchemeHeaderTransformation
	/**
	 * uint32 傳入連接的最大請求標頭大小。 
	 * 如果未配置，則允許的默認最大請求標頭為 60 KiB。 
	 * 超過此限制的請求將收到 431 響應。
	 */
	max_request_headers_kb?: number
	/**
	 * 連接管理器管理的連接的流空閒超時
	 * @defaultValue "300s"
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	stream_idle_timeout?: string
	/**
	 * Envoy 等待接收整個請求的時間量。 
	 * 計時器在請求發起時激活，並在請求的最後一個字節向上游發送（即所有解碼過濾器已處理請求）或響應發起時解除。 
	 * 如果未指定或設置為 0，則禁用此超時。
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	request_timeout?: string
	/**
	 * Envoy 等待接收請求標頭的時間量。 
	 * 計時器在接收到報頭的第一個字節時激活，並在接收到報頭的最後一個字節時解除。 
	 * 如果未指定或設置為 0，則禁用此超時。
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	request_headers_timeout?: string
	/**
	 * 在發送 HTTP/2“關閉通知”（具有最大流 ID 的 GOAWAY 幀）和最終 GOAWAY 幀之間等待的時間
	 * @defaultValue "5s"
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	drain_timeout?: string
	/**
	 * 延遲關閉超時適用於由 HTTP 連接管理器管理的下游連接
	 * @defaultValue "1s"
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	delayed_close_timeout?: string
	/**
	 * 日誌
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#envoy-v3-api-msg-config-accesslog-v3-accesslog config.accesslog.v3.AccessLog}
	 */
	access_log?: Array<config.accesslog.v3.AccessLog>
	/**
	 * 刷新上述訪問日誌的時間間隔。
	 * 
	 * @remarks
	 *  默認情況下，當 HTTP 請求完成時，HCM 將在流關閉時恰好刷新一個訪問日誌。
	 *  如果設置了該字段，HCM 將以指定的時間間隔定期刷新訪問日誌。 
	 * 這在長期請求的情況下特別有用，例如 CONNECT 和 Websockets。 
	 * 可以通過訪問日誌過濾器中 StreamInfo 的 requestComplete() 方法或通過 %DURATION% 替換字符串檢測最終訪問日誌。
	 *  間隔必須至少為 1 毫秒
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	access_log_flush_interval?: string
	/**
	 * 如果設置為 true，HCM 將在收到新的 HTTP 請求時刷新一次訪問日誌，在評估請求標頭之後，然後遍歷 HTTP 過濾器鏈。 
	 * 與上游集群相關的詳細信息，例如上游主機，將不適用於此日
	 */
	flush_access_log_on_new_request?: boolean
	/**
	 * 如果設置為 true，連接管理器將在確定內部與外部來源和操作各種標頭時使用客戶端連接的真實遠程地址。 
	 * 如果設置為 false 或不存在，連接管理器將使用 x-forwarded-for HTTP 標頭
	 */
	use_remote_address?: boolean
	/**
	 * uint32 在確定原始客戶端的 IP 地址時，要信任的來自 x-forwarded-for HTTP 標頭右側的額外入口代理躍點數。 
	 * 如果未指定此選項，則默認值為零
	 */
	xff_num_trusted_hops?: number
	/**
	 * 原始 IP 檢測擴展的配置。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig config.core.v3.TypedExtensionConfig}
	 */
	original_ip_detection_extensions?: Array<config.core.v3.TypedExtensionConfig>
	/**
	 * 早期標頭突變擴展的配置
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig config.core.v3.TypedExtensionConfig}
	 */
	early_header_mutation_extensions?: Array<config.core.v3.TypedExtensionConfig>
	/**
	 * 配置哪些網絡地址被認為是內部的，用於統計和標題衛生目的。 
	 * 如果未指定，則只有 RFC1918 IP 地址將被視為內部地址
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-internaladdressconfig extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.InternalAddressConfig}
	 */
	internal_address_config?: extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.InternalAddressConfig
	/**
	 * 如果設置，Envoy 將不會將遠程地址附加到 x-forwarded-for HTTP 標頭
	 */
	skip_xff_append?: boolean
	/**
	 * 通過標頭值附加到請求和響應標頭
	 */
	via?: string
	/**
	 * 連接管理器是否會生成 x-request-id 標頭。 
	 * 生成隨機 UUID4 的成本很高，因此在不需要此功能的高吞吐量場景中可以禁用它
	 * @defaultValue true
	 */
	generate_request_id?: boolean
	/**
	 * 如果傳遞給邊緣請求（邊緣請求是從外部客戶端到前端 Envoy 的請求），連接管理器是否會保留 x-request-id 標頭而不重置它，這是當前 Envoy 的行為
	 * @defaultValue false
	 */
	preserve_external_request_id?: boolean
	/**
	 * 如果設置，Envoy 將始終設置 x-request-id 標頭作為響應。 
	 * 如果這是 false 或未設置，則僅當使用 x-envoy-force-trace 標頭強制跟踪時，請求 ID 才會在響應中返回。
	 */
	always_set_request_id_in_response?: boolean
	/**
	 * 如何處理 x-forwarded-client-cert (XFCC) HTTP 標頭
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-enum-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-forwardclientcertdetails extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.ForwardClientCertDetails}
	 */
	forward_client_cert_details?: extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.ForwardClientCertDetails
	/**
	 * 此字段僅在 forward_client_cert_details 為 APPEND_FORWARD 或 SANITIZE_SET 且客戶端連接為 mTLS 時有效。
	 * 它指定要轉發的客戶端證書中的字段
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-setcurrentclientcertdetails extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.SetCurrentClientCertDetails}
	 */
	set_current_client_cert_details?: extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.SetCurrentClientCertDetails
	/**
	 * 如果 proxy_100_continue 為真，Envoy 將在上游代理傳入的“Expect: 100-continue”標頭，並向下游轉發“100 Continue”響應
	 */
	proxy_100_continue?: boolean
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-upgradeconfig extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.UpgradeConfig}
	 */
	upgrade_configs?: Array<extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.UpgradeConfig>
	/**
	 * 在通過 HTTP 過濾器或路由處理任何請求之前，是否應該根據 RFC 3986 對路徑進行規範化？ 這也會影響上游 :path 標頭
	 */
	normalize_path?: boolean
	/**
	 * 確定在 HTTP 過濾器或路由對請求進行任何處理之前，路徑中的相鄰斜杠是否合併為一個。 這也會影響上游 :path 標頭
	 */
	merge_slashes?: boolean
	/**
	 * 當請求 URL 路徑包含轉義的斜杠序列（%2F、%2f、%5C 和 %5c）時要採取的操作
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-enum-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-pathwithescapedslashesaction extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.PathWithEscapedSlashesAction}
	 */
	path_with_escaped_slashes_action?: 'IMPLEMENTATION_SPECIFIC_DEFAULT' | 'KEEP_UNCHANGED' | 'REJECT_REQUEST' | 'UNESCAPE_AND_REDIRECT' | 'UNESCAPE_AND_FORWARD'
	/**
	 * 請求 ID 擴展的配置
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-requestidextension extensions.filters.network.http_connection_manager.v3.RequestIDExtension}
	 */
	request_id_extension?: RequestIDExtension
	/**
	 * 自定義 Envoy 返回的本地回复的配置
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-localreplyconfig extensions.filters.network.http_connection_manager.v3.LocalReplyConfig}
	 */
	local_reply_config?: LocalReplyConfig
	/**
	 * 確定在 HTTP 過濾器或路由對請求進行任何處理之前是否應從主機/權限標頭中刪除端口部分
	 * @remarks
	 * 僅當該端口等於偵聽器的本地端口時，該端口才會被刪除。 
	 * 這會影響上游主機頭，除非方法是 CONNECT，在這種情況下，如果沒有過濾器添加端口，則在將頭髮送到上游之前，將恢復原始端口。 
	 * 如果不設置此選項，主機 example:443 的傳入請求將不會匹配域匹配設置為 example 的路由。 
	 *  請注意，端口刪除不是 HTTP 規範的一部分，只是為了方便起見。 
	 * 只能設置 strip_matching_host_port 或 strip_any_host_port 之一。
	 * 
	 * @defaultValue false
	 */
	strip_matching_host_port?: boolean
	/**
	 * 確定在 HTTP 過濾器或路由對請求進行任何處理之前是否應從主機/權限標頭中刪除端口部分
	 * @remarks
	 * 這會影響上游主機頭，除非方法是 CONNECT，在這種情況下，如果沒有過濾器添加端口，則在將頭髮送到上游之前，將恢復原始端口。
	 * 如果不設置此選項，主機 example:443 的傳入請求將不會匹配域匹配設置為 example 的路由。 
	 * 請注意，端口刪除不是 HTTP 規範的一部分，只是為了方便起見。 
	 * 只能設置 strip_matching_host_port 或 strip_any_host_port 之一。
	 * @defaultValue false
	 */
	strip_any_host_port?: boolean
	/**
	 * 當從下游接收到無效的 HTTP 時，管理 Envoy 的行為
	 */
	stream_error_on_invalid_http_message?: boolean
	/**
	 * 確定在 HTTP 過濾器或路由對請求進行任何處理之前是否應從主機/權限標頭中刪除主機的尾隨點
	 * @remarks
	 * 這會影響上游主機標頭。 如果不設置此選項，則使用主機 example.com. 傳入請求，不會匹配域匹配設置為 example.com 的路由。 
	 * 當傳入請求包含包含端口號的主機/權限標頭時，設置此選項將從主機部分刪除尾隨點（如果存在），而端口保持原樣（例如主機值 example.com.:443 將是 更新為 example.com:443）。
	 * @defaultValue false
	 */
	strip_trailing_host_dot?: boolean
	/**
	 * proxy-Status HTTP 響應頭配置。 如果設置了此配置，則會填充 Proxy-Status HTTP 響應標頭字段
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/http_connection_manager/v3/http_connection_manager.proto#envoy-v3-api-msg-extensions-filters-network-http-connection-manager-v3-httpconnectionmanager-proxystatusconfig extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.ProxyStatusConfig}
	 */
	proxy_status_config?: extensions.filters.network.http_connection_manager.v3.HttpConnectionManager.ProxyStatusConfig
	/**
	 * 將 x-forwarded-port 標頭附加到客戶端用於連接到 Envoy 的端口值。 
	 * 如果 x-forwarded-port 標頭已由 Envoy 前面的任何受信任代理設置，它將被忽略
	 */
	append_x_forwarded_port?: boolean
	/**
	 * HCM 是否將 ProxyProtocolFilterState 添加到連接生命週期過濾器狀態
	 * @defaultValue true
	 */
	add_proxy_protocol_connection_state?: boolean
}
```

## route_config

([config.route.v3.RouteConfiguration](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route.proto#envoy-v3-api-msg-config-route-v3-routeconfiguration)) 連接管理器的路由表是靜態的，並在此屬性中指定

```typescript
interface RouteConfiguration {
	/**
	 * 路由配置的名稱
	 */
	name?: string
	/**
	 * 虛擬主機列表
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-virtualhost config.route.v3.VirtualHost}
	 */
	virtual_hosts?: Array<VirtualHost>
	/**
	 * 一組虛擬主機將通過 VHDS API 動態加載
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route.proto#envoy-v3-api-msg-config-route-v3-vhds config.route.v3.Vhds}
	 */
	vhds?: Vhds
	/**
	 * 指定連接管理器將認為僅供內部使用的 HTTP 標頭列表。 
	 * 如果它們在外部請求中被發現，它們將在過濾器調用之前被清除
	 */
	internal_only_headers?: Array<string>
	/**
	 * 指定應添加到連接管理器編碼的每個響應的 HTTP 標頭列表
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-headervalueoption config.core.v3.HeaderValueOption}
	 */
	response_headers_to_add?: Array<config.core.v3.HeaderValueOption>
	/**
	 * 指定應從連接管理器編碼的每個響應中刪除的 HTTP 標頭列表
	 */
	response_headers_to_remove?: Array<string>
	/**
	 * 指定應添加到 HTTP 連接管理器路由的每個請求的 HTTP 標頭列表
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-headervalueoption config.core.v3.HeaderValueOption}
	 */
	request_headers_to_add?: Array<config.core.v3.HeaderValueOption>
	/**
	 * 指定應從 HTTP 連接管理器路由的每個請求中刪除的 HTTP 標頭列表
	 */
	request_headers_to_remove?: Array<string>
	/**
	 * 默認情況下，應添加/刪除的標頭從最具體到最不具體進行評估：
	 * - 路由級別
	 * - 虛擬主機級別
	 * - 連接管理器級別
	 * 
	 * 要允許在路由或虛擬主機級別設置覆蓋，可以通過將此選項設置為 true 來顛倒此順序
	 * 
	 * @defaultValue false
	 */
	most_specific_header_mutations_wins?: boolean
	/**
	 * 指定路由表引用的集群是否將由集群管理器驗證
	 * @remarks
	 * 如果設置為 true 且路由引用不存在的集群，則不會加載路由表。 
	 * 如果設置為 false 且路由引用不存在的集群，則路由表將加載，並且如果在運行時選擇路由，路由器過濾器將返回 404。 
	 * 如果路由表是通過 route_config 選項靜態定義的，則此設置默認為 true。 
	 * 如果通過 rds 選項動態加載路由表，則此設置默認為 false。 
	 * 用戶可能希望在某些情況下覆蓋默認行為（例如，當使用帶有靜態路由表的 CDS 時）
	 */
	validate_clusters?: boolean
	/**
	 * uint32 響應直接響應正文大小的最大字節數
	 * 
	 * @defaultValue 4096
	 */
	max_direct_response_body_size_bytes?: number
	/**
	 * 路由中的集群說明符插件名稱可能使用的插件列表及其配置。 
	 * 此列表中的所有 extension.name 字段必須是唯一的
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-clusterspecifierplugin config.route.v3.ClusterSpecifierPlugin}
	 */
	cluster_specifier_plugins?: Array<ClusterSpecifierPlugin>
	/**
	 * 指定一組默認的請求鏡像策略，適用於其虛擬主機下的所有路由
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction-requestmirrorpolicy config.route.v3.RouteAction.RequestMirrorPolicy}
	 */
	request_mirror_policies?: Array<RequestMirrorPolicy>
	/**
	 * 默認情況下， :authority 標頭中的端口（如果有）用於主機匹配。
	 * @remarks
	 * 啟用此選項後，Envoy 在選擇 VirtualHost 時將忽略 :authority 標頭中的端口號（如果有）。
	 * 注意：此選項不會去除路由配置 config.route.v3.VirtualHost.domains 字段中包含的端口號（如果有）。
	 */
	ignore_port_in_host_matching?: boolean
	/**
	 * 忽略路徑匹配中的路徑參數。 
	 * @remarks
	 * 在 RFC3986 之前，URI 類似於(RFC1808): <scheme>://<net_loc>/<path>;<params>?<query>#<fragment> Envoy 默認將 ':path' 作為 '<path>;<params>'。 
	 * 對於只想匹配 '<path>' 部分的用戶，此選項應該為 true
	 */
	ignore_path_parameters_in_path_matching?: boolean
	/**
	 * 用於提供每個過濾器配置的 RouteConfiguration 級別
	 */
	typed_per_filter_config?: Record<string, any>
}
```

### virtual_hosts

(repeated [config.route.v3.VirtualHost](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-virtualhost)) 虛擬主機列表

```
interface VirtualHost {
	/**
	 * 虛擬主機的邏輯名稱。 這在發出某些統計信息時使用，但與路由無關
	 */
	name: string
	/**
	 * 將與此虛擬主機匹配的域列表（主機/權限標頭）。
	 * 支持後綴或前綴形式的通配符主機
	 * @remarks
	 * 域搜索順序:
	 * - 準確的域名: www.foo.com
	 * - 後綴域通配符: *.foo.com 或 *-bar.foo.com
	 * - 前綴域通配符: foo.* 或 foo-*
	 * - 特殊通配符: * 匹配任何域
	 */
	domains: Array<string>
	/**
	 * 將按順序匹配傳入請求的路由列表。 將使用匹配的第一條路線
	 * @remarks
	 * 只能指定 routes 和 matcher 其中之一
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-route config.route.v3.Route}
	 */
	routes?: Array<config.route.v3.Route>
	/**
	 * 解析傳入請求的路由操作時使用的匹配樹
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/xds/type/matcher/v3/matcher.proto#envoy-v3-api-msg-xds-type-matcher-v3-matcher .xds.type.matcher.v3.Matcher}
	 */
	matcher?: any
	/**
	 * 指定虛擬主機期望的 TLS 強制類型。 如果未指定此選項，則虛擬主機沒有 TLS 要求
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-enum-config-route-v3-virtualhost-tlsrequirementtype config.route.v3.VirtualHost.TlsRequirementType}
	 */
	require_tls?: 'NONE' | 'EXTERNAL_ONLY' | 'ALL'
	/**
	 * 為此虛擬主機定義的虛擬集群列表。 虛擬集群用於額外的統計數據收集。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-virtualcluster config.route.v3.VirtualCluster}
	 */
	virtual_clusters?: Array<VirtualCluster>
	/**
	 * 指定一組將應用於虛擬主機的速率限製配置
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-ratelimit config.route.v3.RateLimit}
	 */
	rate_limits?: Array<RateLimit>
	/**
	 * 指定應添加到此虛擬主機處理的每個請求的 HTTP 標頭列表
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-headervalueoption config.core.v3.HeaderValueOption}
	 */
	request_headers_to_add?: Array<config.core.v3.HeaderValueOption>
	/**
	 * 指定應從此虛擬主機處理的每個請求中刪除的 HTTP 標頭列表
	 */
	request_headers_to_remove?: Array<string>
	/**
	 * 指定應添加到此虛擬主機處理的每個響應的 HTTP 標頭列表
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-headervalueoption config.core.v3.HeaderValueOption}
	 */
	response_headers_to_add?: Array<config.core.v3.HeaderValueOption>
	/**
	 * 指定應從此虛擬主機處理的每個響應中刪除的 HTTP 標頭列表
	 */
	response_headers_to_remove?: Array<string>
	/**
	 * 指示虛擬主機具有 CORS 策略
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-corspolicy config.route.v3.CorsPolicy}
	 */
	cors?: CorsPolicy
	/**
	 * 用於為過濾器提供特定於虛擬主機的配置
	 */
	typed_per_filter_config?: Record<string, any>
	/**
	 * 決定 x-envoy-attempt-count 標頭是否應包含在上游請求中
	 * @defaultValue false
	 */
	include_request_attempt_count?: boolean
	/**
	 * 決定 x-envoy-attempt-count 標頭是否應包含在下游響應中
	 * @defaultValue false
	 */
	include_attempt_count_in_response?: boolean
	/**
	 * 指示該虛擬主機中所有路由的重試策略
	 * @remarks
	 * 設置路由級別條目將優先於此配置，並且將被獨立處理（例如：值不會被繼承）
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-retrypolicy config.route.v3.RetryPolicy}
	 */
	retry_policy?: RetryPolicy
	/**
	 * 指示該虛擬主機中所有路由的對沖策略
	 * @remarks
	 * 設置路由級別條目將優先於此配置，並且將被獨立處理（例如：值不會被繼承）
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-hedgepolicy config.route.v3.HedgePolicy}
	 */
	hedge_policy?: HedgePolicy
	/**
	 * 決定是否在每次嘗試超時啟動的重試中包含 x-envoy-is-timeout-retry 請求標頭
	 */
	include_is_timeout_retry_header?: boolean
	/**
	 * 將為重試和重試緩衝的最大字節數
	 * @remarks
	 * 如果設置並且未設置特定於路由的限制，則實際緩衝的字節將是此值和偵聽器 per_connection_buffer_limit_bytes 的最小值
	 */
	per_request_buffer_limit_bytes?: number
	/**
	 * 為這個虛擬主機下的每條路由指定一組默認的請求鏡像策略
	 * @remarks
	 * 它完全優先於路由配置鏡像策略。 即不合併策略，最具體的非空策略成為鏡像策略。
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction-requestmirrorpolicy config.route.v3.RouteAction.RequestMirrorPolicy}
	 */
	request_mirror_policies?: Array<config.route.v3.RouteAction.RequestMirrorPolicy>
}
```

# [extensions.transport_sockets.tls.v3.Secret](https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/secret.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-secret)

用於設置 tls 證書

```typescript
interface Secret {
	/**
	 * 引用這個證書的唯一名稱 （FQDN、UUID、SPKI、SHA256 等）
	 */
	name?: string
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/common.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-tlscertificate extensions.transport_sockets.tls.v3.TlsCertificate}
	 */
	tls_certificate?: TlsCertificate
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/secret.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-secret extensions.transport_sockets.tls.v3.TlsSessionTicketKeys}
	 */
	session_ticket_keys?: TlsSessionTicketKeys
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/common.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-certificatevalidationcontext extensions.transport_sockets.tls.v3.CertificateValidationContext}
	 */
	validation_context?: CertificateValidationContext
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/secret.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-genericsecret extensions.transport_sockets.tls.v3.GenericSecret}
	 */
	generic_secret?: GenericSecret
}
```

## tls_certificate

([extensions.transport_sockets.tls.v3.TlsCertificate](https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/common.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-tlscertificate)) 指定 tls 證書

```
interface TlsCertificate {
	/**
	 * The TLS certificate chain.
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource config.core.v3.DataSource}
	 */
	certificate_chain?: config.core.v3.DataSource
	/**
	 * The TLS private key.
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource config.core.v3.DataSource}
	 */
	private_key?: config.core.v3.DataSource
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource config.core.v3.DataSource}
	 */
	pkcs12?: config.core.v3.DataSource
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-watcheddirectory config.core.v3.WatchedDirectory}
	 */
	watched_directory?: config.core.v3.WatchedDirectory
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/common.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-privatekeyprovider extensions.transport_sockets.tls.v3.PrivateKeyProvider}
	 */
	private_key_provider?: PrivateKeyProvider
	/**
	 * The password to decrypt the TLS private key.
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource config.core.v3.DataSource}
	 */
	password?: config.core.v3.DataSource
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource config.core.v3.DataSource}
	 */
	ocsp_staple?: config.core.v3.DataSource
}
```