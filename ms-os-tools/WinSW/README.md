# WinSW

windows 的進程管理 根本就是 狗屎 服務 說是狗屎都是 侮辱 狗屎這個詞 然如果生活所迫 你不得不用 windows的 服務時 WinSW是一個 方案

WinSW 是一個 開源(MIT) 的 windows service 包裝工具 可以 將一個進程 包裝成 windows的 服務 運行

源碼 [https://github.com/kohsuke/winsw](https://github.com/kohsuke/winsw)

# 使用

1. 下載最新 二進制檔案 並改名爲一個 你方便輸入的 命令名稱 (後文都假設改名爲 winsw.exe)

1. 編輯一個 同名 的 xml 檔案 

   ```xml
   <configuration>
   	<!-- window 服務 id 必須 唯一 -->
   	<id>auto-deploy-service</id>
   	<!-- 顯示的服務名稱 必須唯一 -->
   	<name>Auto Deploy Service</name>
   	<!-- 服務描述信息 -->
   	<description>套件管理器 (一個兼容 linux 和 windows 的 套件 發佈平臺)</description>
   	<!-- 設置工作目錄 -->
   	<workingdirectory>%BASE%</workingdirectory>
   	<!-- 服務啓動的 進程 -->
   	<executable>%BASE%\auto-deploy-service.exe</executable>
   	<!-- 啓動服務時 傳給進程的 參數 -->
   	<startargument>daemon</startargument>
   	<startargument>-d</startargument>
   	<!-- 停止服務時 傳給進程的 參數 -->
   	<stopargument>exit</stopargument>
   	<log mode="none"/>
   </configuration>
   ```

1.  安裝/卸載 服務

   ```sh
	 # 安裝
	 ./winsw install
	 # 卸載
	 ./winsw uninstall
   ```

1. 運行/停止/查看 服務

   ```sh
	 # 運行
	 ./winsw start
	 
	 # 停止
	 ./winsw stop
	 
	 # 查看 服務 狀態
	 ./winsw status
   ```

# 進程

* workingdirectory 條目配置 進程工作目錄

* executable 條目配置 可執行檔

* startargument 條目配置 啓動 參數

* stopargument 可選條目 如果沒有配置 強制結束進程 否則 執行 executable + stopargument  結束進程

* stopexecutable 可選條目 如果配置了此項 使用 stopexecutable 結束進程 

* stoptimeout 可選條目 如果配置了 則當結束進程超時 就強制結束

   ```xml
      <!-- 設置10秒超時 -->
   <stoptimeout>10sec</stoptimeout>
   ```
	 
# 監視服務異常退出

如果 服務進程 退出 並且 返回 非0 則 會被認爲是 異常退出 可以設置 onfailure 條目 配置 如何 重啓服務

```xml
<onfailure action="restart" delay="10 sec"/>
<onfailure action="restart" delay="20 sec"/>
<onfailure action="reboot" />
```

1. 上述 配置 第一次 異常退出 會 延遲10秒後 重啓服務
1. 第二次 異常退出 延遲20秒後 重啓服務
1. 第三次 異常退出 重啓windows

當異常退出 次數 超過 onfailure 條目 則會 使用 最後一個 配置 故如果要 讓 服務 一直保證運行 去掉 reboot 即可

```xml
<onfailure action="restart" delay="10 sec"/>
<onfailure action="restart" delay="20 sec"/>
<onfailure action="reboot" />
```

action 支持 三個 取值
* restart 重新運行服務
* reboot 重啓windows
* none 什麼都不做

# 日誌配置

winsw 會將 進程的 stdout stderror 作爲日誌記錄

logpath 配置日誌目錄 默認使用 配置檔案所在目錄

log 條目 配置 日誌 如何工作

## 追加

默認的 行爲 將 stdout 輸出到 xx.out.log 將 stderror 輸出到 xx.err.log

```xml
<log mode="append"/>
```

## 截斷
類似 append 不過 每次重啓服務 都會清空舊日誌
```xml
<log mode="reset"/>
```

## 關閉日誌
```xml
<log mode="none"/>
```

## 大小旋轉
保存 8個 日誌檔案 每個 最大  10240kb 既然10mb

```xml
<log mode="roll-by-size">
	<sizeThreshold>10240</sizeThreshold>
	<keepFiles>8</keepFiles>
</log>
```
* xx.1.out.log
* xx.2.out.log 
* ...

## 日期旋轉

```xml
<log mode="roll-by-time">
		<pattern>yyyyMMdd</pattern>
</log>
```

* xx.20130101.out.log
* xx.20130102.out.log
* ...


## 日誌 大小 一起使用
```xml
<log mode="roll-by-size-time">
	<sizeThreshold>10240</sizeThreshold>
	<pattern>yyyyMMdd</pattern>
	<autoRollAtTime>00:00:00</autoRollAtTime>
	<zipOlderThanNumDays>5</zipOlderThanNumDays>
	<zipDateFormat>yyyyMM</zipDateFormat>
</log>
```