# [Streams](https://developer.mozilla.org/en-US/docs/Web/API/Streams_API)

Streams 是對 IO 操作的標準與簡化，它規定了如何操作 IO 並爲簡化 IO 進行了努力，它同樣可以在 Web Workers 中被運用

# [ReadableStream](https://developer.mozilla.org/en-US/docs/Web/API/ReadableStream/ReadableStream)

ReadableStream 是可讀流接口，可以使用 new ReadableStream(underlyingSource, strategy)來創建

```
function createReadable() {
  let index = 0;
  return new ReadableStream<number>({
    start: (controller) => {
      controller.enqueue(-1);
    },
    pull: (controller) => {
      controller.enqueue(index++);
      if (index == 10) {
        controller.close();
      }
    },
    cancel: (reason) => {
      console.log("cancel", reason);
    },
  });
}
const readable = createReadable();
for await (const val of readable.values({ preventCancel: true })) {
  console.log(readable, val);
  break;
}
for await (const val of readable.values()) {
  console.log(readable, val);
  break;
}
```

```
ReadableStream { locked: true } -1
ReadableStream { locked: true } 0
cancel undefined
```

* start 會被立刻調用，你可以在裏面執行一些初始化操作，它拋出的異常會被 new ReadableStream 捕獲
* pull 會在沒有數據可讀並且被讀取時調用，需要在此填充(controller.enqueue)被讀的數據，可以返回一個 PromiseLike&lt;void&gt; 來異步讀取，它拋出的異常會被 讀取調用 捕獲
* cancel 會在取消(readable.cancel)時被調用一次，如果已經 controller.close(), cancel 不會被回調

1. controller.enqueue 向隊列寫入一個數據
2. enqueue.close 關閉讀取接口

## values

values() 函數返回一個異步迭代器 用於讀取全部數據

```
for await (const val of readable.values()) {
	console.log(readable, val);
}
```

for 提前結束，return/break 會自動調用 readable.cancel()，如果拋出的異常會被 for 捕獲，可以設置 preventCancel 爲 true 防止 readable.cancel() 被調用

```
for await (const val of readable.values({ preventCancel: true })) {
  if (val == 5) {
    console.log("found", 5);
    break;
  }
}
for await (const val of readable.values()) {
  console.log(val);
}
```

## getReader

getReader 返回一個讀取接口:
* **read** 函數用於讀取數據
*  **releaseLock** 釋放 readable 的鎖 locked，以便可以繼續操作 readable
*  **cancel** 函數類似 readable.cancel 
*  **closed** 屬性返回一個 promose 等待 reader 讀取結束(讀完或 cancel)，一旦獲取 closed 就不能調用 releaseLock

```
const reader = readable.getReader();
const { done, value } = await reader.read();
console.log(done, value);
reader.releaseLock();
for await (const value of readable.values()) {
  console.log(value);
}
```

```
const reader = readable.getReader();
reader.closed.then(() => {
  console.log("end");
});
while (true) {
  const { done, value } = await reader.read();
  if (done) {
    break;
  }
  console.log(done, value);
}
```
> console.log("end" 可能會在最後一次 打印之前，因爲 controller.close() 會先喚醒 closed 

## type

可以在 new ReadableStream 時設置 type 爲 "bytes" 來創建一個 二進制的讀取流

```
function createReadable() {
  let index = 0;
  return new ReadableStream({
    type: "bytes",
    start: (controller) => {
      controller.enqueue(new TextEncoder().encode("-1"));
    },
    pull: (controller) => {
      controller.enqueue(new TextEncoder().encode(`${index++}`));
      if (index == 10) {
        controller.close();
      }
    },
    cancel: (reason) => {
      console.log("cancel", reason);
    },
  });
}
const readable = createReadable();
for await (const val of readable.values()) {
  console.log(new TextDecoder().decode(val));
}
```

## strategy

ReadableStream 第二個可選參數定義如下

```
{ highWaterMark?: number; size?: undefined }
```

* **size** 指定每個區塊大小
* **highWaterMark** 指定內部區塊最大數量

## from

靜態函數 from 用於將 迭代器轉會 可讀流

```
const readable = ReadableStream.from([1, 2, 3, 4]);
for await (const val of readable.values()) {
  console.log(val);
}
```

# [WritableStream](https://developer.mozilla.org/en-US/docs/Web/API/WritableStream/WritableStream)

WritableStream 是可寫流接口，可以使用 new WritableStream(underlyingSink, strategy) 來創建

* **start** 會被 new WritableStream 調用，拋出的異常也由 new 捕獲，通常用於打開底層的 寫入設備
* **write** 會在資料準備好寫入時被調用
* **close** 在資料寫入完畢後被調用
* **abort** 在資料寫入中途需要強制結束時被調用

```
const readable = ReadableStream.from([1, 2, 3, 4]);

function createWritable() {
  return new WritableStream({
    start(controller) {
      console.log("writable start");
    },
    write(chunk, controller) {
      console.log(chunk);
    },
    close() {
      console.log("close");
    },
    abort(reason) {
      console.log("abort", reason);
    },
  });
}
const writable = createWritable();
await readable.pipeTo(writable);
```

> writable.close 只能調用一次，重複調用會拋出異常，abort 可多次調用但只有第一次調用會有效

## pipeTo

readable.pipeTo 可以將 ReadableStream 的數據寫入到 WritableStream

1. pipeTo 會在寫入結束後自動調用 writable.close()
2. 如果寫入出錯，會自動調用 readable.cancel(e)
3. 如果讀取出錯，會自動調用 writable.abort(e)

```
await readable.pipeTo(writable, {
	preventClose: true, // 不調用 writable.close()
	preventCancel: true, // 不調用 readable.cancel(e)
	preventAbort: true, // 不調用 writable.abort(e)
});
```