# docekr
jellyfin 官方提供了一個 [docker](https://hub.docker.com/r/jellyfin/jellyfin/) 鏡像，但是 hub 沒找到使用說明， linuxserver 團隊也維護了一份帶使用說明的 [docker](https://hub.docker.com/r/linuxserver/jellyfin) 鏡像，並且 linuxserver 團隊使用起來更加簡單且風格和 linuxserver 其它鏡像類似，故推薦使用 linuxserver 維護的版本

```
docker run -d \
    --name jellyfin \
    -e LANG=en_US.UTF-8 \
    -v /etc/localtime:/etc/localtime:ro \
    -v /etc/timezone:/etc/timezone:ro \
    --device /dev/dri:/dev/dri \
    -e PUID=1000 \
    -e PGID=1000 \
    -v your_data:/data \
    -v your_config:/config \
    -p 12345:8096 \
    linuxserver/jellyfin:latest
```

* PUID/PGID 分別指定運行 jellyfin 的 用戶 id 和 用戶組 id
* your_data 通常是掛載到 docker 中的視頻存儲檔案夾
* your_config 是 jellyfin 保存的設定和元數據，需要掛載到持久層否則 docker 重啓所有設定和元數據就丟失了
* 8096 端口是 jellyfin 默認提供的 http 訪問接口
* --device 用於將 interl vaapi 驅動映射給 jellyfin 以便使用 硬編解碼 視頻，如果不使用 vaapi 或使用 其它硬件可以映射其它驅動

linuxserver 會保證啓動 jellyfin 的用戶擁有正確的權限去打開驅動，對於 jellyfin 官網提供的鏡像似乎需要自己設定好用戶與驅動的權限

# 中文字幕方塊

1. 從 [https://github.com/CodePlayer/webfont-noto/tree/master/release](https://github.com/CodePlayer/webfont-noto/tree/master/release) 下載 [NotoSansCJKtc-hinted-standard.zip](https://github.com/CodePlayer/webfont-noto/blob/master/release/NotoSansCJKtc-hinted-standard.zip) 和 [NotoSansCJKsc-hinted-standard.zip](https://github.com/CodePlayer/webfont-noto/blob/master/release/NotoSansCJKsc-hinted-standard.zip) 並將 **NotoSansCJKsc-Medium.woff2** 和 **NotoSansCJKsc-Medium.woff2** 解壓並掛接到 docker，比如掛接到 **/config/fonts**
2. 在 jellyfin 設定中設置 播放-> 啓用備用字體  
  ![](assets/settings.png)