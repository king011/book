# 動畫

flutter 支持了動畫

* Animation 用來保存 動畫的中間值
* AnimationController 用來 控制 動畫
* Animatable 提供了將 動畫值 映射功能

通過監聽 Animation 值變化 來調用 setState 來重繪 實現動畫 效果

```
import 'dart:math' as math;
import 'package:flutter/material.dart';
import '../widget/drawer.dart';
import '../../Fontello.dart';

class ExamplePage extends StatefulWidget {
  ExamplePage({Key key}) : super(key: key);
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  // 通知 重新繪製 ui
  _listener() {
    setState(() {});
  }

  // 監聽 動畫狀態
  _statusListener(AnimationStatus status) {
    switch (status) {
      case AnimationStatus.dismissed:
        // 動畫 在開始時 停止 通常是 reset 引起的
        debugPrint("dismissed");
        break;
      case AnimationStatus.forward:
        // 正向執行動畫
        debugPrint("forward");
        break;
      case AnimationStatus.reverse:
        // 逆向執行動畫
        debugPrint("reverse");
        break;
      case AnimationStatus.completed:
        // 動畫 執行完畢
        debugPrint("completed");
        break;
    }
  }

  _animation() {
    if (controller == null) {
      // 初始化 控制器
      controller = AnimationController(
        duration: Duration(seconds: 5),
        vsync: this,
      );
    } else {
      // 重置 控制器
      controller.reset();
      // 移除 監聽器
      animation
        ..removeListener(_listener)
        ..removeStatusListener(_statusListener);
    }

    // 映射動畫變換 並監聽狀態
    animation = Tween(begin: 0.0, end: math.pi * 2).animate(controller)
      ..addListener(_listener)
      ..addStatusListener(_statusListener);

    // 啓動正向動畫
    controller.forward();
  }

  @override
  dispose() {
    // 釋放動畫資源
    if (controller != null) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: createDrawer(context, home: true),
      appBar: AppBar(
        title: Text("Example"),
      ),
      body: Center(
        child: Transform.rotate(
          angle: animation == null ? 0 : animation.value,
          child: Icon(
            Fontello.spin1,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _animation,
      ),
    );
  }
}
```

# AnimatedController

AnimatedController 是動畫 控制器 通常需要定義一個 動畫持續時間 動畫值變化(默認 0.0到 1.0) 和 vsync

vsync 是 TickerProvider 通常 用當前 class with SingleTickerProviderStateMixin 即可

用來 將 動畫 綁定到一個 可視的 widget 上 以便 ui 只在 屏幕內才執行 動畫計算

# AnimatedWidget 

AnimatedWidget 用來 簡化 動畫 操作

其會 自動 監聽 動畫變化 並 重繪自己

```
import 'dart:math' as math;
import 'package:flutter/material.dart';
import '../widget/drawer.dart';
import '../../Fontello.dart';

class ExamplePage extends StatefulWidget {
  ExamplePage({Key key}) : super(key: key);
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  @override
  void initState() {
    super.initState();
    // AnimatedWidget 不允許 listenable 爲 null 所以 必須 在 initState 中 初始化
    _animation();
  }

  _animation() {
    if (controller == null) {
      // 初始化 控制器
      controller = AnimationController(
        duration: Duration(seconds: 5),
        vsync: this,
      );
    } else {
      // 重置 控制器
      controller.reset();
    }

    // 映射動畫變換
    animation = Tween(begin: 0.0, end: math.pi * 2).animate(controller);

    // 啓動正向動畫
    controller.forward();
  }

  @override
  dispose() {
    // 釋放動畫資源
    if (controller != null) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: createDrawer(context, home: true),
      appBar: AppBar(
        title: Text("Example"),
      ),
      body: Center(
        child: Rotate(
          animation: animation,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _animation,
      ),
    );
  }
}

class Rotate extends AnimatedWidget {
  Rotate({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Transform.rotate(
      angle: animation == null ? 0 : animation.value,
      child: Icon(
        Fontello.spin1,
        color: Theme.of(context).accentColor,
      ),
    );
  }
}
```

# AnimatedBuilder

AnimatedBuilder 類似  AnimatedWidget 不過 其需要 接受一個 builder 函數 在 動畫變化 後 AnimatedBuilder 會 調用 builder 來重繪製自己

```
import 'dart:math' as math;
import 'package:flutter/material.dart';
import '../widget/drawer.dart';
import '../../Fontello.dart';

class ExamplePage extends StatefulWidget {
  ExamplePage({Key key}) : super(key: key);
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  @override
  void initState() {
    super.initState();
    // AnimatedWidget 不允許 listenable 爲 null 所以 必須 在 initState 中 初始化
    _animation();
  }

  _animation() {
    if (controller == null) {
      // 初始化 控制器
      controller = AnimationController(
        duration: Duration(seconds: 5),
        vsync: this,
      );
    } else {
      // 重置 控制器
      controller.reset();
    }

    // 映射動畫變換
    animation = Tween(begin: 0.0, end: math.pi * 2).animate(controller);

    // 啓動正向動畫
    controller.forward();
  }

  @override
  dispose() {
    // 釋放動畫資源
    if (controller != null) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: createDrawer(context, home: true),
      appBar: AppBar(
        title: Text("Example"),
      ),
      body: Center(
        child: AnimatedBuilder(
          animation: animation,
          builder: (context, child) {
            return Transform.rotate(
              angle: animation.value,
              child: child,
            );
          },
          child: Icon(
            Fontello.spin1,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _animation,
      ),
    );
  }
}
```
