# 序列

序列支持 成員關係操作符(in) 大小計算函數\(len\(\)\) 分片\(\[\]\) 並且可迭代

python 提供了5種內置序列 bytearray bytes list str tuple

# iterable

iterable 數據每次返回其中的一個 數據  
任意包含 \_\_iter\_\_\(\) 方法的對象或序列  
(包含 \_\_getitem\_\_\(\) 方法 此方法接收從0開始的整數參數)  
都是一個 iterable 並可提供一個迭代子  

迭代子 是一個對象 該對象提供一個 \_\_next\_\_\(\) 方法  
該方法 依次返回每個相繼的數據 在沒有數據時 產生 StopIteration 異常

## 常見迭代操作符 函數



| 語法 | 描述 |
| -------- | -------- |
| s + t     | 返回一個序列 該序列是序列 s 和 t 的連接     |
| s * n     | 返回一個序列 該序列是序列 s 的 n 個副本     |
| x in i     | 如果 數據 x 出現在 iterable i 中 返回 True  <br> not in 進行相反的測試     |
| all(i)     | 如果 iterable i 中每一項都爲 True 返回 True     |
| any(i)     | 如果 iterable i 中任意項爲 True 返回 True     |
| enumerate(i,start)     |      |
| len(x)     | 返回 x 的 長度     |
| max(i,key)     | 返回 iterable i 中的最大項 如果給定函數 key 返回 key(item)值最大的項     |
| min(i,key)     | 返回 iterable i 中的最小項 ...     |
| range(start,stop,step)     | 返回一個 給定的 迭代子 \[start,stop\)     |
| reversed(i)     | 返回 iterable i 的反序迭代子     |
| sorted(i,key,reverse)     | 排序 iterable i     |
| sum(i,start)     | 計算 iterable i 的和     |
| zip(i1,...,1N)     | 返回元組的 迭代子     |

#  默認字典

collections 包中的 defaultdict 提供了匿名字典   
其方法 基本同 dict   
只是 當 c\[k\] 不存在時 不會產生 KeyError 異常 而是自動 創建 c\[k\]  
collections.defaultdict(f) 接收一個 函數 f 用於 創建 默認 的 項目  

```py
#!/usr/bin/env python3
 
import collections
 
#創建一個 默認字典 默認值爲 int()
c = collections.defaultdict(int)
print(c[1])
 
#創建一個 默認字典 默認值爲 -1
c = collections.defaultdict(lambda :-1)
print(c[1])
```

# dict 字典
dict 類似 boost::unordered_map 是一個 鍵值對 哈希容器  
\# 設置/新增 元素  
d\[key\] = 

\# 返回元素 不存在可以 產生 KeyError 異常  
d\[key\]

\# 刪除元素    
del d\[key\]

可以使用多種方式 創建 dict
```py
#!/usr/bin/env python3
d1 = {"Id":1,"Name":"kate"}
d2 = dict({"Id":1,"Name":"kate"})
d3 = dict([("Id",1),("Name","kate")])
d4 = dict(zip(("Id","Name"),(1,"kate")))
print(d1)
print(d2)
print(d3)
print(d4)
```
