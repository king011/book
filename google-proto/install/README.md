# install
```bash
git clone -b v3.11.4 https://github.com/protocolbuffers/protobuf
cd protobuf
git submodule update --init
mkdir build && cd build
cmake ../cmake
make 
sudo make install
```