# [快速入門](https://www.envoyproxy.io/docs/envoy/latest/start/quick-start/index.html)

本文將介紹 Envoy 服務器的基本操作 並介紹 Envoy 可以使用的配置

# 運行 Envoy

```
# 打印版本信息
envoy --version

# 打印幫助信息
envoy --help

# 以指定配置檔案運行
envoy -c /etc/envoy/envoy.yaml
```
## 驗證配置

使用 **--mode validate** 參數 驗證配置

```
envoy --mode validate -c /etc/envoy/envoy.yaml
```

## 日誌記錄

envoy 默認將錯誤輸出到 /dev/stderr 可以使用 **--log-path filepath** 指定將錯誤輸出到檔案

```
mkdir logs
envoy -c envoy-demo.yaml --log-path logs/custom.log
```

可以爲 admin interface 和 listeners 設置 Access log 路徑

```
#info=12
typed_config:
    "@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
    stat_prefix: ingress_http
    access_log:
    - name: envoy.access_loggers.stdout
    typed_config:
        "@type": type.googleapis.com/envoy.extensions.access_loggers.stream.v3.StdoutAccessLog
    http_filters:
    - name: envoy.filters.http.router
    route_config:
    name: local_route
```

## 日誌等級

使用 **-l** 或 **--log-level** 設置日誌等級

* trace
* debug
* info (默認等級)
* warning/warn
* error
* critical
* off

此外還可以使用 --component-log-level 爲指定組件設置日誌等級

以下示例禁止所有日誌記錄，但 upstream 和 connection 組件分別設置 debug 和 trace 等級

# 靜態配置

將 listeners  和 clusters 定義到 static_resources 中進行靜態配置，envoy 將在啓動時靜態配置所有內容，而不是在運行時動態配置

```
static_resources:
  listeners:
  clusters:
```

## listeners

下面的示例配置了一個 listener 監聽在 10000 端口 並將所有路徑都匹配到 service\_envoyproxy\_io 集羣

```
static_resources:
  listeners:
    - name: listener_0
      address:
        socket_address:
          address: 0.0.0.0
          port_value: 10000
      filter_chains:
        - filters:
            - name: envoy.filters.network.http_connection_manager
              typed_config:
                "@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
                stat_prefix: ingress_http
                access_log:
                  - name: envoy.access_loggers.stdout
                    typed_config:
                      "@type": type.googleapis.com/envoy.extensions.access_loggers.stream.v3.StdoutAccessLog
                http_filters:
                  - name: envoy.filters.http.router
                route_config:
                  name: local_route
                  virtual_hosts:
                    - name: local_service
                      domains: ["*"]
                      routes:
                        - match:
                            prefix: "/"
                          route:
                            host_rewrite_literal: www.envoyproxy.io
                            cluster: service_envoyproxy_io
  clusters:
    - name: service_envoyproxy_io
```

## clusters
下面示例配置了一個集通過 TLS 代理到 https://www.envoyproxy.io

```
  clusters:
    - name: service_envoyproxy_io
      type: LOGICAL_DNS
      # Comment out the following line to test on v6 networks
      dns_lookup_family: V4_ONLY
      load_assignment:
        cluster_name: service_envoyproxy_io
        endpoints:
          - lb_endpoints:
              - endpoint:
                  address:
                    socket_address:
                      address: www.envoyproxy.io
                      port_value: 443
      transport_socket:
        name: envoy.transport_sockets.tls
        typed_config:
          "@type": type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.UpstreamTlsContext
          sni: www.envoyproxy.io
```

# 動態配置檔案系統

可以配置從檔案系統動態加載配置

當檔案系統發生變化時，envoy 將自動更新配置，這需要配置

* node 唯一標識代理節點
* dynamic_resources 告訴 envoy 在哪裏可以找到動態配置

對於示例還需要兩個檔案

* lds.yaml 配置 listeners
* cds.yaml 配置 clusters

```
node:
  cluster: test-cluster
  id: test-id

dynamic_resources:
  cds_config:
    path: /etc/envoy/cds.yaml
  lds_config:
    path: /etc/envoy/lds.yaml
```

## listeners

監聽器工作在 10000 端口上，所有域和路徑都匹配到 service\_envoyproxy\_io 集羣，host被重寫爲 www.envoyproxy.io

```
#info="lds.yaml"
resources:
  - "@type": type.googleapis.com/envoy.config.listener.v3.Listener
    name: listener_0
    address:
      socket_address:
        address: 0.0.0.0
        port_value: 10000
    filter_chains:
      - filters:
          - name: envoy.http_connection_manager
            typed_config:
              "@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
              stat_prefix: ingress_http
              http_filters:
                - name: envoy.router
              route_config:
                name: local_route
                virtual_hosts:
                  - name: local_service
                    domains:
                      - "*"
                    routes:
                      - match:
                          prefix: "/"
                        route:
                          host_rewrite_literal: www.envoyproxy.io
                          cluster: example_proxy_cluster
```

## clusters

example\_proxy\_cluster 集羣通過 TLS 代理到 https://www.envoyproxy.io

```
#info="cds.yaml"
resources:
  - "@type": type.googleapis.com/envoy.config.cluster.v3.Cluster
    name: example_proxy_cluster
    type: STRICT_DNS
    typed_extension_protocol_options:
      envoy.extensions.upstreams.http.v3.HttpProtocolOptions:
        "@type": type.googleapis.com/envoy.extensions.upstreams.http.v3.HttpProtocolOptions
        explicit_http_config:
          http2_protocol_options: {}
    load_assignment:
      cluster_name: example_proxy_cluster
      endpoints:
        - lb_endpoints:
            - endpoint:
                address:
                  socket_address:
                    address: www.envoyproxy.io
                    port_value: 443
    transport_socket:
      name: envoy.transport_sockets.tls
      typed_config:
        "@type": type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.UpstreamTlsContext
        sni: www.envoyproxy.io
```

# 動態配置控制平面

可以通過控制平面來 動態配置 envoy 示例需要配置至少如下內容

* node 唯一標識的代理節點
* dynamic_resources 告訴 envoy 哪些配置應該動態更新
* static_resources 告訴 envoy 應從何處檢索配置

## node

```
node:
  cluster: test-cluster
  id: test-id
```

## dynamic_resources

示例中配置有 xds\_cluster 提供

```
#info=5
dynamic_resources:
  ads_config:
    api_type: GRPC
    transport_api_version: V3
    grpc_services:
      - envoy_grpc:
          cluster_name: xds_cluster
  cds_config:
    resource_api_version: V3
    ads: {}
  lds_config:
    resource_api_version: V3
    ads: {}
```

## static_resources

在 static\_resources 中 xds\_cluster 被配置爲在 http://my-control-plane:18000 查詢控制平面

```
#info=19
static_resources:
  clusters:
    - type: STRICT_DNS
      typed_extension_protocol_options:
        envoy.extensions.upstreams.http.v3.HttpProtocolOptions:
          "@type": type.googleapis.com/envoy.extensions.upstreams.http.v3.HttpProtocolOptions
          explicit_http_config:
            http2_protocol_options: {}
      name: xds_cluster
      load_assignment:
        cluster_name: xds_cluster
        endpoints:
          - lb_endpoints:
              - endpoint:
                  address:
                    socket_address:
                      address: my-control-plane
                      port_value: 18000
```

# admin interface

可以配置一個 admin interface 用於查看配置和統計信息，更改服務器行爲等

```
admin:
  address:
    socket_address:
      protocol: TCP
      address: 0.0.0.0
      port_value: 9901
```