# 中間件

可以在 cron.New 時使用 WithChain 傳入一系列的包裝器 JobWrapper，這些包裝器會在 Job 執行前被調用以便爲原 Job 添加一些額外處理

```
type JobWrapper func(Job) Job
```

cron 提供了三個默認的包裝器

* Recover 會捕獲 Job 產生的 panic
* DelayIfStillRunning 會判斷上次觸發的 Job 是否完成，如果沒有則等待其完成才繼續執行 Job
* SkipIfStillRunning 會判斷上次觸發的 Job 是否完成，如果沒有則跳過此處 Job 的執行