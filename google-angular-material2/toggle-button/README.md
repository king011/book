# MatButtonToggleModule

MatButtonToggleModule 提供了 有 on/off 狀態的 按鈕 mat-button-toggle

mat-button-toggle 可以 和 mat-button-toggle-group 配合 實現 單選/複選 的效果

```typescript
import {MatButtonToggleModule} from '@angular/material/button-toggle';
```

```html
<!-- 單選時 mat-button-toggle-group 可以直接 和 ngModel 配合取值 -->
<mat-button-toggle-group #formatAlignGroup="matButtonToggleGroup">
  <mat-button-toggle value="left">
    <mat-icon>format_align_left</mat-icon>
  </mat-button-toggle>
  <mat-button-toggle value="center" checked="true">
    <mat-icon>format_align_center</mat-icon>
  </mat-button-toggle>
  <mat-button-toggle value="right">
    <mat-icon>format_align_right</mat-icon>
  </mat-button-toggle>
</mat-button-toggle-group>
<div>{{ formatAlignGroup.value }}</div>
 
<!-- 加上 multiple 則變為 複選 值不會 被設置到 mat-button-toggle-group 故 也無法使用 ngModel -->
<!-- vertical 為 true 使用 垂直 排列 -->
<mat-button-toggle-group multiple vertical="true">
  <mat-button-toggle value="bold" #buttonToggleBold>
    <mat-icon>format_bold</mat-icon>
  </mat-button-toggle>
  <mat-button-toggle value="italic" checked="true" #buttonToggleItalic>
    <mat-icon>format_italic</mat-icon>
  </mat-button-toggle>
  <mat-button-toggle value="underlined" checked="true" #buttonToggleUnderlined>
    <mat-icon>format_underlined</mat-icon>
  </mat-button-toggle>
</mat-button-toggle-group>
<ol>
  <li>bold {{buttonToggleBold.checked}}</li>
  <li>italic {{buttonToggleItalic.checked}}</li>
  <li>underlined {{buttonToggleUnderlined.checked}}</li>
</ol>
```