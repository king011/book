# LinkedList

LinkedList 是一個雙向鏈表，但它要求其模板類型必須實現 LinkedListEntry

```
import 'dart:collection';

class LinkedValue<T> extends LinkedListEntry<LinkedValue<T>> {
  final T value;

  LinkedValue(this.value);

  @override
  String toString() {
    return value.toString();
  }
}
main() {
  var linkedList = LinkedList<LinkedValue<int>>();
  var prevLinkedEntry = LinkedValue<int>(99);
  var currentLinkedEntry = LinkedValue<int>(100);
  var nextLinkedEntry = LinkedValue<int>(101);
  linkedList.add(currentLinkedEntry);
  currentLinkedEntry.insertBefore(prevLinkedEntry);
  currentLinkedEntry.insertAfter(nextLinkedEntry);
  linkedList.forEach((entry) => print('${entry.value}'));
}
```