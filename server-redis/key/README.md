# 鍵

redis 是 key value 數據庫 其操作都圍繞着 key 展開



| 命令 | 描述 |
| -------- | -------- |
| DEL key     | 如果存在 key 刪除之     |
| DUMP key     | 返回此 key 存儲值的 序列化值     |
| EXISTS key     | 返回此 key 是否存在     |
| EXPIRE key seconds     | 爲 key 指定過期 秒數     |
| EXPIREAT key timestamp      | 爲 key 指定過期 時間(unix 時間戳)     |
| PEXPIRE key milliseconds      | 設置 key 指定過期  毫秒數    |
| PEXPIREAT key milliseconds-timestamp      | 爲 key 指定過期 時間(unix 時間戳 精確到毫秒)     |
| KEYS pattern      | 查找所有與模式匹配的 key     |
| MOVE key db      | 移動 key 到另一個數據庫     |
| PERSIST key      | 移除過期的 key     |
| PTTL key      | 獲取 key 過期 毫秒     |
| TTL key     | 獲取 key 過期 秒數     |
| RANDOMKEY      | 從存在的 keys 中返回一個 隨機 key     |
| RENAME key newkey      | 更改 key 名稱     |
| RENAMENX key newkey      | 只有當新 key 不存在時 重命名     |
| TYPE key     | 返回 key 型別     |


