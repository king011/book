# ColumnLayout RowLayout

* ColumnLayout 垂直排列佈局
* RowLayout 水平排列 佈局
* ColumnLayout RowLayout 用法 基本一致

![assets/cr.png](assets/cr.png)

```
import QtQuick 2.9
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.11
ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: "test Layout"

    ColumnLayout
    {
        anchors.fill: parent
        RowLayout{
            Layout.fillWidth: true

            Label{
                text: "url"
            }
            TextField{
                Layout.fillWidth: true

                id:textURL
                placeholderText:"input image url"
            }
            Button{
                text: "load"
                onClicked: {
                    image.source = textURL.text
                }
            }
        }

        Image {
            Layout.fillWidth: true
            Layout.fillHeight: true

            id: image
        }
    }
}
```

# GridLayout

GridLayout 以表格形式 進行 佈局

![](assets/GridLayout.png)

```
import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11
ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: "test Layout"

    GridLayout{
        anchors.fill: parent

        // 每行 3個 列
        columns:3

        Label{
            text: "proxy"
        }
        TextField{
            Layout.fillWidth: true
            Layout.columnSpan:2 // 佔用 2列
            placeholderText:"input proxy addr"
        }

        Label{
            text: "url"
        }
        TextField{
            Layout.fillWidth: true
            Layout.columnSpan:2 // 佔用 2列
            placeholderText:"input url addr"
        }

        Pane{
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan:2
            // 手動指定列 而非 自動安排位置 column row 必須一起設置
            Layout.column: 1
            Layout.row: 2
            Button{

                text: "download"
            }
        }
    }
}
```