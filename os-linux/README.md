# linux

開源(GPL) 的unix like 作業系統(內核)

官網 [https://www.kernel.org/](https://www.kernel.org/)

wiki [https://zh.wikipedia.org/wiki/Linux#GNU](https://zh.wikipedia.org/wiki/Linux#GNU)

# Ubuntu

Ubuntu 一個 注重 桌面用戶 的linux 發行版本

官網 [http://www.ubuntu.com/](http://www.ubuntu.com/)

# Mint

Mint 一個 基於 Ubuntu 的linux 發行版本

在 GNOME 3 將世界導入黑暗 同時停止 GNOME 2 開發的 年代  
Mint 提供了 MATE (繼承 GNOME 2源碼 繼續開發的 GNOME 2風格 桌面) 桌面

Mint 同時 提供了 四種 可選桌面 環境
* Cinnamon
* MATE  GNOME2 分支的一個輕量桌面
* KDE
* Xface  一個輕量級 適合安裝 到 U盘/老設備

官網 [https://www.linuxmint.com/](https://www.linuxmint.com/)

# Centos

CentOS 依據 Red Hat 開源代碼 重新編譯 打包而成的 一個 linux發行版

可以很好的兼容 Red Hat 軟體

CentOS 注重OS穩定 只有當 組件 達到足夠穩定後才會被 加入  
\(故&nbsp;CentOS上&nbsp;可能無法使用&nbsp;最新的&nbsp;技術&nbsp;套件\)

官網 [https://www.centos.org/](https://www.centos.org/)

# Red Hat

Red Hat 公司 從 1994 年開始發布的 linux 發行版  
其推出的 RPM 軟體包 成為 linux社區的 一個 事實標準 被廣泛使用在 linux發行版中

2004年後 9.0版本 的Red Hat 不再發布desktop版本 專注於 server

**其8.0版本 中 竟然無知的 將 青天白日滿地紅 的旗幟移除 故Red Hat必須 永遠從 選擇方案中 移除**

官網 [https://www.redhat.com/](https://www.redhat.com/)