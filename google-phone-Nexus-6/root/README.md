# 解鎖 OEM 和打開 USB 調試

在開發者選項中打開 USB 調試 和 啓用 **OEM 解鎖**，如果  **OEM 解鎖** 是灰色的

1. 將設備恢復出場設置
2. 連接到能夠正常連接 google 的網路
3. 登入 google 帳號

> 但我測試這個方案無效，不知道是否有人成功，我是直接按住 **音量減小** 不要鬆開，然後按住 **開機鍵** 直到手機進入 bootloader 界面，後用 Nexus Root Toolkit v2.1.9 的 Unlock 按鈕進行解鎖，顯示成功後繼續後續操作也成功 root

# 安裝 Nexus Root Toolkit v2.1.9 和 google 驅動

在 widnwos 電腦上安裝 [Nexus Root Toolkit v2.1.9](http://www.wugfresh.com/nrt/) 並且運行，後續操作確保科學上網環境

![](assets/select.png)  
選擇號硬件信息和軟件信息，如果不確定 build 信息，就選擇 any build 之後點擊右下腳的 **Apply**  

![](assets/usb.png)  
這個頁面是告訴你，用 usb 連接手機到電腦，並在手機上允許 USB 調試，一切準備好後你就可以點擊 **確定** 進入下一步

![](assets/install.png)  
點擊主頁面 Full Driver Installation 安裝驅動

![](assets/driver.png)  
直接去 Step 3 點擊 **Google Driver** 安裝驅動。(Step 1/2 是告訴你如果清除之前安裝的舊驅動)

![](assets/test.png)  
去 Step 4 中點擊 **Full Driver Test** 進行測試，如果驅動安裝正確，測試成功後將彈出 **Success** 對話框，否則請重複 Step 1-3 重安裝驅動

# 解鎖 Bootloader

點擊主頁面中 Bootloader 的 Unlock 解鎖 Bootloader， 此操作前請先在手機開發者選項中啓用 **OEM 解鎖**

本咪測試時 **OEM 解鎖** 一直處於灰色(不清楚是否是沒有鎖定)，點擊 Unlock 失敗。於是手機 按住 **音量減小** 不要鬆開，然後按住 **開機鍵** 直到手機進入 bootloader 界面後，再點擊 Unlock 按鈕，最終提升解鎖成功

# Root
點擊主頁面的 **Root** 按鈕解鎖即可，其中會多次自動重啓手機不用進行任何操作，等待自動操作並最終提示成功即可

> 開始 root 前，會下載一個 img 檔案，可能會失敗，選擇繼續然後會給你個下載 url，手動從此 url 多嘗試幾次下載這個 img 檔案，雖然本喵測試時下載一直失敗了大概7到8次，但最終的確是下載成功並最終 root 成功



