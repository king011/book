# example

```
import { Component, OnInit } from '@angular/core';
import { utils, WorkBook, WorkSheet, write } from 'xlsx'
@Component({
  selector: 'app-xlsx',
  templateUrl: './xlsx.component.html',
  styleUrls: ['./xlsx.component.scss']
})
export class XlsxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }
  onClickFromJSON() {
    const book = utils.book_new()
    const sheet = utils.json_to_sheet([
      {
        id: 1,
        name: 'kate',
      },
      {
        id: 2,
        name: 'anita',
      },
    ])
    utils.book_append_sheet(book, sheet, `test`)
    this._saveAs(`test.xlsx`, book)
  }
  onClickFromAOA() {
    const book = utils.book_new()
    const sheet = utils.aoa_to_sheet(
      [
        ['id', '名稱', '等級'],
        [1, 'kate'],
        [2, 'anita'],
        [undefined, 'illusive mane', 10],
      ]
    )
    utils.book_append_sheet(book, sheet, `test`)
    this._saveAs(`test.xlsx`, book)
  }
  private _saveAs(filename: string, book: WorkBook) {
    const data = write(book, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'array'
    })
    const blob = new Blob([data], { type: "application/octet-stream" })
    this._download(filename, blob)
  }
  private _download(filename: string, blob: Blob) {
    const eleLink = document.createElement('a')
    eleLink.style.display = 'none'
    eleLink.download = filename // set file name
    eleLink.href = URL.createObjectURL(blob) // set file data

    // append to document
    document.body.appendChild(eleLink)
    eleLink.click()
    // remove from document
    document.body.removeChild(eleLink)
  }
}
```