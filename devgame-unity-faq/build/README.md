# 編譯64位 android 程式
unity 默認編譯的是32bit程式，google play 從 2019-08-01 開始要求必須上架64bit程式，故需要在 unity 中設置一下

依次打開 File -> Build Settings -> Android -> Player Settings -> Other Settings 


* 將 Configuration 下的 **Scripting Backend** 由 Mono 改爲 **IL2CPP**
* 此外將 Target Architectures 下的 ARM64 勾選，如果不需要32bit 可以取消 ARMv7 選項
* 另外要將 Target API Level 設置到 google 要求的版本才能上架 play store

ETC2 fallback 並不影響編譯的程式 bit，它是圖像壓縮相關參數

![](assets/amd64.png)

# 爲 android 設置屏幕方向

通常對於豎版 或 橫版 遊戲需要鎖定屏幕方向

依次打開 File -> Build Settings -> Android -> Player Settings -> Resolution adn Presentation ，並設置 Orientation -> Default Orientation 即可，其取值有：

* **Portrait** 豎屏
* **Portrait Upside Down** 豎屏，手機顛倒
* **Landscape Right** 橫屏，屏幕在 home 鍵右邊
* **Landscape Left** 橫屏，屏幕在 home 鍵左邊
* **Auto Rotation** 自動選擇屏幕

當設置爲 **Auto Rotation** 時 需要在 **Allow Orientations for Auto Rotation** 下勾選允許的屏幕方向

下圖是一個典型的豎版遊戲 設定

![](assets/orientation.png)

此外 unity 也允許通過腳本設定，使用腳本可以爲每個場景設置不同的方向，但很少有遊戲需要這麼做