# 菜單 ui

在完成第一個關卡後，我們現在來製作一些簡單的 ui 以使其像一個完整的 遊戲，我們將新建一個名爲 **Menu** 的場景

我們將使用下面的兩個素材，將它們導入到項目的 **Sprites/Menu** 檔案夾下

背景 

![](assets/background.png)

LOGO

![](assets/logo.png)
# 標題界面

幾乎所有遊戲都有標題界面，這是玩家在開始遊戲前呆的地方，新建 **Menu** 場景，我們將在此實現標題界面

## 場景

我們標題頁面將由以下內容組成

* 一個背景圖片
* 一個 LOGO
* 一個顯示按鈕

新建一個 UI 節點(Game Object -> UI -> Image)並命名爲 **Background**

![](assets/ui_create_image.png)

將 anchor 設置爲 horizontally & vertically (選中最右下角那個)

![](assets/ui_anchors.png)

確保 Top Bottom Left Rigth 屬性值都爲 0

現在你應該有一個全屏幕的背景，不要擔心使用的分辨率 unity 會自動拉伸

![](assets/ui_set_background.png)

> 上圖爲 16:9，如果你的效果和它不同，將 Game 預覽裏的畫面比例設置爲 16:9 即可

現在，同添加背景一樣添加 logo
1. 設置 anchor 爲 middle & center
2. 設置它的大小爲 51\*512
3. 勾選 **Preserve Aspect**

![](assets/ui_logo.png)

## 加載腳本

現在，我們將添加按鈕(Game Object -> UI -> Button)，並使用腳本來開始遊戲

![](assets/ui_button.png)

添加一個 **MenuScript**腳本用與啓動遊戲

```
#info="MenuScript.cs"

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("main");
    }
}
```

1. 在層級窗口創建一個 Scripts 對象
2. 將 MenuScript 添加到 Scripts 對象
3. 在按鈕 OnClick 列表中新添加一個處理器
4. 拖動 Scripts對象 拖到到新加的處理器上
5. 設置調用 MenuScript.StartGame

![](assets/ui_button_click.png)

## 添加場景到 build

打開 File -> Build Settings 將新建的場景添加到構建中

![](assets/build_settings.png)

# 死亡和重新開始

通常玩家一旦死亡，可以重新開始遊戲。

添加一個 **GameOverScript** 腳本

```
#info="GameOverScript"
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameOverScript : MonoBehaviour
{
    private Button[] buttons;

    void Awake()
    {
        // 獲取所有按鈕
        buttons = GetComponentsInChildren<Button>();

        // 隱藏它們直到玩家死亡
        HideButtons();
    }

    public void HideButtons()
    {
        foreach (var b in buttons)
        {
            b.gameObject.SetActive(false);
        }
    }

    public void ShowButtons()
    {
        foreach (var b in buttons)
        {
            b.gameObject.SetActive(true);
        }
    }

    public void ExitToMenu()
    {
        // 重新加載菜單場景
        SceneManager.LoadScene("Menu");
    }

    public void RestartGame()
    {
        // 重新加載遊戲場景
        SceneManager.LoadScene("Main");
    }
}
```

我們同樣需要一些  UI 組件

1. 在遊戲場景添加一個 Panel
2. 移除 **Source Image**，將顏色設置爲白色且全透明
3. 添加兩個按鈕
4. 添加 GameOverScript 到 Panel 裏
5. 爲按鈕設置 OnClick 事件

![](assets/ui_gameover.png)

![](assets/ui_gameover_wire.png)

最後我們在 **PlayerScript** 腳本裏，在死亡時(被銷毀)調用 GameOverScript 顯示按鈕

```
#info="PlayerScript"
void OnDestroy()
{
		// 銷毀 遊戲結束
		var gameOver = FindObjectOfType<GameOverScript>();
		gameOver.ShowButtons();
}
```