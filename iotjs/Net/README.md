# [Net](https://github.com/jerryscript-project/iotjs/blob/master/docs/api/IoT.js-API-Net.md)

net 包提供了 tcp 服務器與客戶端相關功能


# Server

class Server 提供了服務器相關功能

```
export interface ServerOptions {
		/**
		 * 如果爲 true，則 socket 將不可讀但它依然可寫，你應顯示調用 socket.end()
		 */
		allowHalfOpen?: boolean
}
/**
 * 創建一個服務器
 * @param options 
 * @param listener  自動註冊到 connection 事件監聽器
 */
export function createServer(options?: ServerOptions, listener?: (s: Socket) => void): Server
export class Server extends EventEmitter {
		private constructor()

		/**
		 * 停止监听新到达的连接。 当所有现有连接关闭时，服务器套接字将最终关闭，然后发出 'close' 事件
		 * @param listener 自動註冊 close 事件監聽器
		 */
		close(listener?: () => void): void
		/**
		 * 
		 * @param port 
		 * @param host 
		 * @param backlog 
		 * @param listener 自動註冊 listening 事件監聽器
		 */
		listen(port: number, host = '0.0.0.0', backlog = 511, listener?: () => void): Server
		/**
		 * 
		 * @param options 
		 * @param listener 
		 */
		listen(options: {
				port: number
				host?: string
				backlog?: number
		}, listener?: () => void): void
		on(event: 'close' | 'listening', listener: () => void): Server
		on(event: 'error', listener: (e) => void): Server
		on(event: 'connection', listener: (s: Socket) => void): Server
		once(event: 'close' | 'listening', listener: () => void): Server
		once(event: 'error', listener: (e) => void): Server
		once(event: 'connection', listener: (s: Socket) => void): Server
		removeListener(event: 'close' | 'listening', listener: () => void): Server
		removeListener(event: 'error', listener: (e) => void): Server
		removeListener(event: 'connection', listener: (s: Socket) => void): Server
}
```

# Socket

Socket 用於 tcp 通信，成員函數 connect 用於客戶端連接服務器，全局 connect 函數完成類似功能，但它連接失敗會直接結束腳本


```
/**
 * 連接服務器
 * @param port 
 * @param host 
 * @param listener 自動註冊到 connect 事件監聽器
 */
export function connect(port: number, host = 'localhost', listener?: () => void): Socket
/**
 * 連接服務器
 * @param options 
 * @param listener 自動註冊到 connect 事件監聽器
 */
export function createConnection(options?: {
		port: number
		host?: string
		family: number/** ip version 4 or 6 */
}, listener?: () => void): Socket
/**
 * 連接服務器
 * @param port 
 * @param host 
 * @param listener 自動註冊到 connect 事件監聽器
 */
export function createConnection(port: number, host = '', listener?: () => void): Socket
/**
 * tcp 套接字
 */
export class Socket extends Duplex {
		constructor(options?: {
				allowHalfOpen?: boolean
		})
		/**
		 * 連接服務器
		 * @param options 
		 * @param listener 自動註冊到 connect 事件監聽器
		 */
		connect(options?: {
				port: number
				host?: string
				family?: number/** ip version 4 or 6 */
		}, listener?: () => void): Socket
		/**
		 * 確保 socket 上不再發生 io 並儘快銷毀
		 */
		destroy(): void
		/**
		 * 將 socket 半關閉此後不再允許寫入數據
		 * @param data 
		 * @param callback 
		 */
		end(data?: Buffer | string, callback?: () => void): void

		/**
		 * 設置 keep-alive
		 * @param enable 
		 * @param initialDelay 
		 */
		setKeepAlive(enable = false, initialDelay = 0): void

		/**
		 * 如果在 timeout 時間內 socket 都處於非活躍狀態則發送 timeout 事件
		 * @param timeout 超時毫秒
		 * @param callback 
		 */
		setTimeout(timeout: number, callback?: () => void): void

		/**
		 * 在連接建立後發送
		 */
		on(event: 'connect', listener: () => void): Socket
		/**
		 * 在 socket 關閉後發送
		 */
		on(event: 'close', listener: () => void): Socket
		/**
		 * 在 socket 收到數據後發送
		 */
		on(event: 'data', listener: (data: Buffer | string) => void): Socket
		/**
		* 在 socket 寫緩衝區爲空時發送
		*/
		on(event: 'drain', listener: () => void): Socket
		/**
		* 在 socket 收到 FIN packet
		*/
		on(event: 'end', listener: () => void): Socket
		/**
		* 在 socket 出現錯誤
		*/
		on(event: 'error', listener: (e: Error) => void): Socket
		/**
		* 在 socket 解析 lookup 之後
		*/
		on(event: 'lookup', listener: () => void): Socket
		/**
		* 在 socket setTimeout 處於非活躍超時後發送
		*/
		on(event: 'timeout', listener: () => void): Socket
}
```

# Example

下面是一個 echo 服務器和客戶端的例子

```
#info="server.ts"
import { Server, createServer } from "net";

function listenServer(addr: string, backlog = 511): Promise<Server> {
    const i = addr.lastIndexOf(":")
    if (i == -1) {
        throw new Error(`addr invalid: ${addr}`);
    }
    let host = addr.substring(0, i)
    if (host == '') {
        host = '0.0.0.0'
    }
    const s = addr.substring(i + 1)
    const port = parseInt(s, 10)
    if (!isFinite(port) && port <= 65535 && port > 0) {
        throw new Error(`addr invalid: ${addr}`)
    }
    return new Promise((resolve, reject) => {
        const srv = createServer()
        try {
            srv.once('error', (e: any) => {
                srv.removeAllListeners()
                reject(new Error(`listen error ${e}`))
            })
            srv.once('listening', () => {
                srv.removeAllListeners()
                resolve(srv)
            })

            // 目前 2023-05-05，Promise 實現存在問題(直接在當前循環中 resolve/reject)，故最好在 timeout 中調用，否則一些系統 api 會卡死
            //   srv.listen(port, host, backlog) // 直接調用會導致 resolve 無法觸發 Promise
            setTimeout(() => {
                srv.listen(port, host, backlog)
            }, 0)
        } catch (e) {
            srv.removeAllListeners()
            reject(e)
        }
    })
}

async function main() {
    try {
        const addr = ':9090'
        const srv = await listenServer(addr)
        console.log('listen on', addr)
        let i = 0
        srv.on('connection', (s) => {
            const id = i++
            console.log(`${id}: connection`)
            // echo
            s.on('data', (data) => {
                console.log(`${id} recv: ${data}`)
                s.write(data)
            })
            // destroy on error
            s.on('error', (e) => {
                s.removeAllListeners()
                console.log(`${id} error: ${e}`)
                s.destroy()
            })
            // destroy on remote close
            s.on('close', () => {
                s.removeAllListeners()
                console.log(`${id} close`)
                s.destroy()
            })
            // FIN packet
            s.on('end', () => {
                console.log(`${id} end`)
            })
            // 非活躍超時關閉
            s.setTimeout(1000, () => {
                s.removeAllListeners()
                console.log(`${id} timeout`)
                s.destroy()
            })
        })
    } catch (e) {
        console.log(e)
    }
}
main()
```

```
#info="client.ts"
import Buffer from "buffer";
import { Socket } from "net";
export class Completer<T>{
    private promise_: Promise<T> | undefined
    private resolve_: ((value?: T | PromiseLike<T>) => void) | undefined
    private reject_: ((reason?: any) => void) | undefined
    private c_ = false
    /**
     * Returns whether the Promise has completed
     */
    get isCompleted(): boolean {
        return this.c_
    }
    constructor() {
        this.promise_ = new Promise<T>((resolve, reject) => {
            this.resolve_ = resolve as any
            this.reject_ = reject
        })
    }
    /**
     * Returns the created Promise
     */
    get promise(): Promise<T> {
        return this.promise_ as Promise<T>
    }
    /**
     * Complete success, Promise.resolve(value) 
     * @param value 
     */
    resolve(value?: T | PromiseLike<T>): void {
        if (this.c_) {
            return
        }
        this.c_ = true
        if (this.resolve_) {
            this.resolve_(value)
        }
    }
    /**
     * Complete error, Promise.reject(reason) 
     * @param reason 
     */
    reject(reason?: any): void {
        if (this.c_) {
            return
        }
        this.c_ = true
        if (this.reject_) {
            this.reject_(reason)
        }
    }
}

class Conn {
    static connect(addr: string): Promise<Conn> {
        return new Promise((resolve, reject) => {
            try {
                const i = addr.lastIndexOf(":")
                if (i == -1) {
                    throw new Error(`addr invalid: ${addr}`);
                }
                let host = addr.substring(0, i)
                if (host == '') {
                    host = '127.0.0.1'
                }
                const s = addr.substring(i + 1)
                const port = parseInt(s, 10)
                if (!isFinite(port) && port <= 65535 && port > 0) {
                    throw new Error(`addr invalid: ${addr}`)
                }
                const socket = new Socket()
                socket.on('error', (e) => {
                    socket.removeAllListeners()
                    reject(e)
                })
                socket.on('connect', () => {
                    socket.removeAllListeners()
                    resolve(new Conn(socket))
                })
                setTimeout(() => {
                    socket.connect({
                        port: port,
                        host: host,
                    })
                }, 0)
            } catch (e) {
                reject(e)
            }
        })
    }
    constructor(private readonly s_: Socket) {
        s_.on('error', (e) => {
            s_.removeAllListeners()
            this.close()
        })
        s_.on('end', () => {
            s_.removeAllListeners()
            this.close()
        })
    }
    private closed_ = false
    close(): void {
        if (this.closed_) {
            return
        }
        this.closed_ = true
        this.s_.destroy()
        this._closeRead()
    }
    write(s: string): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.s_.write(s, () => {
                    resolve()
                })
            } catch (e) {
                reject(e)
            }
        })
    }
    private read_?: Completer<Buffer | null>
    private _closeRead() {
        const c = this.read_
        if (c) {
            this.read_ = undefined
            c.reject(new Error("conn already closed"))
        }
    }
    async read(): Promise<Buffer | null> {
        // 等待先前未讀完的數據讀完
        while (this.read_) {
            await this.read_.promise
        }
        if (this.closed_) {
            throw new Error("conn already closed")
        }
        const c = new Completer<Buffer | null>()
        this.read_ = c
        try {
            this.s_.once('readable', () => {
                this.read_ = undefined
                c.resolve(this.s_.read())
            })
        } catch (e) {
            this.read_ = undefined
            c.reject(e)
        }
        return c.promise
    }
}

async function main() {
    try {
        const addr = ':9090'
        const c = await Conn.connect(addr)
        console.log(`connect success: ${addr}`)
        try {
            // 寫入數據並接收
            for (let i = 0; i < 10; i++) {
                const msg = `message ${i}`
                await c.write(msg)
                const recv = await c.read()
                console.log(`recv: ${recv}`)
            }

            // 等待接收數據或關閉
            while (true) {
                const recv = await c.read()
                console.log(`recv: ${recv}`)
            }
        } finally {
            c.close()
        }
    } catch (e) {
        console.log(e)
    }
}
main()
```