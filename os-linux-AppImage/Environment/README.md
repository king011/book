# [環境變量](https://docs.appimage.org/packaging-guide/environment-variables.html)

AppImage 提供了一些環境變量供使用，它依據 AppImage 註冊類型不同包含了不同環境變量

# [Type 1](https://docs.appimage.org/packaging-guide/environment-variables.html#type-1-appimage-runtime)

舊版本的 AppImage 提供的環境變量

* **APPIMAGE** 被執行的 AppImage 絕對路徑(已經解析符號鏈接)
* **APPDIR** ISO9660 鏡像掛載在真實系統中的檔案夾路徑
* **OWD** 執行 AppImage 時的工作路徑

# [Type 2](https://docs.appimage.org/packaging-guide/environment-variables.html#type-2-appimage-runtime)

最小的格式包含的環境變量

* **APPIMAGE** 被執行的 AppImage 絕對路徑(已經解析符號鏈接)
* **APPDIR** SquashFS 鏡像掛載在真實系統中的檔案夾路徑
* **OWD** 執行 AppImage 時的工作路徑
* **ARGV0** 被執行的 AppImage 的路徑或名稱(類似 bash 的 $0)
