# console ui

deno 很適合用作系統腳本來替代 bash 之類的。用參數解析可以很好的開發一個 command program。但有些可能需要做一些簡單的 console ui 更任意讓用戶了解程序執行情況，例如爲並且下載的三個任務分別顯示下載進度

# clearScreen

下面實現的 clearScreen 函數可以將控制檯的屏幕內容清空，以便可以在一個乾淨的 屏幕中構建你的 ui 界面

```
export enum ClearMode {
  /**
   * 默認的系統操作，通常是把屏幕翻頁
   */
  fast,
  /**
   * 只是將光標移動到屏幕左上角，不會清空屏幕內容
   */
  move,
  /**
   * 先將光標移動到屏幕左上角，然後使用 ' ' 填充整個屏幕，最後在將光標移動到屏幕左上角
   */
  fill,
}
let _fast: undefined | Uint8Array;
let _move: undefined | Uint8Array;
let _fill: undefined | { columns: number; rows: number; data: Uint8Array };

/**
 * 清空終端屏幕
 * @param mode
 * @returns
 */
export function clearScreen(mode = ClearMode.fast) {
  switch (mode) {
    case ClearMode.fast:
      if (!_fast) {
        _fast = new TextEncoder().encode("\x1b[2J");
      }
      Deno.stdout.writeSync(_fast);
      return;
    case ClearMode.move:
      if (!_move) {
        _move = new TextEncoder().encode("\x1b[0f");
      }
      Deno.stdout.writeSync(_move);
      return;
    case ClearMode.fill:
      {
        const { columns, rows } = Deno.consoleSize();
        if (!_fill || _fill.columns != columns || _fill.rows != rows) {
          _fill = {
            columns: columns,
            rows: rows,
            data: new TextEncoder().encode(
              "\x1b[0f" + "".padEnd(columns * rows, " ") + "\x1b[0f",
            ),
          };
        }
        Deno.stdout.writeSync(_fill.data);
      }
      return;
    default:
      throw new Error(`unknow clearScreen mode: ${mode}`);
  }
}
```

# Console

下面的 class Console 它可以指定一個 fps 然後自動調用你的渲染函數來構建 ui

```
export type RenderCallback = (ms: number) => void | Promise<void>;
export interface ConsoleOptions {
  /**
   * 每秒渲染多少幀
   * @default 24
   */
  fps?: number;
  /**
   * 如何清空屏幕，默認不清空
   */
  clear?: ClearMode;

  /**
   * 渲染回調函數
   */
  render?: RenderCallback;
  /**
   * 是否打印調試信息
   */
  debug?: boolean;
}

/**
 * 控制檯
 */
export class Console {
  private fps_: number;
  get fps() {
    return this.fps_;
  }
  clear?: ClearMode;
  debug: boolean;

  private render_?: RenderCallback;
  constructor(opts?: ConsoleOptions) {
    const fps = opts?.fps ?? 24;
    if (!Number.isSafeInteger(fps) || fps < 1 || fps > 120) {
      throw new Error(`Console fps invalid: ${fps}`);
    }
    this.fps_ = fps;
    this.clear = opts?.clear;
    this.debug = opts?.debug ? true : false;
    this.render_ = opts?.render;
  }
  private state_ = -1;
  get isClosed() {
    return this.state_ == 1;
  }
  get isNotClosed() {
    return this.state_ != 1;
  }
  close() {
    if (this.state_ == 1) {
      return;
    }
    this.state_ = 1;
  }
  serve(): Promise<void> {
    const state = this.state_;
    switch (state) {
      case -1:
        break;
      case 0:
        throw new Error(`Console already serve`);
      case 1:
        throw new Error(`Console already closed`);
      default:
        throw new Error(`Console unknow state: ${state}`);
    }
    return this._serve();
  }
  private async _serve(): Promise<void> {
    const ms = 1000 / this.fps_;
    let at = 0;
    let last = 0;
    let duration: number;
    let used: number;

    let fps = 0;
    let fpsStart = Date.now();
    let fpsEnd = 0;
    let fpsCount = 0;
    while (this.state_ != 1) {
      at = at == 0 ? fpsStart : Date.now();
      duration = last == 0 ? 0 : at - last;
      try {
        await this._render(duration);
      } catch (e) {
        console.warn(`console render fail:`, e);
      }

      fpsEnd = Date.now();
      used = fpsEnd - at;
      last = at;
      if (fpsEnd != 0 && fpsEnd - fpsStart >= 1000) {
        fpsStart = fpsEnd;
        fpsCount = fps + 1;
        fps = 0;
      } else {
        fps++;
      }
      if (this.debug) {
        console.log(
          `fps:${fpsCount} render: ${used / 1000}s duration: ${
            duration / 1000
          }s`,
        );
      }
      if (used < ms) {
        await new Promise<void>((resolve) => setTimeout(resolve, ms - used));
      }
    }
  }
  private async _render(ms: number) {
    const clear = this.clear;
    const render = this.render_;
    if (clear) {
      clearScreen(clear);
    }
    if (render) {
      await render(ms);
    }
  }
}
```
