# NVIDIA

對於 NVIDIA 顯卡(不低於 GTX650) 可以在 GeForce Experience 中打開 GAMESTREAM 即可，N卡串流不止可以串流遊戲還可以運行任意程式(比如在windows系統上 遠程串流運行 **C:\Windows\System32\mstsc.exe** 就可以當作遠程桌面使用，相對 rdp 可以完整運行 3d 功能 但比 rdp更佔寬帶)，N卡串流是個人用戶目前最完善的串流方案


1. 在運行遊戲的機器上安裝 GeForce Experience 並在設定中啓用 **GAMESTREAM** 之後在同個設定頁面添加要遠端運行的遊戲和應用程式

	![](assets/2.jpg)

1. 在路由中設定端口映射將 GAMESTREAM 串流端口暴露給外網

	* UDP: 47998, 47999, 48000, 48002, 48010
	* TCP: 47984, 47989, 48010

1. 使用 [moonlight](https://github.com/moonlight-stream) 程式，輸入主機 ip 連接串流遊玩

> 首次連接 GAMESTREAM 的設備需要在 運行遊戲的機器上輸入確認碼進行安全驗證，你沒法同個 windows自帶的遠程桌面完成這一操作(rdp 會使系統切換到集成顯卡而使N卡串流失敗 )，可以通過其它一些遠程方案來解決（例如 [https://github.com/powerpuffpenguin/webpc](https://github.com/powerpuffpenguin/webpc)）。

# GeForce Experience
不要更新 GeForce Experience 最新版本的已經沒有 SHIELD 選項了，你可以下載本喵保存的老版本的安裝包

[GeForce\_Experience\_v3.28.0.417.exe](https://drive.google.com/file/d/1tHBRUzd3xzNl9xTgIa-OketZxVhiDj24/view?usp=drive_link)

```
c8e6afce8c01984df837102043675c8b MD5
8dddb758d45f0f46bc2a9d71294a3625ea0c7f53174616b9935f8ac5064ad57e SHA256
```