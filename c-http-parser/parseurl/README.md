# struct http\_parser\_url

URL 是 URI 的一種特定類型 它由如下圖所示的七個部分組成

![assets/uri.png](assets/uri.png)

struct http\_parser\_url 保存了這些信息

```c
struct http_parser_url {
  uint16_t field_set;           /* Bitmask of (1 << UF_*) values */
  uint16_t port;                /* Converted UF_PORT string */

  struct {
    uint16_t off;               /* Offset into buffer in which field starts */
    uint16_t len;               /* Length of run in buffer */
  } field_data[UF_MAX];
};
```

# Example

```c
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "http_parser.h"

// 定義一個 測試用的 url 字符串
#define URL_STR "http://king@my.dev.com/math/add?one=1&two=2#fragment"

void dump_url(const char *url, struct http_parser_url *u);
void dump_name(const char *url, struct http_parser_url *u, const char *name, const uint16_t index);
int main(int argc, char *argv[])
{
    // 初始化 url
    struct http_parser_url u;
    http_parser_url_init(&u);

    // 解析 url 地址
    if (http_parser_parse_url(URL_STR, strlen(URL_STR), 0, &u))
    {
        puts("parse error");
        return 0;
    }

    dump_url(URL_STR, &u);
    return 0;
}
void dump_url(const char *url, struct http_parser_url *u)
{
    dump_name(url, u, "schema", UF_SCHEMA);
    dump_name(url, u, "userinfo", UF_USERINFO);
    dump_name(url, u, "host", UF_HOST);
    dump_name(url, u, "port", UF_PORT);
    dump_name(url, u, "path", UF_PATH);
    dump_name(url, u, "query", UF_QUERY);
    dump_name(url, u, "fragement", UF_FRAGMENT);
}
void dump_name(const char *url, struct http_parser_url *u, const char *name, const uint16_t index)
{
    if (u->field_set & (1 << index))
    {
        printf("%s: %.*s\n", name,
               u->field_data[index].len, url + u->field_data[index].off);
    }
    else
    {
        if (index == UF_PORT && (u->field_set & (1 << UF_SCHEMA)))
        {
            // port 不會被自動設置到 默認值
            assert(!u->port);

            // 識別 http 默認端口
            if (4 == u->field_data[UF_SCHEMA].len && !memcmp("http", url + u->field_data[index].off, 4))
            {
                printf("%s: use default 80\n", name);
                return;
            }
            // 識別 https 默認端口
            if (5 == u->field_data[UF_SCHEMA].len && !memcmp("https", url + u->field_data[index].off, 5))
            {
                printf("%s: use default 443\n", name);
                return;
            }
        }

        printf("%s: <unset>\n", name);
    }
}
```