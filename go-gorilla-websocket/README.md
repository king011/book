# github.com/gorilla/websocket

github.com/gorilla/websocket 提供了 相比 golang.org/x/net/websocket 更好的效率和對websocket更多的擴展能力

```sh
go get -u -v github.com/gorilla/websocket
```

```
import "github.com/gorilla/websocket"
```

* 源碼 [https://github.com/gorilla/websocket](https://github.com/gorilla/websocket)
* 官網 [https://www.gorillatoolkit.org/pkg/websocket](https://www.gorillatoolkit.org/pkg/websocket)