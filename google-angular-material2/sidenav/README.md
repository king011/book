# MatSidenavModule

MatSidenavModule 提供了一個 在 左/右 邊的 側邊欄 當然也可以將其 隱藏 只在用戶 需要時 顯示出來

```typescript
import { MatSidenavModule } from '@angular/material/sidenav';
```

```html
<mat-sidenav-container>
  <mat-sidenav #sideNav>
    側邊欄
  </mat-sidenav>
 
  <mat-sidenav-content>
      <button mat-button (click)="sideNav.toggle()">切換狀態</button>
    <router-outlet></router-outlet>
  </mat-sidenav-content>
</mat-sidenav-container>
```

> 最多只能有 兩個側邊欄 分別位於不同位置


# 方法 屬性 事件

## open\(\) close\(\) toggle\(\)

MatSidenav 提供了 open\(\) close\(\) toggle\(\) 三個 方法 來 顯示 隱藏 切換 側邊欄  
這 三個 方法 都會 返回 一個 Promise 用以 獲取 顯示狀態

```typescript
  toggleSideNav(sideNav: MatSidenav) {
    sideNav.toggle().then((result: MatDrawerToggleResult) => {
      console.log(result);
      console.log(`選單狀態：${result.type}`);
    });
  }
```

## opened closed 事件

MatSidenav 還 提供了 opened closed 兩個 output 事件 可以通過 監聽者兩個事件 獲取現在 狀態

```html
<mat-sidenav #sideNav (opened)="opened()" (closed)="closed()">
```

## mode 屬性

mode 屬性 確定 了 如何 顯示 側邊欄 默認 over

* over 預設值 SideNav會浮動在畫面之上 背後會出現一個灰底的backdrop 點擊SideNav以外的地方\(或按下ESC\)會隱藏起來
* push 跟over類似 但顯示的時候會把畫面往另外一個方向推 同時也會有一個灰底的backdrop 在螢幕較大的裝置時可以同時瀏覽SideNav和選單 但在行動裝置等小螢幕上則比較沒有感覺
* side 效果類似push 但不會出現灰底的backdrop 因此可以同時操作主要的content畫面以及SideNav的內容

## opened 屬性

使用 opened 屬性 也可以 控制 顯示狀態

```html
<mat-sidenav opened="true" mode="side"></mat-sidenav>
```

## position 屬性

position 屬性 確定了 顯示位置

* start 開始\(通常是頁面左邊\)
* end 結束\(通常是頁面右邊\)

```html
  <mat-sidenav opened="true" mode="side">
    <div>我是左邊選單</div>
  </mat-sidenav>
 
  <mat-sidenav opened="true" mode="side" position="end">
    <div>我是右邊選單</div>
  </mat-sidenav>
```

## disableClose 屬性

在 mode 為 over/push 時 默認 esc 會 自動 隱藏 sidenav  
加上 disableClose 屬性 可以 禁用這一行為

```html
  <mat-sidenav #sideNav mode="over" disableClose>
    <div>我是左邊選單</div>
    <div>
      <button mat-raised-button color="warn" (click)="toggleSideNav(sideNav)">切換選單狀態</button>
    </div>
  </mat-sidenav>
```

# toolbar

當有一個toolbar在上層時 預設SideNav現時不會擋住toolbar  
這時可以設定fixedInViewport="true" 讓SideNav能夠顯示在Toolbar之上

```html
<mat-toolbar>我是Toolbar</mat-toolbar>
<mat-sidenav-container>
  <mat-sidenav #sideNav mode="over"  fixedInViewport="true" fixedTopGap="20" fixedBottomGap="20">
    <div>我是左邊選單</div>
    <div>
      <button mat-raised-button color="warn" (click)="toggleSideNav(sideNav)">切換選單狀態</button>
    </div>
  </mat-sidenav>
  ...
</mat-sidenav-container>
```

> 設定 fixedTopGap 和 fixedBottomGap 可以保留一定程度的上下空間


# MatDrawer

MatDrawer 功能用戶 基本同 MatSidenav  
只是 MatDrawer 通常 用在 組件間 而非 整個 頁面 且 也不支持 fixedInViewport 屬性

```html
<mat-drawer-container style="height:100px;border: 1px solid black">
  <mat-drawer mode="side" opened="true">Drawer Side</mat-drawer>
  <mat-drawer-content>Content</mat-drawer-content>
</mat-drawer-container>
```