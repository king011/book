# 命令行啓動模擬器

```bash
# 列出模擬器 名稱列表
emulator -list-avds

# 啓動模擬器
emulator -avd avd_name [ {-option [value]} … ]
```

# 發送模擬器控制檯命令

1. 執行 `telnet localhost console-port` **console-port** 是模擬器端口 會顯示在 模擬器窗口標題
2. 執行 `auth auth_token` **auth\_token** 是 **~/.emulator\_console\_auth\_token**  檔案記錄的 身份令牌
3. 執行 `help` 或 `help command` 查詢命令用法