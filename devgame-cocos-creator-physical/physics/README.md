# [物理系統](http://docs.cocos.com/creator/manual/zh/physics/physics/)

creator 使用 box2d 作爲 內部 物理系統

莫熱 creator 沒有 啓用 物理系統 需要在腳本中 打開

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        // 返回 物理系統
        const manager = cc.director.getPhysicsManager()
        // 啓用 物理系統
        manager.enabled = true

        // 繪製 各種調試信息
        const drawBits = cc.PhysicsManager.DrawBits as any
        manager.debugDrawFlags = drawBits.e_aabbBit |
            drawBits.e_pairBit |
            drawBits.e_centerOfMassBit |
            drawBits.e_jointBit |
            drawBits.e_shapeBit
        // 關閉 所有 繪製的 調試信息
        // manager.debugDrawFlags = 0
    }
}
```

# 物理單位到 像素單位 轉換

box2d 使用 米-千克-秒(MDS) 單位置 creator 則將 米換算爲 像素 只讀屬性 cc.PhysicsManager.PTM_RATIO 記錄了此值 目前是 32(既 32像素 爲 1米)

# 設置重力

重力默認爲 (0, -320) 既 像素/秒^2 換算到 box2d爲 （0，-10）米/秒^2

```
#info={"name":"設置 重力爲0","noline":true}
cc.director.getPhysicsManager().gravity = cc.v2()
```
```
#info={"name":"設置 重力 20米","noline":true}
cc.director.getPhysicsManager().gravity = cc.v2(0, -640)
```

# 設置物理步長

物理系統按照一個 固定的步長來更新 物理世界 默認步長是 遊戲幀率 1/framerate

提供 降低 物理步長 可以 減少消耗

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        // 返回 物理系統
        const manager = cc.director.getPhysicsManager() as any
        // 啓用 物理系統
        manager.enabled = true

        // 開啓物理步長設置
        manager.enabledAccumulator = true

        // 物理步長 默認 FIXED_TIME_STEP 是 1/60
        manager.FIXED_TIME_STEP = 1 / 30

        // 每次更新物理系統處理速度的迭代次數 默認爲 10
        manager.VELOCITY_ITERATIONS = 8

        // 每次更新物理系統處理位置的迭代次數 默認爲 10
        manager.POSITION_ITERATIONS = 8
    }
}
```

> 降低物理步長和各屬性的迭代次數 都會降低物理檢測頻率 這意味着 更可能發生剛體穿透的 情況
> 

# 查詢 物體

物理系統 提供了 多個 方法 高效的 查找 某個區域下有 哪些物體

# 點測試

點測試將測試是否有碰撞體 位與此點下 如果有 則返回

```
const collider: cc.PhysicsCollider = cc.director.getPhysicsManager().testPoint(point)
```

> 如果有 多個 滿足 只會返回一個 隨機的 碰撞體

# 矩形測試

矩形測試指定一個世界座標系下的 矩形 如果一個 碰撞體的包圍盒 與此 矩形 有重疊 則將其 加入到 返回列表中

```
const collider: Array<cc.PhysicsCollider> = cc.director.getPhysicsManager().testAABB(rect)
```
# 射線測試

射線測試 用來檢測 給定的線段 穿過哪些碰撞體

還可以獲取到碰撞體在線段穿過碰撞體的哪個點的 法線向量和其它信息

```
const results: Array<cc.PhysicsRayCastResult> = cc.director.getPhysicsManager().rayCast(p1, p2, type)

for (let i = 0; i < results.length; i++) {
		const result = results[i]
		const collider = result.collider
		const point = result.point
		const normal = result.normal
		const fraction = result.fraction
}
```

box2d射線檢測 不是從射線起點到 最近物體開始檢測的 所以 不能 保證 檢測結果的順序

射線檢測的最後一個參數 指定 檢測類型 決定了 creator 是否爲 檢測結果 排序 支持如下值

* cc.RayCastType.Any 最快

  檢測射線路徑上任意碰撞體 一旦檢測到 任意碰撞體 立刻結束 檢測
	
* cc.RayCastType.Closest 稍慢 默認值

  檢測射線路徑上最近的碰撞體
	
* cc.RayCastType.All 慢

  檢測射線路徑上的所有 碰撞體 檢測結果順序是不固定的

* cc.RayCastType.AllClosest 最慢

  檢測射線路徑上所有碰撞體 但會對值進行刪選 只返回每個 碰撞體距離射線起點最近的那個點的相關信息
	
	
**射線檢測結果 包含了如下信息**

* collider:cc.PhysicsCollider

  指定射線穿過的是那個碰撞體
	
* point:cc.Vec2

  指定射線與穿過的碰撞體在哪一點相交
	
* normal:cc.Vec2

  指定碰撞體在相交點的表面的法線向量
	
* fraction:number

  指定相交點在射線上的分數
	
![](assets/raycasting-output.png)

