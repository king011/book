# libtomcrypt

libtomcrypt 是一個純 c 實現的開源加密解密庫

* 官網 [https://www.libtom.net/LibTomCrypt/](https://www.libtom.net/LibTomCrypt/)
* 源碼 [https://github.com/libtom/libtomcrypt](https://github.com/libtom/libtomcrypt)