# [網路](http://docs.libuv.org/en/v1.x/guide/networking.html)

libuv 中的網路與直接使用 BSD 套接字接口區別不大，有些事情更容易，並且都是非阻塞的但概念保存不變。此外 libuv 提供實用函數來抽象煩人的 重複的 和 低級別的任務，例如使用 BSD 套接字結構設置套接字 DNS
DNS 查詢 和 調整各種套接字參數

# TCP

tcp 是面向連接的流協議 libuv 提供 uv\_tcp\_t 來處理

## server

1. 使用 uv\_tcp\_init 初始化句柄
2. 使用 uv\_tcp\_bind 綁定地址
3. 使用 uv\_listen 以在客戶端建立新連接時回調
4. 使用 uv\_accept 接受連接
5. 使用 streams 操作與客戶端 通信

```
#include <iostream>
#include <uv.h>
#define BUFFER_SIZE (1024 * 32)
#define LISTEN_PORT 9000
#define LISTEN_ADDR "0.0.0.0"
typedef struct
{
    uv_write_t req;
    uv_buf_t buf;
} write_req_t;
void free_write_req(uv_write_t *req)
{
    write_req_t *wr = (write_req_t *)req;
    free(wr->buf.base);
    free(wr);
}
void on_close(uv_handle_t *handle)
{
    free(handle);
}
void alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf)
{
    buf->base = (char *)malloc(suggested_size);
    buf->len = suggested_size;
}
void echo_write(uv_write_t *req, int status)
{
    if (status)
    {
        fprintf(stderr, "Write error %s\n", uv_strerror(status));
    }
    free_write_req(req);
}
void echo_read(uv_stream_t *client, ssize_t nread, const uv_buf_t *buf)
{
    if (nread > 0)
    {
        write_req_t *req = (write_req_t *)malloc(sizeof(write_req_t));
        req->buf = uv_buf_init(buf->base, nread);
        uv_write((uv_write_t *)req, client, &req->buf, 1, echo_write);
        return;
    }
    if (nread < 0)
    {
        if (nread != UV_EOF)
        {
            fprintf(stderr, "Read error %s\n", uv_err_name(nread));
        }
        uv_close((uv_handle_t *)client, on_close);
    }
    free(buf->base);
}
void on_accept(uv_stream_t *server, int status)
{
    if (status < 0)
    {
        fprintf(stderr, "New connection error %s\n", uv_strerror(status));
        // error!
        return;
    }

    // 接受新連接
    uv_tcp_t *client = (uv_tcp_t *)malloc(sizeof(uv_tcp_t));
    uv_tcp_init(server->loop, client);
    if (uv_accept(server, (uv_stream_t *)client) == 0)
    {
        uv_read_start((uv_stream_t *)client, alloc_buffer, echo_read);
    }
    else
    {
        uv_close((uv_handle_t *)client, on_close);
    }
}
int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();

    // 初始化句柄
    uv_tcp_t server;
    uv_tcp_init(loop, &server);
    struct sockaddr_in addr;
    int err = uv_ip4_addr(LISTEN_ADDR, LISTEN_PORT, &addr);
    if (err)
    {
        std::cout << uv_strerror(err) << std::endl;
        return 1;
    }
    // 綁定地址
    err = uv_tcp_bind(&server, (const struct sockaddr *)&addr, 0);
    if (err)
    {
        fprintf(stderr, "Bind error %s\n", uv_strerror(err));
        return 1;
    }
    std::cout << "listen at " << LISTEN_ADDR << ":" << LISTEN_PORT << std::endl;
    // 監聽端口
    err = uv_listen((uv_stream_t *)&server, 128, on_accept);
    if (err)
    {
        fprintf(stderr, "Listen error %s\n", uv_strerror(err));
        return 1;
    }

    return uv_run(loop, UV_RUN_DEFAULT);
}
```

## client

```
uv_tcp_t* socket = (uv_tcp_t*)malloc(sizeof(uv_tcp_t));
uv_tcp_init(loop, socket);

uv_connect_t* connect = (uv_connect_t*)malloc(sizeof(uv_connect_t));

struct sockaddr_in dest;
uv_ip4_addr("127.0.0.1", 80, &dest);

uv_tcp_connect(connect, socket, (const struct sockaddr*)&dest, on_connect);
```