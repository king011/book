# [Components](https://docs.flame-engine.org/1.16.0/flame/components.html)

![](assets/component_tree.png)

所有元件都繼承自 **abstract class Component**，並且所有元件都可以將其他元件作為子元件。 這是我們所謂的 Flame 組件系統（簡稱 FCS）的基礎

可以使用 add(Component c) 方法或直接在建構函式中加入子項

```
void main() {
  final component1 = Component(children: [Component(), Component()]);
  final component2 = Component();
  component2.add(Component());
  component2.addAll([Component(), Component()]);
}
```

這裡的 Component() 當然可以是 Component 的任何子類別

每個元件都有一些您可以選擇實作的方法，這些方法由 FlameGame 類別使用

## [Lifecycle](https://docs.flame-engine.org/1.16.0/flame/components.html#component-lifecycle)

![](assets/lifecycle.png)

* **onGameResize** 每當屏幕調整大小時都會調用 onGameResize 方法，並且在通過 **add addAll** 方法將組件添加到遊戲之前(在 onMount 之前)會被調用依次
* **onParentResize** 與 onGameResize 類似，當元件安裝到元件樹時以及目前元件的父元件更改其大小時也會呼叫它
* **onRemove** 方法可以被重寫以在組件從遊戲樹中移除之前運行的代碼（在 onGameResize 之後運行）
* **onLoad** 方法可以被重寫以運行組件的異步初始化代碼(只會執行一次，在 **onGameResize** **onMount** 之前)
* **onMount** 方法可以被重寫以運行異步初始化代碼，每次將組件添加到新父級時都會運行此代碼，此方法在 **onGameResize** **onLoad** 之後運行，但在組件被包含在父組件列表之前運行
* **onChildrenChanged** 每當將子級新增至父級或從父級中刪除時(包括子級變更其父級)，都會呼叫此方法。它的參數包含目標子項及其所經歷的變更類型（新增或刪除）

組件生命週期狀態可以透過一系列 getter 來檢查:

* **isLoaded**: 返回一個包含目前載入狀態的 bool 值
* **loaded**: 返回一個 future 它將在載入完成後 complete
* **isMounted**: 返回組件是否被安裝到父級
* **mounted**: 返回一個 future 它將在安裝到父級後 complete
* **isRemoved**: 返回一個包含目前刪除狀態的 bool 值
* **removed**: 返回一個 future 它將在組件被刪除後 complete

## [Priority](https://docs.flame-engine.org/1.16.0/flame/components.html#priority)

在 Flame 中，每個元件都具有 int priority 屬性，該屬性決定該元件在其父元件的子元件中的排序順序。 在其他語言和框架中，這有時稱為 z-index。 優先順序設定得越高，元件在螢幕上顯示的位置就越近，因為它將渲染在先前渲染的任何優先順序較低的元件之上。

例如，如果新增兩個元件並將其中一個優先權設為 1，則該元件將呈現在另一個元件之上（如果它們重疊），因為預設優先權為 0

所有元件都將優先權作為命名參數，因此如果您在編譯時知道元件的優先權，則可以將其傳遞給建構函式

```
class MyGame extends FlameGame {
  @override
  void onLoad() {
    final myComponent = PositionComponent(priority: 5);
    add(myComponent);
  }
}
```

要更新元件的優先級，您必須將其設定為新值，例如 component.priority = 2，並且它將在渲染階段之前的當前tick中更新

在下面的範例中，我們首先使用優先權 1 初始化元件，然後當使用者點擊該元件時，我們將其優先權變更為 2：

```
class MyComponent extends PositionComponent with TapCallbacks {

  MyComponent() : super(priority: 1);

  @override
  void onTapDown(TapDownEvent event) {
    priority = 2;
  }
}
```

## [組合組件](https://docs.flame-engine.org/1.16.0/flame/components.html#composability-of-components)

有時將其它組件包裝在自己的組件中很有用。例如，通過層次結構對視覺組件進行分組。你可以通過向任何組件(例如 PositionComponent) 添加子組件來實現此目的。當每次更新和渲染父組件時，組件上的所有子組件都以相同的條件渲染和更新

如果每次更新和渲染父元件時，元件上都有子元件，則所有子元件都會以相同的條件進行渲染和更新

使用範例，其中兩個組件的可見性由包裝器處理：

```
class GameOverPanel extends PositionComponent with HasGameRef<MyGame> {
  bool visible = false;
  final Image spriteImage;

  GameOverPanel(this.spriteImage);
  @mustCallSuper
  @override
  Future<void> onLoad() async {
    await super.onLoad();
    final gameOverText =
        GameOverText(spriteImage); // GameOverText is a Component
    final gameOverButton =
        GameOverButton(spriteImage); // GameOverRestart is a SpriteComponent

    add(gameOverText);
    add(gameOverButton);
  }

  @override
  void render(Canvas canvas) {
    if (visible) {
    } // If not visible none of the children will be rendered
  }
}
```

有兩種方法可以將子組件新增到元件中。 首先，您有方法 add()、addAll() 和 addToParent()，可以在遊戲過程中隨時使用它們。 傳統上，子項將透過組件的 onLoad() 方法建立和添加，但在遊戲過程中添加新子項也很常見。

第二種方法是在元件的建構函式中使用children:參數。 這種方法更類似於標準 Flutter API：
```
class MyGame extends FlameGame {
  @override
  void onLoad() {
    add(
      PositionComponent(
        position: Vector2(30, 0),
        children: [
          HighScoreDisplay(),
          HitPointsDisplay(),
          FpsComponent(),
        ],
      ),
    );
  }
}
```

這兩種方法可以自由組合：首先新增建構函式中指定的子元件，然後再新增任何其他子元件。

請注意，透過任一方法新增的子項僅保證最終可用：在 loaded 和 mounted 之後。 我們只能保證它們將按照計劃新增的順序出現在子清單中

## [從組件訪問世界](https://docs.flame-engine.org/1.16.0/flame/components.html#access-to-the-world-from-a-component)

如果一個元件以 World 作為祖先並且需要存取該 World 對象，則可以使用 HasWorldReference mixin

```
class MyComponent extends Component with HasWorldReference<MyWorld>,
    TapCallbacks {
  @override
  void onTapDown(TapDownEvent info) {
    // world is of type MyWorld
    world.add(AnotherComponent());
  }
}
```

如果您嘗試從不具有正確類型的 World 祖先的元件存取 world，則會拋出斷言錯誤

## [確保組件有給定的父組件](https://docs.flame-engine.org/1.16.0/flame/components.html#ensuring-a-component-has-a-given-parent)

當元件需要新增到特定的父類型時，ParentIsA mixin 可用於強制執行強類型父類型

```
class MyComponent extends Component with ParentIsA<MyParentComponent> {
  @override
  void onLoad() {
    // parent is of type MyParentComponent
    print(parent.myValue);
  }
}
```

如果您嘗試將 MyComponent 新增至不是 MyParentComponent 的父級，則會引發斷言錯誤

## [確保組件具有給定的祖先](https://docs.flame-engine.org/1.16.0/flame/components.html#ensuring-a-component-has-a-given-ancestor)

當元件需要在元件樹中的某個位置具有特定的祖先類型時，可以使用 HasAncestor mixin 來強制執行該關係。

mixin 公開了給定類型的祖先欄位

```
class MyComponent extends Component with HasAncestor<MyAncestorComponent> {
  @override
  void onLoad() {
    // ancestor is of type MyAncestorComponent.
    print(ancestor.myValue);
  }
}
```

如果您嘗試將 MyComponent 新增至不包含 MyAncestorComponent 的樹中，則會引發斷言錯誤

## [Component Keys](https://docs.flame-engine.org/1.16.0/flame/components.html#component-keys)

組件可以有一個標識鍵，允許從組件樹的任何點檢索它們

要使用鍵註冊元件，只需將鍵傳遞給元件建構函數的 key 參數即可:

```
final myComponent = Component(
  key: ComponentKey.named('player'),
);
```

然後，在組件樹的不同點檢索它：

```
flameGame.findByKey(ComponentKey.named('player'));
```

有兩種類型的密鑰：唯一密鑰和命名密鑰。 唯一鍵基於鍵實例的相等性，這意味著：

```
final key = ComponentKey.unique();
final key2 = key;
print(key == key2); // true
print(key == ComponentKey.unique()); // false
```

命名的基於它收到的名稱，因此：

```
final key1 = ComponentKey.named('player');
final key2 = ComponentKey.named('player');
print(key1 == key2); // true
```

當使用命名鍵時，findByKeyName 幫助器也可用於擷取元件
```
flameGame.findByKeyName('player');
```

## [查詢子組件](https://docs.flame-engine.org/1.16.0/flame/components.html#querying-child-components)

已添加的組件存儲在 **final ComponentSet children** 中，可以使用 ComponentSet 提供的 query 方法查詢子組件。預設情況下，children 集中的 strictMode 為 false，但如果將其設為 true，則必須先使用children.register 註冊查詢，然後才能使用查詢

> 如果您在編譯時知道稍後將執行特定類型的查詢，則建議註冊該查詢，無論 strictMode 設定為 true 或 false，因為可以從中獲得一些效能優勢


```
@mustCallSuper
@override
Future<void> onLoad() async {
	await super.onLoad();
	children.register<TuxComponent>();

	add(TuxComponent());
}

@override
void update(double dt) {
	final components = children.query<TuxComponent>();
	debugPrint('${components.length}');
}
```

## [查詢畫面上特定點的組件](https://docs.flame-engine.org/1.16.0/flame/components.html#querying-components-at-a-specific-point-on-the-screen)

ComponentsAtPoint() 方法可讓您檢查在螢幕上的某個點渲染了哪些元件。 傳回的值是元件的可迭代對象，但您也可以透過提供可寫入的 List&lt;Vector2&gt; 作為第二個參數來取得每個元件的局部座標空間中的初始點的座標。

iterable 依照從前到後的順序檢索元件，也就是首先是前面的元件，然後是後面的元件

```
void onDragUpdate(DragUpdateInfo info) {
  game.componentsAtPoint(info.widget).forEach((component) {
    if (component is DropTarget) {
      component.highlight();
    }
  });
}
```

## [定位類型](https://docs.flame-engine.org/1.16.0/flame/components.html#positiontype)

> 如果您使用的是 CameraComponent，則不應使用 PositionType，而應將元件直接新增至視口，例如，如果您想將它們用作 HUD

如果你想創建一個 HUD(平視顯示器) 或其它與遊戲坐標無關的組件，可以更改組件的 **positionType** 其被默認設置爲 **PositionType.game**

* **PositionType.game** 默認值 相對 攝像機 和 viewport
* **PositionType.viewport** 相對 viewport only(忽略攝像機)
* **PositionType.widget** 相對 Flutter Game Widget

大多數組件都會使用 **PositionType.game** 進行定位，因爲你希望它們相對 攝像機 和 viewport。但是很多時候你希望例如按鈕和文本總是顯示在屏幕上，無論你是否移動相機，此時可以使用 **PositionType.viewport**。在極少數情況下，當你不希望尊重相機或視口時，使用 **PositionType.widget** 來相對 widget 定位，例如 這可能用於控制 或 操縱杆，如果它們必須留在 viewport 則使用起來不符合人體工程學

> 請注意，只有當元件直接添加到根 FlameGame 而不是作為另一個元件的子元件時，才會遵守此設定

## [組件的可見性](https://docs.flame-engine.org/1.16.0/flame/components.html#visibility-of-components)

隱藏或顯示組件的建議方法通常是使用新增和刪除方法在樹中新增或刪除它

但是，從樹中新增和移除元件將觸發該元件的生命週期步驟（例如呼叫 onRemove 和 onMount）。 這也是一個非同步過程，如果您要快速連續刪除和新增元件，則需要注意確保元件在再次新增之前已完成刪除

```
/// Example of handling the removal and adding of a child component
/// in quick succession
void show() async {
  // Need to await the [removed] future first, just in case the
  // component is still in the process of being removed.
  await myChildComponent.removed;
  add(myChildComponent);
}

void hide() {
  remove(myChildComponent);
}
```

這些行為並不總是令人滿意的。

另一種顯示和隱藏元件的方法是使用 HasVisibility mixin，它可以用在從 Component 繼承的任何類別上。 這個 mixin 引進了 isVisible 屬性。 只需將 isVisible 設為 false 即可隱藏該元件，設為 true 即可再次顯示該元件，而無需將其從樹中刪除。 這會影響組件及其所有後代（子級）的可見性。

```
/// Example that implements HasVisibility
class MyComponent extends PositionComponent with HasVisibility {}

/// Usage of the isVisible property
final myComponent = MyComponent();
add(myComponent);

myComponent.isVisible = false;
```

mixin只影響元件是否渲染，不會影響其他行為。

> 重要的！ 即使元件不可見，它仍然在樹中，並將繼續接收對「更新」和所有其他生命週期事件的呼叫。 它仍然會響應輸入事件，並且仍然會與其他元件交互，例如碰撞檢測。

mixin 透過阻止 renderTree 方法來運作，因此，如果 renderTree 被重寫，則應包含對 isVisible 的手動檢查以保留此功能

```
class MyComponent extends PositionComponent with HasVisibility {

  @override
  void renderTree(Canvas canvas) {
    // Check for visibility
    if (isVisible) {
      // Custom code here

      // Continue rendering the tree
      super.renderTree(canvas);
    }
  }
}
```


# [PositionComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#positioncomponent)

此類表示屏幕上的定位對象，是浮動矩形或選擇精靈。如果將子項添加到其中，它還可以表示一組定位組件

PositionComponent 的基礎是 **position size scale angle anchor** 用於對組件做變換(transforms)

通常要實現自己的組件都從 **PositionComponent** 派生，但是如果你要創建自己的組件但以不同的方式處理定位則可以從 **Component** 派生

Component 被 flame_forge2d 中的 SpriteBodyComponent PositionBodyComponent BodyComponent 使用，因爲這些組件沒有相對屏幕的位置，而是相對於 Forge2D 世界的位置

## [Position](https://docs.flame-engine.org/1.16.0/flame/components.html#position)

position 只是一個 Vector2，它表示組件的錨點相對與其父級的位置。如果父級是 FlameGame 則表示相對 viewport

## [Size](https://docs.flame-engine.org/1.16.0/flame/components.html#size)

size 表示相機縮放級別爲 1.0 時組件的大小。大小與組件的父級無關

## [Scale](https://docs.flame-engine.org/1.16.0/flame/components.html#scale)

scale 是組件及其子組件應該被縮放的程度。由於它使用 Vector2 表示，你可以通過以相同量更改 x 和 y 以統一縮放，或者以不同量更改 x 和 y 以非均勻方式縮放

## [Angle](https://docs.flame-engine.org/1.16.0/flame/components.html#angle)

angle 是圍繞錨點的旋轉的弧度

## [Native Angle](https://docs.flame-engine.org/1.16.0/flame/components.html#native-angle)

一個弧度順時針測量，表示組件的預設方向。 它可用來定義角度為零時組件面向的方向。

當使基於精靈的組件查看特定目標時，它特別有用。 如果精靈的原始影像沒有面向上/北方向，則計算出的使組件看著目標的角度將需要一些偏移才能使其看起來正確。 對於這種情況，可以使用nativeAngle讓元件知道原始影像面向哪個方向

一個例子是指向東方的子彈圖像。 在這種情況下，nativeAngle 可以設定為 pi/2 弧度。 以下是一些常見的方向及其對應的原始角度值

| Direction | Native Angle | In degrees |
| -------- | -------- | -------- |
| Up/North     | 0     | 0     |
| Down/South     | pi or -pi     | 180 or -180     |
| Left/West     | -pi/2     | -90     |
| Right/East     | pi/2     | 90     |


## [Anchor](https://docs.flame-engine.org/1.16.0/flame/components.html#anchor)

錨點是組件上應定義位置和旋轉的位置（預設為 Anchor.topLeft）。 因此，如果將錨點設為 Anchor.center，則組件在螢幕上的位置將位於組件的中心，並且如果應用 angle，它將圍繞錨點旋轉，因此在本例中圍繞組件的中心旋轉。 您可以將其視為組件內 Flame“抓住”它的點。

當查詢元件的position或absolutePosition時，傳回的座標是該元件的錨點的座標。 如果您想要尋找元件的特定錨點的位置，而該錨點實際上不是該元件的錨點，則可以使用positionOfAnchor和absolutePositionOfAnchor方法。

```
final comp = PositionComponent(
  size: Vector2.all(20),
  anchor: Anchor.center,
);

// Returns (0,0)
final p1 = component.position;

// Returns (10, 10)
final p2 = component.positionOfAnchor(Anchor.bottomRight);
```

使用錨點時的一個常見陷阱是將其誤認為子組件的附著點。 例如，將父元件的錨點設為 Anchor.center 並不表示子元件將放置在父元件的中心

> 子元件的本地原點始終是其父元件的左上角，無論其錨點值為何

## [PositionComponent children](https://docs.flame-engine.org/1.16.0/flame/components.html#positioncomponent-children)

PositionComponent 的所有子級都會相對於父級進行變換，這意味著位置、角度和比例將相對於父級狀態。 因此，例如，如果您想將子級放置在父級的中心，您可以這樣做：

```
@override
void onLoad() {
  final parent = PositionComponent(
    position: Vector2(100, 100),
    size: Vector2(100, 100),
  );
  final child = PositionComponent(
    position: parent.size / 2,
    anchor: Anchor.center,
  );
  parent.add(child);
}
```

請記住，大多數在螢幕上呈現的元件都是 PositionComponent，因此該模式也可以用於 SpriteComponent 和 SpriteAnimationComponent 等


## [Render PositionComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#render-positioncomponent)

爲 PositionComponent 重寫 render 方法以渲染組件時，請記住從左上角(0,0)進行渲染。你的 render 方法不應該處理你的組件應該在屏幕上呈現的位置。使用 **position angle anchor** 來處理組件應該在哪裏以及如何渲染

如果想知道組件的邊界在屏幕上的哪個位置，可以使用 **toRect** 方法

如果想更改組件的渲染方向，可以使用 **renderFlipX renderFlipY** 在渲染期間反轉任何繪製到畫布上的內容(Canvas canvas)。這適用於所有 PositionComponent 對象，尤其適用於 SpriteComponent 和 SpriteAnimationComponent。例如 設置 component.renderFlipX = true 來鏡像水平渲染

如果您想要圍繞其中心翻轉組件，而不必將錨點更改為 Anchor.center，則可以使用 flipHorizontallyAroundCenter() 和 flipVerticallyAroundCenter()。

# [SpriteComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#spritecomponent)

PositionComponent 最常使用的子類是 SpriteComponent，它可以創建一個 Sprite

```
import 'package:flame/components/component.dart';

class MyGame extends FlameGame {
  late final SpriteComponent player;

  @override
  Future<void> onLoad() async {
    final sprite = await Sprite.load('player.png');
    final size = Vector2.all(128.0);
    final player = SpriteComponent(size: size, sprite: sprite);

    // Vector2(0.0, 0.0) by default, can also be set in the constructor
    player.position = Vector2(10, 20);

    // 0 by default, can also be set in the constructor
    player.angle = 0;

    // Adds the component
    add(player);
  }
}
```

# [SpriteAnimationComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#spriteanimationcomponent)

此類別用於表示具有在單一循環動畫中運行的精靈的組件

這將使用 3 個不同的圖像創建一個簡單的三幀動畫：

```
@override
Future<void> onLoad() async {
  final sprites = [0, 1, 2]
      .map((i) => Sprite.load('player_$i.png'));
  final animation = SpriteAnimation.spriteList(
    await Future.wait(sprites),
    stepTime: 0.01,
  );
  this.player = SpriteAnimationComponent(
    animation: animation,
    size: Vector2.all(64.0),
  );
}
```

如果你有一個 sprite sheet，你可以使用 SpriteAnimationData.sequenced 來創建(點擊 [Images > Animation](https://docs.flame-engine.org/1.16.0/flame/rendering/images.html#animation) 查看更多詳情)

```
@override
Future<void> onLoad() async {
  final size = Vector2.all(64.0);
  final data = SpriteAnimationData.sequenced(
    textureSize: size,
    amount: 2,
    stepTime: 0.1,
  );
  this.player = SpriteAnimationComponent.fromFrameData(
    await images.load('player.png'),
    data,
  );
}
```

如果你沒有使用 **FlameGame**，不要忘記這個組件需要 update,因爲動畫需要 ticked 來移動 frames

所有動畫元件內部都會維護一個 SpriteAnimationTicker，用於勾選 SpriteAnimation。 這允許多個元件共享同一個動畫物件:

```
final sprites = [/*You sprite list here*/];
final animation = SpriteAnimation.spriteList(sprites, stepTime: 0.01);

final animationTicker = SpriteAnimationTicker(animation);

// or alternatively, you can ask the animation object to create one for you.

final animationTicker = animation.createTicker(); // creates a new ticker

animationTicker.update(dt);
```

要監聽動畫何時完成（當它到達最後一幀並且不循環時），您可以使用animationTicker.completed:

```
await animationTicker.completed;

doSomething();

// or alternatively

animationTicker.completed.whenComplete(doSomething);
```

此外，SpriteAnimationTicker 還具有以下可選事件回呼：onStart、onFrame 和 onComplete。 若要監聽這些事件，您可以執行以下操作：

```
final animationTicker = SpriteAnimationTicker(animation)
  ..onStart = () {
    // Do something on start.
  };

final animationTicker = SpriteAnimationTicker(animation)
  ..onComplete = () {
    // Do something on completion.
  };

final animationTicker = SpriteAnimationTicker(animation)
  ..onFrame = (index) {
    if (index == 1) {
      // Do something for the second frame.
    }
  };
```
# [SpriteAnimationGroupComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#spriteanimationgroupcomponent)

SpriteAnimationGroupComponent 是 SpriteAnimationComponent 的簡單包裝器，它使用你的組件能夠保存多個動畫並在運行時更改當前播放的動畫

SpriteAnimationGroupComponent 與 SpriteAnimationComponent 的使用非常類似，但它不是使用單個動畫進行初始化，而是接收一個泛型 T 作爲 Key 一個 SpriteAnimation 作爲 Value 的 Map，以及當前動畫

```
enum RobotState {
  idle,
  running,
}

final running = await loadSpriteAnimation(/* omitted */);
final idle = await loadSpriteAnimation(/* omitted */);

final robot = SpriteAnimationGroupComponent<RobotState>(
  animations: {
    RobotState.running: running,
    RobotState.idle: idle,
  },
  current: RobotState.idle,
);

// Changes current animation to "running"
robot.current = RobotState.running;
```

由於該元件與多個 SpriteAnimations 一起使用，因此自然需要相同數量的動畫滾動條來使所有這些動畫滾動。 使用animationsTickers getter 存取包含每個動畫狀態的程式碼的地圖。 如果您想註冊 onStart、onComplete 和 onFrame 的回調，這會很有用

```
enum RobotState { idle, running, jump }

final running = await loadSpriteAnimation(/* omitted */);
final idle = await loadSpriteAnimation(/* omitted */);

final robot = SpriteAnimationGroupComponent<RobotState>(
  animations: {
    RobotState.running: running,
    RobotState.idle: idle,
  },
  current: RobotState.idle,
);

robot.animationTickers?[RobotState.running]?.onStart = () {
  // Do something on start of running animation.
};

robot.animationTickers?[RobotState.jump]?.onStart = () {
  // Do something on start of jump animation.
};

robot.animationTickers?[RobotState.jump]?.onComplete = () {
  // Do something on complete of jump animation.
};

robot.animationTickers?[RobotState.idle]?.onFrame = (currentIndex) {
  // Do something based on current frame index of idle animation.
};
```

# [SpriteGroupComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#spritegroupcomponent)

SpriteGroupComponent 非常類似 SpriteAnimationGroupComponent 但它對應的是 SpriteComponent

```
class PlayerComponent extends SpriteGroupComponent<ButtonState>
    with HasGameReference<SpriteGroupExample>, TapCallbacks {
  @override
  Future<void>? onLoad() async {
    final pressedSprite = await gameRef.loadSprite(/* omitted */);
    final unpressedSprite = await gameRef.loadSprite(/* omitted */);

    sprites = {
      ButtonState.pressed: pressedSprite,
      ButtonState.unpressed: unpressedSprite,
    };

    current = ButtonState.unpressed;
  }

  // tap methods handler omitted...
}
```

# [SpawnComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#spawncomponent)

該元件是一個非視覺元件，它在 SpawnComponent 的父級內部產生其他元件。 例如，如果您想在一個區域內隨機產生敵人或能力提升，那就太棒了。

SpawnComponent 採用一個用於建立新元件的工廠函數以及一個應在其中（或沿著其邊緣）產生元件的區域。

對於區域，您可以使用 Circle、Rectangle 或 Polygon 類，如果您只想沿著形狀的邊緣產生元件，請將 inside 參數設為 false（預設為 true）。

例如，這將在定義的圓圈內每 0.5 秒隨機產生 MyComponent 類型的新元件：

工廠函數採用 int 作為參數，它是正在產生的元件的索引，因此，如果已經產生了 4 個元件，則第 5 個元件的索引將為 4，因為索引從 0 開始

```
SpawnComponent(
  factory: (i) => MyComponent(size: Vector2(10, 20)),
  period: 0.5,
  area: Circle(Vector2(100, 200), 150),
);
```

如果您不希望生成速率是靜態的，則可以使用帶有 minPeriod 和 maxPeriod 參數的 SpawnComponent.periodRange 建構子。 在以下範例中，組件將在圓圈內隨機生成，每個新生成組件之間的時間間隔在 0.5 到 10 秒之間:

```
SpawnComponent.periodRange(
  factory: (i) => MyComponent(size: Vector2(10, 20)),
  minPeriod: 0.5,
  maxPeriod: 10,
  area: Circle(Vector2(100, 200), 150),
);
```

如果您想在工廠函數中自行設定位置，可以在建構函式中使用 set selfPositioning = true ，您將能夠自行設定位置並忽略區域參數:

```
SpawnComponent(
  factory: (i) =>
    MyComponent(position: Vector2(100, 200), size: Vector2(10, 20)),
  selfPositioning: true,
  period: 0.5,
);
```
# [SvgComponent](SpawnComponent)

此組件使用 Svg 類來實例化用於在遊戲中呈現 svg

需要添加 [flame_svg](https://pub.dev/packages/flame_svg) 包

```
final svg = await Svg.load('android.svg');
final android = SvgComponent.fromSvg(
  svg,
  position: Vector2.all(100),
  size: Vector2.all(100),
);
```

# [ParallaxComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#parallaxcomponent)

該組件可以通過繪製多個透明圖像來渲染具有深度的背景，其中每個圖像或動畫(ParallaxRenderer) 以不同的速度移動

當你看地平線並移動時，較近的物體似乎比遠處的物體移動得更快，此組件模擬這種效果以製作出更逼真的背景效果

你可以在 Game 的 onLoad 中 創建 ParallaxComponent
```
@override
Future<void> onLoad() async {
  final parallaxComponent = await loadParallaxComponent([
    ParallaxImageData('bg.png'),
    ParallaxImageData('trees.png'),
  ]);
  add(parallaxComponent);
}
```

ParallaxComponent 也可以透過實作 onLoad 方法來「載入自身」：

```
class MyParallaxComponent extends ParallaxComponent<MyGame> {
  @override
  Future<void> onLoad() async {
    parallax = await gameRef.loadParallax([
      ParallaxImageData('bg.png'),
      ParallaxImageData('trees.png'),
    ]);
  }
}

class MyGame extends FlameGame {
  @override
  void onLoad() {
    add(MyParallaxComponent());
  }
}
```

上面的例子創建來靜態的背景。如果想要移動視差，可以通過幾種不同的方式來實現

最簡單的方法是在 load helper 函數中設置命名的可選參數 **baseVelocity** 和 **velocityMultiplierDelta**。例如，如果想以更快的速度沿 x 軸移動背景圖，則圖顯得更近

```
@override
Future<void> onLoad() async {
  final parallaxComponent = await loadParallaxComponent(
    _dataList,
    baseVelocity: Vector2(20, 0),
    velocityMultiplierDelta: Vector2(1.8, 1.0),
  );
}
```
你也可以隨時設置 **baseSpeed** 和 **layerDelta** 例如 你的角色跳躍或你的遊戲加速

```
@override
void onLoad() {
  final parallax = parallaxComponent.parallax;
  parallax.baseSpeed = Vector2(100, 0);
  parallax.velocityMultiplierDelta = Vector2(2.0, 1.0);
}
```

默認情況下 圖像與左下角對齊，沿 x 軸重複並按比例縮放 以便圖像覆蓋屏幕高度。如果要更改此行爲，例如你不製作橫向卷軸遊戲，則可以爲每個 ParallaxRenderer 設置重複 對齊 和 填充參數 並將它們添加到 ParallaxLayers 然後將其傳遞給 ParallaxComponent 的構造函數

```
final images = [
  loadParallaxImage(
    'stars.jpg',
    repeat: ImageRepeat.repeat,
    alignment: Alignment.center,
    fill: LayerFill.width,
  ),
  loadParallaxImage(
    'planets.jpg',
    repeat: ImageRepeat.repeatY,
    alignment: Alignment.bottomLeft,
    fill: LayerFill.none,
  ),
  loadParallaxImage(
    'dust.jpg',
    repeat: ImageRepeat.repeatX,
    alignment: Alignment.topRight,
    fill: LayerFill.height,
  ),
];

final layers = images.map(
  (image) => ParallaxLayer(
    await image,
    velocityMultiplier: images.indexOf(image) * 2.0,
  )
);

final parallaxComponent = ParallaxComponent.fromParallax(
  Parallax(
    await Future.wait(layers),
    baseVelocity: Vector2(50, 0),
  ),
);
```

* stars.jpg 將在兩個軸上重複繪製，在中心對應並縮放以填充屏幕寬度
* planets.jpg 在 y 軸上重複，與屏幕左下角對齊且不縮放
* dust.jpg' 在 x 軸上重複，與右上角對齊並縮放以填充屏幕高度

一旦設置完成 ParallaxComponent 後 就可以想其它組件一樣使用 game.add 將其添加到組件列表

Parallax 檔案包含遊戲的擴展，其中添加了 loadParallax、loadParallaxLayer、loadParallaxImage 和 loadParallaxAnimation，以便它自動使用遊戲的圖像快取而不是全域圖像快取。 ParallaxComponent 檔案也是如此，但它提供了 loadParallaxComponent。

如果您想要全螢幕 ParallaxComponent，只需省略大小參數，它將採用遊戲的大小，當遊戲更改大小或方向時，它也會調整為全螢幕。

Flame提供了兩種ParallaxRenderer：ParallaxImage和ParallaxAnimation，ParallaxImage是靜態影像渲染器，ParallaxAnimation顧名思義是基於動畫和幀的渲染器。 也可以透過擴充 ParallaxRenderer 類別來建立自訂渲染器。

# [ShapeComponents](https://docs.flame-engine.org/1.16.0/flame/components.html#shapecomponents)

ShapeComponent 是一個基本組件，如果想在屏幕上繪製幾何形狀作爲組件可以使用它。ShapeComponent 派生自 PositionComponents，因此可以對其使用 effects，所有 ShapeComponent 都以 Paint 作爲參數，然後是定義特定組件形狀的參數，它還採用可以傳遞給 PositionComponent 的所有參數


> 這些形狀旨在作為一種工具，以比與碰撞檢測系統一起使用更通用的方式使用幾何形狀的工具，在碰撞檢測系統中您要使用 ShapeHitboxes


ShapeComponent 有三個子類實現

* PolygonComponent
* RectangleComponent
* CircleComponent

## [PolygonComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#polygoncomponent)

PolygonComponent 是透過在建構函式中提供一系列點（稱為頂點）來創建的。 該清單將轉換為具有一定大小的多邊形，該多邊形仍然可以縮放和旋轉。

例如，這將建立一個從 (50, 50) 到 (100, 100) 的正方形，其中心位於 (75, 75):

```
void main() {
  PolygonComponent([
    Vector2(100, 100),
    Vector2(100, 50),
    Vector2(50, 50),
    Vector2(50, 100),
  ]);
}
```


也可以使用相對頂點清單來建立 PolygonComponent，這些相對頂點是相對於給定大小（通常是目標父級的大小）定義的點。

例如，您可以建立一個菱形多邊形，如下所示：
```
void main() {
  PolygonComponent.relative(
    [
      Vector2(0.0, 1.0), // Middle of top wall
      Vector2(1.0, 0.0), // Middle of right wall
      Vector2(0.0, -1.0), // Middle of bottom wall
      Vector2(-1.0, 0.0), // Middle of left wall
    ],
    size: Vector2.all(100),
  );
}
```

範例中的頂點定義了 x 軸和 y 軸上從螢幕中心到邊緣的長度百分比，因此對於清單中的第一項 (Vector2(0.0, 1.0))，我們指向邊界框的頂壁，因為這裡的座標係是從多邊形的中心定義的。

![](assets/polygon_shape.png)

在影像中，您可以看到紫色箭頭形成的多邊形形狀是如何由紅色箭頭定義的。

請記得以逆時針方式定義清單（如果您認為在螢幕座標系中 y 軸翻轉，否則就是順時針）。

## [RectangleComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#rectanglecomponent)

矩形組件的建立方式與位置組件的建立方式非常相似，因為它也有邊界矩形。

例如這樣的事情：

```
void main() {
  RectangleComponent(
    position: Vector2(10.0, 15.0),
    size: Vector2.all(10),
    angle: pi/2,
    anchor: Anchor.center,
  );
}
```

Dart 也已經有一個很好的方法來創建矩形，該類別稱為 Rect，您可以使用 Rectangle.fromRect 工廠從 矩形 創建 Flame 矩形組件，就像設置 PolygonComponent 的頂點一樣，您的矩形將調整大小如果您使用此構造函數，則根據Rect 。

以下將建立一個 RectangleComponent，其左上角位於 (10, 10)，大小為 (100, 50):

```
void main() {
  RectangleComponent.fromRect(
    Rect.fromLTWH(10, 10, 100, 50),
  );
}
```

您也可以透過定義與預期父級大小的關係來建立 RectangleComponent，您可以使用預設建構子從位置、大小和角度建立矩形。 該關係是相對於父級大小定義的向量，例如，Vector2(0.5, 0.8) 關係將建立一個寬度為父級大小的 50%、高度為 80% 的矩形。

在下面的範例中，將建立一個大小為 (25.0, 30.0)、位置為 (100, 100) 的 RectangleComponent:

```
void main() {
  RectangleComponent.relative(
    Vector2(0.5, 1.0),
    position: Vector2.all(100),
    size: Vector2(50, 30),
  );
}
```

由於正方形是矩形的簡化版本，因此還有一個用於建立正方形 RectangleComponent 的建構函數，唯一的差異在於 size 參數是 double 而不是 Vector2：
```
void main() {
  RectangleComponent.square(
    position: Vector2.all(100),
    size: 200,
  );
}
```

## [CircleComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#circlecomponent)

一個 CircleComponent 只能通過定義它的半徑來創建，但你很可能想給它傳遞一個位置或者 paint(默認爲白色)

```
final paint = BasicPalette.red.paint()..style = PaintingStyle.stroke;
final circle = CircleComponent(radius: 200.0, position: Vector2(100, 200), paint: paint);
```

使用相對建構子建立 CircleComponent 時，您可以定義半徑與大小定義的邊界框的最短邊緣相比的長度。

以下範例將產生一個定義半徑為 40（直徑為 80）的圓的 CircleComponent：

```
void main() {
  CircleComponent.relative(0.8, size: Vector2.all(100));
}
```

# [IsometricTileMapComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#isometrictilemapcomponent)

此元件可讓您基於區塊的笛卡爾矩陣和等距圖塊集渲染等距地圖。

關於如何使用它的簡單範例：

```
// Creates a tileset, the block ids are automatically assigned sequentially
// starting at 0, from left to right and then top to bottom.
final tilesetImage = await images.load('tileset.png');
final tileset = IsometricTileset(tilesetImage, 32);
// Each element is a block id, -1 means nothing
final matrix = [[0, 1, 0], [1, 0, 0], [1, 1, 1]];
add(IsometricTileMapComponent(tileset, matrix));
```

它還提供了轉換座標的方法，以便您可以處理點擊、懸停、在圖塊頂部渲染實體、新增選擇器等。

您也可以指定tileHeight，它是圖塊中每個長方體的底部平面和頂部平面之間的垂直距離。 基本上，它是長方體最前緣的高度； 通常它是圖塊大小的一半（預設）或四分之一。 在下圖中，您可以看到深色色調的高度：

![](assets/tile-height-example.png)

這是四分之一長度地圖的範例：

![](assets/isometric.png)

Flame 的範例應用程式包含一個更深入的範例，其中介紹如何解析座標以建立選擇器。 程式碼可以在[這裡](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/isometric_tile_map_example.dart)找到，即時版本可以在[這裡](https://examples.flame-engine.org/#/Rendering_Isometric%20Tile%20Map)查看。

# [NineTileBoxComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#ninetileboxcomponent)

九塊方塊是使用網格精靈繪製的矩形。

網格精靈是一個 3x3 的網格，有 9 個區塊，分別代表 4 個角落、4 個邊和中間。

角部以相同的尺寸繪製，邊緣朝側面拉伸，中間雙向擴展。

使用它，您可以獲得一個可以很好地擴展到任何尺寸的盒子/矩形。 這對於製作面板、對話框、邊框很有用。

檢查範例應用程式 [nine\_tile\_box](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/nine_tile_box_example.dart) 以了解如何使用它的詳細資訊。

# [CustomPainterComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#custompaintercomponent)

CustomPainter 是一個 Flutter 類，與 CustomPaint 小工具一起使用，用於在 Flutter 應用程式中渲染自訂形狀。

Flame提供了一個可以渲染CustomPainter的元件，稱為CustomPainterComponent，它接收自訂畫家並將其渲染在遊戲畫布上。

這可用於在 Flame 遊戲和 Flutter 小部件之間共用自訂渲染邏輯。

檢查範例應用程式 [custom\_painter\_component](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/widgets/custom_painter_example.dart) 以了解如何使用它的詳細資訊。

# [ComponentsNotifier](https://docs.flame-engine.org/1.16.0/flame/components.html#custompaintercomponent)

大多數時候，僅訪問子項及其屬性就足以建立遊戲的邏輯。

但有時，reactivity 可以幫助開發人員簡化並編寫更好的程式碼，Flame 提供了 ComponentsNotifier，它是 ChangeNotifier 的實現，每次新增、刪除或手動變更元件時都會通知偵聽器。

例如，假設我們想在玩家的生命值為零時透過文字顯示遊戲。

為了讓元件在新增或刪除新執行個體時自動報告，可以將 Notifier mixin 套用到元件類別：

```
class Player extends SpriteComponent with Notifier {}
```

然後，要監聽該元件的更改，可以使用 FlameGame 中的 ComponentsNotifier 方法：

```
class MyGame extends FlameGame {
  int lives = 2;

  @override
  void onLoad() {
    final playerNotifier = componentsNotifier<Player>()
        ..addListener(() {
          final player = playerNotifier.single;
          if (player == null) {
            lives--;
            if (lives == 0) {
              add(GameOverComponent());
            } else {
              add(Player());
            }
          }
        });
  }
}
```

通知程式元件還可以手動通知其偵聽器某些內容發生了變化。 讓我們擴展上面的範例，讓 HUD 元件在玩家生命值減半時閃爍。 為此，我們需要 Player 元件手動通知更改，例如：

```
class Player extends SpriteComponent with Notifier {
  double health = 1;

  void takeHit() {
    health -= .1;
    if (health == 0) {
      removeFromParent();
    } else if (health <= .5) {
      notifyListeners();
    }
  }
}
```

那麼我們的 HUD 元件可能如下所示：

```
class Hud extends PositionComponent with HasGameRef {

  @override
  void onLoad() {
    final playerNotifier = gameRef.componentsNotifier<Player>()
        ..addListener(() {
          final player = playerNotifier.single;
          if (player != null) {
            if (player.health <= .5) {
              add(BlinkEffect());
            }
          }
        });
  }
}
```

當 FlameGame 內的狀態改變時，ComponentsNotifiers 也可以派上用場來重建小工具，以協助 Flame 提供 ComponentsNotifierBuilder 小工具。

若要查看其使用範例，請查看[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/components/components_notifier_example.dart)的運行範例。
# [ClipComponent](https://docs.flame-engine.org/1.16.0/flame/components.html#clipcomponent)

ClipComponent 是將畫布裁剪為其大小和形狀的元件。 這意味著，如果元件本身或 ClipComponent 的任何子元件在 ClipComponent 邊界之外渲染，則不會顯示不在該區域內的部分。

ClipComponent 接收一個建構器函數，該函數應傳回將根據其大小定義剪切區域的 Shape。

為了更容易使用該組件，三個工廠提供了常見的形狀：

* **ClipComponent.rectangle**：根據矩形的大小剪切矩形區域。
* **ClipComponent.circle**：根據大小將區域剪切為圓形。
* **ClipComponent.polygon**：根據建構函式中接收到的點以多邊形的形式剪下區域。

檢查範例應用程式 [clip\_component](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/components/clip_component_example.dart) 以取得有關如何使用它的詳細資訊

# [Effects](https://docs.flame-engine.org/1.16.0/flame/components.html#effects)

Flame 提供了一組可套用於特定類型元件的效果，這些效果可用於為元件的某些屬性（如位置或尺寸）設定動畫。 您可以在此處查看這些效果的清單。

運行效果的範例可以在[這裡}(https://github.com/flame-engine/flame/tree/main/examples/lib/stories/effects)找到；

# [不使用FlameGame時](https://docs.flame-engine.org/1.16.0/flame/components.html#when-not-using-flamegame)

如果您不使用 FlameGame，請不要忘記每次遊戲更新時都需要更新所有組件。 這允許組件執行其內部處理並更新其狀態。

例如，所有基於 SpriteAnimation 的元件內部的 SpriteAnimationTicker 都需要勾選動畫物件來決定接下來顯示哪個動畫影格。 這可以透過在不使用 FlameGame 時手動呼叫 component.update() 來完成。 這也意味著，如果您正在實作自己的基於精靈動畫的元件，則可以直接使用 SpriteAnimationTicker 來更新 SpriteAnimation。

