# window 

window 是一整個家族 可以把 Observable&lt;T&gt; 轉到 Observable&lt;Observable&lt;T&gt;&gt;

* window
* windowCount
* windowTime
* windowToggle
* windowWhen

window 用法 類似 buffer

```
import { interval } from 'rxjs';
import { windowCount, concatAll, take } from 'rxjs/operators'
interval(1000).pipe(
    take(3),
    windowCount(3),
    concatAll(),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# groupBy

groupBy 把相同條件的 元素 組合到同個 Observable中

```
import { interval } from 'rxjs';
import { groupBy, switchAll, take } from 'rxjs/operators'
interval(100).pipe(
    take(6),
    groupBy(v => v % 2),
    switchAll(),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```