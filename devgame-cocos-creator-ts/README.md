# Cocos Creator Typescript

Cocos Creator 提供了 javascript 和 typescript 兩種方式來 編寫腳本 正常人類當然會選擇 typescript

* 官網 [http://www.cocos.com](http://www.cocos.com)
* 文檔 [http://docs.cocos.com/creator/manual/zh/](http://docs.cocos.com/creator/manual/zh/)
* API [http://docs.cocos.com/creator/api/zh/](http://docs.cocos.com/creator/api/zh/)
* 源碼 [https://github.com/cocos2d/cocos2d-x](https://github.com/cocos2d/cocos2d-x)

```
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    // update (dt) {}
}
```