# python 要素

python 包含 如下 8個 關鍵要素

* 數據型別
* 對象引用
* 組合數據
* 邏輯運算符
* 控制語句
* 算術運算
* 輸入/輸出
* 函數

# 數據型別
int
* -973
* 1234567890
* (phton 整數大小 只受限於 內存)

str
* "可用雙引號"
* '可用單引號'
> * 可以使用 [] 獲取 字符 但無法修改
> * python中 字符串 是 const 型別
> * 實際上 python 沒有 字符 字符是 長度為1的 字符串
> 	


```py
#!/usr/bin/env python3

# 型別 轉化 寫法 同 go [好吧其實是 go 借鑒的 python語法]
print(int("  123 "))

# str 幾乎可以 用於 任何 型別
print(str(print), str(123))
```

# 對象引用
對象引用 將 變量 與 內存對象 聯繫

使用 引用計數 自動管理 內存

> python 包括 int 也是 一個 到 內存 int的 引用  
> 只不過 其和 字符串一樣 是 const 型別
> 

```py
#!/usr/bin/env python3

x = 1
print(type(x))  # <class 'int'>
```

> type 可以返回數據型別
>

# 組合數據

* 元組(boost::tuple) 使用 () 定義
* 列表(std::vector) 使用 [] 定義

```py
#!/usr/bin/env python3

# 定義一個 元組 <class 'tuple'>
t = (1, 2, 3)
print(t, t[0], len(t), type(t))

# 定義一個 數組 <class 'list'>
l = [1, 2, 3]
l.append(4)
print(l, l[0], len(l), type(l))
```

> len 可以返回 某些型別的 長度 對於字符串 因爲是定長編碼 返回的是 字符串長度 而非 bytes 大小
> ```py
> #!/usr/bin/env python3
> 
> print(len("中文測試1234"))  # 8
> ```
> 
> 對於 長度爲1 的元組 元素後的 **,** 不可省略 以避免 語法歧義
> 
> 使用 () 創建 空 元組件
> 
> ```py
> #!/usr/bin/env python3
> 
> t = (1,)
> print(t, t[0], len(t), type(t))
> t = ()
> print(t, len(t), type(t))
> ```
> 

# 邏輯運算符

python 提供了 4組 邏輯 運算

## 身份操作符號

* is
* is not

用來 判斷 兩個 變量 是否指向同個 對象 

常用來 和 **None** 比較

> None 通常用來 標識 未知/不存在
>  

```py
#!/usr/bin/env python3

x = (1, 2, 3)
y = (1, 2, 3)
print(x is y)  # False)

z = None
print(z is not None)  # False
```
## 比較操作符
* !=
* ==
* <
* <=
* >
* >=

用來 比較 兩個 變量 所引用對象的 值

```py
#!/usr/bin/env python3

t0 = (1, 2, 3)
t1 = (1, 2, 3)
print(t0 is t1)  # False
print(t0 == t1)  # True
```

## 成員操作符號

* in
* not in

用來 測試 成員關係

```py
#!/usr/bin/env python3

print(2 in [1, 2, 3])  # True
print(2 in (1, 2, 3))  # True
```

## 邏輯運算符

* and
* or
* not

# 控制語句
```txt
if bool :
	...
elif bool :
	... 
else:
	...

	
while bool:
	...


for v in iterable:
	...

try
	...
except e1 [as v1]:
	...
...
except eN [as vN]:
	...
```

python 要求語句是一個完整的 suite 既 不能有一個 空的 if ...  
python 提供了 關鍵字 pass 解決此問題
```py
#!/usr/bin/env python3


t = 2
if t == 1:
    pass  # 如果什麼代碼都沒有 python 視錯誤
    # pass 不實現任何功能 只是 提供一個 python 語法要求的 suite
elif t != 1:
    print("ok")
```

# 算術運算符

* +
* -
* *
* /	除法 結果爲浮點
* //	除法 結果爲整型
* ...=
* ...

# 輸入/輸出
```py
#!/usr/bin/env python3

while True:
    try:
        line = input("$>")
        if line == "q":
            break
        try:
            number = int(line)
            print("you input number : ", number)
        except ValueError:
            print("you input string : ", line)
    except EOFError:
        print("\n[ctrl + d]")
        break
    except KeyboardInterrupt:
        print("\n[ctrl + c]")
        break
```

# 函數
def funName(args ...)

```py
#!/usr/bin/env python3


def Min(l, r):
    if l > r:
        return r
    return l


print(Min(1, 2))
```

每個函數 至少有1個 返回值 默認 None  
函數可返回 多個值 會被包裝到 tuple 中