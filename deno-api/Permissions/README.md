# Permissions

deno 運行在受限的安全環境下，一些敏感操作都需要權限，deno 提供了 api 來查詢或申請這些權限

# 查詢權限

```
const status = await Deno.permissions.query({ name: "read", path: "/etc" });
console.log(status.state);
```

# 申請權限
```
const status = await Deno.permissions.request({ name: "env" });
if (status.state === "granted") {
  console.log("'env' permission is granted.");
} else {
  console.log("'env' permission is denied.");
}
```


# 撤銷權限
```
import { assert } from "https://deno.land/std/testing/asserts.ts";

const status = await Deno.permissions.revoke({ name: "run" });
assert(status.state !== "granted")
```