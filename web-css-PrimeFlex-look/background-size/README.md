# [background-size](https://www.primefaces.org/primeflex/backgroundsize)

background-size 指定背景圖像大小

| Class | Properties |
| -------- | -------- |
| bg-auto     | background-size: auto;     |
| bg-cover     | background-size: cover;     |
| bg-contain     | background-size: contain;     |

## bg-auto

bg-auto 的圖像以其原始大小顯示

![](assets/auto.png)

```
<div class="card">
    <div class="card-container blue-container overflow-hidden">
        <div class="bg-auto bg-no-repeat bg-center bg-blue-500 border-round h-20rem w-full" style="background-image: url('./assets/images/product-placeholder-blue.svg');"></div>
    </div>
</div>
```

## bg-cover

bg-cover 如果有不要通過拉伸剪裁調整圖像大小以完全覆蓋容器

![](assets/cover.png)

```
<div class="card">
    <div class="card-container yellow-container overflow-hidden">
        <div class="bg-cover bg-center border-yellow-500 border-2 border-round h-20rem w-full" style="background-image: url('./assets/images/product-placeholder-yellow.svg')"></div>
    </div>
</div>
```

## bg-contain

bg-contain 調整圖像大小以填充容器

![](assets/contain.png)

```
<div class="card">
    <div class="card-container green-container overflow-hidden">
        <div class="bg-contain bg-center bg-no-repeat bg-green-500 border-round h-20rem w-full" style="background-image: url('./assets/images/product-placeholder-green.svg')"></div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同