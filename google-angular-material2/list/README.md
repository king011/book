# List
在 Material Design 中 List 是在一欄中 呈現多個 資料的 列  
每列就是一組資料 在資料中能夠清楚呈現相關資訊  
如果需要 也能夠 對這些資料進行一些 額外操作

## 一個列 資本包含 3種資料
* **Avatar** 通常是一個 頭像 或 icon 用來作爲資料的 基本代表
* **內容** 主要的文字內容 通常是 單行文字 必要時也可以顯示 多行文字
* **行動 or 資訊** 代表資料的補充資訊 可能是一個警告或icon 或一個按鈕等

![list](assets/00-list-intro.png)

# MatListModule
MatListModule 提供了 列表的 功能  
**mat-list** 定義一個 列表  
**mat-list-item** 定義一列資料

```ts
import { MatListModule } from '@angular/material/list';
```
```html
<mat-list>
    <mat-list-item>問卷調查</mat-list-item>
    <mat-list-item>部落格</mat-list-item>
    <mat-list-item>收件夾</mat-list-item>
</mat-list>
```

## mat-list-item

mat-list-item 不但是一個 component 同時也能以 directive 的方式 使用

```html
<mat-list>
    <a [routerLink]="['/', 'dashboard', 'survey']" mat-list-item>問卷調查</a>
    <a [routerLink]="['/', 'dashboard', 'blog']" mat-list-item>部落格</a>
    <a [routerLink]="['/', 'dashboard', 'inbox']" mat-list-item>收件夾</a>
</mat-list>
```
## mat-nav-list

如果在 導航欄中 通常 會去掉 a標籤的 下劃線 等 此時可以使用 mat-nav-list 替代 mat-list  
**mat-nav-list** 和 mat-list 完全一樣 只是 貼心的 爲導航欄 定義了 css 以去掉 a標籤的 下劃線 等

```html
<mat-nav-list>
  <a [routerLink]="['/', 'dashboard', 'survey']" mat-list-item>問卷調查</a>
  <a [routerLink]="['/', 'dashboard', 'blog']" mat-list-item>部落格</a>
  <a [routerLink]="['/', 'dashboard', 'inbox']" mat-list-item>收件夾</a>
</mat-nav-list>
```

## matLine

list 的每列數據 都只顯示 一行 可以使用 matLine 讓每個 matLine 都單獨顯示 一行

```html
<mat-nav-list>
  <h3 matSubheader>多行文字示範</h3>
  <mat-list-item>
    <p matLine>床前明月光</p>
    <p matLine>疑是地上霜</p>
  </mat-list-item>
  <mat-list-item>
    <p matLine>參加ＩＴ鐵人賽</p>
    <p matLine>功力增加一甲子</p>
  </mat-list-item>
</mat-nav-list>
```
## matSubheader
使用 matSubheader 的文字會變成 灰色 並且不能 被選中 可以 當作 分組 標題用

## mat-divider
```
import {MatDividerModule} from '@angular/material/divider';
```

`<mat-divider></mat-divider>`  用來 創建一個分割線

# matListAvatar
使用 matListAvatar 指令 可以在 最右邊 添加一個 頭像
```html
<h3 matSubheader>好友訊息</h3>
<mat-list-item>
  <img matListAvatar src="..." />
  <p matLine>志玲</p>
  <p matLine>hi，好久不見，最近好嗎？</p>
</mat-list-item>
<mat-list-item>
  <img matListAvatar src="..." />
  <p matLine>依晨</p>
  <p matLine>找時間吃個飯吧？</p>
</mat-list-item>
```

# button
安裝 Material Design  的設計原則 button 應該在最右邊 所以 button 無論寫在 list中的哪裏 都會 顯示到 最右邊 
```html
<mat-list-item>
  <img matListAvatar src="..." />
  <p matLine>志玲</p>
  <p matLine>hi，好久不見，最近好嗎？</p>
  <!-- button會自動被推到最後面 -->
  <button mat-icon-button><mat-icon>chat</mat-icon></button>
</mat-list-item>
<mat-list-item>
  <!-- 即使icon button放在前面，還是會被往後推 -->
  <button mat-icon-button><mat-icon>chat</mat-icon></button>
  <img matListAvatar src="..." />
  <p matLine>依晨</p>
  <p matLine>找時間吃個飯吧？</p>
</mat-list-item>
```

# mat-selection-list
mat-selection-list 和list類似 不過 其帶有一個 複選框 使用 mat-list-option 定義列
```html
<mat-nav-list>
  <h3 matSubheader>
    <mat-icon>chat_bubble</mat-icon>
    新訊息
  </h3>
  <mat-list-item *ngIf="optNew.selected">這是新消息</mat-list-item>
  <mat-list-item *ngIf="optAds.selected">這是廣告消息</mat-list-item>

  <mat-divider></mat-divider>
  <h3 matSubheader>
    <mat-icon>settings</mat-icon>
    訊息設定
  </h3>
  <mat-selection-list>
    <mat-list-option [value]="1" selected="true" #optNew>有新訊息時通知我</mat-list-option>
    <mat-list-option [value]="2" #optAds>顯示廣告訊息</mat-list-option>
  </mat-selection-list>
</mat-nav-list>
```