# QEMU

QEMU 是一個可執行硬體虛擬化的開源代管虛擬機器 可以配合 KVM 使用接近本地速度執行虛擬機器

* 官網 [https://www.qemu.org/](https://www.qemu.org/)
* 源碼 [https://git.qemu.org/git/qemu.git](https://git.qemu.org/git/qemu.git)