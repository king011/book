# mariadb 主從

使用 StatefulSet 可以很方便的設置 mariadb 主從模式

首先創建一個 configmap 用於 存儲 master 和 slave 的服務器設定

```
#info="configmap.yaml"
apiVersion: v1
kind: ConfigMap
metadata:
	name: mariadb
	labels:
		app: mariadb
data:
	master.cnf: |
		# config for master
		[mysqld]
		log-bin=master-bin
		binlog_format=mixed
	slave.cnf: |
		# config for slaves
		[mysqld]
		read-only
```

創建一個 secret 用於存儲 數據庫 密碼
```
#info="secret.yaml"
apiVersion: v1
kind: Secret
metadata:
  name: mariadb
  labels:
    app: mariadb
type:
data:
  root_password: MTIz # 123
  slave_name: c2xhdmU= # 從庫備份用的用戶名
  slave_password: NDU2 # 從庫備份用的密碼 456
```


創建一個 無頭的 mariadb 服務 用於集羣內部通信，以及一個 mariadb-read 服務 用於從數據庫讀取數據

```
#info="services.yaml"
# Headless service for stable DNS entries of StatefulSet members.
apiVersion: v1
kind: Service
metadata:
	name: mariadb
	labels:
		app: mariadb
spec:
	ports:
		- name: mariadb
			port: 3306
	clusterIP: None
	selector:
		app: mariadb
---
# Client service for connecting to any mariadb instance for reads.
# For writes, you must instead connect to the master: mariadb-0.mariadb.
apiVersion: v1
kind: Service
metadata:
	name: mariadb-read
	labels:
		app: mariadb
spec:
	ports:
		- name: mariadb
			port: 3306
	selector:
		app: mariadb
```

依據你使用的持久介質，創建一個 StorageClass 用於動態供應 PVC，本例假設已經存在一個名爲 **nfs-storage-class** 的 StorageClass 可用於動態供應 PVC

最後創建 StatefulSet

```
#info="statefulset.yaml"
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: mariadb
  name: mariadb
spec:
  serviceName: "mariadb"
  replicas: 2
  selector:
    matchLabels:
      app: mariadb
  template:
    metadata:
      labels:
        app: mariadb
    spec:
      initContainers:
        - name: init-mariadb
          image: mariadb:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              # Generate mysql server-id from pod ordinal index.
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              echo [mysqld] > /mnt/conf.d/server-id.cnf
              # Add an offset to avoid reserved server-id=0 value.
              echo server-id=$((100 + $ordinal)) >> /mnt/conf.d/server-id.cnf
              # Copy appropriate conf.d files from config-map to emptyDir.
              if [[ $ordinal -eq 0 ]]; then
                cp /mnt/config-map/master.cnf /mnt/conf.d/
              else
                cp /mnt/config-map/slave.cnf /mnt/conf.d/
              fi
          volumeMounts:
            - name: conf
              mountPath: /mnt/conf.d
            - name: config-map
              mountPath: /mnt/config-map
        - name: clone-mariadb
          image: king011/mariabackup:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              # # Skip the clone if data already exists.
              # [[ -d /var/lib/mysql/mysql ]] && exit 0

              # Skip the clone on master (ordinal index 0).
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              [[ $ordinal -eq 0 ]] && exit 0

              # Clone data from previous peer.
              if [[ ! -f /var/lib/mysql/success-ncat ]]; then
                rm /var/lib/mysql/* -rf
                ncat --recv-only mariadb-$(($ordinal-1)).mariadb 3307 | mbstream -x -C /var/lib/mysql
                if [[ -d /var/lib/mysql/mysql ]]; then
                  touch /var/lib/mysql/success-ncat
                else
                  echo ncat not data
                  exit 1
                fi
              fi
              # Prepare the backup.
              if [[ ! -f /var/lib/mysql/success-mariabackup ]]; then
                mariabackup --prepare --target-dir=/var/lib/mysql
                touch /var/lib/mysql/success-mariabackup
              fi
          volumeMounts:
            - name: mariadb-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
      containers:
        - name: mariadb
          image: mariadb:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
            - name: MYSQL_SLAVE_NAME
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_name
            - name: MYSQL_SLAVE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_password
          ports:
            - name: mariadb
              containerPort: 3306
          volumeMounts:
            - name: mariadb-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
          resources:
            requests:
              cpu: 500m
              memory: 1Gi
          livenessProbe:
            exec:
              command:
                - bash
                - "-c"
                - |
                  mysqladmin ping --user=root --password="$MYSQL_ROOT_PASSWORD"
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
          readinessProbe:
            exec:
              # Check we can execute queries over TCP (skip-networking is off).
              command:
                - bash
                - "-c"
                - |
                  mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e 'SELECT 1'
            initialDelaySeconds: 5
            periodSeconds: 2
            timeoutSeconds: 1
        - name: mariabackup
          image: king011/mariabackup:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
            - name: MYSQL_SLAVE_NAME
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_name
            - name: MYSQL_SLAVE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_password
          ports:
            - name: mariabackup
              containerPort: 3307
          command:
            - bash
            - "-c"
            - |
              set -ex

              # master set
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              if [[ $ordinal -eq 0 ]]; then
                echo "Waiting for mysqld to be ready (accepting connections)"
                until mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "SELECT 1"; do sleep 1; done

                # disable root remote
                # create slave user
                mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" \
                      -e "DELETE FROM mysql.user WHERE USER='root' AND HOST='%'; \
                      CREATE USER IF NOT EXISTS '$MYSQL_SLAVE_NAME'@'%' IDENTIFIED BY '$MYSQL_SLAVE_PASSWORD'; \
                      GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO '$MYSQL_SLAVE_NAME'@'%';  \
                      FLUSH PRIVILEGES;" || exit 1
              fi

              cd /var/lib/mysql

              # Determine binlog position of cloned data, if any.
              if [[ -f xtrabackup_slave_info && "x$(<xtrabackup_slave_info)" != "x" ]]; then
                # XtraBackup already generated a partial "CHANGE MASTER TO" query
                # because we're cloning from an existing slave. (Need to remove the tailing semicolon!)
                cat xtrabackup_slave_info | sed -E 's/;$//g' > change_master_to.sql.in
                # Ignore xtrabackup_binlog_info in this case (it's useless).
                rm -f xtrabackup_slave_info xtrabackup_binlog_info
              elif [[ -f xtrabackup_binlog_info ]]; then
                # We're cloning directly from master. Parse binlog position.
                [[ `cat xtrabackup_binlog_info` =~ ^([[:alnum:]_\.\-]*?)[[:space:]]+([[:digit:]]*?)(.*?)$ ]] || exit 1
                rm -f xtrabackup_binlog_info xtrabackup_slave_info
                echo "CHANGE MASTER TO MASTER_LOG_FILE='${BASH_REMATCH[1]}',\
                      MASTER_LOG_POS=${BASH_REMATCH[2]}" > change_master_to.sql.in
              fi

              # Check if we need to complete a clone by starting replication.
              if [[ -f change_master_to.sql.in ]]; then
                echo "Waiting for mysqld to be ready (accepting connections)"
                until mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "SELECT 1"; do sleep 1; done

                echo "Initializing replication from clone position"
                mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" \
                      -e "$(<change_master_to.sql.in), \
                              MASTER_HOST='mariadb-0.mariadb', \
                              MASTER_USER='$MYSQL_SLAVE_NAME', \
                              MASTER_PASSWORD='$MYSQL_SLAVE_PASSWORD', \
                              MASTER_CONNECT_RETRY=10; \
                            START SLAVE;" || exit 1
                # In case of container restart, attempt this at-most-once.
                mv change_master_to.sql.in change_master_to.sql.orig
              fi

              # Start a server to send backups when requested by peers.
              exec ncat --listen --keep-open --send-only --max-conns=1 3307 -c \
                "mariabackup --backup --slave-info --stream=xbstream --host=127.0.0.1 --user=root --password=\"$MYSQL_ROOT_PASSWORD\""
          volumeMounts:
            - name: mariadb-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
      volumes:
        - name: conf
          emptyDir: {}
        - name: config-map
          configMap:
            name: mariadb
  volumeClaimTemplates:
    - metadata:
        name: mariadb-storage
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "nfs-storage-class"
        resources:
          requests:
            storage: 100Mi
```

# 主從分散 statefulset 每日例行備份

前文的代碼參考了 k8s 教學的例子 主從 屬於同個 statefulset 讀取可能會被 分流到寫庫中 下面是另外一個例子創建了三個 statefulset 分別用於

1. 主庫寫入
2. 從庫讀取
3. 每日例行增量備份

首先還是創建一個 configmap 存儲數據庫配置

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: mariadb
  labels:
    app: mariadb
data:
  master.cnf: |
    # config for master
    [mysqld]
    log-bin=master-bin
    binlog_format=mixed
  slave.cnf: |
    # config for slaves
    [mysqld]
    read-only
```

再創建一個 secret 存儲密碼

```
apiVersion: v1
kind: Secret
metadata:
  name: mariadb
  labels:
    app: mariadb
type:
data:
  root_password: MTIz # 123
  slave_name: c2xhdmU= # 從庫備份用的用戶名 slave
  slave_password: NDU2 # 456
```

創建一個 configmap 存儲存儲時區設定
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: timezone
  labels:
    app: timezone
binaryData:
  timezone: QXNpYS9TaGFuZ2hhaQo= # Asia/Shanghai
  localtime: |
    VFppZjIAAAAAAAAAAAAAAAAAAAAAAAADAAAAAwAAAAAAAAAdAAAAAwAAAAyAAAAAoJeigKF5BPDI
    WV6AyQn5cMnTvQDLBYrwy3xAANI7PvDTi3uA1EKt8NVFIgDWTL/w1zy/ANgGZnDZHfKA2UF88B66
    UiAfaZuQIH6EoCFJfZAiZ6EgIylfkCRHgyAlEnwQJidlICbyXhAoB0cgKNJAEAIBAgECAQIBAgEC
    AQIBAgECAQIBAgECAQIBAgECAABx1wAAAAB+kAEEAABwgAAITE1UAENEVABDU1QAAAAAAAAAVFpp
    ZjIAAAAAAAAAAAAAAAAAAAAAAAADAAAAAwAAAAAAAAAeAAAAAwAAAAz4AAAAAAAAAP////9+NkMp
    /////6CXooD/////oXkE8P/////IWV6A/////8kJ+XD/////ydO9AP/////LBYrw/////8t8QAD/
    ////0js+8P/////Ti3uA/////9RCrfD/////1UUiAP/////WTL/w/////9c8vwD/////2AZmcP//
    ///ZHfKA/////9lBfPAAAAAAHrpSIAAAAAAfaZuQAAAAACB+hKAAAAAAIUl9kAAAAAAiZ6EgAAAA
    ACMpX5AAAAAAJEeDIAAAAAAlEnwQAAAAACYnZSAAAAAAJvJeEAAAAAAoB0cgAAAAACjSQBAAAgEC
    AQIBAgECAQIBAgECAQIBAgECAQIBAgECAQIAAHHXAAAAAH6QAQQAAHCAAAhMTVQAQ0RUAENTVAAA
    AAAAAAAKQ1NULTgK
```

創建 4個services 用於 statefulset 間通信
1. mariadb -> 訪問主庫
2. mariadb-read -> 訪問從庫
3. mariadb-reads -> 負載訪問從庫
4. mariadb-backup -> 訪問備份

```
apiVersion: v1
kind: Service
metadata:
  name: mariadb
  labels:
    app: mariadb
spec:
  ports:
    - name: mariadb
      port: 3306
  clusterIP: None
  selector:
    app: mariadb
---
apiVersion: v1
kind: Service
metadata:
  name: mariadb-read
  labels:
    app: mariadb-read
spec:
  ports:
    - name: mariadb-read
      port: 3306
  clusterIP: None
  selector:
    app: mariadb-read
---
apiVersion: v1
kind: Service
metadata:
  name: mariadb-reads
  labels:
    app: mariadb-read
spec:
  ports:
    - name: mariadb-read
      port: 3306
    - name: mariadb-backup
      port: 3307
  selector:
    app: mariadb-read
---
apiVersion: v1
kind: Service
metadata:
  name: mariadb-backup
  labels:
    app: mariadb-backup
spec:
  ports:
    - name: mariadb-backup
      port: 3306
  clusterIP: None
  selector:
    app: mariadb-backup
```

創建 主庫
```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: mariadb
  name: mariadb
spec:
  serviceName: "mariadb"
  replicas: 1
  selector:
    matchLabels:
      app: mariadb
  template:
    metadata:
      labels:
        app: mariadb
    spec:
      initContainers:
        - name: init-mariadb
          image: mariadb:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              # Generate mysql server-id from pod ordinal index.
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              echo [mysqld] > /mnt/conf.d/server-id.cnf
              # Add an offset to avoid reserved server-id=0 value.
              echo server-id=$((100 + $ordinal)) >> /mnt/conf.d/server-id.cnf
              # Copy appropriate conf.d files from config-map to emptyDir.
              cp /mnt/config-map/master.cnf /mnt/conf.d/
          volumeMounts:
            - name: conf
              mountPath: /mnt/conf.d
            - name: config-map
              mountPath: /mnt/config-map
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
      containers:
        - name: mariadb
          image: mariadb:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
          ports:
            - name: mariadb
              containerPort: 3306
          volumeMounts:
            - name: mariadb-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
          resources:
            requests:
              cpu: 500m
              memory: 1Gi
          livenessProbe:
            exec:
              command:
                - bash
                - "-c"
                - |
                  mysqladmin ping --user=root --password="$MYSQL_ROOT_PASSWORD"
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
          readinessProbe:
            exec:
              # Check we can execute queries over TCP (skip-networking is off).
              command:
                - bash
                - "-c"
                - |
                  mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e 'SELECT 1'
            initialDelaySeconds: 5
            periodSeconds: 2
            timeoutSeconds: 1
        - name: mariabackup
          image: king011/mariabackup:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
            - name: MYSQL_SLAVE_NAME
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_name
            - name: MYSQL_SLAVE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_password
          ports:
            - name: mariabackup
              containerPort: 3307
          command:
            - bash
            - "-c"
            - |
              set -ex

              echo "Waiting for mysqld to be ready (accepting connections)"
              until mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "SELECT 1"; do sleep 1; done

              # disable root remote
              # create users for slave and backup
              mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" \
                    -e "DELETE FROM mysql.user WHERE USER='root' AND HOST='%'; \
                    CREATE USER IF NOT EXISTS '$MYSQL_SLAVE_NAME'@'%' IDENTIFIED BY '$MYSQL_SLAVE_PASSWORD'; \
                    GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO '$MYSQL_SLAVE_NAME'@'%';  \
                    FLUSH PRIVILEGES;" || exit 1

              # unset secrets
              unset MYSQL_SLAVE_NAME
              unset MYSQL_SLAVE_PASSWORD

              # Start a server to send backups when requested by peers.
              exec gosu mysql ncat --listen --keep-open --send-only --max-conns=1 3307 -c \
                "mariabackup --backup --slave-info --stream=xbstream --host=127.0.0.1 --user=root --password='$MYSQL_ROOT_PASSWORD'"
          volumeMounts:
            - name: mariadb-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
              readOnly: true
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
      volumes:
        - name: conf
          emptyDir: {}
        - name: config-map
          configMap:
            name: mariadb
        - name: config-timezone
          configMap:
            name: timezone
  volumeClaimTemplates:
    - metadata:
        name: mariadb-storage
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "nfs-storage-class"
        resources:
          requests:
            storage: 10Gi
```

創建從庫
```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: mariadb-read
  name: mariadb-read
spec:
  serviceName: "mariadb-read"
  replicas: 2
  selector:
    matchLabels:
      app: mariadb-read
  template:
    metadata:
      labels:
        app: mariadb-read
    spec:
      initContainers:
        - name: init-mariadb
          image: mariadb:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              # Generate mysql server-id from pod ordinal index.
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              echo [mysqld] > /mnt/conf.d/server-id.cnf
              # Add an offset to avoid reserved server-id=0 value.
              echo server-id=$((1000 + $ordinal)) >> /mnt/conf.d/server-id.cnf
              # Copy appropriate conf.d files from config-map to emptyDir.
              cp /mnt/config-map/slave.cnf /mnt/conf.d/
          volumeMounts:
            - name: conf
              mountPath: /mnt/conf.d
            - name: config-map
              mountPath: /mnt/config-map
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
        - name: clone-mariadb
          image: king011/mariabackup:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              if [[ $ordinal -eq 0 ]];then
                address=mariadb-0.mariadb
              else
                address=mariadb-read-$(($ordinal-1)).mariadb-read
              fi
              # Clone data from previous peer.
              if [[ ! -f /var/lib/mysql/success-ncat ]]; then
                rm /var/lib/mysql/* -rf
                ncat --recv-only $address 3307 | mbstream -x -C /var/lib/mysql
                if [[ -d /var/lib/mysql/mysql ]]; then
                  touch /var/lib/mysql/success-ncat
                else
                  echo ncat not data
                  exit 1
                fi
              fi

              # Prepare the backup.
              if [[ ! -f /var/lib/mysql/success-mariabackup ]]; then
                mariabackup --prepare --target-dir=/var/lib/mysql
                touch /var/lib/mysql/success-mariabackup
              fi
          volumeMounts:
            - name: mariadb-read-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
      containers:
        - name: mariadb
          image: mariadb:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
          ports:
            - name: mariadb-read
              containerPort: 3306
          volumeMounts:
            - name: mariadb-read-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
          resources:
            requests:
              cpu: 250m
              memory: 500Mi
          livenessProbe:
            exec:
              command:
                - bash
                - "-c"
                - |
                  mysqladmin ping --user=root --password="$MYSQL_ROOT_PASSWORD"
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
          readinessProbe:
            exec:
              # Check we can execute queries over TCP (skip-networking is off).
              command:
                - bash
                - "-c"
                - |
                  mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e 'SELECT 1'
            initialDelaySeconds: 5
            periodSeconds: 2
            timeoutSeconds: 1
        - name: mariabackup
          image: king011/mariabackup:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
            - name: MYSQL_SLAVE_NAME
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_name
            - name: MYSQL_SLAVE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_password
          ports:
            - name: mariabackup
              containerPort: 3307
          command:
            - bash
            - "-c"
            - |
              set -ex
              echo "Waiting for mysqld to be ready (accepting connections)"
              until mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "SELECT 1"; do sleep 1; done

              cd /var/lib/mysql

              # Determine binlog position of cloned data, if any.
              if [[ -f xtrabackup_slave_info && "x$(<xtrabackup_slave_info)" != "x" ]]; then
                # XtraBackup already generated a partial "CHANGE MASTER TO" query
                # because we're cloning from an existing slave. (Need to remove the tailing semicolon!)
                cat xtrabackup_slave_info | sed -E 's/;$//g' > change_master_to.sql.in
                # Ignore xtrabackup_binlog_info in this case (it's useless).
                rm -f xtrabackup_slave_info xtrabackup_binlog_info
              elif [[ -f xtrabackup_binlog_info ]]; then
                # We're cloning directly from master. Parse binlog position.
                [[ `cat xtrabackup_binlog_info` =~ ^([[:alnum:]_\.\-]*?)[[:space:]]+([[:digit:]]*?)(.*?)$ ]] || exit 1
                rm -f xtrabackup_binlog_info xtrabackup_slave_info
                echo "CHANGE MASTER TO MASTER_LOG_FILE='${BASH_REMATCH[1]}',\
                      MASTER_LOG_POS=${BASH_REMATCH[2]}" > change_master_to.sql.in
              fi
              # Check if we need to complete a clone by starting replication.
              if [[ -f change_master_to.sql.in ]]; then
                echo "Initializing replication from clone position"
                mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" \
                      -e "$(<change_master_to.sql.in), \
                              MASTER_HOST='mariadb-0.mariadb', \
                              MASTER_USER='$MYSQL_SLAVE_NAME', \
                              MASTER_PASSWORD='$MYSQL_SLAVE_PASSWORD', \
                              MASTER_CONNECT_RETRY=10; \
                            START SLAVE;" || exit 1
                # In case of container restart, attempt this at-most-once.
                mv change_master_to.sql.in change_master_to.sql.orig
              fi
              # unset secrets
              unset MYSQL_SLAVE_NAME
              unset MYSQL_SLAVE_PASSWORD
              # Start a server to send backups when requested by peers.
              exec gosu mysql ncat --listen --keep-open --send-only --max-conns=1 3307 -c \
                "mariabackup --backup --slave-info --stream=xbstream --host=127.0.0.1 --user=root --password='$MYSQL_ROOT_PASSWORD'"
          volumeMounts:
            - name: mariadb-read-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
      volumes:
        - name: conf
          emptyDir: {}
        - name: config-map
          configMap:
            name: mariadb
        - name: config-timezone
          configMap:
            name: timezone
  volumeClaimTemplates:
    - metadata:
        name: mariadb-read-storage
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "nfs-storage-class"
        resources:
          requests:
            storage: 10Gi
```

創建例行備份集羣

```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: mariadb-backup
  name: mariadb-backup
spec:
  serviceName: "mariadb-backup"
  replicas: 1
  selector:
    matchLabels:
      app: mariadb-backup
  template:
    metadata:
      labels:
        app: mariadb-backup
    spec:
      initContainers:
        - name: init-mariadb
          image: mariadb:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              # Generate mysql server-id from pod ordinal index.
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              echo [mysqld] > /mnt/conf.d/server-id.cnf
              # Add an offset to avoid reserved server-id=0 value.
              echo server-id=$((500 + $ordinal)) >> /mnt/conf.d/server-id.cnf
              # Copy appropriate conf.d files from config-map to emptyDir.
              cp /mnt/config-map/slave.cnf /mnt/conf.d/
          volumeMounts:
            - name: conf
              mountPath: /mnt/conf.d
            - name: config-map
              mountPath: /mnt/config-map
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
        - name: clone-mariadb
          image: king011/mariabackup:10.5.9
          command:
            - bash
            - "-c"
            - |
              set -ex
              [[ `hostname` =~ -([0-9]+)$ ]] || exit 1
              ordinal=${BASH_REMATCH[1]}
              if [[ $ordinal -eq 0 ]];then
                address=mariadb-reads
              else
                address=mariadb-backup-$(($ordinal-1)).mariadb-backup
              fi
              # Clone data from previous peer.
              if [[ ! -f /var/lib/mysql/success-ncat ]]; then
                rm /var/lib/mysql/* -rf
                ncat --recv-only $address 3307 | mbstream -x -C /var/lib/mysql
                if [[ -d /var/lib/mysql/mysql ]]; then
                  touch /var/lib/mysql/success-ncat
                else
                  echo ncat not data
                  exit 1
                fi
              fi

              # Prepare the backup.
              if [[ ! -f /var/lib/mysql/success-mariabackup ]]; then
                mariabackup --prepare --target-dir=/var/lib/mysql
                touch /var/lib/mysql/success-mariabackup
              fi
          volumeMounts:
            - name: mariadb-backup-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
      containers:
        - name: mariadb
          image: mariadb:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
          ports:
            - name: mariadb-backup
              containerPort: 3306
          volumeMounts:
            - name: mariadb-backup-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
          resources:
            requests:
              cpu: 100m
              memory: 200Mi
          livenessProbe:
            exec:
              command:
                - bash
                - "-c"
                - |
                  mysqladmin ping --user=root --password="$MYSQL_ROOT_PASSWORD"
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 5
          readinessProbe:
            exec:
              # Check we can execute queries over TCP (skip-networking is off).
              command:
                - bash
                - "-c"
                - |
                  mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e 'SELECT 1'
            initialDelaySeconds: 5
            periodSeconds: 2
            timeoutSeconds: 1
        - name: mariabackup
          image: king011/mariabackup:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
            - name: MYSQL_SLAVE_NAME
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_name
            - name: MYSQL_SLAVE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: slave_password
          ports:
            - name: mariabackup
              containerPort: 3307
          command:
            - bash
            - "-c"
            - |
              set -ex
              echo "Waiting for mysqld to be ready (accepting connections)"
              until mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "SELECT 1"; do sleep 1; done

              cd /var/lib/mysql

              # Determine binlog position of cloned data, if any.
              if [[ -f xtrabackup_slave_info && "x$(<xtrabackup_slave_info)" != "x" ]]; then
                # XtraBackup already generated a partial "CHANGE MASTER TO" query
                # because we're cloning from an existing slave. (Need to remove the tailing semicolon!)
                cat xtrabackup_slave_info | sed -E 's/;$//g' > change_master_to.sql.in
                # Ignore xtrabackup_binlog_info in this case (it's useless).
                rm -f xtrabackup_slave_info xtrabackup_binlog_info
              elif [[ -f xtrabackup_binlog_info ]]; then
                # We're cloning directly from master. Parse binlog position.
                [[ `cat xtrabackup_binlog_info` =~ ^([[:alnum:]_\.\-]*?)[[:space:]]+([[:digit:]]*?)(.*?)$ ]] || exit 1
                rm -f xtrabackup_binlog_info xtrabackup_slave_info
                echo "CHANGE MASTER TO MASTER_LOG_FILE='${BASH_REMATCH[1]}',\
                      MASTER_LOG_POS=${BASH_REMATCH[2]}" > change_master_to.sql.in
              fi
              # Check if we need to complete a clone by starting replication.
              if [[ -f change_master_to.sql.in ]]; then
                echo "Initializing replication from clone position"
                mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" \
                      -e "$(<change_master_to.sql.in), \
                              MASTER_HOST='mariadb-0.mariadb', \
                              MASTER_USER='$MYSQL_SLAVE_NAME', \
                              MASTER_PASSWORD='$MYSQL_SLAVE_PASSWORD', \
                              MASTER_CONNECT_RETRY=10; \
                            START SLAVE;" || exit 1
                # In case of container restart, attempt this at-most-once.
                mv change_master_to.sql.in change_master_to.sql.orig
              fi
              # unset secrets
              unset MYSQL_SLAVE_NAME
              unset MYSQL_SLAVE_PASSWORD
              # Start a server to send backups when requested by peers.
              exec gosu mysql ncat --listen --keep-open --send-only --max-conns=1 3307 -c \
                "mariabackup --backup --slave-info --stream=xbstream --host=127.0.0.1 --user=root --password='$MYSQL_ROOT_PASSWORD'"
          volumeMounts:
            - name: mariadb-backup-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
        - name: cronbackup
          image: king011/mariabackup:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb
                  key: root_password
          command:
            - bash
            - "-c"
            - |
              set -ex
              echo "Waiting for mysqld to be ready (accepting connections)"
              until mysql -h 127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "SELECT 1"; do sleep 1; done

              # crontab every day
              exec gosu mysql /opt/cronbackup/cronbackup backup --host=127.0.0.1 --user=root --password="$MYSQL_ROOT_PASSWORD" --output=/backup --contab="0 3 * * *" -d
          volumeMounts:
            - name: mariadb-backup-storage
              mountPath: /var/lib/mysql
              subPath: mariadb
            - name: mariadb-backup-storage
              mountPath: /backup
              subPath: backup
            - name: conf
              mountPath: /etc/mysql/conf.d
            - name: config-timezone
              mountPath: /etc/localtime
              subPath: localtime
            - name: config-timezone
              mountPath: /etc/timezone
              subPath: timezone
      volumes:
        - name: conf
          emptyDir: {}
        - name: config-map
          configMap:
            name: mariadb
        - name: config-timezone
          configMap:
            name: timezone
  volumeClaimTemplates:
    - metadata:
        name: mariadb-backup-storage
      spec:
        accessModes: ["ReadWriteMany"]
        storageClassName: "nfs-storage-class"
        resources:
          requests:
            storage: 20Gi
```