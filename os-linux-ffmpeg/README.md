# ffmpeg

ffmpeg 是一個多功能是 音視頻 處理 套件 合集 以 GPL 發佈

* ffmpeg 一個命令行工具 用來 對視頻轉碼
* ffserver 一個 http 多媒體即時廣播 串流服務器
* ffplay 一個 簡單的 播放器 基於 SDL 和 ffmpeg
* libavcodec 包含 全部 FFmpeg 音視頻 編碼解碼 函數庫
* libavformat 包含 demuxers muxer 函式庫
* libavutil 一些 工具函數
* libpostproc 對視頻做前處理的函數庫
* libswscale 對影響做縮放的函數庫