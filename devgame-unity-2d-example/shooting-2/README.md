# 射擊  2/2

現在我們的飛船正在射擊無辜的章魚，爲了戰鬥的勢均力敵，敵人也應該可以發射子彈

我們將用下面的圖片來製作新的 發射物，你可以將上章的 shot Prefab 複製一份(CTRL+D)修改圖片資源給章魚使用

![](assets/shot_poulpi.png)

將複製的 Prefab 改名爲 EnemyShot1，將 Scale 改爲  (0.35, 0.35, 1)

![](assets/shot_config2.png)

# 開火

就像對 Player 做的那樣，我們需要爲敵人添加武器，讓它們調用 Attack 來發射彈藥

1. 爲敵人添加 **WeaponScript** 腳本
2. 將 **EnemyShot1** 設置帶腳本的 **Shot Prefab** 變量
3. 新創一個腳本命名爲 **EnemyScript**，它將會簡單的試着在每一幀去使用武器進行自動開火

```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private WeaponScript weapon;

    void Awake()
    {
        // 腳本實例被加載時 執行一次 來獲取武器
        weapon = GetComponent<WeaponScript>();
    }

    void Update()
    {
        // 自動開火
        if (weapon != null && weapon.CanAttack)
        {
            weapon.Attack(true);
        }
    }
}
```

你可能需要調整下敵人的腳本屬性以爲玩家放水，就像下面這樣(將 shooting rate 增加到來 0.75)
![](assets/enemy_config.png)

試着運行你會看到章魚在開火

![](assets/shoot_right.gif)

你會發現武器正在向右邊開火，你可以旋轉敵人讓它向左開火，但這樣精靈也被旋轉來這顯然不是我們想要的

![](assets/gizmo2.png)

# 向任意方向射擊

你可以通過旋轉 **WeaponScript** 掛載的對象來向任意方向射擊。前面我們已經嘗試來旋轉章魚，效果不是我們想要的。

這裏的一個技巧是創建一個空的遊戲對象，作爲 敵人的子對象

我們需要
1. 創建一個 **Empty Game Object**，命名爲 **Weapon**
2. 刪除掛載到章魚 Prefab 上的 **WeaponScript**
3. 將 **WeaponScript** 添加到 **Weapon** 對象
4. 將 **Weapon** 旋轉到 (0,0,180)

![](assets/enemy_full_config.png)

此時我們還需要修改下 **EnemyScript** 腳本因爲 **GetComponent** 函數已經找不到 **WeaponScript** 組件了，unity 還提供了 **GetComponentInChildren** 來在子孫節點中查找組件。**GetComponents** 和 **GetComponentsInChildren** 則會返回數組而非第一個找到的組件

```
#info="EnemyScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private WeaponScript[] weapons;

    void Awake()
    {
        // 腳本實例被加載時 執行一次 來獲取武器
        weapons = GetComponentsInChildren<WeaponScript>();
    }

    void Update()
    {
        foreach (var weapon in weapons)
        {
            // 自動開火
            if (weapon != null && weapon.CanAttack)
            {
                weapon.Attack(true);
            }
        }
    }
}
```
![](assets/shoot_ok.gif)

你可以微調下參數，以改變敵人的攻擊效果，此外你可以多創建幾個 **Weapon** 的副本以使敵人可以同時向多個方向射擊

![](assets/shoot_two_dir.gif)

# 傷害玩家

現在章魚看起來很危險，但它並不會造成傷害，因爲Player還沒有被添加 **HealthScript** 腳本，請添加 並確保 **IsEnemy** 沒有被勾選，運行遊戲如果被章魚的射擊物打到 Player 就會被消滅

![](assets/player_no_enemy.png)

![](assets/player_die.gif)

# Player 和 Enemy 碰撞

Player 和 Enemy 之間如果碰撞到一起沒有反應會相當乏味，碰撞兩個非 Trigger 的 Colliders 2D 碰撞需要編寫 OnCollisionEnter2D 事件

我們在 PlayerScript 中 添加處理

```
#info="PlayerScript.cs"

void OnCollisionEnter2D(Collision2D collision)
{
		bool damagePlayer = false;

		// 獲取碰撞體上 掛載的 EnemyScript
		EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript>();
		if (enemy != null)
		{
				// 殺手敵人
				HealthScript enemyHealth = enemy.GetComponent<HealthScript>();
				if (enemyHealth != null)
				{
						enemyHealth.Damage(enemyHealth.hp);
				}

				damagePlayer = true;
		}

		// 敵人對玩家造成傷害
		if (damagePlayer)
		{
				HealthScript playerHealth = this.GetComponent<HealthScript>();
				if (playerHealth != null)
				{
						playerHealth.Damage(1);
				}
		}
}
```

# 彈藥池

如果你在製作一款需要大量彈藥的遊戲，射擊時 Instantiate 函數實例化的彈藥將是一個巨大的消耗，爲此你可以選擇使用池化技術，簡單來說就是在一個彈藥池中保存實例化的彈藥，如果池中有彈藥就不創建新的直接取出來用，用完也不銷毀而是重新放回池中以供重複使用

**注意** Instantiate 方法會產生大量的消耗

# 子彈行爲

有一些庫像是 BulletML，可以讓你很容易的定義一些複製壯觀的子彈模式

![](assets/screenshot-02.png)

如果你正在製作一款射擊遊戲 可以看下 [BulletML for Unity](https://pixelnest.io/docs/bulletml-for-unity/) 插件

# 延遲射擊

添加一些武裝敵人到場景裏，然後運行遊戲，你應該會看到所有敵人都步驟一致的攻擊移動，你可以爲武器簡單的增加延遲，冷卻時間初始化爲大於0的數值即可，你可以使用一個算法或簡單的使用一個隨機數替代，敵人的速度也可以使用一個隨機數替代。一切都取決與你向如何實現

![](assets/result.png)