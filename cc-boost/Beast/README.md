# [Beast](https://www.boost.org/doc/libs/1_76_0/libs/beast/doc/html/index.html)

* Author(s) -> Vinnie Falco
* First Release -> 1.66.0
* C++ Standard Minimum Level -> 11
* Categories -> Concurrent Programming, Input/Output

beast 是僅使用c++11 和 asio 實現的 http1.1 服務器/客戶端 工具

```
#include <iostream>
#include <boost/bind/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/exception/all.hpp>
#include <boost/beast.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/core.hpp>
typedef boost::asio::io_context io_context_t;
typedef boost::asio::ip::tcp::acceptor acceptor_t;
typedef boost::asio::ip::tcp::socket socket_t;
typedef boost::beast::tcp_stream tcp_stream_t;
#define LISTEN_ADDRESS "0.0.0.0"
#define LISTEN_PORT 9000
template <typename Request>
void send_response(tcp_stream_t &stream, Request &req, const char *mintype, std::string &&body, boost::asio::yield_context yield)
{
    if (req.method() == boost::beast::http::verb::head || body.empty())
    {
        boost::beast::http::response<boost::beast::http::empty_body> res{boost::beast::http::status::ok, req.version()};
        res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(boost::beast::http::field::content_type, mintype);
        res.content_length(0);
        res.keep_alive(req.keep_alive());
        boost::beast::http::async_write(stream, res, yield);
    }
    else
    {
        boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::ok, req.version()};
        res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(boost::beast::http::field::content_type, mintype);
        res.content_length(body.length());
        res.body() = std::move(body);
        res.keep_alive(req.keep_alive());
        boost::beast::http::async_write(stream, res, yield);
    }
}
void http_routine(tcp_stream_t &stream, boost::asio::yield_context yield)
{
    boost::beast::flat_buffer buffer;
    for (;;)
    {
        // 設置超時
        stream.expires_after(std::chrono::seconds(30));
        // 讀取請求
        boost::beast::http::request<boost::beast::http::string_body> req;
        boost::beast::http::async_read(stream, buffer, req, yield);

        // Make sure we can handle the method
        auto method = req.method();
        if (method != boost::beast::http::verb::get &&
            method != boost::beast::http::verb::head)
        {
            boost::beast::http::response<boost::beast::http::empty_body> res{boost::beast::http::status::bad_request, req.version()};
            res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
            res.set(boost::beast::http::field::content_type, "text/plain");
            res.content_length(0);
            res.keep_alive(req.keep_alive());
            boost::beast::http::async_write(stream, res, yield);
            continue;
        }
        auto target = req.target();
        if (target == "/json")
        {
            send_response(stream, req, "application/json", R"({"name":"json"})", yield);
            continue;
        }
        else if (target == "/xml")
        {
            send_response(stream, req, "application/xml", R"(<root><name>xml</name></root>)", yield);
            continue;
        }
        boost::beast::http::response<boost::beast::http::string_body> res{boost::beast::http::status::not_found, req.version()};
        res.set(boost::beast::http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(boost::beast::http::field::content_type, "text/plain");
        res.keep_alive(req.keep_alive());
        res.body() = "not found";
        res.content_length(res.body().length());
        boost::beast::http::async_write(stream, res, yield);
    }
}
void accept_routine(boost::asio::io_context &context, acceptor_t &acceptor, boost::asio::yield_context yield)
{
    for (;;)
    {
        try
        {
            // 接受一個新連接
            socket_t s(context);
            acceptor.async_accept(s, yield);
            // 啓動處理協程
            boost::asio::spawn(context, [stream = tcp_stream_t(std::move(s))](boost::asio::yield_context yield) mutable
                               {
                                   try
                                   {
                                       http_routine(stream, yield);
                                   }
                                   catch (const std::exception &e)
                                   {
                                       std::cerr << boost::diagnostic_information(e) << std::flush;
                                   }
                               });
        }
        catch (const std::bad_alloc &e)
        {
            std::cerr << e.what() << std::endl;
        }
        catch (const boost::system::system_error &e)
        {
            std::cerr << boost::diagnostic_information(e) << std::flush;
        }
    }
}
int main(int argc, char *argv[])
{
    try
    {
        boost::asio::io_context context;

        acceptor_t acceptor(context, boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(LISTEN_ADDRESS), LISTEN_PORT));
        // 啓動 accept 協程
        boost::asio::spawn(context, boost::bind(accept_routine, boost::ref(context), boost::ref(acceptor), boost::placeholders::_1));

        // 啓動工作線程
        auto concurrency = std::thread::hardware_concurrency();
        std::vector<std::thread> threads;
        for (size_t i = 1; i < concurrency; i++)
        {
            threads.emplace_back([&context]
                                 { context.run(); });
        }
        context.run();
    }
    catch (const std::exception &e)
    {
        std::cerr << boost::diagnostic_information(e) << std::flush;
    }
    return 0;
}
```