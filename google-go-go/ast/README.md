# [ast](https://pkg.go.dev/go/ast)

ast 包保存了 golang 的語法樹類型

# Type

在語法樹中有如下接口表示類型定義

* [Ident](https://pkg.go.dev/go/ast#Ident)
* [SelectorExpr](https://pkg.go.dev/go/ast#SelectorExpr)
* [StarExpr](https://pkg.go.dev/go/ast#Ident)
* [ChanType](https://pkg.go.dev/go/ast#ChanType)
* [ArrayType](https://pkg.go.dev/go/ast#ArrayType)
* [FuncType](https://pkg.go.dev/go/ast#FuncType)
* [InterfaceType](https://pkg.go.dev/go/ast#InterfaceType)
* [StructType](https://pkg.go.dev/go/ast#StructType)
* [MapType](https://pkg.go.dev/go/ast#MapType)


## [Ident](https://pkg.go.dev/go/ast#Ident)

Ident 表示了一個最簡單的 類型標識符，其 Name 屬性記錄了標識符的名稱

```
int
Cat
```

## [SelectorExpr](https://pkg.go.dev/go/ast#SelectorExpr)

SelectorExpr 表示了一個從其它包導入的類型，其 Sel 屬性是一個 Ident 記錄了引用的包名，其 X 屬性也是一個 Ident 記錄了類型名稱

```
bytes.Buffer
```

## [StarExpr](https://pkg.go.dev/go/ast#Ident)

StarExpr 表示一個 \* 的指針類型，其 X 屬性是一個 Expr 記錄了指針指向的數據類型

```
* int
* bytes.Buffer
* chan any
```

## [ChanType](https://pkg.go.dev/go/ast#ChanType)

ChanType 表示一個 chan，Dir 屬性記錄了 chan 的可讀寫定義，Value 屬性是一個 Expr 記錄了 chan 中傳遞的數據類型

```
chan int
chan<- int
<-chan int
```

## [ArrayType](https://pkg.go.dev/go/ast#ArrayType)

ArrayType 表示一個 數組/切片，Elt 屬性是一個 Expr 記錄的 數組/切片 存儲的數據類，Len 是一個 Expr 如果爲 nil 表示這是一個切片否則這是一個數組 Len 記錄了數組長度

Len 可能會是如下類型

* \*ast.BasicLit 字面常量數組

	```
	[2]int
	```
	
* \*ast.Ident 由常量變量指定數組

	```
	[Count]int
	```
	
* \*ast.SelectorExpr 導入的常量指定數組

	```
	[abc.Count]int
	```
	
## [FuncType](https://pkg.go.dev/go/ast#FuncType)

FuncType 表示一個函數 TypeParams/Params/Results 三個屬性都是 FieldList 分別記錄了 模板參數 傳入參數 返回值

```
func ()
func (int) int
```

## [InterfaceType](https://pkg.go.dev/go/ast#InterfaceType)

InterfaceType 表示一個接口，Methods 記錄了接口包含的方法，每個 Method 的 Type 都是一個 FuncType
```
interface {
	Speak()
	Ext() int
}
```

## [StructType](https://pkg.go.dev/go/ast#StructType)

StructType 表示一個 struct， Fields 記錄了包含的所有字段，每個 Field 的記錄了字段名稱和 Type 記錄字段類型

```
struct {
	speak func()
	eat   func()
	x     int
}
```

## [MapType](https://pkg.go.dev/go/ast#MapType)

MapType 表示了一個 map， Key/Value 的是 Expr，分別記錄了 key 和 value 的類型

```
map[int]int
```