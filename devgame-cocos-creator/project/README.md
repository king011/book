# 項目結構

* assets
* library
* local
* settings
* temp
* project.json

## assets 資源檔案夾

assets 用於存放 遊戲中所有 本地資源 腳本 第三方庫  
只有在此 檔案夾下 的 才會顯示到 資源管理器中

assets 中 每個 檔案 導入項目後都會 產生一個同名的 .meta 檔案 用於存儲 導入信息 以及 和其它資源的 管理

## library 資源庫

library 下的資源是 assets中導入資源後自動產生的 其會被打包到最終的遊戲中

應該 在 git中 忽略掉此 檔案夾

## local 本地設置

local 保存了 項目本地設置 如 編輯器面板佈局 編輯器窗口大小 日誌 ...

應該 在 git中 忽略掉此 檔案夾

## settings

settings 保存了 項目設置 如 場景 發佈平臺 ...

## project.json

cocos 使用 project.json 和 assets 檔案夾 辨別一個項目

## build

構造發佈項目時的 發佈路徑

應該 在 git中 忽略掉此 檔案夾