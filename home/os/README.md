# linux
* [linux](os-linux/0) linux 基礎知識
    * [bash script](os-linux-bash/0)
    * [vim](os-linux-vim/0)
    * [vim-plug](os-linux-vim-plug/0) vim 插件
* [linux 服務器](os-linuxserver/0) linux 服務器相關
* linux 各發行版本
    * [Ubuntu](os-linux-ubuntu/0)
    * [OpenWrt](os-linux-OpenWrt/0)
    * [alpine](os-linux-alpine/0)
* 工具 
    * [rsync](os-linux-rsync/0) 檔案同步
    * [aria2](os-linux-aria2/0) 下載工具 
    * [sdkman](os-linux-sdkman/0) sdk 管理
    * [wine](os-linux-wine/0) wine 
    * [cmus](os-linux-cmus/0) 音頻播放
    * [ffmpeg](os-linux-ffmpeg/0) 音視頻處理
    * [kazam](os-linux-kazam/0) 屏幕錄製
    * [peek](os-linux-peek/0) 屏幕錄製gif
* app 打包工具
	* [AppImage](os-linux-AppImage/0) 
# 跨平臺工具
* 視頻
	* [youtube-dl](os-m-video-youtube-dl/0) youtube 下載
	* [yt-dlp](os-m-video-yt-dlp/0) youtube 下載
	* [you-get](os-m-video-you-get/0) 網頁視頻下載


# android tv
* [開機廣告](os-android-tvad/0)
* [安裝apk](os-android-tvapk/0)

# 虛擬機
* [QEMU](os-QEMU/0)
* [virtualbox](os-virtualbox/0)
* [vmware](os-vmware/0)


# usb

* [rufus](usb-rufus/0)
* [UBetbootin](usb-UBetbootin/0)