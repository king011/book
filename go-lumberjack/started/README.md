# Logger

* Logger 實現了 io.WriteCloser 接口，用於寫入指定的檔案名
* 在第一次寫入時打開或創建日誌檔案。如果檔案小於 MaxSize 則打開並追加到檔案尾。如果檔案大於或等於 MaxSize，則備份此檔案並房間一個新的檔案。
* 每當創建新日誌檔案時，舊的備份檔案可能會被刪除。如果 MaxBackups 和 MaxAge 都爲0，則不會刪除舊的日誌檔案

```
type Logger struct {
    // 要寫入的日誌名稱。 備份日誌將保留在同一檔案夾中。
    // 如果爲空白則在 os.TempDir() 檔案夾中存儲名爲 <processname>-lumberjack.log 的日誌檔案
    Filename string `json:"filename" yaml:"filename"`

    // 輪轉之前日誌的最大大小(單位:mb)，默認是 100mb
    MaxSize int `json:"maxsize" yaml:"maxsize"`

    // 工具檔案名中編碼的時間戳保留日誌檔案的最大天數。
    // 一天被定義爲24小時，由於 夏令時 閏秒等原因，一天可能與日曆不完全對應。
    // 默認情況下，不會根據時間刪除舊的日誌檔案
    MaxAge int `json:"maxage" yaml:"maxage"`

    // 要保留的日誌備份檔案數量。默認保留所有舊的日誌檔案(仍然可能因爲 MaxAge 刪除)
    MaxBackups int `json:"maxbackups" yaml:"maxbackups"`

    // 如果爲 true 則檔案名的時間戳將使用本地時間而非 UTC 時間
    LocalTime bool `json:"localtime" yaml:"localtime"`

    // 如果爲 true 則在創建備份日誌時使用 gzip 進行壓縮
    Compress bool `json:"compress" yaml:"compress"`
}
```

# Close
實現了io.Closer 接口，會關閉當前日誌檔案

```
func (l *Logger) Close() error
```

# Rotate

立刻關閉當前檔案，輪替到下一個日誌檔案

```
func (l *Logger) Rotate() error
```

# Write

實現了 io.Writer 接口。如果寫入日誌會導致日誌大小大於 MaxSize,則會關閉並重命名檔案(當前時間戳)，並使用原始檔案名創建一個新的日誌檔案。如果寫入長度大於 MaxSize 則返回錯誤

```
func (l *Logger) Write(p []byte) (n int, err error)
```