# [extends Error](https://stackoverflow.com/questions/41102060/typescript-extending-error-class/48342359#48342359)

es5 中內置的 Error 構造會破壞 prototype chain, 這導致的結果是如果子類 CustomError extends Error, 當你 new CustomError 後會最終變成僅僅 new Error 所以實例 instanceof CustomError 會返回 false 也不能得到 CustomError 提供的方法屬性, es6 則沒有這一問題

解決方案是使用 setPrototypeOf 來恢復 proto chain

```
class CustomError extends Error {
  constructor(message?: string) {
    // 'Error' breaks prototype chain here
    super(message); 

    // restore prototype chain   
    Object.setPrototypeOf(this, CustomError); 
  }
}
```

但是這會導致硬編碼，並且所有從 CustomError 派生的子類都需要再硬編碼修復 chain，因爲現在 CustomError 的子類調用 super 時，CustomError 也會破壞掉子類的 prototype chain    

爲此 typescript 引入了 new.target.prototype 記錄當前正確的 proto,你可以在CustomError 中使用 這個特性來恢復 chain,並且現在從 CustomError 派生的子類不會被破壞 prototype chain    

```
class CustomError extends Error {
  constructor(message?: string) {
    // 'Error' breaks prototype chain here
    super(message); 

    // restore prototype chain   
    const actualProto = new.target.prototype;

    if (Object.setPrototypeOf) { Object.setPrototypeOf(this, actualProto); } 
    else { this.__proto__ = actualProto; } 
  }
}
```