# args

args 是 dart 官方團隊維護的一個 命令行 解析庫

[https://pub.dartlang.org/packages/args#-installing-tab-](https://pub.dartlang.org/packages/args#-installing-tab-)


# 參數解析

```
import 'package:args/args.dart';

main(List<String> args) {
  var parser = ArgParser();

  parser.addOption(
    "addr", // 參數名
    abbr: "a", // 可選的 參數 簡寫名
    defaultsTo: "127.0.0.1:8080", // 默認值
    valueHelp: "hostname:port", // 顯示的 參數值 示例
    help: "tcp listen address", // 參數 說明
  );
  parser.addOption(
    "protocol",
    abbr: "p",
    allowed: ["h2c", "h2"], // 有效值列表
    allowedHelp: {
      "h2c": "http2 not use tls",
      "h2": "http2 and safe tls",
    }, // 有效值 對應的 說明信息
    help: "server protocol",
  );
  // bool 參數
  parser.addFlag(
    "help",
    abbr: "h",
    help: "display help",
  );
  // 解析 參數
  final results = parser.parse(args);

  // 獲取 解析的 參數
  if (results.wasParsed("help")) {
    // wasParsed 只能 判斷 Flag
    // wasParsed Option 將 拋出異常

    // 打印 使用 說明
    print(parser.usage);
  } else {
    // 獲取 參數 值
    final addr = results["addr"];
    final protocol = results["protocol"];

    print("tcp work at : $addr $protocol");
  }
}
```

# 子命令
```
import 'package:args/args.dart';

main(List<String> args) {
  var parser = ArgParser();

  parser.addOption(
    "addr", // 參數名
    abbr: "a", // 可選的 參數 簡寫名
    defaultsTo: "127.0.0.1:8080", // 默認值
    valueHelp: "hostname:port", // 顯示的 參數值 示例
    help: "tcp listen address", // 參數 說明
  );
  parser.addOption(
    "protocol",
    abbr: "p",
    allowed: ["h2c", "h2"], // 有效值列表
    allowedHelp: {
      "h2c": "http2 not use tls",
      "h2": "http2 and safe tls",
    }, // 有效值 對應的 說明信息
    help: "server protocol",
  );
  // bool 參數
  parser.addFlag(
    "help",
    abbr: "h",
    help: "display help",
  );

  // 增加子命令
  final daemon = _commandDaemon();
  parser.addCommand(
    "daemon",
    daemon,
  );

  // 解析 參數
  final results = parser.parse(args);
  final command = results.command;
  if (command != null) {
    if (command.wasParsed("help")) {
      print(daemon.usage);
    } else {
      final conf = command["conf"];
      print("done ${command.name} $conf");
    }
    return;
  }
  // 獲取 解析的 參數
  if (results.wasParsed("help")) {
    // wasParsed 只能 判斷 Flag
    // wasParsed Option 將 拋出異常

    // 打印 使用 說明
    print(parser.usage);

    // 打印子命令
    if (parser.commands != null) {
      print("\ncommands:");
      parser.commands.keys.forEach((name) {
        print("   $name");
      });
    }
  } else {
    // 獲取 參數 值
    final addr = results["addr"];
    final protocol = results["protocol"];

    print("tcp work at : $addr $protocol");
  }
}

ArgParser _commandDaemon() {
  var cmd = ArgParser();
  cmd.addOption(
    "conf",
    abbr: "c",
    help: "configure file",
  );
  cmd.addFlag(
    "help",
    abbr: "h",
    help: "display help",
  );
  return cmd;
}
```