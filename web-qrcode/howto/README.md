# angular

創建一個 canvas 標籤用於繪製二維碼

```
<canvas #canvas></canvas>
```

調用 QRCode.toCanvas 繪製二維碼
```
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import QRCode from 'qrcode'

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.scss']
})
export class XXXComponent implements AfterViewInit {
  @ViewChild("canvas")
  private _canvas?: ElementRef
  ngAfterViewInit() {
    QRCode.toCanvas(this._canvas!.nativeElement, '二維碼文本')
  }
}

```