# 創建 Player 

在上篇文章我們添加了背景和一些場景道具，下面就可以添加一些有用的元素了 比如 Player

創建一個可操作的實體 player需要一些元素

* 一個精靈
* 控制它的方法
* 讓它和世界互動的方式

## 添加精靈

我們將使用下面的圖片

![](assets/player.png)

請其導入 unity 資源，之後用它創建一個 名爲 **Player** 的精靈放到  層級窗口的 Foreground 對象下面，最後將其 scale屬性改爲 (0.2,0.2,1)

## 添加 Box Collider

在 player 對象裏點擊 **Add Component** 按鈕，然後選擇 **Box Collider 2D**

這表示 player 將成爲一個 hitbox

你可以在場景窗口裏看到 Collider，並在 Inspector 使用 Size 屬性調整大小，此外你也可以在Inspector中點擊 Collider組件的 **Edit Collider** 你可以看到 綠色的邊框和控制點，拖到控制點來改變 hitbox 大小

將 player 的的 hitbox 大小修改爲 (10,10)，它相對於實際的 player來說足夠大，但仍然小於精靈大小

![](assets/hitbox.png)

請將 player 對象保存爲 prefab

### 多邊形碰撞體

除了矩形碰撞體 unity 也支持 圓形 橢圓等 以及更加精準的 多邊形，但 **Polygon Collider 2D** 效率低效但可以設置爲你想要的形狀，需要考慮是否值的使用

## RigidBody

最後我們爲 player 添加上 **Rigidbody 2D** 組件

現在運行遊戲，將會看到 player 掉下去

這會告訴物理引擎，如何控制對象，此外它還允許腳本中引發的碰撞事件

![](assets/failing_ship.gif)

這是由於 Rigidbody 受到了引力的影響(默認引力大小爲 9.81 即地球的引力)

重力可以被用到很多類型的遊戲裏，但本例子中我們並不需要，只需要在 Rigidbody 組件的中 將 **Gravity Scale** 的值設置爲 0即可。你還需要勾上 **Freeze Rotaion Z** 屬性(凍結旋轉 Z)避免因爲物理的關係讓player 旋轉

## 移動 Player

現在來寫點代碼，unity 使用 c# 作爲代碼，在 Scripts 檔案夾下創建一個 PlayerScript 腳本

你可以在腳本裏定義一些 Message 方法(unity 沒有使用 c#的繼承)，unity會在需要時調用

腳本默認帶有 **Start** 和 **Update** 方法，這裏有一部分常用函數列表:

* **Awake()** 當對象被創建時會被調用一次，它是經典構造函數的替代品
* **Start()** 在 Awake() 後執行，不同的是 Start() 方法在腳本禁用時不會被調用
* **Update()** 在主遊戲循環裏每幀都會被執行
* **FixedUpdate()** 在每個固定頻率調用。當使用物理引擎時，你需要使用這個方法替代 Update 方法上的對象操作
* **Destroy()** 在對象銷毀時被調用。這是是最後執行清理代碼的地方

同樣對於碰撞也包含一些函數：
* **OnCollisionEnter2D(CollisionInfo2D info)** 當另外一個碰撞器接觸到此碰撞器時調用
* **OnCollisionExit2D(CollisionInfo2D info)** 當另外一個碰撞器不再接觸到此碰撞器時調用
* **OnTriggerEnter2D(Collider2D otherCollider)** 當另外一個標記爲“Trigger”的碰撞器接觸到此碰撞器時調用
* **OnTriggerExit2D(Collider2D otherCollider)** 當另外一個標記爲“Trigger”的碰撞器不在接觸到此碰撞器時調用

```
#info="PlayerScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // 定義一個表示速度的屬性，設置爲 public 可以在 unity 可視化窗口中調整值
    public Vector2 speed = new Vector2(50, 50);
    // Start is called before the first frame update

    // 2 - Store the movement and the component
    private Vector2 movement;// 速度
    private Rigidbody2D rigidbodyComponent; // 緩存 剛體對象
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // 獲取用戶輸入
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");

        // 依據輸入設置 當前速度
        movement = new Vector2(
          speed.x * inputX,
          speed.y * inputY);
    }
    void FixedUpdate()
    {
        // 獲取組件並
        if (rigidbodyComponent == null)
        {
            rigidbodyComponent = GetComponent<Rigidbody2D>();
        }

        // 設置速度移動剛體
        rigidbodyComponent.velocity = movement;
    }
}
```

將 PlayerScript 添加到 player 對象，因爲 speed 是一個 public 屬性所以你可以在 unity 的 Inspector 窗口中直接調整速度

![](assets/player_value_tweak.png)

# 敵人

如果射擊遊戲沒有大量的敵人將毫無意義，讓我們使用章魚來創建敵人，這隻無辜的章魚叫做 **Poulpi**

![](assets/poulpi.png)

1. 將圖像資源導入到 unity
2. 使用導入資源創建一個新的 精靈
3. 將精靈的 **Sorting Layer** 設置爲 Enemies
4. 調整 scale 爲 (0.3,0.3,1)
5. 添加 **Box Collider 2D**，大小設置爲(4,4)
6. 添加 **Rigidbody 2D**，設置 **Gravity Scale** 爲0，勾選 **Freeze Rotaion Z**
7. 最後將其保存爲 Prefab

![](assets/enemy_definition.png)

接下來爲敵人創建一個移動的腳本 **MoveScript**

```
#info="MoveScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{
    // 移動速度
    public Vector2 speed = new Vector2(10, 10);

    // 移動方向
    public Vector2 direction = new Vector2(-1, 0);

    private Vector2 movement;
    private Rigidbody2D rigidbodyComponent;

    void Update()
    {
        // 再每幀都重設速度以便讓敵人保持一直移動
        movement = new Vector2(
                speed.x * direction.x,
                speed.y * direction.y);
    }
    void FixedUpdate()
    {
        if (rigidbodyComponent == null)
        {
            rigidbodyComponent = GetComponent<Rigidbody2D>();
        }

        // 將速度應用到剛體
        rigidbodyComponent.velocity = movement;
    }
}
```

![](assets/moving_enemy.gif)

如果把 player 移動到敵人面前，兩個精靈會碰到一起，它們只會相互阻擋，因爲我們還沒有定義碰撞的行爲