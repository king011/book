# [腳本執行順序](https://docs.cocos.com/creator/manual/zh/scripting/execution-order.html)

對於 通過 節點掛接的 組件腳本 按照以下順序執行

1. 先執行 組件 editor.executionOrder 屬性值(默認爲0) 小的 

  ts 需要使用 executionOrder 來標註

    ```ts
    const { ccclass, property, executionOrder } = cc._decorator;
    
    @ccclass
    @executionOrder(-1) 
    export default class NewClass extends cc.Component {
    }
    ```
		
1. 先執行 上面的 組件