# 線程池

libuv 提供了一個線程池 用於將同步操作模擬爲異步 主要用於檔案操作

1. 線程池默認大小爲 4
2. 使用 環境變量 UV\_THREADPOOL\_SIZE 改變線程池大小
3. UV\_THREADPOOL\_SIZE 最大值爲 UV\_THREADPOOL\_SIZE，1.30.0 將最大值由128該爲 1024
4. 線程將在第一次 uv\_queue\_work 時一次性創建全部線程


# uv\_queue\_work

* uv\_queue\_work 函數向線程池 提交一個 工作任務，線程池會使用空閒線程調用其 work\_cb
* 如果線程池沒有空閒線程，將等待由空閒線程後執行
* 可以使用 uv\_cancel 取消尚未執行的 work
* 當 work 執行完畢，會在 loop 線程上調用 after\_work\_cb 通知 工作完成

```
#include <iostream>
#include <uv.h>
#include <thread>
void work_cb(uv_work_t *req)
{
    size_t id = (size_t)req->data;
    std::cout << id << " work on:" << std::this_thread::get_id() << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(10));
}
void after_work_cb(uv_work_t *req, int status)
{
    size_t id = (size_t)req->data;
    if (status == UV_ECANCELED)
    {
        std::cout << id << " canceled on:" << std::this_thread::get_id() << std::endl;
    }
    else
    {
        std::cout << id << " comple on:" << std::this_thread::get_id() << std::endl;
        free(req);
    }
}
int main(int argc, char *argv[])
{
    std::cout << "loop on:" << std::this_thread::get_id() << std::endl;
    uv_loop_t *loop = uv_default_loop();

    setenv("UV_THREADPOOL_SIZE", "5", 1);

    for (size_t i = 0; i < 10; i++)
    {
        uv_work_t *worker = (uv_work_t *)malloc(sizeof(uv_work_t));
        worker->data = (void *)(i);
        uv_queue_work(loop, worker, work_cb, after_work_cb);
        if (i >= 5)
        {
            uv_cancel((uv_req_t *)worker);
        }
    }

    uv_run(loop, UV_RUN_DEFAULT);
    return 0;
}
```

# uv\_async\_t

工作線程需要將處理消息 發回給 loop 對此 libuv 提供 了 uv\_async\_t

1. 使用 uv\_async\_init 初始化 uv\_async\_t 並設定函數回調 當由數據傳來會喚醒 線程並回調此值
2. 在其它線程中使用 uv\_async\_send 發送消息給 uv\_async\_t(uv\_async\_send 是異步的會立刻返回)

uv\_async\_send 和 回調通知不是一一對應的 libuv 可能會組合多次 send 而只回調一次，libuv只保證每次 send 後至少回調一次