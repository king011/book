# [flex-wrap](https://www.primefaces.org/primeflex/flexwrap)

定義了 flex 如何 wrap 子元素



| Class | Properties |
| -------- | -------- |
| flex-wrap     | flex-wrap: wrap;     |
| flex-wrap-reverse     | flex-wrap: wrap-reverse;     |
| flex-nowrap     | flex-wrap: nowrap;     |

## flex-wrap

flex-wrap 使 flex 元素在必要時 wrap 子元素

![](assets/wrap.png)

```
<div class="card">
    <div class="flex flex-wrap card-container blue-container" style="max-width: 500px">
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 border-round" style="min-width: 200px; min-height: 100px">1</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 border-round" style="min-width: 200px; min-height: 100px">2</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 border-round" style="min-width: 200px; min-height: 100px">3</div>
    </div>
</div>
```
## flex-wrap-reverse

flex-wrap-reverse 類似 flex-wrap 但 wrap 的順序相反

![](assets/wrap-reverse.png)

```
<div class="card">
    <div class="flex flex-wrap-reverse card-container yellow-container" style="max-width: 500px">
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 border-round" style="min-width: 200px; min-height: 100px">1</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 border-round" style="min-width: 200px; min-height: 100px">2</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 border-round" style="min-width: 200px; min-height: 100px">3</div>
    </div>
</div>
```
## flex-nowrap

flex-nowrap 使 flex 元素不會 wrap 子元素

![](assets/nowrap.png)

```
<div class="card">
    <div class="flex flex-nowrap overflow-hidden card-container green-container" style="max-width: 500px">
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 border-round" style="min-width: 150px; min-height: 100px">1</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 border-round" style="min-width: 150px; min-height: 100px">2</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 border-round" style="min-width: 150px; min-height: 100px">3</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同