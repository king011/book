# 渲染器

flutter 支持兩種選擇器

* HTMl 使用 html css canvas svg 渲染，應用相對較小
* CanvasKit 將 skia 編譯成爲 WebAssembly 並使用 WebGL 渲染。在移動和桌面端保持一致，有較高的性能，以及降低不同瀏覽器渲染結果不一致的風險。但應用大小會比 HTMl 渲染增加大約 2MB

使用 --web-renderer 參數 可以指定渲染器

```
flutter run -d chrome --web-renderer html
flutter build web --web-renderer canvaskit
```

* auto (默認) 自動選擇渲染器。移動端瀏覽器選擇 HTML，桌面端瀏覽器 選擇 CanvasKit
* html 強制使用 HTML 渲染器
* canvaskit 強制使用 CanvasKit 渲染器