# [overflow](https://www.primefaces.org/primeflex/overflow)

overflow 定義了當內容超出其容器區域時如何顯示
| Class | Properties |
| -------- | -------- |
| overflow-auto     | overflow: auto;     |
| overflow-hidden     | overflow: hidden;     |
| overflow-visible     | overflow: visible;     |
| overflow-scroll     | overflow: scroll;     |
| overflow-x-auto     | overflow-x: auto;     |
| overflow-x-hidden     | overflow-x: hidden;     |
| overflow-x-visible     | overflow-x: visible;     |
| overflow-x-scroll     | overflow-x: scroll;     |
| overflow-y-auto     | overflow-y: auto;     |
| overflow-y-hidden     | overflow-y: hidden;     |
| overflow-y-visible     | overflow-y: visible;     |
| overflow-y-scroll     | overflow-y: scroll;     |

## auto

auto 表示只有在內容超出容器區域時才顯示滾動條

```
<div class="card">
    <div class="line-height-3 card-container blue-container">
        <div class="overflow-auto surface-overlay p-3 border-blue-500 border-2 border-round" style="height: 100px">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </div>
    </div>
</div>
```
## hidden
hidden 表示溢出容器的內容將被隱藏

```
<div class="card">
    <div class="line-height-3 card-container yellow-container">
        <div class="overflow-hidden surface-overlay p-3 border-yellow-500 border-2 border-round" style="height: 100px">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </div>
    </div>
</div>
```

## visible

visible 表示溢出內容將在容器外被繼續顯示

```
<div class="card">
    <div class="line-height-3 card-container green-container">
        <div class="overflow-visible surface-overlay p-3 border-green-500 border-2 border-round" style="height: 100px">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </div>
    </div>
</div>
```
## scroll
scroll 表示無論溢出與否始終顯示滾動條

```
<div class="card">
    <div class="line-height-3 card-container purple-container">
        <div class="overflow-scroll surface-overlay p-3 border-purple-500 border-2 border-round" style="height: 100px">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣 overflow 系列的 class 也支持響應式佈局，使用方式和 display 相同