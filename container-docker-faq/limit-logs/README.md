# 清理日誌

docker 日誌檔案存儲在 **/var/lib/docker/containers** 檔案夾下

使用下述指令 查詢日誌檔案大小並排序

```
du -d1 -h /var/lib/docker/containers | sort -h
```

使用下述指令 清空指定日誌

```
cat /dev/null > /var/lib/docker/containers/${Container_ID}/${Container_ID}-json.log
```

> 請將 Container_ID 替換爲容器 id

# 運行時限制日誌

在執行 docker run 時 可以傳入參數 設置 日誌

```
docker run -it --log-opt max-size=10m --log-opt max-file=3 redis
```

* max-size 指定當個日誌最大10m
* max-file 指定最多3 個日誌

# 全局配置

也可以在全局配置檔案中配置日誌限制

```
#info="/etc/docker/daemon.json"
{
    "log-driver":"json-file",
    "log-opts":{
        "max-size" :"10m",
        "max-file":"3"
    }
}
```

配置檔案對已經創建的容器無效需要 只對新 docker run 的 有效