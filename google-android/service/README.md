# 服務

服務 是長時間 在 後臺運行的 組件 

* 只有當內存不足 必須回收資源 用於顯示 用戶關注的 Activity時 服務才會被停止 
* 如果服務 bind 到用戶 關注的 Activity時 可以極大降低被停止的可能
* 如果服務聲明 聲明爲前臺運行 則 基本 不會 被停止
* 服務 處於 Started 狀態並長時間運行 android 會降低其 在任務類列表中的位置 極大概率 被停止 通常需要設計 如何優雅的重啓服務 以便資源可用時 被系統 重啓

## 服務 分類
* Started 通過 調用 startService 啓動的服務 服務一旦啓動 就會在後臺一直運行 即時創建她的組件已經被銷毀 除非調用 stopService 停止服務 或者 服務自己調用 stopSelf 停止 自己
* Bound 當組件調用 bindService 時會和服務建立 綁定 當所有和服務的綁定 都解除時 服務會被 停止 
 
 服務有 4中 重要的 方法 需要重寫
```
// 當服務第一次被創建時 系統會調用此 方法 應該在此進行 服務的 初始化
//
// 如果 服務 已經 啓動 則不會被在此調用
public void onCreate() ;

// 使用 startService 啓動服務 時 在 onCreate之後 系統會 調用 此方法
//
// 此時代表 有組件 向服務 提出了請求 通常 應該在此 進行 處理
public int onStartCommand(Intent intent, int flags, int startId) ;

// 使用 bindService 綁定服務 時 在 onCreate之後 系統會 調用 此方法 
//
// 此時代表 有組件 向服務 提出了請求 通常 應該在此 進行 處理
public IBinder onBind(Intent intent) ;

// 當服務被停止 前 系統 會調用此 方法 應該在此 釋放 服務 的資源
public void onDestroy();
```

系統都是在 啓動服務 的調用着線程 中 調用 上述 方法 所以 應該 在 onCreate 中 創建新的工作線程 在 onStartCommand/onBind 把 工作 提交到 工作線程

當然 也可以每次在 onStartCommand/onBind 中 沒每個請求 創建一個新線程

## 配置

服務 必須在 AndroidManifest.xml 中 使用 service 標籤 配置 才可使用 下面時一些支持的 屬性

* android:enabled 爲 true 服務才可以被 實例化 
* android:exported 爲 true 其它 程式 才可以調用此服務
* android:icon 服務顯示的圖標資源 如果不設置 則使用 當前程式使用的圖標
* android:name 實現服務的類目
* android:label 服務顯示名稱 如果不設置 則使用 當前程式名稱
* android:permission 
* android:process

# Service

所有 服務 都必須 從 [android.app.Service](https://developer.android.com/reference/android/app/Service) 派生

```
#info="ZooService"
package com.king011.example;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.util.Log;


public class ZooService extends Service {
    static public final String TAG="ZooService";
    HandlerThread thread = null;
    Handler handler = null;
    @Override
    public void onCreate()  {
        // 創建 工作 線程
        thread = new HandlerThread("work thread", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        handler = new Handler(thread.getLooper()){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case 1:
                        Log.i(TAG,"new service request");
                        break;
                    default:
                        Log.i(TAG,"unknow what "+Long.toString(msg.what));
                        break;
                }
            }
        };

        Log.i(TAG,"onCreate");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand");
        // 通知 線程 處理新的 服務請求
        handler.sendEmptyMessage(1);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"onDestroy");
        // 關閉 工作線程
        thread.quitSafely();
    }

    public ZooService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // 返回 null 則 不可綁定 服務
        return null;
    }
}
``` 

```
#info="MainActivity"
package com.king011.example;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static public final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void onStop(View view){
        Intent intent = new Intent(this,ZooService.class);
        stopService(intent);
    }

    public void onStart(View view)throws InterruptedException{
        Intent intent = new Intent(this,ZooService.class);
        startService(intent);
    }
}
```

onStartCommand 的返回值 定義了 服務被kill 系統如何處理

* START\_STICKY 如果 服務被kill 保留 服務狀態爲 Started 但不保留 Intent 在資源可用時 系統會重新調用 onStartCommand 啓動服務 如果此期間 沒有任何 onStartCommand 命令 則 Intent 爲null
* START\_NOT\_STICKY 服務被 kill 系統 不會重啓服務
* START\_REDELIVER\_INTENT 類似 START\_STICKY 不過會 保留 Intent 並以此 Intent 調用 onStartCommand

# IntentService
[android.app.IntentService](https://developer.android.com/reference/android/app/IntentService)

在 Service 的示例代碼中 創建了一個 工作隊列 並將 新的 服務 請求 發送 到隊列中 IntentService 其實已經實現了 這個 功能

> 不過稍有不同 IntentService 會在 處理隊列爲空時 停止 服務 而不是向 上面例子中 一直運行 消息處理 等待下個 消息

只需要 從 其 派生 並且 實現 onHandleIntent 即可 當 新請求到底時 IntentService 會自動將其 加入 工作隊列中 工作線程 從 工作隊列中 取到 任何 便會 調用 onHandleIntent

```
package com.king011.example;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;


public class ZooService extends IntentService {
    static public final String TAG="ZooService";
    ZooService(){
        super("ZooService");
    }
    @Override
    protected void onHandleIntent( Intent intent) {
        Log.i(TAG,"new service request");
    }
}
```

除了 onBind 如果 重寫 onCreate onStartCommand onDestroy 都需要調用 IntentService 中的 同名方法 以便 IntentService 正確工作

# 綁定 服務

使用 bindService 可以 和 服務 進行綁定 此時服務狀態爲 Bound 當 服務沒有任何 綁定時 會被銷毀

服務 可以同時是 Started 和 Bound 

對於 Bound 服務 需要 實現 onBind 並且返回 一個 IBinder 接口 供 綁定者 和 服務通信

android 提供了 三種 方式 來和 服務通信

* 繼承 Binder 類
* 使用 Messenger 
* 使用 AIDL

## bindService

* bindService 會 綁定 服務 並建立 連接 
* bindService 會立即返回 當 連接建立成功時 會 調用 ServiceConnection.onServiceConnected 通知 服務建立成功
* bindService 返回 true 則綁定 成功 正在創建連接 組件需要在後續調用 unbindService 解除綁定 如果 返回false則找不到可綁定的服務
* 多個 組件 都 bindService 時 只有 第一個 bind 會 觸發 服務的 onBind 來 創建 IBinder 後續 都直接 將此 IBinder 返回 給 組件


## Binder

當 服務 僅用於 私有應用時 推薦的使用 方式 (服務 和 綁定者 位於同進程)

1. 繼承 Binder 實現 一個 public 方法 訪問 服務提供的方法/或直接返回服務 實例
2. 通過 Binder/服務實例 和服務通信

```
#info="ZooService"
package com.king011.example;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;


public class ZooService extends Service {
    static public final String TAG="ZooService";
    private final IBinder binder = new LocalBinder();
    private int id = 0;
    // 派生 Binder
    public class LocalBinder extends Binder{
        // 返回 服務 實例
        ZooService getService(){
            return  ZooService.this;
        }
    }
    @Override
    public void onCreate()  {
        Log.i(TAG,"onCreate");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"onDestroy");
    }

    public ZooService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG,"onBind");
        return binder;
    }
    // 提供 public 供 bind 者 調用
    public int getID(){
        return id++;
    }
}
```

```
#info="MainActivity"
package com.king011.example;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static public final String TAG = "MainActivity";
    // 保存 服務 實例
    private ZooService zooService;
    // 到服務的 連接
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // 當 連接建立時 會被調用
            zooService = ((ZooService.LocalBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // 只有當 service 被系統 終止時會被 調用
            // 主動 調用 unbindService  不會 使用 onServiceDisconnected 被 調用
            zooService = null;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onUnbindService(View view){
        if(zooService == null){
            Log.i(TAG,"onUnbindService,not bind");
            return;
        }
        unbindService(connection);
        zooService = null;
    }

    public void onBind(View view) {
        Intent intent = new Intent(this,ZooService.class);
        bindService(intent,connection, Context.BIND_AUTO_CREATE);
    }
    public void onNumber(View view){
        if(zooService == null){
            Log.i(TAG,"onNumber,not bind");
            return;
        }
        // 調用 服務 提供的 方法
        int id = zooService.getID();
        Log.i(TAG,"id = "+Integer.toString((id)));
    }
}
```

## Messenger

Binder 不能 跨進程 如果 服務需要 跨進程 可以使用 Messenger 在進程間 通過 消息 來通信

```
#info="ZooService"
package com.king011.example;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;


public class ZooService extends Service {
    static public final String TAG="ZooService";
    private int id = 0;
    // 派生 Binder
    public class LocalBinder extends Binder{
        // 返回 服務 實例
        ZooService getService(){
            return  ZooService.this;
        }
    }
    @Override
    public void onCreate()  {
        Log.i(TAG,"onCreate");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"onDestroy");
    }

    public ZooService() {
    }

    // 創建 Messenger 接收 消息
    private final Messenger messenger = new Messenger(new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    // 訪問 服務方法
                    int id = getID();
                    try {
                        // 將 結果 通過消息 傳回給 調用者
                        msg.replyTo.send(Message.obtain(null, 1, id, 0));
                    }catch (RemoteException e){
                        e.printStackTrace();
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    });

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG,"onBind");
        return messenger.getBinder();
    }
    public int getID(){
        return id++;
    }
}
```

```
#info="MainActivity"
package com.king011.example;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static public final String TAG = "MainActivity";
    // 保存 服務 通信 Messenger
    private Messenger sendMessenger;
    // 接收 服務 傳來的 消息
    private Messenger recvMessenger = new Messenger(new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    Log.i(TAG,Integer.toString(msg.arg1));
                    break;
            }
            super.handleMessage(msg);
        }
    });
    // 到服務的 連接
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // 當 連接建立時 會被調用
            sendMessenger = new Messenger(service);

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // 只有當 service 被系統 終止時會被 調用
            // 主動 調用 unbindService  不會 使用 onServiceDisconnected 被 調用
            sendMessenger = null;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onUnbindService(View view){
        if(sendMessenger == null){
            Log.i(TAG,"onUnbindService,not bind");
            return;
        }
        unbindService(connection);
        sendMessenger = null;
    }

    public void onBind(View view) {
        Intent intent = new Intent(this,ZooService.class);
        bindService(intent,connection, Context.BIND_AUTO_CREATE);
    }
    public void onNumber(View view){
        if(sendMessenger == null){
            Log.i(TAG,"onNumber,not bind");
            return;
        }
        // 傳遞消息 給服務
        Message msg = Message.obtain(null,1);
        msg.replyTo = recvMessenger; // 設置 消息 響應 Messenger
        try {
            sendMessenger.send(msg);
        }catch (RemoteException e){
            e.printStackTrace();
        }
    }
}
```