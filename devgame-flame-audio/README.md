# [audio](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/flame_audio.html)

播放音頻對於任何遊戲都是必不可少的，Flame 使用一個單獨的包 **flame_audio** 來處理聲音

```
flutter pub add flame_audio
```

```
dependencies:
  flame_audio: ^2.10.0
```

```
import 'package:flame_audio/flame_audio.dart';
```

* pub 倉庫 [https://pub.dev/packages/flame_audio](https://pub.dev/packages/flame_audio)
* 源碼 [https://github.com/flame-engine/flame_audio](https://github.com/flame-engine/flame_audio)

