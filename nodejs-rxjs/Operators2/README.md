# scan

Array 原生的 reduce 函數 接收一個 回調函數與 初始狀態 reduce 將每個元素 依次使用 回調函數調用 並返回 最終狀態

scan 完成 類似 功能

```
import { from } from 'rxjs'
import { scan } from 'rxjs/operators'

const arrs = [1, 2, 3, 4]
const sum = arrs.reduce((origin, next) => {
    return origin + next
}, 0)
console.log("sum =", sum)

// 1 3 6 10
from(arrs).pipe(
    scan((origin, next) => {
        return origin + next
    }, 0)
).subscribe(
    (v) => {
        console.log(v)
    },
)
```
scan 會在每次 獲取到值時 都 將計算的 狀態 發送

reduce 和 scan 的初始狀態 都可以不用 傳入 此時 會直接將 第一個值 作爲第一次 調用的 狀態返回

# buffer

* buffer 
* bufferCount
* bufferTime
* bufferToggle
* bufferWhen


buffer 將數據緩存到一個 Array中 直到另外一個 Observable 發來數據時 將 緩存 發送到當前 Observable 如果此時緩存爲iek則 發送一個 空 數組

源 Observable complete 但是 目標Observable沒有 發來數據時 不會將 緩存數據 發送 但是會 complete

```
import { interval } from 'rxjs'
import { buffer, take } from 'rxjs/operators'

// [ 0, 1, 2 ]
// [ 3, 4, 5 ]
// [ 6, 7, 8 ]
// [ 9, 10, 11, 12 ]
// complete
interval(300).pipe(
    take(15 + 1),
    buffer(interval(1000))
).subscribe({
    next(arrs) {
        console.log(arrs)
    },
    complete() {
        console.log("complete")
    },
})
```

bufferCount 當緩存 達到指定大小 才 發送數據

```
import { interval } from 'rxjs'
import { bufferCount, take } from 'rxjs/operators'

// [ 0, 1, 2 ]
// [ 3, 4, 5 ]
// [ 6, 7 ]
// complete
interval(300).pipe(
    take(8),
    bufferCount(3)
).subscribe({
    next(arrs) {
        console.log(arrs)
    },
    complete() {
        console.log("complete")
    },
})
```

bufferTime 將 一定時間內的數據 都緩存起來 發送 如果 此段時間沒有時間 發送 空 數組

```
import { interval } from 'rxjs'
import { take, bufferTime } from 'rxjs/operators'

// [ 0, 1, 2 ]
// [ 3, 4, 5 ]
// [ 6, 7 ]
// complete
interval(300).pipe(
    take(8),
    bufferTime(1000)
).subscribe({
    next(arrs) {
        console.log(arrs)
    },
    complete() {
        console.log("complete")
    },
})
```

bufferToggle 傳入一個 openings 和 closingSelector 

bufferToggle會收集 openings 到 closingSelector 間 的 緩存 並發送

```
import { interval } from 'rxjs'
import { bufferToggle, first } from 'rxjs/operators'

interval(300).pipe(
    bufferToggle(
        interval(1000), // 收集 每秒  緩存
        () => {
            return interval(1200).pipe(first()) // 每次收集 1200ms
        } ,
    )
).subscribe({
    next(arrs) {
        console.log(arrs)
    },
})
```




bufferWhen 傳入一個回調函數 此回調返回一個 Observable 每當此 Observable 發送數據時 將 緩存 發送

```
import { interval } from 'rxjs'
import { take, bufferWhen } from 'rxjs/operators'

// [ 0, 1, 2 ]
// [ 3, 4, 5 ]
// [ 6, 7 ]
// complete
interval(300).pipe(
    take(8),
    bufferWhen(() => {
        return interval(1000)
    })
).subscribe({
    next(arrs) {
        console.log(arrs)
    },
    complete() {
        console.log("complete")
    },
})
```

# delay

delay 傳入一個時間點 會 等待的時間段 可以延遲數據的發送

```
import { of } from 'rxjs'
import { delay } from 'rxjs/operators'
console.log("start")
of(1, 2, 3).pipe(
    delay(1000)
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# delayWhen

delayWhen 傳入一個 回調 在回調中返回一個 Observable 對每個元素都進行 單獨的延遲處理

```
import { of, empty } from 'rxjs'
import { delayWhen, delay } from 'rxjs/operators'
console.log("start")
of(4, 5, 6).pipe(
    delayWhen((value, index) => {
        return empty().pipe(delay(1000 * (index + 1)))
    })
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# debounce debounceTime

debounceTime 通常用於去抖動 debounceTime 會在一定時間內 沒有新值產生 才會 將 最後收到的值 發送出去

很時候用來做 自動完成組件 必須等待一定時間內沒有新輸入 才查詢自動完成的關鍵字

```
import { interval } from 'rxjs'
import { debounceTime, take } from 'rxjs/operators'
interval(300).pipe(
    take(4),
    debounceTime(1000)
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

debounce 類似  debounceTime 不過不是以時間 計算 而是 返回另外一個 Observable 來確定抖動時間

```
import { interval, empty } from 'rxjs'
import { debounce, take, delay } from 'rxjs/operators'
interval(300).pipe(
    take(4),
    debounce((v) => {
        return empty().pipe(delay(1000))
    })
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# throttle throttleTime
throttle throttleTime 類似 debounce debounceTime

throttleTime 在接收到數據後 一段時間內忽略接收到的新值 並在時間到期後 將值發送出去

```
import { interval } from 'rxjs'
import { throttleTime } from 'rxjs/operators'
interval(300).pipe(
    throttleTime(1000)
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# distinct

distinct 用來過濾重複值 會將 已經出現過的值 忽略掉

```
import { interval, from } from 'rxjs'
import { distinct, zip } from 'rxjs/operators'

from(['a', 'b', 'c', 'a', 'b', 'd']).pipe(
    zip(interval(300), (x) => x),
    distinct(),
).subscribe({
    next(v) {
        console.log(v)
    }
})
```

distinct 默認 使用 == 比較 可以傳入一個 回調函數 指定如何比較相等

```
import { interval, from } from 'rxjs'
import { distinct, zip } from 'rxjs/operators'

from(['a', 'b', 'c', 'a', 'b', 'd']).pipe(
    zip(interval(300), (x, y) => {
        return {
            x: x,
            y: y,
        }
    }),
    distinct((v) => v.x),
).subscribe({
    next(v) {
        console.log(v)
    }
})
```

distinct 實際上會創建一個 Set 存儲已經出現過的值 故不然放一個 無線增長的 Observable 以免 內存溢出

distinct 第二個參數 是一個 Observable 用來通知 distinct 清空 內部的 Set

```
import { interval } from 'rxjs'
import { distinct, map } from 'rxjs/operators'

interval(300).pipe(
    map(v => v % 10),
    distinct(undefined, interval(4000)),
).subscribe({
    next(v) {
        console.log(v)
    }
})
```

# distinctUntilChanged

distinctUntilChanged 也會過濾掉重複數據 但  distinctUntilChanged 只會和最後的next出去的數據 進行比較

```
import { interval } from 'rxjs'
import { distinctUntilChanged, map } from 'rxjs/operators'

interval(300).pipe(
    map(v => {
        const result = v % 10
        return result % 2 == 0 ? result : result - 1
    }),
    distinctUntilChanged(),
).subscribe({
    next(v) {
        console.log(v)
    }
})
```

