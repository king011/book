# [direction](https://www.primefaces.org/primeflex/flexdirection)

direction 定義了 flex 如果排版內部元素

| Class | Properties |
| -------- | -------- |
| flex-row     | flex-direction: row;     |
| flex-row-reverse     | flex-direction: row-reverse;     |
| flex-column     | flex-direction: column;     |
| flex-column-reverse     | flex-direction: column-reverse;     |

## flex-row

flex-row 的子元素將水平顯示

![](assets/row.png)

```
<div class="card">
    <div class="flex flex-row flex-wrap card-container blue-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

## flex-row-reverse

flex-row-reverse 的子元素將水平顯示，但順序相反

![](assets/row-reverse.png)

```
<div class="card">
    <div class="flex flex-row-reverse flex-wrap card-container yellow-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-yellow-500 font-bold text-gray-900 border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-yellow-500 font-bold text-gray-900 border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-yellow-500 font-bold text-gray-900 border-round m-2">3</div>
    </div>
</div>
```

## flex-column

flex-column 的子元素垂直顯示

![](assets/column.png)

```
<div class="card">
    <div class="flex flex-column card-container green-container">
        <div class="flex align-items-center justify-content-center h-4rem bg-green-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center h-4rem bg-green-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center h-4rem bg-green-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

## flex-column-reverse

flex-column-reverse 的子元素垂直顯示，但順序相反

![](assets/column-reverse.png)

```
<div class="card">
    <div class="flex flex-column-reverse card-container purple-container">
        <div class="flex align-items-center justify-content-center h-4rem bg-purple-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center h-4rem bg-purple-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center h-4rem bg-purple-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```
# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同