# [net/url](https://pkg.go.dev/net/url)

```
import ""net/url""
```

# func

```
// 對 s 進行轉義，其結果可以安全的放到 url 查詢參數中
func QueryEscape(s string) string

// 對 s 進行轉義，其結果可以安全的放到 url 路徑中，並根據需要將特殊字符(包括 /) 替換爲 %AB
func PathEscape(s string) string 

// 是 QueryEscape 的反向行爲，將 任何 %AB 形式的字符串解碼，如果 % 後面沒有 AB 則返回錯誤
func QueryUnescape(s string) (string, error)

// 是 PathEscape 的反向行爲，將 任何 %AB 形式的字符串解碼，如果 % 後面沒有 AB 則返回錯誤
// 與 QueryUnescape 不同的是，不會將 '+' 轉義爲 ' '(空格)
func PathUnescape(s string) (string, error)
```

# Values

```
// 通常用於表示查詢參數或者表單的值
// 和 http.Header 不同 , Values 的 key 是區分大小寫的
type Values map[string][]string

// 將查詢參數解析爲 Values,如果沒有查詢參數則返回空的 Values
// ParseQuery 始終會返回一個 Values,即時解析中出現錯誤也是如此並且繼續解析後續數據,error 將會返回遇到的第一個錯誤
func ParseQuery(query string) (Values, error)

// 查找 key 的第一個值，如果不存在返回 空字符串
// 如果要獲取多個值則需要直接訪問 map
func (v Values) Get(key string) string

// 用 value 取代現有 key 的任何值
func (v Values) Set(key, value string) 

// 將值添加到鍵。 它附加到與鍵關聯的任何現有值。
func (v Values) Add(key, value string) 

// 刪除與 key 關聯的值
func (v Values) Del(key string)

// 返回 key 是否存在
func (v Values) Has(key string) bool 

// 將 values 按 key 排序編碼
func (v Values) Encode() string
```

# Userinfo

```
// Userinfo保存了不可變的 用戶名和密碼信息
type Userinfo struct {}

// 創建一個帶用戶名的 Userinfo
func User(username string) *Userinfo

// 創建一個帶用戶名和密碼的 Userinfo
func UserPassword(username, password string) *Userinfo

// 返回用戶名
func (u *Userinfo) Username() string

// 返回密碼
func (u *Userinfo) Password() (string, bool) 

// 返回 "username[:password]" 形式的字符串, username 和 password 中的特殊字符會被轉義 @/?:
func (u *Userinfo) String() string
```

# URL

URL 代表了一個解析的 URL(技術上，它是一個 URI 引用)

一個 url 的表現形式通常是

```
[scheme:][//[userinfo@]host][/]path[?query][#fragment]
```

在 scheme 後不以 **/** 開頭的 URL 形式是

```
scheme:opaque[?query][#fragment]
```


```go
// Path 字段以解碼形式存儲(例如 /%47%6f%2f 存儲爲 /Go/) 
// 從 Path 中無法分別路徑中的哪些 /和%2f 是原始 URL 中的 /和%2f 
// 這種區別很少重要，但當它重要時代碼應該使用 RawPath,一個可選獲取字段，只有在默認編碼與 Path 不同時才會設置
// Fragment 和 RawFragment 的關係也是如此
type URL struct {
	Scheme      string
	Opaque      string    // encoded opaque data
	User        *Userinfo // username and password information
	Host        string    // host or host:port
	Path        string    // 路徑相對路徑可以省略 './'
	RawPath     string    // encoded path hint (see EscapedPath method)
	ForceQuery  bool      //如果爲 true 即時沒有查詢參數也爲輸出字符串添加 '?'
	RawQuery    string    // 查詢參數，沒有前綴字符 '?'
	Fragment    string    // fragment for references, without '#'
	RawFragment string    // encoded fragment hint (see EscapedFragment method)
}

// 解析 rawURL 到 URL
// url 可以是相對的(一個 path，沒有 host) 或絕對的(從 scheme) 開始
// 在沒有明確 scheme 情況下解析 hostname 和 path 是無效的，但由於解析不明確，肯能不一定會返回錯誤
func Parse(rawURL string) (*URL, error) 

// 將原始的 url 解析爲 URL。它假定在 HTTP 請求中收到 url，因此 url 僅被解釋爲 絕對 URI 或者 絕對路徑
// 假定字符串 url 沒有 #fragment 後綴(Web 瀏覽器在將 URL 發送到 Web 服務器之前會去除 #fragment)
func ParseRequestURI(rawURL string) (*URL, error) 

// 返回 u.RawFragment 的轉義形式
// 通常 任何 fagment 都有多種可能的轉義形式
// 當 u.RawFragment 是 u.Fragment 的有效轉義時直接返回 u.RawFragment。否則忽略 u.Fragment 並重新計算 轉義
// String 使用這個函數構造其結果
func (u *URL) EscapedFragment() string

// 返回 u.Path 的轉義形式
// 通常 任何 path 都有多種可能的轉義形式
// 當 u.RawPath 是 u.Path 的有效轉義時直接返回 u.RawPath。否則忽略 u.RawPath 並重新計算 轉義
// String 和 RequestURI 使用這個函數構造其結果
func (u *URL) EscapedPath() string 

// 返回 u.Host，如果存在任何有效的端口號則返回去除端口號的結果
// 如果結果包含在方括號中，就像 IPv6 地址一樣，方括號將從結果中去除
func (u *URL) Hostname() string

// 返回 u.Host 中的端口號，如果不存在端口號則返回空白字符串
func (u *URL) Port() string

// return u.Scheme != ""
func (u *URL) IsAbs() bool

// 解析 u.RawQuery 並返回解析到的值，它會丟棄解析到的錯誤如果需要處理錯誤請使用 ParseQuery
func (u *URL) Query() Values

// 返回經過編碼後的 path?query 或者 opaque?query
func (u *URL) RequestURI() string

// 將 URL 重組爲有效的 URL 字符串
// 結果一般是以下形式之一
// 1. scheme:opaque?query#fragment
// 2. scheme://userinfo@host/path?query#fragment
//
// 如果 u.Opaque 非空使用第一種形式，否則使用第二種形式
// host 中的任何非 ASCII 字符都被轉義。String 使用 u.EscapedPath() 獲取路徑
//
// 在第二種形式中，適用以下規則
// - 如果 u.Scheme 爲空，則省略 scheme:
// - 如果 u.User 為 nil，則省略 userinfo@
// - 如果 u.Host 為空，則省略 host/
// - 如果 u.Scheme 和 u.Host 為空且 u.User 為 nil，省略整個 scheme://userinfo@host/
// - 如果 u.Host 非空且 u.Path 以 / 開頭，form host/path 不添加自己的 /
// - 如果 u.RawQuery 為空，則省略 ?query
// - 如果 u.Fragment 為空，則省略#fragment
func (u *URL) String() string

// 類似 String 但把 password 替換爲 `xxxxx`
func (u *URL) Redacted() string

// 根據 RFC 3986 第 5.2節，將 ref 解析爲絕對 URI
// ref 可以是絕對的或相對的。ResolveReference總是返回一個新的 URL 實例，即時返回的 URL 與 u 或 ref 相同
// 如果 ref 是絕對 URL 則 ResolveReference 會忽略 u 直接返回 ref 的副本
func (u *URL) ResolveReference(ref *URL) *URL

// u.ResolveReference(url.Parse(ref)) 的語法糖
func (u *URL) Parse(ref string) (*URL, error)

func (u *URL) MarshalBinary() (text []byte, err error)
func (u *URL) UnmarshalBinary(text []byte) error 
```