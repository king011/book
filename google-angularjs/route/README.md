# 路由

路由 由 $routeProvider 服務提供 將url #! 後的內容作爲 路由路徑

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">

    <title>angularjs 測試</title>

    <!-- 引入ng依賴 -->
    <script src="asset/js/angular.min.js"></script>
    <script src="asset/js/angular-route.min.js"></script>
</head>

<body ng-app='app'>
    <ul>
        <li><a href="#!/">home</a></li>
        <li><a href="#!/computers">computers</a></li>
        <li><a href="#!/printers">printers</a></li>
    </ul>
    <div ng-view></div>
</body>
<script>
    angular.module('app', ['ngRoute'])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
                .when('/', { template: 'This is the home page' })
                .when('/computers', { template: 'This is the computer classification page' })
                .when('/printers', { template: 'This is the printer page' })
                .otherwise({ redirectTo: '/' });
        }]);
</script>

</html>
```