# 界面設計

android 提供了 多種界面設計方案

* 使用 xml 配置
* 使用 java 代碼
* 混合 xml 配置 和 java 代碼

# 使用 xml 配置

1. **res/layout** 檔案夾下的 xml 檔案 會自動被 R.java 收錄 代碼中 提供 R.layout.檔案名 訪問此 佈局

  通常 在 Activity 的 onCreate 中 使用 setContentView(R.layout.檔案名) 設置佈局

1. **res/values** 下存在 **colors.xml strings.xml styles.xml** 三個檔案 分別用來定義 常量

	* colors 顏色
	* strings 字符串
	* styles 風格

	在 xml 中 使用 @color/name @string/name @style/name 訪問  
	在 java 中 使用 R.color.name R.string.name R.color.style 訪問
 

![](assets/xml.png)
```
#info="MainActivity"
package com.king011.socks5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onClick(View v) {
        Toast.makeText(MainActivity.this, "you are cerberus's soldier now", Toast.LENGTH_SHORT).show();
    }
}
```

```
#info="res/layout/activity_main.xml"
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/idae"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        style="@style/text"/>

    <Button
        android:id="@+id/button"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/join"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        android:onClick="onClick"/>

</android.support.constraint.ConstraintLayout>
```

```
#info="res/values/colors.xml"
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="colorPrimary">#008577</color>
    <color name="colorPrimaryDark">#00574B</color>
    <color name="colorAccent">#D81B60</color>
</resources>
```

```
#info="res/values/strings.xml"
<resources>
    <string name="app_name">Socks5</string>
    <string name="idae">Cerberus is an idea</string>
    <string name="join">Join Cerberus</string>
</resources>
```

```
#info="res/values/styles.xml"
<resources>

    <!-- Base application theme. -->
    <style name="AppTheme" parent="Theme.AppCompat.Light.DarkActionBar">
        <!-- Customize your theme here. -->
        <item name="colorPrimary">@color/colorPrimary</item>
        <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
        <item name="colorAccent">@color/colorAccent</item>
    </style>

    <style name="text">
        <item name="android:textSize">24dp</item>
        <item name="android:textColor">@color/colorPrimary</item>
    </style>
</resources>
```

# 使用 java 代碼
1. 創建一個 佈局 管理器
1. 爲 佈局 管理器 添加元素
1. 將 佈局 設置到 Activity
```
package com.king011.socks5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_main);

        // 創建一個 佈局

        LinearLayout layout = new LinearLayout(this);
        {
            layout.setOrientation(LinearLayout.VERTICAL);
            // 創建 一些 ui
            TextView text = new TextView(this);
            // 設置 文本 字體大小 字體顏色
            text.setText(R.string.idae);
            text.setTextSize(TypedValue.COMPLEX_UNIT_DIP,24);
            text.setTextColor(getResources().getColor(R.color.colorPrimary));
            // 設置 居中
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            params.gravity = Gravity.CENTER;
            text.setLayoutParams(params);

            Button btn = new Button(this);
            btn.setText(R.string.join);
            btn.setOnClickListener(new OnClickListener(){
                public void onClick(View v) {
                    Toast.makeText(MainActivity.this, "you are cerberus's soldier now", Toast.LENGTH_SHORT).show();
                }
            });

            // 將 ui 添加到 佈局
            layout.addView(text);
            layout.addView(btn);
        }
        // 設置佈局
        setContentView(layout);
    }

}
```

# 混合 xml 配置 和 java 代碼

推薦使用 xml 作爲主要佈局 而使用 java 代碼 動態改變 微效果

# 自定義 View

* View 是 所有 ui的 祖先類
* ViewGroup 是 View 的子類 用於 容納 View
* 要 自定義 View 從 View 或其子類 派生即可




