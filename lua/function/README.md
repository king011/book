# 函數

使用如下語法定義函數

```
optional_function_scope function function_name( argument1, argument2, argument3..., argumentn)
    function_body
    return result_params_comma_separated
end
```

* **optional_function_scope** 指定函數是全局函數還是局部函數，未設置參數默認爲全局函數，如果需要局部函數使用 **local** 關鍵字
* **function_name** 指定函數名稱
* **argumentXXX** 指定函數參數，多個參數使用逗號隔開，函數也可以沒有參數
* **function_body** 函數體，函數要執行的代碼
* **result_params_comma_separated** 函數返回值，lua 函數支持多個返回值


```
function swap(a, b)
    -- 函數支持多個返回值
    return b, a
end
function println(prefix, ...) -- 使用 ... 定義不定長參數
    for index, value in ipairs {...} do
        print(prefix .. index .. "=" .. value)
    end
end

-- 調用函數參數只有一個字符串可以省略括號
print "測試開始"

local a, b = swap("b", "a")
println("\t", a, b)
```

# 變長參數

變長參數使用 ... 定義使用 

* ipairs {...} 會迭代連續的數組
* pairs {...} 會迭代所有非 nil 元素
* select('#', …) 返回參數長度
* select(n, …) 返回第n個參數

所以要正確處理 帶 nil 的不定長參數需要使用 select

```
function generate()
    return "a", "b", nil, "c"
end
function printArray(...)
    local str = ""
    for _, value in ipairs {...} do
        str = str .. value .. " "
    end
    print(str)
end
function printMap(...)
    local str = ""
    for _, value in pairs {...} do
        str = str .. value .. " "
    end
    print(str)
end

function printArgs(...)
    local str = ""
    for i = 1, select("#", ...) do
        local v = select(i, ...)
        if v == nil then
            v = "nil"
        end
        str = str .. v .. " "
    end
    print(str)
end

printArray(generate()) -- a b
printMap(generate()) -- a b c
printArgs(generate()) -- a b nil c
```
# 多返回值
如果 多返回值 不是 一系列表達式 最後一個表達式 則 多返回值只返回一個值

```
function f2()
    return 1, 2
end
x, y, z = f2(), 20 --f2()返回一個值
print(x, y, z) -- 1 20 nil

x, y, z = 20, f2() --f2()返回兩個值
print(x, y, z) -- 1 20 2

x = {0, f2(), 3} --f2()返回一個值
print(x[1], x[2], x[3]) -- 0 1 3

x = {0, f2()} --f2()返回兩個值
print(x[1], x[2], x[3]) -- 0 1 2

x = {0, (f2())} --f2()返回一個值  () 強制將返回值只取一個
print(x[1], x[2], x[3]) -- 0 1 nil
```
# unpack

unpack 函數可以將一個數組 返回爲 n 個數字類似 golang 的 slice...

```
a, b = unpack {1, 2, 3, 4}
print(a .. " " .. b) -- 1 2
```

# 深入函數

函數只是存儲在 table 中的一個鍵

```
function printA()
    print("A")
end
printA()

printA = function()
    print("B")
end
printA()
```

lua 函數支持閉包

```
function Speak()
    local md = {
        name = "cat"
    }
    return md, function()
        print(md.name)
    end
end

local md, speak = Speak(md)
speak() -- cat
md.name = "dog"
speak() -- dog

_, speak1 = Speak(md)
speak1() --cat
speak() -- dog
```