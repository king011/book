# 時區和偏移

Luxon 支持時區


通常情況下不需要擔心時區。你的代碼在具有特定時區的計算機上運行，並且無需執行任何操作，一切都將在該時區始終如一的工作。當你想要跨區域做複雜的事情時，你必須考慮它。即便如此，這裏有一些提示可以幫助你避免必須仔細考慮時區的情況

1. 不要讓服務器使用本地時間。將它們配置爲使用 UTC 並編寫你的服務器代理使用 UTF。時間通常可以被認爲是一個簡單的紀元毫秒數；
2. 在系統之間使用 ISO 8601 通信，例如 “2017-05-15T13:30:34Z” 精確的表示了全局時間線上的毫秒
3. 在可能的情況下，僅將時區視爲格式問題；理想情況下，你的應用程序永遠不會知道它正在使用時間稱爲"9:00"，直到它被呈現給用戶
4. 儘量將時間處理集中在同一個客戶端中

# 術語

時區是一個令人頭疼的問題。Luxon有很多工具可以處理它們，但它們的複雜是無法迴避的。下面是一些 Luxon 強加的術語

1. offset 是本地時間和 UTC 時間之間的差異，例如 +5(hours) 或者 -12:30。它們可以直接以 分鐘 小時 或者 其組合形式表示。在這裏我們將使用小時
2. time zone 是一組地理位置相關聯的規則，用於確定在任何給定時間與 UTC 的本地偏移量。識別區域的最佳方法是通過 IANA 字符串，例如 **America/New\_York**。該區域表示皮耶羅爲 -5，除了在 3月 和 11 月之間它使用夏令時間偏移量爲 -4
3. fixed-offset time zone(固定時間偏移) 是永遠不會改變偏移的任何時區，例如 UTC。Luxon 直接支持固定偏移區域；它們被指定爲 UTC+7 你可以將其解釋爲 始終 offset +7
4. named offset(命名偏移) 是偏移量的時區特定名稱，例如**東部夏令時間**，它表示區域(美國 America/New\_York) 也表示當前偏移量(EST 表示 -5)。它們也是模棱兩可的（BST 既是英國夏令時間，又是孟加拉國標準時間）、未標準化和國際化（法國人會如何稱呼美國的 EST？）。 由於所有這些原因，在以編程方式指定時間時應避免使用它們。 Luxon 僅支持在格式化中使用它們。

# Specifying a zone

你可以通過多種方式指定 時區

| Type | Example | Description |
| -------- | -------- | -------- |
| IANA     | 'America/New_York'     | that zone     |
| system     | Text     | the system's local zone     |
| default     | 'default'     | the default zone set by Settings.defaultZone     |
| UTC     | 'utc'     | Universal Coordinated Time     |
| fixed offset     | 'UTC+7'     | a fixed offset zone     |
| Zone     | new YourZone()     | A custom implementation of Luxon's Zone interface (advanced only)
     |

## IANA support

IANA 使用字符串標識時區。例如 **Asia/Tokyo** **Asia/Taipei**

如果你指定了一個無效的時區，將會獲得一個無效的 DateTime

```
bogus = DateTime.local().setZone("America/Bogus");

bogus.isValid; //=> false
bogus.invalidReason; //=> 'unsupported zone'
```

# 默認系統時區

默認情況下，DateTime 實例使用本地時區，例如例子中本地時區是 **America/New\_York**

```
var local = DateTime.local(2017, 05, 15, 9, 10, 23);

local.zoneName; //=> 'America/New_York'
local.toString(); //=> '2017-05-15T09:10:23.000-04:00'

var iso = DateTime.fromISO("2017-05-15T09:10:23");

iso.zoneName; //=> 'America/New_York'
iso.toString(); //=> '2017-05-15T09:10:23.000-04:00'
```

# 在指定時區創建

```
var overrideZone = DateTime.fromISO("2017-05-15T09:10:23", { zone: "Europe/Paris" });

overrideZone.zoneName; //=> 'Europe/Paris'
overrideZone.toString(); //=> '2017-05-15T09:10:23.000+02:00'
```

注意兩點

1. 字符串中指定的日期和時間被解釋爲巴黎當地時間
2. 生成的 DateTime 時區也是 **'Europe/Paris'**


此外靜態方法 utc 專門用於輸入 UTC 時間，它返回 UTC 格式的 DateTime

```
var utc = DateTime.utc(2017, 05, 15, 9, 10, 23);

utc.zoneName; //=> 'UTC'
utc.toString(); //=> '2017-05-15T09:10:23.000Z'
```

# 包含偏移的字符串

某些輸入字符串可能會指定偏移或時區作爲本身的一部分。在這些情況下，Luxon 將時間解析爲使用該偏移量，但將生成的 DateTime 轉換爲系統的本地區域

```
var specifyOffset = DateTime.fromISO("2017-05-15T09:10:23-09:00");

specifyOffset.zoneName; //=> 'America/New_York'
specifyOffset.toString(); //=> '2017-05-15T14:10:23.000-04:00'

var specifyZone = DateTime.fromFormat(
  "2017-05-15T09:10:23 Europe/Paris",
  "yyyy-MM-dd'T'HH:mm:ss z"
);

specifyZone.zoneName; //=> 'America/New_York'
specifyZone.toString(); //=> '2017-05-15T03:10:23.000-04:00'
```

當然如果你現在指定了生成 DateTime 的時區，Luxon 會照你吩咐去做

```
var specifyOffsetAndOverrideZone = DateTime.fromISO("2017-05-15T09:10:23-09:00", {
  zone: "Europe/Paris"
});

specifyOffsetAndOverrideZone.zoneName; //=> 'Europe/Paris'
specifyOffsetAndOverrideZone.toString(); //=> '2017-05-15T20:10:23.000+02:00'
```

# 保留區域

最後一些解析函數允許將字符串中的區域保留爲 Datetime 的區域。請注意如果字符串僅提供了偏移量，則區域將是固定偏移量區域，因爲 Luxon 不知道是指哪個區域

```
var keepOffset = DateTime.fromISO("2017-05-15T09:10:23-09:00", { setZone: true });

keepOffset.zoneName; //=> 'UTC-9'
keepOffset.toString(); //=> '2017-05-15T09:10:23.000-09:00'

var keepZone = DateTime.fromFormat("2017-05-15T09:10:23 Europe/Paris", "yyyy-MM-dd'T'HH:mm:ss z", {
  setZone: true
});

keepZone.zoneName; //=> 'Europe/Paris'
keepZone.toString(); //=> '2017-05-15T09:10:23.000+02:00'
```

# 修改時區

setZone 函數運行創建更改時區的副本，它的時間戳不會改變

```
var local = DateTime.local();
var rezoned = local.setZone("America/Los_Angeles");

// different local times with different offsets
local.toString(); //=> '2017-09-13T18:30:51.141-04:00'
rezoned.toString(); //=> '2017-09-13T15:30:51.141-07:00'

// but actually the same time
local.valueOf() === rezoned.valueOf(); //=> true
```

keepLocalTime 可以修改時區並保持本地時間不變(修改時間戳)

```
var local = DateTime.local();
var rezoned = local.setZone("America/Los_Angeles", { keepLocalTime: true });

local.toString(); //=> '2017-09-13T18:36:23.187-04:00'
rezoned.toString(); //=> '2017-09-13T18:36:23.187-07:00'

local.valueOf() === rezoned.valueOf(); //=> false
```

# Accessors

```
var dt = DateTime.local();

dt.zoneName; //=> 'America/New_York'
dt.offset; //=> -240
dt.offsetNameShort; //=> 'EDT'
dt.offsetNameLong; //=> 'Eastern Daylight Time'
dt.isOffsetFixed; //=> false
dt.isInDST; //=> true
```