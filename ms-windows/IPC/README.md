# IPC$

```sh
# 建立 ipc 連接
net use \\127.0.0.1\ipc$ "12345678" /user:"administrator"

# 映射 共享檔案夾 到 z:
net use z: \\127.0.0.1\c$ 

# 刪除 映射
net use z: /del

# 查看 建立的 ipc 連接
net use

# 查看本地 共享項目
net share
```