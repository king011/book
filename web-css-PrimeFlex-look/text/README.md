# [color](https://www.primefaces.org/primeflex/textcolor)

color 用於指定字體顏色

| Class | Properties |
| -------- | -------- |
| text-primary     | color: var(--primary-color);     |
| text-white     | color: #ffffff;     |
| text-color     | color: var(--text-color);     |
| text-color-secondary     | color: var(--text-color-secondary);     |
| text-0     | color: var(--surface-0);     |
| text-50     | color: var(--surface-50);     |
| text-100     | color: var(--surface-100);     |
| text-200     | color: var(--surface-200);     |
| ...     | ...     |
| text-800     | color: var(--surface-800);     |
| text-900     | color: var(--surface-900);     |
| ...     | ...     |

更多的值和效果可以[在此](https://www.primefaces.org/primeflex/textcolor) 查看

![](assets/color.png)

```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container">
        <div class="text-500 w-12rem h-6rem surface-overlay border-round border-1 border-gray-500 font-bold m-2 flex align-items-center justify-content-center">text-500</div>
        <div class="text-cyan-500 w-12rem h-6rem surface-overlay border-round border-1 border-gray-500 font-bold m-2 flex align-items-center justify-content-center">text-cyan-500</div>
        <div class="text-orange-500 w-12rem h-6rem surface-overlay border-round border-1 border-gray-500 font-bold m-2 flex align-items-center justify-content-center">text-orange-500</div>
    </div>
</div>
```
## Pseudo States

可以使用 focus:/hover:/active: 前綴來指定僞狀態顏色

```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container">
        <div class="text-500 hover:text-700 w-12rem h-6rem surface-overlay border-round border-1 border-gray-500 font-bold m-2 flex align-items-center justify-content-center">hover:text-700</div>
        <div class="text-cyan-500 hover:text-cyan-700 w-12rem h-6rem surface-overlay border-round border-1 border-gray-500 font-bold m-2 flex align-items-center justify-content-center">hover:text-cyan-700</div>
        <div class="text-orange-500 hover:text-orange-700 w-12rem h-6rem surface-overlay border-round border-1 border-gray-500 font-bold m-2 flex align-items-center justify-content-center">hover:text-orange-700</div>
    </div>
</div>
```
# [text-decoration](https://www.primefaces.org/primeflex/textdecoration)

text-decoration 指定如何裝飾文本

| Class | Properties |
| -------- | -------- |
| underline     | text-decoration: underline;     |
| line-through     | text-decoration: line-through;     |
| no-underline     | text-decoration: no-underline;     |

![](assets/decoration.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem min-h-full flex align-items-center justify-content-center">
            <p class="underline">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem min-h-full flex align-items-center justify-content-center">
            <p class="line-through">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem min-h-full flex align-items-center justify-content-center">
            <a href="#" class="no-underline">Link with no underline</a>
        </div>
    </div>
</div>
```

## Pseudo States

可以添加 focus:/hover:/active: 前綴來指定僞狀態

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem min-h-full">
            <a href="#" class="no-underline hover:underline">Link with no underline</a>
        </div>
    </div>
</div>
```

# [text-align](https://www.primefaces.org/primeflex/textalign)

text-align 指定元素內文字如何對齊

| Class | Properties |
| -------- | -------- |
| text-center     | text-align: center;     |
| text-justify     | text-align: justify;     |
| text-left     | text-align: left;     |
| text-right     | text-align: right;     |

![](assets/align.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <div class="surface-overlay border-round border-1 w-16rem min-h-full shadow-1 p-5 py-0 m-3">
            <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis. Maecenas pharetra convallis posuere morbi leo urna molestie. </p>
        </div>
        <div class="surface-overlay border-round border-1 w-16rem min-h-full shadow-1 p-5 py-0 m-3">
            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis. Maecenas pharetra convallis posuere morbi leo urna molestie. </p>
        </div>
        <div class="surface-overlay border-round border-1 w-16rem min-h-full shadow-1 p-5 py-0 m-3">
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis. Maecenas pharetra convallis posuere morbi leo urna molestie. </p>
        </div>
        <div class="surface-overlay border-round border-1 w-16rem min-h-full shadow-1 p-5 py-0 m-3">
            <p class="text-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis. Maecenas pharetra convallis posuere morbi leo urna molestie. </p>
        </div>
    </div>
</div>
```

## 響應式佈局

text-align 和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同


# [text-overflow](https://www.primefaces.org/primeflex/textoverflow)
text-overflow 指定當內容溢出時如何顯示

| Class | Properties |
| -------- | -------- |
| text-overflow-clip     | text-overflow: clip;     |
| text-overflow-ellipsis     | text-overflow: ellipsis;     |

![](assets/overflow.png)

```
<div class="card">
    <div class="flex align-items-center justify-content-center card-container flex-column">
        <div class="surface-overlay border-round border-1 p-3 white-space-nowrap overflow-hidden text-overflow-clip" style="width:200px">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </div>

        <div class="surface-overlay border-round border-1 p-3 mt-3 white-space-nowrap overflow-hidden text-overflow-ellipsis" style="width:200px">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </div>
    </div>
</div>
```

## 響應式佈局

text-overflow 和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同

# [text-transform](https://www.primefaces.org/primeflex/texttransform)

text-transform 指定來如何轉換文字內容

| Class | Properties |
| -------- | -------- |
| lowercase     | text-transform: lowercase;     |
| uppercase     | text-transform: uppercase;     |
| capitalize     | text-transform: capitalize;     |

![](assets/transform.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem h-16rem flex align-items-center justify-content-center">
            <p class="lowercase">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem h-16rem flex align-items-center justify-content-center">
            <p class="uppercase">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
        <div class="surface-overlay border-round border-1 p-3 m-3 w-16rem h-16rem flex align-items-center justify-content-center">
            <p class="capitalize">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
    </div>
</div>
```