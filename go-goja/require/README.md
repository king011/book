# require

`github.com/dop251/goja_nodejs/require` 包 提供了 nodejs 兼容的 模塊

```
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/require"
)

func main() {
	registry := new(require.Registry) // registry 能夠被 多個 goja.Runtime 共用

	// 創建 虛擬機
	vm := goja.New()
	vm.Set(`println`, func(args ...interface{}) {
		fmt.Println(args...)
	})
	req := registry.Enable(vm) // 爲 Runtime 啓用模塊
	_, e := vm.RunString(`
var a = require("./a.js");
println('max =',a.max(1,2))
`)
	if e != nil {
		log.Fatalln(e)
	}

	_, e = req.Require(`./b.js`)
	if e != nil {
		log.Fatalln(e)
	}
	_, e = req.Require(`./b.js`)
	if e != nil {
		log.Fatalln(e)
	}
}
```

如果 模塊路徑爲 xxx require 會按照如下順序 查找模塊

1. xxx
2. xxx.js
3. xxx.json
4. xxx/package.json
5. xxx/index.js
6. xxx/index.json

默認的模塊查找路徑爲 

* $NODE_PATH
* $HOME/.node_modules
* $HOME/.node_libraries

require 模塊是以 require 傳入的參數 執行 filepath.Clean 後的值 作爲的 key 來緩存模塊 故要當心 即時 abs路徑一樣 但 filepath.Clean 模塊路徑 不一樣會被作爲兩個模塊加載

# Registry 屬性

Registry 提供了 多個可選的 參數 用於 指定 如何加載模塊

```
#info="main.go"
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/require"
)

func main() {
	var e error
	modules := []string{
		".",
		".node_modules",
	}
	for i, dir := range modules {
		modules[i], e = filepath.Abs(dir)
		if e != nil {
			log.Fatalln(e)
		}
	}

	// 創建 模塊 登記處
	registry := require.NewRegistry(
		// 設置 全局模塊 加載位置
		require.WithGlobalFolders(modules...),
		// 設置 自定義的 js 模塊加載器
		require.WithLoader(func(filename string) (b []byte, e error) {
			// 限定腳本 只能在 搜索路徑下
			e = require.IllegalModuleNameError
			for _, dir := range modules {
				if strings.HasPrefix(filename, dir) && filename[len(dir)] == filepath.Separator {
					e = nil
					break
				}
			}
			if e != nil {
				return
			}

			// 加載腳本
			log.Println(`load module from file`, filename)
			b, e = ioutil.ReadFile(filename)
			if e != nil {
				if os.IsNotExist(e) {
					// 不存在 返回 ModuleFileDoesNotExistError 將繼續 查找 下個模塊目錄
					e = require.ModuleFileDoesNotExistError
				}
				return
			}

			// 創建 __filename __dirname
			str := fmt.Sprintf(`(function(__filename,__dirname){%s})(%q,%q)`,
				b,
				filename, filepath.Dir(filename),
			)
			b = []byte(str)
			return
		}),
	)

	// 創建 虛擬機
	vm := goja.New()
	vm.Set(`println`, func(args ...interface{}) {
		fmt.Println(args...)
	})
	registry.Enable(vm) // 爲 Runtime 啓用模塊
	_, e = vm.RunScript(`main.js`, `
var b = require('b.js')
println(b.test())
`)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(registry)
}
```

```
#info="b.js"
println('this is b.js')
function test() {
    println('__filename', __filename)
    println('__dirname', __dirname)
    return "passed1";
}
exports.test = test;
```

# 原生模塊

* RegisterNativeModule 可以註冊一個 原生模塊 原生模塊 加載不應該失敗
* 故 不應該在 原生模塊加載時 設置 js 異常或中斷
* Registry.RegisterNativeModule 可以爲當前 Registry 註冊 原生模塊
* require.RegisterNativeModule 可以爲全部 Registry 註冊 原生模塊

```
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/require"
)

func main() {
	// 創建 模塊 登記處
	registry := require.NewRegistry()
	// 註冊 原生模塊
	registry.RegisterNativeModule(`fmt`, func(vm *goja.Runtime, module *goja.Object) {
		obj := module.Get("exports").(*goja.Object)
		obj.Set(`Println`, func(args ...interface{}) {
			fmt.Println(args...)
		})
	})

	// 創建 虛擬機
	vm := goja.New()
	registry.Enable(vm) // 爲 Runtime 啓用模塊
	_, e := vm.RunScript(`main.js`, `
var fmt = require('fmt')
fmt.Println(1,2,3,4,'cerebrus is an idea')
`)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(registry)
}
```