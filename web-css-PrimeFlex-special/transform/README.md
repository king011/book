# [translate](https://www.primefaces.org/primeflex/translate)

translate 系列的 class 通過指定 transform 來移動元素

| Class | Properties |
| -------- | -------- |
| translate-x-0     | transform: translateX(0%);     |
| translate-x-100     | transform: translateX(100%);     |
| -translate-x-100     | transform: translateX(-100%);     |
| translate-y-0     | transform: translateY(0%);     |
| translate-y-100     | transform: translateY(100%);     |
| -translate-y-100     | transform: translateY(-100%);     |

![](assets/translate.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center md:justify-content-between card-container blue-container">
        <div class="h-6rem w-6rem bg-blue-500 border-round-left">
            <img src="./assets/images/product-placeholder-blue.svg" class="translate-x-100 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem bg-blue-500">
            <img src="./assets/images/product-placeholder-blue.svg" class="translate-x-0 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem bg-blue-500 border-round-right">
            <img src="./assets/images/product-placeholder-blue.svg" class="-translate-x-100 h-6rem w-6rem" alt="primeflex">
        </div>
    </div>
</div>
```

# [transform-origin](https://www.primefaces.org/primeflex/transformorigin)

origin 系列的 class 通過指定 transform-origin 來指定元素的原點，原點將影響對元素的變換

| Class | Properties |
| -------- | -------- |
| origin-center     | transform-origin: center;     |
| origin-top     | transform-origin: top;     |
| origin-top-right     | transform-origin: top right;     |
| origin-right     | transform-origin: right;     |
| origin-bottom-right     | transform-origin: bottom right;     |
| origin-bottom     | transform-origin: bottom;     |
| origin-bottom-left     | transform-origin: bottom left;     |
| origin-left     | transform-origin: left;     |
| origin-top-left     | transform-origin: top left;     |

![](assets/origin.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center md:justify-content-between card-container blue-container" style="min-height: 250px">
        <div class="h-6rem w-6rem bg-blue-500 m-5 border-round">
            <img src="./assets/images/product-placeholder-blue.svg" class="origin-center rotate-45 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem bg-blue-500 m-5 border-round">
            <img src="./assets/images/product-placeholder-blue.svg" class="origin-top-right rotate-45 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem bg-blue-500 m-5 border-round">
            <img src="./assets/images/product-placeholder-blue.svg" class="origin-bottom-left rotate-45 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem bg-blue-500 m-5 border-round">
            <img src="./assets/images/product-placeholder-blue.svg" class=" origin-left rotate-45 h-6rem w-6rem" alt="primeflex">
        </div>
    </div>
</div>
```


# [rotate](https://www.primefaces.org/primeflex/rotate)

rotate 系列的 class 通過指定 transform 來旋轉元素

| Class | Properties |
| -------- | -------- |
| rotate-90     | transform: rotate(90deg);     |
| -rotate-90     | transform: rotate(-90deg);     |
| rotate-180     | transform: rotate(180deg);     |
| -rotate-180     | transform: rotate(-180deg);     |

![](assets/rotate.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center md:justify-content-between card-container blue-container">
        <div class="h-6rem w-6rem m-5">
            <img src="./assets/images/rotate-placeholder-blue.svg" class="rotate-90 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem m-5">
            <img src="./assets/images/rotate-placeholder-blue.svg" class="-rotate-90 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem m-5">
            <img src="./assets/images/rotate-placeholder-blue.svg" class="rotate-180 h-6rem w-6rem" alt="primeflex">
        </div>
        <div class="h-6rem w-6rem m-5">
            <img src="./assets/images/rotate-placeholder-blue.svg" class="-rotate-180 h-6rem w-6rem" alt="primeflex">
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同