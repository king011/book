# [鍵盤輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html)

這包括鍵盤輸入的文檔。

其他輸入文檔，另請參閱：

* [手勢輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html)：用於滑鼠和觸控指標手勢
* [其他輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html)：用於操縱桿、遊戲手把等。


# [Intro](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html#intro)


Flame 上的 keyboard API 依賴與 Flutter 的 [Focus widget](https://api.flutter.dev/flutter/widgets/Focus-class.html)

若要自訂焦點行為，請參閱[控制焦點](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html#controlling-focus)

遊戲可以通過兩種方式對鍵盤做出反應；在 Game 級別 或 Component 級別。對於每個級別都由一個可以添加到 Game 或 Component 的 mixin

# [在 Game 級別接收鍵盤輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html#receive-keyboard-events-in-a-game-level)

要在 Game 級別處理鍵盤輸入 **with KeyboardEvents** 即可，之後你就可以覆蓋 **onKeyEvent** 方法來處理按鍵，此函數接收兩個參數，第一個是首先觸發回調的 [RawKeyEvent](https://api.flutter.dev/flutter/services/RawKeyEvent-class.html)，第二個是一組當前按下的 [LogicalKeyboardKey](https://api.flutter.dev/flutter/services/LogicalKeyboardKey-class.html)。其返回值應該是 **KeyEventResult**

* **KeyEventResult.handled** 將告訴框架按鍵是在 Flame 內部解析的，並跳過除 GameWidget 之外的任何其它鍵盤處理程序的 widgets
* **KeyEventResult.ignored** 將告訴框架在除 GameWidget 之外的任何其它鍵盤處理程序 widgets 中繼續測試此事件。如果事件沒有被任何處理程序解析，框架將觸發 **SystemSoundType.alert**
* **KeyEventResult.skipRemainingHandlers** 與 **ignored** 非常相似，除了會跳過任何其它處理程序的 widget 並直接播放 **alert** 聲音

```
import 'package:flame/input.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  final myGame = MyGame();

  runApp(GameWidget(
    game: myGame,
  ));
}

class MyGame extends FlameGame with KeyboardEvents {
  @override
  KeyEventResult onKeyEvent(
    RawKeyEvent event,
    Set<LogicalKeyboardKey> keysPressed,
  ) {
    final isKeyDown = event is RawKeyDownEvent;
    final isSpace = keysPressed.contains(LogicalKeyboardKey.space);

    if (isSpace && isKeyDown) {
      if (keysPressed.contains(LogicalKeyboardKey.shiftLeft) ||
          keysPressed.contains(LogicalKeyboardKey.shiftRight)) {
        debugPrint('shoot harder');
      } else {
        debugPrint('shoot');
      }
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }
}
```

# [在 Component 級別接收鍵盤輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html#receive-keyboard-events-in-a-component-level)

要在組件級接收鍵盤輸入可以使用 **mixin KeyboardHandler**，與 Tappable Draggable 類似，KeyboardHandler 可以被 with 到 Component 的任意子類

**KeyboardHandler** 只能添加到 **with HasKeyboardHandlerComponents** 的 **Game** 中

> HasKeyboardHandlerComponents 和 KeyboardEvents 不能同時作用與 Game，即要麼從 Game 級別 要麼從 Component 級別獲取鍵盤輸入

**with KeyboardHandler** 後就可以覆蓋 **onKeyEvent** 處理鍵盤輸入了，其參數和 KeyboardEvents 中的參數一樣區別是返回值現在是一個 bool，如果返回 true 允許事件繼續在後續組件間傳播，返回 false 則會阻斷事件後續傳播

Flame還提供了一個名為KeyboardListenerComponent的預設實現，可用來處理鍵盤事件。 與任何其他元件一樣，它可以作為子元件添加到 FlameGame 或其他元件中：

例如，假設一個 PositionComponent 具有在 X 軸和 Y 軸上移動的方法，則可以使用下列程式碼將這些方法綁定到按鍵事件：

```
add(
  KeyboardListenerComponent(
    keyUp: {
      LogicalKeyboardKey.keyA: (keysPressed) { ... },
      LogicalKeyboardKey.keyD: (keysPressed) { ... },
      LogicalKeyboardKey.keyW: (keysPressed) { ... },
      LogicalKeyboardKey.keyS: (keysPressed) { ... },
    },
    keyDown: {
      LogicalKeyboardKey.keyA: (keysPressed) { ... },
      LogicalKeyboardKey.keyD: (keysPressed) { ... },
      LogicalKeyboardKey.keyW: (keysPressed) { ... },
      LogicalKeyboardKey.keyS: (keysPressed) { ... },
    },
  ),
);
```

# [控制焦點](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html#controlling-focus)

在 widget 級別，可以使用 FocusNode API 來控制遊戲是否聚焦和普通的 flutter widget 沒什麼區別，GameWidget 有一個可選的 focusNode 參數，允許從外部控制其焦點

默認情況下 GameWidget 的 **autofocus** 設置爲 true，要覆蓋這一行爲請將 autofocus 設置爲 false