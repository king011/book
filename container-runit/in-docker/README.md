# in docker
docker 等容器最初設計來爲一個進程提供隔離的執行環境，並且即時到了現在這一目的也沒改變。

但容器也能做一些更有趣的事情，比如用其替代虛擬機安裝日常軟件，或者在一個容器中打包多個進程，雖然這兩個目的不是容器主要要解決的生產問題，但對一般用戶可能存在不小的意義，這時使用 runit 就可以很好的 打包多個進行到容器並實現有效的管理與監督。
# Dockerfile

要構建一個使用 runit 管理服務的容器需要2步

1. 安裝 runit，對於 ubuntu 可以使用 apt 指令
2. 設置容器啓動指令爲 runit

```
FROM ubuntu:18.04
RUN set -eux; \
    apt-get update; \
    apt-get install -y runit; \
    rm -rf /var/lib/apt/lists/*;
CMD ["runit"]
```