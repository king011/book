# 最大檔案描述符

**/proc/sys/fs/file-max** 記錄了系統可打開的最大檔案數量 可以修改此值 臨時改變設定
```
cat /proc/sys/fs/file-max
1613518
```

在 **/etc/sysctl.conf** 中 添加 **fs.file-max** 可永久修改

```
# 修改打開檔案上限到 3.2 千萬
echo fs.file-max=32270360 >> /etc/sysctl.conf
# 使系統 重載配置
sysctl -p
```

使用 ulimit -n 查看當前用戶 打開檔案上限 
```
# 修改設定到 1百萬
ulimit -n 1048576
```

如果想要 永久生效 需要修改 **/etc/security/limits.conf** 檔案

```
*         hard    nofile      1048576
*         soft    nofile      1048576
root      hard    nofile      1048576
root      soft    nofile      1048576
```

> systemd 打開的 服務 不受 limits.conf 影響 需要使用 LimitNOFILE 參數指定
>
> LimitNOFILE=1048576

hard 的最大值 不能超過 **/proc/sys/fs/nr_open** 中的 定義 故可能海需要修改 **/proc/sys/fs/nr\_open**
```
cat /proc/sys/fs/nr_open
1048576
```
**/proc/sys/fs/file-nr** 檔案則記錄了 當前 系統打開的 檔案數
```
cat /proc/sys/fs/file-nr 
10816	0	1613518
```
* 第一列 系統已經打開的檔案數
* 第二列 分配後已釋放的(目前已不再使用)
* 第三列 file-max

# 網路試優

編輯 **/etc/sysctl.conf** 添加設定 後執行 `sysctl -p`

```
net.ipv4.tcp_syn_retries = 1
net.ipv4.tcp_synack_retries = 1
net.ipv4.tcp_keepalive_time = 600
net.ipv4.tcp_keepalive_probes = 3
net.ipv4.tcp_keepalive_intvl =15
net.ipv4.tcp_retries2 = 5
net.ipv4.tcp_fin_timeout = 2
net.ipv4.tcp_max_tw_buckets = 36000
net.ipv4.tcp_tw_recycle = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_max_orphans = 32768
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_syn_backlog = 16384
net.ipv4.tcp_wmem = 8192 131072 16777216
net.ipv4.tcp_rmem = 32768 131072 16777216
net.ipv4.tcp_mem = 786432 1048576 1572864
net.ipv4.ip_local_port_range = 1024 65000
net.core.netdev_max_backlog = 16384
```

## /proc/sys/net/ipv4/

**/proc/sys/net/ipv4/** 下的檔案記錄了 部分設置的臨時值

| 名稱 | 默認值 | 建議值 | 描述 |
| -------- | -------- | -------- | -------- |
| tcp\_syn\_retries     | 5     | 1     | 對於一個新連接，內核要發送多少個SYN連接請求才決定放棄。不應該大於255，默認值爲5對應180秒左右。（對於大負載而物理通信良好的網路而言 可以修改爲2，此值只對對外連接，對進來的連接由tcp_retries1控制）     |
| tcp\_synack\_retries     | 5     | 1     | 對於遠端連接請求SYN，內核會發送SYN+ACK以確認收到上個SYN。這是三次握手的第二個步驟。此值決定內核在放棄連接之前所發出的SYN+ACK數目。不應該大於255，默認5對應180秒左右     |
| tcp\_keepalive\_time     | 7200     | 600     | tcp發送keepalive探測消息的時間間隔（秒）     |
| tcp\_keepalive\_probes     | 9     | 3     | 在認定tcp失效前 最多發送多少個keepalive探測消息     |
| tcp\_keepalive\_intvl     | 75     | 15     | 探測消息未獲得響應時，重發該消息的時間間隔（秒）     |
| tcp\_retries1     | 3     | 3     | 放棄回應一個tcp連接請求前，需要進行多少次重試，RFC規定最小值爲3     |
| tcp\_retries2     | 15     | 5     | 在丟棄激活（已建立通信狀況）的tcp連接前，需要進行多少次重試。     |
| tcp\_orphan\_retries     | 7     | 3     | 在近端丟棄tcp連接前，要進行多少次重試     |
| tcp\_fin\_timeout     | 60     | 2     | 對於本端斷開的socket，tcp保持在 FIN\_WAIT\_2 狀態時間（秒）     |
| tcp\_max\_tw\_buckets     | 180000     | 36000     | 系統同時處理的最大 TIME\_WAIT socket 數目，如果超過此值， TIME\_WAIT socekt 會被立刻砍出，並顯示警告信息。     |
| tcp\_tw\_recycle     | 0     | 1     | 打開 快速 TIME\_WAIT socket 回收    |
| tcp\_tw\_reuse     | 0     | 1     | 是否允許將 TIME\_WAIT socket用於新的tcp連接      |
| tcp\_max\_orphans     | 8192     | 32768     | 系統能處理不屬於任何進程的tcp socket最大數量。如果超過此值，不屬於任何進程的連接會立刻reset，並同時顯示警告信息     |
| tcp\_abort\_on\_overflow     | 0     | 0     | 當守衛進程太忙而不能接受新的連接，就向對方發送reset消息     |
| tcp\_syncookies     | 0     | 1     | 只有內核編譯時選擇了CONFIG_SYNCOOKIES才有用。當出現syn等候隊列溢出時向對方發送 syn cookies     |
| tcp\_stdurg     | 0     | 0     | 使用tcp urg pointer字段的主機請求解釋功能。大部分主機都使用老舊的BSD解釋，如果打開可能無法正常與之通信     |
| tcp\_max\_syn\_backlog     | 1024     | 16384     | 對於未獲得客戶端確認的連接請求，需要保存在隊列中最大數目。超過128m內存默認爲1024，低於128m內存則爲128     |
| tcp\_window\_scaling     | 1     | 1     | 滑動窗口是否可變     |
| tcp\_timestamps     | 1     | 1     | 時間戳用於防止某些僞造的 sequence 號碼     |
| tcp\_sack     | 1     | 1     | 使用 Selective ACK，可以用來檢查特定的遺失數據     |
| tcp\_fack     | 1     | 1     | 打開FACK擁塞避免和快速重傳功能     |
| tcp\_dsack     | 1     | 1     | 允許TCP發送兩個完全相同的 SACK     |
| tcp\_ecn     | 0     | 0     | tcp 擁塞警告     |
| tcp\_reordering     | 3     | 6     | tcp流中重排序的數據報最大數量     |
| tcp\_retrans\_collapse     | 1     | 0     | 對於某些有bug的打印機提供針對其bug的兼容性     |
| tcp\_wmem : min default max     | 4096<br>16384<br>131072     | 8192<br>131072<br>16777216     | min:爲tcp預留的用於發送緩衝區內存最小值。<br>default: 爲tcp預留用於發送的緩衝區內存大小，默認情況下會影響其它協議使用 net.core.wmen\_default，一般要低於 net.core.wmen\_default<br>max:用於tcp發送緩衝區最大值。此值不會影響 net.core.wmem\_max      |
| tcp\_rmem：min default max     | 4096<br>87380<br>174760     | 32768<br>131072<br>16777216     | recv 緩衝區設置     |
| tcp\_mem:low pressure high     | 根據內存計算     | 786432<br>1048576<br>1572864     | low:當tcp使用了低於此值的內存頁面數時，tcp不會考慮釋放內存。既低於此值沒有內存壓力。（理想情況下，此值應該與 tcp\_wmem 第二個值匹配，表明最大頁面大小乘以最大併發數除以頁面大小 131072\*300/4096）<br>pressure:當tcp使用了超過此值的內存頁面數量時，tcp試圖穩定其內存使用，進入pressure模式，當內存低於low值是退出pressure狀態。（理想情況下此值應該是tcp可以使用的總緩衝區大小的最大值 204800\*300/4096）<br>high:允許tcp用於排隊緩衝區數據報頁面量（如果超過此值 tcp連接將被拒絕）     |
| tcp\_app\_win     | 31     | 31     | 保留max(window/2^tcp\_app\_win)數量的窗口用於應用緩衝     |
| tcp\_adv\_win\_scale     | 2     | 2     | 計算緩存開銷     |
| tcp\_low\_latency     | 0     | 0     | 允許 tcp/ip棧適應在高吞吐情況下低延時的情況     |
| tcp\_westwood     | 0     | 0     | 啓用發送者端的擁塞控制算法，它可以維護對吞吐量的評估，並試圖對帶寬的整體利用情況進行優化，對於WAN通信應該啓用此值     |
| tcp\_bic     | 0     | 0     | 爲快速長距離網路啓用 Binary Increase Congestion,WAN通信應該啓用此值     |
| ip\_forward     | 0     | 0     | NAT是否允許ip轉發     |
| ip\_local\_port\_range:min max     | 32768<br>61000     | 1024<br>65000     | 向外連接使用的端口範圍     |
| ip\_conntrack\_max     | 65535     | 65535     | 系統跟蹤的最大ipv4連接數量（如果內存128m最大值爲8192，如果1g以上內存默認65536）     |

## /proc/sys/net/ipv4/netfilter/

當啓用了 防火牆 **/proc/sys/net/ipv4/netfilter/** 下的檔案記錄了 部分設置的臨時值
| 名稱 | 默認值 | 建議值 | 描述 |
| -------- | -------- | -------- | -------- |
| ip\_conntrack\_max     | 65536     | 65536     | 系統跟蹤的最大ipv4連接數量（如果內存128m最大值爲8192，如果1g以上內存默認65536同時受到 /proc/sys/net/ipv4/ip\_conntrack\_max影響）     |
| ip\_conntrack\_tcp\_timeout\_established     | 432000     | 180     | 已建立的tcp連接超時時間默認 432000爲五天。    |
| ip\_conntrack\_tcp\_timeout\_time\_wait     | 120     | 120     | TIME_WAIT狀態超時時間 超過就清理連接     |
| ip\_conntrack\_tcp\_timeout\_close\_wait     | 60     | 60     | CLOSE_WAIT 狀態超時時間 超過就清理連接     |
| ip\_conntrack\_tcp\_timeout\_fin\_wait     | 120     | 120     | FIN_WAIT狀態超時時間 超過就清理連接     |
## /proc/sys/net/core/
 **/proc/sys/net/core/** 下的檔案記錄了 部分設置的臨時值
 
| 名稱 | 默認值 | 建議值 | 描述 |
| -------- | -------- | -------- | -------- |
| netdev\_max\_backlog     | 1024     | 16384     | 每個網路接口接收數據包的速率比內核處理快時，允許送到隊列的數據包最大數量。     |
| somaxconn     | 128     | 16384     | 用來限制監聽（LISTEN）隊列最大數據包數量，超過會導致連接超時或者觸發重傳機制     |
| wmem\_default     | 129024     | 129024     | 默認發送窗口大小     |
| rmem\_default     | 129024     | 129024     | 默認接收窗口大小     |
| rmem\_max     | 129024     | 873200     | 最大的tcp數據接收緩衝     |
| wmem\_max     | 129024     | 873200     | 最大的tcp數據發送緩衝     |
# Example

```
# 網絡設備接收數據包的速率比內核處理這些包快時，允許送到隊列的數據包最大數量。
net.core.netdev_max_backlog = 400000

# 每個socket允許的最大緩衝區大小
net.core.optmem_max = 10000000

# socket recv window 默認值
net.core.rmem_default = 10000000

# socket recv window 最大值
net.core.rmem_max = 10000000
#指定了接收套接字缓冲区大小的最大值（以字节为单位）。
 
 # socket 監聽 backlog 上限
net.core.somaxconn = 100000

# socket send window 默認大小
net.core.wmem_default = 11059200

# socket send window 最大值
net.core.wmem_max = 11059200

# 嚴謹模式 1 （推薦）
# 鬆散模式 0
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1
 
net.ipv4.tcp_congestion_control = bic
 
# 關閉 tcp_window_scaling
# 根據 RFC 1323 定義 要支持超過 64kb窗口，必須啓用此值。
net.ipv4.tcp_window_scaling = 0

 # 關閉 tcp_ecn
net.ipv4.tcp_ecn = 0
 
# 啓動有選擇應答（Selective Acknowledgment）
# 通過有選擇的應答亂序接收到報文來提高性能（讓發送者只發送丟失的報文段）
#（對於廣域網通信）應該啓用此值，但會增加cpu佔用
net.ipv4.tcp_sack = 1

# 系統同時保存 TIME_WAIT socket的最大數量
net.ipv4.tcp_max_tw_buckets = 10000

# SYN隊列長度 默認1024，8192可以容納更多等待連接的網路連接數
net.ipv4.tcp_max_syn_backlog = 8192

# 開啓 SYN Cookies，當出現SYN等待隊列溢出時，啓用cookies處理，可防範少量SYN攻擊，默認爲0表示關閉
net.ipv4.tcp_syncookies = 1

# 開啓 TCP 時間戳
# 以一種比重發超時更精確的防範(RFC 1323)來啓用對RTT的計算，爲了實現更好的性能應該啓用這個選項
net.ipv4.tcp_timestamps = 1
 
 # 開啓重用。允許將 TIME_WAIT socket 重新用於新的 TCP連接
net.ipv4.tcp_tw_reuse = 1

# 開啓 TCP連接 TIME_WAIT cosket 快速回收
net.ipv4.tcp_tw_recycle = 1

# 如果socket由本地邀請關閉，此值指示了保持 FIN_WAIT_2 狀態時間
net.ipv4.tcp_fin_timeout = 10

# 啓用 keepalive 時，tcp 發送 keepalive 消息的 頻度。默認 2小時 此處爲 1800s 既30分鐘
net.ipv4.tcp_keepalive_time = 1800

# 如果對方不予應答，探測包的發送次數
net.ipv4.tcp_keepalive_probes = 3

# keepalive 探測包發送間隔 
net.ipv4.tcp_keepalive_intvl = 15


# 確定 tcp 棧 應該如何反應內存使用 ；每個值的單位都是內存頁（通常是 4KB）。
# 第一個值是內存使用下限
# 第二個值是內存壓力模式開始對緩衝區使用應用壓力的上限
# 第三個值是內存上限。在這個層次上將報文丟棄,從而減少對內存的使用。
net.ipv4.tcp_mem


# 與 tcp_wmem 類似，不過表示的是爲自動調優使用的接收緩衝區值
net.ipv4.tcp_rmem

# 與 tcp_wmem 類似，不過表示的是爲自動調優使用的發送緩衝區值
net.ipv4.tcp_wmem = 30000000 30000000 30000000
 
 # 向外連接使用的 端口範圍 默認 32768 61000
net.ipv4.ip_local_port_range = 1024 65000
 
 # 系統對最大跟蹤的 tcp 連接數 限制
net.ipv4.netfilter.ip_conntrack_max=204800

# 關閉 tcp 的連接 傳輸的 慢啓動，既先休止一段時間，再初始化擁塞窗口
net.ipv4.tcp_slow_start_after_idle = 0

# 路由緩存刷新頻率，當一個路由失敗後多長時間跳到另一個路由 默認 300
net.ipv4.route.gc_timeout = 100

# 在內核放棄建立連接之前發送 SYN包 數量
net.ipv4.tcp_syn_retries = 1

# 避免放大攻擊
net.ipv4.icmp_echo_ignore_broadcasts = 1

# 開啓 惡意 icmp 錯誤消息保護
net.ipv4.icmp_ignore_bogus_error_responses = 1

# 防止不正確的 udp 攻擊
net.inet.udp.checksum=1

# 是否接受含有源路由信息的ip包
# 從安全角度 建議關閉此功能
net.ipv4.conf.default.accept_source_route = 0
```
