# [獲取和加載資源](http://docs.cocos.com/creator/manual/zh/scripting/load-assets.html)

所有 [cc.Asset](https://docs.cocos.com/creator/api/zh/classes/Asset.html) 的子孫類都是 資源 比如

* [cc.Texture2D](https://docs.cocos.com/creator/api/zh/classes/Texture2D.html)
* [cc.SpriteFrame](https://docs.cocos.com/creator/api/zh/classes/SpriteFrame.html)
* [cc.AnimationClip](https://docs.cocos.com/creator/api/zh/classes/AnimationClip.html)
* [cc.Prefab](https://docs.cocos.com/creator/api/zh/classes/Prefab.html)
* ...

# 靜態加載

在腳本中 使用 property 定義 資源屬性

之後 在 creator 中 將資源 拖動到 對應 屬性 即可

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.SpriteFrame)
    readonly target: cc.SpriteFrame = null
}
```

# 動態加載

* 所有 動態加載的 資源 必須存放到 **assets/resources** 檔案夾下
* assets/resources 下的資源 可 assets/resources 外的資源 可以相互 引用
* 不要將 非動態資源放到 assets/resources 下面 creator 在打包時 會只打包 使用到的資源 而 對應 動態資源 creator 無法識別到哪些是必要的 只能全部打包

```ts
// 加載預製資源
cc.loader.loadRes("tux", (e: Error, prefab: cc.Prefab) => {
		// 加載 失敗
		if (e) {
				console.log(e)
				return
		}

		// 加載 成功
		const node = cc.instantiate(prefab)
		cc.director.getScene().addChild(node)
})
// 加載動畫
cc.loader.loadRes("anim", (e: Error, clip: cc.AnimationClip) => {
		// 加載 失敗
		if (e) {
				console.log(e)
				return
		}

		// 加載 成功
		this.node.getComponent(cc.Animation).addClip(clip, "anim");
})
```
> 傳入 loadRes 的是 相對 **assets/resources** 的資源路徑(不含擴展名)

# 加載 SpriteFrame 

對於 圖片 加載後 會返回 [cc.Texture2D](https://docs.cocos.com/creator/api/zh/classes/Texture2D.html) 此時如果 需要 [cc.SpriteFrame](https://docs.cocos.com/creator/api/zh/classes/SpriteFrame.html) 可以明確的將 SpriteFrame 傳遞給 loadRes 函數

```
// 加載預製資源
cc.loader.loadRes("tux", cc.SpriteFrame, (e: Error, spriteFrame: cc.SpriteFrame) => {
		// 加載 失敗
		if (e) {
				console.log(e)
				return
		}
		// 加載 成功
		const node = new cc.Node('tux')
		const sprite = node.addComponent(cc.Sprite)
		sprite.spriteFrame = spriteFrame

		cc.director.getScene().addChild(node)
})
```

# 加載圖集中的 SpriteFrame

```ts
// 加載預製資源
cc.loader.loadRes("tuxs", cc.SpriteAtlas, (e: Error, atlas: cc.SpriteAtlas) => {
		// 加載 失敗
		if (e) {
				console.log(e)
				return
		}
		// 加載 成功
		const node = new cc.Node('tux')
		const sprite = node.addComponent(cc.Sprite)
		sprite.spriteFrame = atlas.getSpriteFrame('tuxs_down_0');

		cc.director.getScene().addChild(node)
})
```

# 資源 批量 加載

cc.loader.loadResDir 可以加載相同路徑下的 多個資源

```ts
cc.loader.loadResDir("", function (e: Error, assets: Array<any>, urls: Array<string>) {
		console.log(e)
		console.log(assets)
		console.log(urls)
})
cc.loader.loadResDir("", cc.SpriteFrame, function (e: Error, assets: Array<cc.SpriteFrame>, urls: Array<string>) {
		console.log(e)
		console.log(assets)
		console.log(urls)
})
```

# 加載遠程資源

creator 運行 加載遠程 貼圖 這對於 加載用戶 頭像等 很有幫助

要使用 這個 必須 使用 cc.loader.load 來 完成

```
// 远程 url 带图片后缀名
var remoteUrl = "http://unknown.org/someres.png";
cc.loader.load(remoteUrl, function (err, texture) {
    // Use texture to create sprite frame
});

// 远程 url 不带图片后缀名，此时必须指定远程图片文件的类型
remoteUrl = "http://unknown.org/emoji?id=124982374";
cc.loader.load({url: remoteUrl, type: 'png'}, function () {
    // Use texture to create sprite frame
});

// 用绝对路径加载设备存储内的资源，比如相册
var absolutePath = "/dara/data/some/path/to/image.png"
cc.loader.load(absolutePath, function () {
    // Use texture to create sprite frame
});
```

# 釋放資源

調用 cc.loader.releaseRes 傳入 和 loadRes 相同的 資源 參數 即可釋放資源

```
cc.loader.releaseRes("test assets/image", cc.SpriteFrame)
cc.loader.releaseRes("test assets/anim")
```

也可以調用 cc.loader.releaseAsset 來釋放 指定的 資源實例

```
cc.loader.releaseAsset(spriteFrame)
```

# 資源的依賴釋放

```
// 直接释放某个贴图
cc.loader.release(texture);
// 释放一个 prefab 以及所有它依赖的资源
var deps = cc.loader.getDependsRecursively('prefabs/sample');
cc.loader.release(deps);
// 如果在这个 prefab 中有一些和场景其他部分共享的资源，你不希望它们被释放，可以将这个资源从依赖列表中删除
var deps = cc.loader.getDependsRecursively('prefabs/sample');
var index = deps.indexOf(texture2d._uuid);
if (index !== -1)
    deps.splice(index, 1);
cc.loader.release(deps);
```
