# angularjs

1. AngularJS 是 google 2009年開始 開發的 一個 開源(MIT) js 庫 
1. 主要 用於 協助構建 單一頁面 程式
1. 其使用 ng-XXX 在html中定義 元素 所以 又常被簡稱為 ng

* 官網 [https://angularjs.org/](https://angularjs.org/)
* api [https://docs.angularjs.org/api](https://docs.angularjs.org/api)
* 源碼 [https://github.com/angular/angular.js](https://github.com/angular/angular.js)