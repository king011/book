# 服務器

安裝 WireGuard 最簡單方式是使用 linuxserver 提供的 [wireguard image](https://docs.linuxserver.io/images/docker-wireguard/)


```
services:
  wireguard:
    image: lscr.io/linuxserver/wireguard:latest
    container_name: wireguard
    cap_add:
      - NET_ADMIN
      - SYS_MODULE #optional
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
      - SERVERURL=wireguard.domain.com #optional
      - SERVERPORT=51820 #optional
      - PEERS=1 #optional
      - PEERDNS=auto #optional
      - INTERNAL_SUBNET=10.13.13.0 #optional
      - ALLOWEDIPS=0.0.0.0/0 #optional
      - PERSISTENTKEEPALIVE_PEERS= #optional
      - LOG_CONFS=true #optional
    volumes:
      - /path/to/wireguard/config:/config
      - /lib/modules:/lib/modules #optional
    ports:
      - 51820:51820/udp
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    restart: unless-stopped
```



| 參數 | 含義 |
| -------- | -------- |
| SERVERURL     | 外網ip或域名     |
| SERVERPORT     | 監聽端口     |
| PEERS     | 要生成的端點數量，或者僅包含字母的名稱數組(以 , 分隔)     |
| PEERDNS     | 對等客戶端的 dns，auto 表示默認將使用 容器中的 CoreDNS 作爲dns服務器     |
| INTERNAL_SUBNET     | 虛擬網路的子網     |
| ALLOWEDIPS     | 對等方能夠使用 VPN 連線到達的 IP/範圍     |

# 客戶端
使用 docker 可以方便的連接到 wireguard
```
services:
  wireguard:
    image: lscr.io/linuxserver/wireguard:latest
    cap_add:
      - NET_ADMIN
      - SYS_MODULE #optional
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
    volumes:
      - ./conf/wg0.conf:/config/wg_confs/wg0.conf:ro
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
```

將連接設置檔案映射到 **/config/wg\_confs/** 檔案夾喜愛

## linux

1. 安裝好必備工具

	```
	sudo apt install wireguard resolvconf
	```

2. 創建設定檔案 /etc/wireguard/**wg0**.conf

	如果使用上述 docker 創建服務器可以直接將 /config/wireguard/peer{N}/peer{N}.conf 複製過來即可
	
3. 啓動服務

	```
	sudo systemctl start wg-quick@wg0
	```
	
	它會連接 WireGuard 並設置好 iptables
	
4. 關閉服務

	```
	sudo systemctl stop wg-quick@wg0
	```
	
	它會關閉 WireGuard 並清理設置的 iptables