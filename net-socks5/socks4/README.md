# socks4 socks4a

1. 客戶端 發送 socks4 請求
1. 轉發數據

# socks4 請求

1. 客戶端 向服務器 發送 一個 socks4 請求

	| VER | CMD | DST.PORT |DST.IPv4 |USERID |
	| -------- | -------- | -------- | -------- | -------- |
	| 1     | 1     | 2     | 4     | 動態長度     |
	
	* VER 是socks版本 對於 socks4來說 必須是 0x04
	* CMD 請求指令
		* 0x01 Connect
		* 0x02 BIND
	* DST.PORT 網路位元組序標識的 目的端口
	* DST.IPv4 目的地址 ipv4
	* USERID 用戶id 以 0 結尾的字符串
	
1. 服務器 回應 請求

	| VER | REP | DST.PORT |DST.IPv4 |
	| -------- | -------- | -------- | -------- |
	| 1     | 1     | 2     | 4     |

	* VER 是socks版本 對於 socks4來說 必須是 0x00
	* REP 應答
		* 0x5A 表示成功
		* 0x5B 請求被拒絕或失敗
		* 0x5C 由於 socks服務器 無法連接 USERID 驗證服務器 所以 拒絕請求
		* 0x5D USERID 身份沒通過驗證 所以拒絕請求
	* DST.PORT 和請求包中內容相同 但被忽略 
	* DST.IPv4 和請求包中內容相同 但被忽略
	
# socks4a
	
socks4a 兼容 socks4 只是在請求時 DST.IPv4 爲 0.0.0.x 最後一個 不爲 0 此時 服務器 需要 在再接收一個 HOSTNAME 的域名 作爲目標地址
	
| VER | CMD | DST.PORT | DST.IPv4 | USERID | HOSTNAME |
| -------- | -------- | -------- | -------- | -------- | -------- |
| 1     | 1     | 2     | 4     | 動態長度     | 動態長度     |

* VER 是socks版本 對於 socks4來說 必須是 0x04
* CMD 請求指令
	* 0x01 Connect
	* 0x02 BIND
* DST.PORT 網路位元組序標識的 目的端口
* DST.IPv4 0.0.0.x 的特殊ipv4地址 讓服務器 能夠區分 socks4 和 socks4a
* USERID 用戶id 以 0 結尾的字符串
* HOSTNAME 目標域名 以 0 結尾的字符串

