# 視差滾動

我們已經在靜態場景裏創建了主角和敵人，是時候加強一下背景和場景了。在過去的15年裏，你會在每個2d單機遊戲中發現視差滾動(parallax scrolling)效果。

簡單來說，就是以不同的速度移動背景層（較遠的層移動得慢）。如果工作正常，它會有一個深度錯覺，這是一個很酷且易於操作的效果

此外很多橫版卷軸遊戲使用一個或多個軸的捲動

# 理論：定義我們遊戲裏的捲動

關於如何將捲動軸添加到遊戲中，我們需要花一點時間思考。這是一件在編碼前非常值得的事情

## 如何移動？

我們有下面的想法：
1.第一個選擇：主角和攝像機移動，剩下對象固定不動
2.第二個選擇：主角和攝像機移動，level層做跑步機式運動

如果有 **透視攝像機**，第一個選擇非常容易，視差很明顯，背景元素有更高的景深，因此，它們在後面看去上移動得更慢。

但是在 unity 的標準2d遊戲裏，我們使用**正交攝像機**，並沒有景深補償

> 在2d遊戲中，記得將攝像機的 **Projection** 屬性設置爲 **Orthographic**
> * **Perspective** 透視 是擁有景深的經典 3D 攝像機
> * **Orthographic** 正交 表示所有東西都在同一面，它在 GUI 和 2D 遊戲裏特別有用

在我們的遊戲裏添加視差捲動效果，最好的方法是混合使用上述兩種方法，我們有兩種捲動方式：
* 主角和攝像機向前移動
* 除開攝像機的移動 背景元素還要以不同的速度移動

**注意** 你可能會問：“爲什麼不把攝像機設置爲主角的子對象？”。確實，在 unity 裏，如果將一個對象設置爲另外對象的子對象，那麼子對象將處於相對與父對象的位置。所以，如果攝像機是主角的子對象且在它的中心，它將會跟隨主角移動。這樣也許是可行的，但它並不是本次遊戲的玩法

在橫版卷軸遊戲中，攝像機會限制主角的移動，如果攝像機跟隨主角在水平和垂直方向移動，那麼主角可以移動到任何地方，而我們想讓主角呆在一個限制區域裏。

在 2D 遊戲裏，我們建議儘量保持攝像機的獨立性，甚至在平臺類遊戲裏，攝像機同樣不是始終跟隨着主角：它在一定限制裏跟隨，超級瑪俐歐大概是攝像機運用的最好的平臺類遊戲，[你可以看看它是怎樣的](https://www.youtube.com/watch?v=TCIMPYM0AQg)

## 產出敵人

添加捲動會對我們的遊戲產生影響，尤其是敵人們。現在，遊戲以開始，它們會移動，射擊。然而我們希望它們乖乖等着，直到該它們出場時。

如何生成敵人？着肯定依賴遊戲，你可以在觸發事件時產出敵人，產生數量，預定義地點等。

看看我們將怎麼做：我們直接將敵人放置到場景裏（通過拖拽 Prefab 到場景）。默認情況下，它們是靜止且無敵的，直到攝像到達並激活它們

![](assets/camera_use.png)

好消息是你可以使用 unity 編輯器設置敵人們，不用做任何事情，你已經擁有了一個關卡編輯器。

它是一個選擇但並不科學，對於一個大的項目，你最好有一個專用的關卡編輯器，向 **Tiled** 或者自己做一個。

## 平面

首先，我們必須定義我們的平面是什麼，以及每個平面是否循環。在關卡執行期間，循環背景將一遍又一遍的重複。例如，着對天空之類的東西特別有用

爲背景元素添加一個新的平面到場景中，我們將有如下的平面

| 平面 | 循環 |
| -------- | -------- |
| Background with the sky(天空背景)     | Yes     |
| Background(第一組場景懸浮道具)     | No     |
| Middleground(第二組場景懸浮道具)     | No     |
| Foreground(主角和敵人的前景)     | No     |

![](assets/planes.png)

# 練習：深入代碼

unity 的標準包裏面有些 **parallax scrolling** 腳本(可以在 Asset Store 看看 2D platformer demo)。你當然可以使用它們，但我們第一次還是從頭開始構建會比較有趣。

**標準包** 非常實用，但要小心被濫用它們。使用標準包會禁錮你的思想，還會使你的遊戲沒有個性。它會讓你的遊戲有 unity式的感覺。你可能還記得所有 flash 遊戲都是一個模子

## 簡單卷軸

我們將從容易的部分開始：不循環的滾動背景。

還記得之前的 **MoveScript** 嗎？基本是相同的思路，創建一個 **ScrollingScript** 腳本

```
#info="ScrollingScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingScript : MonoBehaviour
{
    // 滾動速度
    public Vector2 speed = new Vector2(2, 2);
    //  滾動方向
    public Vector2 direction = new Vector2(-1, 0);
    // 是否連接到攝像機
    public bool isLinkedToCamera = false;

    void Update()
    {
        // 移動
        Vector3 movement = new Vector3(
          speed.x * direction.x,
          speed.y * direction.y,
          0);
        movement *= Time.deltaTime;
        transform.Translate(movement);

        // 對於連接了攝像機的節點 同時移動攝像機
        if (isLinkedToCamera)
        {
            Camera.main.transform.Translate(movement);
        }
    }
}
```

將腳本添加到下面這些遊戲對象上，並設置對應的參數

| 平面對象 | speed | direction | isLinkedToCamera |
| -------- | -------- | -------- | -------- |
| Background     | (1, 1)     | (-1, 0)     | false     |
| Background elements     | (1.5, 1.5)     | (-1, 0)     | false     |
| Middleground     | (2.5, 2,5)     | (-1, 0)     | false     |
| Foreground     | (1, 1)     | (1, 0)     | true     |

![](assets/scrolling1.gif)

還不錯！但是敵人在攝像機鏡頭之外時它們已經在移動和射擊了，甚至在它們產生前。而且當它們穿過主角後也並沒有消失，我們將在後面修正這些問題，首先，我們需要去管理這無窮盡的背景(天空)

## 無限滾動的背景

爲了獲取無限背景，我們僅僅只需要盯着 **Background** 平面最左邊的子節點，當這個對象超出了攝像機鏡頭的左邊緣，我們就將它移動到所在平面的最右邊，無限循環即可

![](assets/infinite_scrolling_definition.png)

### RendererExtensions

我們創建一個 **RendererExtensions.cs**腳本，來爲 Renderer 添加一個 IsVisibleFrom 的擴展函數，用於返回 Renderer 是否在攝像機鏡頭內

```
#info="RendererExtensions.cs"
using UnityEngine;

namespace Example
{
    public static class RendererExtensions
    {
        public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }
    }
}
```
### ScrollingScript

現在來完善 **ScrollingScript** 腳本

```
#info="ScrollingScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; // 爲 List 提供了 OrderBy 函數
using Example; // 引入 IsVisibleFrom 擴展
public class ScrollingScript : MonoBehaviour
{
    // 滾動速度
    public Vector2 speed = new Vector2(2, 2);
    //  滾動方向
    public Vector2 direction = new Vector2(-1, 0);
    // 是否連接到攝像機
    public bool isLinkedToCamera = false;
    // 是否是無限滾動的
    public bool isLooping = false;
    // 對於無限滾動背景 存儲其渲染子節點
    private List<SpriteRenderer> backgroundPart;

    void Start()
    {
        // 對於無限滾動的執行一些初始化
        if (isLooping)
        {
            // 獲取所有存在渲染器的子節點
            backgroundPart = new List<SpriteRenderer>();
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                SpriteRenderer r = child.GetComponent<SpriteRenderer>();
                // 值添加可見節點
                if (r != null)
                {
                    backgroundPart.Add(r);
                }
            }

            // 按照子節點位置 從左到右排序
            backgroundPart = backgroundPart.OrderBy(
              t => t.transform.position.x
            ).ToList();
        }
    }

    void Update()
    {
        // 移動
        Vector3 movement = new Vector3(
          speed.x * direction.x,
          speed.y * direction.y,
          0);
        movement *= Time.deltaTime;
        transform.Translate(movement);

        // 對於連接了攝像機的節點 同時移動攝像機
        if (isLinkedToCamera)
        {
            Camera.main.transform.Translate(movement);
        }
        if (isLooping)
        {
            loopUpdate();
        }
    }
    private void loopUpdate()
    {
        // 獲取列表最左邊的元素
        SpriteRenderer firstChild = backgroundPart.FirstOrDefault();
        if (firstChild == null)
        {
            return;
        }
        // 檢查節點是否已經在攝像機之前 因爲 IsVisibleFrom 函數比較耗資源
        if (firstChild.transform.position.x < Camera.main.transform.position.x)
        {
            if (!firstChild.IsVisibleFrom(Camera.main))// 沒在鏡頭內
            {
                // 獲取最後一個節點的位置
                SpriteRenderer lastChild = backgroundPart.LastOrDefault();

                Vector3 lastPosition = lastChild.transform.position;
                Vector3 lastSize = (lastChild.bounds.max - lastChild.bounds.min);

                // 將第一個節點移到到最後一個節點之後，目前只處理了水平滾動
                firstChild.transform.position = new Vector3(lastPosition.x + lastSize.x,
                    firstChild.transform.position.y, firstChild.transform.position.z);

                // 移動子節點儀表
                backgroundPart.Remove(firstChild);
                backgroundPart.Add(firstChild);
            }
        }
    }
}
```

## ENEMY V2 WITH SPAWN

敵人應該是不可用的，直到它們出現在攝像機鏡頭中，一旦它們完全離開屏幕，就應該被消除。

我們需要更新 **EnemyScript**：
1. 初始化時 禁止 移動 碰撞 自動開火
2. 確認渲染時對象已經在攝像機鏡頭中
3. 激活它
4. 當它離開鏡頭時銷毀它

```
#info="EnemyScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Example;
public class EnemyScript : MonoBehaviour
{
    private bool hasSpawn;
    private MoveScript moveScript;
    private WeaponScript[] weapons;
    private Collider2D coliderComponent;
    private SpriteRenderer rendererComponent;
    void Awake()
    {
        // 初始化時 獲取組件應用
        weapons = GetComponentsInChildren<WeaponScript>();
        moveScript = GetComponent<MoveScript>();
        coliderComponent = GetComponent<Collider2D>();
        rendererComponent = GetComponent<SpriteRenderer>();
    }
    void Start()
    {
        hasSpawn = false;

        // 禁用所有功能組件
        coliderComponent.enabled = false;
        moveScript.enabled = false;
        foreach (WeaponScript weapon in weapons)
        {
            weapon.enabled = false;
        }
    }


    void Update()
    {
        if (hasSpawn == false)
        {
            if (rendererComponent.IsVisibleFrom(Camera.main))//進入到屏幕
            {
                Spawn();
            }
        }
        else
        {
            foreach (var weapon in weapons)
            {
                // 自動開火
                if (weapon != null && weapon.CanAttack)
                {
                    weapon.Attack(true);
                }
            }
            // 離開屏幕 銷毀對象
            if (rendererComponent.IsVisibleFrom(Camera.main) == false)
            {
                Destroy(gameObject);
            }
        }
    }
    private void Spawn()
    {
        hasSpawn = true;

        // 啓用各種組件
        coliderComponent.enabled = true;
        moveScript.enabled = true;
        foreach (WeaponScript weapon in weapons)
        {
            weapon.enabled = true;
        }
    }
}
```

運行遊戲，你會發現有 bug。

**MoveScript** 並沒有產生正常的效果：Player永遠無法到達 Enemies，因爲它們都跟着 Foreground 面在滾動

![](assets/camera_moving_along.gif)

記得我們往 Foreground 面添加過 **ScrollingScript** 嗎？並且將攝像機跟隨着一起移動！這裏有個簡單的解決方法：把 **ScrollingScript** 從 Foreground 移動到 Player 身上！

按下 **Play** 按鈕，觀察：
1. Enemies 在它們產生前都是禁止的(直到攝像機到達它們的位置)
2. 當它們離開攝像機時，它們消失了

![](assets/enemy_spawn.png)

## 固定 Player 在攝像機範圍內

你可能已經注意到了 Player 沒有被限制在攝像機的區域內。我們需要修改 **PlayScript** 在 Update 方法的最後添加下面的代碼來修復

```
#info="PlayScript.cs"
void Update()
{
	// ...
	
	// 確保 Player 不在攝像機之外
	var dist = (transform.position - Camera.main.transform.position).z;
	var leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
	var rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
	var topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
	var bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;

	transform.position = new Vector3(
		Mathf.Clamp(transform.position.x, leftBorder, rightBorder),
		Mathf.Clamp(transform.position.y, topBorder, bottomBorder),
		transform.position.z
	);
}
```