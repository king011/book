# dart:ffi

目前 flutter 已經支持使用 ffi 直接調用 c api，下面將暫時如何實現一個 native_add 函數供 flutter 調用

## 步驟 1 創建插件

創建一個插件項目，比如 native_add

```
flutter create --platforms android,ios --template=plugin native_add && cd native_add
```

## 步驟 2 創建 c/c++ 代碼

使用 c/c++ 實現代碼，並導出 c 符號

```
#info="native/native_add.cpp"
#include <stdint.h>

extern "C" __attribute__((visibility("default"))) __attribute__((used))
int32_t
native_add(int32_t x, int32_t y)
{
    return x + y;
}
```

### android

對於 android 你還需要創建一個 CMakeLists.txt 檔案指定如何編譯 c/c++ 代碼

```
#info="android/CMakeLists.txt"
cmake_minimum_required(VERSION 3.4.1)

add_library(native_add
    # 編譯動態庫
    SHARED
    # 要編譯的源碼列表
    ../native/native_add.cpp
)
```

最後 添加一個 externalNativeBuild 到 android/build.gradle 中

```
#info="android/build.gradle"
android {
    // ...
    externalNativeBuild {
        // 設置 cmake 配置
        cmake {
            // cmake 編譯腳本路徑
            path "CMakeLists.txt"
        }
    }
    // ...
}
```

## 步驟 3 使用 dart:ffi 調用 c 代碼

首先需要創建一個 DynamicLibrary 來處理本地代碼

```
import 'dart:ffi'; // For FFI
import 'dart:io'; // For Platform.isX

final DynamicLibrary nativeAddLib = Platform.isAndroid
    ? DynamicLibrary.open('libnative_add.so')
    : DynamicLibrary.process();
```

之後調用 lookup 來查找符號

```
final int Function(int x, int y) nativeAdd = nativeAddLib
    .lookup<NativeFunction<Int32 Function(Int32, Int32)>>('native_add')
    .asFunction();
```

現在可以使用 nativeAdd 來調用查找到的符號
```
nativeAdd(1,2)
```