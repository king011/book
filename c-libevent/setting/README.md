# [Setting](https://libevent.org/libevent-book/Ref1_libsetup.html)

libevent 的一些過程是在全局設定的，所以你應該首先依照自己的需要對它們進行設定否則在多線程中 libevent 的狀態可能會不一致

# 日誌

libevent 支持記錄日誌和警告。默認它們被輸出到 stderr，你可以調用 event\_set\_log\_callback 來設定自己的日誌記錄器 

```
#define EVENT_LOG_DEBUG 0
#define EVENT_LOG_MSG   1
#define EVENT_LOG_WARN  2
#define EVENT_LOG_ERR   3

/* Deprecated; see note at the end of this section */
#define _EVENT_LOG_DEBUG EVENT_LOG_DEBUG
#define _EVENT_LOG_MSG   EVENT_LOG_MSG
#define _EVENT_LOG_WARN  EVENT_LOG_WARN
#define _EVENT_LOG_ERR   EVENT_LOG_ERR

typedef void (*event_log_cb)(int severity, const char *msg);

void event_set_log_callback(event_log_cb cb);
```

下面是一個例子，每當由日誌需要記錄時，libevent 就會調用回調函數

```
#include <event2/event.h>
#include <stdio.h>

static void discard_cb(int severity, const char *msg)
{
    /* This callback does nothing. */
}

static FILE *logfile = NULL;
static void write_to_file_cb(int severity, const char *msg)
{
    const char *s;
    if (!logfile)
        return;
    switch (severity) {
        case _EVENT_LOG_DEBUG: s = "debug"; break;
        case _EVENT_LOG_MSG:   s = "msg";   break;
        case _EVENT_LOG_WARN:  s = "warn";  break;
        case _EVENT_LOG_ERR:   s = "error"; break;
        default:               s = "?";     break; /* never reached */
    }
    fprintf(logfile, "[%s] %s\n", s, msg);
}

/* Turn off all logging from Libevent. */
void suppress_logging(void)
{
    event_set_log_callback(discard_cb);
}

/* Redirect all Libevent log messages to the C stdio file 'f'. */
void set_logfile(FILE *f)
{
    logfile = f;
    event_set_log_callback(write_to_file_cb);
}
```

在日誌回調函數裏調用 libevent 提供的 api 通常是不安全的，通常 DEBUG 日誌是不會被記錄的，但你可以調用 event\_enable\_debug\_logging 來啓用它(傳入 EVENT\_DBG\_NONE 獲取默認行爲，傳入 EVENT\_DBG\_ALL 啓用 DEBUG 日誌)

```
#define EVENT_DBG_NONE 0
#define EVENT_DBG_ALL 0xffffffffu

void event_enable_debug_logging(ev_uint32_t which);
```

# 錯誤處理

libevent 如果檢測到不可恢復的內部錯誤通常會調用 exit() 或 abort() 結束當前進程。你可以調用 event\_set\_fatal\_callback 來覆蓋默認行爲，但通常你不應該恢復程序繼續運行那樣行爲將是未知的，它的目的通常在於你可以在進程結束前進行一些優雅的收尾工作(例如關閉連接的數據庫)

```
typedef void (*event_fatal_cb)(int err);
void event_set_fatal_callback(event_fatal_cb cb);
```

# 內存管理

默認情況下 libevent 會使用 c 的 malloc/realloc/free 管理內存，但你可以調用 event\_set\_mem\_functions 來接管 libevent 的內存管理，比如用於更高效的內存池或檢測內存泄漏

```
void event_set_mem_functions(void *(*malloc_fn)(size_t sz),
                             void *(*realloc_fn)(void *ptr, size_t sz),
                             void (*free_fn)(void *ptr));
```

下面是一個簡單的例子，它計算來內存的佔用情況

```
#include <event2/event.h>
#include <sys/types.h>
#include <stdlib.h>

/* This union's purpose is to be as big as the largest of all the
 * types it contains. */
union alignment {
    size_t sz;
    void *ptr;
    double dbl;
};
/* We need to make sure that everything we return is on the right
   alignment to hold anything, including a double. */
#define ALIGNMENT sizeof(union alignment)

/* We need to do this cast-to-char* trick on our pointers to adjust
   them; doing arithmetic on a void* is not standard. */
#define OUTPTR(ptr) (((char*)ptr)+ALIGNMENT)
#define INPTR(ptr) (((char*)ptr)-ALIGNMENT)

static size_t total_allocated = 0;
static void *replacement_malloc(size_t sz)
{
    void *chunk = malloc(sz + ALIGNMENT);
    if (!chunk) return chunk;
    total_allocated += sz;
    *(size_t*)chunk = sz;
    return OUTPTR(chunk);
}
static void *replacement_realloc(void *ptr, size_t sz)
{
    size_t old_size = 0;
    if (ptr) {
        ptr = INPTR(ptr);
        old_size = *(size_t*)ptr;
    }
    ptr = realloc(ptr, sz + ALIGNMENT);
    if (!ptr)
        return NULL;
    *(size_t*)ptr = sz;
    total_allocated = total_allocated - old_size + sz;
    return OUTPTR(ptr);
}
static void replacement_free(void *ptr)
{
    ptr = INPTR(ptr);
    total_allocated -= *(size_t*)ptr;
    free(ptr);
}
void start_counting_bytes(void)
{
    event_set_mem_functions(replacement_malloc,
                            replacement_realloc,
                            replacement_free);
}
```

# 多線程

如果要在多線程中使用 libevent，你需要告訴 libevent 如何加鎖。libevent 存在三種情況

1. 一些對象本質上是单線程的，你不能在多線程中使用
2. 一些對象是可選鎖定的，你可以告訴 libevnet 是否要鎖定它們
3. 一些對象是始終鎖定的，你可以在多線程中安全的使用它們

在 win32 和 pthreads 中，libevent 提供來預設支持

```
#ifdef WIN32
int evthread_use_windows_threads(void);
#define EVTHREAD_USE_WINDOWS_THREADS_IMPLEMENTED
#endif
#ifdef _EVENT_HAVE_PTHREADS
int evthread_use_pthreads(void);
#define EVTHREAD_USE_PTHREADS_IMPLEMENTED
#endif
```

這兩個函數都在成功時返回 0，失敗時返回 1

如果使用 event\_use\_pthreads 則要求必須鏈接 event\_pthreads 庫