# 事務

redis 支持了事務

| 命令 | 含義 |
| -------- | -------- |
| DISCARD     | 取消事務     |
| EXEC     | 執行所有事務塊內的命令     |
| MULTI     | 標記一個事務開始     |
| UNWATCH     | 取消WATCH命令對所有 key 的 監視     |
| WATCH key [key ...]     | 監視 一個或 多個 key 如果在事務執行之前這些 key 被改動 打斷事務     |

