# 物體碰撞組件

物理碰撞組件 繼承自 碰撞組件 編輯和設置方法也 同 碰撞組件

物理碰撞組件 有如下屬性

* sensor 指明碰撞體是否爲傳感器類型 傳感器類型的碰撞體會產生碰撞回調 但不會發生物理碰撞效果
* density 碰撞體密度 用於計算剛體質量
* friction 碰撞體摩擦力 碰撞體接觸時的運動會受到摩擦力影響
* restitution 碰撞體的彈性係數 指明碰撞體碰撞時是否會受到彈力影響

# 物理碰撞組件內部細節

物理碰撞組件內部由 box2d 的 b2Fixture 組成 故 一個多邊形物理碰撞組件 可能由多個 b2Fixture 組成

1. 當多邊形物理碰撞組件的頂點組成的行政爲凹邊形時 物理系統會自動將其 分割爲多個 凸邊形
2. 當多邊形物理碰撞組件的頂點多於 b2.maxPolygonVertices (一般爲8) 時 物理系統會自動將這些頂點分割爲多個凸邊形

一般情況下不用 關心這些 但當使用 射線檢測 並且檢測類型爲 cc.RayCastType.All 時 一個碰撞體可能呢個會被檢測到多個碰撞點 因爲檢測到了多個 b2Fixture

# 碰撞回調

剛體 只有 開啓了 碰撞監聽 發送碰撞才會回調到 對應組件上

```
rigidbody.enabledContactListener = true
```

回調中的信息在物理引擎中是以緩存形式存在的 所以只在回調中 有效 不要直接緩存這些信息 但可以 緩存它們的副本

在回調中創建的 物理物體 比如剛體 關節等 不會立刻創建出 box2d對應物體 會在整個物理系統更新完畢後在進行 這些物體的創建

# 定義回調函數

只需要在剛體所在節點上 掛接一個 實現下述方法的 腳本 即可 收到 碰撞回調

```
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    onLoad() {
        // 啓用碰撞 監聽
        const rigidBody = this.node.getComponent(cc.RigidBody)
        rigidBody.enabledContactListener = true
    }

    // 只在兩個碰撞體 開始接觸時 被調用一次
    onBeginContact(contact: cc.PhysicsContact, selfCollider: cc.PhysicsCollider, otherCollider: cc.PhysicsCollider) {
        console.log("onBeginContact")
    }

    // 只在兩個碰撞體 結束接觸時 被調用一次
    onEndContact(contact: cc.PhysicsContact, selfCollider: cc.PhysicsCollider, otherCollider: cc.PhysicsCollider) {
        console.log("onEndContact")
    }

    // 每次將要處理 碰撞體 接觸邏輯時調用
    onPreSolve(contact: cc.PhysicsContact, selfCollider: cc.PhysicsCollider, otherCollider: cc.PhysicsCollider) {
        console.log("onPreSolve")
    }

    // 每次將要處理完 碰撞體 接觸邏輯時調用
    onPostSolve(contact: cc.PhysicsContact, selfCollider: cc.PhysicsCollider, otherCollider: cc.PhysicsCollider) {
        console.log("onPostSolve")
    }
}
```