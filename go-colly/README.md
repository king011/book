# colly

colly 是一個快速優雅的 golang 開源(Apache License 2.0)爬蟲框架

```
go get github.com/gocolly/colly/v2 
```

* 官網 [http://go-colly.org/](http://go-colly.org/)
* 源碼 [https://github.com/gocolly/colly](https://github.com/gocolly/colly)
* 文檔 [http://go-colly.org/docs/](http://go-colly.org/docs/)