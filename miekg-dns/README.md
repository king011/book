# github.com/miekg/dns

miekg/dns 是一個 開源 的 dns 庫 提供了 dns 服務器 和 解析器 需要的 組件

# Example

```
package main

import (
	"github.com/miekg/dns"
	"log"
	"net"
	"sync"
)

const (
	// LAddr dns 監聽地址
	LAddr = ":10053"
)

// DNSTCP 轉發 tcp 請求
type DNSTCP struct {
	l net.Listener
}

// DNSUDP 轉發 udp 請求
type DNSUDP struct {
	p net.PacketConn
}

func main() {
	// 創建 服務器
	var e error
	t := &DNSTCP{}
	e = t.Init(LAddr)
	if e != nil {
		log.Fatalln(e)
	}
	u := &DNSUDP{}
	e = u.Init(LAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("dns tcp work at", LAddr)
	log.Println("dns udp work at", LAddr)

	// 運行 服務器
	var wait sync.WaitGroup
	wait.Add(2)
	go func() {
		t.Run()
		wait.Done()
	}()
	go func() {
		u.Run()
		wait.Done()
	}()
	wait.Wait()
}

// Init 初始化 服務器
func (d *DNSTCP) Init(laddr string) (e error) {
	l, e := net.Listen("tcp", laddr)
	if e != nil {
		return
	}
	d.l = l
	return
}

// Init 初始化 服務器
func (d *DNSUDP) Init(laddr string) (e error) {
	addr, e := net.ResolveUDPAddr("udp", laddr)
	if e != nil {
		return
	}
	d.p, e = net.ListenUDP("udp", addr)
	return
}

// Run 運行 服務
func (d *DNSTCP) Run() {
	e := dns.ActivateAndServe(d.l, nil, d)
	if e != nil {
		log.Fatalln(e)
	}
}

// Run 運行 服務
func (d *DNSUDP) Run() {
	e := dns.ActivateAndServe(nil, d.p, d)
	if e != nil {
		log.Fatalln(e)
	}
}

// ServeDNS s實現 dns.Handler 接口 響應dns 請求
func (d *DNSTCP) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	// 創建 tcp 客戶端
	c := &dns.Client{
		Net: "tcp",
	}
	// 發送 dns 請求
	in, _, e := c.Exchange(r, "8.8.8.8:53")
	if e != nil {
		log.Println(e)
		return
	}
	// 處理 dns 響應
	e = w.WriteMsg(in)
	if e != nil {
		log.Println(e)
		return
	}
}

// ServeDNS s實現 dns.Handler 接口 響應dns 請求
func (d *DNSUDP) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	// 創建 udp 客戶端
	c := &dns.Client{}
	// 發送 dns 請求
	in, _, e := c.Exchange(r, "8.8.8.8:53")
	if e != nil {
		log.Println(e)
		return
	}
	// 處理 dns 響應
	e = w.WriteMsg(in)
	if e != nil {
		log.Println(e)
		return
	}
}
```