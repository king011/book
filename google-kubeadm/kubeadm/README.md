# [創建集羣](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

kubeadm 用於集羣的初始化 執行下述指令在當前 bash 中創建命令自動完成

```
#info="~/.bashrc"
source <(kubeadm completion bash)
```

此外如果在北朝鮮或西朝鮮之類的地方後續操作先設置好全局代理，或 http 代理環境變量

```
export http_proxy=http://127.0.0.1:8118/
export https_proxy=http://127.0.0.1:8118/
export no_proxy=localhost,127.0.0.1,192.168.0.0/16,10.96.0.0/12
```

# 初始化控制平面節點

控制平面節點是運行控制平面組件的機器，包括 etcd(集羣數據庫) 和 API Server(命令行工具 kubectl 與之通信)

```
sudo kubeadm init \
	--pod-network-cidr=10.244.0.0/16
```

對於 1.24 以後的版本傳入 --cri-socket 參數指定使用 cri-dockerd(docker) 作爲運行時

```
sudo kubeadm init \
	--pod-network-cidr=10.244.0.0/16 \
	--cri-socket='unix:///var/run/cri-dockerd.sock' # 
```

* --pod-network-cidr 爲 pod 指 ip 範圍，kube-router 需要顯示指示此屬性

kubeadm 會下載幾個容器鏡像，可以先執行下述指令進行下載，或通過其它手段準備鏡像

```
kubeadm config images pull
```

此外對於 docker 也可以使用如下腳本進行鏡像拉取
```
#!/bin/bash
set -e
for x in $(kubeadm config images list)
do
    docker pull "$x"
  fi
done
```


如果 init 執行成功最終會顯示類似如下信息

```
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.251.5:6443 --token o2kt5e.v8ho7mj5ix6tsvdo \
	--discovery-token-ca-cert-hash sha256:ab3d82d27a1dc1b09b91568feb804c9f8f48b50d0956e8612a1af714734cff76
```

這段信息主要是在說

1. 對於普通用戶將 **admin.conf** 拷貝到 **/etc/kubernetes/admin.conf** 並修改爲自己所有，以便可以執行 kubectl 指令
2. 使用 **kubeadm join** 指令可以將集羣加入到集羣中

# 安裝 Pod 網路附加組件

你必須部署一個基於 Pod 網路插件的 容器網路接口(CNI)，以便你的 Pod 可以互相通信。在安裝網路之前，集羣 DNS(CoreDNS) 不會啓動

你可以在此查看 [網路模型](https://kubernetes.io/zh/docs/concepts/cluster-administration/networking/#how-to-implement-the-kubernetes-networking-model) 的附加組件列表

使用下述指令查看 coredns pod 是否啓動並運行

```
kubectl get pods --all-namespaces | egrep 'NAMESPACE|coredns'
```

# kube-router
使用下述指令在 Kubernetes v1.8+ 環境上安裝 [kube-router](https://github.com/cloudnativelabs/kube-router/blob/master/docs/kubeadm.md)
```
kubectl apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml
```

kube-router 也提供了 kube-proxy 的代理功能所以可以刪除 kube-proxy

1. 刪除 kube-proxy

	```
	kubectl -n kube-system delete ds kube-proxy
	```

2. 清理 kube-proxy (鏡像名稱 tag 可以替換爲 kubeadm init 安裝的那個)

	```
	#info="docker"
	docker run --privileged -v /lib/modules:/lib/modules --net=host k8s.gcr.io/kube-proxy:v1.23.5 kube-proxy --cleanup
	```
	
	```
	#info="containerd"
	ctr images pull k8s.gcr.io/kube-proxy:v1.23.5
	ctr run --rm --privileged --net-host --mount type=bind,src=/lib/modules,dst=/lib/modules,options=rbind:ro \
			k8s.gcr.io/kube-proxy-amd64:v1.23.4 kube-proxy-cleanup kube-proxy --cleanup
	```
	
# 控制平面節點隔離

默認情況下，出於安全原因，集羣不會在控制平面節點上調度 Pod。如果希望能夠在控制平面節點上調度 Pod，例如開發單機的 Kubernetes 集羣，則需要運行

```
#info="1.23"
kubectl taint nodes --all node-role.kubernetes.io/master-
```
```
#info="1.25.1"
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
```

輸出看起來像是這樣
```
node "test-01" untainted
taint "node-role.kubernetes.io/master:" not found
taint "node-role.kubernetes.io/master:" not found
```

這將從任何擁有 **node-role.kubernetes.io/master** taint 標記的節點中移除該標記，包括控制平面節點，這意味着調度程序能夠在任何地方調度 Pods

# 加入節點

節點是工作負載(容器和 Pod 等)運行的地方。要將新節點添加到集羣，需要在每臺計算機執行下列操作(就是 kubeadm init 最後輸出的內容)

```
kubeadm join --token <token> <control-plane-host>:<control-plane-port> --discovery-token-ca-cert-hash sha256:<hash>
```

使用下述指令列出可用 token

```
kubeadm token list
```

通常令牌會在 24小時過期，之後可以使用下述指令創建一個令牌

```
kubeadm token create
```

如果沒有 --discovery-token-ca-cert-hash 值可以輸入如下指令獲取
```
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | \
   openssl dgst -sha256 -hex | sed 's/^.* //'
```

這些操作都需要 **admin.conf** 權限，你可以爲普通用戶生成一個爲其授權的唯一證書，`kubeadm alpha kubeconfig user --client-name <CN>` 命令會將 KubeConfig 檔案輸出到 STDOUT，你可以將其保存爲檔案並發送給用戶。之後使用 **kubectl create (cluster)rolebinding** 授予特權

可以將API服務器代理到本地主機
```
kubectl --kubeconfig ./admin.conf proxy
```
你現在可以在本地訪問 api 服務器 http://localhost:8001/api/v1

# 清理

如果你在集羣中使用了一次性的服務進行測試，則可以關閉這些服務器，而無需進一步清理。你可以使用 **kubectl config delete-cluster** 刪除對集羣的本地引用。

但是如果要更乾淨的取消配置集羣，則應實現清空節點(drain)並確保該節點爲空，然後取消配置該節點。

# 刪除節點
1. 清空節點

	```
	kubectl drain <node name> --delete-emptydir-data --force --ignore-daemonsets
	```
	
2. 重置 kubeadm 狀態

	```
	kubeadm reset
	```
3. 重置過程不會清除 iptables 規則 和 IPVS 表，需要手動執行

	```
	iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X
	```

	```
	ipvsadm -C
	```
	
4. 最後刪除節點

	```
	kubectl delete node <node name>
	```