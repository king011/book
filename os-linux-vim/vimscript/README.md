# vimscript

是 vim 提供的 擴展腳本 配置檔案 .vimrc 也使用此腳本

vimscript 通常使用 **.vim** 作爲後綴名 在一個 vim中 使用 `:source 腳本路徑` 執行腳本

使用 但引號**"** 註釋單行
# 變量

[內建變量](https://yianwillis.github.io/vimcdoc/doc/eval.html#internal-variables)

一個變量 使用 ASCII 字符 數字 下劃線組成 但不能以數字開始

使用 let 定義全局變量 直接使用 let 指令 可以 列出當前定義的所有變量

在變量名前 加上 s: 前綴 可以定義局部變量

```
let s:i = 1
while s:i < 5
  echo "count is" s:i
  let s:i += 1
endwhile
```

| 變量類型 | 變量名前綴 | 含義 |
| -------- | -------- | -------- |
|      | 無     | 函數內(局部於函數) 否則(全局)     |
| buffer-variable     | b:     | 局部於當前緩衝區     |
| window-variable     | w:     | 局部於當前窗口     |
| tabpage-variable     | t:     | 局部於當前標籤頁     |
| global-variable     | g:     | 全局     |
| local-variable     | l:     | 局部於函數     |
| script-variable     | s:     | 局部與 :source 的vim腳本     |
| function-argument     | a:     | 函數參數(只限於函數內使用)     |
| vim-variable     | v:     | vim 預定義的全局變量     |

## 刪除 變量

* 使用 unlet 變量名 指令 刪除 變量
* unlet! 變量名 只在變量存在時才刪除

> 當一個腳本結束時 它的局部變量不會被自動刪除 下次執行時舊值仍然可以被使用
> 

exists() 函數 可以檢查一個變量是否已經被定義過了

## 字符串

vim 使用 "" 定義轉義 字符串 使用 '' 定義非轉義字符串

```
let s:name = "king"
echo s:name
```

## 數值

數值 可以使用 16進制 10進制 8進制 2進制 表示

* 以 0x 0X 開始的是16進制
* 以 0 開始的是 8進制
* 以 0b 0B 開始的是 2進制
* 其它作爲10進制

echo 指令 始終以 10進制 顯示
```
echo 0x7f 036
```


# 表達式
<table border="0" cellpadding="0" cellspacing="0" class="ibm-data-table display dataTable no-footer dtr-inline ibm-widget-processed" data-widget="datatable" id="DataTables_Table_2" role="grid" style="width: 940px;"><thead xmlns:dw="http://www.ibm.com/developerWorks/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><tr role="row"><th class="ibm-background-neutral-white-30 sorting_disabled" style="vertical-align: top; width: 544px;" rowspan="1" colspan="1">运算</th><th class="ibm-background-neutral-white-30 sorting_disabled" style="vertical-align: top; width: 316px;" rowspan="1" colspan="1">运算符语法</th></tr></thead><tbody xmlns:dw="http://www.ibm.com/developerWorks/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><tr role="row" class="odd"><td style="vertical-align:top" tabindex="0"> 赋值<br>
                        数值相加并赋值<br> 数值相减并赋值<br>
                        字符串连接并赋值</td><td style="vertical-align:top"><strong>let</strong><em>var</em><strong>=</strong><em>expr</em><br><strong>let</strong><em> var</em><strong>+=</strong><em>expr</em><br><strong>let</strong><em> var</em><strong>-=</strong><em>expr</em><br><strong>let</strong><em> var</em><strong>.=</strong><em>expr</em></td></tr><tr role="row" class="even"><td style="vertical-align:top" tabindex="0"> 三元运算符 </td><td style="vertical-align:top"><em>bool</em><strong>?</strong><em>expr-if-true</em><strong>:</strong><em>expr-if-false</em></td></tr><tr role="row" class="odd"><td style="vertical-align:top" tabindex="0"> 逻辑 OR </td><td style="vertical-align:top"><em>bool</em><strong>||</strong><em>bool</em></td></tr><tr role="row" class="even"><td style="vertical-align:top" tabindex="0"> 逻辑 AND </td><td style="vertical-align:top"><em>bool</em><strong>&amp;&amp;</strong><em>bool</em></td></tr><tr role="row" class="odd"><td style="vertical-align:top" tabindex="0"> 数值或字符串相等<br>
                        数值或字符串不相等<br> 数值或字符串大于
                        <br> 数值或字符串大于等于<br>
                        数值或字符串小于<br> 数值或字符串小于等于</td><td style="vertical-align:top"><em>expr</em><strong>==</strong><em>expr</em><br><em>expr</em><strong>!=</strong><em>expr</em><br><em>expr</em><strong>&gt;</strong><em>expr</em><br><em>expr</em><strong>&gt;=</strong><em>expr</em><br><em>expr</em><strong>&lt;</strong><em>expr</em><br><em>expr</em><strong>&lt;=</strong><em>expr</em></td></tr><tr role="row" class="even"><td style="vertical-align:top" tabindex="0"> 数值相加<br> 数值相减<br> 字符串连接</td><td style="vertical-align:top"><em>num</em><strong>+</strong><em>num</em><br><em>num</em><strong>-</strong><em>num</em><br><em>str</em><strong>.</strong><em>str</em></td></tr><tr role="row" class="odd"><td style="vertical-align:top" tabindex="0"> 数值相乘<br> 数值相除<br> 数值系数 </td><td style="vertical-align:top"><em>num</em><strong>*</strong><em>num</em><br><em>num</em><strong>/</strong><em>num</em><br><em>num</em><strong>%</strong><em>num</em></td></tr><tr role="row" class="even"><td style="vertical-align:top" tabindex="0"> 转换为数值<br> 求负数<br> 逻辑 NOT </td><td style="vertical-align:top"><strong>+</strong><em>num</em><br><strong>-</strong><em>num</em><br><strong>!</strong><em>bool</em></td></tr><tr role="row" class="odd"><td style="vertical-align:top" tabindex="0"> 括号优先 </td><td style="vertical-align:top"><strong>(</strong><em>expr</em><strong>)</strong></td></tr></tbody></table>

# 條件語句

```
let i = 0
while i < 2
        if i % 2 == 0
                echo '陰'
        else
                echo '陽'
        endif

        let i+=1
endwhile
```

還可以使用 elseif

類似c語言 所有 非 0 值 都作爲 true


# 函數

使用 call 函數名(參數 調用函數)

```
" 使用 range 的函數 使用 10,20call 函數名
" 會自動傳入 10 作爲 firstline 20作爲 lastline 傳入
function! Echo() range
        let n = a:firstline
        while n <= a:lastline
                echo getline(n)
                let n += 1 
        endwhile
endfunction

" 使用 ... 定義 可變參數
function Add(start,...)
        let sum = a:start
        let i = 0 
        " a:0 記錄了 可變參數長 a:n 記錄了具體值
        while i < a:0 
                let sum += a:{i+1}
                let i += 1
        endwhile
        return sum 
endfunction

call pow(2,3)
let i = pow(2,8)
echo i
echo Min(3,4)
echo Add(1,2,3,4,5)
```

## 刪除函數

使用 delfunction 函數名 刪除函數

## 函數引用

使用 function 將函數 保存到一個 變量中 保存函數的變量名 必須以大寫字母開始

```
function! Min(l,r)
        " 函數變量 需要使用 a: 前綴
        return a:l < a:r ? a:l : a:r
endfunction
let MyMin = function("Min")

" 使用 call 調用 第二個參數 是 傳入函數的 參數列表
echo call(MyMin,[1,2])
```

# 列表

```
" 創建列表
let names = ['king','anita']
" 連接列表
let names += ['kate']
" 添加元素
call add(names,'kk')

for name in names
        echo name
endfor

" 0 1 2
for v in range(3)
        echo v
endfor

" 指定 指定開始值 結束值 步長  
for v in range(1,-10,-2)
        echo v
endfor
```

# 字典

字典是 key value 鍵值對

```
let k = {"king":1,"kate":2,100:"yes"}
echo k["king"]

" keys 函數 返回 字典的 key 列表
for key in keys(k)
        echo key
endfor
```

# 面向對象
類似 javascript 但 不支持閉包

```
function! NewAnimal()
	let m = {}
	function m.speak()
		echo 'animal speak'
	endfunction
	return m
endfunction

function! NewCat()
	let m = NewAnimal()
	function m.eat()
		echo 'cat eat'
	endfunction
	return m
endfunction

function! NewDog(name)
	let m = NewAnimal()
	let m._name = a:name
	" 覆蓋 實現
	function! m.speak()
		" 使用 self 訪問自己
		echo self._name 'speak'
	endfunction
	return m
endfunction

let a = NewAnimal()
call a.speak()

let cat = NewCat()
call cat.eat()
call cat.speak()

let dog1 = NewDog("dog 1")
call dog1.speak()

let dog2 = NewDog("dog 2")
call dog2.speak()
call dog1.speak()
```

