# [Tooltip](https://primeng.org/tooltip)

```
import {TooltipModule} from 'primeng/tooltip';
```

pTooltip 指令創建一個 tooltip，在鼠標懸浮時會顯示一個小的文本提示給用戶

```
<input type="text" pTooltip="Enter your username">
```

## Position

可以通過 **tooltipPosition** 屬性來設置 tooltip 顯示的位置，它支持4個有效的值

* **right** (這是默認值)
* **left**
* **top**
* **bottom**

```
<input type="text" pTooltip="Enter your username" tooltipPosition="top">
```

## Events

默認情況下 tooltip 將在組件 hover 時顯示，你可以通過設置 **tooltipEvent** 屬性讓它在 focus 時顯示

* **hover** (這是默認值)
* **focus**

```
<input type="text" pTooltip="Enter your username" tooltipPosition="top" tooltipEvent="focus">
```

## Delay

使用 **showDelay** **hideDelay** 屬性來控制 tooltip 延遲顯示與隱藏

```
<input type="text" pTooltip="Enter your username" tooltipPosition="top" tooltipEvent="focus" [showDelay]="1000" [hideDelay]="500">
```

## Scrolling Containers

不太明白官方文檔的意思下面只是貼出了官方代碼

```
<div #container style="display:inline-block;position:relative">
    <input type="text" pInputText pTooltip="Enter your username" placeholder="Right" [appendTo]="container">
</div>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| pTooltip     | string     | null     | tooltip 顯示的文本     |
| tooltipPosition     | string     | right     | tooltip 顯示位置: right, left, top or bottom.     |
| fitContent     | boolean     | true     | 當所選位置(tooltipPosition) 上沒有足夠的空間時，自動調整元素位置     |
| tooltipEvent     | string     | hover     | tooltip 在什麼時候顯示: hover or focus      |
| positionStyle     | string     | absolute     | Type of CSS position.     |
| tooltipDisabled     | boolean     | false     | 是否禁用此 tooltip 組件     |
| appendTo     | string     | any     | Target element to attach the overlay, valid values are "body", "target" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| hideDelay     | number     | null     | 延遲多少毫秒隱藏     |
| showDelay     | number     | null     | 延遲多少毫秒顯示     |
| life     | number     | null     | 等待多少毫秒隱藏 tooltip，即使 tooltip 處於活躍     |
| tooltipStyleClass     | string     | null     | Style class of the tooltip.     |
| escape     | boolean     | true     | 默認情況下 tooltip 中內容以文本呈現，如果要支持內容包含 html 則設置此屬性爲 false     |
| tooltipZIndex     | string     | auto     | 是否應該自動管理以始終位於頂部或具有固定值     |
| autoHide     | boolean     | true     | 懸停在 tooltip 內容上時是否隱藏 tooltip     |

# Styling

| Class | Element |
| -------- | -------- |
| p-tooltip     | tooltip 容器元素     |
| p-tooltip-arrow     | tooltip 的箭頭     |
| p-tooltip-text     | tooltip 的文本     |


