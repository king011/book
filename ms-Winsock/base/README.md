# API SPI

winsock 包含兩種 接口

* API 提供了 應用調用 TCP/IP 網路的 功能 
* SPI 分層服務提供者 提供了插入新層來實現 網路防火牆/透明代理/監控過濾數據/hook socket 的一個方案

> 對於網路 應用來說 只需要關心 API 即可
> 
> 對於要 hook socket 來說 可以只關心 SPI 即可
> 

# 初始化/釋放 socket 環境

winsock 有多個 版本的 實現 現在通常都是 使用 ws2_32.dll 中的 v2.2

所有 exe/dll 應該首先調用 WSAStartup 向 windows 註冊 要使用的 版本 之後才可以調用 socket api

當 不在使用時 調用 WSACleanup 釋放 資源

> WSAStartup 應該和 WSACleanup 一一對應
> 
> WSAStartup/WSACleanup 使用了 引用計數 程式只有第一個 WSAStartup 會分配資源 最後一個 WSACleanup 釋放資源
>

```cpp
// 註冊 要使用的 winsock 版本
int WSAStartup(
  WORD      wVersionRequired, 
  LPWSADATA lpWSAData // 返回 實際支持的 版本
);

// 清理 winsock 資源
int WSACleanup(

);
```

```cmake
if(WIN32)
    list(APPEND target_definitions 
        -DWIN32
        -DWIN32_LEAN_AND_MEAN
        -D_WIN32_WINNT=0x0601
    )
    list(APPEND target_libs
        stdc++
        -lws2_32
        -lwsock32
    )
endif()
```

```cpp
#info="utils.hpp"
#if !defined(_UTILS_HPP)
#define _UTILS_HPP
#include <windows.h>
#include <winsock2.h>

#include <iostream>
#include <sstream>

#include <boost/noncopyable.hpp>

// 異常定義
class exception : public std::exception
{
    std::string _e;

  public:
    exception(const char *emsg = nullptr)
    {
        if (emsg)
        {
            _e = emsg;
        }
    }

    virtual const char *what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT
    {
        return _e.c_str();
    }
    template <typename T>
    exception &operator<<(const T &v)
    {
        std::ostringstream o;
        o << v;
        _e += o.str();
        return *this;
    }
};

// winsocket 執行環境
class scoped_winsocket_t : boost::noncopyable
{
  private:
    bool _cleanup = false;

  public:
    // WSAStartup
    // throw std::exception
    scoped_winsocket_t()
    {
        // 初始化 v2.2
        WSAData data;
        int ec = WSAStartup(MAKEWORD(2, 2), &data);
        if (ec != 0)
        {
            exception e("WSAStartup err : ");
            switch (ec)
            {
            case WSASYSNOTREADY:
                e << "WSASYSNOTREADY";
                break;
            case WSAVERNOTSUPPORTED:
                e << "WSAVERNOTSUPPORTED";
                break;
            case WSAEINPROGRESS:
                e << "WSAEINPROGRESS";
                break;
            case WSAEPROCLIM:
                e << "WSAEPROCLIM";
                break;
            case WSAEFAULT:
                e << "WSAEFAULT";
                break;
            default:
                e << "unknow";
                break;
            }
            throw e;
        }

        // 驗證 釋放支持 v2.2
        if (LOBYTE(data.wVersion) != 2 || HIBYTE(data.wVersion) != 2)
        {
            WSACleanup();
            throw exception("WSAStartup err : Could not find a usable version of Winsock.dll");
        }
        _cleanup = true;
    }
    // WSACleanup
    ~scoped_winsocket_t()
    {
        if (_cleanup)
        {
            WSACleanup();
        }
    }
};
#endif // _UTILS_HPP
```
```cpp
#info="main.cpp"
#include <iostream>
#include "utils.hpp"

void do_something()
{
    std::cout << "do something about winsocket\n";
}
int main(int argc, char *argv[])
{
    try
    {
        // init
        scoped_winsocket_t context;

        // do something
        do_something();
    }
    catch (const std::exception &e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }
    return 0;
}
```