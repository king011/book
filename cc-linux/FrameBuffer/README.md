# FrameBuffer

[FrameBuffer](http://betteros.org/tut/graphics1.php) 是linux 內核 2.2 開始提供的一個驅動程序接口 用於用戶態進程直接對屏幕進行讀寫

通常可以用來截取屏幕

```
#include <iostream>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/fb.h>

#include <cerrno>
#include <cstring>

#include <boost/timer/timer.hpp>

class exception : public std::exception
{
private:
    const char *message;

public:
    exception(const char *message)
    {
        this->message = message;
    }
    virtual const char *what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW
    {
        return message;
    }
};

// 創建一個包裝FrameBuffer的類
class FrameBuffer
{
private:
    int fd;

public:
    FrameBuffer()
    {
        // 打開 FrameBuffer 設備
        fd = open("/dev/fb0", O_RDONLY);
        if (fd < 0)
        {
            throw exception(strerror(errno));
        }
    }
    virtual ~FrameBuffer()
    {
        close(fd);
    }
    // 獲取 屏幕信息
    void screeninfo(fb_var_screeninfo &screeninfo) const
    {
        if (ioctl(fd, FBIOGET_VSCREENINFO, &screeninfo) < 0)
        {
            throw exception(strerror(errno));
        }
    }
    // 讀取屏幕像素
    int read_frame(void *buf, size_t bytes) const
    {
        int n = read(fd, buf, bytes);
        if (n < 0)
        {
            throw exception(strerror(errno));
        }
        return n;
    }
};

int main(int argc, char *argv[])
{
    void *buf = nullptr;
    try
    {
        FrameBuffer fb;
        fb_var_screeninfo info;
        fb.screeninfo(info);
        std::cout << "每個像素 bit 位: " << info.bits_per_pixel << "\n"
                  << "屏幕分辨率 寬高: " << info.xres << " * " << info.yres << "\n"
                  << "r 分量: " << info.red.length << "bit, " << info.red.offset << "bit offset\n"
                  << "g 分量: " << info.green.length << "bit, " << info.green.offset << "bit offset\n"
                  << "b 分量: " << info.blue.length << "bit, " << info.blue.offset << "bit offset\n"
                  << "a 分量: " << info.transp.length << "bit, " << info.transp.offset << "bit offset\n";
        std::cout << std::flush;
        size_t buffer_size = (info.xres * info.yres * info.bits_per_pixel / 8);
        buf = malloc(buffer_size);
        if (!buf)
        {
            throw exception("malloc error");
        }
        boost::timer::cpu_timer timer;
        fb.read_frame(buf, buffer_size);
        boost::timer::cpu_times duration = timer.elapsed();
        std::cout << "duration: " << duration.wall / 1000 / 1000 << "ms" << std::endl;
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
    if (buf)
    {
        free(buf);
    }
    return 0;
}
```

不過本喵測試時只能截到字符界面的圖形 在圖形界面下截圖爲黑屏