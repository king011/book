# 變量

* lua 支持 全局變量 和 局部變量
* 局部變量 訪問快於 全局變量
* 使用 local 定義局部變量
* 變量可以多個賦值並且等號左右兩邊可以不用匹配

```
g = "這是一個 全局變量"
local l = "這是一個 局部變量"

-- 支持多重賦值
local a, b = g, l

-- swap
a, b = b, a
print(a, b)

a, b = 1, 2, 3, 4
print(a, b) -- 1 2
a, b = 1
print(a, b) -- 1 nil
```

# 循環

lua 支持三種循環，並且循環可以互相嵌套

* while
* repeat until
* for

## while
```
while 判斷式 do
	...
end 
```

## repeat until
```
repeat
	...
until 判斷式
```

## for

for 從 exp1 循環到 exp2 每次步長為 exp3 
* exp3 不寫 默認 為 1
* exp1 2 3 在循環開始一次性求值 不能改變其值 否則 發生未定義行為

```
for exp1,exp2,exp3 do
	...
end
```

```
for i = 10, 1, -1 do
    print(i)
end
```

for 可以用於迭代器
```
for k,v in iterator do
	...
end
```

```
days = {"Sunday", "Momday", "Tuesday", "Wednesday", "Tuesday", "Friday", "Saturday"}
for k, v in pairs(days) do --pairs為一個 訪問 table 的 迭代器
    print(k .. "=" .. v)
end

for k in pairs(days) do -- value 可以不接收
    print(k)
end
```

## 循環控制

* break 退出當前循環
* return 函數返回

lua 不支持 contime，也不支持 goto(5.2 支持 goto)

在 5.1 版本想使用 continue 可以在循環中嵌入一個只執行一次的循環就像這樣

```
for i = 1, 10 do
    repeat
        if i%2==0 then
            break -- 使用 break 模擬 continue 的功能
        end
        print(i)
    until true
end
```

不過上述方法會導致 break 無法使用，如果要正常使用 break 可以在加入一個 變量控制是否退出外層循環

```
local exit=false
for i = 1, 10 do
    repeat
        if i == 5 then
            exit = true -- 設置標記退出外層循環
            break
        elseif i%2==0 then
            break -- 使用 break 模擬 continue 的功能
        end
        print(i)
    until true
    
    if exit then
        break
    end
end
```

# 流程控制

lua 不支持 switch 只能使用 elseif

```
if 判斷式 then
	...
elseif 判斷式 then
	...
elseif 判斷式 then
	...
else
	...
end
```

# 運算符

lua 支持四種運算符

* 算術運算符
* 關係運算符
* 邏輯運算符
* 其它運算符

## 算術運算符

算術運算符對數字進行數學運算

下面例子中 A 爲 10，B 爲 20

| 算符 | 描述 | 實例 |
| -------- | -------- | -------- |
| +     | 加法     | A + B 輸出結果 30     |
| -     | 減法     | A - B 輸出結果 -10     |
| *     | 乘法     | A * B 輸出結果 200     |
| /     | 除法     | B / A 輸出結果 2     |
| %     | 取餘     | B % A 輸出結果 0     |
| ^     | 乘冪     | A^2 輸出結果 100     |
| -     | 負號     | -A 輸出結果 -10     |

lua 不支持 ++ -- += 等寫法

## 關係運算符

關係運算符用於對兩個數值進行比較

下面例子中 A 爲 10，B 爲 20

| 算符 | 描述 | 實例 |
| -------- | -------- | -------- |
| ==     | 等於     | A == B 輸出結果 false     |
| ~=     | 不等於     | A ~= B 輸出結果 true     |
| >     | 大於     | A > B 輸出結果 false     |
| <     | 小於     | A < B 輸出結果 true     |
| >=     | 大於或等於     | A >= B 輸出結果 false     |
| <=     | 小於或等於     | A <= B 輸出結果 true     |


對於 table userdata function 可以比較是否相等，但 lua 比較的是引用(既然=是否指向同一個實例)而非值

## 邏輯運算符

邏輯運算符用於連接多個判別式

下面例子中 A 爲 true，B 爲 false

| 算符 | 描述 | 實例 |
| -------- | -------- | -------- |
| and     | 邏輯 與     | A and B 輸出結果 false     |
| or     | 邏輯 或     | A or B 輸出結果 true     |
| not     | 邏輯 非     | not(A and B) 輸出結果 true     |

## 其它運算符

| 算符 | 描述 | 實例 |
| -------- | -------- | -------- |
| ..     | 連接字符串     | "Hello" .. " World" 輸出結果 "Hello World"     |
| #     | 返回字符串或 table 的長度     | #"Hello" 輸出結果 5     |


## 運算符優先級

![](assets/order.png)
