# WATCHER

1. 每個觀察者 都以 ev\_TYPE 命名
2. 每個觀察者都提供了 ev\_TYPE\_set 和 ev\_TYPE\_init 宏 來配置 
3. 爲了使觀察者實時監視事件 必須使用 ev\_TYPE\_start 啓動 使用 ev\_TYPE\_stop 來 停止監視 (只要監視器處於 活動狀態 就不能 重設她)
4. 每個回調都接收3個參數 事件循環指針 註冊的觀察者結構 事件類型
5. 每種事件類型都包含一位(可能同時接收多個事件)


| 事件掩碼 | 含義 | 
| -------- | -------- |
| EV_READ     | ev_io 監視的檔案已經可讀     |
| EV_WRITE     | ev_io 監視的檔案已經可寫     |
| EV_TIMER     | ev_timer 監視器 已經超時     |
| EV_PERIODIC     | ev_periodic 監視器 已經超時     |
| EV_SIGNAL     | ev_signal 監視的 singnal 已經被線程接收     |
| EV_CHILD     | ev_child 監視的pid已收到狀態更改     |
| EV_STAT     | ev_stat 監視的指定路徑以某種方式更改了其屬性     |
| EV_IDLE     | ev_idle 監視器發現無事可做     |
| EV_PREPARE     | ev_prepare 在loop開始收集事件前調用     |
| EV_CHECK     | ev_check 在loop收集事件後調用     |
| EV_EMBED     | ev_embed     |
| EV_FORK     | ev_fork     |
| EV_CLEANUP     | ev_cleanup 事件循環即將被破壞     |
| EV_ASYNC     | ev_async 指定的異步觀察者已被通知     |
| EV_CUSTOM     | 絕對不會被 libev本身發送 但用戶可以使用它向觀察者發出信號 例如使用 ev_feed_event     |
| EV_ERROR     | 發送了未指定的錯誤 觀察者已經停止 通常是內存不足 或檔案描述符被關閉無法正常啓動監視 好的程序不應該會收到任何此類錯誤     |

# GENERIC WATCHER FUNCTIONS

```
// 初始化 觀察者 通用部分 之後需要調用 ev_TYPE_set 設置觀察者 獨有屬性
ev_init (ev_TYPE *watcher, callback);

// 設置觀察者 獨有的 屬性
ev_TYPE_set (ev_TYPE *watcher, [args])

// ev_init 和 ev_TYPE_set 的語法糖
ev_TYPE_init (ev_TYPE *watcher, callback, [args])

// 啓動觀察者 如果觀察者已經處於活動狀態 則不會發送任何事情
ev_TYPE_start (loop, ev_TYPE *watcher)

// 停止觀察者 並清除掛起狀態 (如果要重用 觀察者 最好調用此函數 確認觀察者 不處於 啓動或掛起狀態)
ev_TYPE_stop (loop, ev_TYPE *watcher)

// 如果觀察者 處於活動狀態(既已啓動 但 尚未停止) 則返回 true
bool ev_is_active (ev_TYPE *watcher)

// 當觀察者待處理 返回 true 此時不可重設觀察者或 free
bool ev_is_pending (ev_TYPE *watcher)

// 返回/設置 觀察者 回調函數
callback ev_cb (ev_TYPE *watcher)
ev_set_cb (ev_TYPE *watcher, callback)

// 返回 設置 觀察者 優先級 
// 優先級是 EV_MINPRI(-2) 到 EV_MAXPRI(2)間的整數 值越高越先被回調
ev_set_priority (ev_TYPE *watcher, int priority)
int ev_priority (ev_TYPE *watcher)

//使用 指定循環 和事件 調用觀察者
ev_invoke (loop, ev_TYPE *watcher, int revents)
// 如果觀察者處於 pending 狀態 則返回事件 並清除 pending 狀態 就好像 觀察者被真實調用了一樣
int ev_clear_pending (loop, ev_TYPE *watcher)

// 向觀察者模擬一個事件
ev_feed_event (loop, ev_TYPE *watcher, int revents)
```

# WATCHER 狀態

* initialised 通過 ev\_init 對觀察者進行了 初始化
* started/running/active ev\_TYPE\_start 之後的狀態
* pending 一個 觀察者是 active 並且一個讓她感興趣的事件到來 進入 pending狀態
* stopped 調用 ev\_TYPE\_stop 後 與 initialised 狀態相同


