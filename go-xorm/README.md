# go-xorm

go-xorm 是一個開源(BSD)的 數據庫 orm

* 官網 [https://xorm.io/](https://xorm.io/)
* 源碼 [https://gitea.com/xorm/xorm](https://gitea.com/xorm/xorm)
* 文檔 [https://gobook.io/read/gitea.com/xorm/manual-zh-CN/](https://gobook.io/read/gitea.com/xorm/manual-zh-CN/)

```
go get xorm.io/xorm 
```

* 支持Struct和数据库表之间的灵活映射，并支持自动同步表结构
* 事务支持
* 支持原始SQL语句和ORM操作的混合执行
* 使用连写来简化调用
* 支持使用ID, In, Where, Limit, Join, Having, Table, Sql, Cols等函数和结构体等方式作为条件
* 支持级联加载Struct
* 支持LRU缓存 
* 支持反转，即根据数据库自动生成xorm的结构体
* 支持事件
* 支持created, updated, deleted和version记录版本（即乐观锁）
