# [compose](https://docs.docker.com/compose/compose-file/)

Compose 規範合併了docker-compose 2.x 3.x 的一些屬性，是最新的規範標準

compose 支持加載多個檔案，但如果不指定的化將查找當前路徑下的下列檔案（優先級從高到低）
* compose.yaml
* compose.yml
* docker-compose.yaml
* docker-compose.yml

中文翻譯 [https://www.kancloud.cn/yiyanan/docker-compose/2240704](https://www.kancloud.cn/yiyanan/docker-compose/2240704)
# [services](https://docs.docker.com/compose/compose-file/#services-top-level-element)

services:Record&lt;string,&nbsp;Service&gt; 是頂層元素用於定義項目服務

服務由一組容器構成，其名字作爲 services 的鍵，值則定義了服務的各種細節
```
services:
  app0:
    image: busybox
    environment:
      - COMPOSE_PROJECT_NAME
    command: echo "I'm running ${COMPOSE_PROJECT_NAME}"
  app1:
    image: busybox
    environment:
      - COMPOSE_PROJECT_NAME
    command: echo "I'm running ${COMPOSE_PROJECT_NAME}"
```

上面例子定義了 app0 app1 兩個服務

## [build](https://docs.docker.com/compose/compose-file/#build)

build 指定用於從源構建容器的構建配置，對於 compose 這是可選的

## [blkio_config](https://docs.docker.com/compose/compose-file/#blkio_config)

blkio_config 定義了一組選項來爲服務設置 IO 限制

```
services:
  foo:
    image: busybox
    blkio_config:
       weight: 300
       weight_device:
         - path: /dev/sda
           weight: 400
       device_read_bps:
         - path: /dev/sdb
           rate: '12mb'
       device_read_iops:
         - path: /dev/sdb
           rate: 120
       device_write_bps:
         - path: /dev/sdb
           rate: '1024k'
       device_write_iops:
         - path: /dev/sdb
           rate: 30
```

**weight**

weight 分配給該服務的帶寬相對於其它服務的比例

有效值是 [10,1000]，默認值爲 500

**weight\_device**

weight\_device 列表指定每項必須包含完整的兩個屬性

* **path**: 受影響的設備路徑
* **weight**: [10,1000] 的整數定義帶寬相對比例

**device\_read\_bps, device\_write\_bps**

列表指定每項必須包含完整的兩個屬性用於爲設備指定每秒讀寫的字節數限制

* **path**: 受影響的設備路徑
* **weight**: 表示字節數的整數，或者表示字節值的字符串

**device\_read\_iops, device\_write\_iops**

列表指定每項必須包含完整的兩個屬性用於爲設備指定每秒讀寫的次數

* **path**: 受影響的設備路徑
* **rate**: 一個整數表示每秒讀寫次數上限

> 所有 path 都必須是設備符號，不可以限制平臺檔案

## [cpu_count](https://docs.docker.com/compose/compose-file/#cpu_count) 測試無效

一個整數定義服務可以使用的 cpu 數量

## [cpu_percent](https://docs.docker.com/compose/compose-file/#cpu_count) 測試無效

定義服務可以使用的 cpu 比例

## [deploy](https://docs.docker.com/compose/compose-file/#deploy)

deploy 指定了服務部署的生命週期,其定義[在此](https://docs.docker.com/compose/compose-file/deploy/)

