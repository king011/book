# Debian/Ubuntu (apt-get)

APT 既 Advanced Packaging Tools 的縮寫 是 Debian 及其 衍生發行版的 軟體包管理器

## 常用 命令

| apt 命令 | 取代的舊命令 | 功能 |
| -------- | -------- | -------- |
| apt install     | apt-get install     | 安裝套件     |
| apt remove     | apt-get remove     | 移除套件     |
| apt purge     | apt-get purge     | 移除套件及配置檔案     |
| apt autoremove     | apt-get autoremove     | 自動刪除 不需要的 套件     |
| apt update     | apt-get update     | 刷新存儲庫索引     |
| apt upgrade     | apt-get upgrade     | 升級所有可升級的 套件     |
| apt full-upgrade     | apt-get dist-upgrade     | Text     |
| apt search     | apt-cache search     | 搜索套件     |
| apt show     | apt-cache show     | 顯示套件細節     |


## dpkg

dpkg 既 Debian Package 的 縮寫 是 APT 的 底層工具

```sh
# 安裝套件
dpkg -i xxx.deb

# 刪除套件
dpkg -r xxx

# 刪除套件 及配置
dpkg -P xxx

# 列出包關聯的 檔案
dpkg -L xxx

# 顯示 套件 版本
dpkg -l xxx

# 顯示 當前安裝的 套件
dpkg -l

# 解開 deb 包
dpkg --unpack xxx.deb

...
```

## 清空下載緩存

**/var/cache/apt/archives** 檔案夾 中 存儲的是 使用 apt 安裝套件時下載的 套件 安裝包

使用 apt-get clean 可以 清空 此緩存

```sh
#info=false
sudo apt clean
```