# 文本框 TextView

[android.widget.TextView](https://developer.android.com/reference/android/widget/TextView) 用來顯示文本 可以顯示 單行/多行/帶圖像/帶Link 的文本

![](assets/TextView.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal"
        android:gravity="center">
        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:autoLink="web|email"
            android:text="https://book.king011.com\nzuiwuchang@gmail.com\ncerberus is an idae"
            />
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```



| 屬性 | 含義 |
| -------- | -------- |
| android:autoLink     | 指定是否將指定格式的文本轉爲link 多個值用 | 組合 支持 none web email phone map all     |
| android:drawableTop     | 在文本框 頂部 繪製圖像     |
| android:drawableBottom     | 在文本框 底部 繪製圖像     |
| android:drawableLeft     | 在文本框 左側 繪製圖像     |
| android:drawableRight     | 在文本框 右側 繪製圖像     |
| android:gravity     | 設置文本對齊方式     |
| android:hint     | 設置文本爲空時 顯示的提示文本     |
| inputType     | 文本框類型 textPassword textEmailAddress phone date ...     |
| android:singleLine     | 爲 true 則爲單行 模式 默認 false     |
| android:text     | 顯示的文本內容     |
| android:textColor     | 文本顯示顏色     |
| android:textSize     | 字體大小     |
| android:width     | 寬度     |
| android:height     | 高度     |

# 輸入框 EditText
[android.widget.EditText](https://developer.android.com/reference/android/widget/EditText) 是TextView的子類 和 TextView 的唯一區別在於 提供了 文本輸入功能

# 普通按鈕 Button
[android.widget.Button](https://developer.android.com/reference/android/widget/Button) 同樣是TextView的子類 提供了一個 點擊事件供 用戶使用

![](assets/Button.png)
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal"
        android:gravity="center">
        <Button
            android:id="@+id/speak"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Speak"
            android:textAllCaps="false"
            android:onClick="Speak"/>
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    static final private String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setOnClickListener 可以動態 設置/改變 響應 函數
        Button btn =  findViewById(R.id.speak);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"汪~嗷嗚");
            }
        });
    }
    // xml 中 可以通過 android:onClick 屬性 設置 響應 函數
    public void  Speak(View v){
        Log.i(TAG,"喵~嗷嗚");
    }
}
```

# 圖像視圖 ImageView
[android.widget.ImageView](https://developer.android.com/reference/android/widget/ImageView) 可以顯示 任何 Drawable 對象 通常用來顯示 圖片

![](assets/ImageView.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal"
        android:gravity="center">

        <ImageView
            android:id="@+id/imageView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:srcCompat="@android:drawable/btn_star_big_on" />

    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

| 屬性 | 含義 |
| -------- | -------- |
| android:adjustViewBounds     | 是否調整自己的邊界來保持顯示圖像的 長寬比     |
| android:maxWidth     | android:adjustViewBounds 爲 true 才有效 設置 組件最大寬度     |
| android:maxHeight     | android:adjustViewBounds 爲 true 才有效 設置 組件最大高度     |
| android:src     | 要顯示的圖像資源id     |
| android:tint     | 爲圖像着色     |
| android:scaleType     | 指示如何 縮放圖像     |

android:scaleType 取值
* matrix 
* fitXY 縮放圖像 填充滿 組件
* fitStart 保持寬高比例縮放 指定圖像完全顯示 完成後 圖像位於 ImageView 左上角 
* fitCenter 保持寬高比例縮放 指定圖像完全顯示 完成後 圖像位於 ImageView 中心
* fitEnd 保持寬高比例縮放 指定圖像完全顯示 完成後 圖像位於 ImageView 右下角
* center 將圖像 放置到 ImageView 中心 但不要縮放
* centerCrop 保持寬高比例縮放 使圖像完全覆蓋 ImageView
* centerInside 保持寬高比例縮放 使 ImageView 能夠完全顯示 圖像 

# 圖像按鈕 ImageButton

[android.widget.ImageButton](https://developer.android.com/reference/android/widget/ImageButton) 是 ImageView 的子類 爲圖像 添加了按鈕功能

默認 情況下 ImageButton 表現的像 是將 圖像 覆蓋到 Button 上

如果 要完全定製 ImageButton 的按鈕 三態顯示效果 需要安裝如下步驟

1. res/drawable/ 下定義一個 selector 用於指定 不同狀態下 要顯示的圖像

	```
	 <?xml version="1.0" encoding="utf-8"?>
	 <selector xmlns:android="http://schemas.android.com/apk/res/android">
			 <item android:state_pressed="true"
						 android:drawable="@drawable/button_pressed" /> <!-- pressed -->
			 <item android:state_focused="true"
						 android:drawable="@drawable/button_focused" /> <!-- focused -->
			 <item android:drawable="@drawable/button_normal" /> <!-- default -->
	 </selector>
	```
1. 將 ImageButton 的 android:src 指向定義的 selector
1. 將 ImageButton 的 android:background 屬性指定爲 透明

# 單選按鈕 RadioGroup RadioButton
* [android.widget.RadioGroup](https://developer.android.com/reference/android/widget/RadioGroup.html) 定義一個單選組
* [android.widget.RadioButton](https://developer.android.com/reference/android/widget/RadioButton) 定義組中的單選按鈕

![](assets/RadioButton.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center">

        <RadioGroup
            android:id="@+id/radioGroup"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="horizontal">
            <RadioButton
                android:id="@+id/cat"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="喵~"
                android:checked="true"/>
            <RadioButton
                android:id="@+id/dog"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="汪~"/>
            <RadioButton
                android:id="@+id/mouse"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="吱~"/>
        </RadioGroup>

        <LinearLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content">
            <Button
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textAllCaps="false"
                android:text="Speak"
                android:onClick="Speak"/>
            <Button
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textAllCaps="false"
                android:text="who am i?"
                android:onClick="Who"/>
        </LinearLayout>
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    static final private String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RadioGroup group = findViewById(R.id.radioGroup);
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(TAG,"changed to id = "+ Integer.toString(checkedId));
            }
        });
    }
    public void  Speak(View v){
        // 依據 isChecked 判斷選中 項目
       int [] ids = { R.id.cat,R.id.dog,R.id.mouse};
       for (int id:ids){
           RadioButton btn = findViewById(id);
           if(btn.isChecked()) {
               Log.i(TAG,btn.getText().toString());
               break;
           }
       }
    }
    public void Who(View view){
        // getCheckedRadioButtonId 返回選中項 id/-1
        RadioGroup group = findViewById(R.id.radioGroup);
        int id = group.getCheckedRadioButtonId();
        switch (id){
            case R.id.cat:
                Log.i(TAG,"you are cat");
                break;
            case R.id.dog:
                Log.i(TAG,"you are dog");
                break;
            case R.id.mouse:
                Log.i(TAG,"you are mouse");
                break;
            default:
                Log.i(TAG,"you are monster");
                break;
        }
    }
}
```

# 複選框 CheckBox

[android.widget.CheckBox](https://developer.android.com/reference/android/widget/CheckBox) 提供了複選框功能

![](assets/CheckBox.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal"
        android:gravity="center">

        <CheckBox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="喵~"/>
        <CheckBox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="汪~"/>
        <CheckBox
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="吱~"/>
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

# 日期選擇器 DatePicker
[android.widget.DatePicker](https://developer.android.com/reference/android/widget/DatePicker) 提供了日期選擇功能

# 時間選擇器 TimePicker
[android.widget.TimePicker](https://developer.android.com/reference/android/widget/TimePicker) 提供了時間選擇功能

# 計時器 Chronometer

[android.widget.Chronometer](https://developer.android.com/reference/android/widget/Chronometer) 是一個計時器 自動更新 經過的時間

![](assets/Chronometer.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center">

        <!--android:format 默認爲 %s-->
       <Chronometer
           android:id="@+id/chronometer"
           android:layout_width="wrap_content"
           android:layout_height="wrap_content"
           android:format="已用時 : %s"/>

        <LinearLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content">
            <Button
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="開始"
                android:onClick="onStart"/>
            <Button
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="停止"
                android:onClick="onStop"/>
        </LinearLayout>
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Chronometer;

public class MainActivity extends AppCompatActivity {
    static final private String TAG="MainActivity";
    private boolean run = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Chronometer chronometer = findViewById(R.id.chronometer);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                // 20 秒後 自動 停止 計時器
                if(SystemClock.elapsedRealtime() - chronometer.getBase() >= 20 * 1000){
                    run = false;
                    chronometer.stop();
                }
            }
        });
    }
    public void onStart(View view){
        if(run){
            return;
        }
        Chronometer chronometer = findViewById(R.id.chronometer);
        // 設置 啓始 時間
        chronometer.setBase(SystemClock.elapsedRealtime());
        // 開始 運行 計時器
        chronometer.start();

        run = true;
    }
    public void onStop(View view){
        if(!run){
            return;
        }
        Chronometer chronometer = findViewById(R.id.chronometer);
        // 停止 計時器 並釋放 計時器資源
        chronometer.stop();
    }
}
```



