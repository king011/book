# 查看依賴
一些 rom 需要依賴其它 rom 可以在下面網站查詢(將 kof97 換成要查詢的 rom zip 名稱即可)

* [https://www.planetemu.net/rom/mame-roms/kof97](https://www.planetemu.net/rom/mame-roms/kof97)

# rom

* [0.263](https://archive.org/details/mame-merged)
* [0.220](https://archive.org/details/MAME220RomsOnlyMerged)
* [0.139](https://archive.org/details/MAME0.139RomCollectionByGhostware)

