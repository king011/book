# GFWList

GFWList 是 github 上的一個 開源(LGPL) 項目 其維護了一個 被 共匪封鎖的 域名列表

* 項目地址 [https://github.com/gfwlist/gfwlist](https://github.com/gfwlist/gfwlist)
* 列表地址 [https://raw.githubusercontent.com/gfwlist/gfwlist/master/gfwlist.txt](https://raw.githubusercontent.com/gfwlist/gfwlist/master/gfwlist.txt)