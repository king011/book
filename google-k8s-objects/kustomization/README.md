# [使用 kustomization 進行聲明式管理](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/)

[kustomization](https://github.com/kubernetes-sigs/kustomize) 是一個獨立的工具 用來通過 kustomization 檔案定製 kubernetes 對象

從 1.14 版本開始 kubectl 開始支持使用 kustomization 檔案來管理 k8s 對象

使用下述指令 查看包含 kustomization 檔案的目錄中的 資源

```
kubectl kustomize <kustomization_directory>
```

要應用這些資源 使用 `--kustomize` 或 `-k` 標誌來執行

```
kubectl apply -k <kustomization_directory>
```

# 生成資源


## configMapGenerator

configMapGenerator 用來生成 configmap

```
cat <<EOF >./application.properties
FOO=Bar
EOF

cat <<EOF >./kustomization.yaml
configMapGenerator:
- name: example-configmap-1
  files:
  - application.properties
EOF
```

```
kubectl kustomize ./
```

```
apiVersion: v1
data:
  application.properties: |
        FOO=Bar
kind: ConfigMap
metadata:
  name: example-configmap-1-8mbdf7882g
```

除了檔案 使用 literals 也可以直接基於 key value 來生成

```
cat <<EOF >./kustomization.yaml
configMapGenerator:
- name: example-configmap-2
  literals:
  - FOO=Bar
EOF
```

```
kubectl kustomize ./
```

```
apiVersion: v1
data:
  FOO: Bar
kind: ConfigMap
metadata:
  name: example-configmap-2-g2hdhfc6tk
```

## secretGenerator

secretGenerator 用於 生成 secret

```
cat <<EOF >./password.txt
username=admin
password=secret
EOF

cat <<EOF >./kustomization.yaml
secretGenerator:
- name: example-secret-1
  files:
  - password.txt
EOF
```

```
kubectl kustomize ./
```

```
apiVersion: v1
data:
  password.txt: dXNlcm5hbWU9YWRtaW4KcGFzc3dvcmQ9c2VjcmV0Cg==
kind: Secret
metadata:
  name: example-secret-1-t2kt65hgtb
type: Opaque
```

同樣使用 literals 可以基於 key value 來生成 secret

```
cat <<EOF >./kustomization.yaml
secretGenerator:
- name: example-secret-2
  literals:
  - username=admin
  - password=secret
EOF
```

```
kubectl kustomize ./
```

```
apiVersion: v1
data:
  password: c2VjcmV0
  username: YWRtaW4=
kind: Secret
metadata:
  name: example-secret-2-t52t6g96d8
type: Opaque
```

## generatorOptions

所有生成的 ConfigMap 和 Secret 都會包含內容哈希後綴。這是爲了確保內容發生變化時，所生成的是新的 ConfigMap 或 Secret。要禁用自動添加後綴的行爲，可以使用 generatorOptions。此外也可以指定貫穿性選項

```
cat <<EOF >./kustomization.yaml
configMapGenerator:
- name: example-configmap-3
  literals:
  - FOO=Bar
generatorOptions:
  disableNameSuffixHash: true
  labels:
    type: generated
  annotations:
    note: generated
EOF
```

```
apiVersion: v1
data:
  FOO: Bar
kind: ConfigMap
metadata:
  annotations:
    note: generated
  labels:
    type: generated
  name: example-configmap-3
```

# 設置貫穿性字段

在項目中爲所有 k8s 對象設置貫穿性字段是一種常見操作：
* 爲所有資源設置相同的名字空間
* 爲所有對象添加相同的前綴或後綴
* 爲對象添加相同的標籤集合
* 爲對象添加相同的註解集合


```
cat <<EOF >./deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
EOF

cat <<EOF >./kustomization.yaml
namespace: my-namespace
namePrefix: dev-
nameSuffix: "-001"
commonLabels:
  app: bingo
commonAnnotations:
  oncallPager: 800-555-1212
resources:
- deployment.yaml
EOF
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    oncallPager: 800-555-1212
  labels:
    app: bingo
  name: dev-nginx-deployment-001
  namespace: my-namespace
spec:
  selector:
    matchLabels:
      app: bingo
  template:
    metadata:
      annotations:
        oncallPager: 800-555-1212
      labels:
        app: bingo
    spec:
      containers:
      - image: nginx
        name: nginx
```

# 組織和定製資源

一種常見的做法是在項目中構建資源集合並將其放到同一個檔案目錄中管理。kustomize 提供基於不同檔案來組織資源並向其應用補丁或其它定製的能力。

## 組織

kustomize 支持組合不同的資源。kustomization.yaml 檔案的 resources 字段定義配置中要包含的資源列表。可以將 resources 列表中的路徑設置爲資源配置檔案的路徑

```
# 創建 deployment.yaml 檔案
cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
EOF

# 創建 service.yaml 檔案
cat <<EOF > service.yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nginx
  labels:
    run: my-nginx
spec:
  ports:
  - port: 80
    protocol: TCP
  selector:
    run: my-nginx
EOF

# 創建 kustomization.yaml 來組織以上兩個資源
cat <<EOF >./kustomization.yaml
resources:
- deployment.yaml
- service.yaml
EOF
```

## 定製
補丁檔案(Patches) 可以用來對資源執行不同的定製。Kustomize 通過 patchesStrategicMerge 和 patchesJson6902  支持不同的補丁機制。patchesStrategicMerge 的內容是一個檔案路徑列表，其中每個檔案都應可解析爲 策略性補丁(Strategic Merge Patch)。補丁檔案中的名稱必須與已經加載的資源名稱匹配。

建議構建規模較小的，僅做一件事情的補丁。例如 構建一個補丁來增加 Deployment 的副本數量，構建另外一個補丁來設置內存限制

```
# 創建 deployment.yaml 檔案
cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
EOF

# 生成一個補丁 increase_replicas.yaml
cat <<EOF > increase_replicas.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  replicas: 3
EOF

# 生成另一個補丁 set_memory.yaml
cat <<EOF > set_memory.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  template:
    spec:
      containers:
      - name: my-nginx
        resources:
          limits:
            memory: 512Mi
EOF

cat <<EOF >./kustomization.yaml
resources:
- deployment.yaml
patchesStrategicMerge:
- increase_replicas.yaml
- set_memory.yaml
EOF
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      run: my-nginx
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - image: nginx
        name: my-nginx
        ports:
        - containerPort: 80
        resources:
          limits:
            memory: 512Mi
```

並非所有資源或字段都支持策略性合併補丁。爲了支持對任何資源的任何字段進行修改，Kustomize 提供通過 patchesJson6902 來應用 JSON補丁 的能力。爲了給JSON補丁找到正確的資源，需要在 kustomization.yaml 檔案中指定資源的 組(group) 版本(version) 類別(king) 名稱(name)。例如爲 Deployment 對象增加副本個數的操作也可以通過 patchesJson6902 來完成

```
# 創建 deployment.yaml
cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
EOF

# 創建一個  JSON 補丁
cat <<EOF > patch.yaml
- op: replace
  path: /spec/replicas
  value: 3
EOF

# 創建 kustomization.yaml
cat <<EOF >./kustomization.yaml
resources:
- deployment.yaml

patchesJson6902:
- target:
    group: apps
    version: v1
    kind: Deployment
    name: my-nginx
  path: patch.yaml
EOF
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      run: my-nginx
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - image: nginx
        name: my-nginx
        ports:
        - containerPort: 80
```

## 更新鏡像名稱

除了補丁外，Kustomize 還提供了定製容器鏡像或者將其它對象字段值注入到容器中的能力，並且不需要創建補丁。例如可以使用 images 字段設置新的鏡像來更改容器中使用的鏡像

```
cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
EOF

cat <<EOF >./kustomization.yaml
resources:
- deployment.yaml
images:
- name: nginx
  newName: my.image.registry/nginx
  newTag: 1.4.0
EOF
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      run: my-nginx
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - image: my.image.registry/nginx:1.4.0
        name: my-nginx
        ports:
        - containerPort: 80
```

## 注入變量

某些時候 Pod 中運行的應用可能需要使用來自其它對象的配置值。例如，某個 Deployment 對象的 Pod 需要從環境變量或命令行參數中讀取 Service 名稱。由於在 kustomization.yaml 檔案中添加 namePrefix 或 nameSuffix  時 Service 名稱可能發生變化，建議不要在命令行中使用硬編碼 Service 名稱。此時可以使用 Kustomize 提供的 vars  將名稱注入到容器中

```
# 創建 deployment.yaml
cat <<EOF > deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        command: ["start", "--host", "$(MY_SERVICE_NAME)"]
EOF

# 創建 service.yaml
cat <<EOF > service.yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nginx
  labels:
    run: my-nginx
spec:
  ports:
  - port: 80
    protocol: TCP
  selector:
    run: my-nginx
EOF

cat <<EOF >./kustomization.yaml
namePrefix: dev-
nameSuffix: "-001"

resources:
- deployment.yaml
- service.yaml

vars:
- name: MY_SERVICE_NAME
  objref:
    kind: Service
    name: my-nginx
    apiVersion: v1
EOF
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dev-my-nginx-001
spec:
  replicas: 2
  selector:
    matchLabels:
      run: my-nginx
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - command:
        - start
        - --host
        - dev-my-nginx-001
        image: nginx
        name: my-nginx
```

# 基準(Bases) 與覆蓋(Overlays)

Kustomize 中有 bases 和 overlays 的概念。

* bases 定義了一組資源及相關的定製
* overlays 將 bases 引用並可針對其中的資源執行組織操作還可以在其上執行定製

```
mkdir base
# 創建 base/deployment.yaml
cat <<EOF > base/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-nginx
spec:
  selector:
    matchLabels:
      run: my-nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: my-nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
EOF

# 創建 base/service.yaml 
cat <<EOF > base/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nginx
  labels:
    run: my-nginx
spec:
  ports:
  - port: 80
    protocol: TCP
  selector:
    run: my-nginx
EOF

# 創建 base/kustomization.yaml
cat <<EOF > base/kustomization.yaml
resources:
- deployment.yaml
- service.yaml
EOF
```

基準可以在多個覆蓋中使用，並在覆蓋中針對不同情況做定製
```
mkdir dev
cat <<EOF > dev/kustomization.yaml
bases:
- ../base
namePrefix: dev-
EOF

mkdir prod
cat <<EOF > prod/kustomization.yaml
bases:
- ../base
namePrefix: prod-
EOF
```

# 使用 Kustomize 

```
# 查看 kustomize 包含資源
kubectl kustomize <kustomization_directory>


# 應用資源
kubectl apply -k <kustomization_directory>

# 查看對象
kubectl get -k <kustomization_directory>
kubectl describe -k <kustomization_directory>

# 比較變化
kubectl diff -k <kustomization_directory>

# 刪除資源
kubectl delete -k <kustomization_directory>
```

# Kustomize 功能特性列表

| 字段 | 型別 | 描述  |
| -------- | -------- | -------- |
| namespace     | string     | 爲所有資源添加名字空間     |
| namePrefix     | string     | 爲所有資源添加名稱前綴     |
| nameSuffix     | string     | 爲所有資源添加名稱後綴     |
| commonLabels     | map[string]string     | 爲所有資源和選擇運算添加標籤     |
| commonAnnotations     | map[string]string     | 爲所有資源添加註解     |
| resources     | []string     | 列表中的每個條目都必須能夠解析爲現有的資源配置檔案     |
| configmapGenerator     | []ConfigMapArgs     | 列表中的每個條目都會生成一個 ConfigMap     |
| secretGenerator     | []SecretArgs	     | 列表中的每個條目都會生成一個 Secret     |
| generatorOptions     | GeneratorOptions	     | 更改所有 configmapGenerator secretGenerator 的行爲     |
| bases     | []string     | 列表中每個條目都應能夠解析爲一個包含 kustomization.yaml 檔案的目錄     |
| patchesStrategicMerge     | []string     | 列表中每個條目都能夠解析爲某 Kubernetes 對象的策略性合併補丁     |
| patchesJson6902     | []Json6902     | 列表中每個條目都能夠解析爲某 Kubernetes 對象 和一個 JSON 補丁     |
| vars     | []Var     | 每個條目用來從某資源的字段來析取文字     |
| images     | []Image     | 每個條目都用來更改鏡像的名稱 標記 或 摘要 不必生成補丁     |
| configurations     | []string     | 列表中每個條目都能解析爲一個包含 Kustomize 轉換器配置 的檔案     |
| crds     | []string     | 列表中每個條目都應能夠解析爲 Kubernetes 類別的 OpenAPI 定義檔案     |

