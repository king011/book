# jellyfin

emby 本來是一個開源且免費的多媒體串流工具但其流行後官方開始收費並對於免費版本不提供硬件加速編解碼，雖然編解碼的工作都是 ffmpeg 和顯卡在做，但 emby 還是將這一功能從免費版本中剔除了，並且將部分源碼設置爲閉源，沒有硬件加速 emby 實際上根本無法使用。在此情況下 jellyfin fork 了一份 emby 進行維護與開發並且完全開源(GPL)免費沒有任何功能閹割

* 官網 [https://jellyfin.org/](https://jellyfin.org/)
* 源碼 [https://github.com/jellyfin/jellyfin](https://github.com/jellyfin/jellyfin)
* 命名規則 [https://jellyfin.org/docs/general/server/media/movies.html](https://jellyfin.org/docs/general/server/media/movies.html)

