# 函數

cmake 允許自定義函數 以供調用

```
function(<name>[arg1 [arg2 [arg3 ...]]])
    COMMAND1(ARGS ...)
    COMMAND2(ARGS ...)
    ...

endfunction(<name>)
```

函數體內的命令直到函數調用時才會執行 此外 函數體內提供了幾個預設變量

* ARGC 傳遞給函數的參數個數
* ARGV0 ARGV1 ARGV2 ARGV3 ... 位置參數值 

```
cmake_minimum_required(VERSION 3.0)
project(example)

function(forecho a1)
    message("參數個數: ${ARGC}")
    message("第一個參數: ${a1}")
    set(i 0)
    while( i LESS ${ARGC})
         list(GET ARGV ${i} v)
         message("argv${i}: ${v}")
         math(EXPR i "${i} + 1")
    endwhile()
endfunction()

forecho(測試 "2 3" ok)
```
