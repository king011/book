# metadata

google.golang.org/grpc/metadata 包 提供了 元數據的 支持

元數據 是和 context 管理的 數據 可以 在一個gprc 請求時 完成 服務器 和 客戶端 間 的數據交互

元數據 的 定義爲
```go
#info=false
type MD map[string][]string
```
metadata 包 提供了 New/Pairs 函數 用來 創建 元數據

```
#info=false
func New(m map[string]string) MD{}
func Pairs(kv ...string) MD{}
```

> Pairs 必須 是 k/v 爲一組的 字符串 也就是 len(kv)%2 == 0
> 

對於 key 有 如下 要求
* 只支持如下 ASCII 字符
   * 0-9
   * A-Z 會被轉爲小寫
   * a-z
   * \- \_ \. 三個字符 
* 不能以 grpc- 開始 這被 grpc 內部保留

# 客戶端向服務器 發送 元數據

```
#info=false
// NewOutgoingContext 客戶端 創建一個 元數據 的 Context
func NewOutgoingContext(ctx context.Context, md MD) context.Context {}

// FromIncomingContext 服務器 從 Context 中 獲取 客戶端 傳來的 元數據
func FromIncomingContext(ctx context.Context) (md MD, ok bool) {}
```

```
#info="client"
func requestPenguin(client grpc_zoo.ServiceClient) {
	// 創建 帶 元數據 的 Context
	md := metadata.Pairs(
		"name", "Penguin",
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	// 發送 請求
	_, e := client.Penguin(ctx, &grpc_zoo.PenguinRequest{})
	if e == nil {
		fmt.Println("Penguin Success")
	} else {
		fmt.Println(e)
	}
}
```

```
#info="server"
// Penguin 基礎 方法
func (s Zoo) Penguin(ctx context.Context, request *grpc_zoo.PenguinRequest) (response *grpc_zoo.PenguinResponse, e error) {
	// 獲取 客戶端 傳來的 元數據
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		strs := md.Get("name")
		if strs != nil && len(strs) != 0 {
			fmt.Println(strs[0])
		}
	}

	// 設置響應
	response = &grpc_zoo.PenguinResponse{}
	return
}
```

# 服務器向客戶端 發送 Header Trailer 元數據

服務器向客戶端 發送 元數據 依據 grpc 四種不同模式 要不同的編碼

## 非流式 響應 rpc

客戶端調用 grpc.Header grpc.Trailer 作爲 grpc.CallOption 接收 元數據
服務器調用 grpc.SetHeader grpc.SetTrailer 在響應時 發送 元數據
```
#info="client"
func requestPenguin(client grpc_zoo.ServiceClient) {
	var header, trailer metadata.MD
	// 發送 請求
	_, e := client.Penguin(context.Background(),
		&grpc_zoo.PenguinRequest{},
		grpc.Header(&header),
		grpc.Trailer(&trailer),
	)
	if e == nil {
		fmt.Println(header)
		fmt.Println(trailer)

		fmt.Println("Penguin Success")
	} else {
		fmt.Println(e)
	}
}
```
```go
#info="server"
// Penguin 基礎 方法
func (s Zoo) Penguin(ctx context.Context, request *grpc_zoo.PenguinRequest) (response *grpc_zoo.PenguinResponse, e error) {
	header := metadata.Pairs("name", "Penguin")
	grpc.SetHeader(ctx, header)
	header = metadata.Pairs("name", "Penguin")
	grpc.SetHeader(ctx, header) // 對相同 key SetHeader 會覆蓋掉 arr[0] = val

	trailer := metadata.Pairs("lv", "10")
	grpc.SetTrailer(ctx, trailer)
	trailer = metadata.Pairs("lv", "11")
	grpc.SetTrailer(ctx, trailer) // 對相同 key SetTrailer 會覆 arr=append(arr,val)
	
	// 設置響應
	response = &grpc_zoo.PenguinResponse{}
	return
}
```
## 流式請求 非流式響應
對於這種特殊情況  使用 grpc.SetHeader grpc.SetTrailer 並且 stream 提供了 stream.SetHeader stream.SetTrailer 的語法糖

## 流式 響應 rpc
服務器調用 Stream.SetHeader Stream.SetTrailer 在第一次響應時 發送 元數據
客戶端調用 Stream.Header Stream.Trailer 接收 元數據

> Header 將阻塞 直到 服務器 SetHeader
> 

```
#info="client"
func requestCat(client grpc_zoo.ServiceClient) {
	stream, e := client.Cat(context.Background(),
		&grpc_zoo.CatRequest{},
	)
	if e != nil {
		fmt.Println(e)
		return
	}
	stream.CloseSend()
	// 讀取 元數據
	fmt.Println(stream.Header())
	fmt.Println(stream.Trailer())
	n := 0
	for {
		n++
		_, e = stream.Recv()
		if e == io.EOF {
			fmt.Println("Cat Success")
			break
		} else if e != nil {
			fmt.Println("Cat <-", n, e)
			return
		}
		fmt.Println("Cat <-", n)
	}
}
```

```
#info="server"
// Cat 響應流
func (s Zoo) Cat(request *grpc_zoo.CatRequest, stream grpc_zoo.Service_CatServer) (e error) {
	// 發送 元 數據
	header := metadata.Pairs("name", "Cat")
	stream.SendHeader(header) // SendHeader 必須 是最先 send 的 數據 且 只有第一次 send 有效
	trailer := metadata.Pairs("lv", "10")
	stream.SetTrailer(trailer)
	trailer = metadata.Pairs("lv", "11")
	stream.SetTrailer(trailer) // 對相同 key SetTrailer 會覆 arr=append(arr,val)

	for i := 0; i < 2; i++ {
		// 響應
		e = stream.Send(&grpc_zoo.CatResponse{})
		if e == nil {
			fmt.Println("Cat -> ", i)
		} else {
			fmt.Println("Cat -> ", i, e)
			return
		}
	}

	return
}
```

# UnaryInterceptor/StreamInterceptor

使用 UnaryInterceptor/StreamInterceptor 為 rpc 服務設置 攔截器所有 請求 路由 前 都會調用 此 函數

在此 驗證 token 並且 設置新的 token 是個 簡單 省事的 好主意

```go
s := grpc.NewServer(
	//為服務 設置 攔截器
	grpc.UnaryInterceptor(
			func(ctx context.Context,
					req interface{},
					info *grpc.UnaryServerInfo,
					handler grpc.UnaryHandler) (resp interface{}, err error) {

					// 解析metada中的信息并验证
					md, ok := metadata.FromIncomingContext(ctx)
					if !ok {
							return nil, grpc.Errorf(codes.Unauthenticated, "unknow token")
					}
					if _, ok := md["appkey"]; !ok {
							return nil, grpc.Errorf(codes.Unauthenticated, "unknow token")
					}

					//設置 新的 token 值
					md = metadata.Pairs("lv", "10")
					ctx = metadata.NewIncomingContext(ctx, md)

					//路由
					return handler(ctx, req)
			},
	),
)
```
> Incoming 包含 傳遞過來的 元數據 而 Outgoing 則是 傳出給遠端的 元數據
>
> UnaryInterceptor 不能攔截 stream 形式的 請求 對於 stream 需要使用 StreamInterceptor 註冊
> 
```go
grpc.StreamInterceptor(
		func(srv interface{},
				ss grpc.ServerStream,
				info *grpc.StreamServerInfo,
				handler grpc.StreamHandler) error {
				fmt.Println(info.FullMethod)
				return handler(srv, ss)
		},
),
```

