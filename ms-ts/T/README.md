# 泛型
ts 提供類 泛型的 支持 使用 **<>** 定義和 調用

```ts

// 定義泛型 類
class Stack<T>{
    private arrs = new Array<T>()
    Push(v: T) {
        this.arrs.push(v)
    }
    Pop(): T | undefined {
        if (this.arrs.length > 0) {
            const v = this.arrs[this.arrs.length - 1]
            this.arrs.splice(this.arrs.length - 1, 1)
            return v
        }
        return undefined
    }
    Empty(): boolean {
        return this.arrs.length == 0
    }
}
// 定義泛型 函數
function Push<T>(stack: Stack<T>, base: T, n: number) {
    for (let i = 0; i < n; i++) {
        stack.Push(base)
    }
}

// 實例化 泛型
const stack = new Stack<string>()
// 調用 泛型 函數
Push(stack, "kate", 5)
Push<string>(stack, "anita", 5)
while (!stack.Empty()) {
    console.log(stack.Pop(), "is a beauty")
}
```

# 泛型約束
可以在定義 泛型時 指定一個 extends interface 這樣 必須實現類 此 interface的 型別 才可以作爲 泛型 調用代碼
```ts
interface IArray {
    length: number
    [index: number]: any
}
function Show<T extends IArray>(arrs: T) {
    for (let i = 0; i < arrs.length; i++) {
        console.log(i + " : " + arrs[i]);
    }
}
const arrs = ["kate", "anna", "anita"]
Show(arrs)
```

# 工廠函數
```ts
// 定義 工廠 函數
function create<T>(c: { new(): T; }): T {
    return new c();
}
class Cat {
    Speak() {
        console.log("喵~")
    }
}
const cat = create(Cat);
cat.Speak()
```

```ts
interface Speak {
    Speak(): void
}
// 定義 工廠 函數
function create<T extends Speak>(c: { new(): T; }): T {
    return new c();
}
class Cat {
    Speak() {
        console.log("喵~")
    }
}
class Dog {
    Speak() {
        console.log("汪~")
    }
}
create(Cat).Speak();
create(Dog).Speak();
```