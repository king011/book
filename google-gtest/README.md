# gtest

gtest 是 google 爲 c++ 提供的 一個 開源(BSD) 單元測試 庫

源碼 [https://github.com/google/googletest/](https://github.com/google/googletest/)

# ubuntu 環境
1. sudo apt install cmake gcc
2. sudo apt-get install libgtest-dev
3. git clone https://github.com/google/googletest.git
4. sudo cmake CMakeLists.txt
5. sudo make
6. sudo make install

```
set -e; \
mkdir build-linux; \
cd build-linux; \
cmake -DCMAKE_BUILD_TYPE=Release  ../; \
make
```

```
#info="cross to windows"
set -e; \
mkdir build-windows; \
cd build-windows; \
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_SYSTEM_NAME=Windows -DCMAKE_C_COMPILER=x86_64-w64-mingw32-gcc-posix -DCMAKE_CXX_COMPILER=x86_64-w64-mingw32-g++-posix  ../; \
make
```