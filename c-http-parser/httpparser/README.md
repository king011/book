# http_parser

`typedef struct http_parser http_parser;` 用來解析http 對於每個tcp需要使用一個 獨立的 http\_parser

```c
struct http_parser {
  /** PRIVATE **/
  unsigned int type : 2;         /* enum http_parser_type */
  unsigned int flags : 8;       /* F_* values from 'flags' enum; semi-public */
  unsigned int state : 7;        /* enum state from http_parser.c */
  unsigned int header_state : 7; /* enum header_state from http_parser.c */
  unsigned int index : 5;        /* index into current matcher */
  unsigned int uses_transfer_encoding : 1; /* Transfer-Encoding header is present */
  unsigned int allow_chunked_length : 1; /* Allow headers with both
                                          * `Content-Length` and
                                          * `Transfer-Encoding: chunked` set */
  unsigned int lenient_http_headers : 1;

  uint32_t nread;          /* # bytes read in various scenarios */
  uint64_t content_length; /* # bytes in body. `(uint64_t) -1` (all bits one)
                            * if no Content-Length header.
                            */

  /** READ-ONLY **/
  unsigned short http_major;
  unsigned short http_minor;
  unsigned int status_code : 16; /* responses only */
  unsigned int method : 8;       /* requests only */
  unsigned int http_errno : 7;

  /* 1 = Upgrade header was present and the parser has exited because of that.
   * 0 = No upgrade header present.
   * Should be checked when http_parser_execute() returns in addition to
   * error checking.
   */
  unsigned int upgrade : 1;

  /** PUBLIC **/
  void *data; /* A pointer to get hook to the "connection" or "socket" object */
};
```

`typedef struct http_parser_settings http_parser_settings;` 用於設定 http解析中的 各種回調

```c
struct http_parser_settings {
  http_cb      on_message_begin; //開始解析消息
  http_data_cb on_url; //開始解析消息(僅用於解析 request)
  http_data_cb on_status; // 接收到 status 字符串
  http_data_cb on_header_field; // field 和 value 會被交替調用 用於解析出 header
  http_data_cb on_header_value;
  http_cb      on_headers_complete; // headers 解析完成後回調
  http_data_cb on_body; // 接收到一個 body
  http_cb      on_message_complete; // 消息接收完成後回調
  /* When on_chunk_header is called, the current chunk length is stored
   * in parser->content_length.
   */
  http_cb      on_chunk_header; // 讀取到 chunk 頭時回調 此時 parser->content_length 記錄了header 長度(注意 要判斷 parser->content_length 是否爲0)
  http_cb      on_chunk_complete; // 每當一個 chunk 被讀完時回調
  // 對於 Content-Length 會回調一次
  // 對於 Transfer-Encoding: chunked 會回調多次 每次傳入 chunk 數據偏移at 和 長度length
};
```
# Example

```c
#include <stdio.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include "http_parser.h"

#define BUFFER_SIZE 1024 * 32

/**
 * \brief send recv 用到的緩衝區
 */
typedef struct
{
    char *data;
    int length;
    int capacity;
} buffer_t;
/**
 * \brief 測試用的 客戶端 包裝了 http_parser 和 socket
 */
typedef struct
{
    http_parser parser;
    http_parser_settings settings;
    const char *host;
    uint16_t port;
    int fd;
    buffer_t *request;
    buffer_t *response;

    int completed;
    const char *body;
    size_t bodyLength;
    int debug;
} client_t;

int on_message_begin(http_parser *parser)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        puts("on_message_begin");
    }
    return 0;
}
int on_status(http_parser *parser, const char *at, size_t length)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        printf("on_status -> %.*s\n", (int)length, at);
    }
    return 0;
}
int on_header_field(http_parser *parser, const char *at, size_t length)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        printf("on_header_field -> %.*s\n", (int)length, at);
    }
    return 0;
}
int on_header_value(http_parser *parser, const char *at, size_t length)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        printf("on_header_value -> %.*s\n", (int)length, at);
    }
    return 0;
}
int on_headers_complete(http_parser *parser)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        puts("on_headers_complete");
    }
    return 0;
}
int on_body(http_parser *parser, const char *at, size_t length)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        printf("on_body -> %.*s\n", (int)length, at);
    }
    client->body = at;
    client->bodyLength = length;
    return 0;
}
int on_message_complete(http_parser *parser)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        puts("on_message_complete");
    }
    client->completed = 1;
    return 0;
}
int on_chunk_header(http_parser *parser)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        printf("on_chunk_header -> %lu\n", parser->content_length);
    }
    return 0;
}
int on_chunk_complete(http_parser *parser)
{
    client_t *client = (client_t *)parser->data;
    if (client->debug)
    {
        puts("on_chunk_complete");
    }
    return 0;
}
/**
 * \brief 初始化客戶端
 */
int client_init(client_t *client, const char *host, uint16_t port)
{
    // 解析域名
    struct hostent *h = gethostbyname(host);
    if (!h)
    {
        puts("gethostbyname error");
        return 1;
    }
    if (!h->h_addr_list[0])
    {
        puts("gethostbyname unknow ip");
        return 1;
    }
    printf("server ip : %s\n", inet_ntoa(*(struct in_addr *)h->h_addr_list[0]));
    if (h->h_addrtype != AF_INET)
    {
        puts("only support ipv4");
        return 1;
    }
    // 創建一個到服務器的連接 以實現http通信
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        puts("create socket error");
        return 1;
    }
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    //addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_addr.s_addr = *(in_addr_t *)h->h_addr_list[0];
    addr.sin_port = htons(port);
    if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        puts("connect error");
        close(fd);
        return 1;
    }
    client->fd = fd;
    client->host = host;
    client->port = port;
    client->debug = 0;

    // 初始化 http 解析器
    http_parser_init(&client->parser, HTTP_RESPONSE); // 值需要解析 響應
    http_parser_settings_init(&client->settings);
    client->parser.data = client;

    // 設置回調
    client->settings.on_message_begin = on_message_begin; //開始解析消息
    client->settings.on_status = on_status;               // status 字符串
    client->settings.on_header_field = on_header_field;   // field 和 value 會被交替調用
    client->settings.on_header_value = on_header_value;
    client->settings.on_headers_complete = on_headers_complete; // headers 解析完成後回調

    // 注意 要判斷 parser->content_length 是否爲0
    client->settings.on_chunk_header = on_chunk_header;     // 讀取到 chunk 頭時回調 此時 parser->content_length 記錄了header 長度
    client->settings.on_chunk_complete = on_chunk_complete; // 每當一個 chunk 被讀完時回調

    // 對於 Content-Length 會回調一次
    // 對於 Transfer-Encoding: chunked 會回調多次 每次傳入 chunk 數據偏移at 和 長度length
    client->settings.on_body = on_body;                         // 接收到一個 body 

    client->settings.on_message_complete = on_message_complete; // 消息接收完成後回調
    return 0;
}
int recv_response(client_t *client)
{
    buffer_t *response = client->response;
    int nparsed;
    // 設置標記 開始 接收響應
    client->completed = 0;
    client->body = NULL;
    client->bodyLength = 0;
    response->length = 0;
    while (!client->completed)
    {
        // 讀取 網路數據
        int offset = response->length;
        int capacity = response->capacity - offset;
        if (capacity < 1)
        {
            puts("recv_response buffer less");
            return 1;
        }
        char *buffer = response->data + offset;
        int recved = read(client->fd, buffer, capacity);
        if (recved == EOF)
        {
            nparsed = http_parser_execute(&client->parser, &client->settings, buffer, 0);
            if (client->completed)
            {
                break;
            }
            else
            {
                puts("connection closed by remote");
                return 1;
            }
        }
        else if (recved < 0)
        {
            puts("net read error");
            return 1;
        }
        nparsed = http_parser_execute(&client->parser, &client->settings, buffer, recved);
        if (nparsed != recved)
        {
            puts("parser error");
            return 1;
        }
        response->length += recved;
    }
    return 0;
}
int test_header(client_t *client, const char *method, const char *path, const char *accept)
{
    printf("***** %s %s [%s] *****\n", method, path, accept);
    buffer_t *request = client->request;
    int n = sprintf(request->data, "%s %s HTTP/1.1\r\n"
                                   "Host: %s\r\n"
                                   "Accept: %s\r\n"
                                   "\r\n",
                    method, path,
                    client->host,
                    accept);

    int sended = write(client->fd, request->data, n);
    if (sended != n)
    {
        printf("%s %s error", method, path);
        return 1;
    }
    if (recv_response(client))
    {
        return 1;
    }
    printf("status_code : %d %s\n", client->parser.status_code, http_status_str(client->parser.status_code));
    if (client->bodyLength && client->body)
    {
        printf("body : \n%.*s\n",
               (int)(client->bodyLength), client->body);
    }
    else
    {
        puts("body : NULL");
    }
    return 0;
}
int test_body(client_t *client, const char *method, const char *path, const char *accept)
{
    printf("***** %s %s *****\n", method, path);
    buffer_t *request = client->request;
    const char *body = "[1,2,3,4,5]";
    int n = sprintf(request->data, "%s %s HTTP/1.1\r\n"
                                   "Host: %s\r\n"
                                   "Accept: %s\r\n"
                                   "Content-Type: application/json\r\n"
                                   "Content-Length: %ld\r\n"
                                   "\r\n"
                                   "%s",
                    method, path,
                    client->host,
                    accept,
                    strlen(body), body);

    int sended = write(client->fd, request->data, n);
    if (sended != n)
    {
        printf("%s %s error", method, path);
        return 1;
    }
    if (recv_response(client))
    {
        return 1;
    }
    printf("status_code : %d %s\n", client->parser.status_code, http_status_str(client->parser.status_code));
    if (client->bodyLength && client->body)
    {
        printf("body : \n%.*s\n",
               (int)(client->bodyLength), client->body);
    }
    else
    {
        puts("body : NULL");
    }
    return 0;
}
int test_all(client_t *client)
{
    if (test_header(client, "GET", "/api/v1/version", "application/json") ||
        test_header(client, "GET", "/api/v1/version", "application/xml") ||
        test_header(client, "GET", "/api/v1/version", "application/x-yaml") ||
        test_header(client, "DELETE", "/api/v1/animal?id=1&name=dog", "application/json") ||
        test_body(client, "POST", "/api/v1/sum", "application/json") ||
        test_body(client, "PUT", "/api/v1/sum", "application/json") ||
        test_body(client, "PATCH", "/api/v1/sum", "application/json"))
    {
        return 1;
    }
    client->debug = 1;
    if (test_header(client, "GET", "/api/v1/version", "application/json"))
    {
        return 1;
    }
    return 0;
}
int main(int argc, char *argv[])
{
    int result = 0;
    client_t client;
    result = client_init(&client, "localhost", 9000);
    if (result)
    {
        return result;
    }
    // 設置 緩衝區
    buffer_t request;
    buffer_t response;
    memset(&request, 0, sizeof(buffer_t));
    memset(&response, 0, sizeof(buffer_t));
    request.capacity = response.capacity = BUFFER_SIZE;
    char requestData[BUFFER_SIZE];
    char responseData[BUFFER_SIZE];
    request.data = requestData;
    response.data = responseData;
    client.request = &request;
    client.response = &response;
    // 執行 測試
    result = test_all(&client);
    close(client.fd);
    if (result)
    {
        return result;
    }
    puts("all success");
    return 0;
}
```