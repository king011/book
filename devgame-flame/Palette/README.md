# [Palette](https://docs.flame-engine.org/1.16.0/flame/rendering/palette.html)

在整個遊戲過程中，您將需要在許多地方使用顏色。 dart:ui 上有兩個類別可以使用，Color 和 Paint。

Color 類別表示十六進位整數格式的 ARGB 顏色。 因此，要建立 Color 實例，您只需將顏色作為 ARGB 格式的整數傳遞即可。

你可以使用 Dart 的十六進位表示法來使其變得非常簡單； 例如：0xFF00FF00 是完全不透明的綠色（「遮罩」將為 0xAARRGGBB）。

**注意**：前兩個十六進位數字用於 Alpha 通道（透明度），與常規（非 A）RGB 不同。 前兩位數字的最大值（FF = 255）表示完全不透明，最小值（00 = 0）表示完全透明。

在Material Flutter套件中有一個Colors類，它提供常用顏色作為常數：

```
import 'package:flutter/material.dart' show Colors;

const black = Colors.black;
```

一些更複雜的方法也可能採用 Paint 對象，這是一個更完整的結構，可讓您配置與描邊、顏色、濾鏡和混合相關的方面。 然而，通常在使用更複雜的 API 時，您只需要一個 Paint 物件的實例來表示一個簡單的純色。

**注意**：我們不建議您每次需要特定 Paint 時都建立一個新的 Paint 對象，因為這可能會導致建立大量不必要的物件。 更好的方法是在某處定義Paint 物件並重新使用它（但是，請注意Paint 類別是可變的，與Color 不同），或使用Palette 類別來定義您想要在您的遊戲應用程式中使用的所有顏色。

您可以像這樣建立這樣的物件：

```
Paint green = Paint()..color = const Color(0xFF00FF00);
```

為了幫助您完成此任務並保持遊戲的調色板一致，Flame 添加了 Palette 類。 您可以使用它在需要時輕鬆存取顏色和paint，還可以將遊戲使用的顏色定義為常數，這樣您就不會混淆它們。

BasicPalette 類別是調色板的範例，並添加黑色和白色作為顏色。 因此，要使用黑色或白色，您可以直接從 BasicPalette 存取它們； 例如，使用顏色：

```
TextConfig regular = TextConfig(color: BasicPalette.white.color);
```

Or using paint:

```
canvas.drawRect(rect, BasicPalette.black.paint);
```

然而，我們的想法是，您可以按照 BasicPalette 範例創建自己的調色板，並添加遊戲的調色板/方案。 然後您將能夠靜態存取元件和類別中的任何顏色。 以下是來自範例遊戲 BGUG 的 Palette 實作範例：

```
import 'dart:ui';

import 'package:flame/palette.dart';

class Palette {
  static PaletteEntry white = BasicPalette.white;

  static PaletteEntry toastBackground = PaletteEntry(Color(0xFFAC3232));
  static PaletteEntry toastText = PaletteEntry(Color(0xFFDA9A00));

  static PaletteEntry grey = PaletteEntry(Color(0xFF404040));
  static PaletteEntry green = PaletteEntry(Color(0xFF54a286));
}
```

PaletteEntry 是一個保存顏色資訊的 const 類，它具有以下成員：

* **color**: 傳回指定的顏色
* **paint**: 使用指定的顏色建立一個新的 Paint。 Paint 是一個非 const 類，因此該方法每次呼叫時實際上都會建立一個全新的實例。 對此進行鏈式調用是安全的。