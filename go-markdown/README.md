# markdown

```
go get github.com/gomarkdown/markdown
```
github.com/gomarkdown/markdown 是一個非常快速的開源 golang 庫，用於解析 markdown 文檔並將其轉化爲 html 文檔

* 源碼 [https://github.com/gomarkdown/markdown](https://github.com/gomarkdown/markdown)