# 格式

mp3 檔案通常由三部分組成

1. 可選的 ID3V2
2. 數據幀
3. 可選的 ID3V1

# ID3V2

ID3V2 一共有四個版本

1. ID3V2.1
2. ID3V2.2
3. ID3V2.3
4. ID3V2.4

目前流行的軟體一般只支持了 ID3V2.3，ID3V2將數據記錄在檔案最開始且比 ID3V1 複雜，故如果修改會比 ID3V1 慢很多，所以同樣的屬性應該首選修改 ID3V1

## ID3V2.3

ID3V2.3 是由一個標籤頭與若干個標籤幀組成的，至少要由一個標籤幀，每個標籤幀記錄一種信息

### 標籤頭

標籤是固定的 10 個字節


| 偏移 | 長度 | 說明 |
| -------- | -------- | -------- |
| 0     | 3     | 固定爲 **ID3** 字符標記      |
| 3     | 1     | 版號 ID3V2.3 此值爲 3     |
| 4     | 1     | 副版本號 ID3V2.3 此值爲 0     |
| 5     | 1     | 標誌字節，只使用最高 3bit，其它 bit 爲 0     |
| 6     | 4     | 標籤大小     |

標誌字節最有最高 3 bit 有效

1. **flag&0x80 != 0** 爲 true 表示使用 Unsynchronization
2. **flag&0x40 != 0** 爲 true 表示使用含有擴展頭部
3. **flag&0x20 != 0** 爲 true 表示爲測試標籤


標籤大小每個字節只使用低 7bit，最高 bit 不使用固定爲 0，所以對於 golang 可以這樣計算標籤大小:
```
size := (uint32(buf[0]&0x7F) << 21) | (uint32(buf[1]&0x7F) << 14) |
			(uint32(buf[2]&0x7F) << 7) | uint32(buf[3]&0x7F)
```

# 數據幀

數據幀由如下結構組成

| 組成 | 長度(字節) | 存在 |
| -------- | -------- | -------- |
| 幀頭     | 4     | 必須存在     |
| CRC     | 2     | 可能存在     |
| Side Info     | 9 or 17 or 32     | 必須存在     |
| 聲音數據     | N     | 必須存在     |

Side Info 依據版本和聲道數不同而差異



|   | MPEG 1 | MPEG 2/2.5 |
| -------- | -------- | -------- |
| 雙聲道     | 32     | 17     |
| 單聲道     | 17     | 9     |


## 幀頭

幀頭固定暫用 32bit 可以分爲三個部分

1. 前兩字節使用 大端序存儲
2. 中間1字節
3. 最後1字節 

### 前兩字節

1. 11 bit 作爲同步值固定全爲 1，用於在錯誤的數據中找到幀頭
2. 2 bit 記錄版本
	* 00 MPEG 2.5
	* 01 未定義
	* 10 MPEG 2
	* 11 MPEG 1
3. 2 bit 記錄層
	* 00 未定義
	* 01 Layer 3
	* 10 Layer 2
	* 11 Layer 1
4. 最高 bit 如果爲 0 則帶 有 crc效驗

### 中間字節

中間的字節用於確定位率等信息

![](assets/bitrate.png)

1. 高 4 bits 是比特率索引，依據 Version Layer 的不同而不同
	
	```
	var bitrates = [2][3][16]int{
		{
			// MPEG 1 Layer 3
			{0, 32000, 40000, 48000, 56000, 64000, 80000, 96000,
				112000, 128000, 160000, 192000, 224000, 256000, 320000},
			// MPEG 1 Layer 2
			{0, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
				128000, 160000, 192000, 224000, 256000, 320000, 384000},
			// MPEG 1 Layer 1
			{0, 32000, 64000, 96000, 128000, 160000, 192000, 224000,
				256000, 288000, 320000, 352000, 384000, 416000, 448000},
		},
		{
			// MPEG2 2 Layer 3
			{0, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
				64000, 80000, 96000, 112000, 128000, 144000, 160000},
			// MPEG 2 Layer 2
			{0, 8000, 16000, 24000, 32000, 40000, 48000, 56000,
				64000, 80000, 96000, 112000, 128000, 144000, 160000},
			// MPEG 2 Layer 1
			{0, 32000, 48000, 56000, 64000, 80000, 96000, 112000,
				128000, 144000, 160000, 176000, 192000, 224000, 256000},
		},
	}
	```
	
	0xFFFF 值不被允許，0 代表比特率可變

2. 2 bit 代表採樣率

	| Version | 00 | 01 | 10 | 11 |
	| -------- | -------- | -------- | -------- |
	| MPEG 1     | 44.1kHz     | 48kHz     | 32kHz     | 未定義     |
	| MPEG 2     | 22.05kHz     | 24kHz     | 16kHz     | 未定義     |
	| MPEG 2.5     | 11.025kHz     | 12kHz     | 8kHz     | 未定義     |

3. 1 bit 爲 1 代表有填充數據
4. 1 bit 保留未使用

### 最後一字節

1. 高 2 bit 表示聲道

	* 00 Stereo
	* 01 Joint Stereo
	* 10 Dual Channel
	* 11 Single Channel

	除了 Single Channel 都會有兩個聲道

2. 2 bit 表示擴展模式 只有在 Joint Stereo 時此值才有效

	| value | 強度立體聲 | MS立體聲 |
	| -------- | -------- | -------- |
	| 00     | off     | off     |
	| 01     | on     | off     |
	| 10     | off     | on     |
	| 11     | on     | on     |

3. 1 bit 表示版權，爲 1 時合法否則非法
4. 1 bit 爲 1 是表示原版否則爲非原版
5. 2 bit 用於聲音降噪壓縮後再補償的分類

### 幀大小與幀長度

幀大小既每幀的採樣數，表示一幀數據中的採樣個數，此值是恆定的


|  | MPEG 1 | MPEG2 | MPEG2.5 |
| -------- | -------- | -------- | -------- |
| Layer 1     | 384     | 384     | 384     |
| Layer 2     | 1152     | 1152     | 1152     |
| Layer 3     | 1152     | 576     | 576     |

幀長度是壓縮時每一幀數據的字節大小包括了幀頭 和 CRC，也將填充的空位計算在內。Layer 1的一個空位長 4 字節，Layer 2 和 Layer 3的空位是 1 字節。當讀取 MPEG 檔案時必須計算此值以找到相鄰的幀。因爲有填充和比特率的變化，所以幀長度可能變化

計算公式如下

1. Layer 1 = (每幀採樣數/8\*比特率)/採樣率+填充\*4
2. Layer 3 = (每幀採樣數/8\*比特率)/採樣率+填充

MPEG 1 Layer3 比特率爲 12800 採樣率 44100 填充爲 0 則幀長度爲 \(\(1152/8\*128K\)/44\.1K\+0=417

### 幀持續時間

每幀持續時間(毫秒)=每幀採樣數 / 採樣率 \*1000

例如 1152/44100\*1000=26ms

## 幀數據

在幀頭之後是 Side Info。此後就是編碼數據了。

對於 mp3 來說現在有兩種編碼方式

1. CBR 固定採樣率，CBR 幀的大小是固定的，故依據檔案大小即可計算出時長與定位
2. VBR 比特率是變化的，其第一幀數據不存儲音頻數據而是，編碼信息

### VBR

Side Info 之後的 VBR 格式如下
| 偏移 | 字節大小 | 說明 |
| -------- | -------- | -------- |
| 0    | 4     |  VBR 標記，通常是 **Xing** 或 **Info**，一些 CBR 也會模仿 VBR 用第一幀來存儲指導數據，例如 lame 會這樣做並使用 Info 作爲標記     |
| 4     | 4     | 標記位 0x1  frames 有效，0x2 bytes 有效 ，0x4 toc 有效，0x8 quality 有效     |
| 8     | 4     | 大端存儲了 總幀數     |
| 8/12     | 4     | 大端存儲了 檔案大小     |
| 8 /12/16     | 100     | 100 個定位表用於播放器爲 vbr 調整播放進度    |
| 8/12/16/108/112/116 | 4 | 質量指示 |   


* 第 4 到 8 字節的標記位標記了後續哪些字段有效，字段只有在有效時才會佔用空間否則響應字段大小爲0字節
* frames 是未包含的 首幀 (VBR 幀)


# ID3V1

v1 標準並不周全，存放的信息少無法存入歌詞，專輯封面等。但是因爲 v2 標準會給軟體編寫帶來困難所以目前絕大多數 mp3 仍然使用 ID3V1 標準。

ID3V1標準是固定的 128 個字節，存儲在 mp3 檔案的末尾


| 偏移 | 長度 | 說明 |
| -------- | -------- | -------- |
| 0     | 3     | 固定的 **TAG** 字符標記     |
| 3     | 30     | 歌名     |
| 33     | 30     | 藝人     |
| 63     | 30     | 專輯     |
| 93     | 4     | 年份     |
| 97     | 30     | 附注     |
| 128     | 1     | 音樂類型     |


1. 本喵在測試時發現 歌名 藝人 專輯 附註，如果使用中文 vlc 會將其存儲到 ID3V2 使用英文則存儲到 ID3V1，所以猜測這些數據在 ID3V1 可能只能存儲 anscii 碼
2. 當同時存在 IDV1 與 IDV2 時，且包含了重複的屬性時 vlc 會以 ID3V2 的數據爲準



|音樂類型|含義|
|---|---|
| 0 | Blues |
| 1 | ClassicRock |
| 2 | Country |
| 3 | Dance |
| 4 | Disco |
| 5 | Funk |
| 6 | Grunge |
| 7 | Hip-Hop |
| 8 | Jazz |
| 9 | Metal |
| 10 | NewAge |
| 11 | Oldies |
| 12 | Other |
| 13 | Pop |
| 14 | R&B |
| 15 | Rap |
| 16 | Reggae |
| 17 | Rock |
| 18 | Techno |
| 19 | Industrial |
| 20 | Alternative |
| 21 | Ska |
| 22 | Deathl |
| 23 | Pranks |
| 24 | Soundtrack |
| 25 | Euro-Techno |
| 26 | Ambient |
| 27 | Trip-Hop |
| 28 | Vocal |
| 29 | Jazz+Funk |
| 30 | Fusion |
| 31 | Trance |
| 32 | Classical |
| 33 | Instrumental |
| 34 | Acid |
| 35 | House |
| 36 | Game |
| 37 | SoundClip |
| 38 | Gospel |
| 39 | Noise |
| 40 | AlternRock |
| 41 | Bass |
| 42 | Soul |
| 43 | Punk |
| 44 | Space |
| 45 | Meditative |
| 46 | InstrumentalPop |
| 47 | InstrumentalRock |
| 48 | Ethnic |
| 49 | Gothic |
| 50 | Darkwave |
| 51 | Techno-Industrial |
| 52 | Electronic |
| 53 | Pop-Folk |
| 54 | Eurodance |
| 55 | Dream |
| 56 | SouthernRock |
| 57 | Comedy |
| 58 | Cult |
| 59 | Gangsta |
| 60 | Top40 |
| 61 | ChristianRap |
| 62 | Pop/Funk |
| 63 | Jungle |
| 64 | NativeAmerican |
| 65 | Cabaret |
| 66 | NewWave |
| 67 | Psychadelic |
| 68 | Rave |
| 69 | Showtunes |
| 70 | Trailer |
| 71 | Lo-Fi |
| 72 | Tribal |
| 73 | AcidPunk |
| 74 | AcidJazz |
| 75 | Polka |
| 76 | Retro |
| 77 | Musical |
| 78 | Rock&Roll |
| 79 | HardRock |
| 80 | Folk |
| 81 | Folk-Rock |
| 82 | NationalFolk |
| 83 | Swing |
| 84 | FastFusion |
| 85 | Bebob |
| 86 | Latin |
| 87 | Revival |
| 88 | Celtic |
| 89 | Bluegrass |
| 90 | Avantgarde |
| 91 | GothicRock |
| 92 | ProgessiveRock |
| 93 | PsychedelicRock |
| 94 | SymphonicRock |
| 95 | SlowRock |
| 96 | BigBand |
| 97 | Chorus |
| 98 | EasyListening |
| 99 | Acoustic |
| 100 | Humour |
| 101 | Speech |
| 102 | Chanson |
| 103 | Opera |
| 104 | ChamberMusic |
| 105 | Sonata |
| 106 | Symphony |
| 107 | BootyBass |
| 108 | Primus |
| 109 | PornGroove |
| 110 | Satire |
| 111 | SlowJam |
| 112 | Club |
| 113 | Tango |
| 114 | Samba |
| 115 | Folklore |
| 116 | Ballad |
| 117 | PowerBallad |
| 118 | RhythmicSoul |
| 119 | Freestyle |
| 120 | Duet |
| 121 | PunkRock |
| 122 | DrumSolo |
| 123 | Acapella |
| 124 | Euro-House |
| 125 | DanceHall |
| 126 | Goa |
| 127 | Drum&Bass |
| 128 | Club-House |
| 129 | Hardcore |
| 130 | Terror |
| 131 | Indie |
| 132 | BritPop |
| 133 | Negerpunk |
| 134 | PolskPunk |
| 135 | Beat |
| 136 | ChristianGangstaRap |
| 137 | Heavyl |
| 138 | Blackl |
| 139 | Crossover |
| 140 | ContemporaryChristian |
| 141 | ChristianRock |
| 142 | Merengue |
| 143 | Salsa |
| 144 | Trashl |
| 145 | Anime |
| 146 | JPop |
| 147 | Synthpop |