# 模塊

lua 使用 require 加載模塊，要導出一個模塊返回一個 table 即可

```
local function echo(...)
    print(...)
end
return {
    Version = "v1.0.0",
    echo = echo
}
```

注意在模塊檔案中定義的 函數和變量要使用 local 關鍵字，否則 lua 會將其定義到全局變量

# require

require 會從 **package.path** 的值中指定的位置加載模塊，配置環境變量 **LUA\_PATH** 可以設定 package.path 的值

```
require "a.b" 
```

