# flutter_tts

[flutter_tts](https://pub.dev/packages/flutter_tts) 提供了一個 tts 單件用於將文字以語音讀出來

```
flutter_tts: ^3.7.0
```

對於 android 需要編輯 **android/app/src/main/AndroidManifest.xml** 添加上 tts 權限聲明

```
<manifest xmlns:android="http://schemas.android.com/apk/res/android">
    <queries>  
        <intent>  
            <action android:name="android.intent.action.TTS_SERVICE" />  
        </intent>  
    </queries>
</manifest>
```

# example

flutter_tts 只是將api調用傳遞給後端原生代碼，並沒有出來併發請求以及記錄狀態這可能會引發問題，可以先自己包裝個新的單件

```
#info="single_tts.dart"
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

enum TtsState { playing, stopped, paused }

class SingleTts {
  static SingleTts? _instance;
  final _listeners = <ValueChanged<TtsState>>[];
  SingleTts._();
  factory SingleTts() => SingleTts._instance ??= SingleTts._();

  /// 監聽 tts 狀態
  void addListener(ValueChanged<TtsState> listener) {
    _listeners.add(listener);
  }

  /// 移除狀態監聽器
  bool removeListener(ValueChanged<TtsState> listener) {
    return _listeners.remove(listener);
  }

  _notify() {
    final state = _state;
    for (var l in _listeners) {
      l(state);
    }
  }

  /// 後端 flutter_tts
  FlutterTts? _backend;

  /// 後端狀態
  var _state = TtsState.stopped;

  /// api 調用鎖，保證同時只有一個 api調用被發送到後端以避免未知bug
  Completer<void>? _mutex;

  bool _closed = false;

  /// 關閉並釋放所有資源
  Future<bool> close() async {
    if (_closed) {
      return false;
    }
    _closed = true;
    if (_backend != null) {
      var mutex = _mutex;
      while (mutex != null) {
        await mutex.future;
        mutex = _mutex;
      }
      try {
        if (!isStopped) {
          await _backend!.speak('');
        }
      } catch (_) {}
    }
    return true;
  }

  /// 如果處於播放狀態，返回 true
  bool get isPlaying => _state == TtsState.playing;

  /// 如果處於關閉狀態，返回 true
  bool get isStopped => _state == TtsState.stopped;

  /// 如果處於播放暫停狀態，返回 true
  bool get isPaused => _state == TtsState.paused;

  bool get isClosed => _closed;

  FlutterTts get backend {
    if (_backend != null) {
      return _backend!;
    }
    _backend = FlutterTts();
    return _backend!
      ..setStartHandler(() {
        _state = TtsState.playing;
        _notify();
      })
      ..setCompletionHandler(() {
        _state = TtsState.stopped;
        _notify();
      })
      ..setErrorHandler((msg) {
        debugPrint("tts error: $msg");
        _state = TtsState.stopped;
        _notify();
      })
      ..setCancelHandler(() {
        _state = TtsState.stopped;
        _notify();
      })
      ..setPauseHandler(() {
        _state = TtsState.paused;
        _notify();
      })
      ..setContinueHandler(() {
        _state = TtsState.playing;
        _notify();
      })
      ..setProgressHandler(
          (String text, int startOffset, int endOffset, String word) {
        _offset = startOffset;
      });
  }

  /// 存儲當前文本以便從暫停中恢復
  String _text = '';

  /// 存儲已讀文本以便從暫停中恢復
  int _offset = 0;

  /// 將 text 轉爲語音並播放
  Future<bool> speak(String text) async {
    var mutex = _mutex;
    if (_closed || mutex != null || _state != TtsState.stopped) {
      return false;
    }

    mutex = Completer();
    _mutex = mutex;

    try {
      await backend.speak(text);

      _text = text;
      _offset = 0;
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  /// 如果不處於停止狀態，則停止語音播放
  Future<bool> stop() async {
    var mutex = _mutex;
    if (_closed || mutex != null || _state == TtsState.stopped) {
      return false;
    }

    mutex = Completer();
    _mutex = mutex;

    try {
      await backend.speak('');
      _text = '';
      _offset = 0;
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  /// 如果正在播放則暫停
  Future<bool> pause() async {
    var mutex = _mutex;
    if (_closed || mutex != null || _state != TtsState.playing) {
      return false;
    }

    mutex = Completer();
    _mutex = mutex;
    try {
      await backend.pause();
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  /// 如果暫停播放則恢復
  Future<bool> resume() async {
    var mutex = _mutex;
    if (_closed || mutex != null || _state != TtsState.paused) {
      return false;
    }
    mutex = Completer();
    _mutex = mutex;
    try {
      final text = _offset > 0 ? _text.substring(_offset) : _text;
      await backend.speak(text);
      _text = text;
      _offset = 0;
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  Future<List<String>> getEngines() async {
    var mutex = _mutex;
    if (_closed || mutex != null) {
      return [];
    }
    mutex = Completer();
    _mutex = mutex;
    try {
      List<dynamic> vals = await backend.getEngines;
      return vals.map((e) => e.toString()).toList();
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  Future<String>? _engine;
  Future<bool> setEngine(String val) async {
    var mutex = _mutex;
    if (_closed || mutex != null) {
      return false;
    }
    mutex = Completer();
    _mutex = mutex;
    try {
      await backend.setEngine(val);
      _engine = Future.value(val);
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  Future<String> getEngine() async {
    if (_engine != null) {
      return _engine!;
    }
    var c = Completer<String>();
    _engine = c.future;
    try {
      final v = await backend.getDefaultEngine;
      c.complete(v);
    } catch (e) {
      if (_engine == c.future) {
        _engine = null;
        c.completeError(e);
      }
    }
    return c.future;
  }

  double _volume = 1.0;
  Future<bool> setVolume(double volume) async {
    var mutex = _mutex;
    if (_closed || mutex != null) {
      return false;
    }
    mutex = Completer();
    _mutex = mutex;
    try {
      await backend.setVolume(volume);
      _volume = volume;
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  double getVolume() {
    return _volume;
  }

  double _pitch = 1.0;
  Future<bool> setPitch(double pitch) async {
    var mutex = _mutex;
    if (_closed || mutex != null) {
      return false;
    }
    mutex = Completer();
    _mutex = mutex;
    try {
      await backend.setPitch(pitch);
      _pitch = pitch;
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  double getPitch() {
    return _pitch;
  }

  double _rate = 0.5;
  Future<bool> setRate(double rate) async {
    var mutex = _mutex;
    if (_closed || mutex != null) {
      return false;
    }
    mutex = Completer();
    _mutex = mutex;
    try {
      await backend.setSpeechRate(rate);
      _rate = rate;
      return true;
    } catch (e) {
      rethrow;
    } finally {
      _mutex = null;
      mutex.complete();
    }
  }

  double getRate() {
    return _rate;
  }
}
```

下面是使用示例

```
import 'dart:async';

import 'package:flutter/material.dart';
import '/pages/dev/single_tts.dart';
import '/pages/dialog.dart';

/// 自動換行的多行輸入框
class MyTextField extends StatefulWidget {
  const MyTextField({
    Key? key,
    required this.controller,
    this.enabled,
    this.decoration,
  }) : super(key: key);
  final TextEditingController controller;
  final bool? enabled;
  final InputDecoration? decoration;
  @override
  // ignore: library_private_types_in_public_api
  _MyTextFieldState createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  var _maxLines = 2;
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_listener);
    _listener();
  }

  @override
  void dispose() {
    widget.controller.removeListener(_listener);
    super.dispose();
  }

  _listener() {
    final controller = widget.controller;
    var n = controller.text.split('\n').length + 1;
    if (n < 2) {
      n = 2;
    }
    if (n != _maxLines) {
      setState(() {
        _maxLines = n;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: widget.enabled,
      decoration: widget.decoration,
      maxLines: _maxLines,
      controller: widget.controller,
    );
  }
}

/// 選擇框
class MySelectPage extends StatefulWidget {
  const MySelectPage({
    Key? key,
    this.value,
  }) : super(key: key);
  final dynamic value;
  @override
  // ignore: library_private_types_in_public_api
  _MySelectPageState createState() => _MySelectPageState();
}

class _MySelectPageState extends State<MySelectPage> {
  var list = <String>[];
  final tts = SingleTts();
  @override
  void initState() {
    super.initState();

    tts.getEngines().then((value) {
      if (!_closed) {
        setState(() {
          list = value;
        });
      }
    });
  }

  var _closed = false;
  @override
  void dispose() {
    super.dispose();
    _closed = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("選擇引擎"),
      ),
      body: ListView(
        children: list.map((item) {
          return ListTile(
            leading: item == widget.value
                ? const Icon(Icons.check_box)
                : const Icon(Icons.check_box_outline_blank_sharp),
            title: Text(item),
            onTap: () {
              Navigator.of(context).pop(item);
            },
          );
        }).toList(),
      ),
    );
  }
}

class MyTTSPage extends StatefulWidget {
  const MyTTSPage({
    Key? key,
  }) : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _MyTTSPagePageState createState() => _MyTTSPagePageState();
}

class _MyTTSPagePageState extends State<MyTTSPage> {
  final _text = TextEditingController();
  final tts = SingleTts();
  String _engine = '';
  bool _disabled = false;
  double volume = SingleTts().getVolume();
  double pitch = SingleTts().getPitch();
  double rate = SingleTts().getRate();
  @override
  void initState() {
    super.initState();
    _init();
  }

  _init() async {
    try {
      final engine = await tts.getEngine();
      if (_closed) {
        return;
      }
      setState(() {
        _engine = engine;
      });
      tts.addListener(_listener);
    } catch (e) {
      if (!_closed) {
        showErrorMessageDialog(context, e);
      }
    }
  }

  var _closed = false;
  @override
  void dispose() {
    _closed = true;
    tts.removeListener(_listener);
    super.dispose();
  }

  _listener(v) {
    debugPrint("tts: $v");
    setState(() {});
  }

  _call<T>(Future<T> Function() f) async {
    setState(() {
      _disabled = true;
    });
    try {
      await f();
    } catch (e) {
      if (!_closed) {
        showErrorMessageDialog(context, e);
      }
    } finally {
      if (!_closed) {
        setState(() {
          _disabled = false;
        });
      }
    }
  }

  _speak() => _call(() => tts.speak(_text.text));
  _stop() => _call(() => tts.stop());
  _pause() => _call(() => tts.pause());
  _resume() => _call(() => tts.resume());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("tts"),
      ),
      body: Container(
        padding: const EdgeInsets.all(8),
        child: Expanded(
          child: ListView(
            children: <Widget>[
              Wrap(
                alignment: WrapAlignment.center,
                children: [
                  TextButton(
                    onPressed: _disabled || !tts.isStopped ? null : _speak,
                    child: const Text("開始"),
                  ),
                  TextButton(
                    onPressed: _disabled || tts.isStopped ? null : _stop,
                    child: const Text("停止"),
                  ),
                  TextButton(
                    onPressed: _disabled || !tts.isPlaying ? null : _pause,
                    child: const Text("暫停"),
                  ),
                  TextButton(
                    onPressed: _disabled || !tts.isPaused ? null : _resume,
                    child: const Text("繼續"),
                  ),
                ],
              ),
              MyTextField(
                enabled: !_disabled && tts.isStopped,
                decoration: const InputDecoration(
                  hintText: '輸入文本',
                  labelText: '文本轉語音',
                ),
                controller: _text,
              ),
              ListTile(
                title: const Text("引擎"),
                subtitle: Text(_engine),
                onTap: () async {
                  try {
                    final val =
                        await Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MySelectPage(
                                  value: _engine,
                                )));
                    if (val != null && val != _engine) {
                      await tts.setEngine(val);
                      setState(() {
                        _engine = val;
                      });
                    }
                  } catch (e) {
                    showErrorMessageDialog(context, e);
                  }
                },
              ),
              Row(
                children: [
                  const Text("音量"),
                  Expanded(
                    child: Slider(
                      label: "$volume",
                      min: 0.0,
                      max: 1.0,
                      divisions: 10,
                      value: volume,
                      onChanged: (val) {
                        setState(() {
                          volume = val;
                        });
                      },
                      onChangeEnd: (val) {
                        tts.setVolume(val);
                      },
                    ),
                  ),
                  Text("$volume"),
                ],
              ),
              Row(
                children: [
                  const Text("音調:"),
                  Expanded(
                    child: Slider(
                      label: "$pitch",
                      min: 0.5,
                      max: 2.0,
                      divisions: 15,
                      value: pitch,
                      onChanged: (val) {
                        setState(() {
                          pitch = val;
                        });
                      },
                      onChangeEnd: (val) {
                        tts.setPitch(val);
                      },
                    ),
                  ),
                  Text("$pitch"),
                ],
              ),
              Row(
                children: [
                  const Text("語速:"),
                  Expanded(
                    child: Slider(
                      label: "$rate",
                      min: 0.0,
                      max: 1.0,
                      divisions: 10,
                      value: rate,
                      onChanged: (val) {
                        setState(() {
                          rate = val;
                        });
                      },
                      onChangeEnd: (val) {
                        tts.setRate(rate);
                      },
                    ),
                  ),
                  Text("$rate"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
```