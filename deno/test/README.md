# [測試](https://deno.land/manual@v1.28.1/basics/testing)

deno 內置的測試環境

# 快速開始

創建一個以 **\_test.ts** 結尾的檔案，並在裏面使用 **Deno.test** 註冊單元測試

```
// url_test.ts
import { assertEquals } from "https://deno.land/std@0.165.0/testing/asserts.ts";

Deno.test("url test", () => {
  const url = new URL("./foo.js", "https://deno.land/");
  assertEquals(url.href, "https://deno.land/foo.js");
});
```

運行測試
```
$ deno test url_test.ts
running 1 test from file:///dev/url_test.js
test url test ... ok (2ms)

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (9ms)
```

# 書寫測試代碼

使用 Deno.test 來註冊你的測試代碼，這個 api 重載了多種形式，以允許最大的靈活性來註冊測試代碼

```
import { assertEquals } from "https://deno.land/std@0.165.0/testing/asserts.ts";

// Compact form: name and function
Deno.test("hello world #1", () => {
  const x = 1 + 2;
  assertEquals(x, 3);
});

// Compact form: named function.
Deno.test(function helloWorld3() {
  const x = 1 + 2;
  assertEquals(x, 3);
});

// Longer form: test definition.
Deno.test({
  name: "hello world #2",
  fn: () => {
    const x = 1 + 2;
    assertEquals(x, 3);
  },
});

// Similar to compact form, with additional configuration as a second argument.
Deno.test("hello world #4", { permissions: { read: true } }, () => {
  const x = 1 + 2;
  assertEquals(x, 3);
});

// Similar to longer form, with test function as a second argument.
Deno.test(
  { name: "hello world #5", permissions: { read: true } },
  () => {
    const x = 1 + 2;
    assertEquals(x, 3);
  },
);

// Similar to longer form, with a named test function as a second argument.
Deno.test({ permissions: { read: true } }, function helloWorld6() {
  const x = 1 + 2;
  assertEquals(x, 3);
});
```

# 異步測試

Deno.test 也可以接收異步函數來測試異步代碼

```
import { delay } from "https://deno.land/std@0.165.0/async/delay.ts";

Deno.test("async hello world", async () => {
  const x = 1 + 2;

  // await some async task
  await delay(100);

  if (x !== 3) {
    throw Error("x should be equal to 3");
  }
});
```

# Test steps

step 提供了一種方法來報告測試中的不同步驟在該測試中執行代碼的測試情況

```
import { assertEquals } from "https://deno.land/std@0.165.0/testing/asserts.ts";
import { Client } from "https://deno.land/x/postgres@v0.15.0/mod.ts";

interface User {
  id: number;
  name: string;
}

interface Book {
  id: number;
  title: string;
}

Deno.test("database", async (t) => {
  const client = new Client({
    user: "user",
    database: "test",
    hostname: "localhost",
    port: 5432,
  });
  await client.connect();

  // provide a step name and function
  await t.step("insert user", async () => {
    const users = await client.queryObject<User>(
      "INSERT INTO users (name) VALUES ('Deno') RETURNING *",
    );
    assertEquals(users.rows.length, 1);
    assertEquals(users.rows[0].name, "Deno");
  });

  // or provide a test definition
  await t.step({
    name: "insert book",
    fn: async () => {
      const books = await client.queryObject<Book>(
        "INSERT INTO books (name) VALUES ('The Deno Manual') RETURNING *",
      );
      assertEquals(books.rows.length, 1);
      assertEquals(books.rows[0].title, "The Deno Manual");
    },
    ignore: false,
    // these default to the parent test or step's value
    sanitizeOps: true,
    sanitizeResources: true,
    sanitizeExit: true,
  });

  // nested steps are also supported
  await t.step("update and delete", async (t) => {
    await t.step("update", () => {
      // even though this test throws, the outer promise does not reject
      // and the next test step will run
      throw new Error("Fail.");
    });

    await t.step("delete", () => {
      // ...etc...
    });
  });

  // steps return a value saying if they ran or not
  const testRan = await t.step({
    name: "copy books",
    fn: () => {
      // ...etc...
    },
    ignore: true, // was ignored, so will return `false`
  });

  // steps can be run concurrently if sanitizers are disabled on sibling steps
  const testCases = [1, 2, 3];
  await Promise.all(testCases.map((testCase) =>
    t.step({
      name: `case ${testCase}`,
      fn: async () => {
        // ...etc...
      },
      sanitizeOps: false,
      sanitizeResources: false,
      sanitizeExit: false,
    })
  ));

  client.end();
});
```

```
#info="output"
test database ...
  test insert user ... ok (2ms)
  test insert book ... ok (14ms)
  test update and delete ...
    test update ... FAILED (17ms)
      Error: Fail.
          at <stack trace omitted>
    test delete ... ok (19ms)
  FAILED (46ms)
  test copy books ... ignored (0ms)
  test case 1 ... ok (14ms)
  test case 2 ... ok (14ms)
  test case 3 ... ok (14ms)
FAILED (111ms)
```

測試嵌套的 step 如果的 async 屬性應該和父測試一致，並且嵌套時應該設置好接收 測試環境的變量t 以免錯誤的引用到了祖先的 環境

```
Deno.test("my test", async (t) => {
  await t.step("step", async (t) => {
    // note the `t` used here is for the parent step and not the outer `Deno.test`
    await t.step("sub-step", () => {
    });
  });
});
```

# 運行測試

要運行測試執行 deno test 即可。你可以省略檔案名這種情況下，會遞歸查找目錄中所有測試檔案

* 檔案名爲 test.{ts, tsx, mts, js, mjs, jsx, cjs, cts},
* 檔案名後綴爲 .test.{ts, tsx, mts, js, mjs, jsx, cjs, cts}
* 檔案名後綴爲 \_test.{ts, tsx, mts, js, mjs, jsx, cjs, cts}

```
# 從當前檔案夾遞歸查找所有測試檔案並運行
deno test

# 運行 util/ 下所有測試檔案
deno test util/

# 運行指定測試檔案
deno test my_test.ts

# 並行運行測試檔案
deno test --parallel
```

# 過濾測試
有許多方法可以過濾掉要測試的內容

## Command line filtering

可以在命令行中傳入 --filter 選項來選擇要運行的測試

```
Deno.test({ name: "my-test", fn: myTest });
Deno.test({ name: "test-1", fn: test1 });
Deno.test({ name: "test-2", fn: test2 });
```

```
# 所有 測試都會運行，因爲名稱都包含 test
deno test --filter "test" tests/
```

```
# 使用 // 類似js語法糖 使用正則匹配要測試的名稱，故 my-test 不會被執行
deno test --filter "/test-*\d/" tests/
```

## 配置檔案

你也可以在配置檔案中定義要測試的內容

```
#info="include 指定要測試的內容"
{
  "test": {
    "files": {
      "include": [
        "src/fetch_test.ts",
        "src/signal_test.ts"
      ]
    }
  }
}
```

```
#info="exclude 指定要排除的測試內容"
{
  "test": {
    "files": {
      "exclude": ["out/"]
    }
  }
}
```

## 測試本身過濾

Deno.test 註冊測試代碼時有兩個可選變量，用於在特定情況下排除測試會只在特定情況下執行測試，比如要寫一段只在 windows 平臺的測試或在 windwos 平臺時不執行的測試

```
Deno.test({
  name: "do macOS feature",
  ignore: Deno.build.os !== "darwin",
  fn() {
    // do MacOS feature here
  },
});
```

```
Deno.test({
  name: "Focus on this test only",
  only: true,
  fn() {
    // test complicated stuff here
  },
});
```

# Failing fast

如果你的測試很長，你想在遇到的第一個測試時就停止後續測試，可以指定 --fail-fast

```
deno test --fail-fast
```