# dart

dart 是 google 2011年10月10日 發佈的 一個 開源(BSD)  物件導向 程式語言 目標是成爲 下一代 結構化 Web 開發語言

* 官網 [http://www.dartlang.org/](http://www.dartlang.org/)
* 源碼 [https://github.com/dart-lang](https://github.com/dart-lang)
* API [https://api.dartlang.org/stable/2.1.1/index.html](https://api.dartlang.org/stable/2.1.1/index.html)
* 官方教學 [https://www.dartlang.org/guides/language/language-tour](https://www.dartlang.org/guides/language/language-tour)
* DartPad [https://dartpad.dartlang.org/](https://dartpad.dartlang.org/)