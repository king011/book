# [Fiber](https://www.boost.org/doc/libs/1_76_0/libs/fiber/)

```
#include <boost/fiber/all.hpp>
```
```
namespace boost::fibers
namespace boost::this_fiber
```

```
find_package(Boost
    1.76.0
    REQUIRED
    COMPONENTS 
        fiber
)
```

* Author(s) -> Oliver Kowalke
* First Release -> 1.62.0
* C++ Standard Minimum Level -> 11
* Categories -> Concurrent Programming, System

纖程是一種協同調度的用戶線程，Fiber爲此提供了框架，並且爲其提供了類似標準庫 線程的 各種工具


```
#include <iostream>
#include <boost/fiber/all.hpp>

int main(int argc, char *argv[])
{
    std::cout << boost::this_fiber::get_id() << std::endl;

    boost::fibers::fiber fiber([]
                               { std::cout << boost::this_fiber::get_id() << std::endl; });
    fiber.join();
    return 0;
}
```

# BOOST\_FIBERS\_NO\_ATOMICS 宏

默認 Fiber 提供的所有同步對象可以安全的運行在不同的 thread 或 fiber 上。但是如果你的 fiber 都只運行在同一線程上可以定義 BOOST\_FIBERS\_NO\_ATOMICS 宏來編譯庫 這樣 Fiber 庫不會進行線程級別的同步，可以提高一點性能

但很明顯這無法利用多cpu的優勢，再高的但cpu性能也比不了一般性能的多cpu協作 故一般不推薦使用此宏

# 內部實現

Fiber 針對不同環境使用的不同的實現方式：

* fcontext_t -> 默認實現基於彙編，但並不使用所有平臺，能提供最高的性能
* ucontext_t -> 替代方案 可以通過 編譯 BOOST_USE_UCONTEXT 和 b2 屬性 context-impl=ucontext 來使用。有比 fcontext_t 更廣泛的 POSIX 平臺可用 但性能下降很多(百倍差距)
* WinFiber -> 使用 BOOST_USE_WINFIB 和 b2 屬性 context-impl=winfib 來使用

如果在 windows 上使用 fcontext_t 實現需要 關閉全局程序優化(**/GL**)，並且將 **/EHsc**(編譯器假定 extern "C" 函數從不拋出 c++異常 ) 更改爲 **/EHs**(告訴編譯器 extern "C" 函數可能拋出異常) 

# fiber 管理

fiber 的管理基本和 std::thread 差不多 boost 爲此提供了 類似 std::thread 風格的 api

## boost::fibers::fiber

class boost::fibers::fiber 用來表示一個纖程 其接口和用法基本和 std::thread 差不多

不過 fiber 可以多 接收一個 enum class launch 指定如何運行 fiber
```
enum class launch {
    dispatch,
    post
};
```

* dispatch -> 使用調度啓動fiber的調用者(之前運行的 fiber) 會被暫停，直到 調度器 有機會恢復 
* post -> 默認 調用者繼續執行，新啓動的 fiber 會在調度器稍後執行

```
#include <iostream>
#include <boost/fiber/all.hpp>
typedef boost::fibers::fiber fiber_t;
int main(int argc, char *argv[])
{
    fiber_t([]
            {
                puts("0");
                fiber_t([]
                        { puts("2"); })
                    .detach();
                puts("1");
            })
        .join();
    return 0;
}
```

```
#include <iostream>
#include <boost/fiber/all.hpp>
typedef boost::fibers::fiber fiber_t;
typedef boost::fibers::launch launch_t;
int main(int argc, char *argv[])
{
    fiber_t([]
            {
                puts("0");
                fiber_t(launch_t::dispatch, []
                        { puts("1"); })
                    .detach();
                puts("2");
            })
        .join();
    return 0;
}
```

## class boost::fibers::fiber::id

boost::fibers::fiber::id 用來唯一標識一個 fiber 實例

boost::fibers::fiber::id 重載了運算符 使用其和 thread::id 使用起來差不多

所有沒有fiber實例的id都會比較相等 並且和任何有實例的 id比較都不等

## boost::this_fiber

namespace boost::this_fiber 下提供了一些函數 基本上是 std::this_thread 函數的 fiber 版本

# [Scheduling 調度](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/scheduling.html)

線程中的 fibers 由 fiber 管理器協調， fibers 以協作方式交換控制權而非搶佔式，當前運行的 fiber 保留控制權，直到它調用某些將控制權交給管理器的操作。每次 fiber 掛起時， fiber 管理器都會諮詢調度程序以確定接下來運行哪個 fiber

每個線程都有自己的 調度器，boost默認爲線程使用 round_robin  算法，你可以實現自己的算法，此外 boost 也提供了 幾種默認算法實現

* round_robin
* work_stealing
* numa::work_stealing
* shared_work

調用 use\_scheduling\_algorithm 函數來爲線程指定 調度器算法
```
void thread_fn() {
    boost::fibers::use_scheduling_algorithm< my_fiber_scheduler >();
    ...
}
```

```
void thread( std::uint32_t thread_count) {
    // thread registers itself at work-stealing scheduler
    boost::fibers::use_scheduling_algorithm< boost::fibers::algo::work_stealing >( thread_count);
    ...
}

// count of logical cpus
std::uint32_t thread_count = std::thread::hardware_concurrency();
// start worker-threads first
std::vector< std::thread > threads;
for ( std::uint32_t i = 1 /* count start-thread */; i < thread_count; ++i) {
    // spawn thread
    threads.emplace_back( thread, thread_count);
}
// start-thread registers itself at work-stealing scheduler
boost::fibers::use_scheduling_algorithm< boost::fibers::algo::work_stealing >( thread_count);
...
```

## round\_robin

round\_robin 是 fiber 庫默認使用的算法，此算法使用循環的方式調度 fiber

```
#include <iostream>
#include <boost/fiber/all.hpp>

int main(int argc, char *argv[])
{
    boost::fibers::use_scheduling_algorithm<boost::fibers::algo::round_robin>();
    boost::fibers::fiber([]
                         {
                             puts("0");
                             boost::this_fiber::yield();
                             puts("2");
                         })
        .detach();
    boost::fibers::fiber([]
                         {
                             puts("1");
                             boost::this_fiber::yield();
                             puts("3");
                         })
        .detach();
    return 0;
}
```

## work\_stealing

work\_stealing 算法在本地就緒隊列中的 fiber 如果用完，則會從其它調度器中 竊取就緒的 fiber(隨機選擇 被竊取的 調度器)

工作線程存儲在靜態變量中，故不支持動態添加/刪除工作線程，也不能同時具有不同的工作線程域

構造函數 `work_stealing( std::uint32_t thread_count, bool suspend = false);` thread_count 指定工作線程數量，suspend 如果爲 true 則沒有可偷取的 fiber 則掛起調度器，直到超時或從遠程收到通知才被喚醒

```
#include <iostream>
#include <boost/fiber/all.hpp>

int main(int argc, char *argv[])
{
    std::thread t0([]
                   {
                       boost::fibers::use_scheduling_algorithm<boost::fibers::algo::work_stealing>(2);
                       std::this_thread::sleep_for(std::chrono::milliseconds(100));
                       boost::fibers::fiber f0([]
                                               {
                                                   std::this_thread::sleep_for(std::chrono::seconds(1)); // 使用線程掛起調度器 以便fiber 被偷取
                                                   std::cout << "t0 0 " << std::this_thread::get_id() << std::endl;
                                               });
                       boost::fibers::fiber f1([]
                                               { std::cout << "t0 1 " << std::this_thread::get_id() << std::endl; });
                       f0.join();
                       f1.join();
                       puts("t0 exit");
                   });
    std::thread t1([]
                   {
                       boost::fibers::use_scheduling_algorithm<boost::fibers::algo::work_stealing>(2);
                       std::this_thread::sleep_for(std::chrono::milliseconds(100));
                       boost::fibers::fiber f0([]
                                               {
                                                   std::cout << "t1 0 " << std::this_thread::get_id() << std::endl;
                                                   boost::this_fiber::sleep_for(std::chrono::seconds(1)); // 防止當前 線程結束，但所有 fiber 都已完成 所以會從 t0 偷取 f1 到當前線程執行
                                               });
                       f0.join();
                       puts("t1 exit");
                   });
    t0.join();
    t1.join();
    return 0;
}
```

## shared\_work

以循環方式調度 fiber，就緒的 fiber 在 shared\_work 的所有實例(在不同線程上運行)之間共享，因此 fiber 在所有線程上平均分配(shared\_work 效率低於 work\_stealing)

工作線程存儲在靜態變量中，故不支持動態添加/刪除工作線程，也不能同時具有不同的工作線程域

構造函數:
* shared_work();
* shared_work( bool suspend);

# [Synchronization 同步](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization.html)

fiber 庫 依據 std::thread 庫的 風格 定義了 用於 纖程的 同步 組件 此外還提供了 一些 fiber 特有的同步 組件

## [mutex](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/mutex_types.html)

幾個基本的 同步對象 用法和 c++11 中的同步對象 一致

* boost::fibers::mutex
* boost::fibers::recursive_mutex
* boost::fibers::timed_mutex
* boost::fibers::recursive_timed_mutex

## [condition_variable](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/conditions.html)

條件變量 boost::fibers::condition_variable 和 boost::condition_variable 用法一致

* boost::fibers::condition_variable
* boost::fibers::condition_variable_any

## [barrier](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/barriers.html)

barrier 和 boost::barrier 用戶一致

## [buffered\_channel/unbuffered\_channel](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/channels.html)

fiber 庫提供了 buffered\_channel/unbuffered\_channel 類似 golang 的 channel

枚舉 channel_op_status 定義的 channel 操作狀態
```
enum class channel_op_status {
    success,
    empty,
    full,
    closed,
    timeout
};
```

* success -> 操作成功
* empty -> channel 爲空，操作失敗
* full -> channel 已滿，操作失敗
* closed -> channel 已關閉，操作失敗
* timeout -> 在指定的時間內，操作沒有準備好

### [buffered\_channel](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/channels/buffered_channel.html)

buffered\_channel 是一個帶緩衝的 channeel

```
#include <iostream>
#include <boost/fiber/all.hpp>
typedef boost::fibers::buffered_channel<int> channel_t;

void send(channel_t &chan)
{
    for (int i = 0; i < 5; ++i)
    {
        chan.push(i);
    }
    chan.close();
}

void recv(channel_t &chan)
{
    // int i;
    // while (boost::fibers::channel_op_status::success == chan.pop(i))
    // {
    //     std::cout << "received " << i << std::endl;
    // }
    for (auto i : chan)
    {
        std::cout << "received " << i << std::endl;
    }
}
int main(int argc, char *argv[])
{

    channel_t chan{2};
    boost::fibers::fiber f1(std::bind(send, std::ref(chan)));
    boost::fibers::fiber f2(std::bind(recv, std::ref(chan)));

    f1.join();
    f2.join();
    return 0;
}
```

### [unbuffered\_channel](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/channels/unbuffered_channel.html)

unbuffered\_channel 用法類似 buffered\_channel 只是沒有緩衝

## [Futures](https://www.boost.org/doc/libs/1_76_0/libs/fiber/doc/html/fiber/synchronization/futures.html)

fiber 爲 c++11 中的 Future 提供了 fiber 版本

* boost::fibers::future
* boost::fibers::promise
* boost::fibers::packaged_task
* boost::fibers::async