# [align-items](https://www.primefaces.org/primeflex/alignitems)

align-items 定義 flex 內部橫軸怎麼對齊

| Class | Properties |
| -------- | -------- |
| align-items-stretch     | align-items: stretch;     |
| align-items-start     | align-items: flex-start;     |
| align-items-end     | align-items: flex-end;     |
| align-items-center     | align-items: center;     |
| align-items-baseline     | align-items: baseline;     |


## align-items-stretch

align-items-stretch 使 flex 內部子項被拉伸以適合容器

![](assets/stretch.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container blue-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```
## align-items-start

align-items-start 使項目位於容器開頭

![](assets/start.png)

```
<div class="card">
    <div class="flex align-items-start flex-wrap card-container yellow-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 150px">2</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 100px">3</div>
    </div>
</div>
```

## align-items-end 

align-items-end 使項目位於容器結尾

![](assets/end.png)

```
<div class="card">
    <div class="flex align-items-end flex-wrap card-container purple-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 150px">2</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 100px">3</div>
    </div>
</div>
```

## align-items-center

align-items-center 使項目位於容器中心

![](assets/center.png)

```
<div class="card">
    <div class="flex align-items-center flex-wrap card-container green-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 150px">2</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 100px">3</div>
    </div>
</div>
```

## align-items-baseline

align-items-baseline 使項目位於容器基線

![](assets/baseline.png)

```
<div class="card">
    <div class="flex align-items-baseline flex-wrap card-container indigo-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 150px">2</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 100px">3</div>
    </div>
</div>
```

# 響應式佈局
和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同