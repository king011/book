# 容器管理
```txt
# 顯示 容器
sudo docker ps [-a]
	a	顯示所有容器

# 使用 映像ubuntu 啓動一個 容器 並且 運行  /bin/bash
sudo docker run -ti ubuntu /bin/bash
	t	爲容器綁定一個虛擬終端
	i	將容器的標準輸入打開
	d	在後臺以 守護態執行
	--name	爲容器指定名稱 而非默認生成的隨機名

# 獲取 守護態執行 容器的 輸出
sudo docker logs 容器id

# 進入到 run -d 的容器中
sudo docker attach 容器id

# 進入到 容器並且 執行指定 命令
# sudo docker exec -it 容器id bash
sudo docker exec 容器id

# 停止/運行/重啓容器
sudo docker stop/start/restart 容器id

# 刪除一個 容器
sudo docker rm 容器id

# 導出 容器 快照
sudo docker export 容器id > ubuntu.back.tar

# 將 容器快照 導入爲 映像
cat ubuntu.back.tar | sudo docker import - king/ubuntu:v1
# 導入 支持 http 協議
sudo docker import http://example.com/exampleimage.tgz king/ubuntu:v1
```

# 離開 docker 容器
```txt
# 離開 容器 並且 停止 容器
exit

# 離開容器 但 不要停止 容器(在後臺繼續運行)
CTRL-p + CTRL-q
```