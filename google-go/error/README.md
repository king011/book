# error

golang 使用返回 error 接口來處理錯誤，然 error 只剩下了文本信息 標準庫 errors.New 用於將文件創建爲 error

```
package main

import (
	"errors"
	"fmt"
)

var ErrSys = errors.New(`syscall error`)

func Syscall() error {
	return ErrSys
}

func Action() (e error) {
	e = Syscall()
	if e != nil {
		return
	}
	// do other things
	return
}
func main() {
	e := Action()
	fmt.Println(e)
}
```

上面代碼中如果 Action 需要將 Syscall 的錯誤 包裝下再返回，將丟失原本的錯誤信息，爲此 golang1.13 提供了解決方案爲 error 使用 Wrap Unwrap 並且提供了輔助函數

* func Is(err, target error) bool 返回 err 是否是 target 會自動爲 err 迭代解包
* func As(err error, target interface{}) bool 返回 err 是否含有和 target 相同型別的 錯誤 會自動爲 err 迭代解包
* func Unwrap(err error) error 解包如果沒有 如果 err 沒有實現 `Unwrap() error` 函數 返回 nil
* e = fmt.Errorf(`syscall: %w`, e) %w 格式化輸出並創建 Wrap err

```
package main

import (
	"errors"
	"fmt"
)

var ErrSys = errors.New(`syscall error`)

func Syscall() error {
	return ErrSys
}

func Action() (e error) {
	e = Syscall()
	if e != nil {
		e = fmt.Errorf(`action error -> %w`, e)
		return
	}
	// do other things
	return
}
func main() {
	e := Action()
	fmt.Println(errors.Is(e, ErrSys))
	fmt.Println(e)
}
```

# 除了使用 fmt.Errof 也可以自己實現 Wrap

```
package main

import (
	"errors"
	"fmt"
)

var ErrSys = errors.New(`syscall error`)

type ErrAction struct {
	e error
}

func (ErrAction) Error() string {
	return `action error`
}
func (e ErrAction) Unwrap() error {
	return e.e
}
func Syscall() error {
	return ErrSys
}

func Action() (e error) {
	e = Syscall()
	if e != nil {
		e = ErrAction{e: e}
		return
	}
	// do other things
	return
}
func main() {
	e := Action()
	fmt.Println(errors.Is(e, ErrSys))
	fmt.Println(e)
}
```
