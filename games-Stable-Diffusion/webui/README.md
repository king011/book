# webui

[stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui) 爲 stable diffusion 提供了一個開源(GPL3)的 web ui 界面

# windows 安裝

windows 至少需要 windowns 10 最簡單的安裝方法是
1. 從 [v1.0.0-pre](https://github.com/AUTOMATIC1111/stable-diffusion-webui/releases/tag/v1.0.0-pre) 下載 [sd.webui.zip](https://github.com/AUTOMATIC1111/stable-diffusion-webui/releases/download/v1.0.0-pre/sd.webui.zip) 並且解壓。解壓後會等待如下檔案和檔案夾

	* **system** 此檔案夾存儲了一些依賴軟體主要是 git 和 特定版本的 python 
	* **webui** 核心程式
	* **environment.bat** 環境變量設置，如果在朝鮮等地區建議在此設置代理 
	
		```
		set HTTP_PROXY=http://127.0.0.1:8118
		set HTTPS_PROXY=http://127.0.0.1:8118
		```
		
	* **update.bat** 運行它將自動拉取最近的 webui
	* **run.bat** 運行它將運行 webui

2. 雙擊 **update.bat**  更新 webui 到最新
3. 雙擊 **run.bat** 運行 webui