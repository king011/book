# 安裝

```
sudo apt install python3 python3-pip
```

```bash
pip3 install you-get
```

* you-get 會被安裝到 ` ~/.local/bin` 所以請將  `~/.local/bin` 加入到 PATH 環境變量
* 此外 you-get 需要依賴 FFmpeg 1.0 或以上版本

	```bash
	sudo apt install ffmpeg
	```
	
* you-get 支持 -s 參數設置 socks 代理，但此時需要依賴 PySock

	```bash
	pip3 install PySocks
	```
	
# 升級

```bash
pip3 install --upgrade you-get
```