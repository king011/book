# 動態日誌等級

zap 提供了 AtomicLevel 是一個 原子可變的 動態日誌記錄 級別 它允許在運行時 安全的更改 日誌記錄級別

```go
package main

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

func main() {
	// atom := zap.NewAtomicLevelAt(zap.DebugLevel)
	atom := zap.NewAtomicLevel()

	encoderCfg := zap.NewProductionEncoderConfig()

	logger := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		atom,
	))
	defer logger.Sync()

	logger.Info("info logging enabled")
	// 改變 日誌 記錄 等級
	atom.SetLevel(zap.ErrorLevel)
	logger.Info("info logging disabled")
}
```

# http
AtomicLevel 本身 是一個 http.Handler 可以提供 以json的 http 訪問 改變 日誌等級
```go
package main

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net"
	"net/http"
	"os"
	"time"
)

func main() {
	// atom := zap.NewAtomicLevelAt(zap.DebugLevel)
	atom := zap.NewAtomicLevel()

	encoderCfg := zap.NewProductionEncoderConfig()

	logger := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		atom,
	))
	defer logger.Sync()

	// 持續打印 日誌
	go func() {
		for {
			time.Sleep(time.Second)
			logger.Info("http test")
		}
	}()

	// 運行 http 服務
	srv := http.Server{Handler: atom}
	addr := "localhost:9000"
	l, e := net.Listen("tcp", "localhost:9000")
	if e != nil {
		logger.Fatal(e.Error())
	}
	logger.Info("http run",
		zap.String("addr", addr),
	)
	srv.Serve(l)
}
```

```bash
king@king-company ~ $ curl 127.0.0.1:9000
{"level":"info"}
king@king-company ~ $ curl -X PUT -H "Content-Type:application/json" -d '{"Level":"error"}' 127.0.0.1:9000 
{"level":"error"}
king@king-company ~ $
```

