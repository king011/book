# 抽取音頻
```
ffmpeg -i 3.mp4 -vn -y -acodec copy 3.aac
```

# 剪切音頻
```
ffmpeg -i source.mp3 -vn -acodec copy --ss 00:03:21.36 -t 00:00:41 output.mp3
```

| 參數 | 含義 |
| -------- | -------- |
| -ss     | 切割開始 小時:分鐘:秒     |
| -t     | 持續時間     |
| -to     | 切割結束時間     |

