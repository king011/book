# websocket

websocket 在 2011 年被 [IETF](https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force) 標準化爲 [RFC 6455](https://datatracker.ietf.org/doc/html/rfc6455)。它爲 web 程序提供了全雙工的雙向傳輸能力。

websocket 的 scheme 是 ws 和 wss 分別對標 http 和 https 其它 url 和 http 相同