# 命令行工具
```sh
# 打印 lua 版本信息
lua -v

# 執行 main.lua
lua main.lua

# 進入交互模式，可以輸入 lua 語句，輸入的語句將立刻執行
# (以換行符 分隔語句 若一行不足構成合法的 lua 程序塊 將等待輸入)
lua

# 執行 main.lua，並在執行成功後進入交互模式(失敗則不會進入交互模式)
lua -i main.lua
```

# 語法

* lua 語句間不需要分隔符號，但也可以加上分號或換行符
* 使用 os.exit() 退出lua腳本
* lua 源碼區分大小寫
* 使用 **--** 進行單行註釋
* 使用 **--[[ ]]** 進行多行註釋
* 全局變量可以直接使用不需要定義，將全局變量賦值爲 nil 可釋放其內存

	```
	print(b) -- 輸出 nil
	b = 123 -- 設置全局變量
	print(b) -- 輸出 123
	b = nil -- 釋放全局變量
	print(b) -- 輸出 nil
	```
	
**注意** lua 檔案第一行如果以 **#** 開始則會忽略此行，則是爲了兼容 \*inux 系統下指定 shell 外殼

```
#!/usr/local/bin/lua
```

# 標識符

* 標識符可以是任意 **字母 數字 下劃線**，但不能以下劃線作爲開頭
* 應該避免使用像 **\_VERSION** 這種以下劃線開始的全大寫標識符，因爲 lua 使用這類標識符做一些特殊用途

## 保留字

lua 使用了一些保留字，標識符不能和它們同名

<table>
<tr><td>and</td><td>break</td><td>do</td><td>else</td><td>elseif</td></tr>
<tr><td>end</td><td>false</td><td>for</td><td>function</td><td>if</td></tr>
<tr><td>in</td><td>local</td><td>nil</td><td>not</td><td>or</td></tr>
<tr><td>repeat</td><td>return</td><td>then</td><td>true</td><td>until</td></tr>
<tr><td>while</td></tr>
</table>


				 
				
				
				

