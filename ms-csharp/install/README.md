# 環境安裝

[donet] 是微軟為 linux 提供的 .Net 運行環境

```
#info="sdk for ubuntu 20.0.4"

# 添加 微軟源
wget https://packages.microsoft.com/config/ubuntu/20.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

# 更新源並按照 sdk5.0
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-5.0
```

```
#info="創建一個 sample1 項目 並運行"
dotnet new console --output sample1
dotnet run --project sample1
```

```
# 編譯項目
dotnet build --configuration Release
```