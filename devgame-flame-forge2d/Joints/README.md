# [Joints](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html)

關節用於以各種方式將兩個不同的物體連接在一起。 它們有助於模擬物體之間的相互作用，以創建鉸鏈、輪子、繩索、鏈條等。

關節中的一個 Body 可以是 BodyType.static 類型。 BodyType.static 和/或 BodyType.kinematic 之間的關節是允許的，但沒有效果並且需要一些處理時間

要建構Joint，您需要建立JointDefand的對應子類別並使用其參數對其進行初始化

要註冊關節，請使用 world.createJoint，稍後當您想要刪除它時，請使用 world.destroyJoint

# [Built-in joints](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#built-in-joints)

目前，Forge2D 支援以下關節：

* ConstantVolumeJoint
* DistanceJoint
* FrictionJoint
* GearJoint
* MotorJoint
* MouseJoint
* PrismaticJoint
* PulleyJoint
* RevoluteJoint
* RopeJoint
* WeldJoint
* WheelJoint

## [ConstantVolumeJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#constantvolumejoint)

這種類型的關節將一組物體連接在一起並在它們內部保持恆定的體積。 本質上，它是一組距離關節，將所有物體依次連接起來

例如，它在模擬“軟體”時很有用

```
  final constantVolumeJoint = ConstantVolumeJointDef()
    ..frequencyHz = 10
    ..dampingRatio = 0.8;

  bodies.forEach((body) {
    constantVolumeJoint.addBody(body);
  });
    
  world.createJoint(ConstantVolumeJoint(world, constantVolumeJoint));
```

ConstantVolumeJointDef 需要使用 addBody 方法新增至少 3 個實體。 它還有兩個可選參數：

* **frequencyHz**: 此參數設定關節的振動頻率。 如果未設定為 0，則值越高，每個複合 DistantJoint 的彈性越小
* **dampingRatio**: 此參數定義了振盪停止的速度。 它的範圍從 0 到 1，其中 0 表示無阻尼，1 表示臨界阻尼

## [DistanceJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#distancejoint)

DistanceJoint 約束兩個實體上的兩點彼此保持固定距離

您可以將其視為一根無質量的剛性桿

```
final distanceJointDef = DistanceJointDef()
  ..initialize(firstBody, secondBody, firstBody.worldCenter, secondBody.worldCenter)
  ..length = 10
  ..frequencyHz = 3
  ..dampingRatio = 0.2;

world.createJoint(DistanceJoint(distanceJointDef));
```

要建立 DistanceJointDef，您可以使用初始化方法，該方法需要兩個物體以及每個物體上的世界錨點。 此定義使用局部錨點，允許稍微違反初始配置中的約束。 這在保存和加載遊戲時很有用

DistanceJointDef 有三個可設定的可選參數：

* **length**: 此參數決定兩個錨點之間的距離，必須大於0。預設值為1
* **frequencyHz**: 此參數設定關節的振動頻率。 如果未設定為 0，則數值越高，關節的彈性就越小
* **dampingRatio**: 此參數定義了振盪停止的速度。 它的範圍從 0 到 1，其中 0 表示無阻尼，1 表示臨界阻尼

> 不要使用零或短長度

## [FrictionJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#frictionjoint)

FrictionJoint 用於模擬自上而下遊戲中的摩擦力。 它提供 2D 平移摩擦力和角摩擦力

FrictionJoint 與兩個形狀在螢幕 x-y 平面上碰撞時發生的摩擦無關。 相反，它旨在模擬沿垂直於螢幕的 z 軸的摩擦力。 它最常見的用例是在移動物體和遊戲地板之間施加摩擦力

FrictionJointDef 方法的初始化方法需要兩個將施加摩擦力的主體和一個錨點

第三個參數是世界座標中將施加摩擦力的錨點。 在大多數情況下，它將是第一個物件的中心。 但是，對於物體之間更複雜的物理相互作用，您可以將錨點設定到一個或兩個物體上的特定位置

```
final frictionJointDef = FrictionJointDef()
  ..initialize(ballBody, floorBody, ballBody.worldCenter)
  ..maxForce = 50
  ..maxTorque = 50;

  world.createJoint(FrictionJoint(frictionJointDef));
```

創建 FrictionJoint 時，可以透過最大力和扭力值應用模擬摩擦：

**maxForce**: 施加到連接體上的最大平移摩擦力。 較高的值模擬較高的摩擦力
**maxTorque**: 可施加在連接體上的最大角摩擦力。 較高的值模擬較高的摩擦力

換句話說，前者模擬身體滑動時的摩擦，後者模擬身體旋轉時的摩擦

## [GearJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#gearjoint)

GearJoint 用於將兩個關節連接在一起。 接頭必須是旋轉接頭或棱柱接頭的任意組合

> 連接的關節必須將動態主體連接到靜態主體。 靜態主體預期是這些關節上的 bodyA


```
final gearJointDef = GearJointDef()
  ..bodyA = firstJoint.bodyA
  ..bodyB = secondJoint.bodyA
  ..joint1 = firstJoint
  ..joint2 = secondJoint
  ..ratio = 1;

world.createJoint(GearJoint(gearJointDef));
```

* **joint1**, **joint2**: 連接的旋轉關節和棱柱關節
* **bodyA**, **bodyB**: 任何物體都可以形成連接的關節，只要它們不是同一物體即可
* **ratio**: 齒輪比

與 PulleyJoint 類似，您可以指定齒輪比將運動綁定在一起：

```
coordinate1 + ratio * coordinate2 == constant 
```

此比率可以是負值也可以是正值。 如果一個關節是 RevoluteJoint，另一個關節是 PrismaticJoint，則比率的單位將為長度或 1/長度。

由於 GearJoint 依賴其他兩個關節，因此如果這些關節被破壞，則 GearJoint 也需要被破壞

> 如果joint1或joint2被破壞，則手動破壞GearJoint

## [MotorJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#motorjoint)

MotorJoint 用於控制兩個物體之間的相對運動。 典型用途是控制動態主體相對於固定點的運動，例如建立動畫

MotorJoint 可讓您透過指定目標位置和旋轉偏移來控制主體的運動。 您可以設定達到目標位置和旋轉所需的最大馬達力和扭矩。 如果主體被阻擋，它將停止，並且接觸力將與最大電機力和扭矩成正比

```
final motorJointDef = MotorJointDef()
  ..initialize(first, second)
  ..maxTorque = 1000
  ..maxForce = 1000
  ..correctionFactor = 0.1;

  world.createJoint(MotorJoint(motorJointDef));
```

MotorJointDef 有三個可選參數：

* **maxForce**: 將施加到連接體以到達目標位置的最大平移力
* **maxTorque**: 將施加到連接體以達到目標旋轉的最大角力
* **correctionFactor**: 位置修正係數在 \[0, 1\] 範圍內。 它調整關節對目標位置偏差的反應。 較高的值使關節響應更快，而較低的值使其響應更慢。 如果該值設定得太高，關節可能會過度補償並振盪，從而變得不穩定。 如果設定太低，它可能會反應太慢

線性和角度偏移是物體相對於彼此的位置和旋轉應達到的目標距離和角度。 預設情況下，線性目標將是兩個主體中心之間的距離，角度目標將是主體的相對旋轉。 使用 MotorJoint 的 setLinearOffset(Vector2) 和 setLinearOffset(double) 方法設定主體之間所需的相對平移和旋轉

例如，此程式碼在每個更新周期都會增加關節的角度偏移，從而導致主體旋轉

```
@override
void update(double dt) {
  super.update(dt);
  
  final angularOffset = joint.getAngularOffset() + motorSpeed * dt;
  joint.setAngularOffset(angularOffset);
}
```

## [MouseJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#mousejoint)

MouseJoint 用於用滑鼠操縱物體。 它嘗試將主體上的點移向遊標的當前位置。 輪換沒有限制

MouseJoint 定義具有目標點、最大力、頻率和阻尼比。 目標點最初與身體的錨點重疊。 最大力用於防止多個動態體相互作用時發生劇烈反應。 您可以將其設定為您喜歡的大小。 頻率和阻尼比用於產生類似距離接頭的彈簧/阻尼器效果

> 許多用戶嘗試過調整滑鼠關節來玩遊戲。 使用者往往希望實現精確定位和瞬時響應。 老鼠關節在這種情況下不能很好地工作。 您可能希望考慮使用運動體

```
final mouseJointDef = MouseJointDef()
  ..maxForce = 3000 * ballBody.mass * 10
  ..dampingRatio = 1
  ..frequencyHz = 5
  ..target.setFrom(ballBody.position)
  ..collideConnected = false
  ..bodyA = groundBody
  ..bodyB = ballBody;

  mouseJoint = MouseJoint(mouseJointDef);
  world.createJoint(mouseJoint);
}
```

* **maxForce**: 此參數定義了移動候選體所能施加的最大約束力。 通常，您將表示為重量的某個倍數（乘數質量重力）
* **dampingRatio**: 此參數定義了振盪停止的速度。 它的範圍從 0 到 1，其中 0 表示無阻尼，1 表示臨界阻尼
* **frequencyHz**: 此參數定義了身體的反應速度，即它嘗試到達目標位置的速度
* **target**: 初始世界目標點。 假設這最初與身體錨點一致

## [PrismaticJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#prismaticjoint)

PrismaticJoint 提供單一自由度，讓兩個物體沿著固定在 bodyA 中的軸進行相對平移。 防止相對旋轉

PrismaticJointDef 需要使用軸和錨點定義運動線。 此定義使用局部錨點和局部軸，以便初始配置可以稍微違反約束

當局部錨點在世界空間中重疊時，關節平移為零。 使用本地錨點和本地軸有助於保存和加載遊戲

> 至少一個物體應該是動態的且具有非固定旋轉


PrismaticJoint 定義與 RevoluteJoint 定義類似，但它使用平移而不是旋轉

```
final prismaticJointDef = PrismaticJointDef()
  ..initialize(
    dynamicBody,
    groundBody,
    dynamicBody.worldCenter,
    Vector2(1, 0),
  )
```

* **b1**, **b2**: 透過關節連接的實體
* **anchor**: World 錨點，用於放置軸。 通常是第一個實體的中心
* **axis**: World 平移軸，平移將沿著此軸固定

在某些情況下，您可能想要控制運動範圍。 為此，Prismatic Joint Def 具有可選參數，可讓您模擬關節限制和/或馬達

### [Prismatic Joint Limit](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#prismatic-joint-limit)

您可以使用指定下平移和上平移的關節限制來限制相對旋轉

```
jointDef
  ..enableLimit = true
  ..lowerTranslation = -20
  ..upperTranslation = 20;
```

* **enableLimit**: 設定為 true 以啟用平移限制
* **lowerTranslation**: 平移向下限制
* **upperTranslation**: 平移向上限制

您也可以使用此方法建立關節後變更限制：

```
prismaticJoint.setLimits(-10, 10);
```

### [Prismatic Joint Motor](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#prismatic-joint-motor)


您可以使用馬達來驅動運動或模擬關節摩擦。 提供最大馬達扭力，因此不會產生無限的力

```
jointDef
  ..enableMotor = true
  ..motorSpeed = 1
  ..maxMotorForce = 100;
```

* **enableMotor**: 設定為 true 以啟用馬達
* **motorSpeed**: 所需的馬達速度（弧度每秒）
* **maxMotorForce**: 用於實現所需馬達速度的最大馬達扭力（以 N-m 為單位）

使用以下方法建立關節後，您可以改變馬達的速度和力：

```
prismaticJoint.setMotorSpeed(2);
prismaticJoint.setMaxMotorForce(200);
```

另外，您可以使用以下方法來獲得關節角度和速度：

```
prismaticJoint.getJointTranslation();
prismaticJoint.getJointSpeed();
```

## [PulleyJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#pulleyjoint)

PulleyJoint 用於創建理想化的滑輪。 滑輪將兩個物體連接到地面並相互連接。 當一個身體上升時，另一個身體就會下降。 根據初始配置，滑輪繩的總長度保持不變：

```
length1 + length2 == constant
```

您可以提供模擬滑輪組的比率。 這導致滑輪的一側比另一側延伸得更快。 同時一側的約束力小於另一側。 您可以使用它來創建機械槓桿。

```
length1 + ratio * length2 == constant
```

例如，如果比率為 2，則 length1 將以 length2 的兩倍速率變化。 此外，連接到第一主體的繩索中的力將具有連接到第二主體的繩索的約束力的一半。

```
final pulleyJointDef = PulleyJointDef()
  ..initialize(
    firstBody,
    secondBody,
    firstPulley.worldCenter,
    secondPulley.worldCenter,
    firstBody.worldCenter,     
    secondBody.worldCenter,
    1,
  );

world.createJoint(PulleyJoint(pulleyJointDef));
```

PulleyJointDef 的初始化方法需要兩個地錨、兩個動態體及其錨點以及一個滑輪比

* **b1**, **b2**: 透過關節連接的兩個動態體
* **ga1**, **ga2**: 兩個地錨
* **anchor1**, **anchor2**: 關節將連接到的動態主體上的錨點
* **r:** 模擬滑輪組的滑輪比

PulleyJoint 也提供當前長度：

```
joint.getCurrentLengthA()
joint.getCurrentLengthB()
```

> PulleyJoint 本身可能會有點麻煩。 當與棱柱形接頭結合使用時，它們通常會發揮更好的作用。 您還應該用靜態形狀覆蓋錨點，以防止一側長度為零

## [RevoluteJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#revolutejoint)

RevoluteJoint 強制兩個實體共享一個公共錨點，通常稱為鉸鏈點。 旋轉關節具有單一自由度：兩個物體的相對旋轉

若要建立 RevoluteJoint，請為初始化方法提供兩個主體和一個公共點。 此定義使用局部錨點，以便初始配置可以稍微違反約束

```
final jointDef = RevoluteJointDef()
  ..initialize(firstBody, secondBody, firstBody.position);
world.createJoint(RevoluteJoint(jointDef));
```

在某些情況下，您可能想要控制關節角度。 為此，RevoluteJointDef 具有可選參數，可讓您模擬關節限制和/或馬達

### [Revolute Joint Limit](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#revolute-joint-limit)

您可以使用指定下角度和上角度的關節限制來限制相對旋轉

```
jointDef
  ..enableLimit = true
  ..lowerAngle = 0
  ..upperAngle = pi / 2;
```

* **enableLimit**: 設定為 true 以啟用角度限制
* **lowerAngle**: 下角的弧度
* **upperAngle**: 上角的弧度

您可以在使用此方法建立關節後變更限制：

```
revoluteJoint.setLimits(0, pi);
```

### [Revolute Joint Motor](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#revolute-joint-motor)

您可以使用馬達來驅動繞共享點的相對旋轉。 提供最大馬達扭矩，從而不會產生無限的力

```
jointDef
  ..enableMotor = true
  ..motorSpeed = 5
  ..maxMotorTorque = 100;
```

* **enableMotor**: 設定為 true 以啟用馬達
* **motorSpeed**: 所需的馬達速度（弧度每秒）
* **maxMotorTorque**: 用於實現所需馬達速度的最大馬達扭力（以 N-m 為單位）

在使用以下方法建立接頭後，您可以更改馬達的速度和扭矩：

```
revoluteJoint.setMotorSpeed(2);
revoluteJoint.setMaxMotorTorque(200);
```

另外，您可以使用以下方法來獲得關節角度和速度：

```
revoluteJoint.jointAngle();
revoluteJoint.jointSpeed();
```

## [RopeJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#ropejoint)

RopeJoint 限制兩個實體上兩點之間的最大距離

RopeJointDef 需要兩個身體錨點和最大長度

```
final ropeJointDef = RopeJointDef()
  ..bodyA = firstBody
  ..localAnchorA.setFrom(firstBody.getLocalCenter())
  ..bodyB = secondBody
  ..localAnchorB.setFrom(secondBody.getLocalCenter())
  ..maxLength = (secondBody.worldCenter - firstBody.worldCenter).length;

world.createJoint(RopeJoint(ropeJointDef));
```

* **bodyA**, **bodyB**: 連接體
* **localAnchorA**, **localAnchorB**: 可選參數，相對於身體原點的錨點
* **maxLength**: 繩子的最大長度。 該值必須大於 LinearSlop，否則關節將無法運作

> 關節假設最大長度在模擬過程中不會改變。 如果您想動態控制長度，請參閱 DistanceJoint

## [WeldJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#weldjoint)

WeldJoint 用於限制兩個實體之間的所有相對運動，從而有效地將它們連接在一起

WeldJointDef 需要連結兩個實體和一個世界錨

```
final weldJointDef = WeldJointDef()
  ..initialize(bodyA, bodyB, anchor);

world.createJoint(WeldJoint(weldJointDef));
```

* **bodyA**, **bodyB**: 將連接的兩個物體
* **anchor**: 世界座標中的錨點，兩個物體將在此處焊接在一起，值為 0，值越高，接頭的彈性越小

### [Breakable Bodies and WeldJoint](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/joints.html#breakable-bodies-and-weldjoint)

由於 Forge2D 約束求解器是迭代的，因此關節有些靈活。 這意味著透過 WeldJoint 連接的實體可能會輕微彎曲。 如果要模擬易碎的主體，最好建立具有多個夾具的單一主體。 當主體損壞時，您可以銷毀固定裝置並在新主體上重新建立它，而不是依賴焊接接頭