# [Dock](https://primeng.org/dock)


```
import {DockModule} from 'primeng/dock';
import {MenuItem} from 'primeng/api';
```

Dock 是由菜單項組成的導航組件(看起來很像 ubuntu 桌面的系統菜單)

```
<p-dock [model]="dockBasicItems" position="bottom">
    <ng-template pTemplate="item" let-item>
        <img [src]="item.icon" [alt]="item.label" width="100%">
    </ng-template>
</p-dock>
```
```
export class DockDemo {

    dockItems: MenuItem[];

    ngOnInit() {
        this.dockItems = [
            {
                label: 'Finder',
                icon: "https://primefaces.org/cdn/primeng/images/dock/finder.svg"
            },
            {
                label: 'App Store',
                icon: "https://primefaces.org/cdn/primeng/images/dock/appstore.svg"
            },
            {
                label: 'Photos',
                icon: "https://primefaces.org/cdn/primeng/images/dock/photos.svg"
            },
            {
                label: 'Trash',
                icon: "https://primefaces.org/cdn/primeng/images/dock/trash.png"
            }
        ];

    }
}
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| id     | string     | null     | 元素唯一標識     |
| model     | object     | null     | 菜單項數組     |
| style     | object     | null     | 組件的內聯 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| position     | string     | bottom     | 菜單顯示位置: bottom top left right     |

# Styling

| Name | Element |
| -------- | -------- |
| p-dock     | Container element.     |
| p-dock-list     | List of items.     |
| p-dock-item     | Each items in list.     |

