# [height](https://www.primefaces.org/primeflex/height)

height 用於指定元素的高度

| Class | Properties |
| -------- | -------- |
| h-full     | height: 100%;     |
| h-screen     | height: 100vh;     |
| h-auto     | height: auto;     |
| h-1rem     | height: 1rem;     |
| h-2rem     | height: 2rem;     |
| ...     | ...     |
| h-29rem     | height: 29rem;     |
| h-30rem     | height: 30rem;     |

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="border-round bg-blue-100 w-12rem h-6rem p-3 m-3">
            <div class="h-full border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">h-full</div>
        </div>
        <div class="border-round bg-blue-100 w-12rem h-6rem p-3 m-3">
            <div class="h-auto border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">h-auto</div>
        </div>
    </div>
</div>
```

## Fixed Height

你可以使用 rem 來指定固定高度

![](assets/rem.png)

```
<div class="card">
    <div class="flex flex-row flex-wrap align-items-center justify-content-center card-container yellow-container">
        <div class="h-6rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">h-6rem</div>
        <div class="h-11rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">h-11rem</div>
        <div class="h-20rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">h-20rem</div>
        <div class="h-24rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">h-24rem</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同