# [background-position](https://www.primefaces.org/primeflex/backgroundposition)

background-position 用於配置背景圖像的起始位置

| Class | Properties |
| -------- | -------- |
| bg-bottom     | background-position: bottom;     |
| bg-center     | background-position: center;     |
| bg-left     | background-position: left;     |
| bg-left-bottom     | background-position: left bottom;     |
| bg-left-top     | background-position: left top;     |
| bg-right     | background-position: right;     |
| bg-right-top     | background-position: right top;     |
| bg-right-bottom     | background-position: right bottom;     |
| bg-top     | background-position: top;     |

![](assets/0.png)

```
<div class="card">
    <div class="card-container blue-container overflow-hidden">
        <div class="flex flex-wrap align-items-center justify-content-center">
            <div>
                <div class="bg-left-top bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-left-top</div>
            </div>
            <div>
                <div class="bg-top bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-top</div>
            </div>
            <div>
                <div class="bg-right-top bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-right-top</div>
            </div>
        </div>
        <div class="flex flex-wrap align-items-center justify-content-center">
            <div>
                <div class="bg-left bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-left</div>
            </div>
            <div>
                <div class="bg-center bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-center</div>
            </div>
            <div>
                <div class="bg-right bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-right</div>
            </div>
        </div>
        <div class="flex flex-wrap align-items-center justify-content-center">
            <div>
                <div class="bg-left-bottom bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-left-bottom</div>
            </div>
            <div>
                <div class="bg-bottom bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">bg-bottom</div>
            </div>
            <div>
                <div class="bg-right-bottom bg-no-repeat w-10rem h-10rem bg-blue-500 border-round m-3"
                    style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 5rem;">
                </div>
                <div class="text-center">text-right-bottom</div>
            </div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同