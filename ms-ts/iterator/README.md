# 迭代器

迭代器 是一個實現了 next 方法 的對象 每次 返回一條數據

可迭代對象 Iterable 需要實現一個 `[Symbol.iterator](): Iterator<T>` 返回迭代器

可以使用 for of 遍歷 Iterable 的迭代器


```
class Container<T> implements Iterable<T>  {
    constructor(private datas_: Array<T>) { }
    [Symbol.iterator](): Iterator<T> {
        let index = 0
        const datas = this.datas_
        return {
            next(): IteratorResult<T> {
                if (index < datas.length) {
                    return {
                        done: false,
                        value: datas[index++],
                    }
                }
                return {
                    done: true,
                    value: undefined,
                }
            }
        }
    }
}
const arrs: Array<number> = [1, 2, 3, 4]
const c = new Container(arrs)
for (const val of c) {
    console.log(val)
}
```