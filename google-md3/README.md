# Material Design 3

Material Design 3 是由 google 開源設計的用於構建美觀 ui 的系統規範

* 官網 [https://m3.material.io/](https://m3.material.io/)
* 源碼 [https://github.com/material-components](https://github.com/material-components)

md3 由三個主要部分組成

1. foundations
2. styles
3. components