# [Breadcrumb](https://primeng.org/breadcrumb)

```
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {MenuItem} from 'primeng/api';
```

Breadcrumb 提供有關頁面層次結構的上下文信息

![](assets/primeng.org_breadcrumb.png)

```
<p-breadcrumb [model]="items"></p-breadcrumb>
```

```
export class BreadcrumbDemo implements OnInit {

    private items: MenuItem[];

    ngOnInit() {
        this.items = [
            {label:'Categories'},
            {label:'Sports'},
            {label:'Football'},
            {label:'Countries'},
            {label:'Spain'},
            {label:'F.C. Barcelona'},
            {label:'Squad'},
            {label:'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi'}
        ];
    }
}
```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onItemClick     | originalEvent: Browser event<br>item: Selected menu item     | Fired when an item is selected.     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單項數組     |
| home     | MenuItem     | null     | 配置 home 圖標     |
| style     | string     | null     | 組件內聯的 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| homeAriaLabel     | string     | null     | home 圖標的  aria label     |

# Styling

| Name | Element |
| -------- | -------- |
| p-breadcrumb     | Container element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-breadcrumb-chevron     | Chevron element.     |
