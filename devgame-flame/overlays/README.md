# [overlays](https://docs.flame-engine.org/1.16.0/flame/overlays.html)

由於 Flame 遊戲可以封裝在 widget 中，因此可以輕鬆地將其與樹中的其他 Flutter widget 一起使用

首先需要在 GameWidget.overlayBuilderMap 中註冊如何創建 widget

```
// On the widget declaration
final game = MyGame();

Widget build(BuildContext context) {
  return GameWidget(
    game: game,
    overlayBuilderMap: {
      'PauseMenu': (BuildContext context, MyGame game) {
        return Text('A pause menu');
      },
    },
  );
}
```

然後使用 game.overlays.add/remove 來 添加/刪除 widget

```
// Inside your game:
final pauseOverlayIdentifier = 'PauseMenu';

// Marks 'PauseMenu' to be rendered.
overlays.add(pauseOverlayIdentifier);
// Marks 'PauseMenu' to not be rendered.
overlays.remove(pauseOverlayIdentifier);
```

> overlays 的渲染順序由overlayBuilderMap 中鍵的順序決定