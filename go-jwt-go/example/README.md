# example

```
package main

import (
	"fmt"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Session .
type Session struct {
	Name string `json:"name"`
	// 嵌入標準 payload
	jwt.StandardClaims
}

func main() {
	// 簽名密鑰
	key := []byte(`cerberus is an idea`)

	at := time.Now()
	session := &Session{
		Name: `kk`,
		StandardClaims: jwt.StandardClaims{
			// 設置失效時間
			ExpiresAt: at.Unix() + 1,
			Issuer:    `king`,
			Subject:   `login`,
		},
	}
	// 創建 token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, session)
	tokenString, e := token.SignedString(key)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`tokenString =`, tokenString)
	// 解析 token
	var s Session
	token, e = jwt.ParseWithClaims(tokenString, &s, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(token.Valid, s)
	time.Sleep(time.Second * 2)
	token, e = jwt.ParseWithClaims(tokenString, &s, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if e != nil {
		// log.Fatalln(e)
	}
	fmt.Println(token.Valid, s.Valid())
}
```