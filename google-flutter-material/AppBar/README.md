# AppBar

[AppBar](https://docs.flutter.io/flutter/material/AppBar-class.html) 是 位於 app 最上方的 工具組合

通常 由以下 部分 組成

* 一個 標題
* 兩個圖標按鈕
* 下拉按鈕組合

![](assets/AppBar.gif)

```dart
AppBar(
	title: const Text('AppBar Example'),
	actions: <Widget>[
		// 按鈕
		IconButton(
			icon:Icon(Icons.directions_car),
		),
		IconButton(
			icon:Icon(Icons.directions_bike),
		),
		// 按鈕菜單
		PopupMenuButton<IconData>(
				itemBuilder:(BuildContext context){
					return [
						PopupMenuItem<IconData>(
							child:new Text("open"),
						),
						PopupMenuItem<IconData>(
							child:new Text("close"),
						),
					];
				}
		)
	]
)
```
