# [第一天](https://www.youtube.com/watch?v=jP81NGYYyag&list=PLx5HU4Hz75RFNlWiZ75jMPF4-QDJy5WBR&index=1)


## abandon

**v** 拋棄，放棄

We had to **abandon** the car.  
我們不得不棄車而去。

How could she **abandon** her own child?  
她字母能拋棄自己的孩子？

## ability

**n** 能力，才能

There's no doubting her **ability**.  
她的能力毋庸置疑。

She's a woman of considerable **abilities**.  
她是個非常有才幹的女性。

## able

**adj** 能做某事

Will she be **able** to cope with the work?  
她能應付這項工作嗎？

I'm sorry that i wasn't **able** to phone you yesterday.  
對不起，昨天我沒能給你打電話。

## about

**prep** 有關，關於

What's that book **about**?  
這本書是關於什麼的？

I'm worried **about** David.  
我真爲大衛擔心。

## above

**prep/adv** 在...上面，高於

It's on the shelf just **above** your head.  
就在你頭頂上方的架子上。

They value their freedom **above** all else.  
他們把自由看到高於一切。

## abroad

**adv** 在國外

He's currently **abroad** on business.  
目前他在國外出差。

We always go **abroad** in the summer.  
我們夏天總是到國外去。

## absence

**n** 缺席；缺勤；不在場

A new manager was appointed during her **absence**.  
她不在的時候任命了一位新經理。

She has had repeated **absences** from work this year.  
今年她屢次曠工。

## absolute

**adj** 完全的；絕對的

I have **absolute** faith in her judgment.  
我完全相信她的判斷。

There was no **absolute** proof of fraud.  
沒有確凿的證據能證明是詐騙行爲。

## absolutely

**adv** 完全的/絕對的

I trusted him **absolutely**.  
我絕對信任他。

Are you **absolutely** sure?  
你完全肯定嗎？

## absorb

**v** 吸收，掌握

Plants **absorb** nutrients from the soil.  
植物從土壤中吸收養分。

It's hard to **absorb** so much information.  
掌握如此大量的資訊非常困難。

## abuse

**n** 虐待，濫用

He was arrested for animal **abuse**.  
他因虐待動物而被逮捕。

The politician was accused of power **abuse**.  
那位政治家被指控濫用權利。

## academic

**adj** 學術的，學習好的

She published an **academic** paper on climate change.  
她發表了一篇關於氣候變化的學術論文。

He's not very **academic**.  
他的學習不怎麼樣。

## accept

**v** 接受

Do you **accept** credit cards?  
你們接受信用卡付帳嗎？

Please **accept** this small gift.  
請收下這份小小的禮物。

## acceptable

**adj** 可接受的

This kind of attitude is simply not **acceptable**.  
這種態度是完全無法接受的。

The quality of this product is **acceptable** for its price.  
考慮到其價格，這個產品的質量是可接受的。

## access

**n** 通道，途徑；使用權

The only **access** to the village is by boat.  
到那個村子去的唯一途徑是乘船。

**Access** to the paper is restricted to senior management.  
只有高級管理層才有權查閱這些文件。

## accident

**n** 意外，事故

He broke the vase by **accident**.  
他不小心打碎了花瓶。

She was injured in a traffic **accident**.   
她在一起交通事故中受傷了。

## accommodation

**n** 住處

They paid for his flights and hotel **accommodations**.  
他們支付了他的機票和酒店住宿。

There's a shortage of cheap **accommodation**.  
便宜的住宿供不應求。

## accompany

**v** 陪伴，配有

Whatever her husband went, she would **accompany** him.  
無論丈夫去哪裏，她都會陪伴左右。

The course books are **accompanied** by four CDs.  
這些課本配有四張光盤。

## according to

**prep** 據...所說，根據

**According to** Sarah, they're not getting on very well at the moment.  
據 Sarah 說，他們目前關係不是太好。

**According to** our records you owe us $130.  
根據我們的紀錄，你欠我們130美元。


## account

**n** 賬戶，帳戶；報道

She deposited the check in her **account**.  
她把支票存入了帳戶。

This gives a first-hand **account** of the war.  
這是關於這場戰爭的第一手報道。

## accurate

**adj** 準確的

Her account of the events was surprisingly **accurate**.  
她對事件的描述出奇的準確。

We need **accurate** measurements to complete the design.  
我們需要準確的測量數據來完成設計。

## accuse

**v** 指控；指責

Are you **accusing** me of lying?  
你是在指控我撒謊嗎？

He's been **accused** of robbery.  
他被指控犯有搶劫。

## achieve

**v** 實現，完成

She finally **achieved** her ambition to visit South America.  
她終於實現了去南美洲旅行的夢想。

She finally **achieved** her goal of running a marathon.  
她終於完成了跑馬拉松的目標。

## achievement

**n** 成就

Winning the award was a major **achievement** in her life.  
贏得這個獎項是她人生中的一個重大成就。

His scientific **achievements** have contributed greatly to the field.  
他在科學上的成就對這個領域有很大的貢獻。

## acid

**n** 酸，酸的

Vinegar is an **acid**.  
醋是一種酸。

Blueberry bushes need a very **acid** soil.  
藍莓的生長需要強酸性的土壤。

# [第二天](https://www.youtube.com/watch?v=NaDSjkxlQek&list=PLx5HU4Hz75RFNlWiZ75jMPF4-QDJy5WBR&index=2)

## acknowledge

**v** 承認

The family **acknowledges** the need for change.  
全家人都承認需要改變一下。

Maybe you're right, she **acknowledged**.  
也許你是對的，她承認道。

## acquire

**v** 購得，獲得

He **acquired** the firm in 2008.  
他於 2008 年收購了這家公司。

I seem to have **acquired** two copies of this book.  
這本書我好像買重複了。

## across

**prep** 穿過，越過  
**adv** 在...對面

They ran straight **across** the road.  
他們徑自越過了馬路。

My best frined lives **across** the road.  
我最好的朋友住在馬路對面。

## act

**v** 行爲，行動  
**n** 演出

You're **acting** like a child!  
你的行爲簡直就像個孩子！

Have you ever **acted** in a play before?  
你以前演過戲劇嗎？

## action

**n** 行動，行爲

So what's the plan of **action**?  
那麼行動計劃是什麼？

I asked him to explain his **actions**.  
我要求他爲自己的行爲作出解釋。

## active

**adj** 忙碌的;積極參與的

My father always led a very **active** life.  
我父親一直過著忙碌的生活。

She's very **active** in local politics.  
她積極參與當地的政治活動。

## activist

**n** 積極分子，活動家

He's been a party **activist** for many years.  
多年來他一直是黨的積極分子。

He's an **activist** for free speech.  
他是言論自由的活動家。

## activity

**n** 活動，繁忙

Join our weekend **activity** at the park.  
參加我們週末在公園的活動。

Her day is full of **activity**.  
她的一天總是很繁忙。

## actor

**n** 演員

Who's your favorite **actor**?  
你最喜歡的演員是誰？

She has starred with many leading **actors**.  
她曾經與許多大牌演員一起擔任主角。

## actual

**adj** 實際的，真正的

Those were his **actual** words.  
那都是他的原話(實際說的話)。

He discovered his **actual** talent in painting.  
他發現了自己在繪畫方面的真正才能。

## actually

**adv** 實際上

**Actually**, we've known each other for many years.  
實際上我們認識很多年了。

No, **actually** i'm rather glad.  
不，實際上我挺高興的。

## ad

**n** 廣告

I saw your product in an **ad**.  
我在廣告中看到了你的產品。

That **ad** was really catchy.  
那個廣告真的很吸引人。

## adapt

**v** 適應，改編

It tooke me a while to **adapt** to the new job.  
我過了一段時間才適應了新工作。

The play had been **adapted** for children.  
這個劇本已被改編成兒童劇。

## add

**v** 加，增加

Please **add** sugar to my coffee.  
請在我的咖啡裏加糖

**Add** more details to your report.  
在你的報告中增加更多細節。

## addition

**n** 除此之外，增加

In **addition**, we need consider the budget.  
除此之外，我們還需要考慮預算。

The **addition** of a new member strengthened our team.  
新成員的加入加強了我們的團隊。

## additional

**adj** 額外的，附加的

We need **additional** chairs for the meeting.  
我們會議需要額外的椅子。

There are **additional** costs for delivery.  
遞送需要附加費用。

## address

**n** 地址

Her home **address**.  
她的家庭地址。

What's your email **address**?  
你的郵箱地址是什麼？

## adequate

**adj** 足夠的

I have **adequate** time for prepare.  
我有足夠的時間來準備。

It's **adquate** for our needs.  
這足以滿足我們的需要了。

## adjust

**v** 調整

Taste the soup and **adjust** the seasoning.  
嘗嘗湯的味道，在調整佐料的用量。

Check and **adjust** the brakes regularly.  
經常檢查並調節剎車裝置。

## administration

**n** 管理，行政

She has little experience in **administration**.  
她沒有甚麼管理經驗。

She works in the school **administration**.  
她在學校行政部門工作。

## administrative

**adj** 管理的，行政的

This is an **administrative** problem.  
這是個管理的問題。

Your responsibilities will be mainly **administrative**.  
你的職責主要是在行政方面。

## admire

**v** 傾佩，欣賞

I **admired** him for his determination.  
我很傾佩他那堅毅的性格。

We stood for a few moments **admiring** the view.  
我們駐足了一會兒，欣賞美麗的景色。

## admission

**n** 承認，入場費

Her slience was taken as an **admission** of guilt.  
她的沉默被認爲是承認自己有罪。

How much do they charge for **admission**?
他們入場費要收多少錢？

## admit

**v** 承認

He **admitted** his guilt.  
他承認有罪。

I must **admit**, i was scared.  
我必須承認，我害怕了。

## adopt

**v** 領養

They've **adopt** a baby girl.  
他們領養了一個女嬰。

We decided to **adopt** his suggestion.  
我們決定採納他的建議。

# [第三天](https://www.youtube.com/watch?v=p3lHy1Pzn9E&list=PLx5HU4Hz75RFNlWiZ75jMPF4-QDJy5WBR&index=3)

## adult

**n** 成年人  
**adj** 成年的

**Adults** pay an admission charge but children get in free.  
成年人買票入內，兒童免費。

Let's try to be **adult** about this.  
讓我們以成年人的方式處理這事。

## advance

**v/n** 預先，提前；前進，進步

Please book tickets 21 days in **advance**.  
請提前21天訂票。

We need to **advance** further to reach the summit.  
我們需要繼續前進才能到達山頂。

## advanced

**adj** 先進的，高級的

These are **advanced** weapon systems.  
這些是先進的武器系統。

She has **advanced** skills in cooking.  
她的烹飪技巧很高級。

## advantage

**n** 優勢

Her experience gave her an **advantage**.  
她的經驗比較有優勢。

It would be to your **advantage** to agree to his demands.  
答應他的要求對你有利。

## advert

**n** 廣告

Did you see that funny **advert** on TV last night?  
你昨晚看到了電視上那個有趣的廣告了嗎？

This magazine has more **adverts** than content.  
這本雜誌上的廣告比內容還多。

## advertise

**v** 做廣告

We **advertised** our cat in the local newspaper.  
我們在當地報紙上登了廣告出售我們的轎車。


How much does it cost to **advertise** on TV?  
在電視上打廣告需要多少錢？

## advertisement

**n** 廣告

The Sunday papers are full of **advertisement** for cars.  
週日的報紙上全是汽車廣告。

She saw an **advertisement** for a ski vacation in Vermont.  
她看到一則佛特蒙州滑雪度假的廣告。

## advertising

**n** 廣告業

She works in **advertising**.  
她在廣告業工作。

Work of mouth is the best form if **advertising**.  
口碑是最好的廣告形式。

## advice

**n** 建議

Can you give me some **advice**?  
你能給我些建議嗎？

My parents gave me great **advice** when i was younger.  
我年輕時，父母給了我很好的建議。

## advise

**v** 建議，勸告

My doctor **advised** me to get more exercise.  
我的醫生建議我多做些運動。

Evans **advised** him to leave London.  
埃文斯勸他離開倫敦。

## adviser

**n** 顧問

He acted as an **adviser** to the mayor.  
他擔任市長的顧問。

I need to talk to my career **adviser**.  
我需要和我的職業顧問談談。

## affair

**n** 事務；事情

The whole **affair** was a misunderstanding.  
整件事情都是一個誤會。

She's daeling with a family **affair** right now.  
她現在正在處理一個家庭事務。

## affect

**v** 影響

Her words **affected** me deeply.  
她的話深深的影響了我。

The economy **affects** everyone's life.  
經濟影響著每個人的生活。

## afford

**v** 買得起

I can't **afford** a new car right now.  
我現在買不起新車。

I couldn't **afford** the rent on my own.  
我單靠自己付不起房租。

## afraid

**adj** 害怕的

I'm **afraid** of spiders.  
我害怕蜘蛛。

She's **afraid** to fly.  
她害怕乘坐飛機。

## after

**prep/adv** 之後

Let's go for a walk **after** breakfast.  
我們吃過早餐後去散步吧。

Her name came **after** mine on the list.  
名單上她的名字排在我後面。

## afternoon

**n** 下午

It was on a Saturday **afternoon**.  
那是在一個星期六的下午。

It was a sunny **afternoon**.
那是一個陽光明媚的下午。

## afterwards

**adv** 隨後，後來

She died not long **afterwards**.  
不久以後她去世了。

**Afterwards**, i was asked to write a book.  
後來，有人要我寫一本書。

## again

**adv** 再一次

He's late **again**.  
他又遲到了。

If he does it **again**, i'll have to tell him.  
如果他再這麼做，我就必須得提醒他了。


## against

**prep** 反對

She voted **against** the proposal.  
她投票反對這個提案。

I'm **against** smoking in public places.  
我反對在公共場所抽菸。

## age

**n** 年齡

**Age** is just a number.  
年齡只是個數字。

What is your **age**?  
你多大年紀？

## aged

**adj** 年老的，歲

A man **aged** around 50 walked in.  
一個大約50歲的男人走進來。

They offer services for the **aged** population.  
他們爲老年人提供服務。

## agency

**n** 機構；中介

She works for an advertising **agency**.  
她在廣告代理機構上報。

I booked my trip through a travel **agency**.  
我通過旅行機構預訂了行程。

## agent

**n** 代理人；特工

We hired a real estate **agent** to help us find a house.  
我們僱了一個房產經紀人幫忙找房子。

An **agent** will contact you with more details.  
代理人會與你聯繫並提供更多的詳情。

## aggressive

**adj** 好鬥的；富有攻擊性的；挑釁的

He plays football in a very **aggressive** manner.  
他踢球的方式非常激進。

You need to be less **aggressive** in negotiations.  
你在談判中不需要那麼激進。

# [第四天](https://www.youtube.com/watch?v=IDpFFcqlIEA&list=PLx5HU4Hz75RFNlWiZ75jMPF4-QDJy5WBR&index=4)

## ago

**adv** 以前

I read that book weeks **ago**.  
我幾周前讀過那本書。

A long time **ago**.  
很久以前。

## agree

**v** 贊同

I **agree** with you on this issue.  
在這個問題上我贊同你的意見。

He and i nerver seem to **agree**.  
他和我好像從來都沒有意見一致過。

## agreement

**n** 協議

Finally the two sides have reached an **agreement**.  
雙方終於達成了協議。

They signed a trade **agreement** last week.  
他們上週簽署了一項貿易協議。

## agriculture

**n** 農業

She studies **agriculture** at the university.  
她在大學學習農業。

**Agriculture** is a vital part of our economy.  
農業是我們經濟的重要部分。

## ahead

**adv** 在前面

The road **ahead** is very busy.  
前面路上非常擁擠。

Rick walked on **ahead** of us.  
瑞克走在我們前面。

## aid

**n** 援助

The charity provides **aid** to children in need.  
該慈善機構爲有需要的兒童提供援助。

They are seeking legal **aid** for the case.  
他們正在爲這個案件尋求法律援助。

## aim

**n** 目標，瞄準

My main **aim** in life is to be a good husband.  
我人生的主要目標是做一個好丈夫。

She raised her gun, took **aim** and fired.  
她舉起了槍，瞄準開火。

## air

**n** 空氣，空中

I went outside to get some fresh **air**.  
我到室外呼吸一下新鮮空氣。

The **air** was filled with the scent of roses.  
空氣中瀰漫著玫瑰的芳香。

## aircraft

**n** 飛機

That is a military **aircraft**.  
那是一架軍用飛機。

I saw an **aircraft** flying over.  
我看到一架飛機飛過。

## airline

**n** 航空公司

What **airline** did you fly with?  
你乘坐的是哪家航空公司的飛機？

She was working for an **airline**.  
她在航空公司工作。

## airport

**n**  機場

Her family went to see her off at the **airport**.  
她的家人去機場送她。

How do i get to the **airport**?  
我怎麼去機場？

## alarm

**n** 警報器，鬧鐘

I forgot to set the burglar **alarm**.  
我忘記打開防盜警報器了。

I sleep until the **alarm** goes off.  
我睡到鬧鐘響才醒來。

## album

**n** 專輯，相冊

Have you heard their new **album**?  
你聽過他們的新專輯嗎？

A photograph **album**.  
一本相冊。

## alcohol

**n** 酒

I don't drink **alcohol** anymore.  
我不再喝酒了。

Low **alcohol** drinks.
低度酒。

## alive

**adj** 活着的

My grandparents are still **alive**.  
我的祖父母都還健在。

They're lucky to be **alive**.  
他們能活着實屬幸運。

## all

**pron/adj** 全部；完全

Have you done **all** your homework?  
你作業都做完了嗎？

The boys played video games **all** day.  
男孩子們整天玩電子遊戲。

## allow

**v** 允許

How much time are we **allowed**?  
我們可以有多少時間？

The audience is not **allowed** backstage.  
觀衆不允許進入後臺。

## allowance

**n** 補助，津貼，零用錢

Do you get a commuting **allowance**?  
你有通勤補貼嗎？

How much **allowance** do you get?  
你有多少零用錢？

## all right

**interj** 好的；沒問題

Are you feeling **all right**?  
你覺得還好嗎？

Don't worry, it'll turn out **all right**.  
別擔心，會好起來的。

## almost

**adv** 差不多

Have you **almost** finished?  
你差不多完成了嗎？

Dinner's **almost** ready.  
晚餐差不多好了。

## alone

**adj/adv** 單獨

She lives **alone**.  
她一個人生活。

Do you like live **alone**?  
你喜歡獨居嗎？

## along

**adv/prep** 向前；順着

We were just walking **along**, chatting.  
我們就這麼一邊往前走一邊聊。

Go straight **along** this way.  
沿着這條路直走。

## alongside

**prep/adv** 在旁邊；與一起

A car drew up **alongside**.  
一輛車在旁邊停了下來。

The new pill will be used **alongside** existing medicines.  
這種新藥將配合現有藥物一起服用。

## already

**adv** 已經

I've **already** told him.   
我已經告訴他了。

It's 5 o'clock **already**.  
已經5點了。

## also

**adv** 而且；還是

I'm cold, and i'm **also** hungry.  
我很冷，而且又俄。

She's photographer and **also** writes books.  
它是個攝影師，還是個作家。

