# record_mp3

[record_mp3](https://pub.dev/packages/record_mp3) 支持以mp3 錄音

```
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kc_microservice_flutter/pages/component/dialog.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:record_mp3/record_mp3.dart';

class MyKingPage extends StatefulWidget {
  MyKingPage({Key key}) : super(key: key);
  @override
  _MyKingPageState createState() => _MyKingPageState();
}

class _MyKingPageState extends State<MyKingPage> {
  RecordMp3 get mp3 => RecordMp3.instance;
  Future<bool> _checkPermission() async {
    Map<PermissionGroup, PermissionStatus> map = await new PermissionHandler()
        .requestPermissions(
            [PermissionGroup.storage, PermissionGroup.microphone]);
    return map[PermissionGroup.microphone] == PermissionStatus.granted;
  }

  void _startRecord() async {
    if (mp3.status == RecordStatus.PAUSE) {
      mp3.resume();
      setState(() {});
      return;
    }
    bool hasPermission = await _checkPermission();
    if (!hasPermission) {
      showErrorMessageDialog(context, "hasPermission no");
      return;
    }
    debugPrint("$hasPermission");
    final temp = Directory.systemTemp;
    final exists = await temp.exists();
    if (!exists) {
      temp.createSync(
        recursive: true,
      );
    }
    final str = temp.path + "/a.mp3";
    mp3.start(str, (e) {
      debugPrint(e.toString());
      return null;
    });
    setState(() {});
  }

  _pausRecord() {
    mp3.pause();
    setState(() {});
  }

  _stopRecord() {
    mp3.stop();
    setState(() {});
  }

  _open() async {
    final temp = Directory.systemTemp;
    final exists = await temp.exists();
    if (!exists) {
      temp.createSync(
        recursive: true,
      );
    }
    final str = temp.path + "/a.mp3";
    final file = File(str);
    final stream = file.openRead();
    stream.listen((v) {
      debugPrint("${v.length}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("king 測試"),
      ),
      body: ListView(
        children: <Widget>[
          Text("${mp3.status}"),
          RaisedButton(
            child: Text("start"),
            onPressed: _startRecord,
          ),
          RaisedButton(
            child: Text("pause"),
            onPressed: _pausRecord,
          ),
          RaisedButton(
            child: Text("stop"),
            onPressed: _stopRecord,
          ),
          RaisedButton(
            child: Text("open"),
            onPressed: _open,
          ),
        ],
      ),
    );
  }
}
```