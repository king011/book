# vbox

1. 從 [snapshots/targets/x86/64/](https://downloads.openwrt.org/snapshots/targets/x86/64/) 下載 虛擬機 鏡像 openwrt-x86-64-combined-squashfs.img.gz 並解壓

	```
	gzip -d openwrt-x86-64-combined-squashfs.img.gz
	```

2. 將鏡像轉爲 vbox鏡像

	```
	VBoxManage convertfromraw --format VDI openwrt-x86-64-combined-squashfs.img openwrt-x86-64-combined-squashfs.vdi
	```