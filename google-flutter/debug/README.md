# print

可以 直接使用 print 將 日誌 輸出 到控制檯 但如果 輸內容 太多 android 可能會 直接 丟棄 輸出

# debugPrint

```dart
#info=false
import 'package:flutter/foundation.dart';
```

flutter 提供了 debugPrint 函數 包裝了 print 不會輸出 讓 android 丟棄的 日誌