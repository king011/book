# [fmt](https://pkg.go.dev/fmt) 

fmt 實現了 C 語言的 scanf printf 來格式化 輸入 輸出

## General

* **%v** 默認 format 值
* **%+v** 類似 **%v** 同時輸出字段名
* **%#v** 類似 **%v** 同時以golang語法輸出
* **%T** 型別
* **%%** **%** 符號

## Boolean
* **%t** **true** 或 **false**

## Integer

* **%b** 二進制
* **%c** 對應 Unicode 代表的字符
* **%d** 十進制
* **%o** 八進制
* **%q** 使用 go 語法 安全的 轉義 **'** 符號
* **%x** 十六進制 小寫
* **%X** 十六進制 大寫
* **%U** Unicde format: U+1234;same as "U+%04X"

## Floating-point and complex constituents

* **%b** 十進制科學計數  指數爲2的冪 e.g. -123456p-78
* **%e** 科學計數 e.g. -1.234456e+78
* **%E** 科學計數 e.g. -1.234456E+78
* **%f** decimal point but no exponent, e.g. 123.456
* **%F** 和 %f 相同
* **%g** %e for large exponents, %f otherwise. Precision is discussed below.
* **%G** %E for large exponents, %F otherwise

# String 和 \[\]byte
* **%s** 字符串
* **%q** 使用 go 語法安全轉義 **"**
* **%x** 16進制小寫
* **%X** 16進制大寫

# slice

* %p
...
