# atomic

```
#include <stdatomic.h>
```

c11 提供了 原子操作

```
#include <stdatomic.h>
#include <stdio.h>
int main(void)
{
    atomic_size_t i = 0;
    printf("%ld\n", i);
    // 存儲
    atomic_store(&i, 123);

    // 加載
    size_t current = atomic_load(&i);
    printf("%ld\n", current);

    // 計算
    size_t old = atomic_fetch_add(&i, 321);
    printf("old=%ld current=%ld\n", old, i);
    atomic_fetch_sub(&i, 443);
    printf("%ld\n", i);
    atomic_fetch_or(&i, 2); // 位運算
    printf("%ld\n", i);

    // 設置並返回舊值
    old = atomic_exchange(&i, 123);
    printf("old=%ld current=%ld\n", old, i);

    // cas
    size_t target = 99;
    size_t compare = 1;
    atomic_compare_exchange_strong(&i, &compare, 99); // 不等 故 不設置 i
    printf("%ld\n", i);

    compare = 123;
    atomic_compare_exchange_strong(&i, &compare, 99); //相等 故 設置 i
    printf("%ld\n", i);
    return 0;
}
```