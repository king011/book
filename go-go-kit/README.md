# go-kit

Go kit 是golang實現的一個 開源(MIT) 的微服務器框架

* 官網 [https://gokit.io/](https://gokit.io/)
* 源碼 [https://github.com/go-kit/kit](https://github.com/go-kit/kit)

```
go get github.com/go-kit/kit
```