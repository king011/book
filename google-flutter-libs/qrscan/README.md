# qrscan

[qrscan](https://pub.dev/packages/qrscan) 是一個 flutter 插件 支持 android 識別 的 barcode qrcode 掃碼識別

```
dependencies:
 qrscan: ^0.2.17
```

```
#info="AndroidManifest.xml"
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
```

```
import 'package:qrscan/qrscan.dart' as scanner;

String cameraScanResult = await scanner.scan();
```
