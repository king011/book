# 編譯

出於一些目的，可能會需要自行編譯 ffmpeg(例如需要使用顯卡的硬件加速)，下面以 ubuntu 爲例

## 下載源碼

從 [官網](https://ffmpeg.org/download.html) 下載源碼，並且選定一個路徑作爲編譯的工作路徑，下面的例子將工作路徑設置到環境變量 BUILD_DIR

```
export BUILD_DIR=$(pwd)

curl -O -L https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
tar -jxvf ffmpeg-snapshot.tar.bz2
```


## 安裝依賴

```
sudo apt-get -y install build-essential autoconf automake cmake libtool git
```

```
sudo apt install build-essential autoconf automake cmake libtool git \
checkinstall nasm yasm libass-dev libfreetype6-dev libsdl2-dev p11-kit \
libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev \
libxcb-xfixes0-dev pkg-config texinfo wget zlib1g-dev libchromaprint-dev \
frei0r-plugins-dev gnutls-dev ladspa-sdk libcaca-dev libcdio-paranoia-dev \
libcodec2-dev libfontconfig1-dev libfreetype6-dev libfribidi-dev libgme-dev \
libgsm1-dev libjack-dev libmodplug-dev libmp3lame-dev libopencore-amrnb-dev \
libopencore-amrwb-dev libopenjp2-7-dev libopenmpt-dev libopus-dev \
libpulse-dev librsvg2-dev librubberband-dev librtmp-dev libshine-dev \
libsmbclient-dev libsnappy-dev libsoxr-dev libspeex-dev libssh-dev \
libtesseract-dev libtheora-dev libtwolame-dev libv4l-dev libvo-amrwbenc-dev \
libvorbis-dev libvpx-dev libwavpack-dev libwebp-dev libx264-dev libx265-dev \
libxvidcore-dev libxml2-dev libzmq3-dev libzvbi-dev liblilv-dev \
libopenal-dev opencl-dev libjack-dev
```

```
sudo apt install libbluray-dev libfdk-aac-dev
```

## 編譯

```
cd "$BUILD_DIR/ffmpeg"
./configure  --prefix="$BUILD_DIR/build" --bindir="$BUILD_DIR/bin" \
    --disable-static --enable-shared \
    --enable-nonfree --enable-gpl --enable-libfreetype \
    --enable-libfdk_aac --enable-libmp3lame --enable-libopus --enable-libvorbis \
    --enable-libvpx --enable-libx264 --enable-libx265
make
make install
```

```
export LD_LIBRARY_PATH="$BUILD_DIR/build/lib"
"$BUILD_DIR/bin/ffmpeg" -version
```

# nvidia

ffmpeg 支持 n 卡硬件加速

1. 安裝 [CUDA](https://developer.nvidia.com/cuda-downloads) 工具包

2. 安裝 ffnvcodec

	```
	git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git
	cd nv-codec-headers && sudo make install
	```
	
3. 重新編譯 ffmpeg 並啓用 cuda

	```
	./configure  --prefix="$BUILD_DIR/build" --bindir="$BUILD_DIR/bin" \
			--extra-cflags="-I/usr/local/cuda/include" \
			--extra-ldflags="-L/usr/local/cuda/lib64" \
			--disable-static --enable-shared \
			--enable-nonfree --enable-gpl --enable-libfreetype \
			--enable-libfdk_aac --enable-libmp3lame --enable-libopus --enable-libvorbis \
			--enable-libvpx --enable-libx264 --enable-libx265 \
			--enable-cuda --enable-cuvid  --enable-nvenc --enable-libnpp
	```

4. 查詢支持硬件加速的的 解碼器(cuvid) 編碼器(nvenc)

	```
	export LD_LIBRARY_PATH="$BUILD_DIR/build/lib"
	"$BUILD_DIR/bin/ffmpeg" -codecs |egrep 'cuvid|nvenc'
	```
	

```
#!/bin/bash

set -e
function get_bitrate
{
      local bitrates=$(ffprobe -v quiet -print_format json -show_format -show_streams "$1" | egrep 'bit_rate' | awk '$2 ~ "^\"[0-9]+\",$" {print $2}')
      local bitrate
      local str
      for str in $bitrates
      do
            bitrate="$str"
      done
     
      if [[ "$bitrate" == "" ]];then
            return 1
      fi
      bitrate=${bitrate%\",}
      bitrate=${bitrate#\"}
      # Bitrate=$((bitrate+200000))
      Bitrate="$bitrate"
}

function convert
{
      local input="$1"
      local output="$2.mp4"

      get_bitrate "$input"

      ./ffmpeg -y -i "$input" \
            -c:v h264_nvenc -preset p7 -tune:v hq -profile:v high \
            -vf format=yuv420p \
            -b:v "$Bitrate" \
            -c:a aac \
            "$output"
}
```
# VAAPI

```
sudo apt install libmfx1 libmfx-tools libva-dev libmfx-dev intel-media-va-driver-non-free vainfo
```

```
export LIBVA_DRIVER_NAME=i965
```

```
sudo apt install cmake pkg-config meson libdrm-dev automake libtool
```

```
git clone https://github.com/Intel-Media-SDK/MediaSDK
cd MediaSDK
git checkout abb56fa
mkdir build && cd builder
cmake ..
make
sudo make install
```

```
export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig
export LIBVA_DRIVERS_PATH=/usr/lib/x86_64-linux-gnu/dri
export LD_LIBRARY_PATH=/opt/intel/mediasdk/lib
```

```
./configure  --prefix="$BUILD_DIR/build" --bindir="$BUILD_DIR/bin" \
        --extra-cflags="-I/usr/local/cuda/include" \
        --extra-ldflags="-L/usr/local/cuda/lib64" \
        --disable-static --enable-shared \
        --enable-nonfree --enable-gpl --enable-libfreetype \
        --enable-libfdk_aac --enable-libmp3lame --enable-libopus --enable-libvorbis \
        --enable-libvpx --enable-libx264 --enable-libx265 \
        --enable-cuda --enable-cuvid  --enable-nvenc --enable-libnpp \
        --enable-vaapi --enable-libmfx
```