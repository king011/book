# Nexus 6

Nexus 6 算是可選的 [Nexus](https://zh.wikipedia.org/wiki/Google_Nexus) 系列最後一代手機，雖然後面還有 Nexus 6P，但正常人不推薦使用把員工送到監獄的獨裁政府白手套的公司代工的產品。
雖然 還有一個 LG 代工的 Nexus 5X，但其配置比 Nexus 6 低，故目前還是推薦購買 Nexus 6，目前(2024-09-26)大概 280 rmb 左右可淘寶購得。

雖然是一款 10年前的手機，目前還是有購買的價值，它還有下述本喵推薦購買的原因：

1. root 容易，root 後能玩很多有趣的東西。價格便宜(即使root失敗，變磚也不心疼)攜帶方便，把它不要當作手機當作一個 root 後隨身攜帶的 linux 便攜設備
2. 感受 google 原生整合的用戶體驗(當然如果是此目的，建議購買最新的 [Pxiel](https://zh.wikipedia.org/wiki/Google_Pixel) 系列，但你可以先花小錢來 Nexus 嘗試下，使用後絕對不會再想用其它 android 廠商的產品)
3. 在 root 後，你可以把它一直接上電源，放在家裏作爲一個透明代理的旁路由使用，雖然很多人使用刷路由器來實現此功能，但對代理工具的支持完整度嵌入式的路由器還是遠不及對 android 的支持完整和易用
4. 如果身處朝鮮，可以把它和日常設備進行隔離，用它訪問非朝鮮網路和 app
5. 可作爲 android 的真實測試機
6. 屏幕足夠大，可用作看視頻
7. 屏幕足夠大，配個手柄，玩下 PPSSPP 或 街機遊戲都行。本喵則是使用 Moonlight 串流家中電腦玩 3A 遊戲(這個比較費流量)，另外一些運營商可能會限流 udp 對 Moonlight 影響很大。這時 root 過的設備可以使用 udp2raw 或 hysteria_v1 的 fake tcp 來嘗試解決(看吧 root 後用處多吧，但 root 有風險，來 root 10年前的 Nexus 6 就很合適)

目前本喵主要把它用於上述第 7 點，如果後面 Moonlight 不再支持 android 7(Nexus 6 最高支持的版本)，本喵可能就會永遠封存之前購買的 google 信仰到收藏箱。