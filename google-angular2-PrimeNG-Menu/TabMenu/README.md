# [TabMenu](https://primeng.org/tabmenu)

```
import {TabMenuModule} from 'primeng/tabmenu';
import {MenuItem} from 'primeng/api';
```

TabMenu 是一個導航組件，將項目顯示爲選項卡標題

![](assets/primeng.org_tabmenu.png)

```
<p-tabMenu [model]="items"></p-tabMenu>
```

```
export class TabMenuDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {label: 'Home', icon: 'pi pi-fw pi-home'},
            {label: 'Calendar', icon: 'pi pi-fw pi-calendar'},
            {label: 'Edit', icon: 'pi pi-fw pi-pencil'},
            {label: 'Documentation', icon: 'pi pi-fw pi-file'},
            {label: 'Settings', icon: 'pi pi-fw pi-cog'}
        ];
    }
}
```

## ActiveItem

默認情況下會突出顯示活動項目，可以使用 **activeItem** 屬性來選擇活動的初始項目

```
<p-tabMenu [model]="items" [activeItem]="activeItem"></p-tabMenu>
```

```
export class TabMenuDemo {

    items: MenuItem[];

    activeItem: MenuItem;

    ngOnInit() {
        this.items = [
            {label: 'Home', icon: 'pi pi-fw pi-home'},
            {label: 'Calendar', icon: 'pi pi-fw pi-calendar'},
            {label: 'Edit', icon: 'pi pi-fw pi-pencil'},
            {label: 'Documentation', icon: 'pi pi-fw pi-file'},
            {label: 'Settings', icon: 'pi pi-fw pi-cog'}
        ];

        this.activeItem = this.items[0];
    }
}
```

## Templating

可以項目模板進行自定義，該模板可以獲取 MenuItem 實例和索引

```
<p-tabMenu [model]="items">
    <ng-template pTemplate="item" let-item let-i="index">
        //item content
    </ng-template>
</p-tabMenu>
```

## Scrollable

```
<p-tabMenu [model]="scrollableItems" [activeItem]="activeItem2"></p-tabMenu>
```

```
export class TabMenuDemo {

    scrollableItems: MenuItem[];

    activeItem2: MenuItem;

    ngOnInit() {
        this.scrollableItems = Array.from({ length: 50 }, (_, i) => ({ label: `Tab ${i + 1}`, icon: `pi pi-fw pi-display` }));
        this.activeItem2 = this.scrollableItems[0];
    }
}
```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| activeItemChange     | item: Newly selected MenuItem     | 活動項目變化時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| activeItem     | MenuItem     | null     | 定義默認的活動項目     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| scrollable     | boolean     | false     | 當啓動時，顯示選項卡標題每一側存在一個按鈕用於滾動列表     |

# Templates

| Name | Parameters |
| -------- | -------- |
| item     | $implicit: Data of the menu item
index: Index of the option     | 

# Styling

| Name | Element |
| -------- | -------- |
| p-tabmenu     | Container element.     |
| p-tabmenu-nav     | List element of headers.     |
| p-tabmenuitem     | Menuitem element.     |
| p-menuitem-link     | Link inside a menuitem.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
