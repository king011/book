# [docker](https://coredns.io/manual/toc/#docker)

CoreDNS 會將每個版本推送到 [https://hub.docker.com/r/coredns/coredns/](https://hub.docker.com/r/coredns/coredns/)

官方沒有對鏡像進行任何說明，好在 coredns 使用簡單，這個鏡像沒有打包 coredns 之外的任何東西包括 SHELL，所以你只能執行 coredns 唯一的配置是將你自己的 Corefile 檔案掛接到容器 /Corefile 以替代默認的 coredns 設定即可

```
services:
  dns:
    image: coredns/coredns:1.10.1
    restart: always
    volumes:
      - ./conf/Corefile:/Corefile:ro
```

```
#info="./conf/Corefile"
.:53 {
	cache
	forward . 8.8.8.8:53 {
	}
}
```

