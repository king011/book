# 映像檔

docker 默認的倉庫 是 [Docker Hub](https://hub.docker.com/) 如果不額外設置 映像檔 都會從此處下載 或上傳到此

# 獲取 映像檔
1. 在倉庫中 搜索需要的 映像檔

	```sh
	sudo docker search ubuntu
	NAME                                                      DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
	ubuntu                                                    Ubuntu is a Debian-based Linux operating s...   8320      [OK]       
	dorowu/ubuntu-desktop-lxde-vnc                            Ubuntu with openssh-server and NoVNC            214                  [OK]
	rastasheep/ubuntu-sshd                                    Dockerized SSH service, built on top of of...   170                  [OK]
	...
	```
	
	| 字段 | 描述 | 
	| -------- | -------- | 
	| NAME     | 映像名     |
	| DESCRIPTION     | 映像描述     |
	| STARS     | 受歡迎程度     |
	| OFFICIAL     | 是否 官方建立     |
	| AUTOMATED     | 是否自動更新 軟體     |

2. 從 倉庫 下載 映像檔

	```sh
	# 下載名爲 ubuntu 的 映像檔
	sudo docker pull ubuntu
	```
	
	> 在 映像名稱 可以加上 :tag 指定版本 如  
	> sudo docker pull ubuntu:16.04
	> 默認使用 tag 爲 latest

# 上傳映像檔
可以使用 push 將 自己的 映像檔 上傳到 服務器
1. 到 [Docker Hub](https://hub.docker.com/) 完成 註冊
2. sudo docker push king/ubuntu:mariadb

# 列出 本機 映像檔
```sh
$ sudo docker images
REPOSITORY       TAG      IMAGE ID      CREATED      VIRTUAL SIZE
ubuntu           12.04    74fe38d11401  4 weeks ago  209.6 MB
ubuntu           precise  74fe38d11401  4 weeks ago  209.6 MB
ubuntu           14.04    99ec81b80c55  4 weeks ago  266 MB
ubuntu           latest   99ec81b80c55  4 weeks ago  266 MB
ubuntu           trusty   99ec81b80c55  4 weeks ago  266 MB
```

| 字段 | 描述 |
| -------- | -------- |
| REPOSITORY     | 來自哪個倉庫 比如 ubuntu     |
| TAG     | 映像檔的標記 比如 14.04     |
| IMAGE ID     | 唯一標識     |
| CREATED     | 建立時間     |
| VIRTUAL SIZE     | 映像檔大小     |

有了 映像檔 之後 就可以用來 啓動一個 容器
```sh
sudo docker run -t -i ubuntu /bin/bash
```
```sh
sudo docker run -t -i ubuntu:14.04 /bin/bash
```
> 同樣 tag 默認爲 latest
 
# 移除本機 映像檔
docker rmi 指令 用於 刪除 映像檔
```sh
sudo docker rmi king/ubuntu:v1
```
> docker rm 用於刪除 容器
> 
> 在刪除 映像檔 前 需要先 刪除所有 依賴此 映像檔 的 容器

配合 awk 可以輕易的刪除所有沒有 tag 的 映像檔
```sh
sudo docker rmi $(docker images | awk  '/^<none>/ { print $3 }')
```
# 映像檔 存儲位置
使用 docker info 可以查看 映像檔 存儲位置 默認爲 **/var/lib/docker**
```
$ docker info | grep "Docker Root Dir"
WARNING: No swap limit support
 Docker Root Dir: /var/lib/docker
```

編輯 **/etc/docker/daemon.json** 添加 graph 屬性可修改 映像檔 存儲位置

```
{
	"graph": "/media/king/虛擬機/docker"
}
```