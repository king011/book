# [mim-height](https://www.primefaces.org/primeflex/minheight)

min-height 指定了元素的最小高度

| Class | Properties |
| -------- | -------- |
| min-h-0     | min-height: 0px;     |
| min-h-full     | min-height: 100%;     |
| min-h-screen     | min-height: 100vh;     |

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="border-round bg-blue-100 w-12rem h-6rem p-3 m-3">
            <div class="min-h-full border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">min-h-full</div>
        </div>
    </div>
</div>
```
# [max-height](https://www.primefaces.org/primeflex/maxheight)

max-height 指定了元素最大高度

| Class | Properties |
| -------- | -------- |
| max-h-0     | max-height: 0px;     |
| max-h-full     | max-height: 100%;     |
| max-h-screen     | max-height: 100vh;     |
| max-h-1rem     | max-height: 1rem;     |
| max-h-2rem     | max-height: 2rem;     |
| ...     | ...     |
| max-h-29rem     | max-height: 29rem;     |
| max-h-30rem     | max-height: 30rem;     |

![](assets/1.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="border-round bg-blue-100 w-12rem h-6rem p-3 m-3">
            <div class="h-30rem max-h-full border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">max-h-full</div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同


