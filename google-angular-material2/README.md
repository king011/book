# material2

material2 是angular2架構下的 一個 開源\(MIT\) material 風格 ui庫 \(由google的angular團隊開發維護\)

官網 [https://material.angular.io/](https://material.angular.io/)  
github [https://github.com/angular/material2](https://github.com/angular/material2)

# 配置環境

1. 安裝 Angular Material 和 Angular CDK

    ```
    #info=false
    npm install --save @angular/material @angular/cdk
    ```

1. Animations
 
 material2 一些組建 使用了 @angular/animations 如果要讓這些動畫運行 需要 安裝 @angular/animations 並在 AppModule 中 導入 BrowserAnimationsModule
		
    ```
    #info=false
    npm install --save @angular/animations
    ```
		
    ```
    import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

    @NgModule({
        ...
        imports: [BrowserAnimationsModule],
        ...
    })
    export class AppModule { }
    ```
		
	如果你不想要這些 動畫 從而獲取更高效 但生硬的 界面 可以導入 NoopAnimationsModule 來替代 BrowserAnimationsModule
		
    ```
    import {NoopAnimationsModule} from '@angular/platform-browser/animations';
     
    @NgModule({
      ...
      imports: [NoopAnimationsModule],
      ...
    })
    export class AppModule { }
    ```
		
1. 導入 MatButtonModule MatCheckboxModule

 在 要使用 material2 的  NgModule中 導入需要的組件 以便可以使用 material2(例子中 imports 了 MatButtonModule MatCheckboxModule )
 
    ```
    import {MatButtonModule, MatCheckboxModule} from '@angular/material';
     
    @NgModule({
      ...
      imports: [MatButtonModule, MatCheckboxModule],
      ...
    })
    export class AppModule { }
    ```
		
1. import material2 theme css
    1. deeppurple-amber
    2. indigo-pink
    3. pink-bluegrey
    4. purple-green
    
    ```
    #info=false
    @import "~@angular/material/prebuilt-themes/indigo-pink.css";
    ```
		
# hammerjs

material2 一些組件(mat-slide-toggle, mat-slider, matTooltip ...)使用了 hammerjs

如果 需要 使用這些組件 需要 安裝 hammerjs 並在 main.ts 中 import hammerjs

```
#info=false
npm install --save hammerjs
```

```
#info=false
import 'hammerjs';
```

# Add Material Icons
如果需要使用 Material Icons 需要 將 如下 css 加入到 index.html 中

```
#info=false
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

淪陷區 可以使用 下面 link 替代
```
#info=false
<link href="https://cdn.bootcss.com/material-design-icons/3.0.1/iconfont/material-icons.min.css" rel="stylesheet">
```