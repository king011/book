# [Asio](https://www.boost.org/doc/libs/1_76_0/doc/html/boost_asio.html)

* Author(s) -> Chris Kohlhoff
* First Release -> 1.35.0
* C++ Standard Minimum Level -> 03
* Categories -> Concurrent Programming, Input/Output

asio 是一個跨平臺的 網路庫 此外得益於其高度的抽象和boost對協程的支持 也可以作爲事件框架 或 協程調度器使用

# io_context

首先 asio 程序 需要一個 i/o 執行上下文 可以是 boost::asio::io\_context boost::asio::thread\_pool boost::asio::system\_context，此上下文表示程式與系統 i/o 服務的鏈接


boost::asio::deadline\_timer 是 asio 提供的一個 定時器 下面例子使用 boost::asio::deadline\_timer 演示如果使用 asio 異步編程 

```
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/exception/diagnostic_information.hpp>

void handler(boost::system::error_code ec, boost::asio::deadline_timer *deadline)
{
    if (ec)
    {
        std::cout << boost::diagnostic_information(ec) << std::flush;
        return;
    }
    std::cout << "async io success" << std::endl;
    if (deadline)
    {
        // 重設定時器
        deadline->expires_from_now(boost::posix_time::seconds(1));
        // 發送新的 異步等待
        deadline->async_wait(boost::bind(handler, boost::placeholders::_1, nullptr));
    }
}
int main(int argc, char *argv[])
{
    // 創建一個執行 上下文 老版把中是 io_service
    boost::asio::io_context ctx(1); // 參數1是預計要創建的線程數量

    // 創建一個定時器
    boost::asio::deadline_timer deadline(
        ctx,
        boost::posix_time::milliseconds(0) //立刻到期
    );
    std::cout << "async io" << std::endl;
    // 異步等待
    deadline.async_wait(boost::bind(handler, boost::placeholders::_1, &deadline));
    std::cout << "context run" << std::endl;
    boost::system::error_code ec;
    ctx.run(ec);
    if (ec)
    {
        std::cout << boost::diagnostic_information(ec) << std::flush;
        return -1;
    }
    return 0;
}
```

1. 第29行 創建了一個 io_context 用在執行上下文
2. 第 23 和 39 行發起了一個 異步請求 當異步完成時 io\_context 會在 run 中 回調傳入的處理器 通知異步結果
3. 第 41 行 調用 io\_context 的 run 在當前線程上執行 io\_context 的異步操作 (如果不傳入 boost::system::error\_code 則出錯時拋出異常 boost::system::system_error)

#  boost::asio::spawn

```
#include <boost/asio/spawn.hpp>
```

```
find_package(Boost
    1.76.0
    REQUIRED
    COMPONENTS 
        coroutine
        thread
)
```
* spawn 函數將 asio 包裝以支持 boost 提供的協程
* spawn 要求一個簽名爲  `void(boost::asio::yield_context)` 的協程函數

```
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind/bind.hpp>
#include <boost/exception/diagnostic_information.hpp>
// 定義一個協程函數
void routine(boost::asio::io_context &ctx, boost::asio::yield_context yield)
{
    puts("routine");
    try
    {
        boost::asio::deadline_timer deadline(
            ctx,
            boost::posix_time::seconds(1));
        puts("wait 1s");
        deadline.async_wait(yield);

        puts("wait 1s");
        deadline.async_wait(yield);
    }
    catch (const boost::system::system_error &e)
    {
        std::cout << boost::diagnostic_information(e) << std::flush;
    }
}
int main(int argc, char *argv[])
{
    // 創建一個執行 上下文 老版把中是 io_service
    boost::asio::io_context ctx(1);
    try
    {
        // spawn 啓動一個 協程
        boost::asio::spawn(ctx, boost::bind(routine, boost::ref(ctx), boost::placeholders::_1));
        puts("context run");
        ctx.run();
        return 0;
    }
    catch (const boost::system::system_error &e)
    {
        std::cout << boost::diagnostic_information(e) << std::flush;
    }
    return -1;
}
```

# Coroutines TS

asio 通過 awaitable 類型模板 use\_awaitable 完成令牌 和  co\_spawn 函數 提供了對 Coroutines TS 的支持

# 實現異步方法
除了使用 boost 提供的各種異步操作 asio 也允許自己實現異步 以支持 boost 未提供的 異步操作

```
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/thread/future.hpp>

// 定義一個 新類型來包裝自己的 異步操作
template <typename Context = boost::asio::io_context>
class my_async_t
{
public:
    /// 與對象關聯的執行器類型
    typedef typename Context::executor_type Executor;
    typedef Executor executor_type;
    explicit my_async_t(Context &ctx)
        : executor_(ctx.get_executor())
    {
    }

    executor_type get_executor() const BOOST_ASIO_NOEXCEPT
    {
        return executor_;
    }

private:
    // 禁止拷貝和賦值
    my_async_t(const my_async_t &) BOOST_ASIO_DELETED;
    my_async_t &operator=(const my_async_t &) BOOST_ASIO_DELETED;
    executor_type executor_;

public:
    // 提供一個 沒有返回值的 異步函數
    template <BOOST_ASIO_COMPLETION_TOKEN_FOR(void(boost::system::error_code))
                  WaitHandler BOOST_ASIO_DEFAULT_COMPLETION_TOKEN_TYPE(boost::asio::any_io_executor)>
    BOOST_ASIO_INITFN_AUTO_RESULT_TYPE(WaitHandler, void(boost::system::error_code))
    async_wait(
        boost::posix_time::time_duration duration,
        BOOST_ASIO_MOVE_ARG(WaitHandler) handler
            BOOST_ASIO_DEFAULT_COMPLETION_TOKEN(boost::asio::any_io_executor))
    {
        return boost::asio::async_initiate<WaitHandler, void(boost::system::error_code)>(
            initiate_async_wait(this, duration), handler);
    }
    // 提供一個 帶返回值的 異步函數
    template <BOOST_ASIO_COMPLETION_TOKEN_FOR(void(boost::system::error_code, int))
                  SquareHandler BOOST_ASIO_DEFAULT_COMPLETION_TOKEN_TYPE(boost::asio::any_io_executor)>
    BOOST_ASIO_INITFN_AUTO_RESULT_TYPE(SquareHandler, void(boost::system::error_code, int))
    async_square(
        int value,
        BOOST_ASIO_MOVE_ARG(SquareHandler) handler
            BOOST_ASIO_DEFAULT_COMPLETION_TOKEN(boost::asio::any_io_executor))
    {
        return boost::asio::async_initiate<SquareHandler, void(boost::system::error_code, int)>(
            initiate_async_square(this, value), handler);
    }

private:
    // 實現 async_wait
    class initiate_async_wait
    {
    private:
        my_async_t *self_;
        boost::posix_time::time_duration duration_;

    public:
        typedef Executor executor_type;
        explicit initiate_async_wait(my_async_t *self, boost::posix_time::time_duration duration)
            : self_(self), duration_(duration)
        {
        }
        executor_type get_executor() const BOOST_ASIO_NOEXCEPT
        {
            return self_->get_executor();
        }

        // 實現異步方法
        template <typename WaitHandler>
        void operator()(BOOST_ASIO_MOVE_ARG(WaitHandler) handler) const
        {
            auto executor = get_executor();
            // 增加 asio 等待完成的事件 計數
            executor.on_work_started();

            // 啓動一個線程模擬異步操作
            boost::asio::detail::non_const_lvalue<WaitHandler> handler2(handler);
            boost::async(boost::launch::async, [handler = std::move(handler2.value), executor, duration = duration_]() mutable
                         {
                             // 模擬異步耗時
                             boost::this_thread::sleep(duration);
                             boost::system::error_code ec;
                             // 使用 boost::asio::dispatch 將完成消息 路由回 asio
                             boost::asio::dispatch(executor, [executor, ec, handler = std::move(handler)]() mutable
                                                   {
                                                       // 調用 處理器 通知 調用者
                                                       handler(ec);
                                                       // 減少 asio 等待完成的事件 計數
                                                       executor.on_work_finished();
                                                   });
                         });
        }
    };
    // 實現 async_square
    class initiate_async_square
    {
    private:
        my_async_t *self_;
        int value_;

    public:
        typedef Executor executor_type;
        explicit initiate_async_square(my_async_t *self, int value)
            : self_(self), value_(value)
        {
        }
        executor_type get_executor() const BOOST_ASIO_NOEXCEPT
        {
            return self_->get_executor();
        }

        // 實現異步方法
        template <typename WaitHandler>
        void operator()(BOOST_ASIO_MOVE_ARG(WaitHandler) handler) const
        {
            auto executor = get_executor();
            // 增加 asio 等待完成的事件 計數
            executor.on_work_started();

            // 啓動一個線程模擬異步操作
            boost::asio::detail::non_const_lvalue<WaitHandler> handler2(handler);
            boost::async(boost::launch::async, [handler = std::move(handler2.value), executor, value = value_]() mutable
                         {
                             // 模擬異步耗時
                             boost::this_thread::sleep(boost::posix_time::milliseconds(150));
                             boost::system::error_code ec;
                             int result = value * value;
                             if (result == 0)
                             {
                                 ec = boost::system::errc::make_error_code(boost::system::errc::not_supported);
                             }

                             // 使用 boost::asio::dispatch 將完成消息 路由回 asio
                             boost::asio::dispatch(executor, [executor, ec, result, handler = std::move(handler)]() mutable
                                                   {
                                                       // 調用 處理器 通知 調用者
                                                       handler(ec, result);
                                                       // 減少 asio 等待完成的事件 計數
                                                       executor.on_work_finished();
                                                   });
                         });
        }
    };
};
int main(int argc, char *argv[])
{
    boost::asio::io_context ctx(1);
    std::cout << "pid: " << boost::this_thread::get_id() << ::std::endl;
    try
    {
        boost::asio::deadline_timer t(ctx);
        my_async_t my(ctx);
        my.async_wait(boost::posix_time::milliseconds(100), [&ctx = ctx, &my = my](boost::system::error_code ec)
                      {
                          std::cout << "async_wait pid: " << boost::this_thread::get_id() << ::std::endl;
                          // 協程
                          boost::asio::spawn(ctx, [&my = my](boost::asio::yield_context yield)
                                             {
                                                 try
                                                 {
                                                     std::cout << "pid: " << boost::this_thread::get_id() << ", wait 100ms" << ::std::endl;

                                                     my.async_wait(boost::posix_time::milliseconds(100), yield);
                                                     std::cout << "pid: " << boost::this_thread::get_id() << ", wait 100ms success" << ::std::endl;

                                                     std::cout << "pid: " << boost::this_thread::get_id() << ", async 5 * 5" << ::std::endl;
                                                     // 異步 並獲取返回值
                                                     auto value = my.async_square(5, yield);
                                                     std::cout << "pid: " << boost::this_thread::get_id() << ", 5 * 5 = " << value << ::std::endl;

                                                     // 傳入 0 將 引發異常
                                                     my.async_square(0, yield);
                                                     puts("never execute");
                                                 }
                                                 catch (const boost::system::system_error &e)
                                                 {
                                                     std::cout << "catch err -> pid: " << boost::this_thread::get_id() << ", " << boost::diagnostic_information(e) << std::flush;
                                                 }
                                             });
                      });
        my.async_square(2, [&ctx = ctx](boost::system::error_code ec, int value)
                        {
                            std::cout << "pid: " << boost::this_thread::get_id() << ", "
                                      << "2 * 2 = " << value << "\n"
                                      << std::flush;
                        });
        puts("context run");
        ctx.run();
        puts("context end");
        return 0;
    }
    catch (const boost::system::system_error &e)
    {
        std::cout << boost::diagnostic_information(e) << std::flush;
    }
    return -1;
}
```

# tcp

```
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/bind/bind.hpp>
#define LISTEN_ADDRESS "0.0.0.0"
#define LISTEN_PORT 9000
#define BUFFER_SIZE (1024 * 32)
typedef boost::asio::io_context io_context_t;
typedef boost::asio::ip::tcp::acceptor acceptor_t;
typedef boost::asio::ip::tcp::socket socket_t;

void accept_routine(boost::asio::io_context &ctx, acceptor_t &acceptor, boost::asio::yield_context yield)
{
    for (;;)
    {
        try
        {
            // 接受一個新連接
            socket_t s(ctx);
            acceptor.async_accept(s, yield);

            // 啓動 socket 協程
            boost::asio::spawn(ctx, [s = boost::move(s)](boost::asio::yield_context yield) mutable
                               {
                                   auto remote = s.remote_endpoint();
                                   std::cout << "one in: " << remote << std::endl;
                                   try
                                   {
                                       char buffer[BUFFER_SIZE];
                                       for (;;)
                                       {
                                           // 讀取
                                           size_t length = s.async_read_some(
                                               boost::asio::buffer(buffer, BUFFER_SIZE),
                                               yield);
                                           // 寫入
                                           s.async_write_some(
                                               boost::asio::buffer(buffer, length),
                                               yield);
                                       }
                                   }
                                   catch (const boost::system::system_error &e)
                                   {
                                       std::cout << boost::diagnostic_information(e) << std::endl;
                                   }
                                   std::cout << "one out: " << remote << std::endl;
                               });
        }
        catch (const std::bad_alloc &e)
        {
            std::cout << e.what() << std::endl;
        }
        catch (const boost::system::system_error &e)
        {
            std::cout << boost::diagnostic_information(e) << std::flush;
        }
    }
}
int main(int argc, char *argv[])
{
    io_context_t ctx(1);
    try
    {
        acceptor_t acceptor(ctx, boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(LISTEN_ADDRESS), LISTEN_PORT));
        // 啓動 accept 協程
        boost::asio::spawn(ctx, boost::bind(accept_routine, boost::ref(ctx), boost::ref(acceptor), boost::placeholders::_1));
        ctx.run();
        return 0;
    }
    catch (const boost::system::system_error &e)
    {
        std::cout << boost::diagnostic_information(e) << std::flush;
    }
    return -1;
}
```