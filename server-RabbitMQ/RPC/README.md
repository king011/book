# [RPC](https://www.rabbitmq.com/tutorials/tutorial-six-go.html)

rpc 的本質是 客戶端 發送一個 請求給服務器 服務器返回一個處理 響應

利用 RabbitMQ 可以 實現此機制

當前 AMQP 0-9-1 協議 爲消息提供了 14個屬性 通常只需要添幾個

* persistent 是否要持久化消息
* content_type 編碼mime類型 通常使用 application/json 是個不錯的主意
* reply_to 指示用於回調的命名隊列
* correlation_id rpc 請求 id

要實現 rpc 只需要 
1. 客戶端 設置 reply_to correlation_id 後 request 推送到 RabbitMQ
2. 服務器 從 RabbitMQ 獲取 request 處理完成後 將 response 推送到 reply_to 指定的隊列即可


```
#info="server"

package main

import (
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `topic`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	startService(c)
}
func startService(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()

	// 聲明一個 隊列 接收 rpc 請求
	q, e := channel.QueueDeclare(
		`rpc`, // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		true,  // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		false, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	for msg := range ch {
		serviceFibonacci(channel, &msg)
	}
}
func serviceFibonacci(channel *amqp.Channel, msg *amqp.Delivery) {
	var ok bool
	defer func() {
		if ok {
			msg.Ack(false)
		} else {
			msg.Reject(false)
		}
	}()
	args, e := strconv.Atoi(string(msg.Body))
	if e != nil {
		e = channel.Publish(
			``,
			msg.ReplyTo, // 指定 routing key
			false,
			false,
			amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: msg.CorrelationId,
				Body:          []byte(e.Error()), //消息內容
			},
		)
		if e == nil {
			ok = true
		} else {
			log.Println(e)
		}
		return
	}
	if args < 0 {
		args = 0
	}
	result := fibonacci(args)
	e = channel.Publish(
		``,
		msg.ReplyTo, // 指定 routing key
		false,
		false,
		amqp.Publishing{
			ContentType:   "text/plain",
			CorrelationId: msg.CorrelationId,
			Body:          []byte(strconv.Itoa(result)), //消息內容
		},
	)
	if e == nil {
		ok = true
	} else {
		log.Println(e)
	}
}
func fibonacci(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fibonacci(n-1) + fibonacci(n-2)
	}
}
```

```
#info="client"

package main

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"sync"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 執行遠程調用
	var wait sync.WaitGroup
	args := []int{10, 10, 20}
	results := make([]struct {
		Index  int
		Result string
		Error  error
	}, len(args))
	wait.Add(len(args))
	for i := 0; i < len(args); i++ {
		go func(i int) {
			results[i].Index = i + 1
			results[i].Result, results[i].Error = callFibonacci(c, strconv.Itoa(results[i].Index), args[i])
			wait.Done()
		}(i)
	}
	wait.Wait()
	for _, result := range results {
		fmt.Println(result)
	}
}

func callFibonacci(c *amqp.Connection, id string, arg int) (result string, e error) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()

	// 聲明一個 隊列 接收 rpc 請求
	q, e := channel.QueueDeclare(
		`rpc`, // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		true,  // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 聲明一個 排他隊列 接收 rpc 響應
	q, e = channel.QueueDeclare(
		``,    // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		true,  // 是否在沒人使用時 自動刪除
		true,  // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}
	// 向 rpc 隊列 推送一個 請求
	e = channel.Publish(
		``,
		`rpc`, // 指定 routing key
		false,
		false,
		amqp.Publishing{
			ContentType:   "text/plain",
			ReplyTo:       q.Name, // 設置 響應回調 目標
			CorrelationId: id,
			Body:          []byte(strconv.Itoa(arg)), //消息內容
		},
	)
	log.Println("request", arg)
	if e != nil {
		log.Println(e)
		return
	}

	// 創建一個 消費者 獲取 響應
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		true, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		return
	}
	for msg := range ch {
		if msg.CorrelationId != id {
			continue
		}
		result = (string)(msg.Body)
		return
	}
	e = errors.New("server exit")
	return
}
```