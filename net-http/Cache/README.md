# Expires

HTTP Response Header 可以加上一個 Expires 字段指定 資源 過期時間

```
Expires: Wed, 21 Oct 2017 07:28:00 GMT
```

瀏覽器 會在 資源過期前都使用 緩存 但瀏覽器會依據 本地時間 判斷資源是否過期

# Cache-Control

Cache-Control 終端可以設置 數據有效秒數

如下指定 數據 30秒後過期 瀏覽器會在收到數據後 緩存 30秒
```
Cache-Control: max-age=30
```

max-age=0 的規範做法是 no-cache
```
Cache-Control: no-cache
```

> no-cache 告訴瀏覽器 不要使用緩存 應該向服務器 發送請求
>
> max-age=0 告訴服務器 資源立刻過期 一些情況下 瀏覽器可能會繼續使用 緩存


no-store 指示不要使用緩存 並且也不想要 Last-Modified/If-Modified-Since 和 Etag/If-None-Match 每次都下載完成的新資料

```
Cache-Control: no-store
```