# Node

k8s 通過將容器放入在 Node 上運行的 Pod 中來執行工作負載。節點可以是一個虛擬機或物理機器。每個節點包含運行 Pods 所需要的服務，這些服務 由 Control Plane 負責管理

Node 上的組件包括
* kubelet
* 容器
* kube-proxy