# [URL](https://developer.mozilla.org/en-US/docs/Web/API/URL_API)

現代化的瀏覽器都提供了一個 class URL,它用於解析 構造 規範化 編碼 URL。使用它可以輕鬆的讀取或修改 URL

一個 url 的表現形式通常是

```
[scheme:][//[userinfo@]host][/]path[?query][#fragment]
```

在 scheme 後不以 / 開頭的 URL 形式是

```
scheme:opaque[?query][#fragment]
```

```
#info="new URL('http://king:123@localhost:80/api?id=1&lv=2#docs')"
interface URL {
    // http://king:123@localhost/api?id=1&amp;lv=2#docs
    // 完整的 url 字符串，就是瀏覽器地址裏面的值
    href: string;
    // 返回 href
    toString(): string;
		
    // http:
    // 協議:
    protocol: string;
		
    // localhost
    // 域名[:端口] 如果是默認端口則會省略
    host: string;
    // localhost
    // 域名 不會帶有端口
    hostname: string;
		
    // 端口 如果是默認端口則爲空字符串
    port: string;

    // http://localhost
    // `${protocol}//${host}`
    readonly origin: string;
		
    // /api
    // 路徑
    pathname: string;

    // ?id=1&amp;lv=2
    // 查詢字符串 ? 符號
    search: string;
    // 與 search 屬性關聯的一個 URLSearchParams 可以方便的 讀取/修改 查詢字符串
    readonly searchParams: URLSearchParams;


    // #docs
    // fragment 部分包括了 # 符號
    hash: string;

    // king		
    // 用戶名
    username: string;
    // 123
    // 密碼
    password: string;
}
```

url 的屬性都是 setter 你可以傳入未轉義的字符串，setter 會自動爲你轉義，但 getter 沒有調用轉義所以你需要自己調用轉義函數

```
decodeURI(u.pathname)
decodeURIComponent(u.XXX)
```

* 除了 pathname 應該調用 decodeURI 解碼，其它可能轉義屬性都應該調用 decodeURIComponent 解碼
* 如果將 pathname 設置爲空字符串， setter 會將其設置爲 **/**(所以獲取的 pathname 永遠不會是空字符串)


# [URLSearchParams](https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams)

class URLSearchParams 用來處理查詢參數

```
// 從字符串構造
// 如果字符串第一個字符是 ? 則會忽略第一個字符串
// 只會回來第一個字符串所以 ??id=1 會認爲查詢參數名是 ?id
const u = new URLSearchParams("lv=1&lv=2&name=測試")

// 得到轉義後的查詢字符串不包括 ?
console.log(u.toString())

// 由 Record<string, string> 構造
new URLSearchParams({
    lv: "1",
    name: "測試"
})
// 有二維數組構造
new URLSearchParams([
    ["lv", "1"],
    ["name", "測試"]
])

// 遍歷參數
for (const [k, v] of u) {
    console.log(`${k}=${v}`)
}
```

此外還提供一些其它好用的方法

```
interface URLSearchParams {
    [Symbol.iterator](): IterableIterator<[string, string]>;
    /** Returns an array of key, value pairs for every entry in the search params. */
    entries(): IterableIterator<[string, string]>;
    /** Returns a list of keys in the search params. */
    keys(): IterableIterator<string>;
    /** Returns a list of values in the search params. */
    values(): IterableIterator<string>;

    /** Appends a specified key/value pair as a new search parameter. */
    append(name: string, value: string): void;
    /** Deletes the given search parameter, and its associated value, from the list of all search parameters. */
    delete(name: string): void;
    /** Returns the first value associated to the given search parameter. */
    get(name: string): string | null;
    /** Returns all the values association with a given search parameter. */
    getAll(name: string): string[];
    /** Returns a Boolean indicating if such a search parameter exists. */
    has(name: string): boolean;
    /** Sets the value associated to a given search parameter to the given value. If there were several values, delete the others. */
    set(name: string, value: string): void;
    sort(): void;
}
```

# [URLPattern](https://developer.mozilla.org/en-US/docs/Web/API/URL_Pattern_API)

class URLPattern 提供了一種對 URL 進行模式匹配的接口，目前(2022-11-29)這依然是實驗性的特性，大部分瀏覽器都還不支持(chrome 95 開始支持)故不推薦在瀏覽器環境下使用

然而 deno 支持了 URLPattern 如果使用 deno 寫 http 服務器的路由器則 URLPattern 可能是一個不錯的選擇

pattern syntax 是基於 [path-to-regexp](https://github.com/pillarjs/path-to-regexp) 庫的。Patterns可以包括

* 完全匹配的文字字符串
* **/posts/\*** 匹配任意文字的通配符 
* **/books/:id** 提取部分 URL 的命名組
* **/books{/old}?** 使部分模式可選或多次匹配
* **/books/\(\\\\d\+\)** 進行任意複雜的正則匹配，但存在一些限制

## 固定文本和捕獲組

每個 pattern 可以包括固定文本和組。

* 固定文本是精準匹配的字符序列
* 組根據匹配規則匹配任意字符串

每個 URL 部分都有其自己的默認規則，如下所述，但它們可以被覆蓋

```
// 匹配一些固定的文本
const pattern = new URLPattern({ pathname: "/books" });
console.log(pattern.test("https://example.com/books")); // true
console.log(pattern.exec("https://example.com/books")!.pathname.groups); // {}
```

```
// 匹配一個命名的組
const pattern = new URLPattern({ pathname: "/books/:id" });
console.log(pattern.test("https://example.com/books/123")); // true
console.log(pattern.exec("https://example.com/books/123")!.pathname.groups); // { id: '123' }
```

## 段通配符

默認情況下
* 匹配 URL.pathname  部分的組將匹配除 **/** 之外的所有字符
* 匹配 URL.hostname 部分的組將匹配除 **.** 之外的所有字符
* 在所有其它部分，組將匹配所有字符

段通配符是非貪婪的，這意味著它將匹配最短的字符

## 正則匹配表達式

你可以爲每個組使用正則表達式，而不是對組使用默認的匹配規則。此正則表達式定義組的匹配規則

下面是命名組上的正則表達式匹配示例，它限制該組僅在包含一個或多個數字時才匹配：

```
const pattern = new URLPattern("/books/:id(\\d+)", "https://example.com");
console.log(pattern.test("https://example.com/books/123")); // true
console.log(pattern.test("https://example.com/books/abc")); // false
console.log(pattern.test("https://example.com/books/")); // false
```

### 正則表達式的限制

一些正則表達式不會如預期的方式工作

#### startWith ^

^ 只在匹配 URL.protocol 時有效，且即時被使用也會忽略掉 ^ 

* 但是我在 deno 1.28.2 (release, x86_64-unknown-linux-gnu) 中測試這個符號在 URL.hash URL.search 中和 URL.protocol 中一樣
* 最好不要使用這個符號

```
// with `^` in pathname
const pattern = new URLPattern({ pathname: '(^b)' });
console.log(pattern.test('https://example.com/ba')); // false
console.log(pattern.test('https://example.com/xa')); // false
```

```
// with `^` in protocol
const pattern = new URLPattern({ protocol: '(^https?)' });
console.log(pattern.test('https://example.com/index.html')); // true
console.log(pattern.test('xhttps://example.com/index.html')); // false
```

```
// without `^` in protocol
const pattern = new URLPattern({ protocol: '(https?)' });
console.log(pattern.test('https://example.com/index.html')); // true
console.log(pattern.test('xhttps://example.com/index.html')); // false
```

#### endWith $

$ 只在匹配 URL.hash 時有效，且即時被使用也會忽略掉$ 最好不要使用這個符號

```
// with `$` in pathname
const pattern = new URLPattern({ pathname: "(path$)" });
console.log(pattern.test("https://example.com/path")); // false
console.log(pattern.test("https://example.com/other")); // false
```

```
// with `$` in protocol
const pattern = new URLPattern({ hash: '(hash$)' });
console.log(pattern.test('https://example.com/#hash')); // true
console.log(pattern.test('xhttps://example.com/#otherhash')); // false
```

```
// without `$` in protocol
const pattern = new URLPattern({ hash: '(hash)' });
console.log(pattern.test('https://example.com/#hash')); // true
console.log(pattern.test('xhttps://example.com/#otherhash')); // false
```

#### Lookaheads Lookbehinds

前瞻和後視 永遠不會匹配 URL 的任何部分

```
// lookahead
const pattern = new URLPattern({ pathname: '(a(?=b))' });
console.log(pattern.test('https://example.com/ab')); // false
console.log(pattern.test('https://example.com/ax')); // false
```

```
// negative-lookahead
const pattern = new URLPattern({ pathname: '(a(?!b))' });
console.log(pattern.test('https://example.com/ab')); // false
console.log(pattern.test('https://example.com/ax')); // false
```

```
// lookbehind
const pattern = new URLPattern({ pathname: '((?<=b)a)' });
console.log(pattern.test('https://example.com/ba')); // false
console.log(pattern.test('https://example.com/xa')); // false
```

```
// negative-lookbehind
const pattern = new URLPattern({ pathname: '((?<!b)a)' });
console.log(pattern.test('https://example.com/ba')); // false
console.log(pattern.test('https://example.com/xa')); // false
```
#### ()

\(\) 需要被轉義，即時它們不在正則表達式中

```
new URLPattern({ pathname: '([()])' }); // throws
new URLPattern({ pathname: '([\\(\\)])' }); // ok

new RegExp('[()]'); // ok
new RegExp('[\\(\\)]'); // ok
```

## 組

組可以命名也可以匿名。命名組是通過在組名前加上 **:** 來指定的。沒有以冒號和名稱爲前瞻的正則表達式組是未命名的。未命名的組根據它們在 pattern 中的順序在匹配結果中進行數字索引

```
// A named group
const pattern = new URLPattern("/books/:id(\\d+)", "https://example.com");
console.log(pattern.exec("https://example.com/books/123")!.pathname.groups); // { id: '123' }
```

```
// An unnamed group
const pattern = new URLPattern("/books/(\\d+)", "https://example.com");
console.log(pattern.exec("https://example.com/books/123")!.pathname.groups); // { '0': '123' }
```

### 組修飾符

組也可以有修飾符。這些在組名之後指定(或者在正則表達式之後，如果有的話)。


| 修飾符 | 含義 |
| -------- | -------- |
| ?     | 組是可選的     |
| +     | 組重複一次或多次     |
| *     | 組重複0次或多次     |

```
// An optional group
const pattern = new URLPattern("/books/:id?", "https://example.com");
console.log(pattern.test("https://example.com/books/123")); // true
console.log(pattern.test("https://example.com/books")); // true
console.log(pattern.test("https://example.com/books/")); // false
console.log(pattern.test("https://example.com/books/123/456")); // false
console.log(pattern.test("https://example.com/books/123/456/789")); // false
```

```
// A repeating group with a minimum of one
const pattern = new URLPattern("/books/:id+", "https://example.com");
console.log(pattern.test("https://example.com/books/123")); // true
console.log(pattern.test("https://example.com/books")); // false
console.log(pattern.test("https://example.com/books/")); // false
console.log(pattern.test("https://example.com/books/123/456")); // true
console.log(pattern.test("https://example.com/books/123/456/789")); // true
```

```
// A repeating group with a minimum of zero
const pattern = new URLPattern("/books/:id*", "https://example.com");
console.log(pattern.test("https://example.com/books/123")); // true
console.log(pattern.test("https://example.com/books")); // true
console.log(pattern.test("https://example.com/books/")); // false
console.log(pattern.test("https://example.com/books/123/456")); // true
console.log(pattern.test("https://example.com/books/123/456/789")); // true
```

### 組定界符

pattern 還可以包含定界符。這是有 **{}** 包圍的模式片段。這些定界符不會像捕獲組那樣在匹配結果中被捕獲，但仍然可以像組一樣對齊應用修飾符。如果組定界符未被修飾符修改，則它們將被視爲父模式的一部分。

組的定界符不能包含其它定界符，但可以包含其它任意模式(捕獲組 正則表達式 通配符 固定文本)

```
// A group delimiter with a ? (optional) modifier
const pattern = new URLPattern('/book{s}?', 'https://example.com');
console.log(pattern.test('https://example.com/books')); // true
console.log(pattern.test('https://example.com/book')); // true
console.log(pattern.exec('https://example.com/books').pathname.groups); // {}
```

```
// A group delimiter without a modifier
const pattern = new URLPattern("/book{s}", "https://example.com");
console.log(pattern.pathname); // /books
console.log(pattern.test("https://example.com/books")); // true
console.log(pattern.test("https://example.com/book")); // false
```

```
// A group delimiter containing a capturing group
const pattern = new URLPattern({ pathname: '/blog/:id(\\d+){-:title}?' });
console.log(pattern.test('https://example.com/blog/123-my-blog')); // true
console.log(pattern.test('https://example.com/blog/123')); // true
console.log(pattern.test('https://example.com/blog/my-blog')); // false
```

### URL.pathname 自動前綴

在 URL.pathname 部分的匹配模式中，如果組定義前面有 **/** 則組會自動添加一個 **/** 前綴。這對於帶有修飾符的組很有用，因爲它允許重複組按預期工作

如果你不想自動添加前綴，你可以通過在組周圍加上定界符**{}** 來禁用它。組定界符沒有自動前綴的行爲

```
// A pattern with an optional group, preceded by a slash
const pattern = new URLPattern('/books/:id?', 'https://example.com');
console.log(pattern.test('https://example.com/books/123')); // true
console.log(pattern.test('https://example.com/books')); // true
console.log(pattern.test('https://example.com/books/')); // false
```

```
// A pattern with a repeating group, preceded by a slash
const pattern = new URLPattern('/books/:id+', 'https://example.com');
console.log(pattern.test('https://example.com/books/123')); // true
console.log(pattern.test('https://example.com/books/123/456')); // true
console.log(pattern.test('https://example.com/books/123/')); // false
console.log(pattern.test('https://example.com/books/123/456/')); // false
```

```
// Segment prefixing does not occur outside of pathname patterns
const pattern = new URLPattern({ hash: '/books/:id?' });
console.log(pattern.test('https://example.com#/books/123')); // true
console.log(pattern.test('https://example.com#/books')); // false
console.log(pattern.test('https://example.com#/books/')); // true
```

```
// Disabling segment prefixing for a group using a group delimiter
const pattern = new URLPattern({ pathname: '/books/{:id}?' });
console.log(pattern.test('https://example.com/books/123')); // true
console.log(pattern.test('https://example.com/books')); // false
console.log(pattern.test('https://example.com/books/')); // true
```

### 通配符

通配符 **\*** 是一個爲命名捕獲組的簡寫，它匹配所有字符0次或多次。你可以將其放置在模式的任意位置。通配符是貪心的，這意味著它將匹配最長的字符串

```
// A wildcard at the end of a pattern
const pattern = new URLPattern("/books/*", "https://example.com");
console.log(pattern.test("https://example.com/books/123")); // true
console.log(pattern.test("https://example.com/books")); // false
console.log(pattern.test("https://example.com/books/")); // true
console.log(pattern.test("https://example.com/books/123/456")); // true
```

```
// A wildcard in the middle of a pattern
const pattern = new URLPattern('/*.png', 'https://example.com');
console.log(pattern.test('https://example.com/image.png')); // true
console.log(pattern.test('https://example.com/image.png/123')); // false
console.log(pattern.test('https://example.com/folder/image.png')); // true
console.log(pattern.test('https://example.com/.png')); // true
```

### 模式規範化

解析模式時，它會自動規範化形式。例如

* unicode 字符在 pathname中使用 % 編碼，在 hostname 中使用 punycode 編碼省略默認端口號
* 像 /foo/./bar 這樣的路徑被最短化爲等效的 /foo/bar
* 一些模式表示可以解析爲相同的潛在含義，例如 foo 和 {foo} 這種情況被規範爲最簡單的形式 foo

## Example

### 過濾特定 URL component

以下例子顯示了 URLPattern 如何過濾特定的 URL component。當使用組件模式的結構化對象構造  URLPattern 時，任何缺少的組件默認爲使用 **\*** 通配符匹配

```
// Construct a URLPattern that matches a specific domain and its subdomains.
// All other URL components default to the wildcard `*` pattern.
const pattern = new URLPattern({
  hostname: "{*.}?example.com",
});

console.log(pattern.hostname); // '{*.}?example.com'

console.log(pattern.protocol); // '*'
console.log(pattern.username); // '*'
console.log(pattern.password); // '*'
console.log(pattern.pathname); // '*'
console.log(pattern.search); // '*'
console.log(pattern.hash); // '*'

console.log(pattern.test("https://example.com/foo/bar")); // true

console.log(pattern.test({ hostname: "cdn.example.com" })); // true

console.log(pattern.test("custom-protocol://example.com/other/path?q=1")); // true

// Prints `false` because the hostname component does not match
console.log(pattern.test("https://cdn-example.com/foo/bar"));
```

### 從完整 URL 構造 URLPattern

以下示例顯示如何從具有嵌入模式的完整 URL 字符串構造 URLPattern。

例如 a: 即可以是 URL.protocol 前綴例如 https:，也可以是命名模式組的開頭例如 :foo。如果字符是 URL 語法的一部分還是模式語法的一部分之間沒有歧義，它就可以正常工作。(通常本喵不建議使用 完整 URL 定義 URLPattern)

```
// Construct a URLPattern that matches URLs to CDN servers loading jpg images.
// URL components not explicitly specified, like search and hash here, result
// in the empty string similar to the URL() constructor.
const pattern = new URLPattern("https://cdn-*.example.com/*.jpg");

console.log(pattern.protocol); // 'https'

console.log(pattern.hostname); // 'cdn-*.example.com'

console.log(pattern.pathname); // '/*.jpg'

console.log(pattern.username); // ''
console.log(pattern.password); // ''
console.log(pattern.search); // ''
console.log(pattern.hash); // ''

// Prints `true`
console.log(
  pattern.test("https://cdn-1234.example.com/product/assets/hero.jpg"),
);

// Prints `false` because the search component does not match
console.log(
  pattern.test("https://cdn-1234.example.com/product/assets/hero.jpg?q=1"),
);
```

### 使用不明確的 URL 字符串構造 URLPattern

以下示例顯示了使用不明確字符串構造 URLPattern 

在下述情況下 **:** 字符可以是 protocol 組件後綴 也可以是 pattern 中命名組件前綴。構造函數選擇將其視爲模式的一部分，因此確定這是一個相對 pathname 模式。由於沒有 base URL 因此無法解析相對 pathname 故將拋出異常

```
// Throws because this is interpreted as a single relative pathname pattern
// with a ":foo" named group and there is no base URL.
const pattern = new URLPattern("data:foo*");
```

### 轉義字符

以下示例顯示如何轉義不明確的構造函數字符串以將其視爲 URL 分隔符而不是模式字符. 這裏 **:** 被轉義爲 **\\:**

```
// Constructs a URLPattern treating the `:` as the protocol suffix.
const pattern = new URLPattern('data\\:foo*');

console.log(pattern.protocol); // 'data'

console.log(pattern.pathname); // 'foo*'

console.log(pattern.username); // ''
console.log(pattern.password); // ''
console.log(pattern.hostname); // ''
console.log(pattern.port); // ''
console.log(pattern.search); // ''
console.log(pattern.hash); // ''

console.log(pattern.test('data:foobar')); // true
```

### 在 test exec 中使用 base url

你可以在 test 和 exec 函數中傳入爲要匹配的 url 指定 base url

```
const pattern = new URLPattern({ hostname: "example.com", pathname: "/foo/*" });

// Prints `true` as the hostname based in the dictionary `baseURL` property
// matches.
console.log(pattern.test({
  pathname: "/foo/bar",
  baseURL: "https://example.com/baz",
}));

// Prints `true` as the hostname in the second argument base URL matches.
console.log(pattern.test("/foo/bar", "https://example.com/baz"));

// Throws because the second argument cannot be passed with a dictionary input.
try {
  pattern.test({ pathname: "/foo/bar" }, "https://example.com/baz");
} catch (e) {}

// The `exec()` method takes the same arguments as `test()`.
const result = pattern.exec("/foo/bar", "https://example.com/baz")!;

console.log(result.pathname.input); // '/foo/bar'

console.log(result.pathname.groups[0]); // 'bar'

console.log(result.hostname.input); // 'example.com'
```

### 在 URLPattern 構造函數中使用 base url

你也可在 URLPattern 的構造函數中指定 base url, 主要注意的是在這種情況下 base url 被視爲嚴格的 URL 而不能包含任何 pattern 語法。

此外由於 base url 爲每個 URL.XXX 屬性都提供了一個值因此生成的 URLPattern 也將爲每個組件提供一個值，即使它是空字符串，這意味着你不會獲得 默認爲通配符 的行爲

```
const pattern1 = new URLPattern({ pathname: '/foo/*',
                                  baseURL: 'https://example.com' });

console.log(pattern1.protocol); // 'https'
console.log(pattern1.hostname); // 'example.com'
console.log(pattern1.pathname); // '/foo/*'

console.log(pattern1.username); // ''
console.log(pattern1.password); // ''
console.log(pattern1.port); // ''
console.log(pattern1.search); // ''
console.log(pattern1.hash); // ''

// Equivalent to pattern1
const pattern2 = new URLPattern('/foo/*', 'https://example.com');

// Throws because a relative constructor string must have a base URL to resolve
// against.
try {
  const pattern3 = new URLPattern('/foo/*');
} catch (e) {}
```

### 訪問匹配的值

以下示例顯示了如何訪問 exec 所匹配到的值

```
#info="匿名組"
const pattern = new URLPattern({ hostname: '*.example.com' });
const result = pattern.exec({ hostname: 'cdn.example.com' });

console.log(result.hostname.groups[0]); // 'cdn'

console.log(result.hostname.input); // 'cdn.example.com'

console.log(result.inputs); // [{ hostname: 'cdn.example.com' }]
```

```
#info="命名組"
// Construct a URLPattern using matching groups with custom names. These
// names can then be later used to access the matched values in the result
// object.
const pattern = new URLPattern({ pathname: '/:product/:user/:action' });
const result = pattern.exec({ pathname: '/store/wanderview/view' });

console.log(result.pathname.groups.product); // 'store'
console.log(result.pathname.groups.user); // 'wanderview'
console.log(result.pathname.groups.action); // 'view'

console.log(result.pathname.input); // '/store/wanderview/view'

console.log(result.inputs); // [{ pathname: '/store/wanderview/view' }]
```


### 使用正則表達式匹配

```
#info="匿名組"
const pattern = new URLPattern({ pathname: '/(foo|bar)' });

console.log(pattern.test({ pathname: '/foo' })); // true
console.log(pattern.test({ pathname: '/bar' })); // true
console.log(pattern.test({ pathname: '/baz' })); // false

const result = pattern.exec({ pathname: '/foo' });

console.log(result.pathname.groups[0]); // 'foo'
```

```
#info="命名組"
const pattern = new URLPattern({ pathname: '/:type(foo|bar)' });
const result = pattern.exec({ pathname: '/foo' });

console.log(result.pathname.groups.type); // 'foo'
```

### 使組可選

組修飾符 **?** 可以使用組變得可選，並且對於 pathname 也會使用組前的 **/** 變成可選的


```
const pattern = new URLPattern({ pathname: '/product/(index.html)?' });

console.log(pattern.test({ pathname: '/product/index.html' })); // true
console.log(pattern.test({ pathname: '/product' })); // true

const pattern2 = new URLPattern({ pathname: '/product/:action?' });

console.log(pattern2.test({ pathname: '/product/view' })); // true
console.log(pattern2.test({ pathname: '/product' })); // true

// Wildcards can be made optional as well. This may not seem to make sense
// since they already match the empty string, but it also makes the prefix
// `/` optional in a pathname pattern.
const pattern3 = new URLPattern({ pathname: '/product/*?' });

console.log(pattern3.test({ pathname: '/product/wanderview/view' })); // true
console.log(pattern3.test({ pathname: '/product' })); // true
console.log(pattern3.test({ pathname: '/product/' })); // true
```

### 使組重複

組修飾符 **+** 可以使用組變得可重複，並且對於 pathname 也會使用組前的 **/** 一起納入重複

```
const pattern = new URLPattern({ pathname: '/product/:action+' });
const result = pattern.exec({ pathname: '/product/do/some/thing/cool' });

result.pathname.groups.action; // 'do/some/thing/cool'

console.log(pattern.test({ pathname: '/product' })); // false
```

### 使用組可選或可重複

組修飾符 **\*** 可以使用組變得即可選又可重複相當於同時有 **?** 和 **+** 的特性

```
const pattern = new URLPattern({ pathname: '/product/:action*' });
const result = pattern.exec({ pathname: '/product/do/some/thing/cool' });

console.log(result.pathname.groups.action); // 'do/some/thing/cool'

console.log(pattern.test({ pathname: '/product' })); // true
```

### 爲 ?\+\* 修飾符使用自定義前綴或後綴

以下示例顯示如何使用 **{}** 來表示自定義 前綴/後綴 以後後續的 ?\+\* 修飾符操作

```
const pattern = new URLPattern({ hostname: '{:subdomain.}*example.com' });

console.log(pattern.test({ hostname: 'example.com' })); // true
console.log(pattern.test({ hostname: 'foo.bar.example.com' })); // true
console.log(pattern.test({ hostname: '.example.com' })); // false

const result = pattern.exec({ hostname: 'foo.bar.example.com' });

console.log(result.hostname.groups.subdomain); // 'foo.bar'
```

### 在沒有匹配模式的情況下使用文本可選或可重複

以下示例顯示如何在不使用匹配組的情況下使用 **{}** 將固定文本值表示爲可選或可重複

```
const pattern = new URLPattern({ pathname: '/product{/}?' });

console.log(pattern.test({ pathname: '/product' })); // true
console.log(pattern.test({ pathname: '/product/' })); // true

const result = pattern.exec({ pathname: '/product/' });

console.log(result.pathname.groups); // {}
```

### 使用多個組件和功能

以下示例顯示了可以同時使用多個 URL組件 和 組

```
const pattern = new URLPattern({
  protocol: 'http{s}?',
  username: ':user?',
  password: ':pass?',
  hostname: '{:subdomain.}*example.com',
  pathname: '/product/:action*',
});

const result = pattern.exec(
  'http://foo:bar@sub.example.com/product/view?q=12345',
);

console.log(result.username.groups.user); // 'foo'
console.log(result.password.groups.pass); // 'bar'
console.log(result.hostname.groups.subdomain); // 'sub'
console.log(result.pathname.groups.action); // 'view'
```