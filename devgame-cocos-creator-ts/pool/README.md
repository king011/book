# [對象池](http://docs.cocos.com/creator/manual/zh/scripting/pooling.html)

遊戲 往往有些 節點要 不斷的 創建(cc.instantiate) 銷毀(node.destroy) 這是相當 耗費性能的 creator 爲此 提供了 [cc.NodePool](http://docs.cocos.com/creator/api/zh/classes/NodePool.html)

通常 在 場景 onLoad 初始化 NodePool 並且創建 足夠的 Node 之後 從 NodePool 中 取出 對象 加入到場景

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // creator 定義 對象池 預設資源
    @property(cc.Prefab)
    readonly prefab: cc.Prefab = null
    // creator 定義 對象池 大小
    @property
    readonly size: number = 5

    // 對象池
    private _pool: cc.NodePool = null
    // 場景 怪物數量
    private _monster: Array<cc.Node> = null
    // LIFE-CYCLE CALLBACKS:

    get monster(): Array<cc.Node> {
        if (this._monster == null) {
            this._monster = new Array<cc.Node>()
        }
        return this._monster
    }
    onLoad() {
        if (this.size < 1) {
            throw Error("size < 1")
        }
        if (this.prefab == null) {
            throw Error("prefab null")
        }
        // 創建對象池
        const pool = new cc.NodePool()
        for (let i = 0; i < this.size; i++) {
            // 預創建的對象
            pool.put(cc.instantiate(this.prefab))
        }
        this._pool = pool

        // 啓動一個 定時器 模擬 對象創建 釋放
        this.schedule(() => {
            if (this.monster.length < 2) {
                this.create()
                return
            } else if (this.monster.length > this.size * 3 / 2) {
                this.kill()
                return
            }
            if (Math.floor(Math.random() * 10) % 2 == 0) {
                this.create()
            } else {
                this.kill()
            }
        }, 1 / 2)
    }
    private create() {
        // 從 內池池 創建節點
        let node: cc.Node
        if (this._pool.size() == 0) {
            node = cc.instantiate(this.prefab)
        } else {
            // 如果 對象池 爲空 會返回 null
            node = this._pool.get()
        }
        // 添加到 場景
        this.monster.push(node)
        node.setPosition(this.randomXY())
        cc.director.getScene().addChild(node)
    }
    private kill() {
        const node = this.monster.pop()
        if (this._pool.size() == this.size) {
            // 移除節點 並且 將其歸還到 對象池
            this._pool.put(node)
        } else {
            // 移除節點
            node.parent = null
        }
    }
    private randomXY(): cc.Vec2 {
        return new cc.Vec2(Math.floor(Math.random() * 800), Math.floor(Math.random() * 640))
    }
    // update (dt) {}
}
```
> 如果需要手動 清空 對象池 調用 clear 方法 即可
> 


