# ubuntu

## 安裝

```
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
```


```
youtube-dl [OPTIONS] URL [URL...]
```

```
youtube-dl --version
```

## 更新

```
sudo youtube-dl -U
```

# 配置

你可以創建配置檔案來爲 youtube-dl 設置默認選項，對於 linux 來說全局設定在 **/etc/youtube-dl.conf** 用戶設定在 **~/.config/youtube-dl/config**

下面是一個配置示例

```
#info="~/.config/youtube-dl/config"

# 總是 提取音頻
-x

# 不要使用 Last-modified  設置到下載檔案的 mtime
--no-mtime

# 使用代理
# --proxy 127.0.0.1:8118
--proxy socks5://127.0.0.1:1080

# 將視頻保存到 youtube 檔案夾下
-o ~/youtube/%(title)s.%(ext)s
```

