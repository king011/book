# 安裝

```
go get github.com/boltdb/bolt/...
```

# 打開數據庫

```
package main
 
import (
	"github.com/boltdb/bolt"
	"log"
)
 
func main() {
	// 如果 不存在檔案 則 創建數據庫
	// 否則 打開數據庫
	db, e := bolt.Open("my.db", 0600, nil)
	if e != nil {
		log.Fatal(e)
	}
	// 關閉 數據庫
	defer db.Close()
}
```

# 注意

* bolt 會使用 獨佔寫鎖 打來 數據庫 這意味者著 數據庫 同時只能被一個 進程 打開使用
* 如果 數據庫 已經被 bolt 打開 此時 另外一個 程式 繼續打開 bolt 在linux下 後Open的程式 會一直阻塞等待 數據庫被關閉 會再繼續 Open，此時 可以 在Open時 傳入 一個 超時時間 以便在打開超時後 返回

```
b, e := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
```
