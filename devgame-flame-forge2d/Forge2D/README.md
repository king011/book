# [Forge2D](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/forge2d.html)

Blue Fire 維護 Box2D 實體引擎的移植版本，我們的版本稱為 Forge2D

如果您想專門為 Flame 使用 Forge2D，您應該使用我們的橋接庫 [Flame_forge2d](https://github.com/flame-engine/flame/tree/main/packages/flame_forge2d)，如果您只想在 Dart 專案中使用它，您可以直接使用 [forge2d](https://github.com/flame-engine/forge2d) 庫

要在遊戲中使用它，您只需將 Flame_forge2d 添加到 pubspec.yaml 中，如 Forge2D 範例和 [pub.yaml](https://pub.dev/packages/flame_forge2d) 中所示

# [Forge2DGame](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/forge2d.html#forge2dgame)

如果您打算在專案中使用 Forge2D，那麼使用 Forge2D 特定的 FlameGame 類別 Forge2DGame 可能是個好主意

Forge2DGame 支援稱為 BodyComponents 的特殊 Forge2D 組件以及普通的 Flame 組件

Forge2DGame 有一個內建的 CameraComponent，預設為縮放等級設定為 10，因此您的元件將比普通 Flame 遊戲中的元件大得多。 這是由於 Forge2D 世界中的速度限制，如果您在 Zoom = 1.0 的情況下使用它，您很快就會達到速度限制。 您可以透過在建構函式中呼叫 super(zoom: yourZoom) 或在稍後階段執行 game.cameraComponent.viewfinder.zoom = yourZoom 輕鬆變更縮放等級

如果您以前熟悉 Box2D，那麼最好知道 Box2d 世界的整個概念都映射到 Forge2DGame 組件中的 world，並且您想要用作組件的每個 Body 都應該包裝在 BodyComponent 中，並添加到Forge2DGame 中的 world

您可以在 Forge2DGame 世界的元件清單中包含與非物理元件。 當呼叫更新時，它將使用Forge2D物理引擎正確更新每個BodyComponent，遊戲中的其他元件將按照正常的FlameGame方式更新

在Forge2DGame 中，與Forge2D 相比，重力被翻轉，以保持與Flame 中相同的座標系，因此重力中的正y 軸（如Vector2(0, 10)）會將物體向下拉，同時，負y 軸將拉力他們向上。 重力可以直接在 Forge2DGame 的構造函數中設定

範例資料夾中可以看到一個簡單的 Forge2DGame [實作範例](https://github.com/flame-engine/flame/tree/main/packages/flame_forge2d/example)

# [BodyComponent](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/forge2d.html#bodycomponent)

BodyComponent 是 Forge2D 主體的包裝器，它是物理引擎與之互動的主體。 要創建 BodyComponent，您可以：

* 重寫 createBody() 並建立並返回您建立的主體
* 透過將 BodyDef 實例（以及可選的 FixtureDef 實例清單）傳遞給 BodyComponent 的建構子來使用預設的 createBody() 實作
* 使用預設的 createBody() 實作並將 BodyDef 實例指派給 this.bodyDef，並且可以選擇將 FixtureDef 實例清單指派給 this.fixtureDefs

BodyComponent 預設具有 renderBody = true，否則，在創建 Body 並將 BodyComponent 添加到遊戲中後，它不會顯示任何內容。 如果你想關閉它，你可以將 renderBody 設定（或覆蓋）為 false

就像任何其他 Flame 組件一樣，您可以將子組件添加到 BodyComponent，如果您想在身體頂部添加動畫或其他組件，這會非常有用

您建立的主體應該根據 Flame 的座標系定義，而不是根據 Forge2D 的座標系（其中 Y 軸翻轉）

:exclamation: 在 Forge2D 中，你不應該將任何主體作為子元件加入其他元件中，因為 Forge2D 沒有嵌套主體的概念。 所以物體應該生活在物理世界的頂層，Forge2DGame.world。 因此，不應使用 add(Weapon()))，而應使用 world.add(Weapon()) （如下所示），並且 Player 當然也應該首先添加到世界中

```
class Weapon extends BodyComponent  {
  @override
  void onLoad() {
    ...
  }
}

class Player extends BodyComponent  {
  @override
  void onLoad() {
    world.add(Weapon());
  }
}
```

稍後您可能想要添加來自武器的子彈，這些子彈以相同的方式添加到 world 中，但如果它們移動得非常快，請確保設定 isBullet = true 以避免一些穿模問題

# [Contact callbacks](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_forge2d/forge2d.html#contact-callbacks)

Forge2DGame 提供了一個簡單的開箱即用的解決方案來傳播接觸事件

每當兩個夾具相遇時，就會發生接觸事件。 這些事件允許在這些夾具開始接觸 (beginContact) 和停止接觸 (endContact) 時進行偵聽

有多種方法可以監聽這些事件。 一種常見的方法是使用 Contact Callbacks 類別作為您對這些事件感興趣的 Body 元件中的 mixin

```
class Ball extends BodyComponent with ContactCallbacks {
  ...
  void beginContact(Object other, Contact contact) {
    if (other is Wall) {
      // Do something here.
    }
  }
  ...
}
```

為了使上述工作正常進行，球的 body.userData 或 contact Fixture.userData 必須設定為 ContactCallback。 如果 Wall 是 BodyComponent，它的 body.userData 或 contact fixture.userData 必須設定為 Wall

如果 userData 為 null，則忽略 contact 事件，預設為 null

設定 userData 的一個便捷方法是在建立 body 時分配它。 例如：

```
class Ball extends BodyComponent with ContactCallbacks {
  ...

  @override
  Body createBody() {
    ...
    final bodyDef = BodyDef(
      userData: this,
    );
    ...
  }

}
```

每次 Ball 和 Wall 開始接觸時，beginContact 都會被調用，一旦設備停止接觸，endContact 就會被調用

可以在 [Flame Forge2D 範例](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/bridge_libraries/flame_forge2d/utils/balls.dart)中看到實作範例