# [AudioPool](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/audio_pool.html)

AudioPool 是音訊播放器的提供者，它預先加載了本地資源以最大程度地減少延遲

單一 AudioPool 始終播放相同的聲音，通常是快速音效，例如船上的雷射射擊或平台遊戲的跳躍聲音

使用音訊池的優點是，透過配置最小（起始）大小和最大大小，池將建立並預先載入一些播放器，並允許它們重複使用多次

您可以使用輔助方法 FlameAudio.createPool 來使用相同的全域 FlameAudio.audioCache 建立 AudioPool 實例