# Encoding

deno 提供了一些 Encoding api 與瀏覽器保持一致，因爲瀏覽器也有提供這些 api 所以它們被直接放置在全局 namespace 中


# [btoa](https://deno.land/api@v1.28.1?s=btoa)

將字符串轉爲 base64 字符串
```
function btoa(s: string): string;
```

# [atob](https://deno.land/api@v1.28.1?s=atob)

將 base64 字符串解碼爲 字符串

```
/** 
 * @throws DOMException
 */
function atob(s: string): string;
```

# utf8 編碼/解碼

* [TextEncoder](https://deno.land/api@v1.28.1?s=TextEncoder) 提供了將字符串編碼爲 utf8 字節的功能
* [TextDecoder](https://deno.land/api@v1.28.1?s=TextDecoder) 提供了將 utf8 字節解碼爲字符串的功能
* [TextEncoderStream](https://deno.land/api@v1.28.1?s=TextEncoderStream) 提供了將字符串流編碼爲 utf8 字節流的功能
* [TextDecoderStream](https://deno.land/api@v1.28.1?s=TextDecoderStream) 提供了將 utf8 字節流解碼爲 字符串流的功能

```
import { assertEquals } from "std/testing/asserts.ts";

Deno.test("utf8", () => {
  const expected = "編碼到 utf8";
  const b = new TextEncoder().encode(expected);
  const actual = new TextDecoder().decode(b);
  assertEquals(actual, expected);
});

Deno.test("stream", async () => {
  const expected = ["編碼 chunk1", "編碼 chunk2"];
  const enc = new TextEncoderStream();
  (async () => {
    const w = enc.writable.getWriter();
    for (const str of expected) {
      await w.write(str);
    }
    await w.close();
  })();

  const dec = new TextDecoderStream();
  (async () => {
    const w = dec.writable.getWriter();
    for await (const b of enc.readable) {
      await w.write(b);
    }
    await w.close();
  })();
  let i = 0;
  for await (const actual of dec.readable) {
    assertEquals(actual, expected[i++]);
  }
});
```


