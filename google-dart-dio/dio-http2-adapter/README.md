# dio\_http2\_adapter

[dio\_http2\_adapter](https://pub.dev/packages/dio_http2_adapter) dio的一個開源適配器 爲dio提供了 http2 的支持

```
import 'package:dio/dio.dart';
import 'package:dio_http2_adapter/dio_http2_adapter.dart';

void main() async {
  final dio = Dio(BaseOptions(
    baseUrl: 'https://my.dev.com/',
  ))
    // 設置http2適配器
    ..httpClientAdapter = Http2Adapter(
      ConnectionManager(
        // 設置空閒 連接 時間
        idleTimeout: 15000,
        // 忽略證書驗證
        onClientCreate: (_, config) => config.onBadCertificate = (_) => true,
      ),
    );
  try {
    final response = await dio.get('api/v1/chapters/text', queryParameters: {
      'book': 'google-flutter',
      'chapter': 'install',
    });
    print(response.data);
  } on DioError catch (e) {
    print(e);
  }
}
```