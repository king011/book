# Shizuku

Shizuku 是一個 android root 工具庫

* 源碼 [https://github.com/RikkaApps/Shizuku](https://github.com/RikkaApps/Shizuku)
* 官網 [https://shizuku.rikka.app/zh-hant/](https://shizuku.rikka.app/zh-hant/)
* google play [https://play.google.com/store/apps/details?id=moe.shizuku.privileged.api](https://play.google.com/store/apps/details?id=moe.shizuku.privileged.api)