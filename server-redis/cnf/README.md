# 配置

redis 配置存儲在 **/etc/redis/redis.conf** 中 此外 也可以在 redis-cli 中使用指令 查看或設定 

```
# 獲取所有設定
CONFIG GET *

# 獲取指定項目設定
CONFIG GET loglevel

# 修改設定
CONFIG SET loglevel "notice"
```