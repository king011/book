# 字母表

西班牙語包含 27 個字母，其中5個元音，22個輔音

# 元音

[https://www.youtube.com/shorts/09exWol3Zwo](https://www.youtube.com/shorts/09exWol3Zwo)

<video controls loop style="max-width:100%;">
  <source src="https://share.king011.com/youtube/spanish/a-u.mp4" type="video/mp4">
</video>

# a
[https://www.youtube.com/shorts/dYqM1r_ivpU](https://www.youtube.com/shorts/dYqM1r_ivpU)

<video controls loop  style="max-width:100%;">
  <source src="https://share.king011.com/youtube/spanish/a.mp4" type="video/mp4">
</video>

# e

[https://www.youtube.com/shorts/ZSYqzHti7PY](https://www.youtube.com/shorts/ZSYqzHti7PY)

<video controls loop  style="max-width:100%;">
  <source src="https://share.king011.com/youtube/spanish/e.mp4" type="video/mp4">
</video>


# i

[https://www.youtube.com/shorts/U3A-4VIo-RM](https://www.youtube.com/shorts/U3A-4VIo-RM)

<video controls loop  style="max-width:100%;">
  <source src="https://share.king011.com/youtube/spanish/i.mp4" type="video/mp4">
</video>


# o

[https://www.youtube.com/shorts/HyGh4CXdkmQ](https://www.youtube.com/shorts/HyGh4CXdkmQ)

<video controls loop  style="max-width:100%;">
  <source src="https://share.king011.com/youtube/spanish/o.mp4" type="video/mp4">
</video>

# u

[https://www.youtube.com/shorts/Z3SqrDgxvT0](https://www.youtube.com/shorts/Z3SqrDgxvT0)

<video controls loop  style="max-width:100%;">
  <source src="https://share.king011.com/youtube/spanish/u.mp4" type="video/mp4">
</video>

