# 資料卷
docker 允許 將 一個 主機的 檔案夾/檔案 掛接到 容器中 使用 -v 參數即可
```sh
# 將 主機的 /home/king 掛接到 容器的 /mnt/king
docker run -it --name test -v /home/king:/mnt/king ubuntu:18.04 /bin/bash

# 同上 不過使用 只讀 掛接
docker run -it --name test -v /home/king:/mnt/king:ro ubuntu:18.04 /bin/bash

# 將 volumes 下創建一個 隨機名稱的主機目錄 掛接到  /mnt/king
docker run -it --name test -v /mnt/king ubuntu:18.04 /bin/bash

# 掛接 多個 目錄
docker run -it --name test -v /home/king:/mnt/king -v /home/king/bin:/mnt/bin  ubuntu:18.04 /bin/bash

# 掛接檔案
docker run -it --name test -v /home/king/.bashrc:/mnt/.bashrc ubuntu:18.04 /bin/bash
```
> 儘量不要掛接 檔案 因爲使用 vi sed的工具 操作檔案時 可能會修改 inode 這會造成錯誤
>
> 主機目錄 儘量使用 全路徑 如果 是相對路徑 別非相對 當前位置 通常是 相對 /var/lib/docker/volumes/


# 時區修正

利用資料卷可以很任意解決時區問題 直接將主機的時區檔案掛接給 容器使用即可

```
docker run \
    --rm \
    -v /etc/timezone:/etc/timezone:ro \
    -v /etc/localtime:/etc/localtime:ro \
    ubuntu:20.04 date
```

* /etc/timezone 代表當前時區設置 一般鏈接指向 /usr/share/zoneinfo 下的具體時區
* /etc/localtime 代表當前時區設置下的本地時間
# 刪除 資料卷
在創建 資料卷 時 如果 傳入了 主機檔案夾名稱 docker 不會刪除 檔案夾

如果 使用 docker 創建的 隨機 檔案夾 掛接 則 在使用 `docker rm 容器 -v` 命令 在刪除 最後一個 掛機到此目錄的 容器時 會自動 刪除 主機上的 檔案夾

# 資料卷 容器
可以 將 一個容器 作爲 資料卷 容器 她只需要 正常掛接目錄即可
(這樣 數據將 存儲在 資料卷 容器 中 而非 主機中)

其她容器 可以通過 --volumes-from 此容器 將 掛接點 掛接過來  
(資料卷 容器 只要存在即可 可以不必 運行)

```sh
# 創建一個 名爲 dbdata 的資料卷 容器 並創建掛接點 /dbdata
docker run -d -v /dbdata --name dbdata ubuntu:18.04

#  創建 名爲 db1 的新容器 並將 dbdata 容器 掛接的 資料卷 掛接到新容器
docker run -it --volumes-from dbdata --name db1 ubuntu:18.04 /bin/bash
#  創建 名爲 db2 的新容器 並將 dbdata 容器 掛接的 資料卷 掛接到新容器
docker run -it --volumes-from dbdata --name db2 ubuntu:18.04 /bin/bash
```

# 備份
要備份 可以 創建一個 新容器 掛接 資料卷容器 和 主機 並將 資料卷容器 打包 到 主機
```sh
# 創建 備份 容器 並備份
docker run --name backup --volumes-from dbdata -v $(pwd):/backup ubuntu:18.04 tar czvf /backup/backup.tar.gz /dbdata

# 之後 備份 只需要 運行 備份容器 即可
docker start backup
```

> docker start backup 會 備份 到 創建 備份 容器 時的 目錄 而非 當前 目錄

# 還原
還原 的原理 和 備份 類似 創建一個 新容器 掛接 資料卷容器 和 主機 並將 主機備份數據 解壓到 資料卷容器
```sh
# 創建 還原容器
sudo docker run --name restore --volumes-from dbdata -v $(pwd):/backup ubuntu:18.04 tar zxvf /backup/backup.tar.gz -C /

# 之後 還原 只需要 運行 還原容器 即可
docker start restore
```