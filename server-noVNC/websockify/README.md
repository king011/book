# websockify

noVNC 提供了多種語言實現的 websockify 用來將 websocket 協議傳輸的 vnc 數據轉爲 tcp 以與 vnc 服務器通訊

* python 源碼 [https://github.com/novnc/websockify](https://github.com/novnc/websockify)
* nodejs 源碼 [https://github.com/novnc/websockify-js](https://github.com/novnc/websockify-js)
* c 源碼 [https://github.com/novnc/websockify-other](https://github.com/novnc/websockify-other)


下述指令 可在 ubuntu 下 安裝 python 實現的 websockify
```
sudo apt install websockify
```

```
websockify [options] [source_addr:]source_port [target_addr:target_port]

websockify 127.0.0.1:1234 192.168.251.155:5900
```

# golang

使用 golang 只需要少許代碼即可實現 類似 websockify 的功能

```
package main

import (
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/gorilla/websocket"
)

const Addr = `:1234`
const Target = `192.168.251.155:5900`

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024 * 32,
	WriteBufferSize: 1024 * 32,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc(`/websockify`, websockify)

	l, e := net.Listen(`tcp`, Addr)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`work at`, Addr)
	http.Serve(l, mux)
}

func websockify(w http.ResponseWriter, r *http.Request) {
	client, e := upgrader.Upgrade(w, r, nil)
	if e != nil {
		return
	}
	target, e := net.Dial(`tcp`, Target)
	if e != nil {
		client.Close()
		return
	}
	// target -> client
	go func() {
		b := make([]byte, 1024*32)
		for {
			n, e := target.Read(b)
			if e != nil {
				log.Println(e)
				break
			}
			e = client.WriteMessage(websocket.BinaryMessage, b[:n])
			if e != nil {
				log.Println(e)
				break
			}
		}

		target.Close()
		client.Close()
	}()

	// client -> target
	for {
		_, p, e := client.ReadMessage()
		if e != nil {
			log.Println(e)
			break
		}
		_, e = target.Write(p)
		if e != nil {
			log.Println(e)
			break
		}
	}
	target.Close()
	client.Close()
}
```