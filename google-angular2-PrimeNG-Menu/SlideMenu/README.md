# [SlideMenu](https://primeng.org/slidemenu)

```
import {SlideMenuModule} from 'primeng/slidemenu';
import {MenuItem} from 'primeng/api';
```

SlideMenu 顯示滑動風格的菜單
![](assets/0.gif)

```
<p-slideMenu [model]="items"></p-slideMenu>
```

```
export class SlideMenuDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'Project'},
                            {label: 'Other'},
                        ]
                    },
                    {label: 'Open'},
                    {label: 'Quit'}
                ]
            },
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-trash'},
                    {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
                ]
            }
        ];
    }
}
```

## Popup Mode

默認情況下，菜單是內聯的，設置 **popup** 屬性爲 true 來使用彈出菜單

```
<p-slideMenu #menu [model]="items" [popup]="true" ></p-slideMenu>
<button #btn type="button" pButton icon="pi pi-list" label="Show" (click)="menu.toggle($event)"></button>
```

## Effects

默認使用的緩動函數是 **ease-out**，可以通過 easing 屬性來改變設定

```
<p-slideMenu #menu [model]="items" effectDuration="1000" easing="ease-in"></p-slideMenu>
```

## Animation Configuration

使用 **showTransitionOptions/hideTransitionOptions** 屬性來自定義打開和關閉的過渡動畫，下面的例子將完全禁用動畫

```
<p-slideMenu [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'" #menu [model]="items" [popup]="true"></p-slideMenu>
<button #btn type="button" pButton icon="pi pi-list" label="Show" (click)="menu.toggle($event)"></button>
```

# Properties
| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| popup     | boolean     | false     | 是否作爲彈出菜單     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| easing | string | ease-out | 用於滑動的簡易動畫 |
| effectDuration | any | 250 | 滑動動畫持續時間(以毫秒爲單位) |
| backLabel | string | Back | 返回標籤顯示的文本 |
| menuWidth | number | 180 | 子菜單寬度 |
| viewportHeight | number | 175 | 可滾動區域的高度，如果菜單高度大於此值，則出現滾動條 |
| appendTo | any | null | Target element to attach the overlay, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name). |
| baseZIndex     | number     | 0     | 分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| showTransitionOptions     | string     | 0.12s cubic-bezier(0, 0, 0.2, 1)	     | 顯示過渡動畫     |
| hideTransitionOptions     | string     | 0.1s linear	     | 隱藏過渡動畫     |
# Methods

| Name | Parameters | Description |
| -------- | -------- | -------- |
| toggle     | event: browser event     | 切換菜單顯示狀態     |
| show     | event: browser event     | 顯示菜單     |
| hide     | -     | 隱藏菜單     |

# Styling

| Name | Element |
| -------- | -------- |
| p-slidemenu     | Container element.     |
| p-slidemenu-wrapper     | Wrapper of content.     |
| p-slidemenu-content     | Content element.     |
| p-slidemenu-backward     | Element to navigate to previous menu on click.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
| p-submenu-icon     | Arrow icon of a submenu.     |

