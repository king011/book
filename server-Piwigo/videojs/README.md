# [piwigo-videojs](https://github.com/Piwigo/piwigo-videojs)

piwigo-videojs 是 piwigo 的一個插件以允許 piwigo 上傳和播放視頻

piwigo 使用了一些第三方依賴，除了安裝此插件需要自行安裝依賴

* MediaInfo
* FFmpeg

# [設定](https://github.com/Piwigo/piwigo-videojs/wiki)

此外需要在 **LocalFiles Editor** 中添加如下設定

```
<?php
// 允許上傳任何檔案
$conf['upload_form_all_types'] = true;
$conf['picture_ext'] = array('jpg','jpeg','png','gif');
// 設置允許上傳檔案後綴名
$conf['file_ext'] = array_merge(
  $conf['picture_ext']
  # array('tiff', 'tif', 'mpg','zip','avi','mp3','ogg','pdf')
  );
	

// 可選設定，允許視頻檔案名包含特殊字符
$conf['sync_chars_regex'] = '/^[a-zA-Z0-9-_. ]+$/';

// 顯示元數據
$conf['show_exif'] = true;
// 設定可顯示的元數據字段
$conf['show_exif_fields'] = array(
  'Make',
  'Model',
  'DateTimeOriginal',
  'COMPUTED;ApertureFNumber',
  'ExifVersion',
  'Software',
  'FNumber',
  'ExposureBiasValue',
  'FILE;FileSize',
  'ExposureTime',
  'Flash',
  'ISOSpeedRatings',
  'FocalLength',
  'FocalLengthIn35mmFilm',
  'WhiteBalance',
  'ExposureMode',
  'MeteringMode',
  'ExposureProgram',
  'LightSource',
  'Contrast',
  'Saturation',
  'Sharpness',
  'bitrate',
  'channel',
  'date_creation',
  'display_aspect_ratio',
  'duration',
  'filesize',
  'format',
  'formatprofile',
  'codecid',
  'frame_rate',
  'latitude',
  'longitude',
  'make',
  'model',
  'playtime_seconds',
  'sampling_rate',
  'type',
  'resolution',
  'rotation',
  );
?>
```
