# ScriptableObject

ScriptableObject 是一個數據容器，可用於保存大量數據，通常 ScriptableObject 用作一些不會變化的數據配置 例如關卡設定，翻譯資源等，將 ScriptableObject 實際上被看作一種資源最終也會在發佈遊戲時被 unity 打包爲資源，所以對同一份 ScriptableObject 資源的引用實際上是操作的通過單例

ScriptableObject 允許在 Editor 中隨時修改包括 Play 模式下，以方便設計師隨時調整 ScriptableObject 的設定，所有修改都會自動被序列化

# 使用 ScriptableObject 

要使用 ScriptableObject 需要自定義一個從 ScriptableObject 派生的類，同時使用 CreateAssetMenu 爲 Editor 添加一個新建資源的按鈕，之後在 Editor 使用此按鈕即可創建一個 ScriptableObject 資源供遊戲使用

```
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class SpawnManagerScriptableObject : ScriptableObject
{
    public string prefabName;

    public int numberOfPrefabsToCreate;
    public Vector3[] spawnPoints;
}
```

```
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // The GameObject to instantiate.
    public GameObject entityToSpawn;

    // An instance of the ScriptableObject defined above.
    public SpawnManagerScriptableObject spawnManagerValues;

    // This will be appended to the name of the created entities and increment when each is created.
    int instanceNumber = 1;

    void Start()
    {
        SpawnEntities();
    }

    void SpawnEntities()
    {
        int currentSpawnPointIndex = 0;

        for (int i = 0; i < spawnManagerValues.numberOfPrefabsToCreate; i++)
        {
            // Creates an instance of the prefab at the current spawn point.
            GameObject currentEntity = Instantiate(entityToSpawn, spawnManagerValues.spawnPoints[currentSpawnPointIndex], Quaternion.identity);

            // Sets the name of the instantiated entity to be the string defined in the ScriptableObject and then appends it with a unique number. 
            currentEntity.name = spawnManagerValues.prefabName + instanceNumber;

            // Moves to the next spawn point index. If it goes out of range, it wraps back to the start.
            currentSpawnPointIndex = (currentSpawnPointIndex + 1) % spawnManagerValues.spawnPoints.Length;

            instanceNumber++;
        }
    }
}
```

# 注意

ScriptableObject 只在 Editor 下可以被序列化與反序列化，但在最終發佈遊戲時其值的修改不會被存入磁盤，但同個 session 即內存中的值允許修改，故不可用其存儲玩家進度或玩家設定