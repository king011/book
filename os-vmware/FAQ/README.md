# linux 宿主機提示無法支持3D

linux 顯卡驅動已經正常工作但是 vmware 提示 3d 無法工作，這可能是因爲某些驅動被默認列入在黑名單中

![](assets/20200309150745628.png)

執行 **glxinfo | grep -i direct** 確保輸出 **yes** 表示 3d 已經正常工作

```
$ glxinfo | grep -i direct
direct rendering: Yes
    GL_AMD_multi_draw_indirect, GL_AMD_performance_monitor, 
    GL_ARB_derivative_control, GL_ARB_direct_state_access, 
    GL_ARB_draw_elements_base_vertex, GL_ARB_draw_indirect, 
    GL_ARB_multi_draw_indirect, GL_ARB_occlusion_query2, 
    GL_AMD_draw_buffers_blend, GL_AMD_multi_draw_indirect, 
    GL_ARB_direct_state_access, GL_ARB_draw_buffers, 
    GL_ARB_draw_indirect, GL_ARB_draw_instanced, GL_ARB_enhanced_layouts, 
    GL_ARB_multi_draw_indirect, GL_ARB_multisample, GL_ARB_multitexture, 
    GL_EXT_copy_texture, GL_EXT_depth_bounds_test, GL_EXT_direct_state_access,
```

編輯 **.vmware/preferences** 檔案添加如下內容

```
mks.gl.allowBlacklistedDrivers = "TRUE"
```