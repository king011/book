# Service Worker
Service Worker 可以 對web提供 強大的緩存 機制 從而 提升 網頁打開速度 以及 節省用戶 訪問流量

特別對於 angular 這種 單頁面程式 是Service Worker最大的受益者

angular 從 v5.0.0 開始 提供了對 Service Worker 的支持 現在 你只需要 執行簡單的 命令 告訴 angular 打開 對Service Worker的支持 即可得到 Service Worker 帶來的好處 並且不想要 對原有代碼做任何的修改

> 出於安全考慮 Service Worker 只工作在 https 下  
> 一些瀏覽器爲了方便調試 Service Worker 也被允許工作在 http://http://localhost http://127.0.0.1 

# 使用
angular 已經把 使用 Service Worker 的一切都處理好了 只需要 按部就班 的執行 如下步驟 即可 獲得 Service Worker 帶來的 好處

1. 添加支持  
 `ng add  @angular/pwa`
 
1. 編譯項目  
 `ng build --prod`

## ng add @angular/pwa
ng add @angular/pwa 會爲項目 添加 Service Worker 需要的 一切
* 將依賴的包安裝
* 創建 Service Worker 配置檔案 **ngsw-config.json**
* 創建 src/manifest.json 
* 在 src/app/app.module.ts 中 加入 **ServiceWorkerModule** 模塊
* 在 src/index.html 中 加入 Service Worker 必要設置

## ng build --prod
angular 的 Service Worker 只在 發佈時有效 執行 `ng build --prod` 時 ng 如果發現 需要 **Service Worker** 會自動 爲如下項目 創建 Service Worker 緩存
* index.html
* favicon.ico
* 構建出的 js 和 css
* assets下的所有檔案