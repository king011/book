# [cURL](https://curl.se/)

curl 是一個開源的 http 客戶端，curl 不是 bash 組件，但如果要在 bash 中處理 http 使用 curl 通常是最好的選擇

## w

[-w, --write-out &lt;format&gt;](https://curl.se/docs/manpage.html#-w) 格式化輸出內容，可以使用 **%varname%** 或 **%{varname}** 的形式輸出變量，使用**%header{name}** (在 curl 7.84.0 中添加)的形式輸出最近的響應header

```
curl -Sso /dev/null -w "%{http_code}\n " http://127.0.0.1

curl -Sso /dev/null -w "code: %{http_code}\ncontent-type: %header{content-type}\n" http://127.0.0.1
```

下面是支持的變量:

* **certs** 输出带有详细信息的证书链。 仅由 OpenSSL、GnuTLS、Schannel、NSS、GSKit 和安全传输后端支持 (Added in 7.88.0)
* **content\_type** 響應 header 中的 Content-Type
* **errormsg** 錯誤信息 (Added in 7.75.0)
* **exitcode** curl 命令退出代碼 (Added in 7.75.0)
* **filename\_effective** curl 写入的最终文件名。 这仅在 curl 被告知使用 -O、--remote-name 或 -o、--output 选项写入文件时才有意义。 它与 -J, --remote-header-name 选项结合使用最为有用
* **ftp\_entry\_path** 登录到远程 FTP 服务器时，初始路径
* **header\_json** 一个 JSON 对象，其中包含最近传输的所有 HTTP 响应标头。 值以数组形式提供，因为在多个标头的情况下可以有多个值 (Added in 7.83.0)
* **http\_code** http/https 的響應代碼
* **http\_connect** 在对 curl CONNECT 请求的最后响应（来自代理）中找到的数字代码
* **http\_version** 使用的有效 http 版本
* **local\_ip** 最近完成的连接的本地端的 IP 地址 - 可以是 IPv4 或 IPv6
* **local\_port** 最近完成的连接的本地端口号
* **method** 在最近的 HTTP 请求中使用的 http 方法 (Added in 7.72.0)
* **num\_certs** (Added in 7.88.0)
* **num\_connects**
* **num\_headers** (Added in 7.73.0)
* **num\_redirects**
* **onerror** (Added in 7.75.0)
* **proxy\_ssl\_verify\_result** (Added in 7.52.0)
* **redirect\_url**
* **referer** (Added in 7.76.0)
* **remote\_ip**
* **remote\_port**
* **response\_code**
* **scheme** (Added in 7.52.0)
* **size\_download** 這是實際下載的 body 大小，故無法獲取 HEAD 請求返回的 content-length
* **size\_header**
* **size\_request**
* **size\_upload**
* **speed\_download**
* **speed\_upload**
* **ssl\_verify\_result**
* **stderr** (Added in 7.63.0)
* **stdout** (Added in 7.63.0)
* **time\_appconnect**
* **time_connect**
* **time\_namelookup**
* **time\_pretransfer**
* **time\_redirect**
* **time\_starttransfer**
* **time\_total**
* **url** (Added in 7.75.0)
* **url.scheme**  (Added in 8.1.0)
* **url.user**  (Added in 8.1.0)
* **url.password**  (Added in 8.1.0)
* **url.options**  (Added in 8.1.0)
* **url.host**  (Added in 8.1.0)
* **url.port**  (Added in 8.1.0)
* **url.path**  (Added in 8.1.0)
* **url.query**  (Added in 8.1.0)
* **url.fragment**  (Added in 8.1.0)
* **url.zoneid**  (Added in 8.1.0)
* **urle.scheme**  (Added in 8.1.0)
* **urle.user**  (Added in 8.1.0)
* **urle.password**  (Added in 8.1.0)
* **urle.options**  (Added in 8.1.0)
* **urle.host**  (Added in 8.1.0)
* **urle.port**  (Added in 8.1.0)
* **urle.path**  (Added in 8.1.0)
* **urle.query**  (Added in 8.1.0)
* **urle.fragment**  (Added in 8.1.0)
* **urle.zoneid**  (Added in 8.1.0)
* **urlnum** (Added in 7.75.0)
* **url\_effective**

## 下載檔案

下面例子下載 **http://127.0.0.1/shared/a.txt** 並保存爲 a.txt

```
curl -#Rz a.txt -o a.txt http://127.0.0.1/shared/a.txt
```

* **-#, --progress-bar** 顯示傳輸進度條
* [**-R, --remote-time**](https://curl.se/docs/manpage.html#-R) 使用遠程時間(last-modified)，替代本地時間
* [-z, --time-cond &lt;time&gt;](https://curl.se/docs/manpage.html#-z) 使用 http 提供的 **If-Modified-Since** 特性

推薦始終使用 -Rz 參數配合，這樣可以重複使用相同指令，但只有在服務器檔案更新後才會真的去下載新的檔案數據，這對於要定時從服務器上同步檔案的需求很有幫助

### 恢復下載

如果下載意外中斷，可以使用 -C 指定要下載的偏移

* [-C, --continue-at &lt;offset&gt;](https://curl.se/docs/manpage.html#-C) Resumed transfer offset

```
curl -#R -o a.txt -C - http://127.0.0.1/shared/a.txt
```

直接將 -C 和 -z 一起使用可能存在一些風險(例如：服務器更新了一個較小的檔案，因爲 -z 存在導致request設置的請求請求頭 `Range: bytes=N-` N 大於當前服務器檔案大小，依據服務器的實現，服務器可能會返回 416 以及錯誤信息，而非重新下載檔案)

解決方案是你可以先使用 HEAD 請求來驗證下伺服器是否存在新版本然後執行不帶 -z 的 -C 指令(但是這個方案並不完美，如果服務器在 HEAD 之後，下載之前更新了檔案腳本無法得知，但這種情況很少發生)

* [-I, --head](https://curl.se/docs/manpage.html#-I) 發送 HEAD 請求，並輸出響應

```
code=`curl -w "%{http_code}\n" -o /dev/null -ISsz a.txt  http://127.0.0.1/shared/a.txt`
if [ "$code" == '304' ];then
    echo not changed
elif [ "$code" == '200' ];then
    echo changed
fi
```


## 代理

curl 支持多種代理

* [--socks5 &lt;host\[:port\]&gt;](https://curl.se/docs/manpage.html#--socks5) 指定 socks5 代理

```
curl --socks5 127.0.0.1:1080 ...
```

# [urldecode](https://gist.github.com/cdown/1163649)

下面是 github 上的一個 url 編解碼 函數

```
urlencode() {
    # urlencode <string>

    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C

    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:$i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf '%s' "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

urldecode() {
    # urldecode <string>

    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}
```