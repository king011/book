# Mint
mint 默認使用 noto 顯示 無線額外 安裝 設置

# Linux
* Command-line:

	```sh
	cd ~/Downloads   # or wherever you downloaded a font pack.
	unzip .zip
	# for a single user
	mkdir -p ~/.fonts
	cp *otf *otc ~/.fonts
	fc-cache -f -v # optional
	# for all users
	sudo mkdir -p /usr/share/fonts/opentype/noto
	sudo cp *otf *otc /usr/share/fonts/opentype/noto
	sudo fc-cache -f -v # optional
	```
	
* Gnome desktop for a single user
    1. [Download](https://noto-website.storage.googleapis.com/pkgs/Noto-hinted.zip) and uncompress a font package
    2. Make *.fonts* directory in your home directory if it's not presen
    3. Make a folder named "*noto*" (optional)
    4. Drag and drop fonts to either *.fonts* or *.fonts/noto*
* Gnome desktop for all users
    1. [Download](https://noto-website.storage.googleapis.com/pkgs/Noto-hinted.zip) and uncompress a font package
    2. Press Alt-F2 and type "*gksu nautilus /usr/share/fonts*"
    3. Make a folder named "*opentype*" if not present and open it
    4. Make a folder "*noto*" in "*opentype*" folder.
    5. Drag and drop fonts obtained in step a to "*noto*" folder.
* Using fonts
    1. Some programs will recognize newly installed fonts as soon as font-cache is updated. It can take a while to update the fontcache in case of super-otc.
    2. Other programs such as Chromre requires restart.
    3. Chrome on Linux cannot recognize more than 2 weights. See [http://crbug.com/368442](http://crbug.com/368442)

# Windows
1. Download the font package (.zip)
2. Uncompress the package
3. Search for “fonts” in the Start Menu or go to Start → Control Panel → Appearance and Personalization → Fonts
4. Select all fonts and drag them into the Fonts folder

# OS X
1. [Download](https://noto-website.storage.googleapis.com/pkgs/Noto-unhinted.zip) the font package (.zip)
2. Double click on the package to uncompress it
3. Open Font Book (Go to Finder → Applications → Font Book)
4. Select all of the font files and drag them to the Font column (i.e., the second column) of Font Book