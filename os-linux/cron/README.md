# 例行工作

有一些例行工作需要在指定時間執行 linux 爲它們提供了很好的支持

# at
at 用於安排那些只需要執行一次的工作，at 任務會被記錄在 **/var/spool/at** 檔案夾下

## 配置檔案

at 存在兩個配置檔案：

1. **/etc/at.deny** 通常安裝 at 組件時會自動創建 at.deny 檔案記錄了禁止使用 at 的用戶(每行一個用戶名)
2. **/etc/at.allow** 如果 at.allow 存在則忽略 at.deny 並且只有 at.allow 中記錄的用戶(每行一個用戶名)可以使用 at

如果兩個配置檔案都不存在，則只有 root 可以執行 at

## TIME
at 用於在指定時間執行一個指令，故需要設置執行時間，at 提供了多種靈活的時間設置方式


| 格式 | 含義 | 例子 |
| -------- | -------- | -------- |
| hh:mm     | 小時:分鐘 如果指定時間今天已過則明天此時執行否則今天此時執行     | 23:00     |
| hh:mm[am|pm]     | 12小時計時 am 表上午 pm表示下午    | 11:00pm     |
| hh:mm YYYY-MM-DD     | 指定日期和時間   | 11:00 2022-04-16     |
| hh:mm YY-MM-DD     | 同上年份前兩個數字可省略   | 11:00pm 22-04-16     |
| now     | 立刻執行   |      |
| TIME+number\[weeks\|days\|hours\|min\]     | 相當 TIME 延遲一段時間執行   | now+1min     |

## 常用命令

```
# 進入 at 命令模式，編寫要執行的命令
at TIME

# 直接以檔案內容 創建 at 任務
at -f 檔案名 TIME

# 列出當前用戶的 at 任務表
at -l
atq


# 查詢指定 任務的 具體命令
at -c 任務id

# 刪除 任務
ad -d 任務id
atrm 任務id
```

## 注意

* at 任務不需要用戶登入也會執行
* at 會將執行結果 mail 到 /var/main/用戶名
* at 會啓動一個全新的 shell 執行 故應該考慮環境變量的不同，特別是 **PATH** 變量

ubuntu 可能沒有安裝 at 需要自行安裝，另外如果要收到 mail 執行結果還需要安裝 mailutils
```
sudo apt install at mailutils
```

在 systemd 下 at 由 atd.service 提供服務
```
sudo systemctl status atd.service
```

# batch

batch 用法和 at 基本一致區別在於 batch 在負載高於 0.8 時不會執行任務

# crontab

crontab 比 at 更加常用，crontab 用於安排需要週期執行的任務，每個用戶的任務被記錄在 **/var/spool/cron/用戶名** 下

## 配置檔案

crontab 存在兩個配置檔案：
1. **/etc/cron.deny** 記錄了禁止使用 crontab 的用戶(每行一個用戶名)
2. **/etc/cron.allow** 如果 cron.allow 存在則忽略 cron.deny 並且只有 cron.allow 中記錄的用戶(每行一個用戶名)可以使用 cron

## 常用命令

```
crontab [-u username] [-elr]
	-u	# 需要root權限 操作指定用戶的 crontab
	-e	# 編輯
	-l	# 查詢
	-r	# 刪除所有的 crontab 任務
```

```
crontab -l
#分鐘     小時     日期     月份    星期          命令
#minute   hour     date     month   day of week   command
#[0,59]   [0,23]   [1,31]   [1~12]  [0,7] 0==7
0          13       *		*       0            sudo yum update -y
```

## 時間定義
crontab 的執行時間支持多種模式

| 語法 | 含義 | 示例以日期爲例子 | 示例含義 |
| -------- | -------- | -------- | -------- |
| *     | 任意時間     | *     | 每天執行     |
| ,     | 分隔多個時間     | 1,5,7     | 每月1 5 7 號執行     |
| -     | 時間段     | 1-5     | 每月前五天執行     |
| /n     | 每隔多久     | /3     | 每隔 3 天執行     |

## /etc/crontab
除了用戶的 crontab，系統管理員還可以編輯 **/etc/crontab** 安排系統級別的 週期任務，其任務寫法類型 **crontab -e**  但在指定命令之前的一列需要指定以哪個用戶名執行任務

```
#info="/etc/crontab" 
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#

```

## 注意

* crontab 任務不需要用戶登入也會執行
* crontab 會將執行結果 mail 到 /var/main/用戶名
* crontab 會啓動一個全新的 shell 執行 故應該考慮環境變量的不同，特別是 **PATH** 變量
* crontab 的日誌通常存儲在 **/var/log/cron** 中(但本喵在使用 crontab 時未發現此日誌檔案，不知爲何?)

ubuntu 可能沒有安裝 mailutils 無法接收 crontab 的 mail 通知需要自行安裝

```
sudo apt install at mailutils
```

在 systemd 下 crontab 由 cron.service 提供服務

```
sudo systemctl status cron.service
```

# anacron

anacron 會在系統開機時比較 crontab 的執行時間戳，並將未執行的 crontab 任務執行，以避免 crontab 因爲系統未開機等原因導致錯過任務執行

anacron 的配置檔案是 **/etc/anacrontab** 通常系統已經設置好了 anacron，不需要自行配置

因爲 anacron 會去執行錯過執行的 crontab，所以 linux 系統關機一段時間後再開機，在開機時 cpu 會忙碌一段時間這很可能就是 anacron 正在執行關機時錯過執行的 crontab 任務