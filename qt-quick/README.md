# QtQuick

QtQuick 是 qt 提供的 UI 排版引擎

其提供了 QML 用於 方便的 編寫界面  
同時提供了 用 c++ 擴展 QtQuick 的能力

# QML

QML 是qt爲 QtQuick  開發的一個新的 宣告式 語言  
其提供了 高可讀 復用 交換 的 ui編寫模式

QML 使用 類似 json 語法聲明

# Controls Controls2

早期的 qml ui在 Controls 1.XXX 中 爲了支持移動端 qt 實現了 Controls2.XXX 來提供更美觀 高效的 ui

如果要同時 使用 Controls Controls2 通常或 重命名 Controls 1.XXX 並顯示 的調用 Controls 1.XXX 中的 ui
```
import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Controls 1.4 as Controls14
import QtQuick.Layouts 1.11

Controls14.CheckBox {
	...
}
```

#  hello world

```
#info="qrc:/main.qml"
import QtQuick 2.10
import QtQuick.Window 2.10
 
Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("test quick")
    Text {
        anchors.centerIn: parent
        text: qsTr("this is test")
    }
}
```

```cpp
#info="main.cpp"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
 
int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
 
    QGuiApplication app(argc, argv);
 
    // 創建 qml 程式 並加載 qml
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
 
    return app.exec();
}
```