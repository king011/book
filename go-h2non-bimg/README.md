# h2non/bimg

h2non/bimg 是一個使用 cgo 的開源(MIT)高效圖像處理庫，通常比標準庫快4倍左右

```
go get -u github.com/h2non/bimg
```

h2non/bimg 使用 libvips 處理圖像，對於 ubuntu 可以使用 apt 進行安裝

```
sudo apt install libvips
```

* 源碼  [https://github.com/h2non/bimg](https://github.com/h2non/bimg)