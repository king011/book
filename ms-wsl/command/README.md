# 常用命令

```
# 更新 wsl 系統
wsl --update

# 查看 wsl 版本信息
wsl --version

# 查看所有子系統狀態，帶 * 前綴的是默認子系統
wsl -l -v

# 關閉所有的 wsl 2 子系統
wsl --shutdown

# 在默認的子系統中執行 bash
wsl bash

# 在指定名稱的子系統中執行 bash
wsl -d ubuntu bash

# 設置默認子系統
wslconfig /setdefault XXX
```