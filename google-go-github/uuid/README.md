# uuid

* UUID的字符串 格式爲 8-4-4-4-12 的32個字符+4個**-**
* 不區分大小寫

行如 xxxxxxxx-xxxx-**M**xxx-**N**xxx-xxxxxxxxxxxx

* M 代表 版本號 當前可以是 1 2 3 4 5 五個版本
* N 必須爲 8 9 a b 中的一個

# google/uuid

```sh
#info=false
go get -u -v github.com/google/uuid
```

* google/uuid 是由google維護的 一個 開源(BSD-3-Clause) go uuid 庫
* RFC 4122 and DCE 1.1
* [https://github.com/google/uuid](https://github.com/google/uuid)

# v1

使用 當前時間戳 序列 MAC 地址 創建 uuid

```go
package main
 
import (
	"fmt"
	"github.com/google/uuid"
	"log"
)
 
func main() {
	var e error
	var u, u1, u2 uuid.UUID
	//創建一個 uuid
	u, e = uuid.NewUUID()
	if e != nil {
		log.Fatalln(e)
	}
	//顯示 uuid 值 版本號
	fmt.Println(u, u.Version())
 
	//string <-> uuid
	u1, e = uuid.Parse(u.String())
	//u1, e = uuid.ParseBytes([]byte(u.String()))
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(u == u1)
 
	//[]byte <-> uuid
	b := [16]byte(u)
	u2, e = uuid.FromBytes(b[:])
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(u == u2)
}
```
> uuid.UUID 是 \[16\]byte 故可以直接 **==**  
> \[\]byte 是 不支持 **==** 操作符的
> 

# v2
* v2 和v1算法 差不多 只是將 時間戳的 前 4位 替換爲 **uid 或 gid**
* 一般 uuid 庫都沒實現 也不建議使用 v2

```go
package main
 
import (
	"fmt"
	"github.com/google/uuid"
	"log"
)
 
func main() {
	var e error
	var u uuid.UUID
	//替換 uid
	u, e = uuid.NewDCEPerson()
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(u, u.Version())
 
	//替換gid
	u, e = uuid.NewDCEGroup()
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(u, u.Version())
}
```

> 雖然 google 有提供 v2的 實現 然 v2不建議使用v2 而是使用 v1
> 

# v3
* v3 使用 名字空間 和md5 爲指定 名稱 產生 uuid
* 如果 名字空間相同 且名字一樣 則產生 相同的 uuid

```go
package main
 
import (
	//"crypto/md5"
	"fmt"
	"github.com/google/uuid"
)
 
func main() {
 
	var u, u1 uuid.UUID
 
	//相同 名字空間 相同數據 會產生 相同 uuid
	u = uuid.NewMD5(uuid.Nil, []byte("kate"))
	//同 u = uuid.NewHash(md5.New(), uuid.Nil, []byte("kate"), 3)
	fmt.Println(u, u.Version())
 
	u1 = uuid.NewMD5(uuid.Nil, []byte("kate"))
	fmt.Println(u1, u1.Version())
 
	fmt.Println(u == u1)
 
	//不同 名字空間 相同數據 會產生 不同 uuid
	u1 = uuid.NewMD5(u, []byte("kate"))
	fmt.Println(u1, u1.Version())
	fmt.Println(u != u1)
}
```

# v4

v4 將所有數據都以 僞隨機 產生

```go
package main
 
import (
	"fmt"
	"github.com/google/uuid"
	"log"
)
 
func main() {
	var e error
	var u uuid.UUID
	//創建一個 uuid
	u, e = uuid.NewRandom()
	if e != nil {
		log.Fatalln(e)
	}
	//顯示 uuid 值 版本號
	fmt.Println(u, u.Version())
 
	//創建一個 uuid panic(error)
	//等同與 uuid.Must(uuid.NewRandom())
	u = uuid.New()
	//顯示 uuid 值 版本號
	fmt.Println(u, u.Version())
}
```

# v5
v5 和 v3 一樣 只是 把 v3的md5算法 替換爲 sha1算法

```go
package main
 
import (
	//"crypto/sha1"
	"fmt"
	"github.com/google/uuid"
)
 
func main() {
 
	var u, u1 uuid.UUID
 
	//相同 名字空間 相同數據 會產生 相同 uuid
	u = uuid.NewSHA1(uuid.Nil, []byte("kate"))
	//同 u = uuid.NewHash(sha1.New(), uuid.Nil, []byte("kate"), 5)
	fmt.Println(u, u.Version())
 
	u1 = uuid.NewSHA1(uuid.Nil, []byte("kate"))
	fmt.Println(u1, u1.Version())
 
	fmt.Println(u == u1)
 
	//不同 名字空間 相同數據 會產生 不同 uuid
	u1 = uuid.NewSHA1(u, []byte("kate"))
	fmt.Println(u1, u1.Version())
	fmt.Println(u != u1)
}
```
