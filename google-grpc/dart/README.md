# dart

要 由 \*.proto 生成 dart代碼 需要 **protoc-gen-dart** 插件

1. 安裝好 dart 環境 和 pub 工具

1.  安裝 **protoc-gen-dart** 插件

	```
	#info=false
	pub global activate protoc_plugin
	```
	
	pub 默認會將 將其 安裝到 **~/.pub-cache/bin/protoc-gen-dart**

1. 生成 dart代碼

	```
	#info=false
	protoc -I $root --dart_out=grpc:$out $_SOURCES
	```
	
# pubspec.yaml
在要在 pubspec.yaml 中 添加 grpc 和 protobuf 的依賴

```yaml
dependencies:
  grpc:
  protobuf: ^0.10.1
```

# 客戶端 代碼

```dart
#info="main.dart"
import 'dart:io';
import 'dart:convert';
import 'client.dart';

// 聲明 main 爲 異步 函數
main() async {
  // 創建 客戶端
  var client = Client();
  client.dila();

  // 執行 命令
  stdout.write(r"$>");
  await for (var b in stdin) {
    var cmd = utf8.decode(b).trim();
    if (!cmd.isEmpty) {
      var v = await doCmd(client, cmd);
      if (v != null) {
        break;
      }
    }
    stdout.write(r"$>");
  }

  // 關閉 客戶端
  client.shutdown();
}

doCmd(Client client, String cmd) async {
  switch (cmd) {
    case "e":
      return 1;
    case "p":
      await client.requestPenguin();
      break;
    case "d":
      await client.requestDog();
      break;
    case "c":
      await client.requestCat();
      break;
    case "f0":
      await client.requestFish0();
      break;
    case "f":
      await client.requestFish();
      break;
  }
}
```

```dart
#info="client.dart"
import 'package:grpc/grpc.dart';
import 'dart:core';
import 'package:examples/zoo/Service.pbgrpc.dart';
import 'dart:async';

class Client {
  // 通道 是到 服務器間 虛擬數據 交互 管道
  ClientChannel channel;

  // stub 管理到 具體的 服務器模塊 的調用
  ServiceClient stub;
  dila() {
    // 創建 通道
    channel = ClientChannel(
      "127.0.0.1",
      port: 1102,
      options: const ChannelOptions(
        credentials: const ChannelCredentials.insecure(), //h2c
      ),
    );

    // 創建 全局 的調用 上下文
    stub = ServiceClient(
      channel,
      options: CallOptions(
          timeout: Duration(
        seconds: 10,
      )),
    );
  }

  shutdown() => channel.shutdown();

  Future<void> requestPenguin() async {
    try {
      // 調用
      await stub.penguin(PenguinRequest());
      print("penguin sucess");
    } on GrpcError catch (e) {
      print("$e");
    }
  }

  Future requestDog() async {
    // 創建 stream
    // stream 中的 異常 會被 grpc 框架 捕獲 後 停止本次 grpc 請求
    Stream<DogRequest> requestStream(int count) async* {
      var request = DogRequest();
      for (int i = 0; i < count; i++) {
        // 將 數據 發往 生成器
        print("set dog ${i + 1}");
        yield request;
      }
    }

    try {
      // 發送 請求
      await stub.dog(requestStream(2));
      print("dog sucess");
    } catch (e) {
      print("$e");
    }
  }

  Future requestCat() async {
    try {
      // 調用
      var i = 0;
      await for (var _ in stub.cat(CatRequest())) {
        print("get cat ${++i}");
      }
      print("cat sucess");
    } on GrpcError catch (e) {
      print("$e");
    }
  }

  Future requestFish0() async {
    // 創建 stream
    // stream 中的 異常 會被 grpc 框架 捕獲 後 停止本次 grpc 請求
    Stream<FishRequest> requestStream(int count) async* {
      var request = FishRequest();
      for (int i = 0; i < count; i++) {
        // 將 數據 發往 生成器
        print("set fish ${i + 1}");
        yield request;
      }
    }

    try {
      // 發送 請求
      var i = 0;
      await for (var _ in stub.fish(requestStream(2))) {
        print("get fish ${++i}");
      }
      print("fish sucess");
    } on GrpcError catch (e) {
      print("$e");
    }
  }

  Future requestFish() async {
    var controller = StreamController<FishRequest>();
    try {
      // 發送 請求
      var i = 0;
      print("set fish ${i + 1}");
      controller.add(FishRequest());

      await for (var _ in stub.fish(controller.stream)) {
        print("get fish ${i + 1}");
        i++;
        if (i == 2) {
          // 發送了 2次 請求 結束
          // 關閉 stream 使用 stream 函數 返回
          controller.close();
          break;
        }
        print("set fish ${i + 1}");
        controller.add(FishRequest());
      }
      print("fish sucess");
    } on GrpcError catch (e) {
      print("$e");
    }
  }
}
```

# TLS

要 使用 tls 只需要在 創建 ClientChannel 時 指定 ChannelCredentials.secure 到 credentials 即可

```dart
class Client {
  // 通道 是到 服務器間 虛擬數據 交互 管道
  ClientChannel channel;

  static bool _onBadCertificate(X509Certificate x, String host) => true;
  dila() {
    // 創建 通道
    channel = ClientChannel(
      "127.0.0.1",
      port: 1102,
      options: const ChannelOptions(
        credentials: const ChannelCredentials.secure(
          onBadCertificate: _onBadCertificate, // 函數 返回 true 不驗證 服務器 證書
        ), // h2
      ),
    );
  }
```

# metadata

dart 中 metadata 就是  Map&lt;String,String&gt;

可以在 創建 client stub 時 指定 metadata 此 stub 的 所有 請求 都會 帶上此 metadata

```dart
// 創建 全局 的調用 上下文
stub = ServiceClient(
	channel,
	options: CallOptions(
		timeout: Duration(
			seconds: 10,
		),
		metadata: {
			"name": "king",
		},
	),
);
```

也可以 在每次調用 時 指定 metadata 此時 只對 當前 調用 有效 如果 stub 有 metadata 會 合併 覆蓋 stub 設置

```dart
Future<void> requestPenguin() async {
	try {
		// 調用
		await stub.penguin(
			PenguinRequest(),
			options: CallOptions(metadata: {
				"lv": "10",
				"name": "jj",
			}),
		);
		print("penguin sucess");
	} on GrpcError catch (e) {
		print("$e");
	}
}
```


