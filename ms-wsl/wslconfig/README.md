# [高級設定](https://learn.microsoft.com/zh-cn/windows/wsl/wsl-config)

wsl 提供來兩個配置檔案，來定義一些細節

* **wsl.conf** 爲 wsl 1 和 wsl 2 上運行的特定發行版本 進行設定 (檔案路徑是要設定的子系統中 **/etc/wsl.conf** )
* **.wslconfig** 這個只在 wsl2 中有效，爲 全局 配置設定 (檔案路徑是宿主機的 **%UserProfile%/.wslconfig** )

> wsl.conf 需要 windows 版本 17093 或更高版本

# wsl.conf

wsl.conf 以 ini 格式定義在子系統的 **/etc/wsl.conf**，用於指定這個子系統的一些特定設定

它支持如下 section

* boot
* automount
* network
* interop
* user


## boot

這個定義 wsl 啓動相關

| key | 類型 | 默認值 | 說明 | 要求 |
| -------- | -------- | -------- | -------- | -------- |
| command     | string     |      | 子系統啓動時運行的命令，此命令以 root 身份運行     | Windows 11 和 Server 2022     |
| systemd     | boolean     | false     | 以支持 systemd 的形式啓動子系統    | wsl版本在 0.67.6+ 之上，使用 wsl --version 確認      |

```
[boot]
systemd=true
```

## automount


| key | 類型 | 默認值 | 說明 |
| -------- | -------- | -------- | -------- |
| enabled | boolean | true | 如果爲true自動將宿主機磁盤掛接到子系統的 /mnt/ 下 |
| mountFsTab | boolean | true | 如果爲true要處理 /etc/fstab 設定 |
| root | string | /mnt/ | 要自動掛接到子系統的哪個目錄 |
| options | 以逗號分隔的 uid gid ... |  | 掛接選項 |

options 提供如下值用於控制如何掛接檔案的細節

| key | 說明 | 默認值 |
| -------- | -------- | -------- |
| uid     | 掛接檔案所有者 id     | 子系統默認用戶 id (通常是 1000)     |
| gid     | 掛接檔案所有組 id      | 子系統默認組 id (通常是 1000)     |
| umask     | 對所有檔案及目錄的權限排除八進制的掩碼     | 000     |
| fmask     | 對所有檔案的權限排除八進制的掩碼     | 000     |
| dmask     | 對所有目錄的權限排除八進制的掩碼     | 000     |
| metadata     | 身份將元數據添加到 Windows 檔案以支持 Linux 的權限系統     | disabled     |
| [case](https://learn.microsoft.com/en-US/windows/wsl/case-sensitivity)     | 掛接目錄中路徑是否區分大小寫有效值爲 dir off force     | off     |


## network

network 網路設定

| key | 類型 | 默認值 | 說明 |
| -------- | -------- | -------- | -------- |
| generateHosts | boolean | true | 如果爲 true，在 /etc/hosts 檔案中創建 hostname 的靜態 ip 映射 |
| generateResolvConf | boolean | true | 如果爲 true，在 resolv.conf 中包含宿主機 ip 作爲 dns 解析服務器 |
| hostname | string | 宿主機的 hostname | 設置子系統的 hostname |

## interop

interop 指定子系統和宿主機互操作，需要 Windows 17713 或更高版本

| key | 類型 | 默認值 | 說明 |
| -------- | -------- | -------- | -------- |
| enabled | boolean | true | 如果爲 true，子系統中可以啓動 windwos 進程 |
| appendWindowsPath | boolean | true | 如果爲 true，將 Windows 的 PATH 環境變量的值添加到 子系統的 PATH 變量中 |

強烈建議把 appendWindowsPath 設置爲 false，把 enabled 宿主爲 true，很少需要在子系統中執行 windwos 命令把 windwos 的 PATH 加入到子系統中會嚴重影響 bash completion 的效率從而極大降低 linux shell 的使用體驗，對於少數要啓動的 windows 命令可以添加別名到 **~/.bashrc** 例如

```
alias caja=/mnt/c/Windows/explorer.exe
```

## user

user 需要 windwos 版本 18980 或以上版本

| key | 類型 | 默認值 | 說明 |
| -------- | -------- | -------- | -------- |
| default | string | 首次運行時創建的初始用戶名 | wsl 在啓動時，使用哪個用戶運行進程 |

## example

```
#info="/etc/wsl.conf"
# Automatically mount Windows drive when the distribution is launched
[automount]

# Set to true will automount fixed drives (C:/ or D:/) with DrvFs under the root directory set above. Set to false means drives won't be mounted automatically, but need to be mounted manually or with fstab.
enabled = true

# Sets the directory where fixed drives will be automatically mounted. This example changes the mount location, so your C-drive would be /c, rather than the default /mnt/c. 
root = /

# DrvFs-specific options can be specified.  
options = "metadata,uid=1003,gid=1003,umask=077,fmask=11,case=off"

# Sets the `/etc/fstab` file to be processed when a WSL distribution is launched.
mountFsTab = true

# Network host settings that enable the DNS server used by WSL 2. This example changes the hostname, sets generateHosts to false, preventing WSL from the default behavior of auto-generating /etc/hosts, and sets generateResolvConf to false, preventing WSL from auto-generating /etc/resolv.conf, so that you can create your own (ie. nameserver 1.1.1.1).
[network]
hostname = DemoHost
generateHosts = false
generateResolvConf = false

# Set whether WSL supports interop process like launching Windows apps and adding path variables. Setting these to false will block the launch of Windows processes and block adding $PATH environment variables.
[interop]
enabled = false
appendWindowsPath = false

# Set the user when launching a distribution with WSL.
[user]
default = DemoUser

# Set a command to run when a new WSL instance launches. This example starts the Docker container service.
[boot]
command = service docker start
```

# .wslconfig

.wslconfig 同樣用 ini 格式定義，路徑爲 **%UserProfile%/.wslconfig**

目前有如下 section
* wsl2

## wsl2

wsl2 定義 wsl2 子系統的一些全局設定

類型說明中帶 \* 的 key 只在 windwos 11 可用

| key | 類型 | 默認值 | 說明 |
| -------- | -------- | -------- | -------- |
| kernel | path | windwos 內置的一個內核路徑 | 自定義linux內核的絕對 windows 路徑，可用於替換 linux 內核 |
| memory | size | 宿主機上總內存的 50%(20175 版本之前是 80%) 或 8GB 取兩者較小的值 | 要分配給子系統的內存量 |
| processors | number | 與宿主機上的一致 | 要分配給子系統的 cpu 數量 |
| localhostForwarding | boolean | true | 如果爲true，在宿主機自動映射通過 localhost:port 可訪問子系統端口  |
| kernelCommandLine | string |  | 其它的內核命令參數 |
| swap | size | 宿主機 25% 內存四捨五入到最近的 GB | 子系統交換空間大小，0表示無交換空間 |
| swapFile | path | %USERPROFILE%\AppData\Local\Temp\swap.vhdx | 交換空間在宿主機上的絕對路徑 |
| pageReporting | boolean | true | 設置爲 true 使 宿主機能夠回收分配給WSL2虛擬機的未使用內存 |
| [guiApplications](https://github.com/microsoft/wslg) | boolean\* | true | 如果爲 true，允許在子系統中打開或關閉GUI應用 |
| debugConsole | boolean\* | false | 用於在子系統中啓動時打開顯示 dmesg 內容的輸出到控制檯窗口 |
| nestedVirtualization | boolean\* | true | 打開或關閉嵌套在子系統中的虛擬化，比如可以在子系統中運行另外一個vm軟體 |
| vmIdleTimeout | number\*	 | 60000 | 子系統在關閉之前處於空閒狀態的毫秒數 |

強烈建議使用 **memory** 控制子系統內存，因爲 linux 即使內存釋放也會將這部分內存保持與緩存中以供下次分配內存時可以快速分配，這在 linux 中顯示爲 buff/cache 這部分內存沒有被使用只是爲了快速分配給下次申請內存而做的系統優化，但是宿主機並不知道，在宿主機看來這部分內存被佔用著所以無法回收，並且 linux 通常也不會主動將 buff/cache 的內存釋放掉，導致的結果就是長時間運行 wsl 子系統，子系統肯定會向宿主機申請到它最多可申請的內存並且一致不會被宿主機回收。

同時強烈建議將 **localhostForwarding** 設置爲 false，否則 windwos 會主動將 localhost:port 映射到子系統中，相當於子系統的進程會佔用宿主機的端口，這在大量使用子系統時很容易產生衝突導致你的宿主機一些網路配置失效或出現奇怪 bug


## example 

```
#info="%UserProfile%/.wslconfig"
# Settings apply across all Linux distros running on WSL 2
[wsl2]

# Limits VM memory to use no more than 4 GB, this can be set as whole numbers using GB or MB
memory=4GB 

# Sets the VM to use two virtual processors
processors=2

# Specify a custom Linux kernel to use with your installed distros. The default kernel used can be found at https://github.com/microsoft/WSL2-Linux-Kernel
kernel=C:\\temp\\myCustomKernel

# Sets additional kernel parameters, in this case enabling older Linux base images such as Centos 6
kernelCommandLine = vsyscall=emulate

# Sets amount of swap storage space to 8GB, default is 25% of available RAM
swap=8GB

# Sets swapfile path location, default is %USERPROFILE%\AppData\Local\Temp\swap.vhdx
swapfile=C:\\temp\\wsl-swap.vhdx

# Disable page reporting so WSL retains all allocated memory claimed from Windows and releases none back when free
pageReporting=false

# Turn off default connection to bind WSL 2 localhost to Windows localhost
localhostforwarding=true

# Disables nested virtualization
nestedVirtualization=false

# Turns on output console showing contents of dmesg when opening a WSL 2 distro for debugging
debugConsole=true
```