# ng

```
<!DOCTYPE html>
<html ng-app>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">

    <title>angularjs 測試</title>

    <!-- 引入ng依賴 -->
    <script src="asset/js/angular.min.js"></script>
</head>

<body>
    <div>
        <label>Name:</label>
        <input type="text" ng-model="name" placeholder="請輸入名字">
        <hr>
        <h1>Hellow {{name}}</h1>
        <h1>Hellow <span ng-bind="name"></span></h1>
    </div>
</body>

</html>
```

AngularJS 擴展了 html  
使用 ng-XXX 定義這些擴展

* 第2行 **ng-app** 定義流一個 ng 程式 通常一個頁面 只要一個
* 第19行 **ng-model** 將一個 ng自定義變量 **name** 和 input輸入域的值 進行雙向綁定(任何一端變化 都會自動更新到另一端)
* 第21行 **{{}}** 將 ng 表達式 自動 更新過來
* 第22行 **ng-bind** 將 ng 表達式 自動 更新為 元素的 innerHTML

# 表達式
**{{}}** 或 **ng-bind** 中 指定的 值為 ng表達式

ng表達式 可以包含 變量 運算符 常量 文本 (文本 需要加上 **'** 或 **"**)

**ng-init** 可以初始化 一些 變量 以便提供給 ng表達式 使用

```
<!DOCTYPE html>
<html ng-app>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">

    <title>angularjs 測試</title>

    <!-- 引入ng依賴 -->
    <script src="asset/js/angular.min.js"></script>
</head>

<body>
    <!-- 數字 -->
    <div ng-init="quantity=1.3;cost=5">
        <p>總價：{{ quantity * cost }}</p>
    </div>
    <!-- 字符串 -->
    <div ng-init="firstName='kate';lastName='beckinsale'">
        <p>姓名：{{ firstName + " " + lastName }}</p>
    </div>
    <!-- 對象 不支持函數 -->
    <div ng-init="beauty={firstName:'kate',lastName:'beckinsale'}">
        <p>姓名：{{ beauty.firstName + " " + beauty.lastName }}</p>
    </div>
    <!-- 數組 -->
    <div ng-init="strs=['kate','beckinsale']">
        <p>姓名：{{ strs[0] + " " + strs[1] }}</p>
    </div>
</body>

</html>
```

# 指令
AngularJS 通過 指令 擴展流 html 內置指令 完成了常用功能

## ng-repeat

ng-repeat 可以遍歷 數組/對象 的值 並重複生產 當前元素

```
<ol ng-init="beauty=['kate','anita','anna']">
	<li ng-repeat="name in beauty">
		{{name}}
	</li>
</ol>
```

#  控制器

**ng-controller** 定義一個 控制器

AngularJS 以 控制器 區分 作用域

## controller
controller 方法 註冊一個 控制器 方法 其中的 $scope 變量 可以操縱 當前 控制器範圍內的 變量

```
<!DOCTYPE html>
<html ng-app="app">
 
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>AngularJS 測試</title>
	<script src="angular1.6.9.min.js"></script>
</head>
 
<body>
	<div ng-controller="ctrl-0">
		{{beauty}}
	</div>
	<div ng-controller="ctrl-1">
		{{beauty}}
		{{name}}
	</div>
</body>
 
</html>
<script>
	// 創建/返回 模塊
	var app = angular.module("app", []);
	// 為 控制器 註冊 方法
	app.controller('ctrl-0', function ($scope) {
		$scope.beauty = "kate beckinsale";
	});
	app.controller('ctrl-1', function ($scope) {
		$scope.beauty = "anna lee";
	});
</script>
```

一個 控制器 只能有一個 方法 如果定義了 多個 只有最後一個會 生效

# 事件

## ng-click

* ng-click ... 等 可以 綁定 事件
* $event 可以停止 瀏覽器 默認事件 傳遞

```
<a href="" ng-click="doSomething()">
	<span class="glyphicon glyphicon-th-list" ng-click="doSomethingElse($event)"></span>
</a>
```

```
$scope.doSomethingElse = function(e) {
	// do something
 
	// 終止瀏覽器 事件傳遞
	e.stopPropagation();
	e.preventDefault();
};
```

