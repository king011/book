# videojs-playlist

[videojs-playlist](https://github.com/brightcove/videojs-playlist) 是一個開源(Apache 2.0)的 video.js 播放列表插件

```
npm install videojs-playlist
```

```
<!-- 導入 video.js 樣式 和 腳本 -->
<link href="css/video-js.min.css" rel="stylesheet">
<script src="js/video.min.js"></script>
<script src="js/videojs-playlist.min.js"></script>
```

```
import videojs from "video.js"
import { VideoJsPlayer } from "video.js"
interface Playlist {
    /**
      * 返回當前列表索引，如果爲 -1 則表示正在播放列表之外內容
      */
    currentItem(): number
    /**
     * 設置當前播放列表
     * @param i 
     */
    currentItem(i: number): void

    contains(val: string | Object | Array<any>): boolean
    indexOf(val: string | Object | Array<any>): number
    /**
     * 和 currentItem 函數相同
     */
    currentIndex(): number
    /**
     * 返回列表下個內容索引，如果當前在末尾則返回 0，如果在播放列表外內容返回 -1
     */
    nextIndex(): number
    /**
     * 返回列表上個內容索引，如果當前爲 0 則返回 0，如果當前爲 0 且設置了 repeat 則返回 最後一個索引，如果在播放列表外內容返回 -1
     */
    previousIndex(): number
    /**
     * 返回最後一個索引
     */
    lastIndex(): number
    first(): Object | undefined
    last(): Object | undefined
    next(): Object | undefined
    previous(): Object | undefined
    /**
     * 設置在每個視頻結束後延遲多久播放下一個
     * @param delay 延遲多少秒
     */
    autoadvance(delay: number): undefined
    /**
     * 啓用或禁用重複，如果設置
     * @param val 
     */
    repeat(val: boolean): undefined
    repeat(): boolean
    sort(compare: (l: any, r: any) => number): undefined
    /**
     * 反轉列表
     */
    reverse(): undefined
    /**
     * 隨機化 播放列表順序
     */
    shuffle(): undefined
}
interface Player extends VideoJsPlayer {
    readonly playlist: Playlist
}
const player = videojs('player', // html id
    {
        controls: true,
        preload: 'auto',
        autoplay: true,
        // 初始化插件
        plugins: {
            playlist: [
                {
                    sources: [
                        {
                            src: 'movie/001.mp4',
                            type: 'video/mp4',
                        }
                    ],
                    poster: 'image/poster.webp',
                    name: '001.mp4',
                    // 設置字幕
                    textTracks: [
                        {
                            src: 'movie/001.vtt',
                            kind: 'captions',
                            srclang: 'zh',
                            label: 'Chinese',
                            default: true,
                        },
                    ],
                },
                {
                    sources: [
                        {
                            src: 'movie/002.mp4',
                            type: 'video/mp4',
                        }
                    ],
                    name: '002.mp4',
                    textTracks: [
                        {
                            src: 'movie/002.vtt',
                            kind: 'captions',
                            srclang: 'zh',
                            label: 'Chinese',
                            default: true,
                        },
                    ],
                },
            ],
        },
    },
    function () {
        this.on('ended', () => {
            // 播放結束
            console.log('ended')
        })
    },
) as Player
player.playlist.autoadvance(0)
```