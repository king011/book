# PrimeNG

PrimeNG 是一個開源(MIT) 的 angular 組件庫，提供了很多實用的 ui

* 官網 [https://www.primefaces.org/primeng/](https://www.primefaces.org/primeng/)
* 源碼 [https://github.com/primefaces/primeng](https://github.com/primefaces/primeng)

PrimeNG 大量使用了 PrimeFlex 你可以[在此](web-css-PrimeFlex/0)找到 PrimeFlex 的相關用法

此外由於 PrimeNG 內容太多，這裏只列出了安裝以及一些常用的基礎組件和一些不便分類的少量組件，更多組件用法你可以點擊下述鏈接查看：

* [菜單](google-angular2-PrimeNG-Menu/0)
* [FORM](google-angular2-PrimeNG-FORM/0)