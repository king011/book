# 組件
Angular 的組件 構造了 顯示ui 通常由 一個 ts .html css 組成
* **css** 定義了 組件 的 樣式
* **html** 是 Angular 模板
* **ts** 構成 組件 代碼

使用 ng generate component home  
可以在 app/home 檔案夾下 創建一個 HomeComponent 組件
```ts
import { Component, OnInit } from '@angular/core';
 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  constructor() { }
 
  ngOnInit() {
  }
 
}
```

## 定義屬性
要爲組件 定義屬性 只需要創建 class 的 屬性 即可
通常 在 構造函數中 初始化屬性
```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // 定義一個 title 屬性
  title: string;
  // 定義屬性的 同時 初始化值
  id: number = 1;

  constructor() {
    // 初始化 屬性
    this.title = "this is home";
  }

  ngOnInit() {
  }

}
```
## set get
可以 使用 set get 將 函數 定義 爲屬性 這樣 可以將函數 想屬性一樣使用  
如果沒定義 set 則無法 寫入 沒定義 get 則值爲 **undefined**
```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private _id:number = 0;
  get id():number{
    console.log("get");
    return this._id;
  }
  set id(val:number){
    console.log("set");
    this._id = val;
  }
  constructor() {
    // 設置 屬性
    this.id = 123;
    // 返回屬性
    console.log(this.id);
  }
  ngOnInit() {
  }
}
```
> 使用 set get 屬性 可以方便的在對屬性 進行讀寫前 進行 過濾 或調用 其它方法


# 模板
模板 提供方便的 動態創建 html 以及與 關聯組件交互的 方法  
模板 擴展了 html 提供了 許多 方便的 擴展功能  
模板 幾乎 兼容 html 但 有過濾了 script 標籤

## bind
使用 *{{}}* 把 組件 屬性 綁定到 模板
```html
<p id="{{id}}">
  {{title}}
</p>
```

使用 *\[\]* 進行 屬性綁定  
可以使用 \[class\.XXX\] 只對某個 css 綁定  
同理使用 \[style\.XXX\] 只對某個 style 綁定
```html
<button mat-button (click)="isRead=!isRead">toggle red</button>
<p [id]="id" [class.red]="isRead">
  {{title}}
</p>
```

使用 *\(\)* 可以  模板事件 綁定到 組件  
可選的 **$event** 參數 包含了 事件 具體信息
```html
<p (click)="onClick($event)">
  {{title}}
</p>
```
> $event 是標準的瀏覽器 event 可以調用 瀏覽器 event提供的 方法 比如調用 $event.stopPropagation() 阻止事件冒泡

## 雙向綁定

雙向綁定 其實 只是 語法糖罷了
```html
<app-sizer [(size)]="fontSizePx"></app-sizer>
```
```html
<app-sizer [size]="fontSizePx" (sizeChange)="fontSizePx=$event"></app-sizer>
```

**NgModel** 爲常用的 form 表單 提供了進一步的 語法糖 以便如預期的處理 form表單 和 組件屬性
```ts
import { FormsModule } from '@angular/forms';
```
```html
<input [(ngModel)]="name">
```

## Example

### 屬性

* 元素 property
* 組件 property
* 指令 property

```html
<img [src]="heroImageUrl">
<app-hero-detail [hero]="currentHero"></app-hero-detail>
<div [ngClass]="{'special': isSpecial}"></div>
```
### 事件

* 元素的事件
* 組件的事件
* 指令的事件

```html
<button (click)="onSave()">Save</button>
<app-hero-detail (deleteRequest)="deleteHero()"></app-hero-detail>
<div (myClick)="clicked=$event" clickable>click me</div>
```
### 雙向

* 事件 與 property

```html
<input [(ngModel)]="name">
```

### Attribute

* attribute

```html
<button [attr.aria-label]="help">help</button>
```

### css 類

* class property

```html
<div [class.special]="isSpecial">Special</div>
```

### 樣式

* style property

```html
<button [style.color]="isSpecial ? 'red' : 'green'">
```

# 生命週期鉤子

每個 組件 都有一個 被 angular 管理的 生命週期 angular 提供了鉤子 讓使用者 能夠 捕獲

## OnChanges

* 每當 組件 重新設置 輸入屬性時 調用此鉤子 並傳入一個 SimpleChanges 對象\(記錄了 變化的 輸入屬性\)

* 只有外部輸入 變化 才會回調 在組件 內部 直接修改 不會 回調

* ngOnChanges 首次調用 會在 ngOnInit 之前

```ts
import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, OnChanges {
  // 定義輸入屬性
  @Input()
  val: string

  constructor() { }

  ngOnInit() {

  }
  // 實現 OnChanges 接口
  ngOnChanges(changes: SimpleChanges) {
    console.log("變化列表")
    for (let name in changes) {
      console.log("屬性名 :", name)
      const ele = changes[name]
      console.log("當期值 :",ele.currentValue)
      console.log("之前值 :",ele.previousValue)
      console.log("首次變化 :",ele.firstChange)
    }
  }
}
```

> * 首此 輸入 undefined 空字符串 也會 調用 OnChanges 鉤子
> * 如果 父組件 不綁定 此屬性 不會調用 OnChanges
> * 不可以在 回調中 修改 currentValue 的值

## OnInit

* 當 第一次 顯示數據綁定和設置 指令/組件的 輸入屬性之後 初始化 指令/組件

* ngOnInit 只會被調用 一次 通常在此 訂閱 可觀察對象 初始化 服務 ...

```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    console.log("init")
  }

}
```

## DoCheck

* 在每個 angular 變更檢查週期中 調用 \(在 OnInit, OnChanges 之後\)

```ts
import { Component, OnInit, DoCheck } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, DoCheck {

  constructor() { }

  ngOnInit() {

  }

  ngDoCheck() {
    console.log("DoCheck")
  }
}
```

## AfterContentInit

* 當把內容 投影進 組件後 調用一次 在 首次 DoCheck 之後

```ts
import { Component, OnInit, AfterContentInit } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, AfterContentInit {

  constructor() { }

  ngOnInit() {

  }

  ngAfterContentInit() {
    console.log("AfterContentInit")
  }
}
```

## AfterContentChecked

* 每次 完成 被投影組件內容 的變更檢查之後 在 DoCheck AfterContentInit 之後

```ts
import { Component, OnInit, AfterContentChecked } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, AfterContentChecked {

  constructor() { }

  ngOnInit() {

  }

  ngAfterContentChecked() {
    console.log("AfterContentChecked")
  }
}
```

## AfterViewInit

* 初始化完 組件 及其子組件 後被 調用一次

* 通常在此之後 才操作 document 才是安全的 做法 可以看着 舊時代的 document ready

```ts
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    console.log("AfterViewInit")
  }
}
```

## AfterViewChecked

* 每次 完成 組件 和 子組件的 變更檢查 之後

```ts
import { Component, OnInit, AfterViewChecked } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, AfterViewChecked {

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewChecked() {
    console.log("AfterViewChecked")
  }
}
```

## OnDestroy

* 當 組件/指令 銷毀之前被 調用一次

* 應該在此 釋放資源 以免 內容泄漏 比如 關閉 定時器 反 訂閱可觀察對象 分離事件處理器

```ts
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {

  }

  ngOnDestroy() {
    console.log("OnDestroy")
  }
}
```