# gopkg.in

gopkg.in 爲golang 提供了一種穩定api的發佈方式 

將 到 gopkg.in 的 go get 請求 轉發到 github

```
gopkg.in/pkg.v3      → github.com/go-pkg/pkg (branch/tag v3, v3.N, or v3.N.M)
gopkg.in/user/pkg.v3 → github.com/user/pkg   (branch/tag v3, v3.N, or v3.N.M)
```

官網 [http://labix.org/gopkg.in](http://labix.org/gopkg.in)