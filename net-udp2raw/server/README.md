# server

最簡單的推薦的部署方案是使用 docker，你可以依據自己的情況打包 docker 容器，或者使用本喵打包的 [amd64](https://hub.docker.com/r/king011/udp2raw) 容器

```
services:
  wireguard:
    image: lscr.io/linuxserver/wireguard:latest
    container_name: wireguard
    cap_add:
      - NET_ADMIN
      - SYS_MODULE #optional
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Asia/Shanghai
      - SERVERURL=king011.tpddns.cn #optional
      - SERVERPORT=51820 #optional
      - PEERS=2 #optional
      - PEERDNS=auto #optional
      - INTERNAL_SUBNET=10.13.13.0 #optional
      - ALLOWEDIPS=0.0.0.0/0 #optional
      - PERSISTENTKEEPALIVE_PEERS= #optional
      - LOG_CONFS=true #optional
    volumes:
      - /opt/data/wireguard:/config
      # - /lib/modules:/lib/modules #optional
    ports:
      - 51820:51820/udp
      - 51820:51821/tcp
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    restart: unless-stopped
  wireguard-fake:
    image: king011/udp2raw:20230206.0
    cap_add:
      - NET_ADMIN
    restart: unless-stopped
    network_mode: service:wireguard
    command: [
      "udp2raw_amd64", "-s",
      "-l", "0.0.0.0:51821",
      "-r", "127.0.0.1:51820",
      "-k", "passwd",
      "--raw-mode", "faketcp",
      "--cipher-mode", "xor",
      -a
    ]
```

* wireguard 服務使用 udp 在 51820 端口上創建了一個 wireguard 服務程序
* wireguard-fake 使用 udp2raw_amd64 將 51821 的 fake tcp 數據還原爲 udp 後轉發給 udp 51820 端口

> udp2raw_amd64 轉發目標最好是 127.0.0.1 因爲本喵時測試發現非此地址可能只有 send 的數據沒有recv 的數據，原因未知

