# adb

adb 是 android 開發包 中 提供的 工具 通常 位於 **Sdk/platform-tools** 中

使用 adb 可以 進行 對 android 模擬器/手機 進行 控制 以便 開發調試等

```sh
#info="常用指令"

#  查看 當前連接的 android 設備
adb devices

# 登錄到設置 打開一個 shell 環境
# 打開 shell 後 可以執行 su root 切換到 root 用戶
adb shell

# 安裝 套件
adb install xxx.apk

# 卸載 套件
# -k 可以 保留配置 和 緩存
adb uninstall 套件包名
adb uninstall -k 套件包名

# 將 本機 檔案 傳輸到 設備
adb push 本機路徑 設備路徑

# 將 設備檔案 下載到 本機
adb pull 設備路徑 本機路徑
```