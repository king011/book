# 流程

1. 客戶端和服務器 協商 認證方式 並認證
2. 客戶端 發送 socks5 請求
3. 轉發數據

# 認證

1. 客戶端 向服務器 發送 一個 支持的 認證列表

  | VER | NMETHODS | METHODS |
  | -------- | -------- | -------- |
  | 1     | 1     | 1-255     |
	
	* VER 是socks版本 對於 socks5來說 必須是 0x05
	
	* NMETHODS 指定了 METHODS 長度
	
	* METHODS 是一個 byte 數組 每個字節 指定了 客戶端支持的 認證方式
	    * 0x00 不需要認證 
	    
	    * 0x01 GSSAPI
	    
	    * 0x02 使用 用戶名/密碼 認證
	    
	    * \[ 0x03 , 0x7F \] 由 IANA分配(保留)
	    
	    * \[ 0x80 , 0xFE \] 爲私人方法 保留
	    
	    * 0xFF 無可接受的 方法
	    
			> 常用的服務器客戶端 都只實現了 0x00 0x02 兩種 認證方式
	

1. 服務器 向 客戶端 返回 使用的 認證方式

  | VER | METHOD | 
  | -------- | -------- | 
  | 1     | 1     |
	
	* VER 是socks版本 對於 socks5來說 必須是 0x05

	* METHOD 是服務器選擇的 認證方式 
	
    	* 如果 爲 0xFF 則代表 沒有可用的 認證方案 客戶應該 關閉連接
    	* 如果爲 0x00 直接進入 下一階段 進行 socks5 請求
    	* 其它值時 按照定義進行 認證 
	
## 用戶名/密碼 認證

1. 客戶端 向服務器 發送 用戶名/密碼

  | VER | ULEN | UNAME | PLEN | PASSWD |
  | -------- | -------- | -------- | -------- | -------- |
  | 1     | 1     | 1-255     | 1     | 1-255     |
	
	VER 目前為 0x01 

1. 服務器 響應認證結果

  | VER | STATUS |
  | -------- | -------- |
  | 1     | 1     |

  STATUS 0x00 表示成功，其它值表示失敗 客戶端應該斷開連接
# socks5 請求

1. 客戶端 向服務器 發送 一個 socks5 請求

  | VER | CMD | RSV | ATYP | DST.ADDR | DST.PORT |
  | -------- | -------- | -------- | -------- | -------- | -------- |
  | 1     | 1     | 1     | 1     | 動態長度     | 2     |
	
	* VER 是socks版本 對於 socks5來說 必須是 0x05
	
	* CMD 請求指令
	
	    * 0x01 Connect
	    * 0x02 BIND
	    * 0x03 UDP轉發
	
	* RSV 0x00 保留
	
	* ATYP 指定 DST.ADDR 的類型
	
	    * 0x01 DST.ADDR 是 IPv4 位址 4 bytes
	    * 0x03 DST.ADDR 是域名 DST.ADDR 第一個字節爲 域名長度 沒有 \0 結尾
	    * 0x04 IPv6 位址 16 bytes
	    
	* DST.ADDR 目的地址
	
	* DST.PORT 網路位元組序標識的 目的端口

1. 服務器 回應 請求

  | VER | REP | RSV | ATYP | BND.ADDR | BND.PORT |
  | -------- | -------- | -------- | -------- | -------- | -------- |
  | 1     | 1     | 1     | 1     | 動態長度     | 2     |

	* VER 是socks版本 對於 socks5來說 必須是 0x05
	
	* REP 應答
	
	    * 0x00 表示成功
	    * 0x01 普通SOCKS伺服器連接失敗
	    * 0x02 現有規則不允許連接
	    * 0x03 網路不可達
	    * 0x04 主機不可達
	    * 0x05 連接被拒
	    * 0x06 TTL超時
	    * 0x07 不支援的命令
	    * 0x08 不支援的位址類型
	    * \[ 0x09 , 0xFF \] 未定義
	
	* RSV 0x00 保留
	
	* ATYP 指定 BND.ADDR 的類型
	
	    * 0x01 BND.ADDR 是 IPv4 位址 4 bytes
	    * 0x03 BND.ADDR 是域名 BND.ADDR 第一個字節爲 域名長度 沒有 \0 結尾
	    * 0x04 IPv6 位址 16 bytes
	    
	* BND.ADDR 伺服器地址
	
	* BND.PORT 網路位元組序標識的 伺服器端口

> BND.ADDR BND.PORT 只在 BIND 請求時 有效 其它請求時 通常  ATYP 返回 0x01 BND.ADDR BND.PORT 返回 用 0 補齊的數據即可
> 
> {0x01,0x00,0x00,0x00,0x00,0x00,0x00}

