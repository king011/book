# concatAll

concatAll 將 Observable&lt;Observable&lt;T&gt;&gt;  發送來的 Observable&lt;T&gt; 依次處理 將 Observable&lt;T&gt; 發來的數據 next 出去

```
import { of } from 'rxjs'
import { concatAll, delay } from 'rxjs/operators'

of(
    of(1, 2, 3),
    of(4, 5, 6).pipe(delay(1000)),
    of(7, 8, 9).pipe(delay(1000)),
).pipe(
    concatAll(),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

一定要前個 Observable&lt;T&gt; complete 後一個 Observable&lt;T&gt; 才會被處理

# switchAll

switchAll 同樣 處理 Observable&lt;Observable&lt;T&gt;&gt; 但 每當有新的 Observable&lt;T&gt; 發來值 則會 退訂 之前的 Observable&lt;T&gt;

```
import { interval } from 'rxjs';
import { switchAll, map, take } from 'rxjs/operators';
interval(1000).pipe(
    take(3),
    map((i) => {
        return interval(100).pipe(map(v => i * 100 + v % 10))
    })
).pipe(
    switchAll()
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# mergeAll

mergeAll 同樣 處理 Observable&lt;Observable&lt;T&gt;&gt; 並且並行 處理所有的 Observable&lt;T&gt; 

mergeAll 也可以傳入一個 參數 指定 最大並行量 超過最大量的 Observable&lt;T&gt;  需要等待 正在處理的 Observable&lt;T&gt;  complete 才會被處理

```
import { interval } from 'rxjs';
import { mergeAll, map, take } from 'rxjs/operators';
interval(1000).pipe(
    take(3),
    map((i) => {
        return interval(100).pipe(map(v => i * 100 + v % 10))
    })
).pipe(
    mergeAll()
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# concatMap

concatMap 實際上就是 map + concatAll 的語法糖

```
import { of } from 'rxjs'
import { concatAll, delay, map } from 'rxjs/operators'

of(
    of(1, 2, 3),
    of(4, 5, 6).pipe(delay(1000)),
    of(7, 8, 9).pipe(delay(1000)),
).pipe(
    map(v => v.pipe(map(v => v * 10))),
    concatAll(),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

```
import { of } from 'rxjs'
import { concatMap, delay, map } from 'rxjs/operators'

of(
    of(1, 2, 3),
    of(4, 5, 6).pipe(delay(1000)),
    of(7, 8, 9).pipe(delay(1000)),
).pipe(
    concatMap((observable) => observable.pipe(map((v => v * 10)))),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# switchMap

switchMap 實際上就是 map + switchAll 的語法糖

```
import { interval } from 'rxjs';
import { switchAll, map, take } from 'rxjs/operators';
interval(1000).pipe(
    take(3),
    map((i) => {
        return interval(100).pipe(map(v => i * 100 + v % 10))
    })
).pipe(
    map(v => v.pipe(map(v => v * 10))),
    switchAll()
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

```
import { interval } from 'rxjs';
import { switchMap, map, take } from 'rxjs/operators';
interval(1000).pipe(
    take(3),
    map((i) => {
        return interval(100).pipe(map(v => i * 100 + v % 10))
    })
).pipe(
    switchMap(v => v.pipe(map(v => v * 10))),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# mergeMap

mergeMap 實際上就是 map + mergeAll 的語法糖

```
import { interval } from 'rxjs';
import { mergeAll, map, take } from 'rxjs/operators';
interval(1000).pipe(
    take(3),
    map((i) => {
        return interval(100).pipe(map(v => i * 100 + v % 10))
    })
).pipe(
    map(v => v.pipe(map(v => v * 10))),
    mergeAll(),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

```
import { interval } from 'rxjs';
import { mergeMap, map, take } from 'rxjs/operators';
interval(1000).pipe(
    take(3),
    map((i) => {
        return interval(100).pipe(map(v => i * 100 + v % 10))
    })
).pipe(
    mergeMap(v => v.pipe(map(v => v * 10))),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```
