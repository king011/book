# [網卡設置](https://openwrt.org/docs/guide-user/network/ipv4/configuration)

1. 修改 **/etc/config/network** 檔案配置 網卡
2. 執行 **/etc/init.d/network reload** 重載網卡設置


# dhcp

通常原始的 network 定義如下

```
config interface 'loopback'
	option device 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd52:65b5:7190::/48'

config device
	option name 'br-lan'
	option type 'bridge'
	list ports 'eth0'

config interface 'lan'
	option device 'br-lan'
	option proto 'static'
	option ipaddr '192.168.1.1'
	option netmask '255.255.255.0'
	option ip6assign '60'
```

將 interface lan 改成 dhcp 就可以上網了


```
config interface 'loopback'
	option device 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd52:65b5:7190::/48'

config device
	option name 'br-lan'
	option type 'bridge'
	list ports 'eth0'

config interface 'lan'
	option device 'br-lan'
	option proto 'dhcp'
	option ip6assign '60'
```

# static

dhcp 是最簡單的方案可以用來測試網路是否正常，但 openwrt 通常作爲路由網關需要保持它的 ip 不變這時就需要使用 爲 'lan' 接口指定 ip 網段 網關 dns 等信息

```
config interface 'lan'
	option device 'br-lan'
	option proto 'static'
	option ipaddr '192.168.0.3'
	option netmask '255.255.255.0'
	option gateway '192.168.0.1'
	option dns '192.168.0.1'
	option ip6assign '60'
```