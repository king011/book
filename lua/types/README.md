# 型別

lua 是動態語言，變量不需要型別定義，只需要賦值即可，值可以存儲在任意變量中

lua 支持 8個基本的型別

| 型別 | 描述 |
| -------- | -------- |
| nil     | 只有 nil 值，表示一個無效值(在表達式中相當於 false)     |
| boolean     | 包含兩個值： false 和 true     |
| number     | 表示雙精度浮點     |
| string     | 字符串由一對雙引號或單引號表示     |
| function     | 由 C 或 lua 編寫的函數     |
| userdata     | 表示任意存儲在變量中的 C 數據     |
| thread     | 表示執行的獨立線路，用於執行協同程序     |
| table     | table 其實是一個“關聯數組”，數組的索引可以是 數字 字符串 或 table。在 lua 中 table 的創建是通過“構造表達式”來完成，最簡單的構造表達式是 {}，用來創建一個空 table     |

## type

內置的 **type** 函數可以返回變量型別名稱的字符串

```
print(type("Hello world")) --> string
print(type(10.4 * 3)) --> number
print(type(print)) --> function
print(type(type)) --> function
print(type(true)) --> boolean
print(type(nil)) --> nil
print(type(type(X))) --> string
```

# nil

nil 表示一種沒有任何有效值的狀態，它只有一個值 **nil**

* 全局變量的默認值爲 nil
* 將 nil 賦值給 全局變量 等同於 刪除此變量
* 給 table 的鍵賦值爲 nil，也等同與刪除此鍵

	```
	tab1 = { key1 = "val1", key2 = "val2", "val3" }
	for k, v in pairs(tab1) do
			print(k .. " - " .. v)
	end

	tab1.key1 = nil
	for k, v in pairs(tab1) do
			print(k .. " - " .. v)
	end
	```

# boolean

boolean 只有 **false** 和 **true** 兩個值

lua 將 **nil** 和 **false** 視爲假，其它都視爲真。(所以 空字符串 數字0 都是真)

```
print(type(true)) --boolean
print(type(false)) --boolean
print(type(nil)) --nil

if false or nil then
    print("not print")
else
    print("false 和 nil 都是 假")
end

if 0 and "" then
    print("数字 0 和 空字符串都是 真")
else
    print("not print")
end
```

# number
* number 用於表示所有實數，lua沒有整型
* number 可以表示任意的32位整型，而不會存在四捨五入的問題(number 爲 64位浮點)

lua 亦可以在源碼 **luaconf.h** 中指定 number 使用的數字精度

數字可以使用 科學計數法 或 普通計數法 表示

```
a = 4
a = 4.0
a = 4.57e-3
a = 0.3e12
a = 5 + 20
a = 0x11 --十六進制
```

lua 不支持八進制和二進制寫法

# string

string 使用 8位編碼，字符可以是任意數值編碼，包括0

* 故可以存儲二進制數據
* string 和 java 類似是 const 的，每次字符串 sub 等操作，實際上是創建了一個新的字符串，故不要對大字符串進行這些操作，即時是將一個 1024 長的字符串尾添加一個字符也將重新創建一個長度爲 1025 的新字符串並拷貝原內容

## 字符串表示

使用 雙引號 或 單引號 表示字符串

```
print('string "string value"')
print("string 'string value'")
```

轉義字符和 c 一樣，此外 \數字 可以將一個數字作爲轉義字符

```
print("\97bcd") --abcd  \97 為 a (a的ascill編碼為 97)
```

使用 **[[]]** 可以定義非轉義字符串

```
-- 第一行若只有一個換行則會忽略
print([[
<html>
</html>]])
```

如果要表示的內容本身包含 **[[** 或 **]]** 可以使用 **[=[ ]=]** 定義字符串。(**=** 的數量是任意的只要，開始和結束匹配即可)

```
-- 第一行若只有一個換行則會忽略
print([=[
<html>]]
</html>]=])
```

```
-- 使用 .. 連接字符串
print("abc" .. "def" .. "ghi") --abcdefghi

-- 和數字計算會自動轉換數字
print("34" + 12) --46
print("034" + 12) --46
print("0x34" + 12) --64

-- 使用 # 計算字符串長度
print(#"123")
-- 對於字符串變量可以使用 # 或 len 函數計算長度
a = "123"
print(a:len())
print(#a)
```

# function

在 lua 中 函數被看作是第一類值(First-Class Value)，可以直接被存儲到變量中

```
function factorial1(n)
    if n == 0 then
        return 1
    else
        return n * factorial1(n - 1)
    end
end

print(factorial1(5))
factorial2 = factorial1
print(factorial2(5))
```

# userdata

userdata 是一種用戶自定義數據，用於表示有 c/c++ 創建的類型，可以是任意的 c/c++ 數據存儲到 lua 中

# thread
lua 其實並不支持線程，它支持的是協程(coroutine)，但協程和線程一樣有自己獨立的棧局部變量和指令指針

區別是協程在任意時刻只運行一個，並且只有被掛起(suspend)時才會切換 cpu 給其它協程執行
# table
* 使用 **{}** 創建 table
* 當沒有任何變量持有 table 時，table 會自動被釋放
* 除了向數組一樣使用 [] 指定 key 外亦可以使用 **.** 訪問鍵

```
a = {}
a["name"] = "illusive man"
a.hp = 1
a.mp = {}
a.mp.max = 10

print(a["name"])
print(a.name)
print(a.hp)
print(a.mp.max)
print(a.mp.min) --nil

print(table.maxn(a)) --返回 table 最大數值 索引 此處為0
print(#a) --返回 長度 0 因為 table 不是線性的

a = nil
```


## 注意
* lua 索引從 1 開始計數
* **#** 返回一個 線性數組的長度 或 線性表最後一個索引值
