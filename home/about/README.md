# 簡介
* 大概14年 計算機專業畢業
* 14年畢業後在家研究win32內核hook
* 15~16年和同學一起創業 開發遊戲外掛 網站 windows程序
* 17~現在 在成都某公司任職 開發c++/go服務器

# 網站

| 網址 | 內容 |
| -------- | -------- |
| [https://blog.king011.com/](https://blog.king011.com/) | 我的一個部落格 |
| [https://book.king011.com](https://book.king011.com)   | 一個類似gitbook的個人資料整理網站 記錄了我所學的才能(就是當前你看到的這個網站)     |
| [https://doc.king011.com](https://doc.king011.com) | 我在使用book之前的一個資料整理站點 目前有比book上更多的資料|
| [https://github.com/powerpuffpenguin](https://github.com/powerpuffpenguin) | 寫了很多開源代碼 把一些精心維護的項目單獨放在此 |
| [https://gitlab.com/users/king011/projects](https://gitlab.com/users/king011/projects) | 我開源的項目 大多在此 |
| [https://github.com/zuiwuchang](https://github.com/zuiwuchang) | 之前開源項目地址 因為ms的原因基本已經棄用 現在改用gitlab |

# 專業技能
## google
google的都鍾意 只在於是否有空玩 目前在玩的有
* kubernetes
* jsonnet
* protocol buffer
* grpc
* angular
* go
* dart
* flutter
* android

## linux
* 大學開始玩linux 應該是12年開始吧
* 使用mint作為桌面和工作環境
* 自己的web服務器使用ubuntu
* 玩過 centos redhat opensuse ubuntu 對centos ubuntu熟悉點 目前主要使用ubuntu

## golang
* 2017年開玩 現在作為主要開發工具
* 開發過 應用服務器 網管服務器 socks5代理 web cli 一個2d遊戲引擎

## c++
* 大學開始玩c++ 
* 開發過 遊戲外掛 為外掛實現過一個簡易腳本語言 音樂播放器 服務器
* 常年使用 boost 經驗 熟悉asio 和asio協程
* 使用cmake gcc作為構建工具

## js ts
* 現在常玩 nodejs
* 常年用duktape作為嵌入js環境在c++中使用
* 現在基本不寫js代碼 只寫typescript後編譯成js

## 彙編
* 玩逆向時 玩過一點 intel x86 彙編

## lua
* 玩過幾年 使用dukape前一直作為c++嵌入腳本 直到無法忍受lua的反人類語法後用dukape替代lua的定位
## python
* 學過語法 然未用來寫過東西 因為我以js作為主要腳本

## java
* 厭惡至極的東西 不過語法都會 大學時用來寫過web 和 tcp服務器
* 看過android開發 不過android都允許 用c++ flutter了 再無用過
* 被oracle收購後 我已完全放棄java 極度不看好

## microsoft 和 oracle
* 厭惡 microsoft 和 oracle 的一切
* 最近幾年 ms還有可取之處 稍有改觀 
* oracle 為什麼不去死

## 其它厭惡的
* 除了 php 好像就沒了

# 怪癖
* 香港電影
* 中國古拳法
* google是一種信仰
* cerberus ia an idea



