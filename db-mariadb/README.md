# mariadb

mariadb 是在 mysql 淪陷後 其原作者 重新 開發的一個 完全 兼容 mysql 的 數據庫 可以算是 mysql 的 重生

mariadb 是一個 開源(GPL2 LGPL2) 的關係型數據庫

官網

* [https://mariadb.com/](https://mariadb.com/)   
* [https://mariadb.org/](https://mariadb.org/)

源碼 [https://github.com/MariaDB/server](https://github.com/MariaDB/server)

# 常用 數據庫 指令
```sql
-- 創建 數據庫
create database King DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

-- 創建一個 用戶 操作 數據庫
-- create user 'king'@'%' identified by '12345678';
create user 'king'@'localhost' identified by '12345678';

-- 允許指定用戶 遠程 登入
-- GRANT ALL PRIVILEGES ON *.* TO 'king'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;

-- 將數據庫 授權給 用戶
GRANT ALL PRIVILEGES ON King.* TO 'king'@'localhost';

-- 查看用戶 授權
show grants for 'king'@'localhost';

-- 刷新權限
flush privileges;
```

# 常用 指令
```sh
# 連接本地 數據庫
mysql -u king -p

# 連接 遠端 數據庫
mysql -u king -p -h domain
mysql -u king -p -h domain -P 3306
mysql -u king -p -h domain -P 3306 --default-character-set=utf8
```