# git

```
pacman -S git
```

# autocrlf
正常 系統使用 LF 作爲換行 而windows 使用 CRLF 換行
git 提供了 core.autocrlf 屬性 如果 設置爲 true 
則在 check out 出代碼時 自動 將 LF 換成 白目的 CRLF
而在 commit 時 自動將 白目的 CRLF 換成 LF

```
git config --global core.autocrlf true
```

# filemode
windows 下 檔案權限 可能是 錯誤的 配置 core.filemode false 可以讓 git 忽略 權限變化

```
git config --global core.filemode false
```