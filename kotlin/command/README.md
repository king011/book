# 命令

```bash
# 編譯 hello.kt 
# -include-runtime 包含 kotlinc 運行時態
# -d 指定輸出 檔案 可以是 路徑 目錄 或 一個 jar
 kotlinc hello.kt -include-runtime -d hello.jar
 
 # 編譯一個庫 供 其它程式使用
 kotlinc hello.kt -d hello.jar
 
 # 運行 腳本中的主類 HelloKt
 kotlin -classpath helloer.jar HelloKt
 
 # 直接以腳本運行 kotlinc
 kotlinc -script list_folders.kts -- -d <檔案夾>
 
 # 運行 java 程式
 java -jar helloer.jar
```