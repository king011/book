# [安裝](https://deno.land/manual@v1.28.1/getting_started/installation)

deno 使用一個唯一的二進制檔案並且沒有外部依賴，你下載後將其加入 PATH 環境變量即可，此外 官方爲不同平臺也提供了安裝腳本

```
#info="linux"
curl -fsSL https://deno.land/x/install/install.sh | sh
```
上述命名會將 deno 安裝到 **~/.deno/bin/deno** 記得更新你的 **~/.bashrc** 檔案
```
export DENO_INSTALL="$HOME/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"
source <(deno completions bash)
```

# 更新
```
deno upgrade
```

也可以目前指定要更新到的版本

```
deno upgrade --version 1.0.1
```

# vscode
官方爲 vscode 提供了支持插件

```
ext install denoland.vscode-deno
```

因爲 nodejs tsc 等的流行，deno插件默認不會爲 js/ts 項目啓用，你需要手動啓用或者在 vscode 中配置 deno.enable 爲 true


```
{
  "deno.enable": true,
  "deno.lint": true,
  "editor.formatOnSave": true,
  "[typescript]": { "editor.defaultFormatter": "denoland.vscode-deno" }
}
```