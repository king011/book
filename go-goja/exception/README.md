# 異常
js throw 的異常 都會作爲 \*goja.Exception 返回  並通過 Export 獲取值
```
package main

import (
	"github.com/dop251/goja"
)

func main() {
	var (
		vm *goja.Runtime
		e  error
	)

	// 創建 虛擬機
	vm = goja.New()
	_, e = vm.RunString(`throw(123)`)
	if jserr, ok := e.(*goja.Exception); ok {
		if jserr.Value().Export() != int64(123) {
			panic("wrong value")
		}
	} else {
		panic("wrong type")
	}
}
```

js 調用的 go函數 如果 go 使用 panic 引發恐慌則將 被 js try 捕獲

```
package main

import (
	"log"

	"github.com/dop251/goja"
)

var vm *goja.Runtime

func test() {
	panic(vm.ToValue("Error"))
}
func main() {
	vm = goja.New()
	vm.Set(`test`, test)
	_, e := vm.RunString(`
try{
	test()
}catch(e){
	if(e!="Error"){
		throw e
	}
}`)
	if e != nil {
		log.Fatalln(e)
	}
}
```

# 中斷

* Runtime.Interrupt 可以設置 js 中斷 在執行玩go代碼後 當需要執行js時 會使用 js執行 立刻失敗 並以 \*goja.InterruptedError 返回
* Runtime.ClearInterrupt 可以在js代碼執行前 清空 Interrupt 設置的中斷

```
package main

import (
	"fmt"
	"log"
	"time"

	"github.com/dop251/goja"
)

func nativePrintln(args ...interface{}) {
	fmt.Println(args...)
}
func main() {
	var (
		vm *goja.Runtime
		e  error
	)

	// 創建 虛擬機
	vm = goja.New()
	time.AfterFunc(200*time.Millisecond, func() {
		vm.Interrupt("halt")
	})
	vm.Set(`println`, nativePrintln)
	_, e = vm.RunString(`
for (var i = 0;;i++) {
	println(i)
}
`)
	if e != nil {
		if _, ok := e.(*goja.InterruptedError); ok {
			log.Fatalln(`InterruptedError`, e)
		} else {
			log.Fatalln(e)
		}
	}
}
```