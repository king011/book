# BasicAuth

gin 提供了 BasicAuth 中間件 來實現 basic auth

```
package main

import (
	"log"
	"net"
	"net/http"

	"github.com/gin-gonic/gin"
)

const addr = `localhost:8080`

func main() {
	l, e := net.Listen(`tcp`, addr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(`work at`, addr)
	router := gin.Default()
	// BasicAuth 中間件 提供了 basic auth 實現
	router.Use(gin.BasicAuth(gin.Accounts{
		`king`: `123`,
		`kate`: `456`,
	}))
	router.GET(`text`, text)

	router.RunListener(l)
}
func text(c *gin.Context) {
	c.String(http.StatusOK, `%s is an idea`, `cerberus`)
}
```