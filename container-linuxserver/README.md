# linuxserver

linuxserver 是一個開源的社區，旨在提供易於使用的 docker 鏡像集合，並提供清晰簡介的文檔。

linuxserver 通過使用 [s6-overlay](https://github.com/just-containers/s6-overlay) 來構建高度可擴展的標準化鏡像，且爲鏡像提供共享的基礎層以達到統一和節省寬帶與存儲空間

* 官網 [https://www.linuxserver.io/](https://www.linuxserver.io/)
* 源碼 [https://github.com/linuxserver](https://github.com/linuxserver)
* 文檔 [https://docs.linuxserver.io/](https://docs.linuxserver.io/)