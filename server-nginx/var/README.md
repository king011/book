# 設置變量

nginx 配置中 值能存放 字符串變量 變量名必須以 **$** 開始

使用 set 設置 變量

```
http {
	charset utf8;
	server {
		listen 8080;
		add_header Content-Type "text/plain;charset=utf-8";
		set $text "cerberus is an idea.";
		set $name king;
		return 200 "welcome $name, $text";
	}
}
```

# 變量作用域
1. set 有兩個含義 
    * 創建變量
    * 給變量賦值
2. 在加載配置時 創建變量
3. 賦值變量是在處理每個獨立請求時 故每個獨立請求獨享一個變量 存儲區域
4. set 可以用在 server location if 上下文中
5. 同個上下文多次 set 同個變量 使用最後一個set值 


```
http {
	charset utf8;
	server {
		listen 8080;
		location /foo {
			add_header Content-Type "text/plain;charset=utf-8";
			return 200 "foo -> foo is: $foo";
		}
		location /bar {
			set $foo 32;
			add_header Content-Type "text/plain;charset=utf-8";
			return 200 "bar -> foo is: $foo";
		}
	}
}
```

rewrite 導致的內部 location 跳轉 屬於同個 上下文 故跳轉前後使用的是同個 變量

```
http {
	charset utf8;
	server {
		listen 8080;
		location /foo {# 直接訪問 /foo $foo 變量未設置 爲空
			add_header Content-Type "text/plain;charset=utf-8";
			return 200 "foo -> foo is: $foo"; 
		}
		location /bar { # 設置了 $foo 後 可以輸出 變量
			set $foo 32;
			add_header Content-Type "text/plain;charset=utf-8";
			return 200 "bar -> foo is: $foo";
		}
		location /chan { # rewrite 導致的內部 location 跳轉 屬於同個 上下文 故 $foo 依然輸出 16
			set $foo 16;
			rewrite ^ /foo; 
		}
		location /bar2 { # 設置了 雖然被設置爲了 64 但 跳轉到 /bar 後被覆蓋爲 32 故輸出 $foo 爲 32
			set $foo 64;
			add_header Content-Type "text/plain;charset=utf-8";
			rewrite ^ /bar; 
		}
	}
}
```

# 內置變量

nginx 提供了 一些內置變量 用來保存 常用的 http 信息

## request 相關

1. $arg\_**name**

    請求中指定 名稱的 參數值，如果參數不存在則爲空字符串。比如 請求是 "GET /text?id=2" 則 $arg\_id 可取出 id參數

2. $is\_args

    如果請求中包含參數 則 值爲 **?** 否則爲空字符串
		
3. $args $query\_string

    請求中的 全部參數。比如 請求是 "GET /text?name=kk&lv=1" 則 $args 值爲 name=kk&lv=1
		
4. $cook\_**name**

    指定cookie名稱的值
		
5. $request

    完整的 原始請求行，比如 "GET /text/../a?a=b HTTP1.1"。（uri不會被規範化）
		
6. $http\_**name**

    取指定名稱(忽略大小寫)的 header
		
7. $request\_length

    請求長度 包含 請求行 請求頭 請求體
		
8. $request\_method

    請求的方法名稱 GET POST ...
		
9. $request\_uri

    完整的原始 uri(帶參數)。
		
10. $scheme

    請求模式 http/https
		
11. $content\_length

    請求頭 Content-Length 的值
		
12. $content\_type

    請求頭 Content-Type 的值
		
13. $document\_root

    應用當前請求的 root 或 alias 指令值
		
14. $uri $document\_uri

    規範化後的 uri
		
15. $host

    host 值按照如下優先級順序設置
		* 從請求行中獲取到主機名
		* 從 Host 請求頭 中獲取到主機名
		* 處理請求的 虛擬主機名

16. $proxy\_add\_x\_fowarded\_for

    在客戶端傳遞過來 的 X-Forwarded-For 加上 $remote\_addr 的值 以 **,** 分隔
		
17. $realpath\_root

    應用與當前請求的 root 或 alias 所對應的決定路徑
		
18. $server\_protocol

    請求協議 "HTTP/1.0" "HTTP/1.1" "HTTP/2.0"
		
19. $request\_filename

    當前請求對應的資源路徑
		
## response 相關

1. $body\_bytes\_sent

    下發給客戶端的 字節數(不包含響應頭)
		
2. $request\_time

   從接收到請求的第一個字節 到 把響應最後一個字節發送給客戶端 所花費時間。單位秒 精度到毫秒
	 
3. $send\_http\_**name**

    獲取下發給客戶端的 響應頭 指定值
		
4. $status

    下發給客戶端的響應碼

## 常用變量

1. $binary\_remote\_addr

    二進制客戶端地址 ipv4 是 4字節 ipv6是16字節
		
2. $connection

    連接序號
		
3. $connection\_requests

    記錄 keepalive 下 客戶端 在同個連接下 發起了多少次請求
		
4. $date\_local 

    本地時區的 當前時間
		
5. $date\_gmt

    GMT格式 當前時間
		
6. $hostname

    運行nginx主機的主機名

7. $msec

    當前時間戳
		
8. $nginx\_version

    nginx 版本號
		
9. $pid

    worker 進程pid
		
10. $proxy\_host

    在 proxy\_pass 指令中指定的被代理服務的名稱 （可能是upstream名稱）
		
11. $proxy\_port

    在 proxy-pass 指令中指定的被代理的服務端的端口。如果在 proxy_pass 指令中未指定端口 則爲協議默認端口
		
12. $remote\_addr

    客戶端地址
		
13. $remote\_port

    客戶端端口
		
14. $remote\_user

    開啓 basic authentication 時 客戶端所使用的用戶名
		
15. $server\_addr

    請求的虛擬主機地址
		
16. $server\_port

    接收請求的虛擬主機的端口
		
17. $server\_name

    接收請求的虛擬主機的名稱
		
18. $time\_iso8601

    ISO8601 格式本地時間
		
19. $time\_local

      本地時間