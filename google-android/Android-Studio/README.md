# Android Studio

Android Studio 是google 提供的 跨平臺(linux windows osx) 免費 開與(apache 2.0) android 集成開發 工具

* 官網 [https://developer.android.com/studio/index.html](https://developer.android.com/studio/index.html)
* 源碼 [https://android-review.googlesource.com/q/status:open](https://android-review.googlesource.com/q/status:open)
* api [https://developer.android.com/reference/packages](https://developer.android.com/reference/packages)
* wiki [https://zh.wikipedia.org/wiki/Android_Studio](https://zh.wikipedia.org/wiki/Android_Studio)

# ubuntu安裝

1. 安裝一些 依賴的 庫

	```
	#info=false
	sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386
	```

1. 安裝 java 環境 

	```
	#info=false
	sudo apt install openjdk-8-jdk
	```
	
1. 從 官網下載 安裝包 android-studio-XXX.zip 並解壓到 ~/Android/ 中

1. 執行 ~/Android/android-studio/bin/studio.sh 運行 Android Studio

# kvm

linux 支持 kvm  安裝可以 極大提高 android emulator 的效率

安裝 詳情 見 [https://help.ubuntu.com/community/KVM/Installation](https://help.ubuntu.com/community/KVM/Installation)

1. 執行 `egrep -c '(vmx|svm)' /proc/cpuinfo` 驗證cpu是否支持 (顯示 大於0 則支持)

	> 即使支持 依然可能需要進入 BIOS 啓動 virtualization 功能

1. 安裝kvm 

	```
	#info=false
	sudo apt-get install qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils
	```
	或者
	```
	sudo apt-get install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils
	```
	
1. 將用戶 添加到 kvm 組

	```sh
	#info=false
	sudo adduser `id -un` libvirtd
	sudo adduser `id -un` kvm
	newgrp libvirtd
	newgrp kvm
	```
	或者
	```sh
	#info=false
	sudo adduser `id -un` libvirt
	sudo adduser `id -un` kvm
	newgrp libvirt
	newgrp kvm
	```

1. 查看是否 安裝成功

	```sh
	#info=false
	virsh list --all
	```
