# grpc-gateway

grpc-gateway 是一個 第三方 開源(BSD3)的  grpc 反向代理網關 插件 可以爲golang語言 生成 grpc 服務器的 反向代理代碼

* 官網 [https://grpc-ecosystem.github.io/grpc-gateway/](https://grpc-ecosystem.github.io/grpc-gateway/)
* 源碼 [https://github.com/grpc-ecosystem/grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway)
* JSON轉碼規則 [https://cloud.google.com/endpoints/docs/grpc/transcoding?hl=zh-cn](https://cloud.google.com/endpoints/docs/grpc/transcoding?hl=zh-cn)

# 創建服務器

```bash
#info="安裝插件"
go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway@latest
```

```bash
#info="生成代碼"
protoc -I /home/king/lib/go/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis -I pb/ --go_out=plugins=grpc:.  math/Service.proto
protoc -I /home/king/lib/go/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis -I pb/ --grpc-gateway_out=logtostderr=true:.  math/Service.proto
```

```
syntax = "proto3";

package test_grpc.math;

// 需要導入 google.api.http
import "google/api/annotations.proto";

// 創建一個 Math 服務
service Math {
    rpc Base (BaseRequest) returns (BaseResponse) {
        // 創建 get 方法
        option (google.api.http) = {
            get: "/v1/math/base"
        };
    }
    // 提供一個 Add 方法
    rpc Add (AddRequest) returns (AddResponse){
        // 創建 post 方法
        option (google.api.http) = {
            post: "/v1/math/add"
            body: "*"
        };
    }
    // 提供一個 AddStream 方法
    // 可以捕獲到錯誤 但 如果 服務器提前 返回 error 也會等到 body 發送完才知道出錯
    rpc AddStream (stream AddStreamRequest) returns (AddStreamResponse){
        // 創建 post 方法
        option (google.api.http) = {
            post: "/v1/math/add_stream"
            body: "*"
        };
    }
    // 提供一個 List 方法
    // 返回值在一個 {result:ListResponse} 中 無法獲取到 grpc 返回的 error
    rpc List (ListRequest) returns (stream ListResponse){
        // 創建 post 方法
        option (google.api.http) = {
            get: "/v1/math/list"
        };
    }
    // 提供一個 Echo 方法
    // 雙向流 測試時 服務器 返回一次數據後 可能丟失 客戶端 發來的數據
    rpc Echo (stream EchoRequest) returns (stream EchoResponse){
        // 創建 post 方法
        option (google.api.http) = {
            post: "/v1/math/echo"
            body: "*"
        };
    }
}
message BaseRequest{
}
message BaseResponse{
    double Double = 1;
    float Float = 2;
    int32 Int32 = 3;
    int64 Int64 = 4; // gateway 中使用 字符串表示
    uint32 Uint32 = 5;
    uint64 Uint64 = 6;// gateway 中使用 字符串表示
    sint32 Sint32 = 7;
    sint64 Sint64 = 8;// gateway 中使用 字符串表示
    fixed32 Fixed32 = 9;
    fixed64 Fixed64 = 10;// gateway 中使用 字符串表示
    sfixed32 Sfixed32 = 11;
    sfixed64 Sfixed64 = 12;// gateway 中使用 字符串表示
    bool Bool = 13;
    string String = 14;
    bytes Bytes = 15; // base64 編碼二進制數據
}
message AddRequest{
    repeated int64 Data = 1;
}
message AddResponse{
    int64 Data = 1;
}
message AddStreamRequest{
    int64 Data = 1;
}
message AddStreamResponse{
    int64 Data = 1;
}
message ListRequest{
}
message ListResponse{
    int64 Data = 1;
}
message EchoRequest{
    string Data = 1;
}
message EchoResponse{
    string Data = 1;
}
```
## 服務器代碼
```
package main

import (
	"context"
	"log"
	"net"
	"net/http"
	grpc_math "test/grpc/math"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	// GRPCAddr grpc 工作地址
	GRPCAddr = "127.0.0.1:3016"
	// HTTPAddr http 反向代理工作地址
	HTTPAddr = "127.0.0.1:8080"
)

func main() {
	// 運行 grpc 服務
	runGRPC()
	// 運行 http 反向代理
	runHTTP(false)
	// 運行 測試 客戶端
	runClient()
}
func runHTTP(sync bool) {
	clientConn, e := grpc.Dial(GRPCAddr, grpc.WithInsecure())
	if e != nil {
		log.Fatalln(e)
	}

	// 創建 ServeMux
	mux := runtime.NewServeMux()
	// 註冊 反向代理
	e = grpc_math.RegisterMathHandler(context.Background(), mux, clientConn)
	if e != nil {
		log.Fatalln(e)
	}

	// 創建 http 服務
	l, e := net.Listen("tcp", HTTPAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("http work at", HTTPAddr)
	if sync {
		e = http.Serve(l, mux)
		if e != nil {
			log.Fatalln(e)
		}
	} else {
		go func() {
			e = http.Serve(l, mux)
			if e != nil {
				log.Fatalln(e)
			}
		}()
	}
}
func runGRPC() {
	//創建 監聽 Listener
	l, e := net.Listen("tcp", GRPCAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("grpc work at", GRPCAddr)

	//創建 rpc 服務器
	s := grpc.NewServer()

	//註冊 服務
	grpc_math.RegisterMathServer(s, serverMath{})

	//註冊 反射 到 服務 路由
	reflection.Register(s)

	go func() {
		//讓 rpc 在 Listener 上 工作
		if e := s.Serve(l); e != nil {
			log.Fatalln(e)
		}
	}()
}
```
```
#info="math.go"
package main

import (
	"context"
	"fmt"
	"io"
	grpc_math "test/grpc/math"
	"time"
)

type serverMath struct {
}

func (serverMath) Base(ctx context.Context, request *grpc_math.BaseRequest) (response *grpc_math.BaseResponse, e error) {
	response = &grpc_math.BaseResponse{
		Double:   -64.64,
		Float:    -32.32,
		Int32:    -32,
		Int64:    -64,
		Uint32:   32,
		Uint64:   64,
		Sint32:   -32,
		Sint64:   -64,
		Fixed32:  32,
		Fixed64:  64,
		Sfixed32: 32,
		Sfixed64: 64,
		Bool:     true,
		String_:  "cebreus is an idea",
		Bytes:    []byte("i'm illusive man"),
	}
	return
}

func (serverMath) Add(ctx context.Context, request *grpc_math.AddRequest) (response *grpc_math.AddResponse, e error) {
	response = &grpc_math.AddResponse{}
	for _, v := range request.Data {
		response.Data += v
	}
	return
}
func (serverMath) AddStream(stream grpc_math.Math_AddStreamServer) (e error) {
	var sum int64
	var request grpc_math.AddStreamRequest
	for {
		e = stream.RecvMsg(&request)
		if e == io.EOF { // 客戶端 CloseSend
			e = nil
			break
		}
		fmt.Println(sum, "+=", request.Data)
		sum += request.Data
	}
	// 設置響應
	e = stream.SendMsg(&grpc_math.AddStreamResponse{
		Data: sum,
	})
	return
}
func (serverMath) List(request *grpc_math.ListRequest, stream grpc_math.Math_ListServer) (e error) {
	var response grpc_math.ListResponse
	for i := 0; i < 5; i++ {
		if i != 0 {
			time.Sleep(time.Second)
		}
		fmt.Println("list send", i)
		response.Data = int64(i)
		e = stream.SendMsg(&response)
		if e != nil {
			break
		}
	}
	return
}
func (serverMath) Echo(stream grpc_math.Math_EchoServer) (e error) {
	var request *grpc_math.EchoRequest
	var response grpc_math.EchoResponse
	for {
		request, e = stream.Recv()
		if e == io.EOF { // 客戶端 CloseSend
			e = nil
			break
		}
		fmt.Println("request", request.Data)
		response.Data = request.Data
		e = stream.SendMsg(&response)
		if e != nil {
			break
		}
	}
	return
}
```
```
#info="client.go"
package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type streamReader struct {
	data []byte
	ch   chan []byte
	e    error
}

func (s *streamReader) Read(p []byte) (n int, e error) {
	if s.e != nil {
		e = s.e
		return
	}
	if len(s.data) == 0 {
		for {
			data, ok := <-s.ch
			if !ok {
				e = io.EOF
				s.e = e
				return
			}
			if len(data) != 0 {
				s.data = data
				break
			}
		}
	}
	n = copy(p, s.data)
	s.data = s.data[n:]
	if len(s.data) == 0 {
		s.data = nil
	}
	return
}
func runClient() {
	baseRoot := "http://" + HTTPAddr
	requestBase(baseRoot + "/v1/math/base")
	requestAdd(baseRoot + "/v1/math/add")
	requestAddStream(baseRoot + "/v1/math/add_stream")
	requestList(baseRoot + "/v1/math/list")
	requestEcho(baseRoot + "/v1/math/echo")
}
func requestBase(url string) {
	response, e := http.Get(url)
	if e != nil {
		log.Fatalln(e)
	}
	b, e := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(string(b))
}
func requestAdd(url string) {
	body := bytes.NewBufferString(`{"Data":["1","2","3"]}`)
	response, e := http.Post(url, "application/json", body)
	if e != nil {
		log.Fatalln(e)
	}
	b, e := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(string(b))
}

func requestAddStream(url string) {
	var body streamReader
	body.ch = make(chan []byte)
	go func() {
		for i := 0; i < 5; i++ {
			if i != 0 {
				time.Sleep(time.Second)
			}
			str := fmt.Sprintf(`{"Data":"%v"}`, i)
			fmt.Println("add stream", i)
			body.ch <- []byte(str)
		}
		close(body.ch)
	}()

	response, e := http.Post(url, "text/plain", &body)
	if e != nil {
		log.Fatalln(e)
	}
	b, e := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(string(b))
}
func requestList(url string) {
	request, e := http.NewRequest("GET", url, nil)
	if e != nil {
		log.Fatalln(e)
	}
	var client http.Client
	response, e := client.Do(request)
	if e != nil {
		log.Fatalln(e)
	}
	defer response.Body.Close()
	fmt.Println(response.Header.Get("Content-Type"))
	b := make([]byte, 1024)
	for {
		n, e := response.Body.Read(b)
		if n > 0 {
			fmt.Println("list get", string(b[:n]))
		}
		if e == io.EOF {
			break
		} else if e != nil {
			log.Fatalln(e)
		}
	}
}
func requestEcho(url string) {
	var body streamReader
	body.ch = make(chan []byte)
	go func() {
		for i := 0; i < 10; i++ {
			if i != 0 {
				time.Sleep(time.Second / 10)
			}
			str := fmt.Sprintf(`{"Data":"%v"}`, i)
			fmt.Println("add stream this is", i)
			body.ch <- []byte(str)
		}
		close(body.ch)
	}()

	request, e := http.NewRequest("POST", url, &body)
	if e != nil {
		log.Fatalln(e)
	}
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Transfer-Encoding", "chunked")
	var client http.Client
	response, e := client.Do(request)
	if e != nil {
		log.Fatalln(e)
	}
	defer response.Body.Close()
	if e != nil {
		log.Fatalln(e)
	}
	defer response.Body.Close()
	b := make([]byte, 1024)
	for {
		n, e := response.Body.Read(b)
		if n > 0 {
			fmt.Println("echo get", string(b[:n]))
		}
		if e == io.EOF {
			break
		} else if e != nil {
			log.Fatalln(e)
		}
	}
}
```

# 共用端口

grpc 使用 http2 協議 grpc-gateway 同樣支持 http2協議 故可以使 她們工作在同一端口 使用 **Content-Type** 進行區分

```
package main

import (
	"context"
	"crypto/tls"
	"flag"
	"log"
	"net"
	"net/http"
	"strings"
	grpc_math "test/grpc/math"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

// Server 定義服務器
type Server struct {
	http2Server *http2.Server
	httpServer  *http.Server
	grpcServer  *grpc.Server
	clientConn  *grpc.ClientConn
	proxyMux    *runtime.ServeMux
}

func main() {
	var h2c, help bool
	var listen string
	var certFile, keyFile string
	flag.BoolVar(&help, `help`, false, `display help`)
	flag.BoolVar(&h2c, `h2c`, false, `use h2c mode`)
	flag.StringVar(&listen, `listen`, `:3016`, `listen address`)
	flag.StringVar(&certFile, `cert`, `test.pem`, `h2 cert file`)
	flag.StringVar(&keyFile, `key`, `test.key`, `h2 key file`)
	flag.Parse()
	if help {
		flag.PrintDefaults()
		return
	}
	// 監聽端口
	l, e := net.Listen(`tcp`, listen)
	if e != nil {
		log.Fatalln(e)
	}
	defer l.Close()

	// 創建 服務器
	var server Server

	// 運行 服務器
	if h2c {
		log.Println("h2c work at", listen)
		server.Serve(l)
	} else {
		log.Println("h2 work at", listen)
		server.ServeTLS(l, "test.pem", "test.key")
	}
}

// Serve 以 h2c 運行 服務
func (s *Server) Serve(l net.Listener) error {
	e := s.init(true, l.Addr().String(), "", "")
	if e != nil {
		return e
	}
	s.httpServer.Handler = h2c.NewHandler(s, s.http2Server)
	return s.httpServer.Serve(l)
}

// ServeTLS 以 h2 運行 服務
func (s *Server) ServeTLS(l net.Listener, certFile, keyFile string) error {
	e := s.init(false, l.Addr().String(), certFile, keyFile)
	if e != nil {
		return e
	}
	s.httpServer.Handler = s
	return s.httpServer.ServeTLS(l, certFile, keyFile)
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	contextType := r.Header.Get(`Content-Type`)
	if strings.Contains(contextType, `application/grpc`) {
		s.grpcServer.ServeHTTP(w, r) // grpc 路由給 grpc服務
	} else {
		s.proxyMux.ServeHTTP(w, r) // 非 grpc 路由給 http 代理服務器
	}
}
func (s *Server) init(h2c bool, address, certFile, keyFile string) (e error) {
	var httpServer http.Server
	var http2Server http2.Server

	// 創建 反向代理
	clientConn, mux, e := s.proxyServer(h2c, address)
	if e != nil {
		return
	}

	// 配置 http2
	e = http2.ConfigureServer(&httpServer, &http2Server)
	if e != nil {
		clientConn.Close()
		return
	}

	//創建 rpc 服務器
	grpcServer := grpc.NewServer()
	//註冊 服務
	grpc_math.RegisterMathServer(grpcServer, serverMath{})
	//註冊 反射 到 服務 路由
	reflection.Register(grpcServer)

	s.httpServer = &httpServer
	s.http2Server = &http2Server
	s.grpcServer = grpcServer
	s.clientConn = clientConn
	s.proxyMux = mux
	return
}
func (s *Server) proxyServer(h2c bool, address string) (clientConn *grpc.ClientConn, mux *runtime.ServeMux, e error) {
	// 創建 客戶端
	var opts []grpc.DialOption
	if h2c {
		opts = append(opts, grpc.WithInsecure())
	} else {
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
		})))
	}
	clientConn, e = grpc.Dial(address, opts...)
	if e != nil {
		return
	}

	// 創建反向代理
	mux = runtime.NewServeMux()
	// 註冊 反向代理
	e = grpc_math.RegisterMathHandler(context.Background(), mux, clientConn)
	if e != nil {
		clientConn.Close()
		return
	}
	return
}
```

# swagger 

grpc-gateway 支持生成 swagger 檔案

```bash

#info="安裝插件"
go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-openapiv2@latest
```

```bash
#info="生成代碼"
protoc -I /home/king/lib/go/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis -I pb/ --openapiv2=logtostderr=true:.  math/Service.proto
```