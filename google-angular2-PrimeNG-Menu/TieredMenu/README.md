# [TieredMenu](https://primeng.org/tieredmenu)

```
import {TieredMenuModule} from 'primeng/tieredmenu';
import {MenuItem} from 'primeng/api';
```

TieredMenu 在嵌套的 overlays 中顯示子菜單

![](assets/primeng.org_menubar.png)

```
<p-tieredMenu [model]="items"></p-tieredMenu>
```

```
export class TieredMenuDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'Project'},
                            {label: 'Other'},
                        ]
                    },
                    {label: 'Open'},
                    {label: 'Quit'}
                ]
            },
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-trash'},
                    {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
                ]
            }
        ];
    }
}
```

## Popup Mode

默認情況下，菜單是內聯的，設置 **popup** 屬性爲 true 來使用彈出菜單

```
<p-tieredMenu #menu [model]="items" [popup]="true"></p-tieredMenu>
<button #btn type="button" pButton icon="pi pi-fw pi-list" label="Show" (click)="menu.toggle($event)"></button>
```

## Animation Configuration

**showTransitionOptions/hideTransitionOptions** 屬性來設置菜單的過渡動畫，下面例子將完全禁用動畫

```
<p-tieredMenu [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'" #menu [model]="items" [popup]="true"></p-tieredMenu>
<button #btn type="button" pButton icon="pi pi-fw pi-list" label="Show" (click)="menu.toggle($event)"></button>
```
# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onShow     | event: Event object     | 顯示時回調     |
| onHide     | event: Event object     | 隱藏時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| popup     | boolean     | false     | 是否作爲彈出菜單     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| appendTo     | any     | null     | Target element to attach the overlay, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| baseZIndex     | number     | 0     | 分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| showTransitionOptions     | string     | 0.12s cubic-bezier(0, 0, 0.2, 1)	     | 顯示過渡動畫     |
| hideTransitionOptions     | string     | 0.1s linear	     | 隱藏過渡動畫     |

# Methods

| Name | Parameters | Description |
| -------- | -------- | -------- |
| toggle     | event: browser event     | 切換菜單顯示狀態     |
| show     | event: browser event     | 顯示菜單     |
| hide     | -     | 隱藏菜單     |

# Styling

| Name | Element |
| -------- | -------- |
| p-tieredmenu     | Container element.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
| p-submenu-icon     | Arrow icon of a submenu.     |




