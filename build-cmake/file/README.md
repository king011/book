# file
cmake 提供了 file 指令 用來對 檔案進行處理

# 寫檔案
```cmake
#info=false
# 新建一個 檔案並寫入 數據
file(WRITE filename "message to write"... )

# 將數據 添加到 檔案尾 如果檔案不存在則創建
file(APPEND filename "message to write"... )
```

```cmake
set(filename a.txt)
file(WRITE  ${filename} "ok yes\n")
file(APPEND ${filename} line2 " : this is number 2\n")
file(APPEND ${filename} line3 " : this is number 3\n")
```

# 讀取檔案
```cmake
#info=false
# 讀取檔案 內容到變量 variable 中
file(READ filename variable [LIMIT numBytes] [OFFSET offset] [HEX])
```
* LIMIT 如果被指定 則最多讀取 numBytes 字節數據
* OFFSET 如果被指定 則從 offset 字節 偏移 開始讀取
* HEX 如果被指定 則將數據 轉爲16進制

```cmake
set(filename a.txt)
file(WRITE  ${filename} "ok yes\n")
file(APPEND ${filename} line2 " : this is number 2\n")
file(APPEND ${filename} line3 " : this is number 3\n")

# 讀取 檔案 全部 內容
file(READ  ${filename} str)
message(${str})

# 讀取 指定 範圍 內容
file(READ  ${filename} str 
    OFFSET 15
    LIMIT 16
)
message(${str})

# 讀取 數據 並轉爲 16 進制
file(READ  ${filename} str 
    OFFSET 15
    LIMIT 16
    HEX
)
message(${str})
```

# 查找檔案
```cmake
# 查找 檔案夾下的 檔案/檔案夾 並以列表形式 存儲到 變量variable中
file(GLOB variable [RELATIVE path] [globbing expressions]...)

# 類似 GLOB  不過會遞歸 查找 子檔案夾
file(GLOB_RECURSE variable [RELATIVE path]
     [FOLLOW_SYMLINKS] [globbing expressions]...)
```
* RELATIVE 如果被指定 則 會 以 RELATIVE指定位置的 相對位置 存儲 找到的 項目 否則 以全路徑 表示

> 不要傳入 重疊的 查找位置 否則 重疊數據會被多次 添加到列表
>

```cmake
file(GLOB files 
    RELATIVE "${CMAKE_SOURCE_DIR}/build"
    cmake/*.cmake
    cmake/modules/*.cmake
)
foreach(file ${files})
    message(${file})
endforeach()
```

```txt
#info={"name":"globbing expressions","noline":true}
*.cxx      - match all files with extension cxx
*.vt?      - match all files with extension vta,...,vtz
f[3-5].txt - match files f3.txt, f4.txt, f5.txt
```

# 檔案 hash
```cmake
# 計算 檔案 hash 值 並存儲到 變量
file(<MD5|SHA1|SHA224|SHA256|SHA384|SHA512> filename variable)
```

```cmake
set(filename a.txt)
file(WRITE  ${filename} "ok yes\n")
file(APPEND ${filename} line2 " : this is number 2\n")
file(APPEND ${filename} line3 " : this is number 3\n")

# 計算 md5
set(filename "${CMAKE_SOURCE_DIR}/${filename}")
file(MD5 "${filename}" str)
message("md5 : ${str}")
# 計算 sha
file(SHA512 "${filename}" str)
message("sha : ${str}")
```
> 計算 hash 傳入的 檔案名稱 需要是全 路徑
> 

# 移動 檔案/檔案夾
```cmake
# 以 原子的 形式 移動一個 檔案/檔案夾
file(RENAME <oldname> <newname>)
```

```cmake
file(RENAME a.txt b.txt)
```

# 刪除 檔案/檔案夾
```cmake
# 刪除 檔案/檔案夾
file(REMOVE [file1 ...])

# 類似 REMOVE 不過 如果 檔案夾非空 會 遞歸 刪掉 此檔案夾 而 REMOVE 只在檔案夾爲空時才會 刪除 檔案夾
file(REMOVE_RECURSE [file1 ...])
```
```cmake
file(REMOVE
    cmake/modules/a.cmake
    cmake/modules/b.cmake 
)

file(REMOVE_RECURSE
    cmake/modules
)
```

# 創建 檔案夾
```cmake
# 如果 檔案夾 不存在 創建
# 類似 mkdir -p
file(MAKE_DIRECTORY [directory1 directory2 ...])
```
```cmake
file(MAKE_DIRECTORY
    include/animal
    src/animal
)
```

# 轉 相對 路徑
```cmake
# 將 file 轉爲 相對 directory 的 相對路徑 並存儲到 變量中
file(RELATIVE_PATH variable directory file)
```
> directory 和 file 都 必須是 全路徑
> 

```cmake
file(RELATIVE_PATH str 
    "${CMAKE_SOURCE_DIR}/build"
    "${CMAKE_SOURCE_DIR}/cmake/source.cmake"
)
message(STATUS ${str})
```

# 路徑 轉換
```cmake
# 將路徑 轉爲 cmake路徑 unix 風格
# 並且 如果 路徑 是有 多個 組合的 比如 PATH 還會 自動 將其 分割爲 多個 路徑 以 列表 存儲
file(TO_CMAKE_PATH path result)

# 類似 TO_CMAKE_PATH  不過會 轉爲 相應 平臺的 路徑表示方式
file(TO_NATIVE_PATH path result)
```

# 下載上傳
