# Android TV

google play 對發佈到 android tv 的應用有一定的[要求](https://developer.android.com/docs/quality-guidelines/tv-app-quality)，你需要在 flutter 中對其進行適配

## banner

因爲 TV 不顯示圖標而是顯示橫幅，所以創建一個尺寸爲 320x180 的 **banner.png**(橫幅應該填充滿尺寸，不要使用透明背景，且包含應用名稱的完整文本，如果支持多語言需要爲不同語言提供對應名稱) 放置到 **android/app/src/main/res/drawable** 檔案夾下

然後在 **AndroidManifest.xml** 中指定 **android:banner**

```
#info="AndroidManifest.xml"
<application
    android:name="io.flutter.app.FlutterApplication"
    android:label="flutter_android_tv"
    android:banner="@drawable/banner"
    android:icon="@mipmap/ic_launcher">
   ...
</application>
```

## LEANBACK\_LAUNCHER

我們必須爲 tv 聲明一個啓動器，LEANBACK\_LAUNCHER 過濾器用於此。此過濾器將應用識別爲支持 Android TV 並讓 Goole Play 商店將其識別爲 TV 應用。當用戶在 TV 屏幕上選擇你的應用時，這有助與確定觸發哪個 activity 以匹配用戶的意圖

```
#info="AndroidManifest.xml"
 <intent-filter>
        <action android:name="android.intent.action.MAIN"/>
        <category  android:name="android.intent.category.LEANBACK_LAUNCHER"/>
         <category android:name="android.intent.category.LAUNCHER"/>
 </intent-filter>
```

## android.software.leanback

提及應用使用 Android TV 所需要的 Leanback UI。如果應用在移動設備(手機 平板) 和 TV 上運行，應該設置 **android:required="false"**，如果只在 Leanback UI 的設備上運行則設置 **android:required="true"**

```
#info="AndroidManifest.xml"
 <manifest>
   <uses-feature android:name="android.software.leanback"
       android:required="false" />
   ...
 </manifest>
```

## android.hardware.touchscreen

TV 應用不能依賴觸屏輸入，所以必須在清單裏面聲明 android.hardware.touchscreen 爲 false

```
#info="AndroidManifest.xml"
 <manifest>
   <uses-feature android:name="android.hardware.touchscreen"
             android:required="false" />
   ...
 </manifest>
```