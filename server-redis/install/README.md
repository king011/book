# ubuntu

```
sudo apt-get update && sudo apt-get install redis-server
```

# redis-cli

redis-cli 是 redis 帶的一個客戶端 執行後 可以在裏面執行 redis 各種操作

```
$ redis-cli 
127.0.0.1:6379> ping
PONG
```

# docker

以默認配置工作在 127.0.0.1:6379 上：
```
docker run \
	--name redis-dev \
	-d redis:7
```

你也可以將配置檔案映射給 docker:

```
docker run \
	--name redis-dev \
	-v your_conf:/etc/redis/redis.conf:ro \
	-d redis:7 \
	redis-server /etc/redis/redis.conf
```

