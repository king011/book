# jstest-gtk

jstest-gtk 可以用於測試手柄按鍵是否被系統捕獲，通常爲了測試手柄是否正常工作了可以使用此程式

```
sudo apt install jstest-gtk
```

# qjoypad

qjoypad 是一個手柄映射程式，她可以捕獲手柄按鍵並將其映射到鍵盤按鍵，這可以爲不支持手柄的遊戲提供手柄支持，此外以 wine 運行的程序可能會出現對手柄無法支持此時也可以使用 qjoypad 來解決

```
sudo apt install qjoypad
```

qjoypad 設置也很簡單，執行 qjoypad 啓動後選擇號要設置的手柄之後點擊 左下腳的 **Quick Set** 即可設置，設置方法是先按下手柄按鈕然後在提示下按下要映射的鍵盤按鈕即可(最後記得保存一份設置以免重啓 qjoypad 後又要全部重新設置一次)
