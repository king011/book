# [unicode/utf8](https://pkg.go.dev/unicode/utf8)

```
import "unicode/utf8"
```

utf8 套件實現了支援 UTF-8 編碼文字的函數和常數

## [func AppendRune](https://pkg.go.dev/unicode/utf8#AppendRune)

```
func AppendRune(p []byte, r rune) []byte
```

AppendRune 將 r 的 UTF-8 編碼附加到 p 的末端並傳回擴展緩衝區。如果 r 超出範圍，則會返回 RuneError 的編碼

## [func DecodeLastRune](https://pkg.go.dev/unicode/utf8#DecodeLastRune)

```
func DecodeLastRune(p []byte) (r rune, size int)
```

DecodeLastRune 解壓縮 p 中的最後一個 UTF-8 編碼並傳回符文及其寬度（以位元組為單位）。如果 p 為空，則傳回 (RuneError, 0)。否則，如果編碼無效，則傳回(RuneError, 1)。對於正確的、非空的 UTF-8 來說，這兩種結果都是不可能的。

如果編碼是錯誤的 UTF-8、編碼超出範圍的符文或不是該值的最短可能的 UTF-8 編碼，則編碼無效。不執行其他驗證。

## [func DecodeLastRuneInString](https://pkg.go.dev/unicode/utf8#DecodeLastRuneInString)

```
func DecodeLastRuneInString(s string) (r rune, size int)
```

類似 DecodeLastRune 但輸入是 string 

## [func DecodeRune](https://pkg.go.dev/unicode/utf8#DecodeRune)

```
func DecodeRune(p []byte) (r rune, size int)
```

DecodeRune 解壓縮 p 中的第一個 UTF-8 編碼並傳回符文及其寬度（以位元組為單位）。如果 p 為空，則傳回 (RuneError, 0)。否則，如果編碼無效，則傳回(RuneError, 1)。對於正確的、非空的 UTF-8 來說，這兩種結果都是不可能的。

如果編碼是錯誤的 UTF-8、編碼超出範圍的符文或不是該值的最短可能的 UTF-8 編碼，則編碼無效。不執行其他驗證。

## [func DecodeRuneInString](https://pkg.go.dev/unicode/utf8#DecodeRuneInString)

```
func DecodeRuneInString(s string) (r rune, size int)
```

類似 DecodeRune 但輸入是 string

## [func EncodeRune](https://pkg.go.dev/unicode/utf8#EncodeRune)

```
func EncodeRune(p []byte, r rune) int
```

EncodeRune 將符文的 UTF-8 編碼寫入 p（必須夠大）。如果符文超出範圍，則寫入 RuneError 的編碼。它傳回寫入的位元組數。

## [func FullRune](https://pkg.go.dev/unicode/utf8#FullRune)

```
func FullRune(p []byte) bool
```

FullRune 報告 p 中的位元組是否以符文的完整 UTF-8 編碼開頭。無效的編碼被視為完整的符文，因為它將轉換為寬度為 1 的錯誤符文

## [func FullRuneInString](https://pkg.go.dev/unicode/utf8#FullRuneInString)

```
func FullRuneInString(s string) bool
```

類似 FullRune 但輸入是 string

## [func RuneCount](https://pkg.go.dev/unicode/utf8#RuneCount)

```
func RuneCount(p []byte) int
```

RuneCount 傳回 p 中的符文數量。錯誤和短的編碼被視為寬度為 1 位元組的單一符文

## [func RuneCountInString](https://pkg.go.dev/unicode/utf8#RuneCountInString)

```
func RuneCountInString(s string) (n int)
```

類似 RuneCount 但輸入是 string

## [func RuneLen](https://pkg.go.dev/unicode/utf8#RuneLen)

```
func RuneLen(r rune) int
```

RuneLen 傳回符文編碼所需的位元組數。如果符文不是以 UTF-8 編碼的有效值，則傳回 -1

## [func RuneStart](https://pkg.go.dev/unicode/utf8#RuneStart)

```
func RuneStart(b byte) bool
```

RuneStart 會報告該位元組是否可能是編碼的、可能無效的符文的第一個位元組。第二個和後續位元組的最高兩位始終設定為 10

## [func Valid](https://pkg.go.dev/unicode/utf8#Valid)

```
func Valid(p []byte) bool
```

Valid 報告 p 是否完全由有效的 UTF-8 編碼符文組成

## [func ValidRune](https://pkg.go.dev/unicode/utf8#ValidRune)

```
func ValidRune(r rune) bool
```

ValidRune 報告 r 是否可以合法編碼為 UTF-8。超出範圍或替代一半的代碼點是非法的


## [func ValidString](https://pkg.go.dev/unicode/utf8#ValidString)

```
func ValidString(s string) bool
```

ValidString 報告 s 是否完全由有效的 UTF-8 編碼符文組成