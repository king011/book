# 指令

angular中 指令 通常有三種

1. 組件 -- 擁有模板
2. 屬性指令 -- 改變元素 組件 或其它指令 外觀 行爲
3. 結構指令 -- 添加 移除 DOM 元素 改變 DOM 佈局

提供使用 `ng generate directive 指令名稱` 創建一個 指令

```ts
import { Directive } from '@angular/core';
 
@Directive({
  selector: '[appShow]'
})
export class ShowDirective {
 
  constructor() { }
 
}
```

# 修改宿主
* 使用 ElementRef 可以將 宿主 的 DOM 引用 注入 以便修改 DOM
* 使用 Renderer2 可以操作 DOM 的 css 樣式 和 DOM attribute
* 使用 Input 可以 定義 輸入 屬性

```ts
import { Directive, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[appShow]'
})
export class ShowDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) { }
  @Input()
  set appShow(ok: boolean) {
    if (ok) {
      this.renderer.removeClass(this.el.nativeElement, 'hide');
    } else {
      this.renderer.addClass(this.el.nativeElement, 'hide');
    }
  }

}
```
```html
<button (click)="show=!show">do</button>
<div [appShow]="show">
  home view
</div>
```

> Input 屬性名 也可以 和 指令名 不同 不過 這樣 就不能 在 指定指令的 同時 指定屬性 需要 分開書寫
> 
> ```ts
> import { Directive, ElementRef, Renderer2, Input } from '@angular/core';
> 
> @Directive({
>   selector: '[appShow]'
> })
> export class ShowDirective {
> 
>   constructor(private el: ElementRef, private renderer: Renderer2) { }
>   @Input()
>   set show(ok: boolean) {
>     if (ok) {
>       this.renderer.removeClass(this.el.nativeElement, 'hide');
>     } else {
>       this.renderer.addClass(this.el.nativeElement, 'hide');
>     }
>   }
> 
> }
> ```
> 
> ```html
> <button (click)="show=!show">do</button>
> <div appShow [show]="show">
>   home view
> </div>
> ```
> 

# HostListener
使用 HostListener 可以 監控 宿主的事件
```ts
import { Directive, Input, HostListener, ElementRef } from '@angular/core';
 
@Directive({
  selector: '[appHight]'
})
export class HightDirective {
  // 只需要 獲取初始值 不需要 監控 改變 就 不要用 set
  @Input('appHight')
  highlightColor: string
 
  constructor(private el: ElementRef) {
  }
 
  // 監控 宿主 事件
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.highlightColor || 'red');
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
 
  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
```
```html
<p [appHight]="'yellow'">
  home view
</p>
```

# 綁定 多個屬性
同個 指令 允許 綁定 多個 屬性

```ts
import { Directive, ElementRef, Renderer2, Input, HostListener } from '@angular/core';
 
@Directive({
  selector: '[appShowHidht]'
})
export class ShowHidhtDirective {
 
  // 注入 依賴
  constructor(private el: ElementRef, private renderer: Renderer2) { }
 
  // 定義一個 和 屬性 同名的 輸入 屬性
  @Input('appShowHidht')
  set show(ok: boolean) { //必須使用 set 才能 監聽 值改變
    console.log(ok)
    if (ok) {
      this.renderer.removeClass(this.el.nativeElement, 'Hide');
    } else {
      this.renderer.addClass(this.el.nativeElement, 'Hide');
    }
  }
 
  // 定義另外一個 屬性
  // 只需要 獲取初始值 不需要 監控 改變 就 不要用 set
  @Input()
  highlightColor: string
 
  // 監控 宿主 事件
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.highlightColor || 'red');
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
 
  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
```
```html
<button (click)="show=!show">do</button>
<p [appShowHidht]="show" [highlightColor]="'yellow'">
  home view
</p>
```
```html
<button (click)="show=!show">do</button>
<p [appShowHidht]="show" highlightColor="yellow">
  home view
</p>
```