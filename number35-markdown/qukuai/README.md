# 段落和換行
一個段落是由 一個以上 相連接的行句 組成

而一個以上的空行**(只有tab和空白的也視爲空行)**會 切分出不同的 段落

markdown 允許 同個段落內 出現換行 輸入連個連續的空白符之後回車 即可 這會自動被轉爲&lt;br&gt; 

# 標題
Markdown 支持兩種 標題語法 setext 和 atx

## settext
setext形式使用 =(最高標題) -(第二階標題)

```text
This is an H1
=============

This is an H2
-------------
```

任意數量的 = 和 - 都可以

## act
atc使用 [1,6]個# 和 空格 定義 標題

```text
# This is an H1

## This is an H2

###### This is an H6
```

# 區塊引言
markdown 允許使用 > 來書寫 引言
```text
> # 引言中可以使用 其它 markdown 標籤等
> 123  
> 456
>
>
> # 可以只在段落開始加上 > 而不一定是每行
> 789  
abc
>
> def  
fed  
cba
```

# 清單
## 無序清單使用 * + 或 -
* Red
* Green
* Blue
```text
*   Red
*   Green
*   Blue

+   Red
+   Green
+   Blue

-   Red
-   Green
-   Blue
```

## 有序清單 使用 數字.
1. Kate
1. Anita
1. Jolin
```text
1. Kate
1. Anita
1. Jolin

1. Kate
2. Anita
3. Jolin
```
數字 不會影響 在html中顯示的 編號值 markdown 始終從1開始 順序編號

## p
如果清單 項 使用 空白分割 則產出的 清單項會被包含在&lt;p&gt;標籤中

```text
1. Anita

1. Kate
```
```html
<ul>
<li><p>Anita</p></li>
<li><p>Kate</p></li>
</ul>
```

## 
多段落清單項可以包含 多行段落 段落的第一行 必須有4個空白或一個tab

```text
1. 段落 1

    段落2  
後面的行可選擇是否 添加 空白和tab

   段落3
1. Kate
1. Anita
```

程式碼區塊

如果要輸入程式碼 每行開始加入 一個 tab或 4個空白即可

另外使用 ``` 將代碼包起來也行

`````text
```
#include <iostream>

void main()
{
  std::cout<<"hellow word"<<std::endl;
}
```
`````

對於行內的 代碼 可以使用 ` 包裹
```
這是一個打印APi `fmt.Println("hellow word")`
```
# 分割線
如果一行的 * + - _ 數量 > 2 且不包含空白 他不之外的 文本 則會創建一條分割線
```text
* * *

***

*****

- - -

---------------------------------------
```