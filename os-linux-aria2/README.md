# aria2

aria2 是一個 開源(GPL2) 跨平臺的 命令行 下載工具

* 官網 [https://aria2.github.io/](https://aria2.github.io/)
* 源碼 [https://github.com/aria2/aria2](https://github.com/aria2/aria2)
* 文檔 [https://aria2.github.io/manual/en/html/aria2c.html#](https://aria2.github.io/manual/en/html/aria2c.html#)

# install
```
sudo apt install aria2 -y
```