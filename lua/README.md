# lua 基礎

lua 是一個開源(MIT) 簡潔 輕量 可伸縮的腳本語言。其有着相對簡單的 C API 可以很容易的嵌入到應用中。

* 官網 [https://www.lua.org/](https://www.lua.org/)
* api [https://www.lua.org/manual/5.1/](https://www.lua.org/manual/5.1/)

# 注意

目前官方最新的版本並非 5.1，並且5.1存在各種問題，但 5.1 是目前相對而言應用得最廣泛且最容易被嵌入的版本故本系列文章以 5.1 版爲目標進行說明