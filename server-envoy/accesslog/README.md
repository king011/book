# [config.accesslog.v3.AccessLog](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#envoy-v3-api-msg-config-accesslog-v3-accesslog)

這個結構用於定義日誌細節

```typescript
/**
 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#config-accesslog-v3-accesslog}
 */
interface AccessLog {
    /**
     * 訪問日誌擴展配置的名稱
     */
    name?: string
    /**
     * 用於判斷是否需要寫入訪問日誌的過濾器
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#envoy-v3-api-msg-config-accesslog-v3-accesslogfilter}
     */
    filter?: AccessLogFilter
    /**
     * 必鬚根據正在實例化的訪問記錄器擴展設置的自定義配置
     */
    typed_config: any
}
```

* filter: [config.accesslog.v3.AccessLogFilter](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#envoy-v3-api-msg-config-accesslog-v3-accesslogfilter)

