# 水獸
![](assets/水獸.jpg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>颈(海绵体)<br>腹部</center></div>
</td><td><div><center>头<br>背部<br>腹部</center></div>
</td><td><div><center>头<br>颈(海绵体)</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 42 </td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">50 </font></td><td> 20 </td><td> 0 </td><td> 15 </td><td> 10 </td><td> 5 </td><td> 100 </td><td> 160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> <font color="#ff0000">45 </font></td><td> 42 </td><td> <font color="#ff0000">45 </font></td><td> 20 </td><td> 0 </td><td> 15 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 30 </td><td> <font color="#ff0000">45 </font></td><td> 30 </td><td> 30 </td><td> 0 </td><td> 10 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">45 </font></td><td> 40 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 30 </td><td> 35 </td><td> 25 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 35 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 42 </td><td> 42 </td><td> 40 </td><td> 20 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 180
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 200 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 120 (660) </td><td> 120 (780) </td><td> 120 (660) </td><td> 100 (600) </td><td> 150 (780)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 60秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E6%B0%B4%E8%A2%8B" title="水袋">水袋</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a> <font color="#008000"><b>15%</b></font><br><a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%9A%AE" title="海绵质的皮">海绵质的皮</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a> <font color="#008000"><b>30%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E7%88%AA" title="水兽的爪">水兽的爪</a> <font color="#008000"><b>20%</b></font> </td><td> <a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="水兽的上鳞">水兽的上鳞</a> <font color="#008000"><b>30%</b></font><br><a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E4%B8%8A%E7%9A%AE" title="海绵质的上皮">海绵质的上皮</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%94%90%E7%88%AA" title="水兽的锐爪">水兽的锐爪</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E6%B0%B4%E8%A2%8B" title="大水袋">大水袋</a> <font color="#008000"><b>10%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E5%B0%BE%E5%B7%B4" title="水兽的尾巴">水兽的尾巴</a> <font color="#008000"><b>75%</b></font> </td><td> <a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E5%B0%BE%E5%B7%B4" title="水兽的尾巴">水兽的尾巴</a> <font color="#008000"><b>70%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="水兽的上鳞">水兽的上鳞</a> <font color="#008000"><b>28%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>2%</b></font> </td><td> 断尾需要积累360的伤害值
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a> <font color="#008000"><b>20%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a> <font color="#008000"><b>20%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="水兽的上鳞">水兽的上鳞</a> <font color="#008000"><b>5%</b></font> </td><td> 关键部位破坏时掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E5%A4%B4%E5%86%A0" title="水兽的头冠">水兽的头冠</a>*1 <font color="#008000"><b>80%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a>*1 <font color="#008000"><b>20%</b></font> </td><td> <a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E5%A4%B4%E5%86%A0" title="水兽的头冠">水兽的头冠</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E9%AB%98%E7%BA%A7%E7%9A%84%E5%A4%B4%E5%86%A0" title="高级的头冠">高级的头冠</a>*1 <font color="#008000"><b>45%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="水兽的上鳞">水兽的上鳞</a>*1 <font color="#008000"><b>20%</b></font> </td><td> 将其打硬直一次头部就破坏了
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> <a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%9A%AE" title="海绵质的皮">海绵质的皮</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a>*2 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E4%B8%8A%E7%9A%AE" title="海绵质的上皮">海绵质的上皮</a>*1 <font color="#008000"><b>62%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a>*2 <font color="#008000"><b>12%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="水兽的上鳞">水兽的上鳞</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%9A%AE" title="海绵质的皮">海绵质的皮</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a>*3 <font color="#008000"><b>8%</b></font> </td><td> 将其打硬直两次颈部就破坏了
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E6%B0%B4%E8%A2%8B" title="水袋">水袋</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E7%88%AA" title="水兽的爪">水兽的爪</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%B3%9E" title="水兽的鳞">水兽的鳞</a>*2 <font color="#008000"><b>13%</b></font><br><a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%9A%AE" title="海绵质的皮">海绵质的皮</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E5%A4%A7%E6%B0%B4%E8%A2%8B" title="大水袋">大水袋</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E9%AB%98%E7%BA%A7%E7%9A%84%E5%A4%B4%E5%86%A0" title="高级的头冠">高级的头冠</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E4%B8%8A%E7%9A%AE" title="海绵质的上皮">海绵质的上皮</a>*1 <font color="#008000"><b>17%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E9%94%90%E7%88%AA" title="水兽的锐爪">水兽的锐爪</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E7%88%AA" title="水兽的爪">水兽的爪</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>水獣たちの争い
</p>
</td><td>
<p>讨伐两头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>仁義なき抗争
</p>
</td><td>
<p>讨伐一头<a href="/%E6%AF%92%E7%8B%97%E9%BE%99" title="毒狗龙">毒狗龙</a>和一头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>山の嵐、川の氾濫
</p>
</td><td>
<p>讨伐一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>和一头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D2%E6%98%9F" title="集会所下位2星">集会所下位2星</a>
</p>
</td><td>
<p>強襲する孤島の水流！
</p>
</td><td>
<p>讨伐一头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D3%E6%98%9F" title="集会所下位3星">集会所下位3星</a>
</p>
</td><td>
<p>ロアルドロスを狩猟せよ！
</p>
</td><td>
<p>讨伐一头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>疾走する猛き大猪！
</p>
</td><td>
<p>讨伐一头<a href="/%E9%87%8E%E7%8C%AA%E7%8E%8B" title="野猪王">野猪王</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>牙むく群れの長を狩れ！
</p>
</td><td>
<p>讨伐一头<a href="/%E7%8B%97%E9%BE%99%E7%8E%8B" title="狗龙王">狗龙王</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>強襲する孤島の水流！
</p>
</td><td>
<p>讨伐一头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>ルドロスを討伐せよ！
</p>
</td><td>
<p>讨伐一头15头<a href="/%E6%B0%B4%E7%94%9F%E5%85%BD" title="水生兽">水生兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>集え！孤島の大連続戦
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%B8%A9%E6%B3%89%E4%BB%BB%E5%8A%A1" title="温泉任务">温泉任务</a>
</p>
</td><td>
<p>全ては湯のため、人のため
</p>
</td><td>
<p>讨伐一头水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%A5%AE%E6%96%99%E4%BB%BB%E5%8A%A1" title="饮料任务">饮料任务</a>
</p>
</td><td>
<p>恐るべき雌達
</p>
</td><td>
<p>讨伐10头<a href="/%E6%B0%B4%E7%94%9F%E5%85%BD" title="水生兽">水生兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>水没林へ向かう我が主のために
</p>
</td><td>
<p>讨伐一头水兽和一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 爬动向猎人突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 跃身使用飞扑攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前进飞扑 </td><td> 先不断修正方位朝猎人快速前进，到一定距离时接飞扑
</td></tr>
<tr style="background:#f9f9f9;">
<td> 高速突进 </td><td> 先后退一小段距离后呈游动状想猎人进行高速突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续啃咬 </td><td> 向前方连续啃咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下压 </td><td> 努力直起上身后使用下压攻击，压倒地面后还会溅起水花，使人陷入潮湿异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 横转 </td><td> 横着身体使用滚动攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 水弹 </td><td> 吐出水弹：向前吐1颗；分别往左中右方向吐3颗。打中会陷入潮湿异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 挥爪 </td><td> 挥动前爪攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬&amp;甩尾 </td><td> 转身使用啃咬的同时尾部也存在甩尾判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续甩尾 </td><td> 尾巴左右连续甩两下进行攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 乱爬 </td><td> 边突进边左右依次不断吐水弹，可以连续突进3次
</td></tr></tbody></table>

# 紫水獸
![](assets/紫水獸.jpg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>腹部</center></div>
</td><td><div><center>头<br>背部</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 36 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 15 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 5 </td><td> 100 </td><td> 160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈(海绵体) </td><td> 44 </td><td> 40 </td><td> 35 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 30 </td><td> <font color="#ff0000">45 </font></td><td> 25 </td><td> 30 </td><td> 0 </td><td> 15 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">45 </font></td><td> 36 </td><td> 35 </td><td> 20 </td><td> 0 </td><td> 15 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 25 </td><td> 30 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 30 </td><td> 25 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 40 </td><td> 40 </td><td> 40 </td><td> 15 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 180
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 250 </td><td> 200 </td><td> 200 </td><td> 200 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 150 (850) </td><td> 140 (900) </td><td> 140 (760) </td><td> 120 (680) </td><td> 150 (780)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 40秒 </td><td> 10秒 </td><td> 60秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 100 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★5（稍微容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> 下位无此怪物 </td><td> <a href="/%E5%A4%A7%E6%B0%B4%E8%A2%8B" title="大水袋">大水袋</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a> <font color="#008000"><b>15%</b></font><br><a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%B4%AB%E7%9A%AE" title="海绵质的紫皮">海绵质的紫皮</a> <font color="#008000"><b>25%</b></font><br><a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="紫水兽的上鳞">紫水兽的上鳞</a> <font color="#008000"><b>30%</b></font><br><a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD%E7%9A%84%E9%94%90%E7%88%AA" title="紫水兽的锐爪">紫水兽的锐爪</a> <font color="#008000"><b>20%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> 下位无此怪物 </td><td> <a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="紫水兽的上鳞">紫水兽的上鳞</a> <font color="#008000"><b>42%</b></font><br><a href="/%E6%B0%B4%E5%85%BD%E7%9A%84%E5%B0%BE%E5%B7%B4" title="水兽的尾巴">水兽的尾巴</a> <font color="#008000"><b>55%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> 斩属性断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> 下位无此怪物 </td><td> <a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="紫水兽的上鳞">紫水兽的上鳞</a> <font color="#008000"><b>10%</b></font> </td><td> 头部破坏时可能掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 下位无此怪物 </td><td> <a href="/%E9%AB%98%E7%BA%A7%E7%9A%84%E5%A4%B4%E5%86%A0" title="高级的头冠">高级的头冠</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="紫水兽的上鳞">紫水兽的上鳞</a>*1 <font color="#008000"><b>60%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 下位无此怪物 </td><td> <a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%B4%AB%E7%9A%AE" title="海绵质的紫皮">海绵质的紫皮</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD%E7%9A%84%E4%B8%8A%E9%B3%9E" title="紫水兽的上鳞">紫水兽的上鳞</a>*2 <font color="#008000"><b>10%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> <a href="/%E6%B5%B7%E7%BB%B5%E8%B4%A8%E7%9A%84%E7%B4%AB%E7%9A%AE" title="海绵质的紫皮">海绵质的紫皮</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E5%A4%A7%E6%B0%B4%E8%A2%8B" title="大水袋">大水袋</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E9%AB%98%E7%BA%A7%E7%9A%84%E5%A4%B4%E5%86%A0" title="高级的头冠">高级的头冠</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E7%8B%82%E8%B5%B0%E6%B5%B8%E5%87%BA%E7%89%A9" title="狂走浸出物">狂走浸出物</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E6%B0%B4%E8%A2%8B" title="水袋">水袋</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>紫水の毒
</p>
</td><td>
<p>讨伐一头紫水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>フラッシュフラッド
</p>
</td><td>
<p>讨伐一头<a href="/%E7%BB%BF%E8%BF%85%E9%BE%99" title="绿迅龙">绿迅龙</a>和一头紫水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>集え！水没林の大連続戦
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>選ばれし者とは
</p>
</td><td>
<p>讨伐全部大型怪物
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>集え！水没林の紫水獣戦
</p>
</td><td>
<p>讨伐一头紫水兽
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>水没林へ向かう我が主のために
</p>
</td><td>
<p>讨伐一头<a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>和一头紫水兽
</p>
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 爬动向猎人突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 跃身使用飞扑攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前进飞扑 </td><td> 先不断修正方位朝猎人快速前进，到一定距离时接飞扑
</td></tr>
<tr style="background:#f9f9f9;">
<td> 高速突进 </td><td> 先后退一小段距离后呈游动状想猎人进行高速突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续啃咬 </td><td> 向前方连续啃咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下压 </td><td> 努力直起上身后使用下压攻击，压倒地面后还会溅起毒液，使人陷入中毒异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 横转 </td><td> 横着身体使用滚动攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 毒弹 </td><td> 吐出毒弹：向前吐1颗；分别往左中右方向吐3颗。打中会陷入中毒异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 毒爪 </td><td> 挥动前爪攻击，附带毒属性
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬&amp;甩尾 </td><td> 转身使用啃咬的同时尾部也存在甩尾判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续甩尾 </td><td> 尾巴左右连续甩两下进行攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 乱爬 </td><td> 边突进边左右依次不断吐毒弹，可以连续突进3次
</td></tr></tbody></table>

# 炎戈龍
![](assets/炎戈龍.jpg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×<br>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>○<br>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>◎<br>×</center></div>
</td><td><div><center>头<br>胸部</center></div>
</td><td><div><center>头<br>胸部</center></div>
</td><td><div><center>头<br>胸部</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">55</font> (15) </td><td> <font color="#ff0000">60</font> (15) </td><td> <font color="#ff0000">50</font> (15) </td><td> 0 (10) </td><td> 30 </td><td> 25 (15) </td><td> 20 (25) </td><td> 35 (0) </td><td> 100 </td><td> 100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 胸部 </td><td> <font color="#ff0000">50</font> (15) </td><td> <font color="#ff0000">45</font> (15) </td><td> <font color="#ff0000">45</font> (15) </td><td> 0 (10) </td><td> 25 </td><td> 20 (10) </td><td> 20 </td><td> 35 (0) </td><td> 0 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 40 (15) </td><td> 35 (15) </td><td> 30 (15) </td><td> 0 (10) </td><td> 15 </td><td> 10 (0) </td><td> 15 </td><td> 20 (0) </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 40 (15) </td><td> 44 (15) </td><td> 35 (15) </td><td> 0 (10) </td><td> 20 </td><td> 10 (0) </td><td> 15 (20) </td><td> 30 (0) </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背鳍 </td><td> 20 </td><td> 24 </td><td> 20 </td><td> 5 </td><td> 15 </td><td> 10 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 35 (15) </td><td> 35 (15) </td><td> 25 (15) </td><td> 0 (10) </td><td> 20 </td><td> 10 (0) </td><td> 15 </td><td> 25 (0) </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 35 (15) </td><td> 40 (15) </td><td> 25 (15) </td><td> 0 (10) </td><td> 20 </td><td> 10 (0) </td><td> 15 </td><td> 25 (0) </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 35 (15) </td><td> 30 (15) </td><td> 25 (15) </td><td> 0 (10) </td><td> 20 </td><td> 15 (5) </td><td> 15 </td><td> 25 (0) </td><td> 0 </td><td> 160
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 200 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 110 (620) </td><td> 120 (780) </td><td> 120 (660) </td><td> 100 (600) </td><td> 75 (450)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 20秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> △ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒） （受到地下攻击会被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> △ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒） （受到地下攻击会被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> ○ </td><td> 13秒（潜地时使用，发怒无效，效果如同落穴）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>

## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="炎戈龙的甲壳">炎戈龙的甲壳</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%9A%AE" title="炎戈龙的皮">炎戈龙的皮</a> <font color="#008000"><b>25%</b></font><br><a href="/%E8%B5%A4%E7%83%AD%E7%9A%84%E8%83%B8%E5%A3%B3" title="赤热的胸壳">赤热的胸壳</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%88%AA" title="炎戈龙的爪">炎戈龙的爪</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a> <font color="#008000"><b>5%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="炎戈龙的坚壳">炎戈龙的坚壳</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="炎戈龙的上皮">炎戈龙的上皮</a> <font color="#008000"><b>20%</b></font><br><a href="/%E8%B5%A4%E7%83%AD%E7%9A%84%E5%9D%9A%E8%83%B8%E5%A3%B3" title="赤热的坚胸壳">赤热的坚胸壳</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="炎戈龙的甲壳">炎戈龙的甲壳</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="炎戈龙的锐爪">炎戈龙的锐爪</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a> <font color="#008000"><b>4%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a> <font color="#008000"><b>3%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="炎戈龙的尾巴">炎戈龙的尾巴</a> <font color="#008000"><b>65%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%B3%8D" title="炎戈龙的鳍">炎戈龙的鳍</a> <font color="#008000"><b>27%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="炎戈龙的尾巴">炎戈龙的尾巴</a> <font color="#008000"><b>60%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%BC%98%E8%B4%A8%E9%B3%8D" title="炎戈龙的优质鳍">炎戈龙的优质鳍</a> <font color="#008000"><b>25%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>7%</b></font> </td><td> 斩属性断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> 1 </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>72%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>35%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a> <font color="#008000"><b>8%</b></font> </td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头(嘴尖) </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%94%9A%E5%98%B4" title="炎戈龙的锚嘴">炎戈龙的锚嘴</a>*1 <font color="#008000"><b>64%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="炎戈龙的甲壳">炎戈龙的甲壳</a>*1 <font color="#008000"><b>31%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*1 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%94%9A%E5%98%B4" title="炎戈龙的锚嘴">炎戈龙的锚嘴</a>*1 <font color="#008000"><b>56%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="炎戈龙的坚壳">炎戈龙的坚壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>6%</b></font> </td><td> 一共要破两回
</td></tr>
<tr style="background:#f9f9f9;">
<td> 胸部 </td><td> <a href="/%E8%B5%A4%E7%83%AD%E7%9A%84%E8%83%B8%E5%A3%B3" title="赤热的胸壳">赤热的胸壳</a>*1 <font color="#008000"><b>55%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%9A%AE" title="炎戈龙的皮">炎戈龙的皮</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a>*1 <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E8%B5%A4%E7%83%AD%E7%9A%84%E5%9D%9A%E8%83%B8%E5%A3%B3" title="赤热的坚胸壳">赤热的坚胸壳</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="炎戈龙的上皮">炎戈龙的上皮</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%9A%AE" title="炎戈龙的皮">炎戈龙的皮</a>*2 <font color="#008000"><b>8%</b></font> </td><td> 一共要破两回
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*1 <font color="#008000"><b>37%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%88%AA" title="炎戈龙的爪">炎戈龙的爪</a>*1 <font color="#008000"><b>34%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%88%AA" title="炎戈龙的爪">炎戈龙的爪</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a>*1 <font color="#008000"><b>9%</b></font> </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="炎戈龙的锐爪">炎戈龙的锐爪</a>*1 <font color="#008000"><b>34%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*1 <font color="#008000"><b>19%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="炎戈龙的锐爪">炎戈龙的锐爪</a>*2 <font color="#008000"><b>18%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%88%AA" title="炎戈龙的爪">炎戈龙的爪</a>*3 <font color="#008000"><b>12%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a>*2 <font color="#008000"><b>5%</b></font> </td><td> 一只或两只，全破几率提升
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体(背) </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%B3%8D" title="炎戈龙的鳍">炎戈龙的鳍</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%9A%AE" title="炎戈龙的皮">炎戈龙的皮</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*1 <font color="#008000"><b>18%</b></font> </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%BC%98%E8%B4%A8%E9%B3%8D" title="炎戈龙的优质鳍">炎戈龙的优质鳍</a>*1 <font color="#008000"><b>48%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="炎戈龙的上皮">炎戈龙的上皮</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%B3%8D" title="炎戈龙的鳍">炎戈龙的鳍</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 背部击破，战斗胜利后必定奖励获得
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%9A%AE" title="炎戈龙的皮">炎戈龙的皮</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%B3%8D" title="炎戈龙的鳍">炎戈龙的鳍</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%88%AA" title="炎戈龙的爪">炎戈龙的爪</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E8%B5%A4%E7%83%AD%E7%9A%84%E8%83%B8%E5%A3%B3" title="赤热的胸壳">赤热的胸壳</a>*2 <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="炎戈龙的上皮">炎戈龙的上皮</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E4%BC%98%E8%B4%A8%E9%B3%8D" title="炎戈龙的优质鳍">炎戈龙的优质鳍</a>*1 <font color="#008000"><b>23%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E7%87%83%E9%B3%9E" title="炎戈龙的燃鳞">炎戈龙的燃鳞</a>*2 <font color="#008000"><b>16%</b></font><br><a href="/%E7%82%8E%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="炎戈龙的锐爪">炎戈龙的锐爪</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E8%B5%A4%E7%83%AD%E7%9A%84%E5%9D%9A%E8%83%B8%E5%A3%B3" title="赤热的坚胸壳">赤热的坚胸壳</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>栖息在火海的龙
</p>
</td><td>
<p>讨伐一头 炎戈龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>双焰
</p>
</td><td>
<p><a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>、炎戈龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>栖息在火海的龙
</p>
</td><td>
<p><a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>、炎戈龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>火山炎上！
</p>
</td><td>
<p><a href="/%E7%88%86%E9%94%A4%E9%BE%99" title="爆锤龙">爆锤龙</a>、炎戈龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>栖息在火海的龙
</p>
</td><td>
<p>讨伐一头炎戈龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>炎火缭乱！
</p>
</td><td>
<p>讨伐两头炎戈龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>被选中的人是
</p>
</td><td>
<p><a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>、炎戈龙、<a href="/%E7%94%B5%E6%80%AA%E9%BE%99" title="电怪龙">电怪龙</a>、<a href="/%E5%86%B0%E7%A2%8E%E9%BE%99" title="冰碎龙">冰碎龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 预测龙车 </td><td> 先后撤一段距离后根据猎人的行动方位进行预测型突进，小心别中它的计！
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下压 </td><td> 直起上半身后下压身体进行攻击，胸口热化状态时还会溅出岩浆，使猎人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 4连啄 </td><td> 边向前移动边将嘴巴啄向地面，同时溅出岩浆，使猎人陷入燃烧异常，连续啄4次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 热线 </td><td> 吐出热线攻击，分直线型和横扫型，同时会使猎人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 2连啃咬 </td><td> 向前连续啃咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身后用身体猛地撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或进入怒状态时会使用咆哮威吓猎人，属于低吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬&amp;甩尾 </td><td> 转身使用啃咬的同时尾部也存在甩尾判定，如果在尾部热化状态被尾巴甩到的话会陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 潜行 </td><td> 先钻入地面或天花板，然后突然冲出来攻击猎人，可以连续钻好几次，被打中时也会陷入燃烧异常
</td></tr></tbody></table>

# 凍戈龍
![](assets/凍戈龍.jpg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性耐性</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>△<br>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>×<br>◎</center></div>
</td><td><div><center>头(冰块暖化时)<br>胸部</center></div>
</td><td><div><center>头(冰块暖化时)<br>胸部</center></div>
</td><td><div><center>头(冰块暖化时)<br>胸部</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 15 <font color="#ff0000">(55) </font></td><td> 15 <font color="#ff0000">(60) </font></td><td> 15 <font color="#ff0000">(50) </font></td><td> 30 </td><td> 0 </td><td> 15(25) </td><td> 0 </td><td> 0 (35) </td><td> 100 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 胸部 </td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 25 </td><td> 0 </td><td> 20 </td><td> 0 </td><td> 30 </td><td> 0 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 15 (40) </td><td> 15 (35) </td><td> 15 (30) </td><td> 15 </td><td> 0 </td><td> 0 (10) </td><td> 0 </td><td> 0 (20) </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 15 (40) </td><td> 15 (44) </td><td> 15 (35) </td><td> 25 </td><td> 0 </td><td> 0 (10) </td><td> 0 </td><td> 0 (30) </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背鳍 </td><td> 20 </td><td> 24 </td><td> 20 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 20 </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 15 (35) </td><td> 15 (35) </td><td> 15 (25) </td><td> 20 </td><td> 0 </td><td> 0 (10) </td><td> 0 </td><td> 0 (25) </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 15 (35) </td><td> 15 (40) </td><td> 15 (25) </td><td> 20 </td><td> 0 </td><td> 0 (10) </td><td> 0 </td><td> 0 (25) </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 15 (35) </td><td> 15 (30) </td><td> 15 (25) </td><td> 20 </td><td> 0 </td><td> 5 (15) </td><td> 0 </td><td> 0 (25) </td><td> 0 </td><td> 160
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 200 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 110 (620) </td><td> 120 (780) </td><td> 120 (660) </td><td> 100 (600) </td><td> 75 (450)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 20秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> △ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒） （受到地下攻击会被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> △ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒） （受到地下攻击会被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> ○ </td><td> 13秒（潜地时使用，发怒无效，效果如同落穴）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★7（难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td>
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> 下位无此怪物 </td><td> <a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冻戈龙的坚壳">冻戈龙的坚壳</a> <font color="#008000"><b>40%</b></font><br><a href="/%E6%B9%BF%E6%B6%A6%E7%9A%84%E5%9D%9A%E8%83%B8%E5%A3%B3" title="湿润的坚胸壳">湿润的坚胸壳</a> <font color="#008000"><b>27%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="冻戈龙的上皮">冻戈龙的上皮</a> <font color="#008000"><b>20%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冻戈龙的锐爪">冻戈龙的锐爪</a> <font color="#008000"><b>8%</b></font><br><a href="/%E5%86%B0%E6%99%B6%E9%87%91%E5%B1%9E" title="冰晶金属">冰晶金属</a> <font color="#008000"><b>5%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> 下位无此怪物 </td><td> <a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="冻戈龙的尾巴">冻戈龙的尾巴</a> <font color="#008000"><b>65%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E4%BC%98%E8%B4%A8%E9%B3%8D" title="冻戈龙的优质鳍">冻戈龙的优质鳍</a> <font color="#008000"><b>27%</b></font><br><a href="/%E5%86%B0%E6%99%B6%E9%87%91%E5%B1%9E" title="冰晶金属">冰晶金属</a> <font color="#008000"><b>8%</b></font> </td><td> 斩属性断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> 1 </td><td> 下位无此怪物 </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%86%B0%E6%99%B6%E9%87%91%E5%B1%9E" title="冰晶金属">冰晶金属</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冻戈龙的坚壳">冻戈龙的坚壳</a> <font color="#008000"><b>10%</b></font> </td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头(嘴尖) </td><td> 下位无此怪物 </td><td> <a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E9%94%9A%E5%98%B4" title="冻戈龙的锚嘴">冻戈龙的锚嘴</a>*1 <font color="#008000"><b>64%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冻戈龙的坚壳">冻戈龙的坚壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>6%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚破坏 </td><td> 下位无此怪物 </td><td> <a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冻戈龙的锐爪">冻戈龙的锐爪</a>*1 <font color="#008000"><b>52%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冻戈龙的锐爪">冻戈龙的锐爪</a>*2 <font color="#008000"><b>28%</b></font><br><a href="/%E5%86%B0%E6%99%B6%E9%87%91%E5%B1%9E" title="冰晶金属">冰晶金属</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E5%86%B0%E6%99%B6%E9%87%91%E5%B1%9E" title="冰晶金属">冰晶金属</a>*2 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背中破坏 </td><td> 下位无此怪物 </td><td> <a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E4%BC%98%E8%B4%A8%E9%B3%8D" title="冻戈龙的优质鳍">冻戈龙的优质鳍</a>*1 <font color="#008000"><b>61%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="冻戈龙的上皮">冻戈龙的上皮</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> <a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="冻戈龙的上皮">冻戈龙的上皮</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E4%BC%98%E8%B4%A8%E9%B3%8D" title="冻戈龙的优质鳍">冻戈龙的优质鳍</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E6%B9%BF%E6%B6%A6%E7%9A%84%E5%9D%9A%E8%83%B8%E5%A3%B3" title="湿润的坚胸壳">湿润的坚胸壳</a>*2 <font color="#008000"><b>12%</b></font><br><a href="/%E5%86%B0%E6%99%B6%E9%87%91%E5%B1%9E" title="冰晶金属">冰晶金属</a>*1 <font color="#008000"><b>11%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冻戈龙的锐爪">冻戈龙的锐爪</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E5%86%BB%E6%88%88%E9%BE%99%E7%9A%84%E9%94%9A%E5%98%B4" title="冻戈龙的锚嘴">冻戈龙的锚嘴</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>氷の禊
</p>
</td><td>
<p>讨伐一头冻戈龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>ハンターからの依頼
</p>
</td><td>
<p>讨伐一头<a href="/%E7%94%B5%E6%80%AA%E9%BE%99" title="电怪龙">电怪龙</a>和一头冻戈龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>凍土に放り込まれたものたち
</p>
</td><td>
<p>两头<a href="/%E7%9C%A0%E7%8B%97%E9%BE%99" title="眠狗龙">眠狗龙</a>、一头<a href="/%E5%86%B0%E7%A2%8E%E9%BE%99" title="冰碎龙">冰碎龙</a>、一头冻戈龙连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 预测龙车 </td><td> 先后撤一段距离后根据猎人的行动方位进行预测型突进，小心别中它的计！
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下压 </td><td> 直起上半身后下压身体进行攻击，并使猎人陷入冰冻异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 4连啄 </td><td> 边向前移动边将嘴巴啄向地面，并能使猎人陷入冰冻异常，连续啄4次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 水射线 </td><td> 喷水柱攻击，分直线型和横扫型，同时会使猎人陷入潮湿异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 2连啃咬 </td><td> 向前连续啃咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身后用身体猛地撞击猎人，连续靠2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或进入怒状态时会使用咆哮威吓猎人，属于低吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬&amp;甩尾 </td><td> 转身使用啃咬的同时尾部也存在甩尾判定，如果在尾部冻结状态时被尾巴甩到的话会陷入冰冻异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 潜行 </td><td> 先钻入地面或天花板，然后突然冲出来攻击猎人，可以连续钻好几次，被打中时也会陷入冻结异常
</td></tr></tbody></table>

# 潛口龍
![](assets/潛口龍.jpg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>鼻尖<br>口内<br>腮<br>身体(腹部·绿侧)(被钓出来时)</center></div>
</td><td><div><center>鼻尖<br>口内<br>腮<br>身体(腹部·绿侧)(被钓出来时)</center></div>
</td><td><div><center>鼻尖<br>口内<br>腮<br>身体(背部)(被钓出来时)</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 24 </td><td> 26 </td><td> 20 </td><td> 0 </td><td> 15 </td><td> 10 </td><td> 15 </td><td> 5 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 鼻尖 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 0 </td><td> 10 </td><td> 15 </td><td> 30 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 口内 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">60 </font></td><td> 0 </td><td> 10 </td><td> 10 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 鳃 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 0 </td><td> 10 </td><td> 15 </td><td> 15 </td><td> 5 </td><td> 0 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体(背中) </td><td> 36 (41) </td><td> 30 (35) </td><td> 40 <font color="#ff0000">(45) </font></td><td> 0 </td><td> 15 </td><td> 15 </td><td> 20 </td><td> 5 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体(腹部·绿侧) </td><td> 36 <font color="#ff0000">(50) </font></td><td> 30 <font color="#ff0000">(50) </font></td><td> 30 (40) </td><td> 0 </td><td> 10 </td><td> 25 </td><td> 20 </td><td> 5 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体(尾巴处) </td><td> 26 (31)	</td><td> 24 (29) </td><td> 24 (29) </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 15 </td><td> 5 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 30 </td><td> 30 </td><td> 30 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 30 </td><td> 30 </td><td> 30 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 左140/右140
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (580) </td><td> 180 (900) </td><td> 180 (900) </td><td> 200 (980) </td><td> 150 (780)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 100秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> △ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒） （受到地下攻击会被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> △ </td><td> 吸引潜地时的潜口龙出来，当该位置有爆弹时，它会吞下去，发怒时有效，但是没有拘束的效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★4（容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场<br>下位</b> </td><td> <b>集会浴场<br>上位</b> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="潜口龙的甲壳">潜口龙的甲壳</a> <font color="#008000"><b>50%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%9A%AE" title="潜口龙的皮">潜口龙的皮</a> <font color="#008000"><b>30%</b></font><br><a href="/%E9%B2%9C%E8%89%B3%E7%9A%84%E4%BD%93%E6%B6%B2" title="鲜艳的体液">鲜艳的体液</a> <font color="#008000"><b>15%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="潜口龙的头壳">潜口龙的头壳</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="潜口龙的坚壳">潜口龙的坚壳</a> <font color="#008000"><b>48%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="潜口龙的上皮">潜口龙的上皮</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E4%BD%93%E6%B6%B2" title="极彩色的体液">极彩色的体液</a> <font color="#008000"><b>12%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="潜口龙的头壳">潜口龙的头壳</a> <font color="#008000"><b>10%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%9A%AE" title="潜口龙的皮">潜口龙的皮</a> <font color="#008000"><b>5%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E9%B2%9C%E8%89%B3%E7%9A%84%E4%BD%93%E6%B6%B2" title="鲜艳的体液">鲜艳的体液</a> <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E4%BD%93%E6%B6%B2" title="极彩色的体液">极彩色的体液</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%B2%9C%E8%89%B3%E7%9A%84%E4%BD%93%E6%B6%B2" title="鲜艳的体液">鲜艳的体液</a> <font color="#008000"><b>5%</b></font> </td><td> 钓鱼成功时掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 鳃 </td><td> <a href="/%E9%B2%9C%E8%89%B3%E7%9A%84%E4%BD%93%E6%B6%B2" title="鲜艳的体液">鲜艳的体液</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="潜口龙的甲壳">潜口龙的甲壳</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="潜口龙的头壳">潜口龙的头壳</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E4%BD%93%E6%B6%B2" title="极彩色的体液">极彩色的体液</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="潜口龙的坚壳">潜口龙的坚壳</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="潜口龙的头壳">潜口龙的头壳</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="潜口龙的甲壳">潜口龙的甲壳</a>*2 <font color="#008000"><b>5%</b></font> </td><td> 疲劳时会露出鳃部呼吸
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> <a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%88%AA" title="潜口龙的爪">潜口龙的爪</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%9A%AE" title="潜口龙的皮">潜口龙的皮</a>*1 <font color="#008000"><b>30%</b></font> </td><td> <a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="潜口龙的尖爪">潜口龙的尖爪</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="潜口龙的上皮">潜口龙的上皮</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%88%AA" title="潜口龙的爪">潜口龙的爪</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 破爪后奖励获得，双爪全破几率提升
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%9A%AE" title="潜口龙的皮">潜口龙的皮</a>*1 <font color="#008000"><b>45%</b></font><br><a href="/%E9%B2%9C%E8%89%B3%E7%9A%84%E4%BD%93%E6%B6%B2" title="鲜艳的体液">鲜艳的体液</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E7%88%AA" title="潜口龙的爪">潜口龙的爪</a>*1 <font color="#008000"><b>23%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="潜口龙的头壳">潜口龙的头壳</a>*1 <font color="#008000"><b>7%</b></font> </td><td> <a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="潜口龙的上皮">潜口龙的上皮</a>*1 <font color="#008000"><b>42%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="潜口龙的尖爪">潜口龙的尖爪</a>*1 <font color="#008000"><b>23%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E4%BD%93%E6%B6%B2" title="极彩色的体液">极彩色的体液</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="潜口龙的头壳">潜口龙的头壳</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E9%B2%9C%E8%89%B3%E7%9A%84%E4%BD%93%E6%B6%B2" title="鲜艳的体液">鲜艳的体液</a>*2 <font color="#008000"><b>5%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>赤甲激震！潜口猛進！
</p>
</td><td>
<p>讨伐一头<a href="/%E8%B5%A4%E7%94%B2%E5%85%BD" title="赤甲兽">赤甲兽</a>和一头潜口龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>砂上のテーブルマナー
</p>
</td><td>
<p>讨伐一头潜口龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>砂原の大食い選手権
</p>
</td><td>
<p>一头潜口龙和一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>砂上のテーブルマナー
</p>
</td><td>
<p>讨伐一头潜口龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>大口に挟まれて
</p>
</td><td>
<p>讨伐两头潜口龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>戦慄の進軍
</p>
</td><td>
<p>一头潜口龙、一头<a href="/%E8%A7%92%E9%BE%99" title="角龙">角龙</a>、一头<a href="/%E9%BB%91%E8%BD%B0%E9%BE%99" title="黑轰龙">黑轰龙</a>连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%B8%A9%E6%B3%89%E4%BB%BB%E5%8A%A1" title="温泉任务">温泉任务</a>
</p>
</td><td>
<p>灼熱の砂風呂の底で
</p>
</td><td>
<p>讨伐一头潜口龙
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 张开大嘴巴朝猎人进行突进攻击，可以连续3次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 潜沙 </td><td> 先潜入沙中后，突然跃出来攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 吐沙 </td><td> 从嘴里吐出巨大的沙暴射线，分直线型和横扫型，被打中会陷入潮湿异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背后喷砂 </td><td> 背后较大范围内喷出沙尘攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 用其大嘴的咬向猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 2连啃咬 </td><td> 朝前方连续啃咬2次
</td></tr></tbody></table>
