# 通配符證書

通配符證書 需要使用 TXT 記錄驗證證書 所有權

```sh
certbot certonly \
--email your-email@example.com \
--agree-tos \
--preferred-challenges dns \
--server https://acme-v02.api.letsencrypt.org/directory \
--manual \
-d yourdomain.com \
-d *.yourdomain.com
```

* certonly 獲取或更新證書
* --email 可選參數 重要通知郵件地址
* --agree-tos 同意ACME服務器訂閱協議
* --preferred-challenges dns 以DNS Plugins方式驗證
* --server https://acme-v02.api.letsencrypt.org/directory 指定驗證服務器地址爲 acme-v02 默認的acme-v01 不支持通配符
* --manual  手動驗證
* -d yourdomain.com -d *.yourdomain.com 要驗證的域名

執行上述指令後 certbot 會生成一個 TXT 的值 需要爲域名 添加一個 TXT記錄 在確認記錄生效後 回車繼續即可成功申請證書

使用 `dig -t txt xxx.yourdomain.com` 查詢 TXT 解析值
# 自動驗證

certbot 提供了 腳本自動驗證

官網文檔 [https://certbot.eff.org/docs/using.html#pre-and-post-validation-hooks](https://certbot.eff.org/docs/using.html#pre-and-post-validation-hooks)

```
certbot certonly \
--email your-email@example.com \
--agree-tos \
--preferred-challenges dns \
--server https://acme-v02.api.letsencrypt.org/directory \
--manual \
--manual-auth-hook /path/to/http/authenticator.sh \
--manual-cleanup-hook /path/to/http/cleanup.sh \
-d yourdomain.com \
-d *.yourdomain.com
```

* --manual-auth-hook 指定了驗證腳本
* --manual-cleanup-hook 指定了清理腳本

如果 使用了 自動驗證 則執行 **certbot renew** 即可更新證書

如果已經使用 手動驗證了 可以修改 配置檔案 **/etc/letsencrypt/renewal/yourdomain.com.conf** 添加上 hook腳本 以便後續可以自動更新證書
```
manual_auth_hook = /path/to/http/authenticator.sh
manual_cleanup_hook = /path/to/http/cleanup.sh
```

# Example
```
certbot certonly \
--email your-email@example.com \
--agree-tos \
--preferred-challenges dns \
--server https://acme-v02.api.letsencrypt.org/directory \
--manual \
--manual-auth-hook /path/to/http/authenticator.sh \
--manual-cleanup-hook /path/to/http/cleanup.sh \
-d yourdomain.com \
-d *.yourdomain.com
```

```
#info="authenticator.sh"
#!/bin/bash

# Get your API key from https://www.cloudflare.com/a/account/my-account
API_KEY="your-api-key"
EMAIL="your.email@example.com"

# Strip only the top domain to get the zone id
DOMAIN=$(expr match "$CERTBOT_DOMAIN" '.*\.\(.*\..*\)')

# Get the Cloudflare zone id
ZONE_EXTRA_PARAMS="status=active&page=1&per_page=20&order=status&direction=desc&match=all"
ZONE_ID=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$DOMAIN&$ZONE_EXTRA_PARAMS" \
     -H     "X-Auth-Email: $EMAIL" \
     -H     "X-Auth-Key: $API_KEY" \
     -H     "Content-Type: application/json" | python -c "import sys,json;print(json.load(sys.stdin)['result'][0]['id'])")

# Create TXT record
CREATE_DOMAIN="_acme-challenge.$CERTBOT_DOMAIN"
RECORD_ID=$(curl -s -X POST "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records" \
     -H     "X-Auth-Email: $EMAIL" \
     -H     "X-Auth-Key: $API_KEY" \
     -H     "Content-Type: application/json" \
     --data '{"type":"TXT","name":"'"$CREATE_DOMAIN"'","content":"'"$CERTBOT_VALIDATION"'","ttl":120}' \
             | python -c "import sys,json;print(json.load(sys.stdin)['result']['id'])")
# Save info for cleanup
if [ ! -d /tmp/CERTBOT_$CERTBOT_DOMAIN ];then
        mkdir -m 0700 /tmp/CERTBOT_$CERTBOT_DOMAIN
fi
echo $ZONE_ID > /tmp/CERTBOT_$CERTBOT_DOMAIN/ZONE_ID
echo $RECORD_ID > /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID

# Sleep to make sure the change has time to propagate over to DNS
sleep 25
```

```
#info="cleanup.sh"
#!/bin/bash

# Get your API key from https://www.cloudflare.com/a/account/my-account
API_KEY="your-api-key"
EMAIL="your.email@example.com"

if [ -f /tmp/CERTBOT_$CERTBOT_DOMAIN/ZONE_ID ]; then
        ZONE_ID=$(cat /tmp/CERTBOT_$CERTBOT_DOMAIN/ZONE_ID)
        rm -f /tmp/CERTBOT_$CERTBOT_DOMAIN/ZONE_ID
fi

if [ -f /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID ]; then
        RECORD_ID=$(cat /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID)
        rm -f /tmp/CERTBOT_$CERTBOT_DOMAIN/RECORD_ID
fi

# Remove the challenge TXT record from the zone
if [ -n "${ZONE_ID}" ]; then
    if [ -n "${RECORD_ID}" ]; then
        curl -s -X DELETE "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$RECORD_ID" \
                -H "X-Auth-Email: $EMAIL" \
                -H "X-Auth-Key: $API_KEY" \
                -H "Content-Type: application/json"
    fi
fi
```
