# rime

rime 既 Rime Input Method Engine 的簡寫 是爲 中州韻輸入法引擎

是由 佛振 使用 c++ 編寫的 開源(BSD) 多平臺 中文輸入法 解決了linux沒有好用 中文輸入方案的 窘境

* 官網 [http://rime.im/](http://rime.im/)
* 源碼 [https://github.com/rime/librime](https://github.com/rime/librime)
* wiki [https://zh.wikipedia.org/wiki/中州韻輸入法引擎](https://zh.wikipedia.org/wiki/%E4%B8%AD%E5%B7%9E%E9%9F%BB%E8%BC%B8%E5%85%A5%E6%B3%95%E5%BC%95%E6%93%8E)

# 發行版本
* 官方 維護這 linux 的 ibus-rime 既 中州韻
* windows 下的 小狼毫 (Weasel)
* OS X 發行版鼠鬚管 (Squirrel)

> 另外 linux下 fcitx 團隊 維護了一份 fcitx-rime
> 
