# [格式化](https://moment.github.io/luxon/#/formatting)

格式化的功能分爲三種類型

1. 基於 ISO 8601 和 RFC 2822 等標準
2. 國際化的人類可讀格式
3. 基於令牌的自定義格式化


# Technical formats (strings for computers)

一些標準的格式化技術，通常用於計算機

## ISO 8601

[ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) 是最廣泛應用的日期和時間字符串格式集。Luxon 可以解析範圍廣泛的 ISO 集，但只提供了其中幾個格式化的直接支持：

```
dt.toISO(); //=> '2017-04-20T11:32:00.000-04:00'
dt.toISODate(); //=> '2017-04-20'
dt.toISOWeekDate(); //=> '2017-W17-7'
dt.toISOTime(); //=> '11:32:00.000-04:00'
```

通常你應該使用 toISO 來在計算機程式之間傳遞日期時間

## HTTP and RFC 2822

有許多傳統的標準日期和時間格式，Luxon 支持其中的一些。除非有特定理由，否則你不應該使用它們

```
dt.toRFC2822(); //=> 'Thu, 20 Apr 2017 11:32:00 -0400'
dt.toHTTP(); //=> 'Thu, 20 Apr 2017 03:32:00 GMT'
```

## Unix timestamps

DateTime 對象也可以轉換爲數字的 [unix 時間戳](https://en.wikipedia.org/wiki/Unix_time)

```
dt.toMillis(); //=> 1492702320000
dt.toSeconds(); //=> 1492702320.000
dt.valueOf(); //=> 1492702320000, same as .toMillis()
```

# toLocaleString (strings for humans)

爲人類生成可讀的字符串

## 基礎

現代瀏覽器(和其它 js 環境) 提供了對人類可讀的字符串的支持。Luxon 爲它們提供了方便的支持，你應該在向用戶顯示時間的時候使用它們。通過調用 toLocaleString 來做到這一點：

```
dt.toLocaleString(); //=> '4/20/2017'
dt.toLocaleString(DateTime.DATETIME_FULL); //=> 'April 20, 2017 at 11:32 AM EDT'
dt.setLocale('fr').toLocaleString(DateTime.DATETIME_FULL); //=> '20 avril 2017 à 11:32 UTC−4'
```

## Intl.DateTimeFormat

在上面的例子中 DateTime.DATETIME_FULL 是 Luxon 提供的幾種預定義格式之一。但實際上可以提供 Intl.DateTimeFormat 的任何選項對象。例如：

```
dt.toLocaleString({ month: 'long', day: 'numeric' }); //=> 'April 20'
```

這是所有預設值：
```
DateTime.DATETIME_FULL;  //=> {
                         //     year: 'numeric',
                         //     month: 'long',
                         //     day: 'numeric',
                         //     hour: 'numeric',
                         //     minute: '2-digit',
                         //     timeZoneName: 'short'
                         //   }
```

這意味着你可以根據自己的選擇修改預設：

```
dt.toLocaleString(DateTime.DATE_SHORT); //=>  '4/20/2017'
var newFormat = {...DateTime.DATE_SHORT, weekday: 'long' };
dt.toLocaleString(newFormat); //=>  'Thursday, 4/20/2017'
```

## 預設

Luxon 預設了一些值，以下是以 1983-10-14 13:30:23 爲例提供的全套預設

```
DateTime.fromObject({
    year: 1983,
    month: 10,
    day: 14,
    hour: 13,
    minute: 30,
    second: 23,
}, {
    zone: 'America/New_York'
})
```

|	Name	 |	描述	 |	en-US	|	fr	|	zh-Hant |
| -------- | -------- | -------- | -------- | -------- |
|	DATE\_SHORT	|	短日期	|	10/14/1983	|	14/10/1983	|	1983/10/14	|
|	DATE\_MED	 |	縮寫日期	 |	Oct 14, 1983	 |	14 oct. 1983	 |	1983年10月14日	|
|	DATE\_MED\_WITH\_WEEKDAY	|	帶星期的縮寫日期	 |	Fri, Oct 14, 1983	|	ven. 14 oct. 1983	|	1983年10月14日 週五 |
|	DATE\_FULL	|	完整日期	 |	October 14, 1983	 |	14 octobre 1983 |	1983年10月14日	|
|	DATE\_HUGE	|	帶星期的完整日期	 |	Friday, October 14, 1983	|	vendredi 14 octobre 1983	 |	1983年10月14日 星期五	|
|	TIME\_SIMPLE |	時間	|	1:30 PM |	13:30	|	下午1:30	|
|	TIME\_WITH\_SECONDS	|	帶秒數的時間	|	1:30:23 PM	|	13:30:23	|	下午1:30:23 |
|	TIME\_WITH\_SHORT\_OFFSET	|	帶秒數的時間和縮寫的偏移	|	1:30:23 PM EDT	|	13:30:23 UTC−4	|	下午1:30:23 [EDT]	|
|	TIME\_WITH\_LONG\_OFFSET	|	帶秒數的時間和完整的偏移	|	1:30:23 PM Eastern Daylight Time	|	13:30:23 heure d’été de l’Est	|	下午1:30:23 [東部夏令時間]	|
|	TIME\_24\_SIMPLE	|	24小時時間	|	13:30	|	13:30	|	13:30	|
|	TIME\_24\_WITH\_SECONDS	|	帶秒數的24小時時間	|	13:30:23	|	13:30:23	|	13:30:23	|
|	TIME\_24\_WITH\_SHORT\_OFFSET	|	帶秒數的24小時時間和縮寫的偏移	|	13:30:23 EDT	|	13:30:23 UTC−4	|	13:30:23 [EDT]	|
|	TIME\_24\_WITH\_LONG\_OFFSET	|	帶秒數的24小時時間和完整的偏移	|	13:30:23 Eastern Daylight Time	|	13:30:23 heure d’été de l’Est	|	13:30:23 [東部夏令時間]	|
|	DATETIME\_SHORT	|	短日期和時間	|	10/14/1983, 1:30 PM	|	14/10/1983 à 13:30	|	1983/10/14 下午1:30	|
|	DATETIME\_MED	|	縮寫日期和時間	|	Oct 14, 1983, 1:30 PM	|	14 oct. 1983 à 13:30	|	1983年10月14日 下午1:30	|
|	DATETIME\_FULL	|	完整日期和時間以及縮寫的偏移	|	October 14, 1983 at 1:30 PM EDT	|	14 octobre 1983 à 13:30 UTC−4	|	1983年10月14日 下午1:30 [EDT]	|
|	DATETIME\_HUGE	|	完整日期和時間星期以及完整的偏移	|	Friday, October 14, 1983 at 1:30 PM Eastern Daylight Time	|	vendredi 14 octobre 1983 à 13:30 heure d’été de l’Est	|	1983年10月14日 星期五 下午1:30 [東部夏令時間]	|
|	DATETIME\_SHORT\_WITH\_SECONDS |	短日期和帶秒數的時間	|	10/14/1983, 1:30:23 PM	|	14/10/1983 à 13:30:23	|	1983/10/14 下午1:30:23	|
|	DATETIME\_MED\_WITH\_SECONDS	|	縮寫日期和帶秒數的時間	|	Oct 14, 1983, 1:30:23 PM	|	14 oct. 1983 à 13:30:23	|	1983年10月14日 下午1:30:23	|
|	DATETIME\_FULL\_WITH\_SECONDS	|	完整日期和帶秒數的時間以及縮寫的偏移	|	October 14, 1983 at 1:30:23 PM EDT	|	14 octobre 1983 à 13:30:23 UTC−4	|	1983年10月14日 下午1:30:23 [EDT]	|
|	DATETIME\_HUGE\_WITH\_SECONDS	|	完整日期和帶秒數的時間星期以及完整的偏	|	Friday, October 14, 1983 at 1:30:23 PM Eastern Daylight Time	|	vendredi 14 octobre 1983 à 13:30:23 heure d’été de l’Est	|	1983年10月14日 星期五 下午1:30:23 [東部夏令時間]	|

toLocaleString 的行爲受到 locale numberingSystem outputCalendar 屬性的影響

# Formatting with tokens (strings for Cthulhu)

程序員可以使用格式令牌來自定義輸出

## 考慮替代方案

通常不應該自定義格式。如果打算讓計算機讀取推薦使用 toISO。如果讓人類讀取，推薦使用 toLocaleString。只有在上述兩種方案都不可行時，才應該考慮使用自定義輸出格式，toFormat 是你需要使用的函數

## toFormat

toFormat 將依據程序員定義的 token 來輸出

```
DateTime.fromISO('2014-08-06T13:07:04.054').toFormat('yyyy LLL dd'); //=> '2014 Aug 06'
```

## Intl
所有字符串(例如月份名稱和星期名稱)都通過 Intl API 生成的字符串進行國際化。因此你獲得的確切字符串是特定與實現的

```
DateTime.fromISO('2014-08-06T13:07:04.054')
  .setLocale('fr')
  .toFormat('yyyy LLL dd'); //=> '2014 août 06'
```

> 注意格式默認爲 en-US。如果需要將字符串國際化，則需要像上面例子中那樣顯示設置語言環境(或者更優先考慮使用 toLocaleString 函數)

## Escaping

你可以使用單引號來轉義字符串

```
DateTime.now().toFormat("HH 'hours and' mm 'minutes'"); //=> '20 hours and 55 minutes'
```

## Standalone vs format tokens

一些 tokens 具有 "standalone" 和 "format" 版本。某些語言需要不同形式的單詞，具體取決於它是較長短語的一部分還是本身

```
var d = DateTime.fromISO('2014-08-06T13:07:04.054').setLocale('ru');
d.toFormat('LLLL'); //=> 'август' (standalone)
d.toFormat('MMMM'); //=> 'августа' (format)
```

## Macro tokens

一些格式是 宏，這意謂著它們對應與多個組件。它們使用本機 Intl API 並將以對語言環境友好的方式對齊組成部分進行排序

```
DateTime.fromISO('2014-08-06T13:07:04.054').toFormat('ff'); //=> 'Aug 6, 2014, 1:07 PM'
```

可用的 宏 選項與爲 toLocaleString 定義的預設格式意義對應

## Tokens

令牌表以 2014-08-06T13:07:04.054 America/New_York 爲例子

|	Standalone	token	|	Format	token	|	描述	|	輸出	|
| -------- | -------- | -------- | -------- |
|	S	|		|	毫秒	|	54	|
|	SSS	|		|	毫秒 填充到3	|	054	|
|	u	|	|	秒的小數部分，和SSS功能一致	|	054	|
|	uu	|		|	秒的小數部分(0到 99) 填充到2	|	05	|
|	uuu	|		|	秒的小數部分(0到 9)	|	0	|
|	s	|		|	秒	|	4	|
|	ss	|		|	秒填充到2	|	04	|
|	m	|		|	分鐘	|	7	|
|	mm	|		|	分鐘 填充到2	|	07	|
|	h	|		|	小時 in 12	|	1	|
|	hh	|		|	小時 in 12 填充到2	|	01	|
|	H	|		|	小時 in 24	|	13	|
|	HH	|		|	小時 in 23 填充到2	|	13	|
|	Z	|		|	narrow offset	|	+5	|
|	ZZ	|		|	short offset	|	+05:00	|
|	ZZZ	|		|	techie offset	|	+0500	|
|	ZZZZ	|		|	縮寫偏移	|	EST	|
|	ZZZZZ	|		|	完整偏移名稱	|	Eastern Standard Time	|
|	z	|		|	IANA zone	|	America/New_York	|
|	a	|		|	meridiem	|	AM	|
|	d	|		|	號數	|	6	|
|	dd	|		|	號數 填充到2	|	06	|
|	c	|	E	|	星期 1到7	|	3	|
|	ccc	|	EEE	|	星期，作爲本地縮寫的字符串	|	Wed	|
|	cccc	|	EEEE	|	星期，作爲本地的字符串	|	Wednesday	|
|	ccccc	|	EEEEE	|	星期，作爲本地單個字母	|	W	|
|	L	|	M	|	月份	|	8	|
|	LL	|	MM	|	月份 填充到2	|	08	|
|	LLL	|	MMM	|	月份，作爲本地縮寫的字符串	|	Aug	|
|	LLLL	|	MMMM	|	月份，作爲本地的字符串	|	August	|
|	LLLL	|	MMMMM	|	月份，作爲本地單個字母	|	A	|
|	y	|		|	年份	|	2014	|
|	yy	|		|	2個字符年份	|	14	|
|	yyyy	|		|	4到6個字符的年份 填充到4	|	2014	|
|	G	|		|	縮寫的本地化時代	|	AD	|
|	GG	|		|	本地化時代	|	Anno Domini	|
|	GGGGG	|		|	本地化時代單個字母	|	A	|
|	kk	|		|	ISO week year	|	14	|
|	kkkk	|		|	ISO week year 填充到4	|	2014	|
|	W	|		|	ISO week number	|	32	|
|	WW	|		|	ISO week number 填充到2	|	32	|
|	o	|		|	day of year	|	218	|
|	ooo	|		|	day of year 填充到3	|	218	|
|	q	|		|	quarter	|	3	|
|	qq	|		|	quarter 填充到2	|	03	|
|	D	|		|	本地化數字日期	|	9/4/2017	|
|	DD	|		|	本地化縮寫日期	|	Aug 6, 2014	|
|	DDD	|		|	本地化完整日期	|	August 6, 2014	|
|	DDDD	|		|	帶星期的本地化完整日期	|	Wednesday, August 6, 2014	|
|	t	|		|	本地化時間	|	9:07 AM	|
|	tt	|		|	帶秒數的本地化時間	|	1:07:04 PM	|
|	ttt	|		|	帶秒數的本地化時間和縮寫偏移	|	1:07:04 PM EDT	|
|	tttt	|		|	帶秒數的本地化時間和完整偏移	|	1:07:04 PM Eastern Daylight Time	|
|	T	|		|	t 的 24小時形式	|	13:07	|
|	TT	|		|	tt 的 24小時形式	|	13:07:04	|
|	TTT	|		|	ttt 的 24小時形式	|	13:07:04 EDT	|
|	TTTT	|		|	tttt 的 24小時形式	|	13:07:04 Eastern Daylight Time	|
|	f	|		|	本地化的數字日期時間	|	8/6/2014, 1:07 PM	|
|	ff	|		|	本地化的短日期時間	|	Aug 6, 2014, 1:07 PM	|
|	fff	|		|	本地化的詳細日期時間	|	August 6, 2014, 1:07 PM EDT	|
|	ffff	|		|	本地化的額外詳細日期時間	|	Wednesday, August 6, 2014, 1:07 PM Eastern Daylight Time	|
|	F	|		|	f 的帶秒數形式	|	8/6/2014, 1:07:04 PM	|
|	FF	|		|	ff 的帶秒數形式	|	Aug 6, 2014, 1:07:04 PM	|
|	FFF	|		|	fff 的帶秒數形式	|	August 6, 2014, 1:07:04 PM EDT	|
|	FFFF	|		|	ffff 的帶秒數形式	|	Wednesday, August 6, 2014, 1:07:04 PM Eastern Daylight Time	|
|	X	|		|	unix timestamp in seconds	|	1407287224	|
|	x	|		|	unix timestamp in milliseconds	|	1407287224054	|
