# [1 ffmpeg](https://ffmpeg.org/ffmpeg.html)

```
ffmpeg [global_options] {[input_file_options] -i input_url} ... {[output_file_options] output_url} ...
```

# [2 說明](https://ffmpeg.org/ffmpeg.html#Description)
ffmpeg 是一個通用媒體轉換器。 它可以讀取各種輸入 - 包括即時抓取/記錄設備 - 過濾並將其轉碼為多種輸出格式。

ffmpeg 從 -i 選項指定的任意數量的輸入“文件”（可以是常規文件、管道、網絡流、抓取設備等）讀取數據，並寫入任意數量的輸出“文件”，其中由純輸出url指定。 在命令列上找到的任何不能解釋為選項的內容都被視為輸出 url。

原則上，每個輸入或輸出 URL 可以包含任意數量的不同類型的串流（視訊/音訊/字幕/附件/資料）。 允許的流數量和/或類型可能受到容器格式的限制。 選擇哪些輸入將進入哪些輸出的流可以自動完成，也可以使用 -map 選項（請參閱流選擇章節）。

要引用選項中的輸入文件，您必須使用它們的索引（從 0 開始）。 例如。 第一個輸入檔是 0，第二個輸入檔是 1，等等。類似地，文件中的流由它們的索引引用。 例如。 2:3 指第三個輸入檔中的第四個流。 另請參閱流說明符一章。

作為一般規則，選項將應用於下一個指定的檔案。 因此，順序很重要，您可以在命令列上多次使用相同的選項。 然後，每次出現的情況都會套用於下一個輸入或輸出檔。 此規則的例外是全域選項（例如詳細程度），應首先指定它。

不要混合輸入和輸出檔案 - 首先指定所有輸入文件，然後指定所有輸出檔案。 也不要混合屬於不同檔案的選項。 所有選項僅適用於下一個輸入或輸出文件，並在文件之間重設。

以下是一些簡單的例子。

* 透過重新編碼媒體串流，將輸入媒體檔案轉換為不同的格式：

	```
	ffmpeg -i input.avi output.mp4
	```
	
* 將輸出檔案的視訊位元率設定為 64 kbit/s

	```
	ffmpeg -i input.avi -b:v 64k -bufsize 64k output.mp4
	```
	
* 將輸出檔案的幀速率強制為 24 fps：

	```
	ffmpeg -i input.avi -r 24 output.mp4
	```
	
* 強制輸入檔案的幀速率（僅對原始格式有效）為 1 fps，輸出檔案的幀速率為 24 fps：

	```
	ffmpeg -r 1 -i input.m2v -r 24 output.mp4
	```
	
原始輸入檔(raw input)可能需要格式選項

# [3 詳細說明](https://ffmpeg.org/ffmpeg.html#Detailed-description)

ffmpeg中每個輸出的轉碼過程可以用下圖描述：

```
 _______              ______________
|       |            |              |
| input |  demuxer   | encoded data |   decoder
| file  | ---------> | packets      | -----+
|_______|            |______________|      |
                                           v
                                       _________
                                      |         |
                                      | decoded |
                                      | frames  |
                                      |_________|
 ________             ______________       |
|        |           |              |      |
| output | <-------- | encoded data | <----+
| file   |   muxer   | packets      |   encoder
|________|           |______________|
```

ffmpeg 呼叫 libavformat 函式庫（包含解復用器demuxer）來讀取輸入檔案並從中取得包含編碼資料的資料包。 當有多個輸入檔時，ffmpeg 會嘗試透過追蹤任何活動輸入流上的最低時間戳來保持它們同步。

然後，編碼的資料包被傳遞到解碼器（除非為流選擇了流複製，請參閱進一步的描述）。 解碼器產生未壓縮的幀（原始視訊/PCM 音訊/...），可以透過過濾進一步處理（請參閱下一節）。 過濾後，幀被傳遞到編碼器，編碼器對它們進行編碼並輸出編碼後的資料包。 最後，這些資料被傳遞到復用器，復用器將編碼後的資料包寫入輸出檔案。

## [3.1 過濾](https://ffmpeg.org/ffmpeg.html#Filtering)

在編碼之前，ffmpeg 可以使用 libavfilter 庫中的過濾器處理原始音訊和視訊幀。 幾個連結的過濾器形成一個過濾器圖。 ffmpeg 區分兩種類型的濾鏡圖：simple 和 complex。

### [3.1.1 Simple filtergraphs](https://ffmpeg.org/ffmpeg.html#Complex-filtergraphs)

Simple filtergraphs 是指只有一個輸入和輸出且類型相同的過濾圖。 在上圖中，它們可以透過簡單地在解碼和編碼之間插入一個附加步驟來表示：

```
 _________                        ______________
|         |                      |              |
| decoded |                      | encoded data |
| frames  |\                   _ | packets      |
|_________| \                  /||______________|
             \   __________   /
  simple     _\||          | /  encoder
  filtergraph   | filtered |/
                | frames   |
                |__________|
```

簡單的過濾器圖表使用每個串流的 -filter 選項進行配置（分別為視訊和音訊使用 -vf 和 -af 別名）。 一個簡單的視訊過濾圖可以如下所示：

```
 _______        _____________        _______        ________
|       |      |             |      |       |      |        |
| input | ---> | deinterlace | ---> | scale | ---> | output |
|_______|      |_____________|      |_______|      |________|
```

請注意，某些篩選器會變更框架屬性，但不會變更框架內容。 例如。 上例中的 fps 過濾器會變更幀數，但不影響幀內容。 另一個例子是 setpts 過濾器，它只設定時間戳，否則不改變地傳遞幀。

### [3.1.2 Complex filtergraphs](https://ffmpeg.org/ffmpeg.html#Complex-filtergraphs)

Complex filtergraphs 不能簡單地描述為應用於一個流的線性處理鏈。 例如，當圖具有多個輸入和/或輸出時，或當輸出流類型與輸入不同時，就會發生這種情況。 它們可以用下圖表示：

```
 _________
|         |
| input 0 |\                    __________
|_________| \                  |          |
             \   _________    /| output 0 |
              \ |         |  / |__________|
 _________     \| complex | /
|         |     |         |/
| input 1 |---->| filter  |\
|_________|     |         | \   __________
               /| graph   |  \ |          |
              / |         |   \| output 1 |
 _________   /  |_________|    |__________|
|         | /
| input 2 |/
|_________|
```

複雜的過濾器圖是使用 -filter_complex 選項配置的。 請注意，此選項是全域性的，因為複雜的篩選器圖本質上不能與單一流或檔案明確關聯。

-lavfi 選項相當於 -filter_complex。

複雜過濾器圖的一個簡單範例是覆蓋過濾器，它有兩個視訊輸入和一個視訊輸出，其中一個視訊疊加在另一個視訊之上。 它的音訊對應部分是 amix 濾鏡。

## [3.2 Stream copy](https://ffmpeg.org/ffmpeg.html#Stream-copy)

流複製是透過向 -codec 選項提供複製參數來選擇的模式。 它使 ffmpeg 省略指定流的解碼和編碼步驟，因此它只進行解復用和復用。 它對於更改容器格式或修改容器級元資料非常有用。 在這種情況下，上圖將簡化為：

```
 _______              ______________            ________
|       |            |              |          |        |
| input |  demuxer   | encoded data |  muxer   | output |
| file  | ---------> | packets      | -------> | file   |
|_______|            |______________|          |________|
```

由於沒有解碼或編碼，因此速度非常快且沒有品質損失。 然而，由於多種因素，它在某些情況下可能不起作用。 應用過濾器顯然也是不可能的，因為過濾器適用於未壓縮的資料。

# [4 Stream selection](https://ffmpeg.org/ffmpeg.html#Stream-selection)

ffmpeg 提供 -map 選項來手動控制每個輸出檔案中的流選擇。 使用者可以跳過 -map 並讓 ffmpeg 執行自動流選擇，如下所述。 -vn / -an / -sn / -dn 選項可用於分別跳過包含視訊、音訊、字幕和資料流，無論是手動映射還是自動選擇，但作為複雜過濾圖輸出的流除外。

## [4.1 描述](https://ffmpeg.org/ffmpeg.html#Description-1)

接下來的小節描述了流選擇中涉及的各種規則。 接下來的範例展示如何在實務中應用這些規則。

雖然我們已盡一切努力準確反映程式的行為，但 FFmpeg 仍在不斷開發中，自撰寫本文以來，程式碼可能已更改。

### [4.1.1 自動流選擇](https://ffmpeg.org/ffmpeg.html#Automatic-stream-selection)

在特定輸出檔案沒有任何映射選項的情況下，ffmpeg 檢查輸出格式以檢查其中可以包含哪種類型的流，即。 視訊、音訊和/或字幕。 對於每種可接受的流類型，ffmpeg 將從所有輸入中選擇一個可用的流。

它將根據以下標準選擇該流：
* 對於視頻，它是具有最高解析度的串流
* 對於音頻，它是具有最多通道的流
* 對於字幕，這是找到的第一個字幕流，但有一個警告。 輸出格式的預設字幕編碼器可以是基於文字的，也可以是基於圖像的，並且只會選擇相同類型的字幕流

在相同類型的多個流速率相等的情況下，選擇具有最低索引的流。

資料或附件流不會自動選擇，只能使用 -map 包含。

### [4.1.2 手動流選擇](https://ffmpeg.org/ffmpeg.html#Manual-stream-selection)

使用 -map 時，此輸出檔中僅包含 -map 指定的流，但下面所述的過濾器圖輸出可能有例外。

### [4.1.3 Complex filtergraphs](https://ffmpeg.org/ffmpeg.html#Complex-filtergraphs-1)

如果存在任何帶有未標記 pad 的複雜 filtergraph 輸出流，它們將被添加到第一個輸出檔案中。 如果輸出格式不支援流類型，這將導致致命錯誤。 在沒有地圖選項的情況下，包含這些串流會導致跳過其類型的自動串流選擇。 如果存在映射選項，除了映射的流之外，還包括這些過濾器圖流。

帶有標記的 pad 的複雜過濾圖輸出流必須映射一次且恰好一次。

### [4.1.4 Stream handling](https://ffmpeg.org/ffmpeg.html#Stream-handling)

流處理與流選擇無關，但下述字幕除外。 流處理是透過 -codec 選項設定的，該選項尋址到特定輸出檔案中的流。 特別是，編解碼器選項由 ffmpeg 在流選擇過程之後應用，因此不會影響後者。 如果沒有為流類型指定 -codec 選項，ffmpeg 將選擇輸出檔案復用器註冊的預設編碼器。

字幕存在例外情況。 如果為輸出檔案指定了字幕編碼器，則將包含找到的任何類型（文字或圖像）的第一個字幕流。 ffmpeg 不會驗證指定的編碼器是否可以轉換選定的流，或者轉換後的流是否在輸出格式中可接受。 這通常也適用：當使用者手動設定編碼器時，流選擇過程無法檢查編碼流是否可以重複使用到輸出檔案中。 如果不能，ffmpeg 將中止並且所有輸出檔案將無法處理。

## [4.2 Examples](https://ffmpeg.org/ffmpeg.html#Examples)

以下範例說明了 ffmpeg 流選擇方法的行為、怪癖和限制。

假設有以下三個輸入檔：

```
input file 'A.avi'
      stream 0: video 640x360
      stream 1: audio 2 channels

input file 'B.mp4'
      stream 0: video 1920x1080
      stream 1: audio 2 channels
      stream 2: subtitles (text)
      stream 3: audio 5.1 channels
      stream 4: subtitles (text)

input file 'C.mkv'
      stream 0: video 1280x720
      stream 1: audio 2 channels
      stream 2: subtitles (image)
```

### Example: 自動流選擇

```
ffmpeg -i A.avi -i B.mp4 out1.mkv out2.wav -map 1:a -c:a copy out3.mov
```

指定了三個輸出文件，對於前兩個文件，沒有設定 -map 選項，因此 ffmpeg 將自動為這兩個文件選擇流。

1. out1.mkv 是一個 Matroska 容器文件，接受視訊、音訊和字幕串流，因此 ffmpeg 將嘗試選擇每種類型之一
	* 對於視頻，它將從 B.mp4 中選 stream 0，該流是所有輸入視頻流中分辨率最高的。
	* 對於音頻，它將從 B.mp4 中選 stream 3，因為它具有最多的通道數。
	* 對於字幕，它將從 B.mp4 中選 stream 2，這是 A.avi 和 B.mp4 中的第一個字幕流

2. out2.wav 僅接受音訊串流，因此僅選擇 B.mp4 中的 stream 3
3. 對於out3.mov，由於設定了-map 選項，因此不會發生自動流選擇。 -map 1:a 選項將從第二個輸入 B.mp4 選擇所有音訊串流。 此輸出檔中不會包含其他流。

對於前兩個輸出，所有包含的流都將被轉碼。 選擇的編碼器將是每個輸出格式註冊的預設編碼器，它可能與所選輸入流的編解碼器不符。

對於第三個輸出，音訊串流的編解碼器選項已設定為複製，因此不會發生或可能發生解碼-過濾-編碼操作。 所選流的資料包應從輸入檔傳送並在輸出檔中復用。

### Example: 自動字幕選擇

```
ffmpeg -i C.mkv out1.mkv -c:s dvdsub -an out2.mkv
```

雖然out1.mkv是一個接受字幕串流的Matroska容器文件，但只能選擇視訊和音訊串流。 C.mkv 的字幕流是基於圖像的，而 Matroska 復用器的預設字幕編碼器是基於文字的，因此字幕的轉碼操作預計會失敗，因此不會選擇該串流。 然而，在out2.mkv中，在命令中指定了字幕編碼器，因此除了視訊串流之外，還選擇了字幕串流。 -an 的存在會停用 out2.mkv 的音訊串流選擇

### Example: unlabeled filtergraph outputs

```
ffmpeg -i A.avi -i C.mkv -i B.mp4 -filter_complex "overlay" out1.mp4 out2.srt
```

此處使用 -filter_complex 選項設定過濾器圖，並由單一影片過濾器組成。 覆蓋過濾器剛好需要兩個視訊輸入，但沒有指定，因此使用前兩個可用的視訊串流，即 A.avi 和 C.mkv。 過濾器的輸出 pad 沒有標籤，因此被傳送到第一個輸出檔 out1.mp4。 因此，會跳過視訊串流的自動選擇，這會選擇 B.mp4 中的串流。 具有大多數通道的音訊串流，即。 B.mp4 中的 stream2 3 是自動選擇的。 然而，沒有選擇字幕串流，因為 MP4 格式沒有註冊預設的字幕編碼器，而且使用者也沒有指定字幕編碼器。

第二個輸出檔 out2.srt 僅接受基於文字的字幕流。 因此，儘管第一個可用的字幕流屬於 C.mkv，但它是基於圖像的，因此會被跳過。 所選流（B.mp4 中的 stream2）是第一個基於文字的字幕流。

### Example: labeled filtergraph outputs

```
ffmpeg -i A.avi -i B.mp4 -i C.mkv -filter_complex "[1:v]hue=s=0[outv];overlay;aresample" \
       -map '[outv]' -an        out1.mp4 \
                                out2.mkv \
       -map '[outv]' -map 1:a:0 out3.mkv
```

上述命令將會失敗，因為標記為 [outv] 的輸出焊盤已被映射兩次。 不應處理任何輸出檔。

```
ffmpeg -i A.avi -i B.mp4 -i C.mkv -filter_complex "[1:v]hue=s=0[outv];overlay;aresample" \
       -an        out1.mp4 \
                  out2.mkv \
       -map 1:a:0 out3.mkv
```

上面的命令也會失敗，因為色調過濾器輸出有一個標籤 [out]，並且尚未映射到任何地方。

該命令應修改如下：

```
ffmpeg -i A.avi -i B.mp4 -i C.mkv -filter_complex "[1:v]hue=s=0,split=2[outv1][outv2];overlay;aresample" \
        -map '[outv1]' -an        out1.mp4 \
                                  out2.mkv \
        -map '[outv2]' -map 1:a:0 out3.mkv
```

來自 B.mp4 的視訊串流被傳送到色調過濾器，其輸出使用分割過濾器克隆一次，並且兩個輸出都被標記。 然後將每個副本映射到第一個和第三個輸出檔案。

覆蓋過濾器需要兩個視訊輸入，使用前兩個未使用的視訊串流。 這些是來自 A.avi 和 C.mkv 的流。 覆蓋輸出未標記，因此無論是否存在 -map 選項，它都會傳送到第一個輸出檔 out1.mp4。

aresample 過濾器被傳送到第一個未使用的音訊串流，即 A.avi 的音訊串流。 由於此過濾器輸出也未標記，因此它也對應到第一個輸出檔。 -an 的存在僅抑制音訊流的自動或手動流選擇，而不抑制從過濾器圖表發送的輸出。 這兩個映射流都應排在 out1.mp4 中的映射流之前。

映射到out2.mkv的視訊、音訊和字幕串流完全由自動串流選擇決定。

out3.mkv 由色調過濾器的克隆視訊輸出和 B.mp4 的第一個音訊串流組成。

# [5 Options](https://ffmpeg.org/ffmpeg.html#Options)

如果沒有另外指定，所有數字選項都接受表示數字的字串作為輸入，該字串後可能跟有 SI 單位前綴之一，例如：「K」、「M」或「G」。

如果將“i”附加到 SI 單位前綴，則完整的前綴將被解釋為二進制倍數的單位前綴，它基於 1024 的冪而不是 1000 的冪。將“B”附加到 SI 單位前綴將乘以值除以8。這允許使用例如：「KB」、「MiB」、「G」和「B」作為數字後綴。

不帶參數的選項是布林選項，並將相應的值設為true。 可以透過在選項名稱前加上“no”前綴將它們設為 false。 例如，使用“-nofoo”會將名稱為“foo”的布林選項設為 false。

## [5.1 Stream specifiers](https://ffmpeg.org/ffmpeg.html#Stream-specifiers-1)

有些選項適用於每個串流，例如 比特率或編解碼器。 流說明符用於精確指定給定選項所屬的流。

流說明符是一個字串，通常附加到選項名稱並用冒號分隔。 例如。 -codec:a:1 ac3 包含 a:1 流說明符，它與第二個音訊流相符。 因此，它將為第二個音訊串流選擇 ac3 編解碼器。

流說明符可以匹配多個流，以便該選項應用於所有流。 例如。 -b:a 128k 中的串流說明符符合所有音訊串流。

空流說明符符合所有流。 例如，-codec copy 或 -codec: copy 將複製所有流而不重新編碼。

流說明符的可能形式是：

* **stream\_index** 將流與該索引進行比對。 例如。 -threads:1 4 會將第二個流的執行緒計數設為 4。如果使用stream_index 作為附加流說明符（見下文），則它會從匹配的流中選擇流編號stream_index。 流編號是基於 libavformat 偵測到的流順序，除非也指定了節目 ID。 在這種情況下，它是基於程式中流的順序。
* **stream\_type\[:additional\_stream\_specifier\]** Stream\_type 是以下之一：“v”或“V”表示視頻，“a”表示音頻，“s”表示字幕，“d”表示數據，“t”表示附件。 「v」符合所有影片串流，「V」僅符合不附加圖片、影片縮圖或封面藝術的影片串流。 如果使用additional\_stream\_specifier，則它會符合既具有此類型又符合additional\_stream\_specifier的流。 否則，它會匹配指定類型的所有流。
* **p:program\_id\[:additional\_stream\_specifier\]** 匹配程式中 ID 為 program\_id 的流。 如果使用了additional\_stream\_specifier，則它符合既是程式的一部分又與 additional\_stream\_specifier 相符的流。
* **stream\_id or i:stream\_id** 透過流 ID 匹配流（例如 MPEG-TS 容器中的 PID）。
* **m:key\[:value\]** 將流與具有指定值的元資料標籤鍵相符。 如果未給出值，則將包含給定標記的流與任何值相符。
* **u** 將串流與可用配置相匹配，必須定義編解碼器，並且必須提供視訊尺寸或音訊取樣率等基本資訊。請注意，在 ffmpeg 中，元資料匹配僅適用於輸入檔案。

## [5.2 Generic options](https://ffmpeg.org/ffmpeg.html#Generic-options)

這些選項在 ff\* 工具之間共用