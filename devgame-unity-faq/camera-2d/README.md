# 2d 攝像機

2d遊戲中 通常使用 Orthographic(正交) 攝像機，去 Size 屬性確定了屏幕內多少內容會繪製到攝像機

Size 使用 unity 單位，它表示攝像機在垂直軸上會呈現多少個單位

例如size爲5，我們的視口將在垂直方向上呈現 5\*2=10 個 unity 單位，寬度則由屏幕寬高比確定。例如常見PC或Android手機的寬高比爲 16:9，則 10/9\*16 = 17.7 個單位的寬將被呈現。如果豎直放置手機寬高比爲 9:16，則 10/16\*9=5.6個寬度單位被呈現

# unity 單位

unity單位既 Pixels per Unit(PPU)，是 unity 抽象出的單位，默認 1 PPU = 100 Pixels

# ui 適配

通常在 unity 中創建的ui會放到一個 Canvas 之下，對於這個 Canvas Scaler 就是控制 ui 下的組件如何進行自適應

![](assets/Canvas.png)

* **UI Scale Mode** 設置爲 **Scale With Screen Size** 隨屏幕大小縮放
* **Reference Resolution** 設置爲設計分辨率
* **Screen Match Mode** 設置爲 **Match With Or Height** 匹配屏幕寬和高
* **Match** 則指定 寬高匹配比例，全選寬則橫向組件一定會全部顯示，全選高則豎向組件一定會全部顯示

# 攝像機範圍

攝像機範圍需要儘量將設計內容全部顯示出來，當分辨率不同時其Size應該是不同的值，可以通過創建一個 **CameraAdapter** 腳本來適配

1. 創建一個空節點(比如 Game)作爲所有遊戲元素的根節點，以便縮放等能影響所有遊戲元素
2. 爲 Game 添加上 CameraAdapter 腳本
3. 在 CameraAdapter 屬性上設置好適配方案以及要適配的攝像機等參數

```
#info="CameraAdapter.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Adapter
{
    // 適配高度 高度可以完整顯示 且不會在上下留下黑邊
    Height,
    // 適配寬度 寬度可以完整顯示 且不會在左右留下黑邊
    Width,
    // 比例誤差小於一定值時 縮放目標 否則適配高度，橫向遊戲建議使用此值
    // abs(ratio/screenRatio-1) < scale
    ScaleHeight,
    // 比例誤差小於一定值時 縮放目標 否則適配寬度，豎直遊戲建議使用此值
    // abs(ratio/screenRatio-1) < scale
    ScaleWidth,
}
public class Snapshot
{
    public Camera target;
    public int width;
    public int height;
    public Adapter adapter;
    public float scale;

    public int screenWidth;
    public int screenHeight;

    public void Save(Camera target, Adapter adapter,
        float width, float height,
        float screenWidth, float screenHeight,
        float scale)
    {
        this.target = target;
        this.adapter = adapter;
        this.width = (int)width;
        this.height = (int)height;
        this.screenWidth = (int)screenWidth;
        this.screenHeight = (int)screenHeight;
        this.scale = scale;
    }
    public bool IsNotChanged(Camera target, Adapter adapter,
        float width, float height,
        int screenWidth, int screenHeight,
        float scale)
    {
        return this.target == target && this.adapter == adapter
            && this.screenWidth == screenWidth && this.screenHeight == screenHeight
            && this.width == (int)width && this.height == (int)height
            && Mathf.Approximately(this.scale, scale);
    }
}
public class CameraAdapter : MonoBehaviour
{
    // 要適配的攝像機
    public Camera target;
    // 設計寬度
    public float width = 1080;
    // 設計高度
    public float height = 1920;
    // 適配模式
    public Adapter adapter = Adapter.ScaleWidth;
    // 縮放適配允許的臨界值
    public float scale = 0.2f;

    private Snapshot snapshot = new Snapshot();
    // 此函數用於適配攝像機必要時縮放遊戲根節點
    public void FitCamera()
    {
        if (snapshot.IsNotChanged(target, adapter, width, height, Screen.width, Screen.height, scale))
        {
            return;
        }

        // 計算攝像機 設計大小
        var orthographicSize = height / 100 / 2; // 1920/100/2=9.6

        // 計算寬高比
        var ratio = width / height;
        float screenWidth = Screen.width;
        float screenHeight = Screen.height;
        var screenRatio = screenWidth / screenHeight;

        transform.localScale = new Vector3(1, 1, 1); //恢復縮放，之前的適配策略可能破壞了此值

        if (Mathf.Approximately(ratio, screenRatio))
        {
            // 屏幕 寬/高 和設計相似 無需適配
            target.orthographicSize = orthographicSize;
            snapshot.Save(target, adapter, width, height, screenWidth, screenHeight, scale);
            return;
        }
        var mode = adapter;
        if (mode == Adapter.ScaleHeight)
        {
            if (Scale(ratio, screenRatio))
            {
                target.orthographicSize = orthographicSize;
                snapshot.Save(target, adapter, width, height, screenWidth, screenHeight, scale);
                return;
            }
            mode = Adapter.Height;
        }
        else if (mode == Adapter.ScaleWidth)
        {
            if (Scale(ratio, screenRatio))
            {
                target.orthographicSize = orthographicSize;
                snapshot.Save(target, adapter, width, height, screenWidth, screenHeight, scale);
                return;
            }
            mode = Adapter.Width;
        }

        if (mode == Adapter.Height)
        {
            // 適配高度 無需額外操作
            target.orthographicSize = orthographicSize;
            snapshot.Save(target, adapter, width, height, screenWidth, screenHeight, scale);

            // 如果屏幕比較寬 screenRatio > ratio，會在屏幕左右兩側留下黑邊
            // 如果屏幕比較窄 screenRatio < ratio，不會有黑邊但寬度顯示不全
            return;
        }

        // mode == Adapter.Height
        // 適配寬度 調整攝像機
        target.orthographicSize = orthographicSize * ratio / screenRatio;
        snapshot.Save(target, adapter, width, height, screenWidth, screenHeight, scale);

        // 如果屏幕比較寬 screenRatio > ratio，會縮小攝像機大小，所以不會有黑邊但高度顯示不全
        // 如果屏幕比較窄 screenRatio < ratio，會放大攝像機大小，會在屏幕上下留下黑邊
    }
    private void Awake()
    {
        FitCamera();
    }
    private void Update()
    {
        FitCamera();
    }
    private bool Scale(float ratio, float screenRatio)
    {
        var value = ratio / screenRatio - 1;
        if (value <= scale && value >= -scale)// 屏幕和設計誤差在允許範圍內 
        {
            // 水平拉伸或縮放
            transform.localScale = new Vector3(screenRatio / ratio, 1, 1);
            return true;
        }
        return false;
    }
}
```