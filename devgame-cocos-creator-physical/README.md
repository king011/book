# Cocos Creator 物理系統

creator 自帶了一個 簡易的 碰撞系統 和 box2d 物理引擎

* 官網 [http://www.cocos.com](http://www.cocos.com)
* 文檔 [http://docs.cocos.com/creator/manual/zh/](http://docs.cocos.com/creator/manual/zh/)
* API [http://docs.cocos.com/creator/api/zh/](http://docs.cocos.com/creator/api/zh/)
* 源碼 [https://github.com/cocos2d/cocos2d-x](https://github.com/cocos2d/cocos2d-x)