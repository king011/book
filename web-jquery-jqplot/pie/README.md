![](assets/pie.png)

```html
<!DOCTYPE html>
<html id="app">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>測試</title>

	<!-- 引入 js css -->

	<!-- 不需要兼容 sb的 ie9 以及以下版本 所以不 引入 excanvas -->
	<!-- <script src="jqplot/excanvas.js"></script> -->
	<script src="jquery-2.2.4.min.js"></script>
	<script src="jqplot/jquery.jqplot.min.js"></script>
	<script src="jqplot/plugins/jqplot.pieRenderer.js"></script>
	<script src="jqplot/plugins/jqplot.enhancedPieLegendRenderer.js"></script>
	<link href="jqplot/jquery.jqplot.min.css">
</head>

<body>
	<!-- 繪製目標 -->
	<div id="chartdiv" style="height:300px;width:400px; "></div>
</body>
<style>
	/* 爲 legend 定義 樣式 */

	table.jqplot-table-legend,
	table.jqplot-cursor-legend {
		background-color: rgba(255, 255, 255, 0.6);
		border: 1px solid #cccccc;
		position: absolute;
		font-size: 0.75em;
		cursor: pointer;
	}

	table.jqplot-table-legend {
		margin-top: 12px;
		margin-bottom: 12px;
		margin-left: 12px;
		margin-right: 12px;
	}

	div.jqplot-table-legend-swatch-outline {
		border: 1px solid #cccccc;
		padding: 1px;
	}

	div.jqplot-table-legend-swatch {
		width: 0px;
		height: 0px;
		border-top-width: 5px;
		border-bottom-width: 5px;
		border-left-width: 6px;
		border-right-width: 6px;
		border-top-style: solid;
		border-bottom-style: solid;
		border-left-style: solid;
		border-right-style: solid;
	}

	.jqplot-table-legend .jqplot-series-hidden {
		text-decoration: line-through;
	}
</style>
<script>
	$(document).ready(function () {
		toolTip1 = ['linux', 'sb mac', 'sb windows',];
		$.jqplot(
			'chartdiv', // 目標 id
			// 繪製 數據
			[[
				['linux', 10],
				['stupid mac', 1],
				['stupid windows', 1],
			]],
			{
				title: 'pie',
				seriesDefaults: {
					renderer: $.jqplot.PieRenderer, // 使用 餅圖
					rendererOptions: {
						padding: 8,
						showDataLabels: true, // 顯示 百分比
					},
				},
				// 顯示 顏色 說明
				legend: {
					show: true,
					location: 'e',//顯示 位置
					renderer: $.jqplot.EnhancedPieLegendRenderer,
					rendererOptions: {
						numberColumns: 1,
						toolTips: toolTip1
					},
				}
			},
		);
	});
</script>

</html>
```