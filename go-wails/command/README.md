# [init](https://wails.io/docs/gettingstarted/firstproject)

init 子命令用於創建一個項目， wails 提供了多種可選的前端模板

```
wails init -n myproject -t svelte-ts
```

創建好的項目佈局大概如下所示
```
.
├── build/
│   ├── appicon.png
│   ├── darwin/
│   └── windows/
├── frontend/
├── go.mod
├── go.sum
├── main.go
└── wails.json
```

* /main.go - 程序入口點
* /frontend/ - 前端項目
* /build/ - 編譯檔案夾
* /build/appicon.png - 程序圖標
* /build/darwin/ - 爲 Mac 平臺編譯的輸出檔案夾
* /build/windows/ - 爲 Windows 平臺編譯的輸出檔案夾
* /wails.json - 項目設定
* /go.mod - Go module file
* /go.sum - Go module checksum file

# [dev](https://wails.io/docs/gettingstarted/development)

你可以在開發時執行 dev 執行，這樣它將監控檔案的修改並生產 js 代碼同時它也會將前端運行起來以便你可以在瀏覽器中隨時看到前端的效果
```
wails dev

wails dev -devserver 0.0.0.0:34115
```

注意目前熱更新 go 代碼很慢，所以建議前後端分開開發，先將後端代碼完成再寫前端以避免熱更新過慢的問題

# [build](https://wails.io/docs/gettingstarted/building)

執行 build 可以編譯項目

```
wails build -ldflags "-s -w"

# 交叉編譯請加上 skipbindings 參數
wails build -platform "windows/amd64" -skipbindings
```

