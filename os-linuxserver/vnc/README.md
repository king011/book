# vnc
Virtual Network Computing 被設計來在 區域網絡中(因此缺乏安全性) 分享電腦熒幕 

1999年 被併入 美國電話電報公司（AT&T） 

AT&T 與 2002 年 終止此研究 並 以 GPL 開源 vnc

## 命令

vnc 一般會啓動 5901~5910 之間的 端口 當 X server 連接後 將 預先 設定好的 X client 傳給客戶端

```txt
# 啓動一個 vnc 
vncserver :1 -geometry 1024x768 -localhost
vncserver [:number] [-geometry 1024x768] [...]
	:number	指定要啓動 那個 vnc 服務
	-geometry	設置分辨率
	-localhost bind 到 本地
	
# 關閉 指定的 vnc 服務
vncserver -kill :1
vncserver -kill :number

# 修改 vnc 連接密碼
vncpasswd
```


## mint 安裝
```
#info=false
sudo apt install tightvncserver
```

```
#info="~/.vnc/xstartup"
#!/bin/sh
 
# Uncomment the following two lines for normal desktop:
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
#exec /etc/X11/xinit/xinitrc
 
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic &
x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
x-window-manager &
mate-session
```

# 注意
vncserver 是爲區域網路設計的 所以 vncserver沒有 加密 切驗證密碼 最長只有 8個字符

所以 務必加上 -localhost 參數 遠程訪問使用 vnc over ssh 


# gvncviewer

gvncviewer 是 ubunt 下的 一個 vnc 客戶端

```sh
sudo apt install gvncviewer
```

# x11vnc

x11vnc 是一個 vnc 服務器，它實時顯示現有的 x11 顯示器

1. 安裝 x11vnc

	```
	sudo apt install x11vnc
	```

1. 創建 x11vnc 使用下密碼檔案

	```
	x11vnc --storepasswd /etc/x11vnc/vncpwd
	```

1. 創建 vnc 服務

	```
	#info="x11vnc.service"
	[Unit]
	Description=Start x11vnc at startup.
	After=multi-user.target

	[Service]
	Type=simple
	ExecStart=/usr/bin/x11vnc -auth guess -forever -noxdamage -repeat -rfbauth /etc/x11vnc/vncpwd -rfbport 5900 -shared -listen localhost -listenv6 localhost

	[Install]
	WantedBy=multi-user.target
	```

1. 啓動 vnc 服務

	```
	sudo systemctl start x11vnc.service
	```