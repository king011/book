# Subject

同個 Observable 可以被多次 訂閱  每個訂閱 都是 獨立運行的

```
import { interval } from 'rxjs';

const observable = interval(1000)

observable.subscribe({
    next(v) {
        console.log("one", v)
    },
})
setTimeout(() => {
    observable.subscribe({
        next(v) {
            console.log("two", v)
        },
    })
}, 2000)
```

有時我們需要 新的訂閱 不要單獨運行 而是 繼續使用和當前 訂閱者 相同的 內容 這種工作模式稱爲組播 可以創建一個 中間人來訂閱 Observable 由中間人來 next 出數據 這個中間人就是  Subject

```
#info="手動創建 Subject"

import { interval, Observer, Observable } from 'rxjs';

// 自己實現一個簡易的 Subject 
class Subject<T>{
    private observers_ = new Array<Observer<T>>()
    constructor(private observable_: Observable<T>) {

    }
    next(value: T) {
        this.observers_.forEach(o => o.next(value))
    }
    error(e: any) {
        this.observers_.forEach(o => o.error(e))
    }
    complete() {
        this.observers_.forEach(o => o.complete())
    }
    subscribe(observer: Observer<T>) {
        this.observers_.push(observer)
        if (this.observers_.length == 1) {
            this._subscribe()
        }
    }
    private _subscribe() {
        this.observable_.subscribe(this)
    }
}

// 創建 subject
const subject = new Subject(interval(1000))

// 訂閱 subject
subject.subscribe({
    next(v: number) {
        console.log("one", v)
    },
    error(e) {
        console.log("one error :", e)
    },
    complete() {
        console.log("one complete")
    },
})

// 1s後 創建另外一個訂閱
setTimeout(() => {
    subject.subscribe({
        next(v) {
            console.log("two", v)
        },
        error(e) {
            console.log("two error :", e)
        },
        complete() {
            console.log("two error :")
        },
    })
}, 2000)
```

# rxjs Subject

* Subject 是 Observable 可以被訂閱 
* Subject 是Observer 可以去觀察其它的 Observable
* Subject 每當獲取到數據 其next 方法會對 內部的 Array&lt;Observer&gt; 進行組播

rxjs 也已經 提供了 Subject 的實現

```
import { interval, Subject } from 'rxjs';


// 創建 subject
const subject = new Subject<number>()

// 爲 subject 設置 數據源
interval(1000).subscribe(subject)

// 訂閱 subject
subject.subscribe({
    next(v: number) {
        console.log("one", v)
    },
    error(e) {
        console.log("one error :", e)
    },
    complete() {
        console.log("one complete")
    },
})

// 1s後 創建另外一個訂閱
setTimeout(() => {
    subject.subscribe({
        next(v) {
            console.log("two", v)
        },
        error(e) {
            console.log("two error :", e)
        },
        complete() {
            console.log("two error :")
        },
    })
}, 2000)
```

# BehaviorSubject

BehaviorSubject 代表當前狀態 BehaviorSubject會 緩存最後next的值 並當有新的訂閱者出現 會直接將 最新值 發送給最新的訂閱者

```
import { BehaviorSubject } from 'rxjs';


// 創建 subject
const subject = new BehaviorSubject<number>(0)
setTimeout(() => {
    subject.next(5)
}, 1000 * 5)

// 訂閱 subject
subject.subscribe({
    next(v: number) {
        console.log("one", v)
    },
    error(e) {
        console.log("one error :", e)
    },
    complete() {
        console.log("one complete")
    },
})

// 1s後 創建另外一個訂閱
setTimeout(() => {
    subject.subscribe({
        next(v) {
            console.log("two", v)
        },
        error(e) {
            console.log("two error :", e)
        },
        complete() {
            console.log("two error :")
        },
    })
}, 2000)
```

# ReplaySubject

ReplaySubject 類似 BehaviorSubject 但 ReplaySubject 可以指定緩存 最近的多個值 

```
import { ReplaySubject } from 'rxjs';


// 創建 subject
const subject = new ReplaySubject<number>(2)
subject.next(1)
subject.next(2)
subject.next(3)

setTimeout(() => {
    subject.next(5)
}, 1000 * 5)

// 訂閱 subject
subject.subscribe({
    next(v: number) {
        console.log("one", v)
    },
    error(e) {
        console.log("one error :", e)
    },
    complete() {
        console.log("one complete")
    },
})

// 1s後 創建另外一個訂閱
setTimeout(() => {
    subject.subscribe({
        next(v) {
            console.log("two", v)
        },
        error(e) {
            console.log("two error :", e)
        },
        complete() {
            console.log("two error :")
        },
    })
}, 2000)
```

ReplaySubject(1) 不等同 ReplaySubject ReplaySubject在創建時就 指定了初始值 ReplaySubject 需要next過數據後才存在緩存的值


# AsyncSubject

AsyncSubject 會在 complete 後 發送出最後的 值

```
import { AsyncSubject, interval } from 'rxjs'
import { take } from 'rxjs/operators'


// 創建 subject
const subject = new AsyncSubject<number>()
interval(100).pipe(take(20)).subscribe(subject)

// 訂閱 subject
subject.subscribe({
    next(v: number) {
        console.log("one", v)
    },
    error(e) {
        console.log("one error :", e)
    },
    complete() {
        console.log("one complete")
    },
})

// 1s後 創建另外一個訂閱
setTimeout(() => {
    subject.subscribe({
        next(v) {
            console.log("two", v)
        },
        error(e) {
            console.log("two error :", e)
        },
        complete() {
            console.log("two error :")
        },
    })
}, 1000)
```

後訂閱的 two 會在 next 後 調用 error 線訂閱的 one 會在 next 會 調用 complete ？ 不知道爲何如此 還好AsyncSubject基本用不到


# multicast

multicast 可以把 Observable 和 Subject 關聯返回一個 ConnectableObservable

所有 subscribe 此 ConnectableObservable 都會 subscribe 到 Subject

只有當 ConnectableObservable.connect 時 ConnectableObservable 才會訂閱 源Observable 獲取數據 必須調用 connect 返回subscription.unsubscribe 來退訂 源Observable

```
import { interval, Subject, ConnectableObservable } from 'rxjs'
import { multicast, map } from 'rxjs/operators'

const source = interval(1000).pipe(
    map((v) => {
        console.log("next", v)
        return v
    }),
    multicast(new Subject())
) as ConnectableObservable<number>

const subscriptionA = source.subscribe({
    next(v) {
        console.log(v)
    },
})
setTimeout(() => {
    // subscriptionA 已經退訂 但 依然會有數據發送 只是 不是發送給 subscriptionA
    subscriptionA.unsubscribe()
}, 1100)

// connect 才會開始 發送數據
const subscription = source.connect()
setTimeout(() => {
    // connect 返回的 subscription 退訂 才不會發送數據
    subscription.unsubscribe()
}, 2100)
```

# publish

publish() 是  multicast(new Subject()) 的語法糖

* publish
* publishReplay
* publishBehavior
* publishLast


下面代碼都等價

```
var source = Rx.Observable.interval(1000)
             .publish() 
             .refCount();
             
// var source = Rx.Observable.interval(1000)
//             .multicast(new Rx.Subject()) 
//             .refCount();
```

```
var source = Rx.Observable.interval(1000)
             .publishReplay(1) 
             .refCount();
             
// var source = Rx.Observable.interval(1000)
//             .multicast(new Rx.ReplaySubject(1)) 
//             .refCount();
```

```
var source = Rx.Observable.interval(1000)
             .publishBehavior(0) 
             .refCount();
             
// var source = Rx.Observable.interval(1000)
//             .multicast(new Rx.BehaviorSubject(0)) 
//             .refCount();
```

```
var source = Rx.Observable.interval(1000)
             .publishLast() 
             .refCount();
             
// var source = Rx.Observable.interval(1000)
//             .multicast(new Rx.AsyncSubject(1)) 
//             .refCount();
```

# refCount

refCount通常用來 配合 multicast 當第一個 subscribe 時 自動 connect 但所有 subscriptionunsubscribe() 後自動爲connect unsubscribe

```
import { interval } from 'rxjs'
import { publish, map, refCount } from 'rxjs/operators'

const source = interval(1000).pipe(
    map((v) => {
        console.log("next", v)
        return v
    }),
    publish(),
    refCount(),
)

source.subscribe({
    next(v) {
        console.log(v)
    },
})

setTimeout(() => {
    source.subscribe({
        next(v) {
            console.log(v)
        },
    })
}, 2100)
```

# share

share 是 multicast refCount 的語法糖

