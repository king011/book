# 連接

服務器 可以配置 一個密碼 默認爲 空，設置密碼後 客戶端 需要 使用 AUTH 驗證密碼

```
CONFIG get requirepass
```

```
CONFIG set requirepass "cerberus"
```

```
AUTH "cerberus"
```


| 命令 | 含義 |
| -------- | -------- |
| AUTH password      | 驗證密碼     |
| ECHO message      | 打印字符串     |
| PING      | 檢查服務器 是否連通     |
| QUIT      | 關閉當前連接     |
| SELECT index      | 更改當前連接所選數據庫     |

