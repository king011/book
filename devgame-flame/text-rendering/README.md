# [Text Rendering](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html)

Flame 有一些專門的類別來幫助您渲染文字

# [Text Components](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#text-components)


使用 Flame 渲染文字最簡單的方法是利用提供的文字渲染元件之一：

* TextComponent 用於渲染單行文本
* TextBoxComponent 用於在一定大小的框中限制多行文本，包括打字效果的可能性
* ScrollTextBoxComponent 透過在文字超出封閉框的邊界時加入捲動功能來增強 TextBoxComponent 的功能

使用 onFinished 回調在文字完全列印時收到通知。

[此範例](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/text_example.dart)中展示了所有組件。


## [TextComponent](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#textcomponent)

TextComponent 是一個呈現單行文字的簡單元件。

簡單用法：

```
class MyGame extends FlameGame {
  @override
  void onLoad() {
    add(
      TextComponent(
        text: 'Hello, Flame',
        position: Vector2.all(16.0),
      ),
    );
  }
}
```

為了配置渲染的各個方面，如字體系列、大小、顏色等，您需要提供（或修改）帶有此類資訊的 TextRenderer； 雖然您可以在下面閱讀有關此介面的更多詳細信息，但您可以使用的最簡單的實作是 TextPaint，它採用 Flutter TextStyle：

```
final regular = TextPaint(
  style: TextStyle(
    fontSize: 48.0,
    color: BasicPalette.white.color,
  ),
);

class MyGame extends FlameGame {
  @override
  void onLoad() {
    add(
      TextComponent(
        text: 'Hello, Flame',
        textRenderer: regular,
        anchor: Anchor.topCenter,
        position: Vector2(size.width / 2, 32.0),
      ),
    );
  }
}
```

您可以在 TextComponent 的 [API](https://pub.dev/documentation/flame/latest/components/TextComponent-class.html) 下找到所有選項。

## [TextBoxComponent](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#textboxcomponent)

TextBoxComponent 與 TextComponent 非常相似，但顧名思義，它用於在邊界框內渲染文本，根據提供的框大小創建換行符。

您可以透過 TextBoxConfig 中的變數變數來決定該方塊是否應該隨著文字的寫入而成長，或者是否應該保持靜態。 靜態框可以具有固定大小（設定 TextBoxComponent 的大小屬性），也可以自動縮小以適合文字內容。

此外，align 屬性可讓您控製文字內容的水平和垂直對齊方式。 例如，將align 設為Anchor.center 將使文字在其邊界框內垂直和水平居中。

如果要變更框的邊距，請使用 TextBoxConfig 中的邊距變數。

最後，如果你想模擬「打字」效果，將字串中的每個字元一一顯示出來，就像即時打字一樣，你可以提供 boxConfig.timePerChar 參數。

用法範例：

```
class MyTextBox extends TextBoxComponent {
  MyTextBox(String text) : super(
    text: text,
    textRenderer: tiny,
    boxConfig: TextBoxConfig(timePerChar: 0.05),
  );

  final bgPaint = Paint()..color = Color(0xFFFF00FF);
  final borderPaint = Paint()..color = Color(0xFF000000)..style = PaintingStyle.stroke;

  @override
  void render(Canvas canvas) {
    Rect rect = Rect.fromLTWH(0, 0, width, height);
    canvas.drawRect(rect, bgPaint);
    canvas.drawRect(rect.deflate(boxConfig.margin), borderPaint);
    super.render(canvas);
  }
}
```

您可以在 TextBoxComponent 的 [API](https://pub.dev/documentation/flame/latest/components/TextBoxComponent-class.html) 下找到所有選項。


## [ScrollTextBoxComponent](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#scrolltextboxcomponent)

ScrollTextBoxComponent 是 TextBoxComponent 的進階版本，設計用於在定義的區域內顯示可捲動文字。 此元件對於建立需要在有限空間中呈現大量文字的介面（例如對話或資訊面板）特別有用。

請注意，TextBoxComponent 的 align 屬性不可用

用法範例：

```
class MyScrollableText extends ScrollTextBoxComponent {
  MyScrollableText(Vector2 frameSize, String text) : super(
    size: frameSize,
    text: text,
    textRenderer: regular, 
    boxConfig: TextBoxConfig(timePerChar: 0.05),
  );
}
```

## [TextElementComponent](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#textelementcomponent)

如果要呈現任何 TextElement（範圍從單一 InlineTextElement 到格式化的 DocumentRoot），您可以使用 TextElementComponent。

一個簡單的範例是建立一個 DocumentRoot 來呈現一系列包含富文本的區塊元素（想像一下 HTML「div」）：

```
  final document = DocumentRoot([
    HeaderNode.simple('1984', level: 1),
    ParagraphNode.simple(
      'Anything could be true. The so-called laws of nature were nonsense.',
    ),
    // ...
  ]);
  final element = TextElementComponent.fromDocument(
    document: document,
    position: Vector2(100, 50),
    size: Vector2(400, 200),
  );
```

請注意，可以透過兩種方式指定大小； 可以通過：

* 所有 PositionComponent 共有的 size 屬性； 或者
* 應用的 DocumentStyle 中包含的寬度/高度。

將樣式套用至文件的範例（可以包含大小，但也可以包含其他參數）：

```
  final style = DocumentStyle(
    width: 400,
    height: 200,
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 14),
    background: BackgroundStyle(
      color: const Color(0xFF4E322E),
      borderColor: const Color(0xFF000000),
      borderWidth: 2.0,
    ),
  );
  final document = DocumentRoot([ ... ]);
  final element = TextElementComponent.fromDocument(
    document: document,
    style: style,
    position: Vector2(100, 50),
  );
```

有關富文本、格式化文字區塊渲染的更詳細範例，請查看此[範例](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/rich_text_example.dart)。

有關文字渲染管道的底層機制的更多詳細信息，請參閱下面的「文字元素、文字節點和文字樣式」。

## [Flame Markdown](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#flame-markdown)

為了更輕鬆地創建基於富文本的 DocumentRoot，從簡單的粗體/斜體字串到完整的結構化文檔，Flame 提供了 Flame_markdown 橋接包，將 Markdown 庫與 Flame 的文本渲染基礎設施連接起來。

只需使用 FlameMarkdown 幫助程式類別和 toDocument 方法將 Markdown 字串轉換為 DocumentRoot（然後可用於建立 TextElementComponent）：

```
import 'package:flame/text.dart';
import 'package:flame_markdown/flame_markdown.dart';

// ...
final component = await TextElementComponent.fromDocument(
  document: FlameMarkdown.toDocument(
    '# Header\n'
    '\n'
    'This is a **bold** text, and this is *italic*.\n'
    '\n'
    'This is a second paragraph.\n',
  ),
  style: ...,
  position: ...,
  size: ...,
);
```

# [Infrastructure](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#infrastructure)

如果您不使用 Flame 元件系統，想要了解文字渲染背後的基礎設施，想要自訂使用的字體和樣式，或想要建立自己的自訂渲染器，本節適合您。

* **TextRenderer**：渲染器知道「如何」渲染文字； 本質上它們包含渲染任何字串的樣式信息
* **TextElement**：格式化的元素，「佈局」的文字片段，包括字串（「什麼」）和樣式（「如何」）

下圖展示了文字渲染管道的類別和繼承結構：

![](assets/Infrastructure.png)

## [TextRenderer](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#textrenderer)

TextRenderer是Flame用來渲染文字的抽象類別。 TextRenderer 的實作必須包含有關文字「如何」呈現的資訊。 字體樣式、大小、顏色等。它應該能夠透過 format 方法將該資訊與給定的文字字串組合起來，以產生 TextElement。

Flame提供了兩種具體的實作：

* **TextPaint**：用得最多，使用Flutter TextPainter渲染常規文本
* **SpriteFontRenderer**：使用 SpriteFont（基於精靈表的字體）來渲染點陣圖文本
* **DebugTextRenderer**：僅用於 Golden Tests

但如果您想擴展到其他自訂形式的文字渲染，您也可以提供自己的。

TextRenderer 的主要工作是將文字字串格式化為 TextElement，然後可以將其渲染到螢幕上：

```
final textElement = textRenderer.format("Flame is awesome")
textElement.render(...) 
```

然而，渲染器提供了一個輔助方法來直接建立元素並渲染它：

```
textRenderer.render(
  canvas,
  'Flame is awesome',
  Vector2(10, 10),
  anchor: Anchor.topCenter,
);
```

## [TextPaint](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#textpaint)

TextPaint 是Flame 中文字渲染的內建實作。 它基於 Flutter 的 TextPainter 類別（因此得名）之上，可以透過樣式類別 TextStyle 進行配置，該類別包含渲染文字所需的所有印刷資訊； 即字體大小和顏色、字體系列等。

在樣式之外，您還可以選擇提供一個額外參數，即 textDirection（但通常已設定為 ltr 或 left-to-right）。

用法範例：

```
const TextPaint textPaint = TextPaint(
  style: TextStyle(
    fontSize: 48.0,
    fontFamily: 'Awesome Font',
  ),
);
```

注意：有幾個套件包含 TextStyle 類別。 我們透過 text 模組導出正確的內容（從 Flutter 中）：

```
import 'package:flame/text.dart';
```

但如果您想明確導入它，請確保從 package:flutter/painting.dart （或從材質或小部件）導入它。 如果您還需要匯入 dart:ui，則可能需要隱藏其 TextStyle 版本，因為該模組包含具有相同名稱的不同類別：

```
import 'package:flutter/painting.dart';
import 'dart:ui' hide TextStyle;
```

TextStyle 的一些常見屬性如下（這是[完整列表](https://api.flutter.dev/flutter/painting/TextStyle-class.html)）：

* **fontFamily**：常用字體，如 Arial（預設），或在 pubspec 中新增的自訂字體（請參閱此處如何操作）。
* **fontSize**：字體大小，以磅為單位（預設為24.0）。
* **height**：文字行的高度，為字體大小的倍數（預設為空）。
* **color**：顏色，作為 ui.Color （預設為白色）。

有關顏色以及如何創建的更多信息，請參閱 [Colors and the Palette](https://docs.flame-engine.org/1.16.0/flame/rendering/palette.html) 指南。

## [SpriteFontRenderer](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#spritefontrenderer)

開箱即用的另一個渲染器選項是 SpriteFontRenderer，它允許您提供基於精靈表的 SpriteFont

## [DebugTextRenderer](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#debugtextrenderer)

該渲染器旨在用於 Golden Tests。 由於跨平台字體定義的差異以及用於抗鋸齒的演算法不同，在 Golden Tests 中渲染基於正常字體的文字是不可靠的。 該渲染器將渲染文本，就好像每個單字都是一個實心矩形一樣，從而可以測試元素的佈局、位置和大小，而無需依賴基於字體的渲染。

# [Inline Text Elements](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#inline-text-elements)

TextElement 是「預先編譯」、格式化和佈局的文本，套用了特定樣式，可以在任何給定位置呈現。

InlineTextElement 實作 TextElement 接口，並且必須實作它們的兩個方法，一個方法是如何將其平移，另一個方法是如何將其繪製到畫布上：

```
  void translate(double dx, double dy);
  void draw(Canvas canvas);
```

這些方法旨在被 InlineTextElement 的實作所覆蓋，並且可能不會被使用者直接呼叫； 因為提供了方便的渲染方法：

```
  void render(
    Canvas canvas,
    Vector2 position, {
    Anchor anchor = Anchor.topLeft,
  })
```

這允許使用給定的錨點將元素呈現在特定位置。

該介面還要求（並提供）與該 InlineTextElement 關聯的 LineMetrics 物件的 getter，它允許您（和渲染實現）存取與元素相關的大小資訊（寬度、高度、上升等）。

```
  LineMetrics get metrics;
```

# [Text Elements, Text Nodes, and Text Styles](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#text-elements-text-nodes-and-text-styles)

雖然普通渲染器總是直接使用 InlineTextElement，但有一個更大的底層基礎設施可用於渲染更豐富的文字或格式化文字。

文字元素是內嵌文字元素的超集，它表示富文本文件中的任意渲染區塊。 本質上，它們是具體的和「物理的」：它們是準備在畫布上渲染的物件。

此屬性將它們與文字節點（文字節點）和文字樣式（在程式碼中稱為FlameTextStyle，以便更容易與Flutter 的TextStyle 一起工作）區分開來，文字節點是結構化文字片段，文字樣式如何渲染任意文字片段的描述符。

因此，在最一般的情況下，使用者會使用 TextNode 來描述所需的富文本； 定義一個 FlameTextStyle 來應用到它； 並使用它來產生 TextElement。 根據渲染類型，產生的 TextElement 將是 InlineTextElement，這使我們回到渲染管道的正常流程。 Inline-Text-type 元素的獨特屬性是它公開了可用於高級渲染的 LineMetrics； 而其他元素僅公開一個更簡單的繪製方法，該方法不知道大小和位置。

但是，如果目的是建立一個包含格式化文字的整個文件（多個區塊或段落），則必須使用其他類型的文字元素、文字節點和文字樣式。 為了呈現任何 TextElement，您也可以使用 TextElementComponent（請參閱上文）。

在此[範例](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/rich_text_example.dart)中可以看到此類用法的範例。

## [Text Nodes and the Document Root](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#text-nodes-and-the-document-root)

DocumentRoot 本身不是 TextNode（繼承方式），而是代表一組 BlockNode，這些 BlockNode 佈局在多個區塊或段落中佈局的富文本「頁面」或「文件」。 它代表整個文件並且可以接收全域樣式。

定義富文本文件的第一步是建立一個節點，它可能是一個 DocumentRoot。

它將首先包含可以定義標題、段落或列的區塊節點的最頂層清單。

然後，每個區塊都可以包含其他區塊或內聯文字節點，可以是純文字節點，也可以是一些具有特定格式的富文本。

請注意，節點結構定義的層次結構也用於依照 FlameTextStyle 類別中定義的樣式目的。

實際的節點都繼承自TextNode，如下圖所示：

![](assets/text.png)


## [(Flame) Text Styles](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#flame-text-styles)

文字樣式可以套用於節點以產生元素。 它們都繼承自 FlameTextStyle 抽象類別（命名是為了避免與 Flutter 的 TextStyle 混淆）。

它們遵循樹狀結構，始終以 DocumentStyle 作為根； 利用此結構將級聯樣式套用至類似的節點結構。 事實上，它們與 CSS 定義非常相似，並且可以被認為是 CSS 定義。

完整的繼承鏈如下圖所示：

![](assets/style.png)


## [Text Elements](https://docs.flame-engine.org/1.16.0/flame/rendering/text_rendering.html#text-elements)

最後，我們有元素，它們表示節點（「什麼」）與樣式（「如何」）的組合，因此表示要在畫布上渲染的預編譯、佈局的富文本片段。

Inline Text Elements 具體可以被視為 TextRenderer（簡化的“how”）和字串（單行“what”）的組合。

這是因為 InlineTextStyle 可以透過 asTextRenderer 方法轉換為特定的 TextRenderer，然後使用該方法將每行文字佈局到唯一的 InlineTextElement 中。

直接使用渲染器時，會跳過整個佈局過程，並傳回單一 TextPainterTextElement 或 SpriteFontTextElement。

正如您所看到的，從所有方面考慮，元素的兩個定義本質上是等效的。 但它仍然給我們留下了兩條渲染文本的路徑。 該選哪一個呢？ 如何解決這個難題？

如有疑問，以下指南可以幫助您選擇最適合您的路徑：

* 對於渲染文字的最簡單方法，請使用 TextPaint （基本渲染器實作）
	*  您可以使用 FCS 提供的元件 TextComponent 來實現此目的。
* 為了渲染 Sprite Fonts，您必須使用 SpriteFontRenderer（接受 SpriteFont 的渲染器實作）； 
* 要渲染多行文字並使用自動換行符，您有兩種選擇：
	* 使用FCS TextBoxComponent，它使用任何文字渲染器將每一行文字繪製為一個Element，並進行自己的佈局和換行；
	* 使用文字節點和樣式系統建立預先佈局元素。 注意：目前沒有適用於它的 FCS 組件。
* 最後，為了獲得格式化（或富）文本，您必須使用 Text Nodes 和 Styles。
