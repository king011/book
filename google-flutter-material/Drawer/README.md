# Drawer

![](assets/Drawer.gif)

[Drawer](https://docs.flutter.io/flutter/material/Drawer-class.html) 是一個 抽屜 通常 設置到 Scaffold.drawer 中


```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: Center(
        child: Text(S.of(context).idae("cerberus")),
      ),
      drawer: Drawer(
        // Drawer 通常是用 ListView 顯示 的 功能導航
        child: ListView(
          children: <Widget>[
            SizedBox(
              // 藉助 SizedBox 來調整 DrawerHeader 高度
              height: 150,
              // 通常以 DrawerHeader 開始 顯示 用戶信息
              child: DrawerHeader(
                margin: EdgeInsets.only(top: 16, left: 16),
                padding: EdgeInsets.only(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      "assets/cerberus_tux.jpg",
                      width: 80,
                      height: 80,
                    ),
                    Text("zuiwuchang"),
                    Text("zuiwuchang@gmail.com"),
                  ],
                ),
              ),
            ),

            // 功能導航
            ListTile(
              dense: true, // 密集 排列
              leading: Icon(Icons.group), // 前置圖標
              title: Text('分享對象'), // 標題
              subtitle: Text('副標題'),
              trailing: Icon(Icons.group), //  後置圖標
              onTap: () => debugPrint("分享對象"),
              onLongPress: () => debugPrint("分享對象 LongPress"),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.event),
              title: Text('活動'),
              onTap: () => debugPrint("活動"),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.settings),
              title: Text('設定'),
              onTap: () => debugPrint("設定"),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.lock),
              title: Text('隱私設定'),
              onTap: () => debugPrint("隱私設定"),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.sms_failed),
              title: Text('提供意見'),
              onTap: () => Navigator.pop(context), //調用 pop 關閉 抽屜
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.help),
              title: Text('說明'),
              onTap: () => debugPrint("說明"),
            ),
            Divider(),

            // 通常以 AboutListTile 結束 顯示 app 關於 信息
            AboutListTile(
              applicationVersion: "v1.0.0",
              child: Text("關於"),
            ),
          ],
        ),
      ),
    );
  }
}
```