# goja

goja 使用純golang實現的 開源(MIT) ECMAScript 5.1

* 源碼 [https://github.com/dop251/goja](https://github.com/dop251/goja)

```bash
go get -u -v github.com/dop251/goja
```


```
package main

import (
	"fmt"
	"log"
	"time"

	"github.com/dop251/goja"
)

func main() {
	vm := goja.New()
	last := time.Now()
	vm.Set(`println`, func(args ...interface{}) {
		fmt.Println(args...)
	})
	v, e := vm.RunString(`function fibonacci( n ){
	if (n < 2){
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}
println(fibonacci(31))
`)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(time.Now().Sub(last), v)

}
```