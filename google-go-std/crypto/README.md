# [crypto](https://pkg.go.dev/crypto)

crypto 下的 子包 提供了 常用的 加密算法的 實現

# 摘要算法

```go
package main

import (
	"crypto/sha512"
	"encoding/hex"
	"fmt"
)

func main() {
	b := sha512.Sum512([]byte("any byte"))
	str := hex.EncodeToString(b[:])

	sha := sha512.New()
	sha.Write([]byte("any byte"))
	fmt.Println(str == hex.EncodeToString(sha.Sum(nil)))

	sha.Reset()
	sha.Write([]byte("new hash"))
	fmt.Println(hex.EncodeToString(sha.Sum(nil)))

}
```

# 對稱加密
```go
package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

func Md5Byte(source []byte) []byte {
	b := md5.Sum(source)
	return b[:]
}
func Md5(str string) string {
	b := md5.Sum([]byte(str))
	return hex.EncodeToString(b[:])
}

func Encrypt(key, iv, source string) (string, error) {
	//產生 加密工廠
	block, err := aes.NewCipher([]byte(Md5(key)))
	if err != nil {
		return "", err
	}

	//使用 cfb nopadding
	stream := cipher.NewCFBEncrypter(block, Md5Byte([]byte(iv)))
	plaintext := []byte(source)
	ciphertext := make([]byte, len(plaintext))
	stream.XORKeyStream(ciphertext, plaintext)

	return hex.EncodeToString(ciphertext), nil
}
func Decrypt(key, iv, source string) (string, error) {
	ciphertext, err := hex.DecodeString(source)
	if err != nil {
		return "", err
	}
	//產生 加密工廠
	block, err := aes.NewCipher([]byte(Md5(key)))
	if err != nil {
		return "", err
	}

	//使用 cfb nopadding
	stream := cipher.NewCFBDecrypter(block, Md5Byte([]byte(iv)))
	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)
	return string(ciphertext), nil
}

func main() {
	key := "12345678901"
	iv := "this is iv"
	str := "123草7890123草"

	fmt.Println(key, iv, str)
	enc, err := Encrypt(key, iv, str)
	fmt.Println(enc, err)
	fmt.Println(Decrypt(key, iv, enc))
}
```

# 非對稱加密

crypto/rsa 包實現了 非對稱加密

非對稱加密 的關鍵在於 PublicKey PrivateKey

* 用 PublicKey 加密的數據 只有 PrivateKey 可解
* 用 PrivateKey 加密的數據 只有 PublicKey 可解

由於這個 特性 可以 達到 安全的 數據交互 如果有 A B 要 通信 且 G 是一個 權威 擁有 A B 的 公鑰

1. A B 互傳自己的 PublicKey
2. A B 向 G 驗證 PublicKey 是否確實是 對方的
3. 傳輸數據時 用對方的 PublicKey 加密傳輸數據 用自己的 PrivateKey 簽名數據
4. 接收數據時 用 對方的 PublicKey 驗證簽名是否是 對方 用 自己的 PrivateKey 解密傳給自己的 數據

> G 至關重要 如果沒有 G 則 C 可以在 A B 互傳 公鑰時 傳輸自己的公鑰 給 A B 達到中間人攻擊的目的
> 

GenerateKey 函數 需要一個 隨機數 產生器 來創建一個 PrivateKey

```go
package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	save()
	load()
}
func load() {
	// 加載 公鑰
	_, e := loadPublicKey("rsa.pub")
	if e != nil {
		log.Fatalln(e)
	}

	// 加載 私鑰
	_, e = loadPrivateKey("rsa.pri")
	if e != nil {
		log.Fatalln(e)
	}
}
func save() {
	reader := rand.Reader
	bitSize := 2048

	// 創建 密鑰
	privateKey, e := rsa.GenerateKey(reader, bitSize)
	if e != nil {
		log.Fatalln(e)
	}

	// 保存 公鑰
	e = savePublicKey("rsa.pub", &privateKey.PublicKey)
	if e != nil {
		log.Fatalln(e)
	}

	// 保存 私鑰
	e = savePrivateKey("rsa.pri", privateKey)
	if e != nil {
		log.Fatalln(e)
	}
}

func savePrivateKey(filename string, privateKey *rsa.PrivateKey) (e error) {
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		return
	}
	defer f.Close()

	block := &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
	}

	e = pem.Encode(f, block)
	return
}
func savePublicKey(filename string, publicKey *rsa.PublicKey) (e error) {
	var f *os.File
	f, e = os.Create(filename)
	if e != nil {
		return
	}
	defer f.Close()

	block := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: x509.MarshalPKCS1PublicKey(publicKey),
	}

	e = pem.Encode(f, block)
	return
}
func loadPrivateKey(filename string) (privateKey *rsa.PrivateKey, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	block, _ := pem.Decode(b)
	privateKey, e = x509.ParsePKCS1PrivateKey(block.Bytes)
	return
}

func loadPublicKey(filename string) (publicKey *rsa.PublicKey, e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	block, _ := pem.Decode(b)
	publicKey, e = x509.ParsePKCS1PublicKey(block.Bytes)
	return
}
```

## 簽名

* SignXXX 函數 提供了 簽名功能
* VerifyXXX 函數 提供了對 簽名驗證的 功能

```go
package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"log"
)

func main() {
	// 創建 密鑰
	bitSize := 2048
	privateKey, e := rsa.GenerateKey(rand.Reader, bitSize)
	if e != nil {
		log.Fatalln(e)
	}
	message := []byte("cerberus is an idea")

	// 爲消息 創建 hash
	hashed := sha256.Sum256(message)

	// 爲 hash 簽名
	sig, e := rsa.SignPKCS1v15(
		nil, // 這個參數是歷史遺留，它被忽略
		privateKey,
		crypto.SHA256, hashed[:])
	if e != nil {
		log.Fatalf("Error from signing: %s\n", e)
		return
	}

	// 驗證 簽名
	e = rsa.VerifyPKCS1v15(&privateKey.PublicKey,
		crypto.SHA256, hashed[:],
		sig)
	if e != nil {
		log.Fatalf("Error from verification: %s\n", e)
		return
	}
}
```

## 加密解密

* EncryptXXX 提供了加密
* DecryptXXX 提供了解密

```go
package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"fmt"
	"log"
)

func main() {
	reader := rand.Reader
	bitSize := 2048

	// 創建 密鑰
	privateKey, e := rsa.GenerateKey(reader, bitSize)
	if e != nil {
		log.Fatalln(e)
	}
	message := []byte("cerberus is an idea")

	// 加密
	var b []byte
	b, e = rsa.EncryptOAEP(sha256.New(), reader, &privateKey.PublicKey, message, nil)
	if e != nil {
		log.Fatalf("Error from encryption: %s\n", e)
		return
	}

	// 解密
	b, e = rsa.DecryptOAEP(sha256.New(), reader, privateKey, b, nil)
	if e != nil {
		log.Fatalf("Error from encryption: %s\n", e)
		return
	}

	// 驗證 解密 成功
	fmt.Println(string(b) == string(message))
}
```

## 持久化密鑰

crypto/x509 包中的

* **MarshalPKCS1PrivateKey/MarshalPKCS1PublicKey** 函數用於，序列化 rsa 密鑰
* **ParsePKCS1PrivateKey/ParsePKCS1PublicKey** 函數用於，反序列 rsa 密鑰