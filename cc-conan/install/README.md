# 安裝

1. 安裝好 python

	```
	sudo apt install python3 python3-pip -y
	```

2. 使用 pip 安裝 conan

	```
	sudo pip install conan
	```
	
3. 更新 conan

	```
	sudo pip install conan --upgrade
	```
	
# 常用命令

```
# 查看包元信息
conan inspect poco/1.9.4

# 安裝包依賴 並將腳本輸出到 build 檔案夾
conan install . -if=build

# 檢測緩存中已經安裝的包
conan search "*"

# 將包依賴生成 html 視圖
conan info . --graph=document/dependencies.html

# 打印項目依賴信息
conan info .
```