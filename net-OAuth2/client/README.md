# 客戶端

下面以 golang 代碼來實現一個 OAuth2 的客戶端，服務提供商選用 github

實現在 github 上註冊一個 客戶端 [https://github.com/settings/applications/new](https://github.com/settings/applications/new)

![](assets/new.png)

註冊超過後記錄下 Client ID 與 Client secrets

```
package main

const (
	authorizeURI  = `https://github.com/login/oauth/authorize`
	grantURI      = `https://github.com/login/oauth/access_token`
	redirectURI   = `http://dev.my.web/oauth/redirect`
	clientID      = `xxx`
	clientSecrets = `xxx`
)

```

*  github OAuth 文檔 [https://docs.github.com/en/developers/apps/building-oauth-apps/authorizing-oauth-apps](https://docs.github.com/en/developers/apps/building-oauth-apps/authorizing-oauth-apps)

# 步驟 A

將用戶重定向到認證服務器

```go
func home(w http.ResponseWriter, req *http.Request) {
	w.Header().Set(`Content-Type`, `text/html; charset=utf-8`)

	vals := url.Values{
		`response_type`: {`code`},
		`client_id`:     {clientID},
		`redirect_uri`:  {redirectURI},
	}
	href := authorizeURI + "?" + vals.Encode()
	fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<title>登入</title>
<body>
	<a href="%s">
	Login with github
	</a>
</body>

</html>`, href)
}
```

用戶點擊頁面的 Login with github 後將被導向 github 的授權服務器

# 步驟 C D E

當用戶授權後 github 的服務器將重定向到 redirectURI 下面來實現 redirectURI 並在裏面向 github 請求訪問令牌

```
func callbackURL(w http.ResponseWriter, req *http.Request) {
	token, statusCode, e := grant(req.URL.Query().Get(`code`))
	if e != nil {
		w.WriteHeader(statusCode)
		w.Write([]byte(e.Error()))
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:  `token`,
		Value: token.TokenType + ` ` + token.AccessToken,
		Path:  `/`,
	})
	http.Redirect(w, req, `/user`, http.StatusFound)
}

type GrantResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	Error       string `json:"error"`
}

func grant(code string) (result *GrantResponse, statusCode int, e error) {
	statusCode = http.StatusInternalServerError
	vals := url.Values{
		`response_type`: {`code`},
		`code`:          {code},
		`redirect_uri`:  {redirectURI},
		`client_id`:     {clientID},
		`client_secret`: {clientSecrets}, // github 要求的密鑰
	}
	req, e := http.NewRequest(http.MethodPost, grantURI, strings.NewReader(vals.Encode()))
	if e != nil {
		return
	}
	header := req.Header
	header.Set(`accept`, `application/json`)
	header.Set(`Content-Type`, `application/x-www-form-urlencoded`)
	resp, e := http.DefaultClient.Do(req)
	if e != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		statusCode = resp.StatusCode
		e = errors.New(resp.Status)
		return
	}
	decoder := json.NewDecoder(resp.Body)
	var tmp GrantResponse
	e = decoder.Decode(&tmp)
	if e != nil {
		return
	} else if tmp.Error != `` {
		e = errors.New(tmp.Error)
		return
	}
	result = &tmp
	statusCode = http.StatusOK
	return
}
```

# 訪問資源

在獲取到 訪問令牌後就可以依據服務商提供的 api 訪問受保護的資源

```
func user(w http.ResponseWriter, req *http.Request) {
	token, e := req.Cookie(`token`)
	if e != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(e.Error()))
		return
	}
	username, statusCode, e := requestUser(token.Value)
	if e != nil {
		w.WriteHeader(statusCode)
		w.Write([]byte(e.Error()))
		return
	}
	fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<title>歡迎</title>
<body>
	歡迎 %s
</body>

</html>`, username)
}

func requestUser(authorization string) (username string, statusCode int, e error) {
	statusCode = http.StatusInternalServerError
	req, e := http.NewRequest(http.MethodGet, `https://api.github.com/user`, nil)
	if e != nil {
		return
	}
	header := req.Header
	header.Add(`Authorization`, authorization)
	header.Set(`accept`, `application/json`)
	resp, e := http.DefaultClient.Do(req)
	if e != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		statusCode = resp.StatusCode
		e = errors.New(resp.Status)
		return
	}

	var tmp struct {
		Username string `json:"login"`
	}
	decoder := json.NewDecoder(resp.Body)
	e = decoder.Decode(&tmp)
	if e != nil {
		return
	}
	username = tmp.Username
	statusCode = http.StatusOK
	return
}
```

# 完整代碼

```
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc(`/`, home)
	mux.HandleFunc(`/oauth/redirect`, callbackURL)
	mux.HandleFunc(`/user`, user)

	e := http.ListenAndServe(`:9000`, mux)
	if e != nil {
		log.Fatalln(e)
	}
}

func home(w http.ResponseWriter, req *http.Request) {
	w.Header().Set(`Content-Type`, `text/html; charset=utf-8`)

	vals := url.Values{
		`response_type`: {`code`},
		`client_id`:     {clientID},
		`redirect_uri`:  {redirectURI},
	}
	href := authorizeURI + "?" + vals.Encode()
	fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<title>登入</title>
<body>
	<a href="%s">
	Login with github
	</a>
</body>

</html>`, href)
}
func callbackURL(w http.ResponseWriter, req *http.Request) {
	token, statusCode, e := grant(req.URL.Query().Get(`code`))
	if e != nil {
		w.WriteHeader(statusCode)
		w.Write([]byte(e.Error()))
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:  `token`,
		Value: token.TokenType + ` ` + token.AccessToken,
		Path:  `/`,
	})
	http.Redirect(w, req, `/user`, http.StatusFound)
}

type GrantResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	Error       string `json:"error"`
}

func grant(code string) (result *GrantResponse, statusCode int, e error) {
	statusCode = http.StatusInternalServerError
	vals := url.Values{
		`response_type`: {`code`},
		`code`:          {code},
		`redirect_uri`:  {redirectURI},
		`client_id`:     {clientID},
		`client_secret`: {clientSecrets}, // github 要求的密鑰
	}
	req, e := http.NewRequest(http.MethodPost, grantURI, strings.NewReader(vals.Encode()))
	if e != nil {
		return
	}
	header := req.Header
	header.Set(`accept`, `application/json`)
	header.Set(`Content-Type`, `application/x-www-form-urlencoded`)
	resp, e := http.DefaultClient.Do(req)
	if e != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		statusCode = resp.StatusCode
		e = errors.New(resp.Status)
		return
	}
	decoder := json.NewDecoder(resp.Body)
	var tmp GrantResponse
	e = decoder.Decode(&tmp)
	if e != nil {
		return
	} else if tmp.Error != `` {
		e = errors.New(tmp.Error)
		return
	}
	result = &tmp
	statusCode = http.StatusOK
	return
}
func user(w http.ResponseWriter, req *http.Request) {
	token, e := req.Cookie(`token`)
	if e != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(e.Error()))
		return
	}
	username, statusCode, e := requestUser(token.Value)
	if e != nil {
		w.WriteHeader(statusCode)
		w.Write([]byte(e.Error()))
		return
	}
	fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<title>歡迎</title>
<body>
	歡迎 %s
</body>

</html>`, username)
}

func requestUser(authorization string) (username string, statusCode int, e error) {
	statusCode = http.StatusInternalServerError
	req, e := http.NewRequest(http.MethodGet, `https://api.github.com/user`, nil)
	if e != nil {
		return
	}
	header := req.Header
	header.Add(`Authorization`, authorization)
	header.Set(`accept`, `application/json`)
	resp, e := http.DefaultClient.Do(req)
	if e != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		statusCode = resp.StatusCode
		e = errors.New(resp.Status)
		return
	}

	var tmp struct {
		Username string `json:"login"`
	}
	decoder := json.NewDecoder(resp.Body)
	e = decoder.Decode(&tmp)
	if e != nil {
		return
	}
	username = tmp.Username
	statusCode = http.StatusOK
	return
}
```