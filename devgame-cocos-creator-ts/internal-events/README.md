# [系統內置事件](https://docs.cocos.com/creator/manual/zh/scripting/internal-events.html)

cocos 在 事件機制下 提供了 一些 內置的系統事件

# 鼠標事件

鼠標事件 只在 桌面平臺下才有效

| 枚舉對象 | 事件名 | 觸發時機 |
| -------- | -------- | -------- |
| cc.Node.EventType.MOUSE_DOWN     | mousedown     | 鼠標在目標區域按下時觸發     |
| cc.Node.EventType.MOUSE_ENTER     | mouseenter     | 鼠標進入目標區域時     |
| cc.Node.EventType.MOUSE_MOVE     | mousemove     | 鼠標在目標區域移動時     |
| cc.Node.EventType.MOUSE_LEAVE     | mouseleave     | 鼠標移出區域     |
| cc.Node.EventType.MOUSE_UP     | mouseup     | 鼠標按下鬆開時     |
| cc.Node.EventType.MOUSE_WHEEL     | mousewheel     | 鼠標滾輪 滾動時     |

[cc.Event.EventMouse](https://docs.cocos.com/creator/2.1/api/zh/classes/Event.EventMouse.html)

```ts
#info="EventMouse 方法"
// 返回 鼠標滾動 y 距離 只有 存在滾動時才有效
getScrollY(): number

// 返回鼠標 位置
getLocation(): Vec2

// 返回鼠標 位置 x y
getLocationX(): number
getLocationY(): number

// 返回 上次 鼠標 事件 觸發時 鼠標位置
getPreviousLocation(): Vec2

// 相對上次 鼠標事件 移動距離
getDelta(): Vec2
getDeltaX(): number
getDeltaY(): number
```

# 觸摸事件

觸發事件 在移動平臺 和 桌面平臺都會 被觸發

桌面平臺 會響應 鼠標事件

| 枚舉對象 | 事件名 | 觸發時機 |
| -------- | -------- | -------- |
| cc.Node.EventType.TOUCH_START     | touchstart     | 手指觸點落在目標區域內時     |
| cc.Node.EventType.TOUCH_MOVE     | touchmove     | 手指在屏幕上目標區域內移動時     |
| cc.Node.EventType.TOUCH_END     | touchend     | 手指在目標區域內離開屏幕時     |
| cc.Node.EventType.TOUCH_CANCEL     | touchcancel     | 手指在目標區域外離開屏幕時     |

[cc.Event.EventTouch](https://docs.cocos.com/creator/2.1/api/zh/classes/Event.EventTouch.html)

```
#info="EventTouch 方法"

// 當前觸點對象
touch: Touch

// 返回 觸點 id 用於多點觸控
getID(): number

// 返回觸點位置
getLocation(): Vec2
getLocationX(): number
getLocationY(): number

// 返回上次 事件觸發時 觸點位置
getPreviousLocation(): Vec2

// 返回 初始 觸點位置
getStartLocation(): Vec2

// 相對 上次 觸點 移動 距離
getDelta(): Vec2
getDeltaX(): number
getDeltaY(): number
```
# 在捕獲階段 註冊事件處理

通常 子節點會先 捕獲到 鼠標/觸摸 事件

如果向父節點 在此之前 捕獲 比如要實現 滾動視圖 [cc.ScrollView](https://docs.cocos.com/creator/2.1/api/zh/classes/ScrollView.html)  可以在 on 註冊事件時 傳入第四個參數 爲 useCapture 爲 true

此時 事件會被先 派送到 註冊在 捕獲階段 的父節點監聽器 之後才派發給節點自身監聽器 最後在進行冒泡

```
this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStartCallback, this, true)
```

# [cc.Node](https://docs.cocos.com/creator/2.1/api/zh/classes/Node.html) 其它事件

| 枚舉對象 | 事件名 | 觸發時機 |
| -------- | -------- | -------- |
| 無     | position-changed     | 位置屬性修改時     |
| 無     | rotation-changed     | 旋轉屬性修改時     |
| 無     | scale-changed     | 縮放屬性修改時     |
| 無     | size-changed     | 寬高屬性修改時     |
| 無     | anchor-changed     | 錨點屬性修改時     |

