# [標準庫](https://jsonnet.org/ref/stdlib.html)

jsonnet 提供了一些默認功能函數 被稱爲標準庫 這些功能被隱式綁定到 std 變量中

# External Variables 外部變量

## std.extVar(x)

如果定義了給定名稱的外部變量 則返回其字符串值 否則 引發錯誤

# Types and Reflection 類型與反射

## std.thisFile

當前 Jsonnet 檔案名字符串

## std.type(x)

返回 x 的類型名稱字符串 "array", "boolean", "function", "null", "number", "object", and "string"

以下函數返回 boolean 值:
* std.isArray(v) 
* std.isBoolean(v)
* std.isFunction(v)
* std.isNumber(v)
* std.isObject(v)
* std.isString(v)

## std.length(x)

根據x類型 返回 數組長度 字符串碼點數 函數中參數數量 或  對象字段數量

## std.objectHas(o, f)

返回 對象o 是否存在 f 字段 如果存在返回 true 如果不存在或是隱藏字段 返回 false

## std.objectFields(o)

返回 對象 包含所有非隱藏字段的 字段名數組

## std.objectValues(o)

自 0.17.0 起可用

返回 對象 包含所有非隱藏字段的 值數組

## std.objectHasAll(o, f)

類似 std.objectHas 但會返回隱藏字段

## std.objectFieldsAll(o)

類似 std.objectFields 但會返回隱藏字段

## std.objectValuesAll(o)

自 0.17.0 起可用

類似 std.objectValues 但會返回隱藏字段

## std.prune(a)

遞歸刪除 a 的所有 空 成員

空的定義爲 0長度的 數組 0 長度的 對象 或 null 

## std.mapWithKey(func, obj)

返回一個新的 Object，它的每個字段值由函數執行 mapTo 創建，它通常用於將一個簡單 Object 擴展爲複製的 Object 例如爲 envoy 生成監聽器

```
{
  items: std.mapWithKey(
    // filed map to
    function(k, v) {
      name: k,
      level: v,
    },
    // source
    {
      v0: 1,
      v1: 1,
    }
  ),
}
```

```
{
	"items": {
		"v0": {
			"level": 1,
			"name": "v0"
		},
		"v1": {
			"level": 1,
			"name": "v1"
		}
	}
}
```
# Mathematical Utilities 數學運算

以下數學函數可用

* std.abs(n)
* std.sign(n)
* std.max(a, b)
* std.min(a, b)
* std.pow(x, n)
* std.exp(x)
* std.log(x)
* std.exponent(x)
* std.mantissa(x)
* std.floor(x)
* std.ceil(x)
* std.sqrt(x)
* std.sin(x)
* std.cos(x)
* std.tan(x)
* std.asin(x)
* std.acos(x)
* std.atan(x)

函數 std.mod(a, b) 是 % 運算符的目標。 如果在 左側是數字 或者字符串 將執行 模算術 並使用 std.format() 進行 Python-style 的字符串 格式化

## std.std.clamp(x, minVal, maxVal)

限制一個值在 minval max val 之間

* Example: std.clamp(-3, 0, 5) yields 0.
* Example: std.clamp(4, 0, 5) yields 4.
* Example: std.clamp(7, 0, 5) yields 5.

# Assertions and Debugging 斷言和調試

## std.assertEqual(a, b)

確保 a==b 返回 true 或 引發錯誤

# String Manipulation 字符串操作

## std.toString(a)

轉型爲 字符串

## std.codepoint(str)

返回字符串的 unicode 碼點的正整數 是 std.char(n) 的逆函數

## std.char(n)

返回長度爲1的字符串 n 其唯一的 unicode 整數碼點

## std.substr(str, from, len)

返回一個子字符串

## std.findSubstr(pat, str)

返回一個數組 包含了 str 中所有 pat 出現的 索引

## std.startsWith(a, b)

返回 字符串a是否以 字符串 b爲前綴

## std.endsWith(a, b)

返回 字符串a是否以 字符串 b爲後綴

## std.stripChars(str, chars)

自 0.15.0 開始可用

從 str 的開頭 和 結尾 刪除 char

* Example: std.stripChars(" test test test ", " ") yields "test test test".
* Example: std.stripChars("aaabbbbcccc", "ac") yields "bbbb".
* Example: std.stripChars("cacabbbbaacc", "ac") yields "bbbb".

## std.lstripChars(str, chars)

自 0.15.0 開始可用

類似 std.stripChars 但只 刪除開頭的字符

## std.rstripChars(str, chars)

自 0.15.0 開始可用

類似 std.stripChars 但只 刪除結束的字符

## std.split(str, c)

使用 c 分隔字符串

* Example: std.split("foo/bar", "/") yields [ "foo", "bar" ].
* Example: std.split("/foo/", "/") yields [ "", "foo", "" ].

## std.splitLimit(str, c, maxsplits)

類似 std.split 但在 分隔maxsplits此後停止分隔 最多返回 長度爲 maxsplits+1 的 數組

maxsplits 爲 -1 則等同 std.split

* Example: std.splitLimit("foo/bar", "/", 1) yields [ "foo", "bar" ].
* Example: std.splitLimit("/foo/", "/", 1) yields [ "foo", "bar" ].

## std.strReplace(str, from, to)

返回 字符串 str 的副本 並將副本中的 from 替換爲 to

* Example: std.strReplace('I like to skate with my skateboard', 'skate', 'surf') yields "I like to surf with my surfboard".

## std.asciiUpper(str)

返回 字符串 str 的副本 並將副本中所有 ASCII 轉爲大寫

* Example: std.asciiUpper('100 Cats!') yields "100 CATS!".

## std.asciiLower(str)

返回 字符串 str 的副本 並將副本中所有 ASCII 轉爲小寫

* Example: std.asciiLower('100 Cats!') yields "100 cats!".

## std.stringChars(str)

將字符串 拆分爲 數組 

* Example: std.stringChars("foo") yields [ "f", "o", "o" ].

## std.format(str, vals)

格式化 字符串

* Example: std.format("Hello %03d", 12) yields "Hello 012".
* Example: "Hello %03d" % 12 yields "Hello 012".
* Example: "Hello %s, age %d" % ["Foo", 25] yields "Hello Foo, age 25".
* Example: "Hello %(name)s, age %(age)d" % {age: 25, name: "Foo"} yields "Hello Foo, age 25".

## std.escapeStringBash(str)

將 str 括在單引號中 並將str內的任何 更改爲 **'"'"'**， 這允許在 bash 腳本中注入任意字符串作爲命令參數

## std.escapeStringDollars(str)

將 str 中的 $ 轉換爲 **$$** ,這允許將任意字符串注入到使用 $進行字符串插值的系統中

## std.escapeStringJson(str)

轉換 str 使用其可以插入在字符串內的 JSON 表示中 

* Example: local description = "Multiline\nc:\\path"; "{name: %s}" % std.escapeStringJson(description) yields "{name: \"Multiline\\nc:\\\\path\"}".

## std.escapeStringPython(str)

轉換 str 使其 可以嵌入 Python 中 這是 std.escapeStringJson 的別名

# Parsing 解析

## std.parseInt(str)

將字符串 解析爲 十進制整型

* Example: std.parseInt("123") yields 123.
* Example: std.parseInt("-123") yields -123.

## std.parseOctal(str)

將字符串 解析爲 八進制整型

* Example: std.parseOctal("755") yields 493.

## std.parseHex(str)

將字符串 解析爲 十六進制整型

* Example: std.parseHex("ff") yields 255.

## std.parseJson(str)

自 0.13.0 版本可用

解析 JSON 字符串

* Example: std.parseJson('{"foo": "bar"}') yields { "foo": "bar" }.

## std.encodeUTF8(str)

自 0.13.0 版本可用

使用 utf8 編碼字符串 返回代表字節的數字數組

## std.decodeUTF8(arr)

自 0.13.0 版本可用

由 utf8 字節的數字數組 返回 字符串

# Manifestation 表現

## std.manifestIni(ini)

將 給定的結構 轉換爲 INI 格式字符串，這允許使用Jsonnet對象模型來構建配置，以供期望的 INI 應用程式使用。

```
{
    main: { a: "1", b: "2" },
    sections: {
        s1: {x: "11", y: "22", z: "33"},
        s2: {p: "yes", q: ""},
        empty: {},
    }
}
```

```
a = 1
b = 2
[empty]
[s1]
x = 11
y = 22
z = 33
[s2]
p = yes
q =
```

## std.manifestPython(v)

將給定值轉爲爲與 Python 兼容的 類似 JSON 的形式的字符串，主要區別是 Python 使用 True/False/None 替代 JSON 的 true/false/null

```
{
    b: ["foo", "bar"],
    c: true,
    d: null,
    e: { f1: false, f2: 42 },
}
```

```
{
    "b": ["foo", "bar"],
    "c": True,
    "d": None,
    "e": {"f1": False, "f2": 42}
}
```

## std.manifestPythonVars(conf)

類似 std.manifestPython 但轉換爲 Python 變量賦值的形式

```
{
    b: ["foo", "bar"],
    c: true,
    d: null,
    e: { f1: false, f2: 42 },
}
```

```
b = ["foo", "bar"]
c = True
d = None
e = {"f1": False, "f2": 42}
```

## std.manifestJsonEx(value, indent)

轉換爲 JSON 字符串 indent 是一個字符串 其中包含一個或多個用於縮進的空格

```
std.manifestJsonEx(
  {
      x: [1, 2, 3, true, false, null,
          "string\nstring"],
      y: { a: 1, b: 2, c: [1, 2] },
  }, "    ")
```

```
{
    "x": [
        1,
        2,
        3,
        true,
        false,
        null,
        "string\nstring"
    ],
    "y": {
        "a": 1,
        "b": 2,
        "c": [
            1,
            2
        ]
    }
}
```

## std.manifestYamlDoc(value, indent_array_in_object=false)

轉換爲 YAML 字符串 

```
std.manifestYamlDoc(
  {
      x: [1, 2, 3, true, false, null,
          "string\nstring\n"],
      y: { a: 1, b: 2, c: [1, 2] },
  },
  indent_array_in_object=false)
```

```
"x":
  - 1
  - 2
  - 3
  - true
  - false
  - null
  - |
      string
      string
"y":
  "a": 1
  "b": 2
  "c":
      - 1
      - 2
```

## std.manifestYamlStream(value, indent_array_in_object=false, c_document_end=false)

給定一個數組 value 則轉爲 YAML 流

```
std.manifestYamlStream(
  ['a', 1, []],
  indent_array_in_object=false,
  c_document_end=true)
```

```
---
"a"
---
1
---
[]
...
```

## std.manifestXmlJsonml(value)

轉爲 XML 字符串

```
std.manifestXmlJsonml([
    'svg', { height: 100, width: 100 },
    [
        'circle', {
        cx: 50, cy: 50, r: 40,
        stroke: 'black', 'stroke-width': 3,
        fill: 'red',
        }
    ],
])
```

```
<svg height="100" width="100">
    <circle cx="50" cy="50" fill="red" r="40"
    stroke="black" stroke-width="3"></circle>;
</svg>;
```

# Arrays 數組

## std.makeArray(sz, func)

創建大小爲 sz 的數組 使用 func(i) 初始化 每個元素

* Example: std.makeArray(3,function(x) x * x) yields [ 0, 1, 4 ].

## std.member(arr, x)

自 0.15.0 可用

返回 x 是否在 arr中出現，arr 可以是 數組或字符串

## std.count(arr, x)

返回 x 在 arr 中出現的次數

## std.find(value, arr)

返回一個數組 包含所有出現在 arr 中的 value的 索引

## std.map(func, arr)

將 給定函數應用與數組的每個元素 以形成一個新的數組

## std.mapWithIndex(func, arr)

類似 std.map 但 也將 索引傳遞給 函數，func 第一個參數爲索引 第二個參數爲 元素原值

## std.filterMap(filter_func, map_func, arr)

實現過濾 之後 執行 map

## std.flatMap(func, arr)

將給定的函數 應用與arr的每個元素以形成一個新的數組 然後將結果展平

參數 arr 必須是數組或字符串，如果是數組，函數必須返回數組 如果是字符串，函數必須返回字符串

* Example: std.flatMap(function(x) [x, x], [1, 2, 3]) yields [ 1, 1, 2, 2, 3, 3 ].
* Example: std.flatMap(function(x) if x == 2 then [] else [x], [1, 2, 3]) yields [ 1, 3 ].
* Example: std.flatMap(function(x) if x == 2 then [] else [x * 3, x * 2], [1, 2, 3]) yields [ 3, 2, 9, 6 ].
* Example: std.flatMap(function(x) x+x, "foo") yields "ffoooo".

## std.filter(func, arr)

返回一個新數組 其中包含 func 函數爲其返回 true 的 所有 元素

## std.foldl(func, arr, init)

經典的摺疊功能。在上一個函數調用和每個數組元素的結果上調用該函數，在初始元素的情況下調用 init。從左到右遍歷數組

* Example: std.foldl(function(last,v) last+v, [1,2,3,4], 0) yields 10.

## std.foldr(func, arr, init)

經典的摺疊功能。在上一個函數調用和每個數組元素的結果上調用該函數，在初始元素的情況下調用 init。從右到左遍歷數組

* Example: std.foldr(function(v,last) last-v, [1,2,3,4], 0) yields -10.

## std.range(from, to)

返回 [from,to] 間的 升序數組

## std.repeat(what, count)

自 0.15.0 可用

以整數計數指定的次數 重複 數組或字符串

* Example: std.repeat([1, 2, 3], 3) yields [ 1, 2, 3, 1, 2, 3, 1, 2, 3 ].
* Example: std.repeat("blah", 2) yields "blahblah".

## std.slice(indexable, index, end, step)

從 索引中選擇數組或字符串的元素 並返回數組或字符串

* Example: std.slice([1, 2, 3, 4, 5, 6], 0, 4, 1) yields [ 1, 2, 3, 4 ].
* Example: std.slice([1, 2, 3, 4, 5, 6], 1, 6, 2) yields [ 2, 4, 6 ].
* Example: std.slice("jsonnet", 0, 4, 1) yields "json".

## std.join(sep, arr)

1. 如果 sep 是 字符串 則 arr 必須是 字符串數組 此時將使用 sep爲分隔符連接字符串
2. 如果 sep 是一個數組 則 arr 必須是一個like二維維數組 將二維數組 使用 sep 爲分隔符 連接爲 like 一維數

* Example: std.join(".", ["www", "google", "com"]) yields "www.google.com".
* Example: std.join([9, 9], [[1], [2, 3]]) yields [ 1, 9, 9, 2, 3 ].

## std.lines(arr)

將字符串數組 以 \n 連接 適用與構造 bash 腳本等

## std.flattenArrays(arr)

將 like 二維數組 連接爲 like 一維數組

* Example: std.flattenArrays([[1, 2], [3, 4], [[5, 6], [7, 8]]]) yields [ 1, 2, 3, 4, [ 5, 6 ], [ 7, 8 ] ].

## std.reverse(arrs)

自 0.13.0 可用

反轉數組

## std.sort(arr, keyF=id)

使用 <= 運算符 排序數組

可選參數 keyF 是一個單參數函數 用於從數組每個元素中提取比較 key。默認爲 keyF = function（x）x

## std.uniq(arr, keyF=id)

刪除 連接的 重複項目。給定排序數組後，將刪除所有重複項

可選參數 keyF 是一個單參數函數 用於從數組每個元素中提取比較 key。默認爲 keyF = function（x）x

# Sets

set 表示沒有重複的有序數組

## std.set(arr, keyF=id)

std.uniq(std.sort(arr)) 的 語法糖

## std.setInter(a, b, keyF=id)

返回 a b 的交集

## std.setUnion(a, b, keyF=id)

返回 a b 的 並集

* Example: std.setUnion([1, 2], [2, 3]) yields [ 1, 2, 3 ].
* Example: std.setUnion([{n:"A", v:1}, {n:"B"}], [{n:"A", v: 9999}, {n:"C"}], keyF=function(x) x.n) yields [ { "n": "A", "v": 1 }, { "n": "B" }, { "n": "C" } ].

## std.setDiff(a, b, keyF=id)

返回 a b 的 差集

## std.setMember(x, arr, keyF=id)

如果 x 是 數組 的成員 返回 true

# Encoding 編碼

## std.base64(input)

將 給定值 編碼爲 base64 字符串 

該值 可以是 字符串 或 數字數組(碼點必須在 [0,255])

## std.base64DecodeBytes(str)

將 給定的 base64 字符串解碼爲 字節數組(數值數組)

## std.base64Decode(str)

已棄用 應該使用 std.base64DecodeBytes 顯示解碼爲 字節數組 在使用 std.decodeUTF8 轉爲 字符串

## std.md5(s)

返回給定值編碼的 md5 字符串

# JSON Merge Patch 補丁

## std.mergePatch(target, patch)

根據 [RFC7396](https://tools.ietf.org/html/rfc7396) 將補丁應用與目標

# Debugging 調試

## std.trace(tarjget, patch)

自 0.11.0 可用

將給定的字符串 str 輸出到 stderr 並返回 rest 作爲結果

```
local conditionalReturn(cond, in1, in2) =
  if (cond) then
      std.trace('cond is true returning '
              + std.toString(in1), in1)
  else
      std.trace('cond is false returning '
              + std.toString(in2), in2);

{
    a: conditionalReturn(true, { b: true }, { c: false }),
}
```

```
TRACE: test.jsonnet:3 cond is true returning {"b": true}
{
    "a": {
        "b": true
    }
}
```