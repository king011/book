# 剛體

剛體 是物理世界的 基本對象 要添加一個 剛體 爲 Node 掛接一個 [cc.RigidBody](https://docs.cocos.com/creator/api/zh/classes/RigidBody.html) 組件 即可

box2d 支持 Static, Dynamic, Kinematic 三種剛體 creator 則增加了 Animated

Animated 是從 Kinematic 衍生而來 一般修改剛體 選擇或位移時 都是直接設置屬性 而 Animated 會更具當前旋轉或位移 屬性與目標計算所需要的速度 並賦值到對應的移動或旋轉 速度上

Animated 是爲了防止對剛體做動畫時 出現 穿透等現象

* cc.RigidBodyType.Static

  靜態剛體 0質量 0速度 不會受到重力或速度影響 但可以設置位置來移動它
	
* cc.RigidBodyType.Dynamic

  動態剛體 有質量 可以設置 速度 會受到重力影響

* cc.RigidBodyType.Kinematic

  運動剛體 0質量 可以設置速度 不會受到重力影響 但可以設置速度來移動
	
* cc.RigidBodyType.Animated

  動畫剛體
# 質量

剛體的質量是由 碰撞組件的密度 和 大小 自動計算得到的

getMass 可以返回 質量

```
const mass = rigidbody.getMass()
```

# 移動速度

```
// 獲取 移動速度
const velocity:cc.Vec2 = rigidbody.linearVelocity
// 設置 移動速度
rigidbody.linearVelocity = velocity
```

移動速度的 衰減係數 可以用來 模擬 開啓摩擦力 等效果 它會使 現有速度 越來越慢

```
// 獲取 移動衰減係數
const damping:number = rigidbody.linearDamping
// 設置 移動衰減係數
rigidbody.linearDamping = damping
```

> 即時 linearDamping 爲0 動態剛體因爲受到重力影響 故依然不會 一直向上飛
> 

如果想獲取 剛體上某點的 速度 可以使用 getLinearVelocityFromWorldPoint 

```
const velocity:cc.Vec2 = rigidbody.getLinearVelocityFromWorldPoint(worldPoint)
```

剛體 方法 大多 提供了一個 帶 out 的 重載 傳入此方法 會將結果 設置到 out 中 從而減少 創建大量對象的 消耗

```
const velocity = cc.v2()
rigidbody.getLinearVelocityFromWorldPoint(worldPoint, velocity)
```

# 旋轉速度

```
// 獲取 旋轉速度 (角度 如果大於0則 順時針)
const velocity:number = rigidbody.angularVelocity
// 設置 旋轉速度
rigidbody.angularVelocity = velocity
```

同樣可以設置 旋轉衰減係數

```
// 獲取 旋轉衰減係數
var velocity = rigidbody.angularDamping;
// 設置 旋轉衰減係數
rigidbody.angularDamping = velocity;
```

# 固定旋轉

做平臺跳躍遊戲時 通常不希望 主角旋轉屬性也被加入到 物理模擬中 因爲這樣可能讓主角在移動過程中 東倒西歪 此時 可以設置 fixedRotation 爲 true

```
rigidbody.fixedRotation = true
```

# 旋轉 位移 縮放

旋轉 位移 縮放 是遊戲中最常用的 功能 而物理系統中 系統會自動對節點的 這些屬性 與 box2d 中對應屬性 進行 同步

1. box2d中 只有 旋轉和位移 如果縮放 會重新構建 剛體依賴的 全部碰撞體 一個有效的避免方法是 將渲染節點作爲剛到的子節點 只縮放渲染節點 避免縮放 剛體
1. 每個物理時間同步後 會把剛體信息同步到 對應節點上 ，處於效率考慮 節點信息只在用戶對節點相關屬性進行顯示時才同步回剛體，並且節點只會監聽它所在節點 如果修改了父節點的旋轉 位移 不會同步這些信息


# 剛體方法

剛體提供了 api 來獲取世界座標系下的 旋轉位移 這比 通過節點來獲取相關屬性要快 因爲節點還需要提供矩陣計算來獲取結果


獲取剛體世界座標

```
// 直接獲取
let out:cc.Vec2 = rigidbody.getWorldPosition()

// 通過 out 獲取
out = cc.v2()
rigidbody.getWorldPosition(out)
```

獲取剛體世界旋轉值

```
const rotation:number = rigidbody.getWorldRotation()
```

# 局部座標與世界座標轉換

```
// 世界座標轉局部座標
let localPoint = rigidbody.getLocalPoint(worldPoint)
// 或者
localPoint = cc.v2()
rigidbody.getLocalPoint(worldPoint, localPoint)
```

```
// 局部座標轉世界座標
let worldPoint = rigidbody.getWorldPoint(localPoint)
// 或者
worldPoint = cc.v2()
rigidbody.getLocalPoint(localPoint, worldPoint)
```

```
// 局部向量轉世界向量
let worldVector = rigidbody.getWorldVector(localVector)
// 或者
worldVector = cc.v2()
rigidbody.getWorldVector(localVector, worldVector)
```

```
// 世界向量轉局部向量
let localVector = rigidbody.getLocalVector(worldVector)
// 或者
localVector = cc.v2()
rigidbody.getLocalVector(worldVector, localVector)
```

# 獲取剛體質心

當對一個剛體進行施力時 一般會旋轉剛體的質心作爲作力點 這樣才能包裝力不會影響到旋轉值

```
// 獲取本地座標系下的質心
let localCenter = rigidbody.getLocalCenter()

// 或者通過 out 獲取
localCenter = cc.v2()
rigidbody.getLocalCenter(localCenter)

// 獲取世界座標系下的質心
let worldCenter = rigidbody.getWorldCenter()

// 或者通過 out 獲取
worldCenter = cc.v2()
rigidbody.getWorldCenter(worldCenter)
```

# 力 沖量

移動物體有兩種方式

1. 施加一個力 力會隨着時間慢慢修改物體速度
2. 施加一個沖量 沖量可以立刻改變物體速度

當然也可以直接修改位置但那看起來 不像真實世界

```
// 施加一個力到剛體的指定點上 這個點是世界座標系下的點
rigidbody.applyForce(force, point)

// 直接施加力到剛體質心上
rigidbody.applyForceToCenter(force)

// 施加一個沖量到剛體的指定點上 這個點是世界座標系下的點
rigidbody.applyLinearImpulse(impulse, point)
```

力 和 沖量 也可以中對 旋轉軸產生影響 這樣的裏叫 扭矩

```
// 施加扭矩到剛體上 因爲值影響旋轉 所以不需要指定一個點
rigidbody.applyTorque(torque)

// 施加旋轉軸上的沖量到剛體
rigidbody.applyAngularImpulse(impulse)
```
