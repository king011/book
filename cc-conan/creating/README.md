# 創建包

輸入指令 conan new 初始化包檔案
```
# 創建一個 hellow 包 版本號爲 0.1
$ conan new hello/0.1 -t
File saved: conanfile.py
File saved: test_package/CMakeLists.txt
File saved: test_package/conanfile.py
File saved: test_package/example.cpp
```

* conanfile.py 是主要配置檔案
* test_package 檔案夾包含一個示例 連接並使用此包 主要用於測試

```
#info="conanfile.py"
from conans import ConanFile, CMake, tools


class HelloConan(ConanFile):
    name = "hello"
    version = "0.1"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Hello here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        self.run("git clone https://github.com/conan-io/hello.git")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file("hello/CMakeLists.txt", "PROJECT(HelloWorld)",
                              '''PROJECT(HelloWorld)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="hello")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="hello")
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["hello"]
```

1. 第5到15行定義了包的基本信息 其中

	* settings 字段定義了不同二進制包的配置，此示例中定義了 os compiler build_type arch 這些值任何更改都會生成不同的二進制包
	* generators 指示了包將使用 cmake 進行構建
	* options 指定項目支持的 option
	* default_options 指定 option 默認值

2. source() 方法指示了如何獲取源碼
3. build() 方法指示了要如何構建包
4. package() 方法將構建好的包 拷貝到最終的包檔案夾 
5. package_info() 定義了使用此包必須連接到 hello

## test_package

test_package 是包測試 其不同與單元測試，test_package應該是更加全面的測試，用於驗證包是否正確創建，並且包消費者將能夠鏈接並重用它

# 構建和測試

輸入 conan create 指令 構建並測試
```
conan create . demo/testing
```

demo/testing 是 user/channel 可以省略但不建議

```
# 查找包
conan search packagename/version@user/channel

# 刪除包
conan remove packagename/version@user/channel

# 上傳包
conan upload packagename/version@user/channel
```

# 獲取源碼

在 **conanfile.py** 中的 source 方法定義了如何獲取源碼

```
from conans import ConanFile, CMake, tools

class HelloConan(ConanFile):
    ...

    def source(self):
        self.run("git clone https://github.com/conan-io/hello.git")
        ...
```

此外 conan 包裝了 [git](https://docs.conan.io/en/latest/reference/tools.html#tools-git) class 到 python
```
from conans import ConanFile, CMake, tools

class HelloConan(ConanFile):
    ...

    def source(self):
        git = tools.Git(folder="hello")
        git.clone("https://github.com/conan-io/hello.git", "master")
        ...
```

# 包和代碼同源

通常 conan 包和 c++ 代碼 在同個倉庫會比較方便對於自己實現的 conan 包建議採取此方式

需要在 **conanfile.py** 檔案中使用 **exports_sources** 屬性 這樣就不需要同個網路下載源碼 

```
#info="conanfile.py"
from conans import ConanFile, CMake

class HelloConan(ConanFile):
    name = "hello"
    version = "0.1"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of hello here>"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = "src/*"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")
        cmake.build()

        # Explicit way:
        # self.run('cmake "%s/src" %s' % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["hello"]
```

和默認創建的 conanfile.py 有兩點變化
1. exports_sources 字段指示將本地 src 檔案夾 複製到 包中
2. 刪除了 source() 函數，因爲不再需要檢索外部源

同時需要修改 CMakeLists.txt 加入如下 conan 生成的腳本
```
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
```

# 打包現有二進制檔案

使用 --bare 創建包
```
conan new hello/0.1 --bare
```

```
#info="conanfile.py"
class HelloConan(ConanFile):
    name = "hello"
    version = "0.1"
    settings = "os", "compiler", "build_type", "arch"

    def package(self):
        self.copy("*")

    def package_info(self):
        self.cpp_info.libs = self.collect_libs()
```

推薦還是使用 --test 創建創建 測試包以驗證二進制檔案的正確性

```
conan new hello/0.1 --bare --test
```

還是可以創建 build 方法在裏面以及 settings 下載不同的二進制檔案
```
class HelloConan(ConanFile):
    name = "hello"
    version = "0.1"
    settings = "os", "compiler", "build_type", "arch"

    def build(self):
        if self.settings.os == "Windows" and self.settings.compiler == "Visual Studio":
            url = ("https://<someurl>/downloads/hello_binary%s_%s.zip"
                   % (str(self.settings.compiler.version), str(self.settings.build_type)))
        elif ...:
            url = ...
        else:
            raise Exception("Binary does not exist for these settings")
        tools.get(url)

    def package(self):
        self.copy("*") # assume package as-is, but you can also copy specific files or rearrange

    def package_info(self):  # still very useful for package consumers
        self.cpp_info.libs = ["hello"]
```
