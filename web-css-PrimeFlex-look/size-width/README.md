# [width](https://www.primefaces.org/primeflex/width)

width 用於指定元素的寬度

<table class="doc-table">
		<thead>
		<tr>
				<th>Class</th>
				<th>Properties</th>
		</tr>
		</thead>
		<tbody>
		<tr>
				<td>w-full</td>
				<td>width: 100%;</td>
		</tr>
		<tr>
				<td>w-screen</td>
				<td>width: 100vw;</td>
		</tr>
		<tr>
				<td>w-auto</td>
				<td>width: auto;</td>
		</tr>
		<tr>
				<td>w-min</td>
				<td>width: min-content;</td>
		</tr>
		<tr>
				<td>w-max</td>
				<td>width: max-content;</td>
		</tr>
		<tr>
				<td>w-1</td>
				<td>width: 8.3333%;</td>
		</tr>
		<tr>
				<td>w-2</td>
				<td>width: 16.6667%;</td>
		</tr>
		<tr>
				<td>w-3</td>
				<td>width: 25%;</td>
		</tr>
		<tr>
				<td>w-4</td>
				<td>width: 33.3333%;</td>
		</tr>
		<tr>
				<td>w-5</td>
				<td>width: 41.6667%;</td>
		</tr>
		<tr>
				<td>w-6</td>
				<td>width: 50%;</td>
		</tr>
		<tr>
				<td>w-7</td>
				<td>width: 58.3333%;</td>
		</tr>
		<tr>
				<td>w-8</td>
				<td>width: 66.6667%;</td>
		</tr>
		<tr>
				<td>w-9</td>
				<td>width: 75%;</td>
		</tr>
		<tr>
				<td>w-10</td>
				<td>width: 83.3333%;</td>
		</tr>
		<tr>
				<td>w-11</td>
				<td>width: 91.6667%;</td>
		</tr>
		<tr>
				<td>w-12</td>
				<td>width: 100%;</td>
		</tr>
		<tr>
				<td>w-1rem</td>
				<td>width: 1rem;</td>
		</tr>
		<tr>
				<td>w-2rem</td>
				<td>width: 2rem;</td>
		</tr>
		<tr>
				<td>w-3rem</td>
				<td>width: 3rem;</td>
		</tr>
		<tr>
				<td>w-4rem</td>
				<td>width: 4rem;</td>
		</tr>
		<tr>
				<td>w-5rem</td>
				<td>width: 5rem;</td>
		</tr>
		<tr>
				<td>w-6rem</td>
				<td>width: 6rem;</td>
		</tr>
		<tr>
				<td>w-7rem</td>
				<td>width: 7rem;</td>
		</tr>
		<tr>
				<td>w-8rem</td>
				<td>width: 8rem;</td>
		</tr>
		<tr>
				<td>w-9rem</td>
				<td>width: 9rem;</td>
		</tr>
		<tr>
				<td>w-10rem</td>
				<td>width: 10rem;</td>
		</tr>
		<tr>
				<td>w-11rem</td>
				<td>width: 11rem;</td>
		</tr>
		<tr>
				<td>w-12rem</td>
				<td>width: 12rem;</td>
		</tr>
		<tr>
				<td>w-13rem</td>
				<td>width: 13rem;</td>
		</tr>
		<tr>
				<td>w-14rem</td>
				<td>width: 14rem;</td>
		</tr>
		<tr>
				<td>w-15rem</td>
				<td>width: 15rem;</td>
		</tr>
		<tr>
				<td>w-16rem</td>
				<td>width: 16rem;</td>
		</tr>
		<tr>
				<td>w-17rem</td>
				<td>width: 17rem;</td>
		</tr>
		<tr>
				<td>w-18rem</td>
				<td>width: 18rem;</td>
		</tr>
		<tr>
				<td>w-19rem</td>
				<td>width: 19rem;</td>
		</tr>
		<tr>
				<td>w-20rem</td>
				<td>width: 20rem;</td>
		</tr>
		<tr>
				<td>w-21rem</td>
				<td>width: 21rem;</td>
		</tr>
		<tr>
				<td>w-22rem</td>
				<td>width: 22rem;</td>
		</tr>
		<tr>
				<td>w-23rem</td>
				<td>width: 23rem;</td>
		</tr>
		<tr>
				<td>w-24rem</td>
				<td>width: 24rem;</td>
		</tr>
		<tr>
				<td>w-25rem</td>
				<td>width: 25rem;</td>
		</tr>
		<tr>
				<td>w-26rem</td>
				<td>width: 26rem;</td>
		</tr>
		<tr>
				<td>w-27rem</td>
				<td>width: 27rem;</td>
		</tr>
		<tr>
				<td>w-28rem</td>
				<td>width: 28rem;</td>
		</tr>
		<tr>
				<td>w-29rem</td>
				<td>width: 29rem;</td>
		</tr>
		<tr>
				<td>w-30rem</td>
				<td>width: 30rem;</td>
		</tr>
		</tbody>
</table>

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="border-round bg-blue-100 w-12rem p-3 m-3">
            <div class="w-full border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-full</div>
        </div>
        <div class="border-round bg-blue-100 w-12rem p-3 m-3">
            <div class="w-auto border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-auto</div>
        </div>
        <div class="border-round bg-blue-100 w-12rem p-3 m-3">
            <div class="w-min border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-min</div>
        </div>
        <div class="border-round bg-blue-100 w-12rem p-3 m-3">
            <div class="w-max border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-max</div>
        </div>
    </div>
</div>
```

## Fixed Width

你可以使用 rem 來指定固定寬度

![](assets/rem.png)

```
<div class="card">
    <div class="flex flex-column flex-wrap align-items-center justify-content-center card-container yellow-container">
        <div class="w-26rem">
            <div class="w-6rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">w-6rem</div>
            <div class="w-11rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">w-11rem</div>
            <div class="w-20rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">w-20rem</div>
            <div class="w-24rem border-round bg-yellow-500 text-gray-900 font-bold p-3 m-3 flex align-items-center justify-content-center">w-24rem</div>
        </div>
    </div>
</div>
```

## Fluid Width

PrimeFlex 將 100% 寬度分爲了 12 等分，你可以方便的使用它來設置百分比的寬度

![](assets/w12.png)

```
<div class="card">
    <div class="card-container green-container">
        <div class="flex border-round bg-green-100 p-3 m-3">
            <div class="w-2 bg-green-300 text-gray-900 font-bold p-2 flex align-items-center justify-content-center">w-2</div>
            <div class="w-10 bg-green-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-10</div>
        </div>
        <div class="flex border-round bg-green-100 p-3 m-3">
            <div class="w-4 bg-green-300 text-gray-900 font-bold p-3 flex align-items-center justify-content-center">w-4</div>
            <div class="w-8 bg-green-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-8</div>
        </div>
        <div class="flex border-round bg-green-100 p-3 m-3">
            <div class="w-5 bg-green-300 text-gray-900 font-bold p-3 flex align-items-center justify-content-center">w-5</div>
            <div class="w-7 bg-green-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-7</div>
        </div>
        <div class="flex border-round bg-green-100 p-3 m-3">
            <div class="w-6 bg-green-300 text-gray-900 font-bold p-3 flex align-items-center justify-content-center">w-6</div>
            <div class="w-6 bg-green-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-6</div>
        </div>
        <div class="flex border-round bg-green-100 p-3 m-3">
            <div class="w-9 bg-green-300 text-gray-900 font-bold p-3 flex align-items-center justify-content-center">w-9</div>
            <div class="w-3 bg-green-500 text-white font-bold p-3 flex align-items-center justify-content-center">w-3</div>
        </div>
        <div class="flex border-round bg-green-100 p-3 m-3">
            <div class="w-12 bg-green-300 text-gray-900 font-bold p-3 flex align-items-center justify-content-center">w-12</div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同