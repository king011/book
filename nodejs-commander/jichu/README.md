# Command
commander 導出了 class Command 來提供 解析 功能

```ts
#!/usr/bin/env node
import { Command } from "commander"

// 創建一個 指定名稱的 解析器 默認名稱爲 所在檔案夾名字
const program = new Command("commander-test")

// 設置 版本 信息 -V --version
program.version("1.0.0")
    .description("commander api test")
    // 設置 參數
    .option('-s, --server []', // 參數 名稱
        'server address',// 參數 描述
    )
    .option('-l, --local [addr]', 'local bind address',
        "localhost:1080", // 參數 默認值
    )
    .option('--encryption', "encryption data") // 沒有 [] 則是一個 bool型 可以只指定 長/短 參數名
    .action((...args: any[]) => { // 執行 命令
        console.log("server", program["server"])
        console.log("local", program["local"])
        console.log("safe", program["safe"])
    })
    // 解析參數
    .parse(process.argv);
```

## version option

commander 導出了 version option 兩個函數 作爲 new Command().version/option 的語法糖 

```ts
#!/usr/bin/env node
import { version } from "commander"

// 設置 版本 信息 -V --version
const program = version("1.0.0")
    .name("commander-test")
    .description("commander api test")
    // 設置 參數
    .option('-s, --server []', // 參數 名稱
        'server address',// 參數 描述
    )
    .option('-l, --local [addr]', 'local bind address',
        "localhost:1080", // 參數 默認值
    )
    .option('--encryption', "encryption data") // 沒有 [] 則是一個 bool型 可以只指定 長/短 參數名
    .action((...args: any[]) => { // 執行 命令
        console.log("server", program["server"])
        console.log("local", program["local"])
        console.log("safe", program["safe"])
    })
    // 解析參數
    .parse(process.argv);
```
## -v,--version
commander 默認使用 -V 輸出 版本 信息 如果 想要使用 -v 或其它參數名 可以在 調用 version時 傳入第二個 參數 進行 指定
```ts
version('0.0.1', '-v, --version')
```

# 子命令
```ts
#!/usr/bin/env node
import { Command } from "commander"

// 創建一個 指定名稱的 解析器 默認名稱爲 所在檔案夾名字
const program = new Command("commander-test")

// 設置 版本 信息 -V --version
program.version("1.0.0")
    .description("commander api test")
    // 設置 參數
    .option('-s, --server []', // 參數 名稱
        'server address',// 參數 描述
    )
    .option('-l, --local [addr]', 'local bind address',
        "localhost:1080", // 參數 默認值
    )
    .option('--encryption', "encryption data") // 沒有 [] 則是一個 bool型 可以只指定 長/短 參數名


// 子命令
const install = program.command("install")
    .option("-p,--path []")
install.action(() => {
    console.log(install["path"])
})

// 解析參數
program.parse(process.argv);
```
