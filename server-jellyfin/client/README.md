# linux

debian 系的可以安裝官方打包的[安裝包](https://github.com/jellyfin/jellyfin-media-player/releases)，目前 ubuntu 可以安裝 
* focal 後綴(20.04)的的版本
* jammy 後綴(22.04)的的版本



# flatpak

可能無法啓動硬件加速

https://flathub.org/apps/com.github.iwalton3.jellyfin-media-player

安裝
```
flatpak install flathub com.github.iwalton3.jellyfin-media-player
```

更新
```
flatpak install flathub com.github.iwalton3.jellyfin-media-player --or-update 
```

運行

```
flatpak run com.github.iwalton3.jellyfin-media-player
```