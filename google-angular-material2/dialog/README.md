# 對話框

Dialog 通常有如下用途

* **產生提示 :** 用來立即的中斷使用者目前的行爲 並告知使用者目前的狀況或所需要知道的資訊
* **簡易的選單 :** 提供一些基本的選項讓使用者選取
* **確認 :** 需要使用者明確的進行一個確認性的選擇

```
import { MatDialogModule } from '@angular/material/dialog';
```

# 使用 MatDialog

1. 創建一個 component 比如 LoginComponent

1. 因爲要動態生成 component 所以將 component 添加到 所屬 Module 的 entryComponents 中

    ```
    @NgModule({
      ...
      entryComponents: [LoginComponent],
    })
    export class SharedModule { }
    ```

1. 在使用 MatDialog 的模塊 import MatDialogModule

1. 在使用dialog的地方 注入 MatDialog 並調用 open 方法 打開 dialog

    ```
    import { MatDialog } from '@angular/material';
    export class XXXComponent {
      constructor(public dialog: MatDialog) {}
    }
    showDialog() {
        this.dialog.open(LoginComponent)
    }
    ```

# mat-dialog-xxx

material 提供了 多個 mat-dialog-xxx 來

* **mat-dialog-title** dialog 的標題部分 即時發生 滾動 依然會固定在整個 dialog的最上方
* **mat-dialog-content** dialog 的正文部分 當超過 可容納高度 會自動 滾動
* **mat-dialog-actions** 按鈕區域 會固定在 dialog 底部 通常是 確認 取消 等 按鈕
* **mat-dialog-close** 只可以用在 button 上的 directive

```
<h2 mat-dialog-title>
  標題
</h2>
<mat-dialog-content class="post-form">
  正文內容
</mat-dialog-content>

<mat-dialog-actions>
  <button mat-button color="primary">確認</button>
  <button mat-button mat-dialog-close color="warn">取消</button>
</mat-dialog-actions>
```

1. dialog 顯示時 預設會 focus 到**第一個 表單 控制項**
2. 當使用 tab/shift+tab 切換 focus 時 永遠不會跳出 dialog 範圍 只會在 dialog 內 移動
3. 不只按下取消按鈕 按下 ESC鍵 也可以 關閉 dialog

# MatDialog

通過 MatDialog Service 可以 對 dialog 進行 操控

## MatDialog 提供了 如下三個 屬性 

1.  `readonly openDialogs: MatDialogRef<any>[];` 記錄了當前 打開的 dialog
2.  `readonly afterOpened: Subject<MatDialogRef<any>>;` 可訂閱對象 會在 dialog 打開後 調用 next
3.  `readonly afterAllClosed: Observable<void>;` 可訂閱對象 會在 dialog 打開後 調用 next

> afterOpened 從 v8.0.0 開始  替代 棄用的 afterOpen

```
  constructor(private matDialog: MatDialog) {
  }
  ngOnInit() {
    this.matDialog.afterAllClosed.subscribe(() => {
      console.log("all closed")
    })
    this.matDialog.afterOpened.subscribe((dialogRef: MatDialogRef<any>) => {
      console.log("opened", dialogRef.id)
      this.matDialog.openDialogs.forEach((dialogRef: MatDialogRef<any>) => {
        console.log(dialogRef.id)
      })
    })
  }
```

## MatDialog 提供了 如下三個 方法

1. `closeAll(): void;` 關閉打開的 所有 dialog
2. `getDialogById(id: string): MatDialogRef<any> | undefined;` 傳入 id 返回 對應的 dialog 或 undefined
3. `open<T, D = any, R = any>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, config?: MatDialogConfig<D>): MatDialogRef<T, R>;` 打開對話框

# MatDialogConfig

可以透過 MatDialogConfig 設定 dailog 打開的 細節 

MatDialogConfig 包含 大量 屬性 

## data?: D | null;

用來 傳入一些 數據到 打開的 dialog

```
doPost() {
  this.dialog.open(AddPostConfirmDialogComponent, {
    data: {
      title: this.title
    }
  });
}
```

在 dialog 內部 通過 **@Inject\(MAT\_DIALOG\_DATA\)** 來注入 data

```
@Component()
export class AddPostConfirmDialogComponent implements OnInit {
  get title(){
    return this.data.title;
  }
  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
      console.log(data.title);
  }
}
```

## autoFocus?: boolean;

當 打開 dialog 時 是否自動 focus 在 第一個控制項

## id?: string;

可以指定一個 唯一的 id

## 高度 寬度

* width?: string;
* height?: string;
* minWidth?: number | string;
* minHeight?: number | string;
* maxWidth?: number | string;
* maxHeight?: number | string;

除了 width height 必須使用 字符串 其它屬性也支持 number 單位 px

## hasBackdrop?: boolean;

如果不爲 false 則會 產生一個 灰底來隔絕 dialog之下的 畫面 也就是模擬模式對話框

## backdropClass?: string;

backdropClass 可以調整 灰底樣式

## position?: DialogPosition;

指定 dialog 顯示位置

## disableClose?: boolean;

如果爲true 則不能使用 des 取消 dialog

# 使用 MatDialogRef

當 dialog 打開後 會產生一個 MatDialogRef&lt;T&gt; 其中 **T** 代表實際產生的 component 或 templateRef

有 多個 方法 可以 獲取此 MatDialogRef

1. MatDialog.open 會 返回 MatDialogRef (透過 MatDialogRef 可以處理 傳出 事件)

    ```
    doPost() {
    const confirmDialogRef = this.dialog.open(AddPostConfirmDialogComponent, {
        data: {
        title: this.title
        }
    });
    // doConfirm是AddPostConfirmDialogComponent中的事件(EventEmitter)
    // 透過componentInstance取得AddPostConfirmDialogComponent產生的實體
    confirmDialogRef.componentInstance.doConfirm.subscribe(() => {
        console.log('開啟的dialog按下確認按鈕了');
    });
    }
    ```
		
1. 使用 MatDialog.getDialogById 獲取

1. 在 dialog 中 通過 注入 取得

    ```
    @Component()
    export class AddPostDialogComponent {
      constructor(private dialogRef: MatDialogRef<AddPostDialogComponent>)
    
      move() {
        this.dialogRef.updatePosition({
          top: '0',
          left: '0'
        });
      }
    }
    ```
