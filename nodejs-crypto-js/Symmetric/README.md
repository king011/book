# 對稱加密

```
import aes from "crypto-js/aes";
import utf8 from "crypto-js/enc-utf8";

// Encrypt
var ciphertext = aes.encrypt('my message 中文測試', 'secret key 123').toString();

// Decrypt
var bytes = aes.decrypt(ciphertext, 'secret key 123');
var originalText = bytes.toString(utf8);

console.log(originalText); // 'my message'
```

# 工廠模式

crypto-js 支持多種工廠模式

* CBC (the default)
* CFB
* CTR
* OFB
* ECB

```
import SHA256 from "crypto-js/sha256";
import MD5 from "crypto-js/md5";
import FormatHex from "crypto-js/format-hex";
import AES from "crypto-js/aes";
import CFB from "crypto-js/mode-cfb";
import NoPadding from "crypto-js/pad-nopadding";
import UTF8 from "crypto-js/enc-utf8";

const key = SHA256("12345678901")
const iv = MD5("this is iv")
const str = "123草7890123草1"

const enc = AES.encrypt(str,
    key,
    {
        iv: iv,
        mode: CFB,
        padding: NoPadding,
    },
)
console.log(enc.toString(FormatHex))

const dec = AES.decrypt(enc,
    key, {
    iv: iv,
    mode: CFB,
    padding: NoPadding,
},
)
console.log(dec.toString(UTF8))
```