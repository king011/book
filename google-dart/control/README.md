# if and else

```dart
if (isRaining()) {
  you.bringRainCoat();
} else if (isSnowing()) {
  you.wearJacket();
} else {
  car.putTopDown();
}
```

# for loops

```dart
var message = StringBuffer('Dart is fun');
for (var i = 0; i < 5; i++) {
  message.write('!');
}
```

dart for 閉包 捕獲了 索引

```dart
var callbacks = [];
for (var i = 0; i < 2; i++) {
  callbacks.add(() => print(i));
}
callbacks.forEach((c) => c());
```
> dart 因爲 捕獲了 索引 會 輸出 0 1
>
> js 沒有捕獲 索引 會 輸出 2 2

如果 不關心 索引 dart 提供了 forEach 
```dart
candidates.forEach((candidate) => candidate.interview());
```

dart 支持 for in

```dart
var collection = [0, 1, 2];
for (var x in collection) {
  print(x); // 0 1 2
}
```

# while and do-while
```dart
while (!isDone()) {
  doSomething();
}
```

```dart
do {
  printLine();
} while (!atEndOfPage());
```

# break continue
dart 支持 continue 跳過當前 循環 break 結束循環

# switch and case
* case 如果 非空 必須使用 break/continue/return/throw 結束
* case 允許有 局部變量 其範圍是當前 case 
* default 是可選的

```dart
var command = 'OPEN';
switch (command) {
  case 'CLOSED':
    executeClosed();
    break;
  case 'PENDING':
    executePending();
    break;
  case 'APPROVED':
    executeApproved();
    break;
  case 'DENIED':
    executeDenied();
    break;
  case 'OPEN':
    executeOpen();
    break;
  default:
    executeUnknown();
}
```

想使用類似 c 中 非空 case 跳過 結束 必須顯示使用 continue 讓代碼 到指定 case 去執行
```dart
var command = 'CLOSED';
switch (command) {
  case 'CLOSED':
    executeClosed();
    continue nowClosed;
  // Continues executing at the nowClosed label.

  nowClosed:
  case 'NOW_CLOSED':
    // Runs for both CLOSED and NOW_CLOSED.
    executeNowClosed();
    break;
}
```

# assert
assert 用來 在 開發階段 進行一個斷言 如果 其傳入值 爲 false 則 會 拋出一個 異常

assert 語句 在發佈時 會被自動刪掉

```
// Make sure the variable has a non-null value.
assert(text != null);

// Make sure the value is less than 100.
assert(number < 100);

// Make sure this is an https URL.
assert(urlString.startsWith('https'));
```
assert 可以接收 兩個 參數 第二個參數 是 自定義的 錯誤描述

