# [scanner](https://pkg.go.dev/go/scanner)

Scanner 接收一個 []byte 作爲分析對象，然後你通過不斷的調用 Scan 可以對 []byte 提供的源碼進行標記化

```
package main

import (
	"fmt"
	"go/scanner"
	"go/token"
)

func main() {
	// 這是要標記化的 go 源碼
	src := []byte("cos(x) + 1i*sin(x) // Euler")

	// 初始化 Scanner
	var s scanner.Scanner
	fset := token.NewFileSet()                             // 位置是相對於 fset 的
	file := fset.AddFile("main.go", fset.Base(), len(src)) // 註冊輸入檔案
	s.Init(file, src,
		nil,                  // 沒有錯誤處理器
		scanner.ScanComments, // 將註釋作爲 COMMENT tokens
	)

	// 重複調用 Scan 掃描輸入源碼
	for {
		pos, tok, lit := s.Scan()
		if tok == token.EOF {
			break
		}
		fmt.Printf("%s\t%s\t%q\n", fset.Position(pos), tok, lit)
	}
}
```

