# 函數

dart 是純面向對象的語言下 和蹩腳的java不同 dart 允許定義 頂級函數 但 函數頁數 一個 對象 **Function**

dart 也 允許 調用 class 的 實例 就向一個 函數一樣

```dart
#info="函數 定義 列子"
bool isNoble(int atomicNumber) {
  return _nobleGases[atomicNumber] != null;
}
```
```
#info="不寫返回值 則返回 dynamic"
isNoble(int atomicNumber) {
  return _nobleGases[atomicNumber] != null;
}
```

```dart
#info="實現只有一個 表達式 可以簡寫"
bool isNoble(int atomicNumber) => _nobleGases[atomicNumber] != null;
```

如果 函數 沒有 寫 return 則 默認 返回 null

# Optional parameters 可選參數

使用 {} 形式 定義 可選 參數

```dart
main() {
  display(
    1,
    // 可選 參數 使用 key/value 形式 設置
    name: "king",
  );

  display(
    2,
    job: "hunter",
  );

  display(
    0,
    name: "king",
    job: "hunter",
  );

  display(0);
}

display(
  int style,
  // 可選 參數 {} 必須 在 最後 且只能有 一個 {}
  {
  // 沒有傳入 和額外設定 默認 爲 null
  String name,
  job: "killer", // 舊式 代碼使用 : 設置默認值 建議新代碼 都使用 = 設置
  int lv = 10, // 設置 默認值
}) {
  switch (style) {
    case 1:
      print("*** name  ***");
      print("name = $name");
      break;
    case 2:
      print("*** job  ***");
      print("job = $job");
      break;
    default:
      print("*** default  ***");
      print("name = $name");
      print("job = $job");
      print("lv = $lv");
      print("style = $style");
  }
  ;
}
```

# 位置參數 默認值

對與位置 參數 也可以設置 默認值 使用 **\[\]** 即可

```dart
main() {
  assert(say('Bob', 'Howdy') == 'Bob says Howdy with a carrier pigeon');
}

String say(String from, String msg,
    [String device = 'carrier pigeon', String mood]) {
  var result = '$from says $msg';
  if (device != null) {
    result = '$result with a $device';
  }
  if (mood != null) {
    result = '$result (in a $mood mood)';
  }
  return result;
}
```

List 和 Map 都可以 作爲 默認值
```dart
void doStuff(
    {List<int> list = const [1, 2, 3],
    Map<String, String> gifts = const {
      'first': 'paper',
      'second': 'cotton',
      'third': 'leather'
    }}) {
  print('list:  $list');
  print('gifts: $gifts');
}
```

# 匿名函數
```
main() {
  var name = "貓";
  // 定義 匿名 函數
  // 函數 可以 存儲到 變量中
  var speak = (String str) {
    // dart 支持 閉包
    print("$name : $str");
  };
  speak("喵~");

  // 函數 簡寫形式
  speak = (String str) => print("$name : $str $str");
  speak("喵~");
}
```