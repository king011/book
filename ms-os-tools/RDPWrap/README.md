# RDPWrap

RDPWrap 爲 windows home 提供了 遠程桌面功能

* 源碼 [https://github.com/stascorp/rdpwrap](https://github.com/stascorp/rdpwrap)

# autoupdate.bat

[https://github.com/asmtron/rdpwrap](https://github.com/asmtron/rdpwrap) 項目爲 RDPWrap 提供了配置自動更新功能

安裝說明 [https://github.com/asmtron/rdpwrap/blob/master/binary-download.md](https://github.com/asmtron/rdpwrap/blob/master/binary-download.md)

