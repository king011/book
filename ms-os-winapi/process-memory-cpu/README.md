# GetProcessMemoryInfo

使用 GetProcessMemoryInfo 可以 獲取 進程 內存使用 情況

使用 GetProcessTimes 可以獲取 進程 cpu 時間 GetSystemTimes 可以 獲取 系統 cpu 時間 由 兩次GetProcessTimes之差/兩次GetSystemTimes之差 即可 算出 cpu 使用率

```
#include <iostream>
#include <windows.h>
#include <psapi.h>
#define KB 1024
#define MB (1024 * KB)
#define GB (1024 * MB)

class process_t
{
    HANDLE _handle = NULL;
    uint64_t _system = 0;
    uint64_t _process;

public:
    process_t(DWORD pid)
    {
        _handle = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid);
        if (!_handle)
        {
            throw std::string("OpenProcess error");
        }
    }
    ~process_t()
    {
        CloseHandle(_handle);
    }
    // 顯示 內存 使用情況
    void display_memory()
    {
        PROCESS_MEMORY_COUNTERS m;
        if (!GetProcessMemoryInfo(_handle, &m, sizeof(PROCESS_MEMORY_COUNTERS)))
        {
            throw std::string("GetProcessMemoryInfo error");
        }
        std::cout << "頁面錯誤 = " << m.PageFaultCount << "\n";
        std::cout << "* 蜂值工作集 = " << (m.PeakWorkingSetSize / KB) << "k = " << format_memory(m.PeakWorkingSetSize) << "\n";
        std::cout << "* 當前工作集 = " << (m.WorkingSetSize / KB) << "k = " << format_memory(m.WorkingSetSize) << "\n";
        std::cout << "蜂值分頁池 = " << (m.QuotaPeakPagedPoolUsage / KB) << "k = " << format_memory(m.QuotaPeakPagedPoolUsage) << "\n";
        std::cout << "當前分頁池 = " << (m.QuotaPagedPoolUsage / KB) << "k = " << format_memory(m.QuotaPagedPoolUsage) << "\n";
        std::cout << "蜂值非頁池 = " << (m.QuotaPeakNonPagedPoolUsage / KB) << "k = " << format_memory(m.QuotaPeakNonPagedPoolUsage) << "\n";
        std::cout << "當前非頁池 = " << (m.QuotaNonPagedPoolUsage / KB) << "k = " << format_memory(m.QuotaNonPagedPoolUsage) << "\n";
        std::cout << "當前提交內存總量 = " << (m.PagefileUsage / KB) << "k = " << format_memory(m.PagefileUsage) << "\n";
        std::cout << "蜂值提交內存總量 = " << (m.PeakPagefileUsage / KB) << "k = " << format_memory(m.PeakPagefileUsage) << std::endl;
    }
    static std::string format_memory(std::size_t v)
    {
        std::string str;
        if (v > GB)
        {
            str += std::to_string(v / GB) + "G";
            v %= GB;
        }
        if (v > MB)
        {
            str += std::to_string(v / MB) + "M";
            v %= MB;
        }
        if (v > KB)
        {
            str += std::to_string(v / KB) + "K";
            v %= KB;
        }
        if (v)
        {
            str += std::to_string(v);
        }
        return str;
    }
    // 顯示 cpu 使用率
    void display_cpu()
    {
        // 獲取 系統 內核 用戶態 時間
        FILETIME systemKernel, systemUser;
        if (!GetSystemTimes(NULL, &systemKernel, &systemUser))
        {
            throw std::string("GetSystemTimes error");
        }
        // 獲取 進程 內核 用戶態 時間
        FILETIME tmp, kernel, user;
        if (!GetProcessTimes(_handle, &tmp, &tmp, &kernel, &user))
        {
            throw std::string("GetProcessTimes error");
        }
        // 計算 時間 總量
        uint64_t system = to_utc(systemKernel) + to_utc(systemUser);
        uint64_t process = to_utc(kernel) + to_utc(user);
        if (_system == 0)
        {
            // 第一次 運行 只記錄 最後值
            _system = system;
            _process = process;
            return;
        }

        // 計算 cpu 佔用
        std::cout << (process - _process) * 100 / (system - _system) << "%" << std::endl;

        // 記錄 最後值
        _system = system;
        _process = process;
    }
    static uint64_t to_utc(const FILETIME &ftime)
    {
        LARGE_INTEGER li;
        li.LowPart = ftime.dwLowDateTime;
        li.HighPart = ftime.dwHighDateTime;
        return li.QuadPart;
    }
};
int main()
{
    try
    {
        process_t process(9428);
        process.display_memory();
        while (true)
        {
            process.display_cpu();
            Sleep(1000);
        }
    }
    catch (const std::string e)
    {
        std::cerr << e << std::endl;
        return 1;
    }
    return 0;
}
```