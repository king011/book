![](assets/bar.png)

```html
<!DOCTYPE html>
<html id="app">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>測試</title>

	<!-- 引入 js css -->

	<!-- 不需要兼容 sb的 ie9 以及以下版本 所以不 引入 excanvas -->
	<!-- <script src="jqplot/excanvas.js"></script> -->
	<script src="jquery-2.2.4.min.js"></script>
	<script src="jqplot/jquery.jqplot.min.js"></script>
	<script src="jqplot/plugins/jqplot.barRenderer.js"></script>
	<script src="jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
	<script src="jqplot/plugins/jqplot.pointLabels.js"></script>
	<link href="jqplot/jquery.jqplot.min.css">
</head>

<body>
	<!-- 繪製目標 -->
	<div id="chartdiv" style="height:400px;width:400px; "></div>
</body>
<script>
	$(document).ready(function () {
		$.jqplot.config.enablePlugins = true;
		const s1 = [2, 6, 7, 10, 5];
		const s2 = [4, 5, 3, 5, 6];
		const ticks = ['a', 'b', 'c', 'd', 'e'];
		$.jqplot(
			'chartdiv', // 目標 id
			// 繪製 數據
			[
				s1, // 柱1
				s2, // 柱2
				s2, // 柱3
				/* 
					...
					每個 數組 定義一個 柱
				*/
			],
			{
				title: 'bar',
				// 顯示 繪製動畫
				animate: true,
				seriesDefaults: {
					renderer: $.jqplot.BarRenderer, // 使用 柱圖
					// 顯示 y 值
					pointLabels: { show: true }
				},
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks // 定義 x 座標 顯示文字
					}
				},
			},
		);
	});
</script>

</html>
```