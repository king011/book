# Cocos Creator 地圖

Cocos 地圖主要使用 [TiledMap](https://www.mapeditor.org/) 

creator 目前只支持到 [TiledMap v1.0.3](https://github.com/bjorn/tiled/releases/tag/v1.0.3)

* 官網 [http://www.cocos.com](http://www.cocos.com)
* 文檔 [http://docs.cocos.com/creator/manual/zh/](http://docs.cocos.com/creator/manual/zh/)
* API [http://docs.cocos.com/creator/api/zh/](http://docs.cocos.com/creator/api/zh/)
* 源碼 [https://github.com/cocos2d/cocos2d-x](https://github.com/cocos2d/cocos2d-x)