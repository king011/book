# 函數
ts 支持js的 全部 函數 寫法 同時 可以爲 參數 返回值 標明 型別
```ts
function Speak(str:string):void{
    console.log(str)
}
Speak("cerberus is an idea")
```

# 可選參數 默認參數
js所有參數都是可選的 如果不傳 則爲 **undefined** ts 需要使用 **?** 定義一個 可選參數 允許 不傳入 此參數

可選參數 必須定義到 參數 最後
```ts
function Speak(str: string, who?: string): void {
    if (who === void 0) {
        who = "illusive man"
    }
    console.log(who, ":", str)
}
Speak("cerberus is an idea")
```

# 默認 參數
ts 支持 默認 參數 如果 此參數 未傳入 或 傳入 undefined 則 ts 會自動將其設置爲 默認值

```ts
function Speak(str: string, who: string = "illusive man"): void {
    console.log(who, ":", str)
}
function Speak2(who: string = "illusive man", str: string): void {
    Speak(str, who)
}
Speak("cerberus is an idea")
Speak2(undefined, "cerberus is an idea")
```

```js
"use strict";
function Speak(str, who) {
    if (who === void 0) { who = "illusive man"; }
    console.log(who, ":", str);
}
function Speak2(who, str) {
    if (who === void 0) { who = "illusive man"; }
    Speak(str, who);
}
Speak("cerberus is an idea");
Speak2(undefined, "cerberus is an idea");
```

# 不定長 參數
js 中 使用 arguments 訪問 不定長 參數 ts 可以使用 ... 將其收集到一個 變量中
```ts
function Speak(name: string, ...speaks: string[]): void {
    console.log(name, ...speaks)
}

Speak("illusive man", "cerberus", "is", "an", "idea")
```

# 函數 重載
js 通過 動態 判斷 函數 傳入 型別 實現類似 函數重載的 功能 ts在此 之前 允許 在函數 實現之上 寫出 重載簽名 以便 編譯器 能夠識別 重載
```ts
function info(v: number): number
function info(v: string): string
function info(v: number | string): any {
    const t = typeof v
    if (t == "number") {
        console.log("number", v)
    } else if (t == "string") {
        console.log("string", v)
    }
}
info(123)
info("456")
```