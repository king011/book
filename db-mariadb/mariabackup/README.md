# [mariabackup](https://mariadb.com/kb/en/mariabackup-overview/)

mysqldump 效率很低只適合小數據非複製 對於 大數據 應該使用 xtraBackup

mariadb 基於 xtraBackup 開發類 mariabackup

# 權限 
執行 mariabackup 的用戶必須具有 RELOAD，PROCESS，LOCK TABLES 和 REPLICATION CLIENT 權限

```
CREATE USER 'mariabackup'@'localhost' IDENTIFIED BY '123';
GRANT RELOAD, LOCK TABLES, PROCESS, REPLICATION CLIENT ON *.* TO 'mariabackup'@'localhost';
```

# 完整備份
1. 創建備份

	```
	mariabackup --backup --host=127.0.0.1 --port=3306  --user=root --password=123456 --target-dir= /backup/$(date '+%y-%m-%d')_fullbackup
	```
	
2. 準備還原

	```
	mariabackup --prepare --target-dir=/backup/$(date '+%y-%m-%d')_fullbackup
	```

3. 還原數據

	```
	mariabackup --copy-back --host=127.0.0.1 --port=3306  --user=root --password=123456 --target-dir=/backup/$(date '+%y-%m-%d')_fullbackup
	```
	
# 增量備份

* --incremental-basedir 參數 指定增加備份的基礎目錄

1. 創建備份

	```
	mariabackup --backup --target-dir=/var/mariadb/backup    --user=root --password=
	mariabackup --backup --target-dir=/var/mariadb/inc1    --incremental-basedir=/var/mariadb/backup    --user=root --password=
	mariabackup --backup --target-dir=/var/mariadb/inc2    --incremental-basedir=/var/mariadb/inc1    --user=root --password=
	mariabackup --backup --target-dir=/var/mariadb/inc3    --incremental-basedir=/var/mariadb/inc2    --user=root --password=
	```
	
2. 準備還原

	```
	mariabackup --prepare --target-dir=/var/mariadb/backup
	mariabackup --prepare --target-dir=/var/mariadb/backup    --incremental-dir=/var/mariadb/inc1
	mariabackup --prepare --target-dir=/var/mariadb/backup    --incremental-dir=/var/mariadb/inc2
	mariabackup --prepare --target-dir=/var/mariadb/backup    --incremental-dir=/var/mariadb/inc3
	```
	
3. 還原數據

	```
	mariabackup --copy-back --target-dir=/var/mariadb/backup
	```