# [background-repeat](https://www.primefaces.org/primeflex/backgroundrepeat)

background-repeat 同於指定背景圖像如何重複

| Class | Properties |
| -------- | -------- |
| bg-repeat     | background-repeat: repeat;     |
| bg-no-repeat     | background-repeat: no-repeat;     |
| bg-repeat-x     | background-repeat: repeat-x;     |
| bg-repeat-y     | background-repeat: repeat-y;     |
| bg-repeat-round     | background-repeat: round;     |
| bg-repeat-space     | background-repeat: space;     |

## bg-repeat

bg-repeat 使背景在水平和垂直的方向上都重複

![](assets/repeat.png)

```
<div class="card">
    <div class="card-container blue-container overflow-hidden">
        <div class="bg-repeat bg-blue-100 border-blue-500 border-2 border-round w-full" style="background-image: url('./assets/images/product-placeholder-blue.svg'); background-size: 10rem; height: 30rem"></div>
    </div>
</div>
```

## bg-no-repeat

bg-no-repeat 的背景不會被重複

![](assets/no.png)

```
<div class="card">
    <div class="flex align-items-center justify-content-center card-container yellow-container overflow-hidden">
        <div class="bg-no-repeat bg-yellow-100 bg-center w-19rem h-19rem bg-yellow-500 border-2 border-yellow-500 border-round" style="background-image: url('./assets/images/product-placeholder-yellow.svg')"></div>
    </div>
</div>
```

## bg-repeat-x

bg-repeat-x 的背景在水平方向重複

![](assets/x.png)

```
<div class="card">
    <div class="card-container green-container overflow-hidden">
        <div class="bg-repeat-x bg-green-100 bg-center border-green-500 border-2 border-round h-10rem w-full" style="background-image: url('./assets/images/product-placeholder-green.svg'); background-size: 10rem"></div>
    </div>
</div>
```

## bg-repeat-y

bg-repeat-y 的背景在垂直方向重複

![](assets/y.png)

```
<div class="card">
    <div  class="flex justify-content-center card-container purple-container overflow-hidden">
        <div class="bg-repeat-y bg-purple-100 bg-center h-20rem w-10rem border-purple-500 border-2 border-round" style="background-image: url('./assets/images/product-placeholder-purple.svg'); background-size: 10rem"></div>
    </div>
</div>
```

## bg-repeat-round

bg-repeat-round 使背景重複或拉伸圖像以填充空間

![](assets/round.png)

```
<div class="card">
    <div class="card-container indigo-container overflow-hidden">
        <div class="bg-repeat-round bg-indigo-100 border-indigo-500 border-round border-2 h-20rem w-full" style="background-image: url('./assets/images/product-placeholder-indigo.svg'); background-size: 10rem"></div>
    </div>
</div>
```

## bg-repeat-space

bg-repeat-space 的背景重複而不剪裁

![](assets/space.png)

```
<div class="card">
    <div class="card-container orange-container overflow-hidden">
        <div class="bg-repeat-space bg-orange-100 bg-center border-orange-500 border-2 border-round h-12rem w-full" style="background-image: url('./assets/images/product-placeholder-orange.svg'); background-size: 10rem"></div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同