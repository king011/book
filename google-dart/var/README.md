# 變量定義

```
void main(List<String> args) {
  // 使用 型別名 變量名 定義變量
  int number = 123;

  // 使用 var 定義變量 自動 推導 型別
  var str = 'king';

  // 使用 dynamic 定義 動態型別 變量
  dynamic any = 123;
  any = "ok";

  print(number);
  print(str);
  print(any);
}
```

dart 中 所有型別(包括 數字 和 布爾)的 默認值 都是 null

```
void main(List<String> args) {
  int number;
  assert(number == null);
  bool ok;
  assert(ok == null);

  dynamic any;
  assert(any == null);
}
```

使用 final 定義 的變量 只能被設置 一次 const 定義的 是編譯時常量(也只能設置一次)
```
void main(final List<String> args) {
  // args 是 final 使所以 函數中 無法修改其引用的 對象

  const c0 = 0;
  const int c1 = 1;
  const dynamic c2 = 2;

  final f0 = 0;
  final int f1 = 1;
  final dynamic f2 = 2;

  print(c0 + c1 + c2 + f0 + f1 + f2);
}
```