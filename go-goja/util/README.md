# util

util 包提供了一些 輔助方法

* 目前只提供了 format 函數 用於 使用 c sprintf 風格 格式化字符串
* format 只支持 最簡單的 %d %s %j 形式 %j 使用 JSON 編碼輸出
* 不建議使用 目前的 util 包 太過簡陋
 
```
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/require"
	_ "github.com/dop251/goja_nodejs/util"
)

func main() {
	// 創建 模塊 登記處
	registry := require.NewRegistry()

	// 創建 虛擬機
	vm := goja.New()
	vm.Set(`println`, func(args ...interface{}) {
		fmt.Println(args...)
	})
	registry.Enable(vm) // 爲 Runtime 啓用模塊
	_, e := vm.RunScript(`main.js`, `
var util = require('util')
println(util.format("%s will win in %d.","Trump",2020))
println(util.format('%j',{name:'kk'}))
`)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(registry)
}
```
