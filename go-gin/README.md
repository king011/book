# gin
gin 是一個 golang 實現的 開源(MIT) 快速 簡潔的 http 服務器框架

* [源碼](https://github.com/gin-gonic/gin)

# 安裝
```sh
go get -v -u github.com/gin-gonic/gin
```

# jsoniter

gin 默認使用 encoding/json 編碼 json 也可以 在編譯時指定使用 jsoniter 

```sh
go build -tags=jsoniter 
```