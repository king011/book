# [ConfirmDialog](https://primeng.org/confirmdialog)

```
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
```
```
@Component({
  providers: [ConfirmationService],
})
export class ConfirmDialogDemo {
}
```

ConfirmDialog 是一個確認對話框，通常用於讓用戶確認某些操作，比如在刪除資料前讓用戶再次確認以免誤操作。

首先需要在 html 中使用 **p-confirmDialog** 創建一個確認框
```
<p-confirmDialog header="Confirmation" icon="pi pi-exclamation-triangle"></p-confirmDialog>

<button (click)="confirm()" pButton icon="pi pi-check" label="Confirm"></button>
```

之後在 ts 中使用 confirmationService 服務來顯示確認框

```
export class ConfirmDialogDemo {

    constructor(private confirmationService: ConfirmationService) {}

    confirm() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                //Actual logic to perform a confirmation
            },
            // key: "XXX",
        });
    }
}
```

如果 html 中存在多個 **p-confirmDialog**,可以在 p-confirmDialog 中設置 key 屬性，然後在 ts 中傳入要顯示的確認框 key

PrimeNG 不會驗證 key 是否唯一，它只是遍歷節點樹讓後使用使用 key 與指定值一致的 p-confirmDialog 組件全部顯示

## Responsive

你可以使用 **breakpoints** 屬性來支持響應式佈局，下面例子中默認寬度是 50vw，屏幕寬度低於 961px 寬度爲 75vw，屏幕寬度低於 641px 寬度是 100vw。（breakpoints 的 key 是屏幕最大尺寸，value 是對應屏幕下對話框的寬度）

```
<p-confirmDialog [breakpoints]="{'960px': '75vw', '640px': '100vw'}" [style]="{width: '50vw'}">
    Content
</p-confirmDialog>
```

## ConfirmationService

ConfirmationService 服務用於管理對話框，它的 confirm 方法接收一個 interface Confirmation 用於顯示對話框

ConfirmationService 提供了一個 close 函數用於隱藏對話框而不調用回調函數

```
export interface Confirmation {
    message?: string;
    key?: string;
    icon?: string;
    header?: string;
    accept?: Function;
    reject?: Function;
    acceptLabel?: string;
    rejectLabel?: string;
    acceptIcon?: string;
    rejectIcon?: string;
    acceptVisible?: boolean;
    rejectVisible?: boolean;
    blockScroll?: boolean;
    closeOnEscape?: boolean;
    dismissableMask?: boolean;
    defaultFocus?: string;
    acceptButtonStyleClass?: string;
    rejectButtonStyleClass?: string;
    target?: EventTarget;
    acceptEvent?: EventEmitter<any>;
    rejectEvent?: EventEmitter<any>;
}
```

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| message     | string     | null     | 顯示的確認消息文本     |
| key     | string     | null     | 指定要顯示哪個確認框     |
| icon     | string     | null     | 在 message 最前面顯示一個圖標     |
| header     | string     | null     | 標題顯示文本     |
| accept     | Function     | null     | accept 時執行的回調     |
| reject     | Function     | null     | reject 時執行的回調     |
| acceptLabel     | string     | null     | 確認按鈕顯示文本     |
| rejectLabel     | string     | null     | 拒絕按鈕顯示文本     |
| acceptIcon     | string     | pi pi-check     | 確認按鈕顯示圖標     |
| rejectIcon     | string     | pi pi-times     | 拒絕按鈕顯示圖標     |
| acceptButtonStyleClass     | string     | null     | accept button 的 css class     |
| rejectButtonStyleClass     | string     | null     | reject button 的 css class     |
| acceptVisible     | boolean     | true     | 是否顯示確認按鈕     |
| rejectVisible     | boolean     | true     | 是否顯示拒絕按鈕     |
| style     | string     | null     | 爲組件嵌入內聯的 css style     |
| styleClass     | string     | null     | 爲組件嵌入的 css class     |
| maskStyleClass     | string     | null     | 爲遮罩層設置 css class     |
| blockScroll     | boolean     | true     | 當對話框可見時是否應阻止背景滾動     |
| closeOnEscape     | boolean     | true     | 鍵盤 Esc 被按下時是否關閉對話框     |
| dismissableMask     | boolean     | false     | 單擊遮罩層時是否關閉對話框     |
| defaultFocus     | string     | accept     | 當對話框可見時是否接收焦點元素，有效值爲: accept reject close none     |

## Customization

你可以客製化自己的確認框，它可以支持客製化 header 和 footer 只是注意需要將確認和拒絕按鈕指定到自己客製化的 click 處理函數

```
<p-confirmDialog #cd [style]="{width: '50vw'}">
    <ng-template pTemplate="header">
        <h3>Header Content</h3>
    </ng-template>
    <ng-template pTemplate="footer">
        <button type="button" pButton icon="pi pi-times" label="No" (click)="cd.reject()"></button>
        <button type="button" pButton icon="pi pi-check" label="Yes" (click)="cd.accept()"></button>
    </ng-template>
</p-confirmDialog>
```

## Animation Configuration
 使用 **transitionOptions** 屬性可以指定確認框顯示和隱藏的過渡動畫，默認值是 **400ms cubic-bezier(0.25, 0.8, 0.25, 1)**，下面的例子將完全禁用過渡動畫
 
 ```
 <p-confirmDialog [transitionOptions]="'0ms'">
</p-confirmDialog>
 ```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onHide     | confirmEventType: ConfirmEventType.ACCEPT | ConfirmEventType.REJECT | ConfirmEventType.CANCEL     | 確認框隱藏時回調     |


# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| header     | string     | null     | 標題文本     |
| message     | string     | null     | 顯示的確認消息文本     |
| key     | string     | null     | 爲對話框指定一個匹配用的 key     |
| icon     | string     | null     | 顯示在 message 前面的圖標     |
| acceptLabel     | string     | Yes     | accept 按鈕文本     |
| acceptAriaLabel     | string     | null     | accept 按鈕的 aria labe     |
| acceptIcon     | string     | pi pi-check     | accept 按鈕圖標     |
| acceptVisible     | boolean     | true     | accept 按鈕是否可見     |
| rejectLabel     | string     | No     | reject 按鈕文本     |
| rejectAriaLabel     | string     | null     | reject 按鈕的 aria labe     |
| rejectIcon     | string     | pi pi-times     | reject 按鈕圖標     |
| rejectVisible     | boolean     | true     | reject 按鈕是否可見     |
| closeOnEscape     | boolean     | true     | 鍵盤 Esc 被按下時是否關閉對話框     |
| dismissableMask     | boolean     | false     | 	單擊遮罩層時是否關閉對話框     |
| rtl     | boolean     | false     | 如果爲 true 對話框以 RTL 方向顯示     |
| closable     | boolean     | true     | 向標題中添加關閉圖標按鈕以隱藏對話框     |
| focusTrap     | boolean     | true     | 啓用後只有對話框內的元素能夠獲取焦點     |
| appendTo     | any     | null     | Target element to attach the dialog, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| acceptButtonStyleClass     | string     | null     | accept button 的 css class     |
| rejectButtonStyleClass     | string     | null     | reject button 的 css class     |
| baseZIndex     | number     | 0     | 用於分層的基礎 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| breakpoints     | object     | null     | 爲不同尺寸的屏幕指定對話框寬度     |
| transitionOptions     | string     | 400ms cubic-bezier(0.25, 0.8, 0.25, 1)     | 過渡動畫     |
| defaultFocus     | string     | accept     | 當對話框可見時是否接收焦點元素，有效值爲: accept reject close none     |

# Templates

| Name | Parameters |
| -------- | -------- |
| header     | -     |
| footer     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-dialog     | 	Container element     |
| p-confirmdialog     | Container element     |
| p-dialog-titlebar     | Container of header.     |
| p-dialog-title     | 	Header element.     |
| p-dialog-titlebar-icon     | Icon container inside header.     |
| p-dialog-titlebar-close     | Close icon element.     |
| p-dialog-content     | Content element.     |