# [Particles](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html)

Flame 提供了一個基本但強大且可擴展的粒子系統。 這個系統的核心概念是 Particle 類，它的行為與 ParticleSystemComponent 非常相似。

FlameGame 中粒子的最基本用法如下所示：

```
import 'package:flame/components.dart';

// ...

game.add(
  // Wrapping a Particle with ParticleSystemComponent
  // which maps Component lifecycle hooks to Particle ones
  // and embeds a trigger for removing the component.
  ParticleSystemComponent(
    particle: CircleParticle(),
  ),
);
```

當 Particle 與自訂 Game 實作一起使用時，請確保在每個遊戲循環週期期間呼叫 update 和 render 方法

實現所需粒子效果的主要方法：
* 現有行為的組成。
* 使用行為鏈（只是第一個的語法糖）。
* 使用 ComputedParticle

組合的工作方式與 Flutter 小部件類似，透過從上到下定義效果。 連結允許透過從下到上定義行為來更流暢地表達相同的組合樹。 計算粒子又將行為的實作完全委託給您的程式碼。 任何方法都可以在需要時與現有行為結合使用。

```
Random rnd = Random();

Vector2 randomVector2() => (Vector2.random(rnd) - Vector2.random(rnd)) * 200;

// Composition.
//
// Defining a particle effect as a set of nested behaviors from top to bottom,
// one within another:
//
// ParticleSystemComponent
//   > ComposedParticle
//     > AcceleratedParticle
//       > CircleParticle
game.add(
  ParticleSystemComponent(
    particle: Particle.generate(
      count: 10,
      generator: (i) => AcceleratedParticle(
        acceleration: randomVector2(),
        child: CircleParticle(
          paint: Paint()..color = Colors.red,
        ),
      ),
    ),
  ),
);

// Chaining.
//
// Expresses the same behavior as above, but with a more fluent API.
// Only Particles with SingleChildParticle mixin can be used as chainable behaviors.
game.add(
  ParticleSystemComponent(
    particle: Particle.generate(
      count: 10,
      generator: (i) => pt.CircleParticle(paint: Paint()..color = Colors.red)
    )
  )
);

// Computed Particle.
//
// All the behaviors are defined explicitly. Offers greater flexibility
// compared to built-in behaviors.
game.add(
  ParticleSystemComponent(
      particle: Particle.generate(
        count: 10,
        generator: (i) {
          Vector2 position = Vector2.zero();
          Vector2 speed = Vector2.zero();
          final acceleration = randomVector2();
          final paint = Paint()..color = Colors.red;

          return ComputedParticle(
            renderer: (canvas, _) {
              speed += acceleration;
              position += speed;
              canvas.drawCircle(Offset(position.x, position.y), 1, paint);
            }
        );
      }
    )
  )
);
```

您可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/particles_example.dart)找到有關如何以各種組合使用不同內建粒子的更多範例

# [Lifecycle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#lifecycle)

所有粒子的共同行為是它們都接受壽命參數。 該值用於在 ParticleSystemComponent 的內部 Particle 達到其生命週期時將其移除。 使用 Flame Timer 類別追蹤粒子本身內的時間。 它可以透過將其傳遞到相應的 Particle 建構函數來配置為以秒為單位（以微秒精度）表示的雙精度數。

```
Particle(lifespan: .2); // will live for 200ms.
Particle(lifespan: 4); // will live for 4s.
```

也可以使用 setLifespan 方法重置粒子的生命週期，該方法也接受雙精度的浮點秒數。

```
final particle = Particle(lifespan: 2);

// ... after some time.
particle.setLifespan(2) // will live for another 2s.
```

在其生命週期中，粒子會追蹤其存活時間並透過進度獲取器公開它，該獲取器傳回 0.0 到 1.0 之間的值。 該值的使用方式與 Flutter 中 AnimationController 類別的 value 屬性類似

```
final particle = Particle(lifespan: 2.0);

game.add(ParticleSystemComponent(particle: particle));

// Will print values from 0 to 1 with step of .1: 0, 0.1, 0.2 ... 0.9, 1.0.
Timer.periodic(duration * .1, () => print(particle.progress));
```

如果給定粒子支持任何嵌套行為，則生命週期將傳遞給給定粒子的所有後代

# [Built-in particles](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#built-in-particles)

Flame 附帶了一些內建的粒子行為：

* **TranslatedParticle** 透過給定的 Vector2 平移其子項
* **MovingParticle** 在兩個預先定義的 Vector2 之間移動其子項，支援曲線
* **AcceleratedParticle** 允許基於物理的基本效果，例如重力或速度阻尼
* **CircleParticle** 渲染各種形狀和大小的圓
* **SpriteParticle** 在粒子效果中渲染火焰精靈
* **ImageParticle** 在粒子效果中渲染 dart:ui 影像
* **ComponentParticle** 在粒子效果中渲染火焰元件
* **FlareParticle** 在粒子效果中渲染光暈動畫

[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/rendering/particles_example.dart)提供了有關如何一起使用這些行為的更多範例。 所有實作都可以在 Flame 儲存庫的[particles](https://github.com/flame-engine/flame/tree/main/packages/flame/lib/src/particles)資料夾中找到。

# [TranslatedParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#translatedparticle)

只需將底層粒子轉換為渲染畫布中指定的 Vector2。 不改變或改變其位置，在需要改變位置的情況下考慮使用 MovingParticle 或 AcceleratedParticle。 透過平移 ​​Canvas 圖層可以達到相同的效果

```
game.add(
  ParticleSystemComponent(
    particle: TranslatedParticle(
      // Will translate the child Particle effect to the center of game canvas.
      offset: game.size / 2,
      child: Particle(),
    ),
  ),
);
```

# [MovingParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#movingparticle)

在其生命週期內將子粒子在來自 Vector2 和目標 Vector2 之間移動。 透過 CurvedParticle 支援曲線

```
game.add(
  ParticleSystemComponent(
    particle: MovingParticle(
      // Will move from corner to corner of the game canvas.
      from: Vector2.zero(),
      to: game.size,
      child: CircleParticle(
        radius: 2.0,
        paint: Paint()..color = Colors.red,
      ),
    ),
  ),
);
```

# [AcceleratedParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#acceleratedparticle)

一個基本的實體粒子，可讓您指定其初始位置、速度和加速度，並讓更新週期完成其餘的工作。 這三個都被指定為 Vector2，您可以將其視為向量。 它對於基於物理的「爆發」尤其有效，但不限於此。 Vector2值的單位是邏輯px/s。 因此，Vector2(0, 100) 的速度將在遊戲時間內每秒將子粒子移動裝置的 100 個邏輯像素

```
final rnd = Random();
Vector2 randomVector2() => (Vector2.random(rnd) - Vector2.random(rnd)) * 100;

game.add(
  ParticleSystemComponent(
    particle: AcceleratedParticle(
      // Will fire off in the center of game canvas
      position: game.canvasSize/2,
      // With random initial speed of Vector2(-100..100, 0..-100)
      speed: Vector2(rnd.nextDouble() * 200 - 100, -rnd.nextDouble() * 100),
      // Accelerating downwards, simulating "gravity"
      // speed: Vector2(0, 100),
      child: CircleParticle(
        radius: 2.0,
        paint: Paint()..color = Colors.red,
      ),
    ),
  ),
);
```

# [CircleParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#circleparticle)

一個粒子，它使用給定的 Paint 在傳遞的 Canvas 的零偏移處渲染一個圓形。 與 TranslatedParticle、MovingParticle 或 AcceleratedParticle 結合使用以實現所需的定位

```
game.add(
  ParticleSystemComponent(
    particle: CircleParticle(
      radius: game.size.x / 2,
      paint: Paint()..color = Colors.red.withOpacity(.5),
    ),
  ),
);
```

# [SpriteParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#spriteparticle)

允許您將精靈嵌入粒子效果中

```
game.add(
  ParticleSystemComponent(
    particle: SpriteParticle(
      sprite: Sprite('sprite.png'),
      size: Vector2(64, 64),
    ),
  ),
);
```

# [ImageParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#imageparticle)

在粒子樹中渲染給定的 dart:ui 影像

```
// During game initialization
await Flame.images.loadAll(const [
  'image.png',
]);

// ...

// Somewhere during the game loop
final image = await Flame.images.load('image.png');

game.add(
  ParticleSystemComponent(
    particle: ImageParticle(
      size: Vector2.all(24),
      image: image,
    );
  ),
);
```

# [ScalingParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#scalingparticle)

在子粒子的生命週期內將其縮放到 1 到 之間。

```
game.add(
  ParticleSystemComponent(
    particle: ScalingParticle(
      lifespan: 2,
      to: 0,
      child: CircleParticle(
        radius: 2.0,
        paint: Paint()..color = Colors.red,
      )
    );
  ),
);
```

# [SpriteAnimationParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#spriteanimationparticle)

嵌入 SpriteAnimation 的粒子。 預設情況下，對齊 SpriteAnimation 的 stepTime，以便它在粒子生命週期內完全播放。 可以使用alignAnimationTime參數覆寫此行為。

```
final spriteSheet = SpriteSheet(
  image: yourSpriteSheetImage,
  srcSize: Vector2.all(16.0),
);

game.add(
  ParticleSystemComponent(
    particle: SpriteAnimationParticle(
      animation: spriteSheet.createAnimation(0, stepTime: 0.1),
    );
  ),
);
```

# [ComponentParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#componentparticle)

此粒子可讓您在粒子效果中嵌入組件。 元件可以有自己的更新生命週期，並且可以在不同的效果樹中重複使用。 如果您唯一需要的是為某個組件的實例添加一些動態，請考慮將其直接添加到遊戲中，而不需要中間的粒子。

```
final longLivingRect = RectComponent();

game.add(
  ParticleSystemComponent(
    particle: ComponentParticle(
      component: longLivingRect
    );
  ),
);

class RectComponent extends Component {
  void render(Canvas c) {
    c.drawRect(
      Rect.fromCenter(center: Offset.zero, width: 100, height: 100),
      Paint()..color = Colors.red
    );
  }

  void update(double dt) {
    /// Will be called by parent [Particle]
  }
}
```

# [ComputedParticle](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#computedparticle)

在以下情況下可以為您提供幫助的粒子：

* 預設行為還不夠
* 複雜效果優化
* 客製緩動

創建後，它將所有渲染委託給提供的 ParticleRenderDelegate，該 ParticleRenderDelegate 在每個幀上調用以執行必要的計算並將某些內容渲染到 Canvas。

```
game.add(
  ParticleSystemComponent(
    // Renders a circle which gradually changes its color and size during the
    // particle lifespan.
    particle: ComputedParticle(
      renderer: (canvas, particle) => canvas.drawCircle(
        Offset.zero,
        particle.progress * 10,
        Paint()
          ..color = Color.lerp(
            Colors.red,
            Colors.blue,
            particle.progress,
          ),
      ),
    ),
  ),
)
```

# [Nesting behavior](https://docs.flame-engine.org/1.16.0/flame/rendering/particles.html#nesting-behavior)

Flame 的粒子實作遵循與 Flutter 小部件相同的極端組合模式。 這是透過將小塊行為封裝在每個粒子中，然後將這些行為嵌套在一起以實現所需的視覺效果來實現的。

兩個允許粒子相互嵌套的實體是：SingleChildParticle mixin 和 CompositParticle 類別。

SingleChildParticle 可以幫助您建立具有自訂行為的粒子。 例如，在每一幀中隨機定位其子級：

SingleChildParticle 可以幫助您建立具有自訂行為的粒子。

例如，在每一幀中隨機定位其子級：

```
var rnd = Random();

class GlitchParticle extends Particle with SingleChildParticle {
  Particle child;

  GlitchParticle({
    required this.child,
    super.lifespan,
  });

  @override
  render(Canvas canvas)  {
    canvas.save();
    canvas.translate(rnd.nextDouble() * 100, rnd.nextDouble() * 100);

    // Will also render the child
    super.render();

    canvas.restore();
  }
}
```

CompositParticle 可以作為獨立的粒子使用，也可以在現有的粒子樹中使用。