# [Button 按鈕](http://docs.cocos.com/creator/manual/zh/components/button.html)

[cc.Button](http://docs.cocos.com/creator/api/zh/classes/Button.html) 是一個按鈕 其擁有自身的狀態變化 此外 還可以讓用戶完成點擊後 響應一個自定義行爲

![](assets/button.png)

![](assets/button-color.png)


| 屬性 | 說明 |
| -------- | -------- |
| Target     | 當 Button 發送 Transition  時 會修改相應 Target 節點的 SpriteFrame/Color/Scale     |
| interactable     | 如果爲 false 則禁用 Button     |
| enableAutoGrayEffect     | 如果爲true 此時 interactable爲false 則 button 的 sprite Target 會使用內置的 shader變灰    |
| Transition     | 狀態表現     |
| Click Event     | 事件列表     |

# Transition

當 Transition 取值不同時 Button 處於不同狀態 將有不同的 表現

* NONE
* COLOR
* SPRITE 
* SCALE

## COLOR

![](assets/color-transition.png)

| 屬性 | 說明 | 
| -------- | -------- | 
| Normal     | Normal 狀態下 顏色     | 
| Pressed     | Pressed 狀態下 顏色     | 
| Hover     | Hover 狀態下 顏色     | 
| Disabled     | Disabled 狀態下 顏色     | 
| Duration     | 狀態切換需要的時間間隔     | 

## SPRITE

![](assets/sprite-transition.png)


| 屬性 | 說明 | 
| -------- | -------- | 
| Normal     | Normal 狀態下 SpriteFrame     | 
| Pressed     | Pressed 狀態下 SpriteFrame     | 
| Hover     | Hover 狀態下 SpriteFrame     | 
| Disabled     | Disabled 狀態下 SpriteFrame     | 

## SCALE

![](assets/scaleTransition.png)

| 屬性 | 說明 | 
| -------- | -------- | 
| Duration     | 狀態切換需要的時間間隔     | 
| ZoomScale     | 當用戶點擊按鈕後 按鈕會縮放一個值 此值等於 原 scale * ZoomScale     | 


# 事件

目前 Button 只支持 Click 事件

![](assets/button.png)

| 屬性 | 說明 |
| -------- | -------- |
| Target     | 帶有腳本的組件節點     |
| Component     | Target中的組件     |
| Handler     | 組件中定義的回調函數 當用戶點擊時觸發此函數     |
| CustomEventData     | 傳遞給 回調函數的 字符串參數     |

```
const { ccclass, property } = cc._decorator
@ccclass
export default class NewClass extends cc.Component {
    callback(evt: cc.Event, data: string) {
        console.log(evt, data)
        this.node.x += 100
    }
}

```

> this.node 將直接操作 Target 節點 而非 Button 本身
> 

此外 cocos 支持 使用 代碼方式 添加 事件回調 和使用 creator 是相同效果

```
#info="Button.ts"
const { ccclass, property } = cc._decorator
@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Node)
    readonly target: cc.Node = null
    onLoad() {
        const clickEventHandler = new cc.Component.EventHandler()
        clickEventHandler.target = this.node
        clickEventHandler.component = "Button" // 代碼檔案名稱
        clickEventHandler.handler = "callback"
        clickEventHandler.customEventData = "ok"

        const button = this.node.getComponent(cc.Button)
        button.clickEvents.push(clickEventHandler)
    }
    callback(evt: cc.Event, data: any) {
        console.log(evt, data)
        this.target.x += 100
    }
}
```

> 需要將腳本 掛接到 Button 並將目標設置到 腳本屬性 target 

此外 可以使用 node.on 來 註冊 click 事件 但 無法在回調中 獲取對齊點擊屏幕座標

```
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Button)
    readonly button: cc.Button = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.button.node.on("click", this.callback, this)
    }

    callback(button: cc.Button) {
        console.log(button)
        this.node.x += 100
    }
}
```

> 需要將 腳本掛接到 操作目標 並將 Button 設置 到腳本屬性 button