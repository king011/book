# [PlayerPrefs](https://docs.unity3d.com/ScriptReference/PlayerPrefs.html)

class **UnityEngine.PlayerPrefs** 是最簡單的存儲方案，它主要用於在遊戲期間存儲玩家設定，支持 string float int 的存取

不要在 PlayerPrefs 中直接存儲敏感信息，因爲 PlayerPrefs 存儲的數據是未加密的文本，同時也不要存儲大量數據，因爲其效率並不高效(通常其讀寫效率你可以將其理解爲對xml配置檔案的讀寫，實際上在linux上數據就是存儲在一個xml檔案中的)

# API

PlayerPrefs 實際上是一個封裝了的 HashMap 其 key 爲字符串，其 value 可以是 int float string

| 簽名 | 描述 |
| -------- | -------- |
| public static void DeleteAll();     | 刪除所有的數據     |
| public static void DeleteKey(string key);     | 如果 key 存在刪除，否則什麼都不做     |
| public static float GetFloat(string key);     | 如果 key 存在且值爲 float 返回其值，否則返回 0     |
| public static float GetFloat(string key, float defaultValue);     | 如果 key 存在且值爲 float 返回其值，否則返回 defaultValue     |
| public static int GetInt(string key);     | 如果 key 存在且值爲 int 返回其值，否則返回 0     |
| public static int GetInt(string key, int defaultValue);     | 如果 key 存在且值爲 int 返回其值，否則返回 defaultValue     |
| public static string GetString(string key);     | 如果 key 存在且值爲 string 返回其值，否則返回空字符串     |
| public static string GetString(string key, string defaultValue);     | 如果 key 存在且值爲 string 返回其值，否則返回 defaultValue     |
| public static bool HasKey(string key);     | 如果 key 存在返回 true，否則返回 false     |
| public static void Save();     | 立刻將數據保存到磁盤，默認情況下數據會在 OnApplicationQuit 期間被寫入到磁盤     |
| public static void SetFloat(string key, float value);     | 設置 key 的值爲 float value     |
| public static void SetInt(string key, int value);     | 設置 key 的值爲 int value     |
| public static void SetString(string key, string value);     | 設置 key 的值爲 string value     |

**注意** 默認情況下數據會在 OnApplicationQuit 即遊戲退出時被寫入磁盤，這意味着如果遊戲中途崩潰之類的意外發生用戶數據就丟失了，所以推薦的做法是在用戶關閉了遊戲中的設置頁面就調用下 Save 立刻存盤

# 存儲位置

依據不同的平臺 PlayerPrefs 會將數據存儲到不同的位置

| 平臺 | 存儲位置 | 備註 |
| -------- | -------- |
| macOS     | ~/Library/Preferences/com.ExampleCompanyName.ExampleProductName.plist     |
| Windows     | HKCU\\Software\\ExampleCompanyName\\ExampleProductName     | 註冊表的鍵中     |
| Linux     | ~/.config/unity3d/ExampleCompanyName/ExampleProductName     |
| Windows 應用商店     | %userprofile%\\AppData\\Local\\Packages\\\[ProductPackageId\]\\LocalState\\playerprefs\.dat     |
| Windows Phone 8     | [Directory.localFolder](https://docs.unity3d.com/ScriptReference/Windows.Directory-localFolder.html) 中     |
| Android     | /data/data/pkg\-name/shared\_prefs/pkg\-name\.v2\.playerprefs\.xml      | 使用標準的 SharedPreferences 所以任何代碼例如本地代碼都可以通過 SharedPreferences 訪問     |
| WebGL     | [IndexedDB](https://developers.google.com/web/ilt/pwa/lab-indexeddb#overview)     |

在 Editor Play 模式下數據依據 Editor 所處平臺存儲到上表相應位置中