# 快速上手

創建一個 html 頁面用於顯示視頻

```
#info="index.html"
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>video.js</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <!-- 導入 video.js 樣式 和 腳本 -->
    <link href="css/video-js.min.css" rel="stylesheet">
    <script src="js/video.min.js"></script>
    <style>
        /* 爲播放器定義寬度和高度 */
        #player {
            width: 100%;
            height: auto;
            aspect-ratio: 16/9;
        }
    </style>
</head>

<body>
    <!-- 定義視頻元素 class 至少要指定 video-js  -->
    <video id="player" class="video-js vjs-default-skin vjs-big-play-centered">
        <p class="vjs-no-js">
            To view this video please enable JavaScript, and consider upgrading to a
            web browser that
            <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
        </p>
    </video>

    <!-- 導入 腳本 -->
    <script src="dist/bundle.js"></script>
</body>

</html>
```

然後調用 **videojs** 函數，一些屬性也可以在 html 中指定但本咪覺得除了樣式外其它都在 js 中指定

```
import videojs from "video.js"

videojs('player', // html id
    {
        controls: true,
        preload: 'auto',
        autoplay: true,
        poster: 'image/poster.webp', // 指定可選的海邊，如果不指定默認爲全黑
        // loop:true, // 是否循環播放
        sources: [
            {
                src: 'movie/001.mp4',
                type: 'video/mp4'
            },
        ],
    },
    function () {// 可選回調當播放器準備好後會調用此函數
        // this 指向了 VideoJsPlayer 和 videojs 函數的返回值一致
        console.log(`ready ${this.videoWidth()}/${this.videoHeight()} ${this.src()}`)
        this.on('ended', () => {
            // 播放結束
            console.log('ended')
            this.off('ended')
            // 播放下一集
            this.src({
                src: 'movie/002.mp4',
                type: 'video/mp4'
            })
            this.load()
            this.play()?.then(() => {
                console.log(`ready ${this.videoWidth()}/${this.videoHeight()} ${this.src()}`)
            })
        })
    },
)
```

使用 webpack 打包 ts，記得設置 externals 告訴 webpack 不用打包 videojs 將在 html 中直接 import video.js (這樣比較容易設置 videojs 插件)
```
#info="webpack.config.js"
const path = require('path');

module.exports = {
    mode: 'production',
		// mode: 'development',
    entry: './src/main.ts',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512 * 1024,
        maxAssetSize: 1024 * 1024
    },
    devtool: 'source-map',
    externals: {
        'video.js': 'videojs',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: [
                    path.resolve(__dirname, 'node_modules'),
                ],
            },
        ],
    },
    resolve: {
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules'),
        ],
        extensions: ['.ts', '.js'],
    },
}
```