# [log/slog](https://pkg.go.dev/log/slog)

golang 1.21.1 加入了新的日誌庫

slog 分爲了前端 struct Logger 和後端 interface Handler

struct 確保了前端api的穩定，後端 interface Handler 提供了靈活的日誌寫入實現

```
package main

import (
	"log/slog"
	"os"
)

func main() {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		Level: slog.LevelInfo, // 默認等級是 Info
	})))

	slog.Debug(`this is debug`)
	slog.Info(`this is info`)
	slog.Warn(`this is warn`)
	slog.Error(`this is error`)
}
```

# With/WithGroup

* 可以使用 With 方法創建一個新的 Logger 並指定一些 Attr ，新的 Logger 在日誌輸出時會自動添加上這些 Attr
* 此外也可以使用 WithGroup 創建一個新的Logger，新的 Logger 在日誌輸出時會自動爲新的 Attr 加上 Group 名稱

```
package main

import (
	"log/slog"
	"os"
)

func main() {
	l0 := slog.New(slog.NewTextHandler(os.Stdout, nil))
	l0.Info(`normal test`)

	l1 := l0.With(`id`, 1,
		`name`, `king`,
	)
	l1.Info(`l1 test`)

	g0 := l1.WithGroup(`g0`)
	g0.Info(`g0 test`,
		`ac`, 12, // g0.ac=12
	)
}
```

# Handler

Handler 用於完成具體的日誌寫入工作，默認提供了 text/json 的handler 可以使用 NewTextHandler/NewJSONHandler 來創建

不應該直接調用 Handler 提供的方法而是調用 Logger 這樣可以統一前端 api 調用

```
// A Handler handles log records produced by a Logger..
//
// A typical handler may print log records to standard error,
// or write them to a file or database, or perhaps augment them
// with additional attributes and pass them on to another handler.
//
// Any of the Handler's methods may be called concurrently with itself
// or with other methods. It is the responsibility of the Handler to
// manage this concurrency.
//
// Users of the slog package should not invoke Handler methods directly.
// They should use the methods of [Logger] instead.
type Handler interface {
	// Enabled reports whether the handler handles records at the given level.
	// The handler ignores records whose level is lower.
	// It is called early, before any arguments are processed,
	// to save effort if the log event should be discarded.
	// If called from a Logger method, the first argument is the context
	// passed to that method, or context.Background() if nil was passed
	// or the method does not take a context.
	// The context is passed so Enabled can use its values
	// to make a decision.
	Enabled(context.Context, Level) bool

	// Handle handles the Record.
	// It will only be called when Enabled returns true.
	// The Context argument is as for Enabled.
	// It is present solely to provide Handlers access to the context's values.
	// Canceling the context should not affect record processing.
	// (Among other things, log messages may be necessary to debug a
	// cancellation-related problem.)
	//
	// Handle methods that produce output should observe the following rules:
	//   - If r.Time is the zero time, ignore the time.
	//   - If r.PC is zero, ignore it.
	//   - Attr's values should be resolved.
	//   - If an Attr's key and value are both the zero value, ignore the Attr.
	//     This can be tested with attr.Equal(Attr{}).
	//   - If a group's key is empty, inline the group's Attrs.
	//   - If a group has no Attrs (even if it has a non-empty key),
	//     ignore it.
	Handle(context.Context, Record) error

	// WithAttrs returns a new Handler whose attributes consist of
	// both the receiver's attributes and the arguments.
	// The Handler owns the slice: it may retain, modify or discard it.
	WithAttrs(attrs []Attr) Handler

	// WithGroup returns a new Handler with the given group appended to
	// the receiver's existing groups.
	// The keys of all subsequent attributes, whether added by With or in a
	// Record, should be qualified by the sequence of group names.
	//
	// How this qualification happens is up to the Handler, so long as
	// this Handler's attribute keys differ from those of another Handler
	// with a different sequence of group names.
	//
	// A Handler should treat WithGroup as starting a Group of Attrs that ends
	// at the end of the log event. That is,
	//
	//     logger.WithGroup("s").LogAttrs(level, msg, slog.Int("a", 1), slog.Int("b", 2))
	//
	// should behave like
	//
	//     logger.LogAttrs(level, msg, slog.Group("s", slog.Int("a", 1), slog.Int("b", 2)))
	//
	// If the name is empty, WithGroup returns the receiver.
	WithGroup(name string) Handler
}
```
