# markdown

typedoc-plugin-markdown 是 typedoc 的一個開源(MIT)第三方插件，用於創建 markdown 的輸出檔案

* 源碼 [https://github.com/tgreyuk/typedoc-plugin-markdown](https://github.com/tgreyuk/typedoc-plugin-markdown)
* npm [https://www.npmjs.com/package/typedoc-plugin-markdown](https://www.npmjs.com/package/typedoc-plugin-markdown)

```
npm install --save-dev typedoc typedoc-plugin-markdown
```

```
typedoc --plugin typedoc-plugin-markdown --out docs src/index.ts
```

```
#info="關閉插件"
typedoc --plugin none --out docs src/index.ts
```