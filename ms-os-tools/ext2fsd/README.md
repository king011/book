# ext2fsd

ext2fsd 是一個開源(GPL)的驅動程式 爲 windows 平臺提供了存取 ext2/ext3 檔案系統的支持

* 官網 [http://www.ext2fsd.com/](http://www.ext2fsd.com/)
* 源碼 [https://github.com/matt-wu/Ext3Fsd](https://github.com/matt-wu/Ext3Fsd)

# /boot/grub/grub.cfg

以 ubuntu+windows 雙系統下如果要在windows下修改 默認開機順序 

1. 安裝ubuntu 時 將 **/boot** 以 **ext2** 掛載爲單獨分區
2. 使用 ext2fsd 在widnwos 掛載 /boot
3. 修改 /boot/grub/grub.cfg 第29行左右的 **set default** 設置默認開機進入的系統

```
#info=23
if [ "${next_entry}" ] ; then
   set default="${next_entry}"
   set next_entry=
   save_env next_entry
   set boot_once=true
else
   set default="0"
fi
```

![](assets/1.jpg)