# 配置
v2ray 有如下 幾個 主要 配置

* log 定義了v2ray如何輸出日誌
* api 遠程擴展API
* dns 內置DNS服務器
* stats 統計數據相關
* routing 路由
* policy 本地策略
* reverse 反向代理
* inbounds 入站配置
* outbounds 出站配置
* transport 全局的 底層傳輸協議

```js
#info="服務器配置"

{
  "inbounds": [{
    // 服務器開放 端口
    "port": 29800,
    // 使用的 傳輸 協議
    "protocol": "vmess",
    "settings": {
      "clients": [
        {
          // uuid 用於 用戶識別
          "id": "xxx-xxx-xxx-xxx-xxx",
          "level": 1,
          "alterId": 64
        }
      ]
    }
  }],
  "outbounds": [{
    "protocol": "freedom",
    "settings": {}
  },{
    "protocol": "blackhole",
    "settings": {},
    "tag": "blocked"
  }],
  "routing": {
    "rules": [
      {
        "type": "field",
        "ip": ["geoip:private"],
        "outboundTag": "blocked"
      }
    ]
  }
}
```