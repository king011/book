# basic

```go
#info="server"
package main

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"time"

	quic "github.com/lucas-clemente/quic-go"
)

const (
	laddr    = "localhost:1102"
	certFile = "server.pem"
	keyFile  = "server.key"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	// 加載 x509 證書
	cert, e := tls.LoadX509KeyPair(certFile, keyFile)
	if e != nil {
		log.Fatalln(e)
	}

	// 創建 quic 服務器
	l, e := quic.ListenAddr(laddr,
		&tls.Config{
			// 配置 證書
			Certificates: []tls.Certificate{cert},
		},
		&quic.Config{
			// 支持的 quic 版本 如果爲nil則使用 此庫支持的 所有版本
			// 不建議設置此值 官方表示 此值 可能會改變
			Versions: nil,
			// connection id 字節數 可以是 0 或 [4,18]
			ConnectionIDLength: 0,
			// 創建 加密連接 超時時間
			// 如果 爲0 使用 默認值 10秒
			HandshakeTimeout: time.Second * 10,
			// 連接 完成 後(quic.Session 創建) 一定時間內 沒有網路活動 斷開連接
			// 如果 爲0 使用默認值 30秒
			IdleTimeout: time.Second * 30,
			// 僅服務器 有效
			AcceptCookie: nil,
			// 針對 quic.Stream 接收數據 滑動窗口大小
			// 如果爲0 使用默認值
			// 客戶端6M
			// 服務器1M
			MaxReceiveStreamFlowControlWindow: 0,

			// 針對 quic.Session 接收數據 滑動窗口大小
			// 如果爲0 使用默認值
			// 客戶端1.5M
			// 服務器15M
			MaxReceiveConnectionFlowControlWindow: 0,
			// 允許的 最大 雙向流 數量
			// 如果爲 0 使用默認值 100
			// 如果小於0 不允許 雙向流
			MaxIncomingStreams: 100,
			// 允許的 最大 單向流 數量 (客戶端 WriteTo 服務器)
			// 如果爲 0 使用默認值 100
			// 如果小於0 不允許 雙向流
			MaxIncomingUniStreams: 100,
		}, //nil 使用 默認 quic 配置

	)
	if e != nil {
		log.Fatalln(e)
	}
	defer l.Close()
	log.Println("work at", laddr)

	// 接受 新的 quic udp 連接
	for {
		session, e := l.Accept()
		if e != nil {
			log.Println(e)
			break
		}
		go onSession(session)
	}
}

func onSession(session quic.Session) {
	addr := session.RemoteAddr()
	log.Println("new session", addr)
	defer func() {
		session.Close()
		log.Println("close session", addr)
	}()

	// 接受 新的 全雙工雙向流 創建
	for {
		stream, e := session.AcceptStream()
		if e != nil {
			log.Println(e)
			break
		}
		go onStream(stream)
	}
}
func onStream(stream quic.Stream) {
	id := stream.StreamID()
	log.Println("new stream", id)
	defer func() {
		stream.Close()
		log.Println("close stream", id)
	}()

	// 讀寫 流
	r := bufio.NewReader(stream)
	for {
		b, _, e := r.ReadLine()
		if e != nil {
			if e != io.EOF { // 客戶端 關閉流
				log.Println(e)
			}
			break
		}
		str := string(b)
		fmt.Println("id :", str)
		_, e = stream.Write([]byte(str + "\n"))
		if e != nil {
			log.Println(e)
			break
		}
	}
}
```
```go
#info="client"
package main

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"log"
	"strconv"
	"strings"

	quic "github.com/lucas-clemente/quic-go"
)

const (
	laddr = "localhost:1102"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	// 連接 quic 服務器
	session, e := quic.DialAddr(laddr,
		&tls.Config{
			InsecureSkipVerify: true, //忽略 證書驗證
		},
		&quic.Config{
			// 向 服務器 發送 ping 包 以保存 連接 活躍
			KeepAlive: true,
		}, //nil 使用 默認 quic 配置
	)
	if e != nil {
		log.Fatalln(e)
	}
	defer session.Close()

	var cmd string
	ss := make(map[quic.StreamID]quic.Stream)
	for {
		fmt.Print("cmd $>")
		fmt.Scan(&cmd)
		cmd = strings.TrimSpace(cmd)
		if cmd == "" {
			continue
		} else if cmd == "h" {
			fmt.Println(`e	exit
n	new stream
l	show all stream id
s	write and read`)
		} else if cmd == "e" {
			break
		} else if cmd == "n" {
			// 創建 新 雙向流
			stream, e := session.OpenStreamSync()
			if e != nil {
				log.Println(e)
				continue
			}
			id := stream.StreamID()
			ss[id] = stream
			fmt.Println("new stream", id)
		} else if cmd == "l" {
			for id := range ss {
				fmt.Println("stream", id)
			}
		} else if strings.HasPrefix(cmd, "s") {
			var stream quic.Stream
			var id quic.StreamID
			for {
				fmt.Print("stream $>")
				fmt.Scan(&cmd)
				cmd = strings.TrimSpace(cmd)
				if cmd == "b" {
					break
				}
				tmp, e := strconv.ParseUint(cmd, 10, 64)
				if e != nil {
					fmt.Println(e)
					continue
				}
				id = quic.StreamID(tmp)
				stream = ss[id]
				if stream == nil {
					fmt.Println("not found stream", id)
					continue
				}
				break
			}

			for {
				fmt.Print("data $>")
				fmt.Scan(&cmd)
				cmd = strings.TrimSpace(cmd)
				if cmd == "b" {
					break
				} else if cmd == "" {
					continue
				}

				// 發送 數據
				_, e = stream.Write([]byte(cmd + "\n"))
				if e != nil {
					log.Println(e)
					delete(ss, id)
					break
				}
				// 接收 數據
				r := bufio.NewReader(stream)
				b, _, e := r.ReadLine()
				if e != nil {
					log.Println(e)
					delete(ss, id)
					break
				}
				str := string(b)
				if str != cmd {
					log.Println(cmd, "!=", str)
					delete(ss, id)
					break
				}
				fmt.Println("echo success")
				break
			}
		}
	}
	for _, stream := range ss {
		stream.Close()
	}
}
```