# workers

```
// 訪問 worker 使用的域名 
const Domain = "dockerhub.king011.com"
// 要反代的後端域名
const Backend = "github.com"

addEventListener(
    "fetch", (event) => {
        let url = new URL(event.request.url)
        url.hostname = Backend
        url.protocol = "https"
        const request = new Request(url, event.request)
        event.respondWith(fetch(request).then((response) => {
            const hreaders = new Headers(response.headers)
            hreaders.set('access-control-allow-origin', '*')
            hreaders.set('access-control-allow-credentials', "true")
            hreaders.delete('content-security-policy')
            hreaders.delete('content-security-policy-report-only')
            hreaders.delete('clear-site-data')

            const cookies = hreaders.getAll('Set-Cookie')
            hreaders.delete('Set-Cookie')
            for (const cookie of cookies) {
                hreaders.append('Set-Cookie', cookie.replaceAll(Backend, Domain));
            }

            return new Response(response.body, {
                status: response.status,
                headers: hreaders,
            })
        }))
    }
)
```

# dockerhub

dockerhub 服務已經被西朝鮮完全封鎖，導致無法正常 pull 鏡像，這時可以利用 worker 來搭建一個鏡像

注意 鏡像保存在 registry-1.docker.io 域名下 而非 hub.docker.com

```
// 訪問 worker 使用的域名 
const Domain = "your domain"
// 要反代的後端域名
const Backend = "registry-1.docker.io"

addEventListener(
    "fetch", (event) => {
        let url = new URL(event.request.url)
        url.hostname = Backend
        url.protocol = "https"
        const request = new Request(url, {
            method: event.request.method,
            headers: event.request.headers,
            body: event.request.body,
            redirect: "follow",
        })
        event.respondWith(fetch(request).then((response) => {
            const hreaders = new Headers(response.headers)
            hreaders.set('access-control-allow-origin', '*')
            hreaders.set('access-control-allow-credentials', "true")
            hreaders.delete('content-security-policy')
            hreaders.delete('content-security-policy-report-only')
            hreaders.delete('clear-site-data')

            const cookies = hreaders.getAll('Set-Cookie')
            hreaders.delete('Set-Cookie')
            for (const cookie of cookies) {
                hreaders.append('Set-Cookie', cookie.replaceAll(Backend, Domain));
            }

            return new Response(response.body, {
                status: response.status,
                headers: hreaders,
            })
        }))
    }
)
```

最後還需要修改 docker 設置使用鏡像服務器

```
> cat /etc/docker/daemon.json

{
  "registry-mirrors": [
    "https://your domain"
  ]
}
```
