# module

2018-08-24 go 發佈了 1.11 版本 首次實驗性的 加入了 模塊機制 用來 替代 GOPATH 和 vendor 功能

## GO111MODULE

環境變量 GO111MODULE 用來 指定 是否使用 module

* GO111MODULE=auto 項目在 GOPATH 之外 且 根目錄 含有 go.mod 檔案 則 開啓 module
* GO111MODULE=off 關閉 module 支持
* GO111MODULE=on 忽略 GOPATH vendor 只使用 go.mod
* GOPROXY 如果不爲空 則會從 這個變量指定的 地址 進行下載 module GOPROXY 只在 go mod 模式下有用

## GOPROXY
GOPROXY 是供開源項目 爲 go 模塊 提供了 代理 下載module 的 網站 
```
#info=false
export GOPROXY=https://goproxy.io
```

* 源碼 [https://github.com/goproxyio/goproxy](https://github.com/goproxyio/goproxy)

# 定義 模塊
* 模塊根目錄 和其子目錄下的 所有包 構成模塊 
* 根本目錄下 操作 go.mod 檔案 定義了 模塊的 依賴

在模塊根目錄下 執行 **go mod init 包名** 會創建一個 模塊 go.mod 檔案

> 如果 GO111MODULE 使用 auto 模式 則 init 必須在 GOPATH 之外執行

# go get

依然 沿用了 go get 來 處理 模塊包 (包會被下載到 **$GOPATH/pkg/mod**)

```sh
# 升級 所有 包
go get -u

# 升級 指定包
go get github.com/zuiwuchang/gomt-utils

# 升級 指定包 到指定 版本
go get github.com/zuiwuchang/gomt-utils@v0.0.2
```

> 版本號 必須是 v(major).(minor).(patch) 形式的
> * major 通常是 主版本改變 不在兼容
> * minor 通常是 優化代碼 新增api 功能 等
> * patch 通常是 修復了bug
> 

# go.mod go.sum
go.mod 記錄了 模塊的 依賴 可以手動編輯 也可以 使用 go mod 指令操作

```
module gitlab.com/king011/residential-go // 指定包名

go 1.12

// 指定 依賴的 包
require (
	github.com/spf13/cobra v0.0.3 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
)
```

go.sum 記錄了 包的 hash 值 防止 下載到被篡改的包

# replace
在 go.mod  中 可以 使用 replace 指定 一個 包的 下載 地址

可以 通過 可選的 tag 指定 具體 版本

也可以 指定 本地 檔案夾

```
replace (
  golang.org/x/crypto v0.0.0-20180820150726-614d502a4dac => github.com/golang/crypto v0.0.0-20180820150726-614d502a4dac
  golang.org/x/net v0.0.0-20180821023952-922f4815f713 => github.com/golang/net v0.0.0-20180826012351-8a410e7b638d
  golang.org/x/text v0.3.0 => github.com/golang/text v0.3.0
  google.golang.org/grpc => /opt/go/grpc
)
```
