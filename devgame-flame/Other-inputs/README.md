# [其它輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html)

這包括除鍵盤和滑鼠之外的輸入方法的文檔。

其他輸入文檔，另請參閱：

* [手勢輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html)：用於滑鼠和觸控指標手勢
* [鍵盤輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html)：用於擊鍵

# [Joystick](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#joystick)

Flame 提供了一個能夠創建虛擬操縱桿的組件，用於為您的遊戲獲取輸入。 要使用此功能，您需要建立一個 JoystickComponent，按照您想要的方式配置它，並將其添加到您的遊戲中

檢視此範例以更好地理解：

```
class MyGame extends FlameGame {

  @override
  Future<void> onLoad() async {
    super.onLoad();
    final image = await images.load('joystick.png');
    final sheet = SpriteSheet.fromColumnsAndRows(
      image: image,
      columns: 6,
      rows: 1,
    );
    final joystick = JoystickComponent(
      knob: SpriteComponent(
        sprite: sheet.getSpriteById(1),
        size: Vector2.all(100),
      ),
      background: SpriteComponent(
        sprite: sheet.getSpriteById(0),
        size: Vector2.all(150),
      ),
      margin: const EdgeInsets.only(left: 40, bottom: 40),
    );

    final player = Player(joystick);
    add(player);
    add(joystick);
  }
}

class Player extends SpriteComponent with HasGameRef {
  Player(this.joystick)
    : super(
        anchor: Anchor.center,
        size: Vector2.all(100.0),
      );

  /// Pixels/s
  double maxSpeed = 300.0;

  final JoystickComponent joystick;

  @override
  Future<void> onLoad() async {
    sprite = await gameRef.loadSprite('layers/player.png');
    position = gameRef.size / 2;
  }

  @override
  void update(double dt) {
    if (joystick.direction != JoystickDirection.idle) {
      position.add(joystick.relativeDelta  * maxSpeed * dt);
      angle = joystick.delta.screenAngle();
    }
  }
}
```

因此，在此範例中，我們建立類別 MyGame 和 Player。 MyGame 創建一個操縱桿，該操縱桿在創建時傳遞給 Player。 在 Player 類別中，我們根據操縱桿的當前狀態進行操作

操縱桿有幾個字段，這些字段根據其所處的狀態而變化。這些字段應該用於了解操縱桿的狀態：

* **intensity** 強度，旋鈕從中心拖動到操縱桿邊緣的百分比 **[0.0,1.0]**，如果設置了 knobRadius 屬性則最大值爲 **knobRadius**
* **relativeDelta** 百分比(Vector2)，表示旋鈕從中心被拖動的方向和距離
* **delta** 旋鈕從中心被拖動的方向和距離，relativeDelta\*knobRadius

如果您想建立與操縱桿配合的按鈕，請查看 [HudButtonComponent](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#hudbuttoncomponent)

可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/input/joystick_example.dart)找到如何使用它的完整範例。 並且可以看到它在[這裡](https://examples.flame-engine.org/#/Input_Joystick)運行

[這裡](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/input/joystick_advanced_example.dart)還有一個更高級的範例正在[運行](https://examples.flame-engine.org/#/Input_Joystick%20Advanced)

# [HudButtonComponent](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#hudbuttoncomponent)

HudButtonComponent 是一個按鈕，可以定義爲相對 Viewport 邊緣的距離而非絕對位置。它需要兩個 PositionComponents。button 和 buttonDown，第一個按鈕用於空閒顯示，第二個在按鈕按下時顯示。如果你不想在按下時更改按鈕外觀則 buttonDown 是可選的

顧名思義，默認情況下它是跟隨着攝像機四處移動的，既它會在屏幕上保持禁止。可以通過設置 hudButtonComponent.respectCamera = true 將此組件設置爲非 hud

如果相對被按下的按鈕採取行動，你可以傳入 onPressed 和 onReleased 參數設置回調，或者從 HudButtonComponent 派生後去重寫 onTapDown onTapUp onTapCancel

```
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

void main() {
  final myGame = MyGame();

  runApp(GameWidget(
    game: myGame,
  ));
}

class MyGame extends FlameGame with HasTappables {
  @override
  Future<void> onLoad() async {
    await super.onLoad();

    add(HudButtonComponent(
        button: CircleComponent(radius: 100),
        buttonDown: RectangleComponent(size: Vector2.all(100 * 2)),
        margin: const EdgeInsets.only(
          right: 40,
          bottom: 40,
        ),
        onPressed: () {
          debugPrint("onPressed");
        }));
  }
}
```

本喵測試時發現，只要按下即 onTapDown 時就會回調 onPressed，但本喵覺得按鈕應該 onTapDown 並且在區域內 onTapUp 才應該調用 onPressed，這或許是 bug 也或許是設計風格不同吧


# [SpriteButtonComponent](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#spritebuttoncomponent)

SpriteButtonComponent 通過兩個 Sprite 來定義按鈕 類似 HudButtonComponent

**注意** SpriteButtonComponent 位置不是相對 Viewport 的

# [ButtonComponent](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#buttoncomponent)

ButtonComponent 類似 SpriteButtonComponent 區別是現在通過兩個 Component 來定義按鈕。如果你只想通過 Sprite 定義按鈕則改用 SpriteButtonComponent，如果想將 SpriteAnimationComponent 或其它非 Sprite 用在按鈕則使用 ButtonComponent

# [Gamepad](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#gamepad)

Flame 有一個單獨的插件來支持外部遊戲控制(遊戲手柄)，查看[這裏](https://github.com/flame-engine/flame_gamepad)了解更多信息

# [AdvancedButtonComponent](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#advancedbuttoncomponent)

AdvancedButtonComponent 對於每個不同的指標階段都有單獨的狀態。 可以為每個狀態自訂皮膚，並且每個皮膚都由 PositionComponent 表示

這些欄位可用於自訂按鈕元件的外觀：

* **defaultSkin**: 預設情況下將顯示在按鈕上的組件
* **downSkin**: 單擊或點擊按鈕時顯示的組件
* **hoverSkin**: 當按鈕懸停時顯示的組件。 （桌面和網頁）
* **defaultLabel**: 顯示在皮膚頂部的組件。 自動對齊到中心
* **disabledSkin**: 按鈕禁用時顯示的組件
* **disabledLabel**: 當按鈕被停用時，組件顯示在皮膚頂部

# [ToggleButtonComponent](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#togglebuttoncomponent)

ToggleButtonComponent 是一個 AdvancedButtonComponent ，可以在選取和未選取之間切換

除了現有的皮膚外，[ToggleButtonComponent] 還包含以下皮膚：

* **defaultSelectedSkin**: 選擇按鈕時要顯示的組件
* **downAndSelectedSkin**: 選擇並按下可選按鈕時顯示的組件
* **hoverAndSelectedSkin**: 將滑鼠懸停在可選擇和選取的按鈕上（桌面和網頁）
* **disabledAndSelectedSkin**: 當按鈕被選取且處於停用狀態時
* **defaultSelectedLabel**: 選擇按鈕時，組件顯示在外觀頂部

# [IgnoreEvents mixin](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html#ignoreevents-mixin)

如果您不希望元件子樹接收事件，可以使用 IgnoreEvents mixin。 新增此 mixin 後，您可以透過設定ignoreEvents = true（新增 mixin 時的預設值）來關閉到達元件及其後代的事件，然後當您想要再次接收事件時將其設為 false

這樣做可以用於最佳化目的，因為所有事件目前都經過整個元件樹