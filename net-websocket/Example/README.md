# Go Client

```
package main

import (
	"bufio"
	"context"
	"crypto/rand"
	"crypto/sha1"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net"
	"net/textproto"
	"net/url"
	"strings"
	"unsafe"
)

func BytesToString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func StringToBytes(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(
		&struct {
			string
			Cap int
		}{s, len(s)},
	))
}

type Dialer interface {
	DialContext(ctx context.Context, network, addr string) (net.Conn, error)
}
type Client struct {
	conn  net.Conn
	read  *bufio.Reader
	write *bufio.Writer
}

func Connect(ctx context.Context, uri string, config *tls.Config) (c *Client, e error) {
	e = ctx.Err()
	if e != nil {
		return
	}
	// 解析 連接地址 組件握手消息
	d, addr, handshake, key, e := parseHandshake(uri, config)
	if e != nil {
		return
	}
	// 創建連接
	conn, e := d.DialContext(ctx, `tcp`, addr)
	if e != nil {
		return
	}
	// 執行握手
	client := &Client{conn: conn}
	ch := make(chan error, 1)
	go func() {
		// 發送握手請求
		_, e := conn.Write(StringToBytes(handshake))
		if e != nil {
			ch <- e
			return
		}
		// 接收握手消息
		e = client.handshake(key)
		if e != nil {
			ch <- e
			return
		}
		close(ch)
	}()
	// 等待握手結果
	select {
	case <-ctx.Done():
		client.Close()
		e = ctx.Err()
	case e = <-ch:
		if e == nil {
			c = client
			c.write = bufio.NewWriter(c.conn)
		} else {
			client.Close()
		}
	}
	return
}
func parseHandshake(uri string, config *tls.Config) (d Dialer, addr, handshake, key string, e error) {
	// 解析 url 組建握手包
	u, e := url.ParseRequestURI(uri)
	if e != nil {
		return
	}
	var (
		scheme string
	)
	switch u.Scheme {
	case `ws`:
		scheme = `http`
		port := u.Port()
		if port == `` {
			addr = u.Host + `:80`
		} else {
			addr = u.Host
		}
		d = &net.Dialer{}
	case `wss`:
		scheme = `https`
		port := u.Port()
		if port == `` {
			addr = u.Host + `:443`
		} else {
			addr = u.Host
		}
		d = &tls.Dialer{
			Config: config,
		}
	default:
		e = errors.New(`scheme not supported: ` + u.Scheme)
		return
	}
	if e != nil {
		return
	}
	path := u.Path
	if path == `` {
		path = `/`
	}
	b := make([]byte, 16)
	io.ReadFull(rand.Reader, b)
	key = base64.StdEncoding.EncodeToString(b)
	handshake = fmt.Sprintf(`GET %s HTTP/1.1
Host: %s
User-Agent: ws-example-client/1.1
Origin: %s://%s
Connection: Upgrade
Upgrade: websocket
Sec-WebSocket-Version: 13
Sec-WebSocket-Key: %s

`,
		path,
		u.Host,
		scheme, u.Host,
		key,
	)
	return
}

func (c *Client) handshake(key string) (e error) {
	buf := bufio.NewReader(c.conn)
	r := textproto.NewReader(buf)
	s, e := r.ReadLine()
	if e != nil {
		return
	}
	strs := strings.SplitN(s, " ", 3)
	if len(strs) < 2 {
		e = errors.New(`server unknow protocol`)
		return
	}
	if strs[1] != `101` { // 升級失敗
		e = errors.New(s)
		return
	}

	// 讀取 header
	var (
		k      string
		accept string
	)
	for {
		s, e = r.ReadLine()
		if e != nil {
			break
		}
		if s == `` {
			break
		}
		strs = strings.SplitN(s, ": ", 2)
		k = strings.ToLower(strs[0])
		switch k {
		case "connection":
			if len(strs) != 2 || strs[1] != "Upgrade" {
				e = errors.New(`unexpected connection: ` + s)
				return
			}
		case "upgrade":
			if len(strs) != 2 || strs[1] != "websocket" {
				e = errors.New(`unexpected upgrade: ` + s)
				fmt.Println(strs)
				return
			}
		case "sec-websocket-accept":
			if len(strs) != 2 {
				e = errors.New(`unexpected sec-websocket-accept: ` + s)
				return
			}
			accept = strs[1]
		}
	}
	if accept == `` {
		e = errors.New(`not found header: sec-websocket-accept`)
		return
	}
	key += `258EAFA5-E914-47DA-95CA-C5AB0DC85B11`
	b := sha1.Sum(StringToBytes(key))
	if base64.StdEncoding.EncodeToString(b[:]) != accept {
		e = errors.New(`sec-websocket-accept value invalid`)
		return
	}
	c.read = buf
	return
}
func (c *Client) Close() error {
	return c.conn.Close()
}
```