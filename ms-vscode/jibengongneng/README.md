# 基本設置

```json
{
    // 控制字型大小 (以像素為單位)。
    "editor.fontSize": 20,
 
    // go 開發設置
    "go.buildOnSave": true,
    "go.lintOnSave": true,
    "go.vetOnSave": true,
    "go.buildFlags": [],
    "go.lintFlags": [],
    "go.vetFlags": [],
    "go.useCodeSnippetsOnFunctionSuggest": false,
    "go.formatOnSave": true,
    "go.formatTool": "gofmt",
 
    // python 設置
    "python.pythonPath": "python3",
    "python.formatting.formatOnSave": true,
		
    // 允許 json 添加 註釋
    "files.associations": {
        "*.json": "jsonc"
    },
}
```

# 命令面板

vscode 提供了高效的命令操作 模式 使用 ctrl+p 可以打開命令面板

| 命令 | 快捷鍵 | 功能 |
| -------- | -------- | -------- |
| &gt;     |      | 輸入命令     |
| ?     |      | 顯示幫助     |
| \!     |      | 顯示錯誤 警告     |
| :     | ctrl+g     | 跳轉到指定行     |
| @     | ctrl+shift+o     | 查找符號     |
| @:     |      | 跳轉到符號     |
| \#     |      | 尋找符號     |

# 快捷鍵

| 快捷鍵 | 功能 |
| -------- | -------- |
| f11     | 切換 全屏     | 
| ctrl+b     | 切換 左上角 面板     | 
| ctrl+shift+y    | 切換 控制檯     | 
| ctrl+shift+d     | 顯示 debug面板     | 
| ctrl+p     | 顯示命令面板     | 


## 自定義 快捷鍵
```json
// 將您的按鍵組合放入此檔案中以覆寫預設值
[
    { 
        "key": "ctrl+k ctrl+i",         
        "command": "editor.debug.action.showDebugHover",
        "when": "editorTextFocus && inDebugMode" 
    },
    {
        "key": "f8",
        "command": "workbench.action.debug.continue",
        "when": "inDebugMode" 
    },
 
    { 
        "key": "f6",                   
        "command": "workbench.action.debug.pause",
        "when": "inDebugMode" 
    },
    { 
        "key": "ctrl+shift+f8",        
        "command": "workbench.action.debug.restart",
        "when": "inDebugMode" 
    },
    { 
        "key": "ctrl+f8",               
        "command": "workbench.action.debug.run",
        "when": "!inDebugMode" 
    },
    { 
        "key": "f8",                  
        "command": "workbench.action.debug.start",
        "when": "!inDebugMode" 
    },
    { 
        "key": "ctrl+f7",             
        "command": "workbench.action.debug.stepOut",
        "when": "inDebugMode" 
    },
    { 
        "key": "f7",                 
        "command": "workbench.action.debug.stepOver",
        "when": "inDebugMode" 
    },
    { 
        "key": "shift+f8",             
        "command": "workbench.action.debug.stop",
        "when": "inDebugMode" 
    },
    { 
        "key": "shift+f7",                   
        "command": "workbench.action.debug.stepInto",
        "when": "inDebugMode" 
    },
    { 
        "key": "f5",                    
        "command": "editor.debug.action.toggleBreakpoint",
        "when": "editorTextFocus" 
    },
    { 
        "key": "shift+f5",             
        "command": "editor.debug.action.toggleColumnBreakpoint",
        "when": "editorTextFocus" 
    }
]
```
## 窗口 編輯

| 快捷鍵 | 功能 |
| -------- | -------- |
| ctrl+shift+n     | 打開新窗口     |
| ctrl+shift+w     | 關閉窗口     |
| ctrl+\\     | 新建一個編輯器(最多3個)     |
| ctrl+1/2/3     | 在3個編輯器中切換     |


