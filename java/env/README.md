# 安裝開發環境
```sh
#info=false
sudo apt install openjdk-8-jdk
```

# 常用 命令

```
#info=false
# 顯示 java 版本信息
java -version

# 在 CLASSPATH 中 搜索 demo/App.class 並執行
java demo.App

# 傳入 路徑 執行 jar 中的 Main-Class
java -jar xxx.jar

# 傳入 路徑 執行 jar 中 指定 class 的 main 函數
java -cp xxx.jar xxx.xxx
```

-jar 執行 的jar 必須包含 **META-INF/MANIFEST.MF** 檔案 並且 在裏面指定了 **Main-Class**
