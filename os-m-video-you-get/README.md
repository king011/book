# you-get

you-get 是使用 python 寫的一個開源(MIT)網頁視頻下載工具，可以下載網頁上的在線視頻

* 官網 [https://you-get.org/](https://you-get.org/)
* 源碼 [https://github.com/soimort/you-get/](https://github.com/soimort/you-get/)
* 支持網站 [https://you-get.org/#supported-sites](https://you-get.org/#supported-sites)