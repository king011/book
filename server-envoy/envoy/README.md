# [envoy](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#bootstrap-proto)

這是 envoy 的最頂層配置

```typescript
/**
 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#bootstrap-proto}
 */
export interface Envoy {
    /**
     * 用於標識節點身份讓管理服務器可以識別 envoy 節點(例如在生成的 headers 中)
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-node}
     */
    node?: config.core.v3.Node
    /**
     * 指定靜態資源
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-bootstrap-staticresources}
     */
    static_resources?: config.bootstrap.v3.Bootstrap.StaticResources
    /**
     * 使用 xDS 配置動態資源
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#config-bootstrap-v3-bootstrap-dynamicresources}
     */
    dynamic_resources?: config.bootstrap.v3.Bootstrap.DynamicResources
    /**
     * 擁有服務器內所有上游集群的集群管理器的配置
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-clustermanager}
     */
    cluster_manager?: config.bootstrap.v3.ClusterManager
    /**
     * 健康發現服務配置選項
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-apiconfigsource}
     */
    hds_config?: config.core.v3.ApiConfigSource
    /**
     * 用於搜索啟動標誌文件的可選文件系統路徑
     */
    flags_path?: string
    /**
     * 可選的統計接收器
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/metrics/v3/stats.proto#envoy-v3-api-msg-config-metrics-v3-statssink}
     */
    stats_sinks?: Array<config.metrics.v3.StatsSink>
    /**
     * 統計數據內部處理的配置
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/metrics/v3/stats.proto#envoy-v3-api-msg-config-metrics-v3-statsconfig}
     */
    stats_config?: config.metrics.v3.StatsConfig
    /**
     * 刷新到配置的統計接收器之間的可選持續時間。 
     * 
     * @remarks
     * 出於性能原因，Envoy 鎖存計數器並且僅定期刷新計數器和儀表。
     *  如果未指定，默認值為 5000 毫秒（5 秒）。 
     * 只能設置 stats_flush_interval 或 stats_flush_on_admin 之一。 
     * 持續時間必須至少為 1 毫秒，最多為 5 分鐘。
     * 
     * @defaultValue 5000
     * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration}
     */
    stats_flush_interval?: number
    /**
     * 僅當在管理界面上查詢時才將統計信息刷新到接收器
     * @remarks
     * 如果設置，則不會創建刷新計時器。 
     * 只能設置 stats_flush_on_admin 或 stats_flush_interval 之一。
     */
    stats_flush_on_admin?: boolean
    /**
     * 可選看門狗配置。 這是針對整個系統的單個看門狗配置。 棄用了具有更細粒度的看門狗。
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-watchdog}
     */
    watchdog?: config.bootstrap.v3.Watchdog
    /**
     * 可選的看門狗配置。 這用於為不同的子系統指定不同的看門狗
     * {@linkhttps://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-watchdogs}
     */
    watchdogs?: config.bootstrap.v3.Watchdogs
    /**
     * 外部跟踪提供程序的配置
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/trace/v3/http_tracer.proto#envoy-v3-api-msg-config-trace-v3-tracing}
     */
    tracing?: config.trace.v3.Tracing
    /**
     * 運行時配置提供程序的配置。 如果未指定，將使用 null 提供程序，這將導致使用所有默認值。
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-layeredruntime}
     */
    layered_runtime?: config.bootstrap.v3.LayeredRuntime
    /**
     * 本地管理 HTTP 服務器的配置
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-admin}
     */
    admin?: config.bootstrap.v3.Admin
    /**
     * 可選的過載管理器配置
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/overload/v3/overload.proto#envoy-v3-api-msg-config-overload-v3-overloadmanager}
     */
    overload_manager?: config.overload.v3.OverloadManager
    /**
     * 為事件調度器啟用統計信息，默認為 false
     * @remarks
     * 請注意，這會為每個線程上的事件循環的每次迭代記錄一個值。
     *  這通常應該是最小的開銷，但是當使用 statsd 時，它將通過線路單獨發送每個觀察值，
     * 因為 statsd 協議沒有任何方式來表示直方圖摘要。 請注意，這可能是一個非常大的數據量。
     */
    enable_dispatcher_stats?: boolean
    /**
     * 可選字符串，將用於代替 x-envoy 作為前綴標題
     * @remarks
     * 
     * 例如，如果此字符串存在並設置為 X-Foo，則 x-envoy-retry-on 將轉換為 x-foo-retry-on 等。
     * 請注意，這適用於 Envoy 將生成的標頭，Envoy 將清理的標頭，以及 Envoy 將僅信任核心代碼和核心擴展的標頭。 
     * 對此字符串進行更改時要非常小心，尤其是在多層 Envoy 部署或使用非上游擴展的部署中
     */
    header_prefix?: string
    /**
     * UInt64Value 可選的代理版本，如果指定，將用於設置 server.version 統計信息的值
     * @remarks
     * Envoy 不會處理這個值，它會按原樣發送到統計接收器。
     * {@link https://protobuf.dev/reference/protobuf/google.protobuf/#uint64-value}
     */
    stats_server_version_override?: string | bigint
    /**
     * @deprecated
     */
    use_tcp_for_dns_lookups?: boolean
    /**
     * config.core.v3.DnsResolutionConfig
     * @deprecated
     */
    dns_resolution_config?: any
    /**
     * DNS 解析器類型配置擴展
     * @remarks
     * 此擴展可用於配置 c-ares、apple 或任何其他 DNS 解析器類型和相關參數
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig}
     */
    typed_dns_resolver_config?: config.core.v3.TypedExtensionConfig
    /**
     * 指定要在啟動時實例化的可選引導程序擴展。 每個項目都包含擴展特定的配置
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig}
     */
    bootstrap_extensions?: Array<config.core.v3.TypedExtensionConfig>
    /**
     * 指定在啟動時實例化並在崩潰時根據導致崩潰的請求調用的可選擴展
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-fatalaction}
     */
    fatal_actions?: Array<config.bootstrap.v3.FatalAction>
    /**
     * 套接字接口
     * @remarks
     * 該值必須是通過引導程序擴展初始化的套接字接口工廠之一的名稱
     */
    default_socket_interface?: string
    /**
     * 指定一組需要註冊為內聯 header 的 headers
     * @remarks
     * 此配置允許用戶在 Envoy 啟動時按需自定義內聯標頭，而無需修改 Envoy 的源代碼
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-custominlineheader}
     */
    inline_headers?: Array<config.bootstrap.v3.CustomInlineHeader>
    /**
     * "Perfetto" SDK 以二進制 ProtoBuf 格式創建的具有性能跟踪數據的文件的可選路徑
     * @defaultValue "envoy.pftrace"
     */
    perf_tracing_file_path?: string
    /**
     * 正則表達式引擎。 如果未指定該值，則默認使用 Google RE2。
     * 
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig}
     */
    default_regex_engine?: config.core.v3.TypedExtensionConfig
    /**
     * 可選的 XdsConfigTracker 配置，它允許跟踪外部組件中的 xDS 響應，例如外部跟踪器或監視器
     * @remarks
     * 它提供了接收、攝取或無法處理 xDS 資源和消息時的處理點。
     *  如果未指定值，則不會使用 XdsConfigTracker。
     * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig}
     */
    xds_config_tracker_extension?: config.core.v3.TypedExtensionConfig
}
```
## node

([config.core.v3.Node](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-node)) 標識特定的 Envoy 實例。 節點標識符被呈現給管理服務器，管理服務器可以使用此標識符來區分每個 Envoy 配置以進行服務

```typescript
interface Node {
		/**
		 * Envoy 節點的不透明標識符，這也提供本地服務節點的名稱
		 * 
		 * @remarks
		 * 如果使用 statsd, CDS, HTTP following 中的任意功能則需要設置這個值
		 * 
		 * 也可以在命令行中爲 envoy 傳入 --service-node 參數來指定
		 */
		id?: string
		/**
		 * 定義運行 Envoy 的本地服務集羣名稱
		 * 
		 * 如果使用 statsd, health check cluster verification, runtime override directory, user agent addition, HTTP global rate limiting, CDS, HTTP tracing 中任意功能則需要設置此值
		 * 
		 * 也可以在命令行中爲 envoy 傳入 --service-cluster 參數來指定
		 */
		cluster?: string
		/**
		 * 擴展節點標識符的不透明元素。Envoy 會將值直接傳遞給管理服務器
		 * 
		 * {@link https://protobuf.dev/reference/protobuf/google.protobuf/#struct}
		 */
		metadata?: any

		/**
		 * Record<string,xds.core.v3.ContextParams>
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/xds/core/v3/context_params.proto#envoy-v3-api-msg-xds-core-v3-contextparams}
		 */
		dynamic_parameters?: Record<string, any>
		/**
		 * 指示 Envoy 服務器運行的位置
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-locality}
		 */
		locality?: Locality
		/**
		 * 請求的自定義標識符字符串。例如 "envoy" , "grpc"
		 */
		user_agent_name?: string
		/**
		 * 請求的自定義標識符字符串。例如 "1.12.2", "abcd1234", "SpecialEnvoyBuild"
		 * 
		 * @remarks
		 * user_agent_version 和 user_agent_build_version 不能同時設置
		 * 
		 */
		user_agent_version?: string
		/**
		 * 請求配置的實體結構化版本
		 * 
		 * @remarks
		 * user_agent_version 和 user_agent_build_version 不能同時設置
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-buildversion}
		 */
		user_agent_build_version?: BuildVersion
		/**
		 * 節點支持的擴展列表及其版本
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-extension}
		 */
		extensions?: Array<Extension>
		/**
		 * 客戶端功能支持列表。這些是 Envoy API 存儲庫中針對給定 API 主要版本描述的衆所周知的功能。客戶端功能使用反向 DNS 命名方案，例如 com.acme.feature。請參閱 xDS 客戶端可能支持的功能列表
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api/client_features#client-features}
		 */
		client_features?: Array<string>
		/**
		 * 節點上的已知監聽器端口作爲對管理服務器的一般提示，用於過濾要返回的監聽器。
		 * 
		 * @remarks
		 * 例如，如果有一個監聽器 bind 到 80 端口，則該列表可以選擇包含 SocketAddress(0.0.0.0,80)。這是一個可選字段，它只是一個提示
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-address}
		 */
		listening_addresses?: Array<Address>
}
```

## static\_resources

([config.bootstrap.v3.Bootstrap.StaticResources](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-bootstrap-staticresources)) 指定靜態資源

```typescript
interface StaticResources {
		/**
		 * 靜態監聽器。 無論 LDS 配置如何，這些偵聽器都可用。
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/listener/v3/listener.proto#envoy-v3-api-msg-config-listener-v3-listener config.listener.v3.Listener}
		 */
		listeners?: Array<config.listener.v3.Listener>
		/**
		 * 如果為 cds_config 指定了基於網絡的配置源，則有必要提供一些初始集群定義，以允許 Envoy 知道如何與管理服務器通信。 這些集群定義可能不使用 EDS（即它們應該是靜態 IP 或基於 DNS）
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/cluster/v3/cluster.proto#envoy-v3-api-msg-config-cluster-v3-cluster config.cluster.v3.Cluster}
		 */
		clusters?: Array<config.cluster.v3.Cluster>
		/**
		 * SdsSecretConfig 可以使用這些靜態 secrets
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/transport_sockets/tls/v3/secret.proto#envoy-v3-api-msg-extensions-transport-sockets-tls-v3-secret extensions.transport_sockets.tls.v3.Secret}
		 */
		secrets?: Array<extensions.transport_sockets.tls.v3.Secret>
}
```
## dynamic\_resources

([config.bootstrap.v3.Bootstrap.DynamicResources](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-bootstrap-dynamicresources)) 使用 xDS 配置動態資源

```typescript
interface DynamicResources {
		/**
		 * 所有監聽器都由單個  LDS 配置源提供
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-configsource config.core.v3.ConfigSource}
		 */
		lds_config?: config.core.v3.ConfigSource
		/**
		 * 所有的後端集群由單個  CDS 配置源提供
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-configsource config.core.v3.ConfigSource}
		 */
		cds_config?: config.core.v3.ConfigSource
		/**
		 * 可以選擇性地指定單個 ADS 源。 這必須具有 api_type GRPC。 
		 * 只有設置了廣告字段的 ConfigSources 才會在 ADS 頻道上流式傳輸。
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-apiconfigsource config.core.v3.ApiConfigSource}
		 */
		ads_config?: config.core.v3.ApiConfigSource
}
```


## layered\_runtime

([config.bootstrap.v3.LayeredRuntime](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-layeredruntime)) 運行時配置提供程序的配置。 如果未指定，將使用 null 提供程序，這將導致使用所有默認值

## admin

([config.bootstrap.v3.Admin](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-msg-config-bootstrap-v3-admin)) 以 HTTP 提供的管理服務器配置

```typescript
interface Admin {
		/**
		 * 管理服務器的日誌配置
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/accesslog/v3/accesslog.proto#envoy-v3-api-msg-config-accesslog-v3-accesslog}
		 */
		access_log?: Array<config.accesslog.v3.AccessLog>
		/**
		 * 管理服務器的 cpu 分析器輸出路徑
		 * 
		 * @defaultValue '/var/log/envoy/envoy.prof'
		 */
		profile_path?: string
		/**
		 * 管理服務器將偵聽的 TCP 地址。
		 * 如果未指定，Envoy 將不會啟動管理服務器。
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-address}
		 */
		address: config.core.v3.Address
		/**
		 * Envoy 源代碼或預編譯二進製文件中可能不存在的其他套接字選項
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/socket_option.proto#envoy-v3-api-msg-config-core-v3-socketoption}
		 */
		socket_options: config.core.v3.SocketOption
		/**
		 * 指示 global_downstream_max_connections 是否應該應用於管理界面
		 */
		ignore_global_conn_limit: boolean
}
```
