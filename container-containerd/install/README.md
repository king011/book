# ubuntu

```
sudo apt install containerd
```

# nerdctl

[nerdctl](https://github.com/containerd/nerdctl) 是爲 containerd 實現的兼容 docker 指令的 cli 工具，其使用方法基本上和 docker 一致，只需將 docker 命令改爲 nerdctl 即可

[安裝](https://github.com/containerd/nerdctl/releases/tag/v0.22.2)
```
sudo bash -c "set -eux; \
	curl -#Lo linux-amd64.tar.gz  https://github.com/containerd/nerdctl/releases/download/v0.22.2/nerdctl-0.22.2-linux-amd64.tar.gz; \
	tar -zxvf linux-amd64.tar.gz -C /usr/bin; \
	rm linux-amd64.tar.gz"
```


[添加命令補齊](https://github.com/containerd/nerdctl#nerd_face-nerdctl-completion-bash)
```
sudo bash -c "nerdctl completion bash > /etc/bash_completion.d/nerdctl"
```

## cni

如果要使用網路需要安裝 [cni](https://github.com/containernetworking/plugins) 插件

```
sudo bash -c "set -eux; \
	curl -#Lo linux-amd64.tar.gz  https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-amd64-v1.1.1.tgz; \
	mkdir /opt/cni/bin -p;\
	tar -zxvf linux-amd64.tar.gz -C /opt/cni/bin; \
	rm linux-amd64.tar.gz"
```

## compose

nerdctl 也兼容 docker-compose 用法也基本保存一致只是使用 **nerdctl compose** 替代 **docker-compose** 指令即可