# 理解 OAuth2

OAuth2 主要用於將資源安全的授權給第三方使用 其中存在幾個概念名詞

* **resource owner** 資源的所有者，通常就是用戶
* **resource server** 資源服務器，既服務提供商存放用戶資源的服務器
* **client** 客戶端，需要得到資源授權的應用程序，例如第三方網站
* **authorization server** 認證服務器，既服務提供商專門用來處理認證的服務器
* **user-agent** 用戶代理，用戶用來訪問客戶端的程序，典型的就是瀏覽器

例如 我(resource owner) 需要用 gmail(resource server) 在瀏覽器(user-agent) 上登入 github(client)，我們先登入 gmail(authorization server)，授權給 github，然後 github 去 gmail資源服務器獲取用戶信息

[RFC6749](https://datatracker.ietf.org/doc/html/rfc6749) 定義如下

```
+--------+                               +---------------+
|        |--(A)- Authorization Request ->|   Resource    |
|        |                               |     Owner     |
|        |<-(B)-- Authorization Grant ---|               |
|        |                               +---------------+
|        |
|        |                               +---------------+
|        |--(C)-- Authorization Grant -->| Authorization |
| Client |                               |     Server    |
|        |<-(D)----- Access Token -------|               |
|        |                               +---------------+
|        |
|        |                               +---------------+
|        |--(E)----- Access Token ------>|    Resource   |
|        |                               |     Server    |
|        |<-(F)--- Protected Resource ---|               |
+--------+                               +---------------+

                Figure 1: Abstract Protocol Flow
```

* A 客戶端請求 resource owner 授權

	這種授權可以是直接向 resource owner 請求，也可以通過 authorization server 間接請求
	
* B 用戶同意授權

* C 客戶端拿着上一步的授權向 authorization server 申請令牌

* D authorization server 確認授權後，發放 access_token

* E 客戶端拿着 access_token 到 resource server 去獲取資源

* F resource server 確認 access_token 後，向客戶端發放受保護的資源

# 授權模式

客戶端要得到 access_token，必須得到用戶的授權，在 OAuth2 中定義了四種授權模式

1. 授權碼模式 Authorization Code
2. 簡化模式 Implicit
3. 密碼模式 Resource Owner Password Credentials
4. 客戶端模式 Client Credentials

# 授權碼模式

授權碼模式的授權流程是基於重定向，是功能最完整 流程最嚴密的模式

```
 +----------+
  | Resource |
  |   Owner  |
  |          |
  +----------+
       ^
       |
      (B)
  +----|-----+          Client Identifier      +---------------+
  |         -+----(A)-- & Redirection URI ---->|               |
  |  User-   |                                 | Authorization |
  |  Agent  -+----(B)-- User authenticates --->|     Server    |
  |          |                                 |               |
  |         -+----(C)-- Authorization Code ---<|               |
  +-|----|---+                                 +---------------+
    |    |                                         ^      v
   (A)  (C)                                        |      |
    |    |                                         |      |
    ^    v                                         |      |
  +---------+                                      |      |
  |         |>---(D)-- Authorization Code ---------'      |
  |  Client |          & Redirection URI                  |
  |         |                                             |
  |         |<---(E)----- Access Token -------------------'
  +---------+       (w/ Optional Refresh Token)

Note: The lines illustrating steps (A), (B), and (C) are broken into
two parts as they pass through the user-agent.

                  Figure 3: Authorization Code Flow
```

* A 用戶訪問客戶端，客戶端將用戶重定向到認證服務器
* B 用戶選擇是否授權
* C 如果用戶同意授權，認證服務器重定向到客戶端事先指定的**重定向地址**，並且帶上授權碼(code)
* D 客戶端收到授權碼，附上前面的**重定向地址**，向認證服務器申請訪問令牌
* E 認證服務器核對了授權碼和重定向地址後，向客戶端發送訪問令牌 和 更新令牌(可選)

## 步驟 A
在步驟 A 中，客戶端授權授權，重定向到認證服務器的URI中需要包含如下參數


| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| response_type     | 授權類型，此模式應該爲 code     | 必須     |
| client_id     | 客戶端ID，客戶端到資源服務器註冊的ID     | 必須     |
| redirect_uri     | 重定向URI     | 可選     |
| scope     | 申請的授權範圍，多個以逗號隔開     | 可選     |
| state     | 客戶端當前狀態，可以是任何值，認證服務器原封不動的返回     | 推薦     |

```
GET /authorize?response_type=code&client_id=s6BhdRkqt3&state=xyz
     &redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1
 Host: server.example.com
```

## 步驟 C
在步驟 C 中，認證服務器返回的 URI 中包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| code     | 授權碼。認證服務器返回的授權碼，生命週期通常爲10分鐘，且只能使用一次和 步驟 A 中的 client_id redirect_uri 綁定     | 必須     |
| state     | 如果步驟 A 包含這個參數，資源服務器原封不動的返回     | 可選     |

```
HTTP/1.1 302 Found
Location: https://client.example.com/cb?code=SplxlOBeZQQYbYS6WxSbIA
          &state=xyz
```

## 步驟 D

在步驟 D 中，客戶端向認證服務器申請令牌時，需要包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| grant_type     | 授權模式，此處爲 authorization_code     | 必須     |
| code     | 授權碼，步驟 C 中獲取的 code     | 必須     |
| redirect_uri     | 重定向URI，需要和步驟 A 中一致     | 必須     |
| client_id     | 客戶端ID，需要和步驟 A 中一致     | 必須     |


```
POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code&code=SplxlOBeZQQYbYS6WxSbIA
&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb
```

## 步驟 E

在步驟 E 中，認證服務器返回的信息包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| access_token     | 訪問令牌     | 必須     |
| token_type     | 令牌類型，大小寫不敏感。例如 Bearer 或 MAC     | 必須     |
| expires_in     | 過期時間單位爲秒，如果不設置也要通過其它方式設置一個     | 推薦     |
| refresh_token     | 刷新令牌，當訪問令牌過期可通過刷新令牌刷新令牌     | 可選     |
| scope     | 授權範圍，如果與客戶端申請訪問一致，可省略     | 可選     |

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
  "access_token":"2YotnFZFEjr1zCsicMWpAA",
  "token_type":"example",
  "expires_in":3600,
  "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
  "example_parameter":"example_value"
}
```

# 簡化模式

在簡化模式中，相當於授權碼模式來說，步驟 C 不再通過客戶端，直接在瀏覽器(user-agent) 中向認證服務器申請令牌，認證服務器不再返回授權碼，所有步驟都在瀏覽器中完成，最後資源服務器將令牌放在 Fragment 中，瀏覽器從中提前令牌發送給客戶端

```
+----------+
| Resource |
|  Owner   |
|          |
+----------+
     ^
     |
    (B)
+----|-----+          Client Identifier     +---------------+
|         -+----(A)-- & Redirection URI --->|               |
|  User-   |                                | Authorization |
|  Agent  -|----(B)-- User authenticates -->|     Server    |
|          |                                |               |
|          |<---(C)--- Redirection URI ----<|               |
|          |          with Access Token     +---------------+
|          |            in Fragment
|          |                                +---------------+
|          |----(D)--- Redirection URI ---->|   Web-Hosted  |
|          |          without Fragment      |     Client    |
|          |                                |    Resource   |
|     (F)  |<---(E)------- Script ---------<|               |
|          |                                +---------------+
+-|--------+
  |    |
 (A)  (G) Access Token
  |    |
  ^    v
+---------+
|         |
|  Client |
|         |
+---------+
```

* A 客戶端將用戶導向認證服務器，攜帶客戶端ID以及重定向URI
* B 用戶是否授權
* C 用戶同意授權後，認證服務器重定向到 A 中指定的 URI，並在 URI 的 Fragment 中包含訪問令牌
* D 瀏覽器向資源服務器發出請求，該請求中不包含 C 中的 Fragment
* E 資源服務器返回一個網頁，其中包含了可以提取 C 中 Fragment 裏面訪問令牌的腳本
* F 瀏覽器執行 E 中獲得的腳本，提取令牌
* G 瀏覽器將令牌發送給客戶端

## 步驟 A

在步驟 A 中，客戶端發送的請求，需要包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| response_type     | 授權類型，此處爲 token     | 必須     |
| client_id     | 客戶端ID     | 必須     |
| redirect_uri     | 重定向URI     | 可選     |
| scope     | 授權範圍     | 可選     |
| state     | 客戶端當前狀態，服務器會原封不動返回     | 推薦     |

```
GET /authorize?response_type=token&client_id=s6BhdRkqt3&state=xyz
    &redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1
Host: server.example.com
```
## 步驟 C

在步驟 C 中，認證服務器返回的信息包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| access_token     | 訪問令牌     | 必須     |
| token_type     | 令牌類型     | 必須     |
| expires_in     | 過期時間單位爲秒，如果不設置也要通過其它方式設置一個     | 推薦     |
| scope     | 授權範圍，如果與客戶端申請訪問一致，可省略     | 可選     |
| state     | 如果步驟 A 中指定了，原封不動的返回     | 可選     |

```
HTTP/1.1 302 Found
Location: http://example.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA
          &state=xyz&token_type=example&expires_in=3600
```

# 密碼模式

密碼模式就是用戶直接將用戶名密碼提供給客戶端，客戶端使用這些信息去認證服務器申請授權。通常只有在用戶對客戶端有宗教崇拜般的信仰時才會使用此模式，實際上就是不要使用此模式，此文只是爲了完整性記錄一下

```
+----------+
| Resource |
|  Owner   |
|          |
+----------+
     v
     |    Resource Owner
    (A) Password Credentials
     |
     v
+---------+                                  +---------------+
|         |>--(B)---- Resource Owner ------->|               |
|         |         Password Credentials     | Authorization |
| Client  |                                  |     Server    |
|         |<--(C)---- Access Token ---------<|               |
|         |    (w/ Optional Refresh Token)   |               |
+---------+                                  +---------------+

       Figure 5: Resource Owner Password Credentials Flow
```

* A 資源所有者提供用戶名密碼給客戶端
* B 客戶端使用用戶名密碼去認證服務器申請令牌
* C 認證服務器去任後返回令牌


步驟 B 中，需要包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| grant_type     | 授權類型此處爲 password     | 必須     |
| username     | 用戶名     | 必須     |
| password     | 密碼     | 必須     |
| scope     | 授權範圍     | 可選     |

```
POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=password&username=johndoe&password=A3ddj3w
```

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
  "access_token":"2YotnFZFEjr1zCsicMWpAA",
  "token_type":"example",
  "expires_in":3600,
  "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
  "example_parameter":"example_value"
}
```

# 客戶端模式
客戶端模式就是客戶端以自己的名義向服務提供商進入認證

```

+---------+                                  +---------------+
|         |                                  |               |
|         |>--(A)- Client Authentication --->| Authorization |
| Client  |                                  |     Server    |
|         |<--(B)---- Access Token ---------<|               |
|         |                                  |               |
+---------+                                  +---------------+

                Figure 6: Client Credentials Flow
```

* A 客戶端向認證服務器進行身份認證，並要求一個令牌
* B 認證服務器確認後向客戶端提供訪問令牌

在步驟 A 中，需要包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| grant_type     | 授權類型，此處爲 client_credentials     | 必須     |
| scope     | 授權範圍     | 可選     |

```
POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=client_credentials
```

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
  "access_token":"2YotnFZFEjr1zCsicMWpAA",
  "token_type":"example",
  "expires_in":3600,
  "example_parameter":"example_value"
}
```

# 刷新

如果訪問令牌過期了，需要使用刷新令牌刷新，需要包含如下參數

| 參數名稱 | 函數 | 是否必須 |
| -------- | -------- | -------- |
| grant_type     | 授權類型，此處應該是 refresh_token     | 必須     |
| refresh_token     | 認證服務器返回的刷新令牌     | 必須     |
| scope     | 授權範圍，不可超過上次申請的範圍，如果省略表示與上次一致     | 可選     |

```
POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA
```