# runsvdir program

啓動並監控一組 runsv 進程

```
runsvdir [-P] dir [ log ]
```

* dir 必須是目錄。log 是 readproctitle 日誌的空間持有者，並且必須至少有7個字符長或不存在
* runsvdir 爲目錄 dir 中每個子目錄或目錄的符號鏈接啓動一個 runsv 進程，最多 1000 個子目錄，如果它終止，則重新啓動一個 runsv 進程。runsvdir 跳過以 **.** 開頭的子目錄。runsv 必須在 runsvdir 的 PATH 中
* runsvdir 至少每 5 秒檢查 dir 目錄是否更改(最後修改時間 inode 或設備已更改)。如果它在 dir 目錄中發現一個新的子目錄或目錄的符號鏈接，它會創建一個新的 runsv 進程；如果 之前的目錄被刪除，它會向相應的 runsv 進程發送一個 TERM signal，停止監視該進程，因此如果 runsv 進程退出，則不會重新啓動它
* 如果將 log 參數傳遞該 runsvdir，則 stderr 的所有輸出都將重定向到此日誌，這類似與 daemontools 的 readproctitle 日誌。runsvdir 每 15 分鐘向 readproctitle 日誌寫入一個點，以便舊的錯誤消息過期

# Options

* -P -> 使用 setsid 在新 session 和 單獨的進程組中運行每個 runsv 進程

# Signals

如果 runsvdir 收到一個 TERM signal 它會立刻以 0 退出

如果 runsvdir 收到 HUP signal 它會向正在監視的每個 runsv 進程發送一個 TERM signal 然後以 111 退出