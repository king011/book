# [Grid System](https://www.primefaces.org/primeflex/gridsystem)

Grid 是一種基於 flex 的輕量級響應式佈局的實用程序，針對 手機 平板 筆記本 台式機 進行了優化

| Class | Properties |
| -------- | -------- |
| grid     | display: flex;<br>    flex-wrap: wrap;<br>    margin-right: -0.5rem;<br>    margin-left: -0.5rem;<br>    margin-top: -0.5rem;     |
| grid-nogutter     |  margin-right: 0;<br>    margin-left: 0;<br>    margin-top: 0;     |
| col     | flex-grow: 1;<br>    flex-basis: 0;<br>    padding: $gutter;     |
| col-fixed     | flex: 0 0 auto;<br>    padding: $gutter;     |
| col-1     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 8.3333%;     |
| col-2     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 16.6667%;     |
| col-3     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 25%;     |
| col-4     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 33.3333%;     |
| col-5     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 41.6667%;     |
| col-6     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 50%;     |
| col-7     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 58.3333%;     |
| col-8     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 66.6667%;     |
| col-9     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 75%;     |
| col-10     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 83.3333%;     |
| col-11     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 91.6667%;     |
| col-12     | flex: 0 0 auto;<br>    padding: $gutter;<br>    width: 100%;     |
| col-offset-0     | margin-left: 0;     |
| col-offset-1     | margin-left: 8.3333%;     |
| col-offset-2     | margin-left: 16.6667%;     |
| col-offset-3     | margin-left: 25%;     |
| col-offset-4     | margin-left: 33.3333%;     |
| col-offset-5     | margin-left: 41.6667%;     |
| col-offset-6     | margin-left: 50%;     |
| col-offset-7     | margin-left: 58.3333%;     |
| col-offset-8     | margin-left: 66.6667%;     |
| col-offset-9     | margin-left: 75%;     |
| col-offset-10     | margin-left: 83.3333%;     |
| col-offset-11     | margin-left: 91.6667%;     |
| col-offset-12     | margin-left: 100%;     |

## Grid

網格是一個基於 flexbox 的 css 實用程序。你可以 [在此](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) 查看完整的 flexbox 教學。

基本網格是通過 class grid 和 class col 來定義

![](assets/grid.png)

```
<div class="grid">
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-blue-500 font-bold text-white">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-blue-500 font-bold text-white">2</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-blue-500 font-bold text-white">3</div>
    </div>
</div>
```

## 12 Column Grid

網格包含一個基於 12 列的佈局，其中列的寬度使用 class col-{number} 定義。也可以使用 col 來與自動寬度的列一起使用


![](assets/12.png)

```
<div class="grid">
    <div class="col-4">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">4</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
    <div class="col">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">1</div>
    </div>
</div>
<div class="grid">
    <div class="col-2">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">2</div>
    </div>
    <div class="col-6">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">6</div>
    </div>
    <div class="col-4">
        <div class="text-center p-3 border-round-sm bg-yellow-500 font-bold text-gray-900">4</div>
    </div>
</div>
```

## MultiLine

當列數超過12時 將換行

![](assets/multiLine.png)

```
<div class="grid">
		<div class="col-6">
				<div class="text-center p-3 border-round-sm bg-green-500 font-bold text-white">6</div>
		</div>
		<div class="col-6">
				<div class="text-center p-3 border-round-sm bg-green-500 font-bold text-white">6</div>
		</div>
		<div class="col-6">
				<div class="text-center p-3 border-round-sm bg-green-500 font-bold text-white">6</div>
		</div>
		<div class="col-6">
				<div class="text-center p-3 border-round-sm bg-green-500 font-bold text-white">6</div>
		</div>
</div>
```

## Fixed Width Column

可以使用 class col-fixed 和 style 來指定固定寬度

![](assets/fixed.png)

```
<div class="grid">
		<div class="col-fixed" style="width:100px">
				<div class="text-center p-3 border-round-sm bg-purple-500 font-bold text-white">100px</div>
		</div>
		<div class="col">
				<div class="text-center p-3 border-round-sm bg-purple-500 font-bold text-white">auto</div>
		</div>
</div>
```

## Offset
使用 class col-offset-{number} 來指定偏移

```
<div class="grid">
    <div class="col-6 col-offset-3">
        <div class="text-center p-3 border-round-sm bg-indigo-500 font-bold text-white">6</div>
    </div>
</div>
<div class="grid">
    <div class="col-4">
        <div class="text-center p-3 border-round-sm bg-indigo-500 font-bold text-white">4</div>
    </div>
    <div class="col-4 col-offset-4">
        <div class="text-center p-3 border-round-sm bg-indigo-500 font-bold text-white">4</div>
    </div>
</div>
```

## Nested

可以嵌套 grid 來創建更複雜的佈局，這也是現代化 ui 架構(2023年) 推薦使用的佈局方案

![](assets/nested.png)

```
<!-- 外層 -->
<div class="grid">
    <div class="col-8">
        <!-- 嵌套 -->
        <div class="grid">
            <div class="col-6">
                <div class="text-center p-3 border-round-sm bg-orange-500 font-bold text-white">6</div>
            </div>
            <div class="col-6">
                <div class="text-center p-3 border-round-sm bg-orange-500 font-bold text-white">6</div>
            </div>
            <div class="col-12">
                <div class="text-center p-3 border-round-sm bg-orange-500 font-bold text-white">12</div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="text-center p-3 border-round-sm h-full bg-orange-500 font-bold text-white">4</div>
    </div>
</div>
```

## Gutter

一個 5rem 的間距被應用到 網格上，如果你想刪除間距可以設置 class grid-nogutter 到 網格或設置到單獨的列上

```
<div class="grid grid-nogutter">
    <div class="col">1</div>
    <div class="col grid-nogutter">2</div>
    <div class="col">3</div>
</div>
```

# 響應式佈局
和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同