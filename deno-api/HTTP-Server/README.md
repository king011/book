# HTTP Server

deno 提供了一個 serveHttp 函數可以用於爲 tcp/tls 連接處理 http 服務

```
const conn = Deno.listen({ port: 80 });
const httpConn = Deno.serveHttp(await conn.accept());
const e = await httpConn.nextRequest();
if (e) {
  e.respondWith(new Response("Hello World"));
}
```

也可以使用迭代器

```
async function handleHttp(conn: Deno.Conn) {
  for await (const e of Deno.serveHttp(conn)) {
    e.respondWith(new Response("Hello World"));
  }
}

for await (const conn of Deno.listen({ port: 80 })) {
  handleHttp(conn);
}
```

如果 httpConn.nextRequest() 遇到錯誤或返回 null，則底層 HttpConn 資源將自動關閉

# Example

```
type HTTPHandler = (req: Request) => Response | Promise<Response>;
/**
 * 實現一個簡單路由
 */
class Router {
  private readonly handler_ = new Array<{
    prefix: string;
    hander: HTTPHandler;
  }>();
  /**
   * 註冊路由
   */
  handle(
    prefix: string,
    handler: HTTPHandler,
  ) {
    this.handler_.push({
      prefix: prefix,
      hander: handler,
    });
    return this;
  }
  /**
   * 運行服務
   */
  async serve(l: Deno.Listener) {
    for await (const c of l) {
      this._serveHttp(c);
    }
  }
  private async _serveHttp(c: Deno.Conn) {
    for await (const evt of Deno.serveHttp(c)) {
      this._handler(evt);
    }
  }
  private async _handler(evt: Deno.RequestEvent) {
    const req = evt.request;
    const url = req.url;
    const host = new URL(req.url).host;
    const path = url.substring(url.indexOf(host) + host.length);

    for (const h of this.handler_) {
      if (path.startsWith(h.prefix)) {
        try {
          evt.respondWith(await h.hander(req));
        } catch (e) {
          evt.respondWith(
            new Response(`${e}`, {
              status: 500,
              statusText: "Internal Server Error",
            }),
          );
        }
        return;
      }
    }
    evt.respondWith(
      new Response(null, {
        status: 404,
        statusText: "Not Found",
      }),
    );
  }
}
const l = Deno.listen({ port: 9000 });
console.log("listen on", l.addr);
const s = new Router();
s.handle(
  "/json",
  (_) =>
    new Response(
      JSON.stringify({ name: "cerberus", lv: 1 }),
      {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
      },
    ),
);
s.handle(
  "/xml",
  (_) =>
    new Response("<root><name>cerberus</name><lv>1</lv></root>", {
      headers: {
        "Content-Type": "application/xml; charset=utf-8",
      },
    }),
);
s.handle(`/file`, async (_) => {
  const f = await Deno.open("./src/main.ts");
  return new Response(f.readable, {
    headers: {
      "Content-Type": "text/plain; charset=utf-8",
    },
  });
});
s.handle(`/gzip`, async (_) => {
  const f = await Deno.open("./src/main.ts");
  const c = new CompressionStream("gzip");
  f.readable.pipeTo(c.writable);
  return new Response(c.readable, {
    headers: {
      "Content-Type": "text/plain; charset=utf-8",
      "Content-Encoding": "gzip",
    },
  });
});
s.handle(
  "/chunked", // 分塊傳輸
  (_) => {
    const enc = new TextEncoderStream();
    (async () => {
      const w = enc.writable.getWriter();
      for (let i = 0; i < 5; i++) {
        await new Promise((resolve) => {
          setTimeout(resolve, 1000);
        });
        const str = JSON.stringify({ name: "cerberus is an idea", lv: i });
        await w.write(`${str}\n`);
      }
      w.close();
    })();
    return new Response(enc.readable, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        // "Transfer-Encoding": "chunked", // 此 header 會自動設置 故不需要顯示設置
      },
    });
  },
);
s.handle("/", (_) =>
  new Response("cerberus is an idea", {
    headers: {
      "Content-Type": "text/plain; charset=utf-8",
    },
  }));
s.serve(l);
```