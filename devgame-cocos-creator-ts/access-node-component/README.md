# [訪問其它節點和組件](https://docs.cocos.com/creator/manual/zh/scripting/access-node-component.html)

在 creator 屬性檢查器 中 可以修改節點和 組件 ， 然 通過 腳本 可以更加靈活 的實現 這些修改

# 獲取組件 所在節點 節點掛接組件

Component 必須被 掛接到一個 Node 上 才能執行 [cc.Component](http://docs.cocos.com/creator/api/zh/classes/Component.html) 提供了 node 屬性來 返回 被掛接的 [cc.Node](http://docs.cocos.com/creator/api/zh/classes/Node.html)

[cc.Node](http://docs.cocos.com/creator/api/zh/classes/Node.html) 提供了 getComponent 函數 查找 Node 上指定的組件 如果不存在 返回 null

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    onLoad() {
        // 創建一個 事件處理器
        const handler = new cc.Component.EventHandler()
        handler.target = this.node // handler 目標
        handler.component = "Button" // 腳本 名
        handler.handler = "callback" // 回調 函數
        handler.customEventData = "test" // 事件參數

        // 獲取 node 上的 Button 組件
        //const button = this.node.getComponent(cc.Button)
        const button = this.node.getComponent("cc.Button")
        // 添加 click 回調
        button.clickEvents.push(handler)
    }
    // 實現 處理函數
    callback(evt: Event, data: any) {
        console.log(evt, data)

        // 設置 掛接 Node 屬性
        this.node.x += 100
    }
}
```

> getComponent 查找 自定義的組件 class name 是 區分大小寫的 檔案名稱
> 

# 通過 Creator 屬性檢查器 獲取 其它 Node Component

可以 通過 **property** 標註 [cc.Node](http://docs.cocos.com/creator/api/zh/classes/Node.html) [cc.Component](http://docs.cocos.com/creator/api/zh/classes/Component.html) 其它組件節點類型 屬性 然後在 creator 屬性檢查器中 直接設置 要獲取的 Node Component

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Component)
    readonly targetComponent: Array<cc.Component> = new Array<cc.Component>() // 數組 必須 設置爲 非 null/undefined 才能被 Creator 識別
    @property(cc.Node)
    readonly targetNode: cc.Node = null


    onLoad() {
        // 創建一個 事件處理器
        const handler = new cc.Component.EventHandler()
        handler.target = this.node // handler 目標
        handler.component = "Button" // 腳本 名
        handler.handler = "callback" // 回調 函數

        // 獲取 node 上的 Button 組件
        const button = this.node.getComponent(cc.Button)
        button.clickEvents.push(handler)
    }
    // 實現 處理函數
    callback(evt: Event, data: any) {
        console.log("component", this.targetComponent)
        console.log("node", this.targetNode)
    }
}
```

# 通過 get find 查找 子/孫 節點
```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    onLoad() {
        // 創建一個 事件處理器
        const handler = new cc.Component.EventHandler()
        handler.target = this.node // handler 目標
        handler.component = "Button" // 腳本 名
        handler.handler = "callback" // 回調 函數

        // 獲取 node 上的 Button 組件
        const button = this.node.getComponent(cc.Button)
        button.clickEvents.push(handler)
    }
    // 實現 處理函數
    callback(evt: Event, data: any) {
        // 查找 子節點
        console.log(this.node.getChildByName("Background"))
        // 查找 指定 路徑的 後繼節點
        console.log(cc.find("Background/Label", this.node))
        console.log(cc.find("Canvas/Background/Label")) // 不傳入 referenceNode 則從 最頂層開始 查找
    }
}
```

# 全局變量 模塊
可以將 節點 保存到 全局變量/模塊 中 供 組件共享訪問

