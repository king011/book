# [layout-trbl](https://www.primefaces.org/primeflex/toprightbottomleft)

PrimeFlex 提供了幾個 class 來指定常用的 top right bottom left 值

| Class | Properties |
| -------- | -------- |
| top-auto     | top: auto;     |
| top-0     | top: 0;     |
| top-50     | top: 50%;     |
| top-100     | top: 100%;     |
| right-auto     | right: auto;     |
| right-0     | right: 0;     |
| right-50     | right: 50%;     |
| right-100     | right: 100%;     |
| bottom-auto     | bottom: auto;     |
| bottom-0     | bottom: 0;     |
| bottom-50     | bottom: 50%;     |
| bottom-100     | bottom: 100%;     |
| left-auto     | left: auto;     |
| left-0     | left: 0;     |
| left-50     | left: 50%;     |
| left-100     | left: 100%;     |

![](assets/example.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="relative bg-blue-200 w-9rem h-9rem mx-3 my-3 md:my-0 border-round">
            <div class="absolute top-0 left-0 bg-blue-500 text-white font-bold flex align-items-center justify-content-center w-4rem h-4rem border-round">1</div>
        </div>
        <div class="relative bg-blue-200 w-9rem h-9rem mx-3 my-3 md:my-0 border-round">
            <div class="absolute top-0 right-0 bg-blue-500 text-white font-bold flex align-items-center justify-content-center w-4rem h-4rem border-round">2</div>
        </div>
        <div class="relative bg-blue-200 w-9rem h-9rem mx-3 my-3 md:my-0 border-round">
            <div class="absolute bottom-0 right-0 bg-blue-500 text-white font-bold flex align-items-center justify-content-center w-4rem h-4rem border-round">3</div>
        </div>
        <div class="relative bg-blue-200 w-9rem h-9rem mx-3 my-3 md:my-0 border-round">
            <div class="absolute bottom-0 left-0 bg-blue-500 text-white font-bold flex align-items-center justify-content-center w-4rem h-4rem border-round">4</div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同