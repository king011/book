# 計時器

除了 js 的 setTimeout/setInterval creator 提供了自己的 計時器 源自 cocos2d-x 的 [cc.Scheduler](https://docs.cocos.com/creator/api/zh/classes/Scheduler.html)

計時器 是附着在 組件上的 直接通過 組件的 this 即可訪問 計時器方法

* schedule 開始一個計時器
* scheduleOnce 開始一個只執行一次的計時器
* unschedule 取消一個計時器
* unscheduleAllCallbacks 取消組件的所有計時器

1. 開始一個計時器

    ```ts
    component.schedule(function() {
        // 這裏的 this 指向 component
        this.doSomething()
    }, 5)
    ```
  上面計時器每5秒執行一次

1. 更靈活的計時器

    ```ts
    // 以秒爲單位的 時間間隔
    const interval = 5
    // 重複次數
    const repeat = 3
    // 延遲執行
    const delay = 10
    component.schedule(function() {
        // 這裏的 this 指向 component
        this.doSomething()
    }, interval, repeat, delay)
    ```

	上面計時器 在10秒後 開始計時，每5秒執行一次，重複3次
	
1. 只執行一次

    ```ts
    component.scheduleOnce(function() {
        // 這裏的 this 指向 component
        this.doSomething()
    }, 5)
    ```

1. 取消計時器

    ```ts
    this.count = 0
    this.callback = function () {
        if (this.count === 5) {
            // 在第六次執行回調時 取消這個 計時器
            this.unschedule(this.callback)
        }
        this.doSomething()
        this.count++
    }
    component.schedule(this.callback, 1)
    ```