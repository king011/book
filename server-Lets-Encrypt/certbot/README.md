# certbot

certbot 是 Let's Encrypt 提供的 自動 獲取 證書工具

* 官網 [https://certbot.eff.org/](https://certbot.eff.org/)
* github [https://github.com/certbot/certbot](https://github.com/certbot/certbot)

# 安裝

目前這種安裝方式已經被遺棄且失效了，可以從[官網](https://eff-certbot.readthedocs.io/en/stable/install.html#installation) 查詢新的安裝方式

```sh
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot 
```

# 獲取證書

```sh
# # 為 example.com www.example.com 申請證書
certbot certonly --webroot -w /var/www/html -d example.com -d www.example.com
certbot certonly --standalone -d example.com -d www.example.com

# 自動 更新證書
certbot renew --dry-run 
```

# 注意
--webroot 模式 certbot 會向 -w 指定 目錄寫入創建 .well-known 檔案夾 並創建若干檔案 
通過 http 訪問這些 檔案 以驗證 域名所有權

--standalone 模式則 自動 監聽 443 端口 來和 Let's Encrypt 通信驗證

生成好的證書 會在 **/etc/letsencrypt/live/xxx/** 中 **fullchain.pem**  **privkey.pem** 分別是 證書 和 key 

如果有要刪除的證書 執行 `certbot delete` 後安提示操作即可

