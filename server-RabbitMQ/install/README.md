# 安裝

RabbitMQ 服務器是跨平臺軟體 詳細 安裝步驟以[官網說明](https://www.rabbitmq.com/download.html)爲準

# Docker

使用 [docker](https://registry.hub.docker.com/_/rabbitmq/) 是最方便的 安裝方式

```
# 安裝 最新的 rabbitmq image
docker pull rabbitmq

# 運行一個 新的 rabbitmq 容器
# * -it 爲容器創建 tty 到當前控制檯標準輸入輸出
# * --rm 容器退出時 自動刪除容器
# * --hostname 顯示爲容器指定主機名 而非使用隨機主機名(RabbitMQ 依據節點名稱存儲數據 莫熱節點名爲主機名 故需要爲每個容器指定主機名)
# * --name 指定容器名稱
# * -p 指定映射到宿主機的容器端口
#     * 5672 amqp連接端口
#     * 15672 web 管理頁面端口
#     * 25672 分佈式集羣端口
# * -e 指定環境變量
#     * RABBITMQ_DEFAULT_USER 指定默認用戶名
#     * RABBITMQ_DEFAULT_PASS 指定默認密碼
docker run -it --rm --hostname rabbitmq0 --name rabbitmq -p 5672:5672 -p 15672:15672 -p 25672:25672 -e RABBITMQ_DEFAULT_USER=king -e RABBITMQ_DEFAULT_PASS=123456 rabbitmq

# 在後臺運行 容器
docker run -d --rm --hostname rabbitmq0 --name rabbitmq -p 5672:5672 -p 15672:15672 -p 25672:25672 -e RABBITMQ_DEFAULT_USER=king -e RABBITMQ_DEFAULT_PASS=123456 rabbitmq
```

使用 -it 參數 運行 成功應該能夠在 控制檯看到 類似如下 打印

```
node           : rabbit@rabbitmq0
home dir       : /var/lib/rabbitmq
config file(s) : /etc/rabbitmq/rabbitmq.conf
							: /etc/rabbitmq/conf.d/management_agent.disable_metrics_collector.conf
cookie hash    : sjzgEhsYkLYzt72P6fMibA==
log(s)         : <stdout>
database dir   : /var/lib/rabbitmq/mnesia/rabbit@rabbitmq0
```

* node 是當前rabbitmq節點名
* config file(s) 是配置檔案
* database dir 是數據存儲位置 

> 你應該爲 /var/lib/rabbitmq 創建 volume 以供存儲數據