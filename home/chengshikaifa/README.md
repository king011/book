# 編輯工具

* [code-server](edit-code-server/0)

# 構建工具
* [xmake](build-xmake/0)
* [CMake](build-cmake/0)

# JavaScript/TypeScript
* [NativeScript](NativeScript/0)

# java
* [java](java/0)
* [gradle](java-gradle/0)

# kotlin
* [kotlin](kotlin/0)

# python
* [python 基礎](python/0)

# lua

* [lua 基礎](lua/0)
* [teal](lua-teal/0)

# 支付
* [微信支付](pay-wx/0)