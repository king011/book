# 常用命令

```
# 創建一個 hello 檔案夾並在裏面初始化一個 c 項目
xmake create -l c -P ./hello

# 編譯項目
xmake

# 運行項目
xmake run
xmake r

# 將項目設置爲調試模式
xmake config -m debug
# 將項目設置爲發佈模式(這是默認值)
xmake config -m release


# 調試項目
xmake run -d
xmake r -d
```