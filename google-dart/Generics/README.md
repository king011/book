# 泛型

dart 支持了 泛型 寫法 基本 和 c++ 類似

```dart
abstract class Cache<T> {
  T getByKey(String key);
  void setByKey(String key, T value);
}
```