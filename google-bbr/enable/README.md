# 開啓 bbr

1. 確認 linux 內核 &gt;= 4.9

	```sh
	#info=false
	$ uname -r
	4.15.0-36-generic
	```

1. 查看 是否已經 啓動 bbr

	```sh
	#info=false
	$ sysctl net.ipv4.tcp_available_congestion_control
	net.ipv4.tcp_available_congestion_control = reno cubic
	```
	如果 已經啓動 通常 會顯示 **reno cubic bbr** 則 可略過後續步驟
	
1. 啓動 bbc

	```sh
	#info=false
	$ sudo modprobe tcp_bbr && sysctl net.ipv4.tcp_available_congestion_control
	net.ipv4.tcp_available_congestion_control = reno cubic bbr
	```
1. 添加 配置 以便 重啓後 繼續使用 bbr
	```sh
	#info=false
	sudo echo "tcp_bbr" >> /etc/modules-load.d/modules.conf

	sudo echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	sudo echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	```


	