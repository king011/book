# CoreDNS
CoreDNS 是用 go 實現的一個 開源(Apache-2.0) 快速 靈活 跨平臺的 DNS 服務器 

* 官網 [https://coredns.io/](https://coredns.io/)
* 源碼 [https://github.com/coredns/coredns](https://github.com/coredns/coredns)

# Corefile

coredns 默認 使用 當前檔案下的 Corefile 定義的 配置

```sh
coredns -conf /opt/coredns/Corefile
```

```
#info="Corefile"
:10053 {
	cache
	forward . 8.8.8.8:53 {
		force_tcp
	}
}
```

```
#info="coredns.service"
[Unit]
Description=CoreDNS
After=network.target
 
[Service]
User=king
Type=simple
ExecStart=/opt/coredns/coredns -conf /opt/coredns/Corefile
KillMode=control-group
Restart=on-failure
 
[Install]
WantedBy=multi-user.target
```