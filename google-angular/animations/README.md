# 動畫

angular 的動畫模塊是 **@angular/animations** 和 **@angular/platform-browser**，需要將其導入到項目中以便使用動畫

```
#info="src/app/app.module.ts"
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  declarations: [ ],
  bootstrap: [ ]
})
export class AppModule { }
```

# 在組件中使用動畫

要在組件中使用動畫需要從 **@angular/animations** 模塊中導入一些必要函數

```
#info="animations.component.ts"
import { Component, HostBinding } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
```

之後在 **@Component** 裝飾器的 animations 屬性中聲明動畫

```
import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
@Component({
  selector: 'app-animations',
  templateUrl: './animations.component.html',
  styleUrls: ['./animations.component.scss'],
  animations: [
    trigger(
      'openClose',
      [
        state('open', style({
          height: '200px',
          opacity: 1,
          backgroundColor: 'yellow'
        })),
        state('closed', style({
          height: '100px',
          opacity: 0.8,
          backgroundColor: 'blue'
        })),
        transition('open => closed', [
          animate('1s')
        ]),
        transition('closed => open', [
          animate('0.5s')
        ]),
      ],
    ),
  ],
})
export class AnimationsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  isOpen = true;
  toggle() {
    this.isOpen = !this.isOpen;
  }
}
```

## trigger 觸發器

trigger 函數用於定義動畫觸發器，觸發器會在合適的時代執行動畫效果，其第一個參數是觸發器名稱，第二個參數是一個數組用於定義動畫狀態和過度

一旦定義好觸發器就可以將其綁定到 html 元素以使此元素可以在必要時觸發動畫效果，使用 `[@觸發器名稱]=觸發器狀態` 來爲元素綁定觸發器(元素可以同時擁有和觸發多個觸發器)

```
#info="animations.component.html"
<nav>
    <button (click)="toggle()">Toggle Open/Close</button>
</nav>

<div [@openClose]="isOpen ? 'open' : 'closed'" class="open-close-container">
    <p>The box is now {{ isOpen ? 'Open' : 'Closed' }}!</p>
</div>
```

上面代碼 **@openClose** 將觸發器綁定到 **div** 元素，依據 **isOpen** 變量的不同而改變觸發器狀態，觸發器將執行動畫使元素最終到達指定狀態定義的 css 樣式

## state 狀態

state 函數用於定義狀態，其第一個參數是狀態名稱(必須唯一)，第二個參數是 style 函數返回的 css 元信息

style 函數用於定義 css 樣式，其 key 是 css 屬性名稱必須是小寫駝峯形式例如 **backgroundColor** 或使用單引號括起來的 css 屬性名稱例如 **'background-color'**

```
state('open', style({
  height: '200px',
  opacity: 1,
  backgroundColor: 'yellow'
}))
```

## transition 過度

可以不定義 transition 但那樣，狀態將會在一瞬間完成而不會有動畫效果

transition 函數用於定義狀態如何過度到另外一個狀態，其第一個參數是一個字符串用於描述這個過度效果用於哪些狀態間的轉換，第二個參數接收一個或一系列的 animate 函數

animate 函數第一個參數是一個 number 或字符串，用於定義時間 **animate (duration)** 或 a**nimate ('duration delay easing')**， duration 有多種表示方式

* 純數字，毫秒爲單位: 100
* 字符串，毫秒爲單位: '100ms'
* 字符串，秒爲單位: '0.1s'

delay 的表示方式和 duration 一致，用於定義延遲多久再執行

easing 用於定義[加速曲線](https://easings.net/)，例如 ease-in 開始慢然後逐漸加速

```
#info="一些例子"

// 等待 100毫秒，運行 200毫秒。按照減速曲線運動，快速啓動並逐漸減速直到靜止
'0.2s 100ms ease-out'

// 運行200毫秒，不等待。按照標準曲線運動，開始很慢，中間加速，最後逐漸減速
'0.2s ease-in-out'

// 運行200毫秒，不等待。按照加速曲線運動，開始很慢，最後達到全速
'0.2s ease-in'
```

animate 第二個參數可以指定 style 函數返回的 css 樣式作爲中間狀態而非轉換到目標狀態的 css 樣式

transition 第一個參數用於指定狀態轉換
1. 'open => closed': 從 open 變爲 closed 時
2. 'open => closed, closed => open': 包含多個狀態轉換
3. 'open <=> closed': 雙向轉換

當定義了多個相同的 transition 時，觸發器會執行第一個匹配的 transition

# 轉場與與定義狀態

在 angular 中可以使用 **\*** 或 **void** 定義轉場狀態

## 通配符狀態

使用通配符 **\*** 可以匹配任意的動畫狀態。它可以用來定義那些不在乎元素起始狀態或結束在他的動畫

```
'open => *'
```

上面代碼定義 當從 open 轉換到任意其它狀態時

```
'* => *'
```
上面代碼 匹配任意狀態轉換，因爲觸發器只執行最先匹配的 transition，所以可以把上面代碼放到 transition 列表最後面作爲默認效果

## void 狀態

1. 當元素離開視圖時，會匹配 **&#39;\*&nbsp;=&gt;&nbsp;void&#39;**，它有個別名叫做 **:leave**
2. 當元素進入視圖時，會匹配 **void&nbsp;=&gt;&nbsp;\***，它有個別名叫做 **:enter**
3. 通配符 **\*** 會匹配任何狀態(包括 void)

當使用 ngIf 和 ngFor 時都會觸發 :enter 和 :leave

## :increment 和 :decrement

transition 函數還能接受 :increment 和 :decrement 分別當數值增加或減少時匹配

## 轉場中的 布爾值

如果觸發器與 'true' 或 'false' 綁定，則它可以和 true false 1 0 相比較是否匹配

```
<div [@openClose]="isOpen ? true : false" class="open-close-container">
</div>
```

```
animations: [
  trigger('openClose', [
    state('true', style({ height: '*' })),
    state('false', style({ height: '0px' })),
    transition('false <=> true', animate(500))
  ])
],
```