# ngx-translate
ngx-translate 是 angular 的一個第三方開源(MIT) i18n 運行時庫，可以動態切換 angular 顯示的文本語言

* [官網](http://www.ngx-translate.com/)
* [源碼](https://github.com/ngx-translate/core)