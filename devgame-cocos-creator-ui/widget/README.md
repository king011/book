# [widget 對齊策略](http://docs.cocos.com/creator/manual/zh/components/widget.html)

[cc.Widget](http://docs.cocos.com/creator/api/zh/classes/Widget.html) 可以讓節點相對於 祖先節點 進行對齊

通常是 佈局 ui 的 不錯選擇

![](assets/default.png)



| 選項 | 說明 | 備註 |
| -------- | -------- | -------- |
| Top     | 對齊上邊界     | 選擇後 可以輸入 邊界距離 支持 像素百分比     |
| Bottom     | 對齊下邊界     | 選擇後 可以輸入 邊界距離 支持 像素百分比     |
| Left     | 對齊左邊界     | 選擇後 可以輸入 邊界距離 支持 像素百分比     |
| Right     | 對齊右邊界     | 選擇後 可以輸入 邊界距離 支持 像素百分比     |
| HorizontalCenter     | 水平居中     |      |
| VerticalCenter     | 垂直居中     |      |
| Target     | 對齊目標     | 默認爲父節點     |
| Align Mode     | 對齊模式     |      |

# Align Mode

Align Mode 對齊模式 有如下取值

* ON_WINDOWS_RESIZE 默認值 當 初始化 和窗口大小改變時對齊
* ONCE  只在初始化時對齊一次
* ALWAYS  每幀對齊

# 縮放

如果 同時指定了 Top+Bottom 或 Left+Right 則會自動 縮放 節點

所以 利用此 功能 可以 創建一個 空節點 作爲 Canvas 的子節點 同時 設置 Top Bottom Left Right 爲 0 以便其大小 和屏幕一致 之後在此節點上 佈局 ui 
