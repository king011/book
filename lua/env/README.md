# 環境

從 lua5 開始 允許每個函數聲明一個 自己的環境來查找 全局/設置 全局變量

使用 **setfenv(f,table)** 設置環境

* **f** 是指定要設置環境的函數 1(當前函數) 2(調用當前函數的函數) ...
* **table** 環境

```
a	=	1;
setfenv(1,{g	=	_G})
g.print(a) --nil 已經丟失了原來的 環境 
g.print(g.a) --1

-- 繼承環境
local env	={}
-- g.setfenv(1,g) -- 與下面兩行功能類似
g.setmetatable(env,{__index = g})
g.setfenv(1,env)
print(a) --1
```