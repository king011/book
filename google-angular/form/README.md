# [表單](https://angular.io/api/forms/FormsModule)

```
import { FormsModule }   from '@angular/forms';
```

## NgForm

angular 會自動爲 form 標籤創建 NgForm，NgForm 爲表單提供來一些額外功能

* 監控控件變化
* 驗證輸入是否有效

### ngModel

NgForm 會爲 ngModel 綁定控件的值，值變化時自動驗證有效性。

ngModel 提供來一些屬性可供獲取控制狀態

* **pristine** 控件處於最初狀態
* **dirty** = !pristine
* **valid** 控件有效時爲 true
* **invalid** = !valid
* **touched** 控件獲取到焦點
* **untouched** = !touched

```
<!-- 創建一個 form 的ngForm引用 -->
<form (submit)="onSubmit()" #form="ngForm">
    <div>
        <label>Name</label>
        <!-- required minlength 設置驗證條件 -->
        <!-- [(ngModel)]= 進行 雙向 綁定 -->
        <!-- name= Angular要求form內的 input 必須添加 name -->
        <!-- #name="ngModel" 創建一個 名爲 name 的 控件引用 -->
        <input required minlength="4" placeholder="Enter name" [(ngModel)]="username" name="name" #name="ngModel">

        <!-- 控件無效且被用戶使用過顯示錯誤 -->
        <small *ngIf="name.invalid && name.dirty">name tool short</small>
    </div>

    <div>
        <!-- 當所有控件值都有效 才可用 -->
        <button type="submit" [disabled]="form.invalid">Submit</button>
        <!-- 重置表單控件 -->
        <button type="button" (click)="form.reset()">Reset</button>
    </div>
</form>
```

## 響應式表單

推薦使用響應式表單，這樣可以將表單的驗證等信息寫到 typescript 中 而保持 html 代碼的乾淨


```
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
```

```
<!-- 創建一個 form 的ngForm引用 -->
<form (submit)="onSubmit()" [formGroup]="form">
    <div>
        <label>Name</label>
        <!-- required 可以保留以便兼容 css 樣式 當前也可以去掉 -->
        <!-- formControlName 指定 和哪個驗證 綁定 -->
        <input required placeholder="Enter name" formControlName="name">

        <!-- 控件無效且被用戶使用過顯示錯誤 -->
        <small *ngIf="formInvalid('name')">name tool short</small>
    </div>

    <div>
        <!-- 當所有控件值都有效 才可用 -->
        <button type="submit" [disabled]="form.invalid">Submit</button>
        <!-- 重置表單控件 -->
        <button type="button" (click)="form.reset()">Reset</button>
    </div>
</form>
```
```
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component()
export class FormDemoComponent {
  // 創建表單
  readonly form = new FormGroup({
    // 為 name 添加一個 驗證器
    name: new FormControl('', // 初始值
      // 驗證 數組
      [
        Validators.required, // 必填
        Validators.minLength(4), // 最少4位
        //...
      ],
    ),
  })
  formInvalid(name: string): boolean {
    const ctrl = this.form.get(name)
    return (ctrl && ctrl.dirty && ctrl.invalid) ? true : false
  }
  onSubmit() {
    console.log("submit", this.form.get('name')!.value)
  }
}
```

## hasError/getError

可以使用 **hasError/getError** 獲取具體的錯誤信息從而爲不同錯誤顯示不同的錯誤提示

# 自定義驗證器
除了 Validators 中提供的默認驗證器外 還可以自定義 驗證器

驗證器 分爲同步和異步

* **同步** 驗證器返回一個 null 或者 錯誤對象
* **異步** 驗證器 返回一個 Observable 或 Promise 在驗證完成時返回 null 或者 錯誤對象

```
import { ValidatorFn, AbstractControl } from '@angular/forms';
// 導出 一個 正則驗證器的 工廠 函數
export function RegExpValidator(match: RegExp): ValidatorFn {
  // 使用傳入的 正則 創建 驗證器
  return (ctrl: AbstractControl): ValidationErrors | null => {
    const v = ctrl.value
    if (match.test(`${ctrl.value}`)) {
      return null
    } else {
      return {
        'regexp': {
          value: ctrl.value,
        }
      }
    }
  }
}
```

```
  // 創建表單
  readonly form = new FormGroup({
    // 為 name 添加一個 驗證器
    name: new FormControl('', // 初始值
      // 驗證 數組
      [
        Validators.required, // 必填
        Validators.minLength(4), // 最少4位
        RegExpValidator(/^\d+$/),
        //...
      ],
    ),
  })
  firstError(name: string): string | null {
    const ctrl = this.form.get(name)
    if (ctrl && ctrl.errors) {
      const errors = ctrl.errors
      for (const key in errors) {
        if (Object.prototype.hasOwnProperty.call(errors, key)) {
          return key
        }
      }
    }
    return null
  }
```

```
<div>
		<label>Name</label>
		<!-- required 可以保留以便兼容 css 樣式 當前也可以去掉 -->
		<!-- formControlName 指定 和哪個驗證 綁定 -->
		<input required placeholder="Enter name" formControlName="name">

		<!-- 依據錯誤類型，顯示不同的錯誤提示 -->
		<ng-container [ngSwitch]="firstError('name')">
				<ng-template [ngSwitchCase]="'minlength'">
						name tool short
				</ng-template>
				<ng-template [ngSwitchCase]="'regexp'">
						regexp not matched
				</ng-template>
		</ng-container>
</div>
```

## 指令

也可以創建一個指令 在指令中使用 自定義驗證器

```
providers: [{provide: NG_VALIDATORS, useExisting: XXXDirective, multi: true}]
```

```
ng generate directive regexp-validator
```

```
import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms'
import { RegExpValidator } from './validators'
@Directive({
  selector: '[appRegexpValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: RegexpValidatorDirective, multi: true }]
})
export class RegexpValidatorDirective {
  private _pattern: RegExp = null;
  @Input('appRegexpValidator')
  set pattern(pattern: RegExp | string) {
    if (pattern) {
      this._pattern = new RegExp(pattern)
    } else {
      this._pattern = null;
    }
  }
 
  validate(control: AbstractControl): { [key: string]: any } {
    return this._pattern.test(control.value) ? null : { 'regexp': { value: control.value } };
  }
}
```

# 密碼驗證器

inputA 輸入原密碼 inputB 輸入密碼 驗證  
此時 inputA 的輸入 變化 有可能 會影響 到 inputB的 驗證結果

其中一個解決之道 就是 在 inputA 的驗證器中 輸入 inputB的 AbstractControl  
當 inputA 變化後 和 inputB 比較 並利用 傳入的 InputB 的 AbstractControl 調用 setErrors 設置 inputB的 驗證結果

```
<form (submit)="onSubmit()" #userForm="ngForm">
    <mat-form-field class="full-width">
      <input matInput type="password" required minlength="8" [appVerificationByValidator]="passwordVerification.control"  i18n-placeholder placeholder="Password" [(ngModel)]="pwd" name="Password">
      <mat-error i18n>Password Invalid (min 8 length)</mat-error>
    </mat-form-field>
    <mat-form-field class="full-width">
      <input matInput type="password" required minlength="8" [appEqualValidator]="pwd" i18n-placeholder placeholder="Verify Password" [(ngModel)]="pwdVerification" name="PasswordVerification" #passwordVerification="ngModel">
      <mat-error i18n>Password Verification not match</mat-error>
    </mat-form-field>
</form>
```

```
import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms'
@Directive({
  selector: '[appVerificationByValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: VerificationByValidatorDirective, multi: true }],
})
export class VerificationByValidatorDirective {
  target: AbstractControl
  @Input('appVerificationByValidator')
  set pattern(control: AbstractControl) {
    this.target = control;
    console.log(this.target)
  }
  validate(control: AbstractControl): { [key: string]: any } {
    if (this.target) {
      if (this.target.dirty && control.value != this.target.value) {
        this.target.setErrors({
          'appVerificationByValidator': {
            value: control.value,
            target: this.target.value,
          }
        });
      } else {
        this.target.setErrors(null);
      }
    }
    return null;
  }
}
```