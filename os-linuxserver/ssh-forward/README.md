# ssh 端口轉發

ssh 是很安全的 傳輸通道 用ssh 套在非安全傳輸通道樹 常見的 方案 ssh提供了 三種 端口轉發 機制 來建立安全的 傳輸通道

* 本地端口轉發
* 遠程端口轉發
* 動態端口轉發

# 本地端口轉發

本地端口轉發 在ssh所在服務器 開啓一個端口 所有進入此端口的數據 都將轉發到 sshd 由sshd代理訪問一個目標

```sh
ssh -L [本地網卡地址:]本地端口:目標地址:目標端口

# 將ssh服務器的 80 轉發到 sshd 上的 8080
ssh -NL localhost:80:localhost:8080 root@ssh.ip
```

> -N 參數 指定 不需要執行遠程命令 僅僅用於端口轉發數據

# 遠程端口轉發

遠程端口轉發 在sshd上的 開一個端口 所有進入此端口的數據 都將轉發到 ssh 由ssh代理訪問一個目標

```sh
ssh -R [遠程網卡地址:]遠程端口:目標地址:目標端口

# 將sshd服務器的 80 轉發到 ssh 上的 8080
ssh -NR localhost:80:localhost:8080 root@ssh.ip
```

# 動態端口轉發

動態端口轉發 其實就是實現了 socks5 代理

```sh
ssh -D [本地網卡地址:]本地端口

ssh -ND localhost:1080 root@ssh.ip
```
