# MatIconModule

MatIconModule 為應用提供了 使用矢量圖標的 快捷方法  
可以註冊 圖標字體 SVG圖標 但不支持\(png jpg\)

```typescript
import {MatIconModule} from '@angular/material/icon';
```

```html
<mat-icon>home</mat-icon>
```

material 默認使用 google 的 Material Icons  
[https://material.io/tools/icons/?style=baseline](https://material.io/tools/icons/?style=baseline)

# Color

mat-icon 默認使用 material2 主題定義的顏色  
同時 也支持 color 屬性 指定 顏色

* primary 主要顏色
* accent 次要顏色
* warn 錯誤警告

```html
<mat-icon>home</mat-icon>
<mat-icon color="primary">home</mat-icon>
<mat-icon color="accent">home</mat-icon>
<mat-icon color="warn">home</mat-icon>
```

# svg

mat-icon 支持將一個 svg 註冊到 mat-icon 以便可以使用 mat-icon 管理 svg 圖像  
通常在 app-root 組件中 注入 MatIconRegistry 來註冊

**app.component.ts**
```typescript
import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIconInNamespace(
      'custom-svg', // 註冊的 namespace 同c++ 避免 重名
      'grade',  // 註冊名稱
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/baseline-grade-24px.svg')); // svg 檔案 url
  }
}
```

> addSvgIconInNamespace 會使用 HttpClient 下載 svg 檔案  
> 故還需要
> 
> ```typescript
> import { HttpClientModule } from '@angular/common/http';
> ```

```
<mat-icon svgIcon="custom-svg:grade"></mat-icon>
```
# registerFontClassAlias

使用 registerFontClassAlias 可以將 第三方的 Icon Font

後文 以 fontawesome 為例  
官網 [https://fontawesome.com/](https://fontawesome.com/)

## Example

引入 fontawesome 的 css

```html
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
```

**ts**
```typescript
import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private matIconRegistry: MatIconRegistry) {
    this.matIconRegistry.registerFontClassAlias(
      'fontawesome-fa', // 為此 Icon Font 定義一個 別名
      'fa' // 此 Icon Font 使用的 class 名稱
    ).registerFontClassAlias(
      'fontawesome-fab',
      'fab'
    ).registerFontClassAlias(
      'fontawesome-fal',
      'fal'
    ).registerFontClassAlias(
      'fontawesome-far',
      'far'
    ).registerFontClassAlias(
      'fontawesome-fas',
      'fas'
    );
  }
}
```

**html**
```html
<mat-icon fontSet="fontawesome-fab" fontIcon="fa-linux"></mat-icon>
<mat-icon fontSet="fontawesome-fab" fontIcon="fa-linux" color="primary"></mat-icon>
<mat-icon fontSet="fontawesome-fab" fontIcon="fa-linux" color="accent"></mat-icon>
<mat-icon fontSet="fontawesome-fab" fontIcon="fa-linux" color="warn"></mat-icon>
<mat-icon fontSet="fontawesome-fas" fontIcon="fa-atom"></mat-icon>
<mat-icon fontSet="fontawesome-fas" fontIcon="fa-atom" color="primary"></mat-icon>
<mat-icon fontSet="fontawesome-fas" fontIcon="fa-atom" color="accent"></mat-icon>
<mat-icon fontSet="fontawesome-fas" fontIcon="fa-atom" color="warn"></mat-icon>
```

## ionicons

ionicons 是另外一個 第三方 的 Icon Font  
官網 [https://ionicons.com/](https://ionicons.com/)

```html
<link href="https://unpkg.com/ionicons@4.3.0/dist/css/ionicons.min.css" rel="stylesheet">
```

**ts**
```typescript
import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private matIconRegistry: MatIconRegistry) {
    this.matIconRegistry.registerFontClassAlias(
      'ionicons', // 為此 Icon Font 定義一個 別名
      'icon' // 此 Icon Font 使用的 class 名稱
    );
  }
}
```
**html**
```html
<mat-icon fontSet="ionicons" fontIcon="ion-logo-tux"></mat-icon>
<mat-icon fontSet="ionicons" fontIcon="ion-logo-tux" color="primary"></mat-icon>
<mat-icon fontSet="ionicons" fontIcon="ion-logo-tux" color="accent"></mat-icon>
<mat-icon fontSet="ionicons" fontIcon="ion-logo-tux" color="warn"></mat-icon>
```

