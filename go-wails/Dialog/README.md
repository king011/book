# [Dialog](https://wails.io/docs/reference/runtime/dialog)

這部分提供對本地對話框的訪問，例如檔案選擇器和消息框

目前(v2.3.1)這些 api 僅能從 golang 中進行調用

# OpenDirectoryDialog

打開一個對話框，提示用戶選擇一個目錄

```
Go: OpenDirectoryDialog(ctx context.Context, dialogOptions OpenDialogOptions) (string, error)
```

如果用戶取消則返回空字符串

# OpenFileDialog

打開一個對話框，提示用戶選擇一個檔案

```
Go: OpenFileDialog(ctx context.Context, dialogOptions OpenDialogOptions) (string, error)
```

如果用戶取消則返回空字符串

# OpenMultipleFilesDialog
打開一個對話框，提示用戶選擇多個檔案

```
Go: OpenMultipleFilesDialog(ctx context.Context, dialogOptions OpenDialogOptions) ([]string, error)
```

如果用戶取消則返回 nil

# SaveFileDialog
打開一個對話框，提示用戶選擇檔案名以便保持

```
Go: SaveFileDialog(ctx context.Context, dialogOptions SaveDialogOptions) (string, error)
```

如果用戶取消則返回空字符串

# MessageDialog

使用消息對話框顯示消息

```
Go: MessageDialog(ctx context.Context, dialogOptions MessageDialogOptions) (string, error)
```

返回所選擇按鈕的文本或錯誤

# Options

XXXOptions 用於設定對話框細節

## OpenDialogOptions

```
type OpenDialogOptions struct {
    DefaultDirectory           string
    DefaultFilename            string
    Title                      string
    Filters                    []FileFilter
    ShowHiddenFiles            bool
    CanCreateDirectories       bool
    ResolvesAliases            bool
    TreatPackagesAsDirectories bool
}
```


| Field | Description | Win | Mac | Lin |
| -------- | -------- | -------- | -------- | -------- |
| DefaultDirectory     | 打開對話框將顯示的目錄     | ✅     | ✅     | ✅     |
| DefaultFilename     | 默認檔案名     | ✅     | ✅     | ✅     |
| Title     | 對話框標題     | ✅     | ✅     | ✅     |
| Filters     | 檔案過濾列表     | ✅     | ✅     | ✅     |
| ShowHiddenFiles     | 顯示系統隱藏的檔案     |      | ✅     | ✅     |
| CanCreateDirectories     | 允許用戶創建目錄     |      | ✅     |      |
| ResolvesAliases     | 如果爲 true 則返回檔案而非別名     |      | ✅     |      |
| TreatPackagesAsDirectories     | 允許導航到包     |      | ✅     |      |

## SaveDialogOptions

```
type SaveDialogOptions struct {
    DefaultDirectory           string
    DefaultFilename            string
    Title                      string
    Filters                    []FileFilter
    ShowHiddenFiles            bool
    CanCreateDirectories       bool
    TreatPackagesAsDirectories bool
}
```

| Field | Description | Win | Mac | Lin |
| -------- | -------- | -------- | -------- | -------- |
| DefaultDirectory     | 打開對話框將顯示的目錄     | ✅     | ✅     | ✅     |
| DefaultFilename     | 默認檔案名     | ✅     | ✅     | ✅     |
| Title     | 對話框標題     | ✅     | ✅     | ✅     |
| Filters     | 檔案過濾列表     | ✅     | ✅     | ✅     |
| ShowHiddenFiles     | 顯示系統隱藏的檔案     |      | ✅     | ✅     |
| CanCreateDirectories     | 允許用戶創建目錄     |      | ✅     |      |
| TreatPackagesAsDirectories     | 允許導航到包     |      | ✅     |      |

## MessageDialogOptions

```
type MessageDialogOptions struct {
    Type          DialogType
    Title         string
    Message       string
    Buttons       []string
    DefaultButton string
    CancelButton  string
}
```

| Field | Description | Win | Mac | Lin |
| -------- | -------- | -------- | -------- | -------- |
| Type     | 消息框類型，例如 question info ...     | ✅     | ✅     | ✅     |
| Title     | 對話框標題     | ✅     | ✅     | ✅     |
| Message     | 對話框顯示文本     | ✅     | ✅     | ✅     |
| Buttons     | 一個按鈕標題文本     |      | ✅     |      |
| DefaultButton     | 帶有此文本的按鈕被視爲默認按鈕。和 return 綁定     | ✅     | ✅     |      |
| CancelButton     | 帶有此文本的按鈕被視爲取消按鈕。和 escape 綁定     |      | ✅     |      |

### Windows

Windows 具有標準對話框類型，其中的按鈕不可自定義。返回的值將是
*  Ok
*  Cancel
*  Abort
*  Retry
*  Ignore
*  Yes
*  No
*  Try Again
*  Continue

對於 Question 對話框默認情況下 DefaultButton 是 Yes，CancelButton 是 No，你可以顯示設置來修改它

```
    result, err := runtime.MessageDialog(a.ctx, runtime.MessageDialogOptions{
        Type:          runtime.QuestionDialog,
        Title:         "Question",
        Message:       "Do you want to continue?",
        DefaultButton: "No",
    })
```

### Linux

Linux 具有標準的對話框類型，其中的按鈕是不可定製的。返回的值將是

* Ok
* Cancel
* Yes
* No

### Mac

Mac 上消息對話框最多可以指定 4 個按鈕。如果沒有給出 DefaultButton/CancelButton 則第一個按鈕被視爲默認按鈕並綁定到 return

下面代碼 one 將是 DefaultButton

```
selection, err := runtime.MessageDialog(b.ctx, runtime.MessageDialogOptions{
    Title:        "It's your turn!",
    Message:      "Select a number",
    Buttons:      []string{"one", "two", "three", "four"},
})
```

![](assets/0.webp)

下面代碼顯示指定 two 作爲 DefaultButton

```
selection, err := runtime.MessageDialog(b.ctx, runtime.MessageDialogOptions{
    Title:         "It's your turn!",
    Message:       "Select a number",
    Buttons:       []string{"one", "two", "three", "four"},
    DefaultButton: "two",
})
```
![](assets/1.webp)

下面代碼指定 three 作爲 CancelButton

```
selection, err := runtime.MessageDialog(b.ctx, runtime.MessageDialogOptions{
    Title:         "It's your turn!",
    Message:       "Select a number",
    Buttons:       []string{"one", "two", "three", "four"},
    DefaultButton: "two",
    CancelButton:  "three",
})
```

![](assets/2.webp)

## DialogType

```
const (
	InfoDialog     DialogType = "info"
	WarningDialog  DialogType = "warning"
	ErrorDialog    DialogType = "error"
	QuestionDialog DialogType = "question"
)
```

## FileFilter
FileFilter 指定檔案過濾列表

```
type FileFilter struct {
    DisplayName string // Filter information EG: "Image Files (*.jpg, *.png)"
    Pattern     string // semi-colon separated list of extensions, EG: "*.jpg;*.png"
}
```


```
selection, err := runtime.OpenFileDialog(b.ctx, runtime.OpenDialogOptions{
		Title: "Select File",
		Filters: []runtime.FileFilter{
				{
						DisplayName: "Images (*.png;*.jpg)",
						Pattern:     "*.png;*.jpg",
				}, {
						DisplayName: "Videos (*.mov;*.mp4)",
						Pattern:     "*.mov;*.mp4",
				},
		},
})
```

### Windows

Windows 允許在對話框中使用多個檔案過濾器。每個 FileFilter 將在對話框中顯示爲單獨條目

![](assets/w.webp)

### Linux

Linux 允許在對話框中使用多個檔案過濾器。每個 FileFilter 將在對話框中顯示爲單獨條目

![](assets/l.webp)

### Mac

Mac 對話框只有一組模式的過濾器。如果提供來多個 FileFilter 將所有所有定義的模式
