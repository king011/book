# 代理

身在**西朝鮮**代理是首要解決的問題，好在 docker 可以支持代理，不過分爲多種情況，需要分別設定

* dockerd
* container
* build

上述三種情況都依賴 linux 的 HTTP\_PROXY HTTPS\_PROXY 環境變量

# dockerd
在執行 **docker pull** 時，是由守護進程 **dockerd** 完成工作的。因此需要爲 dockerd 設置代理，而 dockerd 通常由 systemd 管控，故實際上是爲 systemd 設定配置

```
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo touch /etc/systemd/system/docker.service.d/proxy.conf
```

在 proxy.conf 中寫入環境變量配置
```
#info="proxy.conf"
[Service]
Environment="HTTP_PROXY=http://proxy.example.com:8080/"
Environment="HTTPS_PROXY=http://proxy.example.com:8080/"
Environment="NO_PROXY=localhost,127.0.0.1,.example.com"
```
最後重啓 docker 使設定生效
```
sudo systemctl daemon-reload
sudo systemctl restart docker
```

# container

因爲是依賴的環境變量所以在容器運行時通過 -e 參數注入環境變量即可

```
docker run \
        -e "HTTP_PROXY=http://proxy.example.com:8080/" \
        -e "HTTPS_PROXY=http://proxy.example.com:8080/" \
        -e "NO_PROXY=localhost,127.0.0.1,.example.com" \
        -d image:tag
```

此外在 docker 17.07 及以後的版本可以 **vi ~/.docker/config.json**

```
#info="config.json"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://proxy.example.com:8080",
     "httpsProxy": "http://proxy.example.com:8080",
     "noProxy": "localhost,127.0.0.1,.example.com"
   }
 }
}
```

設定 proxies 後所有新啓動的容器都會使用 proxies 設定的代理

# build

build 本質也是啓動的一個容器，帶需要使用 **--build-arg** 來指定 build 時的環境變量

```
docker build . \
    --build-arg "HTTP_PROXY=http://proxy.example.com:8080/" \
    --build-arg "HTTPS_PROXY=http://proxy.example.com:8080/" \
    --build-arg "NO_PROXY=localhost,127.0.0.1,.example.com" \
    -t your/image:tag
```

# 注意

無論 docker run 還是 docker build 默認都是網路隔絕的所以代理地址不要寫 **localhost** 之類的地址，可以寫局域網地址，或者加上 **--network host** 參數讓 docker 使用主機網路