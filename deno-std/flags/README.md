# [flags](https://deno.land/std@0.166.0/flags/mod.ts)

flags 提供了一個簡漏的命令行解析功能， parse 函數將解析參數並返回一個對象保留了所有解析結果

* - 和 -- 傳入的參數將解析爲 boolean true，如果其後有傳入內容則解析爲 字符串
* 其它爲解析的內容都存儲在 _ 中

```
import { parse } from "std/flags/mod.ts";
// const obj = parse(Deno.args);
const parsedArgs = parse(["--foo", "--bar=baz", "./quux.txt"]);
// parsedArgs: { foo: true, bar: "baz", _: ["./quux.txt"] }
```