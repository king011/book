# [docker](https://hub.docker.com/r/linuxserver/piwigo)

目前 官方沒有推出 docker 鏡像但 linuxserver 提供了一個鏡像

此外 VideoJS 可以使用 piwigo 支持視頻但需要一些第三方軟體的支持，linuxserver 提供的鏡像沒有支持這些依賴

```
#info="docker-compose.yml"
version: '1'
services:
  main: # 配置 piwigo
    image: linuxserver/piwigo:latest
    restart: always
    ports:
      - 9000:80
    environment:
      - TZ=Asia/Shanghai
      - PUID=1000
      - PGID=1000
    volumes:
      - ./data/gallery:/gallery # 數據存儲
  db: # 配置數據庫
    image: mariadb:10.8
    restart: always
    environment:
      - TZ=Asia/Shanghai
      - MYSQL_ROOT_PASSWORD=123
      - MYSQL_DATABASE=piwigo
      - MYSQL_USER=piwigo
      - MYSQL_PASSWORD=piwigo12345678
    volumes:
      - ./data/db:/var/lib/mysql
```

linuxserver 提供的 piwigo 安裝在 **/app/www/public** 目錄中