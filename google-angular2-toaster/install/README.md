# 環境配置

1. 安裝 npm 包

    ```sh
    npm install angular2-toaster --save
    ```
		
1. import css 顏色

    ```css
    @import "~angular2-toaster/toaster.min.css";
    ```
		
1. 在 AppModule中 import  ToasterModule ToasterService

    ```
    import { BrowserModule } from '@angular/platform-browser';
    import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
    import { NgModule } from '@angular/core';
    import { HttpClientModule } from '@angular/common/http';
    import { ScrollingModule } from '@angular/cdk/scrolling';
    import { AppRoutingModule } from './app-routing.module';
    import { AppComponent } from './app.component';
    import { ToasterModule, ToasterService } from 'angular2-toaster';   
    @NgModule({
      declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule, HttpClientModule, ScrollingModule,
        AppRoutingModule,
        ToasterModule.forRoot(),
      ],
      providers: [ToasterService],
      bootstrap: [AppComponent]
    })
    export class AppModule { }
    ```

1. 在 AppComponent 初始化 配置

    ```
    #info="app.component.html"
    <router-outlet></router-outlet>
    <toaster-container [toasterconfig]="config"></toaster-container>
    ```
		
    ```
    #info="app.component.ts"
    import { Component } from '@angular/core';
    import { ToasterConfig } from 'angular2-toaster';
    @Component({
      selector: 'app-root',
      templateUrl: './app.component.html',
      styleUrls: ['./app.component.scss']
    })
    export class AppComponent {
      config: ToasterConfig =
        new ToasterConfig({
          positionClass: "toast-bottom-right"
        });
    }
    ```
		
1. 彈出消息

    ```
    import { ToasterService } from 'angular2-toaster';
    export class XXX {
        constructor(private httpClient: HttpClient,
        private toasterService: ToasterService,
        ) {
        }
        xx(){
            this.toasterService.pop('error', 'title', 'body');
        }
    }
    ```


```
public config: ToasterConfig = 
    new ToasterConfig({typeClasses: {
      error: 'custom-toast-error',
      info: 'custom-toast-info',
      wait: 'custom-toast-wait',
      success: 'custom-toast-success',
      warning: 'custom-toast-warning'
    }});
```