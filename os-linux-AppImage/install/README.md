# 環境安裝

目前大部分系統都已經默認安裝了好了運行環境，用戶不需要執行任何操作即可直接執行 AppImage，只有執行檔的管理員需要[安裝](https://github.com/AppImage/AppImageKit/releases)打包環境用於創建 AppImage

# 解壓

有時網路上的一些 AppImage 缺少了一些依賴項目導致在某些系統下無法運行，你可以嘗試將其手動解壓後加入依賴並重新打包

要解壓通常有兩種方法

* 執行 AppImage 檔案並添加 --appimage-extract 參數，這會在當前目錄創建一個 squashfs-root 檔案夾並將內容解壓到此
* 可以使用 7zip 對其進行解壓