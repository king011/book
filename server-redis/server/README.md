# 服務器

| 命令 | 含義 | 
| -------- | -------- |
| BGREWRITEAOF      | 異步改寫僅追加檔案     |
| BGSAVE      | 異步保存數據集到磁盤     |
| CLIENT KILL [ip:port] [ID client-id]      | 殺死一個客戶端連接     |
| CLIENT LIST      | 獲取客戶端連接到服務器的連接列表     |
| CLIENT GETNAME      | 獲取當前連接的名稱     |
| CLIENT PAUSE timeout      | 停止指定的時間處理來自客戶端的命令     |
| CLIENT SETNAME connection-name      | 設置當前連接的名稱     |
| CLUSTER SLOTS      | 獲取集羣插槽數組節點的映射     |
| COMMAND      | 獲取 redis 的命令的詳細信息數組     |
| COMMAND COUNT      | 得到 redis 命令的總數     |
| COMMAND GETKEYS      | 給予充分的 redis 命令提取鍵     |
| BGSAVE      | 異步保存數據集到磁盤     |
| COMMAND INFO command-name [command-name ...]      | 獲取具體的 redis 命令 詳細消息數組     |
| CONFIG GET parameter      | 獲取配置參數值     |
| CONFIG REWRITE      | 重寫的存儲器配置的配置檔案     |
| CONFIG SET parameter value      | 配置參數設置爲給定值     |
| CONFIG RESETSTAT      | 復位信息返回的統計     |
| DBSIZE      | 返回所選數據庫中的數目     |
| DEBUG OBJECT key      | 獲取有關的一個關鍵鍵的調試信息     |
| DEBUG SEGFAULT      | 使服務器崩潰     |
| FLUSHALL      | 從所有數據庫中刪除所有項     |
| FLUSHDB      | 從當前數據庫中刪除所有項     |
| INFO [section]      | 獲取有關服務器的信息和統計數據     |
| LASTSAVE      | 獲得最後成功的 unix 時間戳保存到磁盤     |
| MONITOR      | 監聽由實時服務器接收到的所有請求     |
| ROLE      | 返回在複製的情況下實例的角色     |
| SAVE      | 同步保存數據集到磁盤     |
| SHUTDOWN [NOSAVE] [SAVE]      | 同步的數據集保存到磁盤 然後關閉服務器     |
| SLAVEOF host port      | 使服務器爲另一個實例的從站或者促進其作爲主     |
| SLOWLOG subcommand [argument]      | 管理 redis 慢查詢日誌     |
| SYNC      | 命令用於複製     |
| TIME      | 返回服務器當前時間     |

