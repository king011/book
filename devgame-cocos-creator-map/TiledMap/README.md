# [TiledMap 組件](http://docs.cocos.com/creator/manual/zh/components/tiledtile.html)

[cc.TiledMap](http://docs.cocos.com/creator/api/zh/classes/TiledMap.html) 提供了對 TiledMap 的支持

> TiledMap 中原點在左上角 而 creator 原點在右下角
> 

# 常用方法

```
#info=false
// 返回 圖層
getLayers(): TiledLayer[]
getLayer(layerName: string): TiledLayer

// 返回 對象層
getObjectGroups(): TiledObjectGroup[]
getObjectGroup(groupName: string): TiledObjectGroup

// 返回地圖大小
getMapSize(): Size
getTileSize(): Size

// 返回地圖 方向
getMapOrientation(): number
```