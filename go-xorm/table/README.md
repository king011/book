# 表結構操作

* engine.DBMetas() 返回數據庫中所有表 字段 索引信息
* engine.TableInfo(bean) 返回傳入模型 bean 計算出的 表結構信息

# 表操作

* CreateTables 創建表 參數爲一個或多個空的 struct 指針。調用前支持 Charset() StoreEngine() 如果數據庫支持 可用於設置字符集 或 存儲引擎。目前僅 mysql 支持
* IsTableEmpty() 判斷表是否爲空 
* IsTableExist 判斷表是否存在
* DropTables() 刪除 表 參數可以是 字符串表名 或 \*struct， 如果傳入 \*struct 還會刪除模型綁定的索引

# 索引

* CreateIndexes 創建 索引
* CreateUniques 創建唯一索引

# 同步數據庫結構

xorm 提供了 同步函數 用於自動檢測表結構的變動 並自動同步

1. Sync
    * 自動檢測和創建表 這個檢測是根據表的名字
    * 自動檢測和新增表中的字段 這個檢測是根據字段名
    * 自動檢測和創建索引以及唯一索引 這個檢測是根據索引的一個或多個字段名 而不根據索引名稱
2. Sync2 是對 Sync 的改進 目前推薦使用 Sync2
    * 自動檢測和創建表 這個檢測是根據表的名字
    * 自動檢測和新增表中的字段 這個檢測是根據字段名 同時對表中多餘的字段給出警告信息
    * 自動檢測和創建索引以及唯一索引 這個檢測是根據索引的一個或多個字段名 而不根據索引名稱
    * 字段轉換 varchar 字段類型到 text 字段類型 自動警告其它字段類型在模型和數據庫之間不一致的情況
    * 自動警告字段的默認值 是否爲空信息在模型和數據庫之間不匹配的情況

# 導出導入

xorm 提供了數據庫 導出導入功能(需要 數據庫後端本身支持)

* engine.DumpAll(w io.Writer)
* engine.DumpAllToFile(fpath string)
* engine.Import(r io.Reader)
* engine.ImportFile(fpath string)