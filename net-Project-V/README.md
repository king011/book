# Project V
Project V 2015-11-30 出現的 v2ray 一套 go 實現的 開源(MIT) 代理平臺 v2ray 是其核心工具

* 官網 [https://www.v2ray.com/](https://www.v2ray.com/)
* 源碼 [https://github.com/v2ray/v2ray-core](https://github.com/v2ray/v2ray-core)

目前 v2ray 更新了新的官網和 源碼地址

* 官網 [https://www.v2fly.org/](https://www.v2fly.org/)
* 源碼 [https://github.com/v2fly](https://github.com/v2fly)
* docker [https://hub.docker.com/r/v2fly/v2fly-core](https://hub.docker.com/r/v2fly/v2fly-core)