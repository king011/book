# [Syntax 文法](https://jsonnet.org/learning/tutorial.html)

任何 JSON 檔案 都是 合法的 Jsonnet 程式，因此本文將只着重結束 Jsonnet 獨特的語法

* Fields 字段名無需使用 引號 括起來
* 允許 註釋
* 字符串 可用 **單引號** 或 **雙引號** 括起來
* 使用 **|||** 支持 多行輸入
* **@** 用於文本 非轉義文本

```
// 單行註釋
{
    /*
        多行註解
    */
    "字段名 可選雙引號": 1,
    '字段名 可選單引號': 1,
    fields: '通常字段名是不帶引號的寫法',
    str0:'單引號 " 測試',
    str1:"雙引號 ' 測試",
    '多行測試': |||
        line 1
        line 2
        
        line 4
    |||,
    s0:@'123\t不會被轉義',
    s1:@"123\t不會被轉義",
    strs:[1,2,3,],// array object 最後的 逗號 可選
}
```

```
{
   "fields": "通常字段名是不帶引號的寫法",
   "s0": "123\\t不會被轉義",
   "s1": "123\\t不會被轉義",
   "str0": "單引號 \" 測試",
   "str1": "雙引號 ' 測試",
   "strs": [
      1,
      2,
      3
   ],
   "多行測試": "line 1\nline 2\n\nline 4\n",
   "字段名 可選單引號": 1,
   "字段名 可選雙引號": 1
}
```

# Variables 變量

* 使用 **local** 定義變量
* 在 fields 旁定義變量 使用 **,** 結尾
* 其它情況定義變量 使用 **;** 結尾

```
local level = 'debug';
{
    items: [
        level,
        {
            level:level, // level 被當前定義覆蓋
            local level = 123, // 當前作用域變量
        },
    ],
}
```

```
{
   "items": [
      "debug",
      {
         "level": 123
      }
   ]
}
```

# References 引用

* **self** 指代當前對象
* **$** 表示最外層對象
* 使用 **[] ** 或 **.** 查找 字段
* 使用 **[number]** 查找數組元素
* 允許任意長度的路徑
* 允許 使用 類似 arr[10:20:2] 的 python 語法取切片
* 字符串也允許取切片

```
{
    local dog = self,
    name: 'dog',
    eat: self.name + ' eat', 
    action: {
        name: 'action',
        speak: $['name'] + ' speak',
        about: 'about '+ $.action.name,
        parent: dog.name,
    },
    math: [1,2,3,4,5],
    first: self.math[0],
    left: self.math[:2], // 1 2
    right: self.math[2:], // 3 4 5
    at: self.math[2:5:2], // 3 5
    s: self.name[:2], // 字符串也允許取切片
}
```
```
{
   "action": {
      "about": "about action",
      "name": "action",
      "parent": "dog",
      "speak": "dog speak"
   },
   "at": [
      3,
      5
   ],
   "eat": "dog eat",
   "first": 1,
   "left": [
      1,
      2
   ],
   "math": [
      1,
      2,
      3,
      4,
      5
   ],
   "name": "dog",
   "right": [
      3,
      4,
      5
   ],
   "s": "do"
}
```

# Arithmetic 算術運算

* 使用 浮點運算 按位運算 布爾邏輯
* 字符串 使用 **+** 串連 如果需要 可以將數字隱式轉換爲 字符串
* 兩個字符串 可以比較 使用 **<** (進行 unicode 碼點比較)
* Objects 使用 **+** 使用右側勝利 合併
* 使用 in 測試對象中的 字段
* == 深層 相等比較
* 通過 **%** 獲取與 Python 兼容的 字符串模板

```
{
  concat_array: [1, 2, 3] + [4,5],// 數組合併 [1,2,3,4,5],
  concat_string: '123' + 4, // 字符串 串連
  equality1: 1 == '1', // false 比較相等
  equality2: [{}, { x: 3 - 1 }] == [{}, { x: 2 }], // true 深層比較相等
  ex1: 1 + 2 * 3 / (4 + 5), // 數字運算
  // bit 運算 自動轉型爲 int
  ex2: self.ex1 | 3,
  // 取模
  ex3: self.ex1 % 2,
  // 邏輯運算
  ex4: (4 > 3) && (1 <= 3) || false, // true
  // 合併 object
  obj: { a: 1, b: 2 } + { b: 3, c: 4 }, // {a:1,b:3,c:4},
  // 測試 是否包含 指定字段
  obj_member: 'foo' in { foo: 1 },
  // 格式化字符串
  str1: 'The value of self.ex2 is ' + self.ex2 + '.', // 連接字符串
  str2: 'The value of self.ex2 is %g.' % self.ex2, // 模板替代 %
  str3: 'ex1=%0.2f, ex2=%0.2f' % [self.ex1, self.ex2], // 模板替代 多個 %
  str4: 'ex1=%(ex1)0.2f, ex2=%(ex2)0.2f' % self, // %(字段) 取模板屬性
  str5: |||
    ex1=%(ex1)0.2f
    ex2=%(ex2)0.2f
  ||| % self,
}
```

```
{
   "concat_array": [
      1,
      2,
      3,
      4,
      5
   ],
   "concat_string": "1234",
   "equality1": false,
   "equality2": true,
   "ex1": 1.6666666666666665,
   "ex2": 3,
   "ex3": 1.6666666666666665,
   "ex4": true,
   "obj": {
      "a": 1,
      "b": 3,
      "c": 4
   },
   "obj_member": true,
   "str1": "The value of self.ex2 is 3.",
   "str2": "The value of self.ex2 is 3.",
   "str3": "ex1=1.67, ex2=3.00",
   "str4": "ex1=1.67, ex2=3.00",
   "str5": "ex1=1.67\nex2=3.00\n"
}
```

# Functions 函數

像 Python 一樣 函數具有 位置參數 命名參數 默認參數 還支持閉包

標準庫 已經定義了許多功能

```
#info="functions.jsonnet"
// 定義一個 函數
// y 使用默認參數
local my_function(x, y=10) = x + y;
// 定義多行函數
local multiline_function(x) =
  // One can nest locals.
  local temp = x * 2;
  // Every local ends with a semi-colon.
  [temp, temp + 1];

local object = {
  // A method
  my_method(x): x * x,
};
{
    // Functions are first class citizens.
    call_inline_function:(function(x) x * x)(5),
    call_multiline_function: multiline_function(4),

    // Using the variable fetches the function,
    // the parens call the function.
    call: my_function(2),

    // Like python, parameters can be named at
    // call time.
    named_params: my_function(x=2),
    // This allows changing their order
    named_params2: my_function(y=3, x=2),

    // object.my_method returns the function,
    // which is then called like any other.
    call_method1: object.my_method(3),

    standard_lib: std.join(' ', std.split('foo/bar', '/')),
    len: [
        std.length('hello'),
        std.length([1, 2, 3]),
    ],
}
```

```
#info="output.json"
{
   "call": 12,
   "call_inline_function": 25,
   "call_method1": 9,
   "call_multiline_function": [
      8,
      9
   ],
   "len": [
      5,
      3
   ],
   "named_params": 12,
   "named_params2": 5,
   "standard_lib": "foo bar"
}
```


```
#info="sours.jsonnet"
// This function returns an object. Although
// the braces look like Java or C++ they do
// not mean a statement block, they are instead
// the value being returned.
local Sour(spirit, garnish='Lemon twist') = {
  ingredients: [
    { kind: spirit, qty: 2 },
    { kind: 'Egg white', qty: 1 },
    { kind: 'Lemon Juice', qty: 1 },
    { kind: 'Simple Syrup', qty: 1 },
  ],
  garnish: garnish,
  served: 'Straight Up',
};

{
  'Whiskey Sour': Sour('Bulleit Bourbon',
                       'Orange bitters'),
  'Pisco Sour': Sour('Machu Pisco',
                     'Angostura bitters'),
}
```
```
#info="output.json"
{
   "Pisco Sour": {
      "garnish": "Angostura bitters",
      "ingredients": [
         {
            "kind": "Machu Pisco",
            "qty": 2
         },
         {
            "kind": "Egg white",
            "qty": 1
         },
         {
            "kind": "Lemon Juice",
            "qty": 1
         },
         {
            "kind": "Simple Syrup",
            "qty": 1
         }
      ],
      "served": "Straight Up"
   },
   "Whiskey Sour": {
      "garnish": "Orange bitters",
      "ingredients": [
         {
            "kind": "Bulleit Bourbon",
            "qty": 2
         },
         {
            "kind": "Egg white",
            "qty": 1
         },
         {
            "kind": "Lemon Juice",
            "qty": 1
         },
         {
            "kind": "Simple Syrup",
            "qty": 1
         }
      ],
      "served": "Straight Up"
   }
}
```

# Conditionals 條件

條件表達式看起來像是 if then else 分支是可選的 默認爲 null

```
local Mojito(virgin=false, large=false) = {
  // A local next to fields ends with ','.
  local factor = if large then 2 else 1,
  // The ingredients are split into 3 arrays,
  // the middle one is either length 1 or 0.
  ingredients: [
    {
      kind: 'Mint',
      action: 'muddle',
      qty: 6 * factor,
      unit: 'leaves',
    },
  ] + (
    if virgin then [] else [
      { kind: 'Banks', qty: 1.5 * factor },
    ]
  ) + [
    { kind: 'Lime', qty: 0.5 * factor },
    { kind: 'Simple Syrup', qty: 0.5 * factor },
    { kind: 'Soda', qty: 3 * factor },
  ],
  // Returns null if not large.
  garnish: if large then 'Lime wedge',
  served: 'Over crushed ice',
};

{
  Mojito: Mojito(),
  'Virgin Mojito': Mojito(virgin=true),
  'Large Mojito': Mojito(large=true),
}
```

```
{
   "Large Mojito": {
      "garnish": "Lime wedge",
      "ingredients": [
         {
            "action": "muddle",
            "kind": "Mint",
            "qty": 12,
            "unit": "leaves"
         },
         {
            "kind": "Banks",
            "qty": 3
         },
         {
            "kind": "Lime",
            "qty": 1
         },
         {
            "kind": "Simple Syrup",
            "qty": 1
         },
         {
            "kind": "Soda",
            "qty": 6
         }
      ],
      "served": "Over crushed ice"
   },
   "Mojito": {
      "garnish": null,
      "ingredients": [
         {
            "action": "muddle",
            "kind": "Mint",
            "qty": 6,
            "unit": "leaves"
         },
         {
            "kind": "Banks",
            "qty": 1.5
         },
         {
            "kind": "Lime",
            "qty": 0.5
         },
         {
            "kind": "Simple Syrup",
            "qty": 0.5
         },
         {
            "kind": "Soda",
            "qty": 3
         }
      ],
      "served": "Over crushed ice"
   },
   "Virgin Mojito": {
      "garnish": null,
      "ingredients": [
         {
            "action": "muddle",
            "kind": "Mint",
            "qty": 6,
            "unit": "leaves"
         },
         {
            "kind": "Lime",
            "qty": 0.5
         },
         {
            "kind": "Simple Syrup",
            "qty": 0.5
         },
         {
            "kind": "Soda",
            "qty": 3
         }
      ],
      "served": "Over crushed ice"
   }
}
```

# Computed Field Names 計算字段名稱
* 使用 obj\[e\] self.f 可以查找字段 但在計算字段名時不能使用 因爲 對象還未構建
* 字段名爲空不會構建字段 配合 if 條件 可以在滿足條件是才構建某些字段

```
local Margarita(salted) = {
  ingredients: [
    { kind: 'Tequila Blanco', qty: 2 },
    { kind: 'Lime', qty: 1 },
    { kind: 'Cointreau', qty: 1 },
  ],
  [if salted then 'garnish']: 'Salt',
};
{
  Margarita: Margarita(true),
  'Margarita Unsalted': Margarita(false),
}
```

```
{
   "Margarita": {
      "garnish": "Salt",
      "ingredients": [
         {
            "kind": "Tequila Blanco",
            "qty": 2
         },
         {
            "kind": "Lime",
            "qty": 1
         },
         {
            "kind": "Cointreau",
            "qty": 1
         }
      ]
   },
   "Margarita Unsalted": {
      "ingredients": [
         {
            "kind": "Tequila Blanco",
            "qty": 2
         },
         {
            "kind": "Lime",
            "qty": 1
         },
         {
            "kind": "Cointreau",
            "qty": 1
         }
      ]
   }
}
```

# Array and Object 數組和對象

如果要創建數組或對象 而又不知道它們在運行時將創建多少個元素/字段時 Jsonnet 具有 Python 樣式的 數組和 對象構造結構

* 可以使用 for 和 if 的任何嵌套
* The nest behaves like a loop nest, although the body is written first.

```
#info="comprehensions.jsonnet"
local arr = std.range(5, 8);
{
  array_comprehensions: {
    higher: [x + 3 for x in arr],
    lower: [x - 3 for x in arr],
    evens: [x for x in arr if x % 2 == 0],
    odds: [x for x in arr if x % 2 == 1],
    evens_and_odds: [
      '%d-%d' % [x, y]
      for x in arr
      if x % 2 == 0
      for y in arr
      if y % 2 == 1
    ],
  },
  object_comprehensions: {
    evens: {
      ['f' + x]: true
      for x in arr
      if x % 2 == 0
    },
    // Use object composition (+) to add in
    // static fields:
    mixture: {
      f: 1,
      g: 2,
    } + {
      [x]: 0
      for x in ['a', 'b', 'c']
    },
  },
}
```
```
#info="output.json"
{
   "array_comprehensions": {
      "evens": [
         6,
         8
      ],
      "evens_and_odds": [
         "6-5",
         "6-7",
         "8-5",
         "8-7"
      ],
      "higher": [
         8,
         9,
         10,
         11
      ],
      "lower": [
         2,
         3,
         4,
         5
      ],
      "odds": [
         5,
         7
      ]
   },
   "object_comprehensions": {
      "evens": {
         "f6": true,
         "f8": true
      },
      "mixture": {
         "a": 0,
         "b": 0,
         "c": 0,
         "f": 1,
         "g": 2
      }
   }
}
```

```
#info="cocktail-comprehensions.jsonnet"
{
  cocktails: {
    "Bee's Knees": {
      // Construct the ingredients by using
      // 4/3 oz of each element in the given
      // list.
      ingredients: [  // Array comprehension.
        { kind: kind, qty: 4 / 3 }
        for kind in [
          'Honey Syrup',
          'Lemon Juice',
          'Farmers Gin',
        ]
      ],
      garnish: 'Lemon Twist',
      served: 'Straight Up',
    },
  } + {  // Object comprehension.
    [sd.name + 'Screwdriver']: {
      ingredients: [
        { kind: 'Vodka', qty: 1.5 },
        { kind: sd.fruit, qty: 3 },
      ],
      served: 'On The Rocks',
    }
    for sd in [
      { name: 'Yellow ', fruit: 'Lemonade' },
      { name: '', fruit: 'Orange Juice' },
    ]
  },
}
```

```
#info="output.json"
{
   "cocktails": {
      "Bee's Knees": {
         "garnish": "Lemon Twist",
         "ingredients": [
            {
               "kind": "Honey Syrup",
               "qty": 1.3333333333333333
            },
            {
               "kind": "Lemon Juice",
               "qty": 1.3333333333333333
            },
            {
               "kind": "Farmers Gin",
               "qty": 1.3333333333333333
            }
         ],
         "served": "Straight Up"
      },
      "Screwdriver": {
         "ingredients": [
            {
               "kind": "Vodka",
               "qty": 1.5
            },
            {
               "kind": "Orange Juice",
               "qty": 3
            }
         ],
         "served": "On The Rocks"
      },
      "Yellow Screwdriver": {
         "ingredients": [
            {
               "kind": "Vodka",
               "qty": 1.5
            },
            {
               "kind": "Lemonade",
               "qty": 3
            }
         ],
         "served": "On The Rocks"
      }
   }
}
```

# Imports 導入

Jsonnet 允許從檔案導入 code 和 raw data 

* 導入類似 複製粘貼Jsonnet代碼
* 按照約定被導入的代碼應該以 **.libsonnet** 作爲後綴名
* Raw JSON 也可以以這種方式被導入
* importstr 用於 逐字導入 utf8 字符串

通常導入內容保存在 頂級局部變量中(當然也可以不這麼做)，這樣看起來類似編程語言的 模塊處理方式

Jsonnet 庫通常返回一個 對象 以便可以輕鬆對其進行擴展

```
#info="imports.jsonnet"
local martinis = import 'martinis.libsonnet';

{
  'Vodka Martini': martinis['Vodka Martini'],
  Manhattan: {
    ingredients: [
      { kind: 'Rye', qty: 2.5 },
      { kind: 'Sweet Red Vermouth', qty: 1 },
      { kind: 'Angostura', qty: 'dash' },
    ],
    garnish: importstr 'garnish.txt',
    served: 'Straight Up',
  },
}
```

```
#info="martinis.libsonnet"
{
  'Vodka Martini': {
    ingredients: [
      { kind: 'Vodka', qty: 2 },
      { kind: 'Dry White Vermouth', qty: 1 },
    ],
    garnish: 'Olive',
    served: 'Straight Up',
  },
  Cosmopolitan: {
    ingredients: [
      { kind: 'Vodka', qty: 2 },
      { kind: 'Triple Sec', qty: 0.5 },
      { kind: 'Cranberry Juice', qty: 0.75 },
      { kind: 'Lime Juice', qty: 0.5 },
    ],
    garnish: 'Orange Peel',
    served: 'Straight Up',
  },
}
```

```
#info="garnish.txt"
Maraschino Cherry
```

```
#info="output.json"
{
  "Manhattan": {
    "garnish": "Maraschino Cherry",
    "ingredients": [
      {
        "kind": "Rye",
        "qty": 2.5
      },
      {
        "kind": "Sweet Red Vermouth",
        "qty": 1
      },
      {
        "kind": "Angostura",
        "qty": "dash"
      }
    ],
    "served": "Straight Up"
  },
  "Vodka Martini": {
    "garnish": "Olive",
    "ingredients": [
      {
        "kind": "Vodka",
        "qty": 2
      },
      {
        "kind": "Dry White Vermouth",
        "qty": 1
      }
    ],
    "served": "Straight Up"
  }
}
```

下面示例演示了如何編寫函數庫。它還顯示了如何在函數範圍內定義局部變量

```
#info="negroni.jsonnet"
local utils = import 'utils.libsonnet';
{
  Negroni: {
    // Divide 3oz among the 3 ingredients.
    ingredients: utils.equal_parts(3, [
      'Farmers Gin',
      'Sweet Red Vermouth',
      'Campari',
    ]),
    garnish: 'Orange Peel',
    served: 'On The Rocks',
  },
}
```

```
#info="utils.libsonnet"
{
  equal_parts(size, ingredients)::
    // Define a function-scoped variable.
    local qty = size / std.length(ingredients);
    // Return an array.
    [
      { kind: i, qty: qty }
      for i in ingredients
    ],
}
```

```
#info="output.json"
{
  "Negroni": {
    "garnish": "Orange Peel",
    "ingredients": [
      {
        "kind": "Farmers Gin",
        "qty": 1
      },
      {
        "kind": "Sweet Red Vermouth",
        "qty": 1
      },
      {
        "kind": "Campari",
        "qty": 1
      }
    ],
    "served": "On The Rocks"
  }
}
```

# Errors 錯誤

Jsonnet 允許一些斷言和直接引發錯誤 Jsonnet 代碼將拋出錯誤並記錄堆棧跟蹤的上下文信息 以便找到調試

* 要拋出一個錯誤使用 error "foo"
* 在表達式前加上 assert 添加斷言 assert "foo"
* 斷言可以增加一個可選的錯誤信息 assert "foo" : "message"
* 斷言字段可以存在屬性 assert self.f == 10

```
// Extend above example to sanity check input.
local equal_parts(size, ingredients) =
  local qty = size / std.length(ingredients);
  // Check a pre-condition
  if std.length(ingredients) == 0 then
    error 'Empty ingredients.'
  else [
    { kind: i, qty: qty }
    for i in ingredients
  ];

local subtract(a, b) =
  assert a > b : 'a must be bigger than b';
  a - b;

assert std.isFunction(subtract);

{
  test1: equal_parts(1, ['Whiskey']),
  test2: subtract(10, 3),
  object: {
    assert self.f < self.g : 'wat',
    f: 1,
    g: 2,
  },
  assert std.isObject(self.object),
}
```

```
{
  "object": {
    "f": 1,
    "g": 2
  },
  "test1": [
    {
      "kind": "Whiskey",
      "qty": 1
    }
  ],
  "test2": 7
}
```

# Parameterize Entire Config 參數化配置

Jsonnet 應該始終會生成相同的數據，但有時候需要在頂層 精心選擇一些參數來定製化 對此有兩種解決方案

1. External variables 可以使用 std.extVar("foo") 在任何位置 訪問 外部變量
2. Top-level arguments 將整個配置 作爲一個函數

## External variables

```
jsonnet --ext-str prefix="Happy Hour " \
        --ext-code brunch=true ...
```

```
#info="top-level-ext.jsonnet"
local lib = import 'library-ext.libsonnet';
{
  [std.extVar('prefix') + 'Pina Colada']: {
    ingredients: [
      { kind: 'Rum', qty: 3 },
      { kind: 'Pineapple Juice', qty: 6 },
      { kind: 'Coconut Cream', qty: 2 },
      { kind: 'Ice', qty: 12 },
    ],
    garnish: 'Pineapple slice',
    served: 'Frozen',
  },

  [if std.extVar('brunch') then
    std.extVar('prefix') + 'Bloody Mary'
  ]: {
    ingredients: [
      { kind: 'Vodka', qty: 1.5 },
      { kind: 'Tomato Juice', qty: 3 },
      { kind: 'Lemon Juice', qty: 1.5 },
      { kind: 'Worcestershire', qty: 0.25 },
      { kind: 'Tobasco Sauce', qty: 0.15 },
    ],
    garnish: 'Celery salt & pepper',
    served: 'Tall',
  },

  [std.extVar('prefix') + 'Mimosa']:
    lib.Mimosa,
}
```

```
#info="library-ext.libsonnet"
local fizz = if std.extVar('brunch') then
  'Cheap Sparkling Wine'
else
  'Champagne';
{
  Mimosa: {
    ingredients: [
      { kind: fizz, qty: 3 },
      { kind: 'Orange Juice', qty: 3 },
    ],
    garnish: 'Orange Slice',
    served: 'Champagne Flute',
  },
}
```
```
#info="output.json"
{
  "Happy Hour Bloody Mary": {
    "garnish": "Celery salt & pepper",
    "ingredients": [
      {
        "kind": "Vodka",
        "qty": 1.5
      },
      {
        "kind": "Tomato Juice",
        "qty": 3
      },
      {
        "kind": "Lemon Juice",
        "qty": 1.5
      },
      {
        "kind": "Worcestershire",
        "qty": 0.25
      },
      {
        "kind": "Tobasco Sauce",
        "qty": 0.15
      }
    ],
    "served": "Tall"
  },
  "Happy Hour Mimosa": {
    "garnish": "Orange Slice",
    "ingredients": [
      {
        "kind": "Cheap Sparkling Wine",
        "qty": 3
      },
      {
        "kind": "Orange Juice",
        "qty": 3
      }
    ],
    "served": "Champagne Flute"
  },
  "Happy Hour Pina Colada": {
    "garnish": "Pineapple slice",
    "ingredients": [
      {
        "kind": "Rum",
        "qty": 3
      },
      {
        "kind": "Pineapple Juice",
        "qty": 6
      },
      {
        "kind": "Coconut Cream",
        "qty": 2
      },
      {
        "kind": "Ice",
        "qty": 12
      }
    ],
    "served": "Frozen"
  }
}
```

## Top-level arguments

也可以使用 頂級參數編寫相同的代碼 需要將整個配置都作爲函數編寫 通常這比 外部變量安全 因爲參數只作爲 函數局部變量 可被訪問

```
jsonnet --tla-str prefix="Happy Hour " \
        --tla-code brunch=true ...
```

```
#info="top-level-tla.jsonnet"
local lib = import 'library-tla.libsonnet';

// Here is the top-level function, note brunch
// now has a default value.
function(prefix, brunch=false) {

  [prefix + 'Pina Colada']: {
    ingredients: [
      { kind: 'Rum', qty: 3 },
      { kind: 'Pineapple Juice', qty: 6 },
      { kind: 'Coconut Cream', qty: 2 },
      { kind: 'Ice', qty: 12 },
    ],
    garnish: 'Pineapple slice',
    served: 'Frozen',
  },

  [if brunch then prefix + 'Bloody Mary']: {
    ingredients: [
      { kind: 'Vodka', qty: 1.5 },
      { kind: 'Tomato Juice', qty: 3 },
      { kind: 'Lemon Juice', qty: 1.5 },
      { kind: 'Worcestershire', qty: 0.25 },
      { kind: 'Tobasco Sauce', qty: 0.15 },
    ],
    garnish: 'Celery salt & pepper',
    served: 'Tall',
  },

  [prefix + 'Mimosa']: lib.Mimosa(brunch),
}
```
```
#info="library-tla.libsonnet"
{
  // Note that the Mimosa is now
  // parameterized.
  Mimosa(brunch): {
    local fizz = if brunch then
      'Cheap Sparkling Wine'
    else
      'Champagne',
    ingredients: [
      { kind: fizz, qty: 3 },
      { kind: 'Orange Juice', qty: 3 },
    ],
    garnish: 'Orange Slice',
    served: 'Champagne Flute',
  },
}
```
```
#info="output.json"
{
  "Happy Hour Bloody Mary": {
    "garnish": "Celery salt & pepper",
    "ingredients": [
      {
        "kind": "Vodka",
        "qty": 1.5
      },
      {
        "kind": "Tomato Juice",
        "qty": 3
      },
      {
        "kind": "Lemon Juice",
        "qty": 1.5
      },
      {
        "kind": "Worcestershire",
        "qty": 0.25
      },
      {
        "kind": "Tobasco Sauce",
        "qty": 0.15
      }
    ],
    "served": "Tall"
  },
  "Happy Hour Mimosa": {
    "garnish": "Orange Slice",
    "ingredients": [
      {
        "kind": "Cheap Sparkling Wine",
        "qty": 3
      },
      {
        "kind": "Orange Juice",
        "qty": 3
      }
    ],
    "served": "Champagne Flute"
  },
  "Happy Hour Pina Colada": {
    "garnish": "Pineapple slice",
    "ingredients": [
      {
        "kind": "Rum",
        "qty": 3
      },
      {
        "kind": "Pineapple Juice",
        "qty": 6
      },
      {
        "kind": "Coconut Cream",
        "qty": 2
      },
      {
        "kind": "Ice",
        "qty": 12
      }
    ],
    "served": "Frozen"
  }
}
```

# Object-Orientation 面向對象

通常 面向對象使從 基類擴展變體變得容易，Jsonnet 也提供了類似功能

* Objects 從 JSON 繼承
* 支持使用 + 合併 兩個對象 字段衝突時 使用 右側勝利
* self 關鍵字 引用當前對象
* 使用 **::** 定義隱藏字段(不會輸出到 JSON)
* super 關鍵字 引用基類
* +: 用於 覆蓋深層嵌套字段

```
local Base = {
  f: 2,
  g: self.f + 100,
};

local WrapperBase = {
  Base: Base,
};

{
  Derived: Base + {
    f: 5,
    old_f: super.f,
    old_g: super.g,
  },
  WrapperDerived: WrapperBase + {
    Base+: { f: 5 },
  },
}
```
```
{
  "Derived": {
    "f": 5,
    "g": 105,
    "old_f": 2,
    "old_g": 105
  },
  "WrapperDerived": {
    "Base": {
      "f": 5,
      "g": 105
    }
  }
}
```

另外一些例子 objects 合併的 **+** 運算符通常可以省略 

```
#info="sours-oo.jsonnet"
local templates = import 'templates.libsonnet';

{
  // The template requires us to override
  // the 'spirit'.
  'Whiskey Sour': templates.Sour {
    spirit: 'Whiskey',
  },

  // Specialize it further.
  'Deluxe Sour': self['Whiskey Sour'] {
    // Don't replace the whole sweetner,
    // just change 'kind' within it.
    sweetener+: { kind: 'Gomme Syrup' },
  },

  Daiquiri: templates.Sour {
    spirit: 'Banks 7 Rum',
    citrus+: { kind: 'Lime' },
    // Any field can be overridden.
    garnish: 'Lime wedge',
  },

  "Nor'Easter": templates.Sour {
    spirit: 'Whiskey',
    citrus: { kind: 'Lime', qty: 0.5 },
    sweetener+: { kind: 'Maple Syrup' },
    // +: Can also add to a list.
    ingredients+: [
      { kind: 'Ginger Beer', qty: 1 },
    ],
  },
}
```

```
#info="templates.libsonnet"
{
  // Abstract template of a "sour" cocktail.
  Sour: {
    local drink = self,

    // Hidden fields can be referred to
    // and overrridden, but do not appear
    // in the JSON output.
    citrus:: {
      kind: 'Lemon Juice',
      qty: 1,
    },
    sweetener:: {
      kind: 'Simple Syrup',
      qty: 0.5,
    },

    // A field that must be overridden.
    spirit:: error 'Must override "spirit"',

    ingredients: [
      { kind: drink.spirit, qty: 2 },
      drink.citrus,
      drink.sweetener,
    ],
    garnish: self.citrus.kind + ' twist',
    served: 'Straight Up',
  },
}
```

```
#info="output.json"
{
  "Daiquiri": {
    "garnish": "Lime wedge",
    "ingredients": [
      {
        "kind": "Banks 7 Rum",
        "qty": 2
      },
      {
        "kind": "Lime",
        "qty": 1
      },
      {
        "kind": "Simple Syrup",
        "qty": 0.5
      }
    ],
    "served": "Straight Up"
  },
  "Deluxe Sour": {
    "garnish": "Lemon Juice twist",
    "ingredients": [
      {
        "kind": "Whiskey",
        "qty": 2
      },
      {
        "kind": "Lemon Juice",
        "qty": 1
      },
      {
        "kind": "Gomme Syrup",
        "qty": 0.5
      }
    ],
    "served": "Straight Up"
  },
  "Nor'Easter": {
    "garnish": "Lime twist",
    "ingredients": [
      {
        "kind": "Whiskey",
        "qty": 2
      },
      {
        "kind": "Lime",
        "qty": 0.5
      },
      {
        "kind": "Maple Syrup",
        "qty": 0.5
      },
      {
        "kind": "Ginger Beer",
        "qty": 1
      }
    ],
    "served": "Straight Up"
  },
  "Whiskey Sour": {
    "garnish": "Lemon Juice twist",
    "ingredients": [
      {
        "kind": "Whiskey",
        "qty": 2
      },
      {
        "kind": "Lemon Juice",
        "qty": 1
      },
      {
        "kind": "Simple Syrup",
        "qty": 0.5
      }
    ],
    "served": "Straight Up"
  }
}
```

上述例子只合併了 文本 通常的清情況是合併混合對象

```
#info="mixins.jsonnet"
local sours = import 'sours-oo.jsonnet';

local RemoveGarnish = {
  // Not technically removed, but made hidden.
  garnish:: super.garnish,
};

// Make virgin cocktails
local NoAlcohol = {
  local Substitute(ingredient) =
    local k = ingredient.kind;
    local bitters = 'Angustura Bitters';
    if k == 'Whiskey' then [
      { kind: 'Water', qty: ingredient.qty },
      { kind: bitters, qty: 'tsp' },
    ] else if k == 'Banks 7 Rum' then [
      { kind: 'Water', qty: ingredient.qty },
      { kind: 'Vanilla Essence', qty: 'dash' },
      { kind: bitters, qty: 'dash' },
    ] else [
      ingredient,
    ],
  ingredients: std.flattenArrays([
    Substitute(i)
    for i in super.ingredients
  ]),
};

local PartyMode = {
  served: 'In a plastic cup',
};

{
  'Whiskey Sour':
    sours['Whiskey Sour']
    + RemoveGarnish + PartyMode,

  'Virgin Whiskey Sour':
    sours['Whiskey Sour'] + NoAlcohol,

  'Virgin Daiquiri':
    sours.Daiquiri + NoAlcohol,

}
```

```
#info="sours-oo.jsonnet"
local templates = import 'templates.libsonnet';

{
  // The template requires us to override
  // the 'spirit'.
  'Whiskey Sour': templates.Sour {
    spirit: 'Whiskey',
  },

  // Specialize it further.
  'Deluxe Sour': self['Whiskey Sour'] {
    // Don't replace the whole sweetner,
    // just change 'kind' within it.
    sweetener+: { kind: 'Gomme Syrup' },
  },

  Daiquiri: templates.Sour {
    spirit: 'Banks 7 Rum',
    citrus+: { kind: 'Lime' },
    // Any field can be overridden.
    garnish: 'Lime wedge',
  },

  "Nor'Easter": templates.Sour {
    spirit: 'Whiskey',
    citrus: { kind: 'Lime', qty: 0.5 },
    sweetener+: { kind: 'Maple Syrup' },
    // +: Can also add to a list.
    ingredients+: [
      { kind: 'Ginger Beer', qty: 1 },
    ],
  },
}
```

```
#info="templates.libsonnet"
{
  // Abstract template of a "sour" cocktail.
  Sour: {
    local drink = self,

    // Hidden fields can be referred to
    // and overrridden, but do not appear
    // in the JSON output.
    citrus:: {
      kind: 'Lemon Juice',
      qty: 1,
    },
    sweetener:: {
      kind: 'Simple Syrup',
      qty: 0.5,
    },

    // A field that must be overridden.
    spirit:: error 'Must override "spirit"',

    ingredients: [
      { kind: drink.spirit, qty: 2 },
      drink.citrus,
      drink.sweetener,
    ],
    garnish: self.citrus.kind + ' twist',
    served: 'Straight Up',
  },
}
```

```
#info="output.json"
{
  "Virgin Daiquiri": {
    "garnish": "Lime wedge",
    "ingredients": [
      {
        "kind": "Water",
        "qty": 2
      },
      {
        "kind": "Vanilla Essence",
        "qty": "dash"
      },
      {
        "kind": "Angustura Bitters",
        "qty": "dash"
      },
      {
        "kind": "Lime",
        "qty": 1
      },
      {
        "kind": "Simple Syrup",
        "qty": 0.5
      }
    ],
    "served": "Straight Up"
  },
  "Virgin Whiskey Sour": {
    "garnish": "Lemon Juice twist",
    "ingredients": [
      {
        "kind": "Water",
        "qty": 2
      },
      {
        "kind": "Angustura Bitters",
        "qty": "tsp"
      },
      {
        "kind": "Lemon Juice",
        "qty": 1
      },
      {
        "kind": "Simple Syrup",
        "qty": 0.5
      }
    ],
    "served": "Straight Up"
  },
  "Whiskey Sour": {
    "ingredients": [
      {
        "kind": "Whiskey",
        "qty": 2
      },
      {
        "kind": "Lemon Juice",
        "qty": 1
      },
      {
        "kind": "Simple Syrup",
        "qty": 0.5
      }
    ],
    "served": "In a plastic cup"
  }
}
```