# kcp-go
kcp-go 是 kcp 的 供實現 以 MIT 發佈

可以 直接 替代 net.TCPConn

源碼 [https://github.com/xtaci/kcp-go](https://github.com/xtaci/kcp-go)


# Example
go get -u -v github.com/xtaci/kcp-go
```go
#info=false
lis, err := kcp.ListenWithOptions(":10000", nil, 10, 3)
```

```go
#info=false
kcpconn, err := kcp.DialWithOptions("192.168.0.1:10000", nil, 10, 3)
```