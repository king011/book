# udp2raw

udp2raw 是一個 fake tcp 的工具，它將 udp 流量僞裝成 tcp 流量，這在某些時候是有用的，例如防火牆阻止傳輸 udp 流量，或者運營商提供的 udp 流量並不穩定

> 對於運營商，udp 僞裝的 tcp 流量計費可能會偏高，因爲對於運營商來說正常的 tcp 重傳的流量不會重複計費，但 fake 的 udp 在上層實現的重傳會被運營商識別爲不同的 tcp 包從而導致重複計費
>
> 對於 windwos，[目前](https://github.com/wangyu-/udp2raw-multiplatform/releases/tag/20230206.0)只支持客戶端不支持服務端。


* 源碼 [https://github.com/wangyu-/udp2raw](https://github.com/wangyu-/udp2raw)