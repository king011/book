# video.js

video.js 是一個開源(Apache 2.0)的 html5 視頻播放器

* 官網 [https://videojs.com/](https://videojs.com/)
* 源碼 [https://github.com/videojs/video.js](https://github.com/videojs/video.js)
* started [https://videojs.com/getting-started/](https://videojs.com/getting-started/)
* document [https://docs.videojs.com/](https://docs.videojs.com/)
* plugins [https://videojs.com/plugins](https://videojs.com/plugins)

```
npm i video.js
npm i @types/video.js --save-dev
```