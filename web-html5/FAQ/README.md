# 前端下載

html5 提供的 Blob 和 URL.createObjectURL 可以實現前端生成下載數據供用戶下載

createObjectURL 會創建一個 url 字符串指向 Blob，可以將其提供給 a 標籤進行下載，注意此 url 會在頁面關閉時釋放指向的資源，你也可以提前調用 revokeObjectURL 移除 url 對資源的引用

```
function download(name: string, data: any) {
  // 創建 url 鏈接對象
  const blob = new Blob([JSON.stringify(data, undefined, "\t")])
  const url = URL.createObjectURL(blob)

  // 創建 a 標籤
  const a = document.createElement('a')
  a.href = url
  a.download = name + '.json'

  // 觸發下載
  a.click()

  // 釋放 url 資源
  URL.revokeObjectURL(url)
}
```

> Blob 通常保存在內存中，並且不同瀏覽器對最大 Blob 限制不同， chrome 通常是最大 2G，firefox 通常是最大 800M，故此方法不適合爲超大檔案提供下載

# 讀取檔案

html5 提供了 File，Arraybuffer，Blob 等可以用於讀取用戶提供的檔案

通常會使用一個隱藏的 input type file 來讓用戶選擇要讀取的檔案，並且使用另外一個自定義外觀(input file 並不好看且不好自定義外觀)的按鈕來觸發選擇操作

```html
<input #file type="file" multiple style="display: none;" (change)="onFileChanged($event)" accept=".json,.xml" />

<button mat-button (click)="file.click()">button</button>
```

```
@Component({
  selector: 'app-xxx',
  templateUrl: './xxx.component.html',
  styleUrls: ['./xxx.component.scss']
})
export class XXX  {
{
  async onFileChanged(evt: any) {
    if (evt.target.files) {
      for (let i = 0; i < evt.target.files.length; i++) {
        const element = evt.target.files[i]
        await this._readFile(element)
      }
    }
  }
  private async _readFile(file: File) {
    console.log('--', file.name, file.size)
    const arrayBuffer = await file.arrayBuffer()
    const text = new TextDecoder('utf8').decode(new Uint8Array(arrayBuffer))
    console.log(text)
  }
}
```

# textarea

textarea 用於輸入長文本,瀏覽器加入了一些特性比如檢測單詞拼寫等,但這些特性通常對非英文地區並不友好,此外對一些特殊文本輸入也會顯示干擾,推薦的做法是禁用掉這些特性下面的屬性參考了 google 翻譯的 textarea

```html
<textarea autocapitalize="off" autocomplete="off" autocorrect="off" spellcheck="false" rows="10" required name="text" (keydown)="onKeyDownEvent($event)"></textarea>
```

通常 textarea 可以使用如下代碼來支持輸入 tab

```
@Component({
  selector: 'app-xxx',
  templateUrl: './xxx.component.html',
  styleUrls: ['./xxx.component.scss']
})
export class XXX  {
{
  onKeyDownEvent(evt: KeyboardEvent) {
    if (evt.key == 'Tab') {
      evt.preventDefault()
      try {
        // 這個函數已經被遺棄，但目前沒找到更好的方法
        document.execCommand("insertText", false, '\t');
      } catch (err) {
        console.log(err)
        // setRangeText 或其它設置 value 都會使用 undo 失去作用
        const textarea = evt.target as any
        textarea.setRangeText(
          '  ',
          textarea.selectionStart,
          textarea.selectionEnd,
          'end'
        )

      }
    }
  }
}
```