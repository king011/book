# particles

我們的射擊遊戲開始有了一個好的樣子，接下來，我們會使用粒子增強一下效果。

粒子是指在非常短的一段時間裏重複和顯示簡單的精靈。

想想 爆炸 激光 煙霧 等等效果，它們都是用粒子做的——在大多數情況下爆炸效果可以是一個簡單的動畫精靈。

Unity 爲粒子提供了一個強大的內置編輯器，使用的是 Shuriken Engine

# 爆炸 Prefab
我們將製作一個爆炸效果，它會在 enemy 或 player 被摧毀時被用到。這會包含以下內容：
1. 爲爆炸效果製作一個粒子系統到 Prefab
2. 在需要時實例化並播放它

一個爆炸效果通常需要兩個東西：火 和 煙

## 煙粒子

創建一個新的 **Particle System** (Game Object -> Effect -> Particle System)

> 建議在場景空曠位置創建，以便你可以看得清楚它是什麼樣子的。如果你想在場景中把焦點放到對象上，你可以在 **Hierarchy** 窗口雙擊或在 **Scene** 窗口按 **F**

放大這個粒子系統，你會看到這個粒子對象在連續的發出火花：

![](assets/new_particle_system.png)

我們將使用下面這個精靈來做煙粒子：

![](assets/cloud.png)

導入素材，並修改其設置到如下圖所示:(設置 **Texture Type**爲 Default，勾選 **Alpha Is Transparent**)

![](assets/cloud_settings.png)

然後將此素材拖到粒子系統的的 **Inspector** 窗口中(也可以拖到層級窗口的 粒子系統節點上)

之後設置 Shader 屬性(Legacy Shaders -> Particles ->Alpha Blended)

![](assets/shader.png)

爲了做出完美的煙粒子，我們還需要對粒子系統的一些屬性進行設置



| 參數名 | 型別 | 值 |
| -------- | -------- | -------- |
| Duration     | General     | 1     |
| Max Particles     | General     | 15     |
| Start Lifetime     | General     | 1     |
| Start Color     | General     | Gray(#808080)     |
| Start Speed     | General     | 3     |
| Start Size	     | General     | 2     |
| Bursts     | Emission     |      |
| Shape     | Shape     | Sphere     |
| Color     | Color Over Lifetime     | 見後文N1     |
| Size     | Size Over Lifetime     | 見後文N2     |

### N1 - Color Over Lifetime

在最後設置一個透明點用來創造淡出效果

![](assets/fade_out.png)

### N2 - Size Over Lifetime

選擇減少的曲線(雙擊可創建一個關鍵點)

![](assets/decreasing_curve.png)

一切就緒後 你差不多會有這樣的設置
![](assets/smoke_settings.png)

你可以繼續微調直到你滿意效果，將其存儲爲名爲 **SmokeEffect** 的 Prefab，最終效果大概這樣：

![](assets/smoke_effect.gif)

## 火粒子

它並沒有什麼不同

1. 創建一個粒子系統
2. 使用默認素材即可，它已經滿足需求

爲它設置參數

| 參數名 | 型別 | 值 |
| -------- | -------- | -------- |
| Looping     | General     | flase     |
| Duration     | General     | Duration     |
| Max Particles     | General     | 10     |
| Start Lifetime     | General     | 1     |
| Start Speed     | General     | 0.5     |
| Start Size     | General     | 2     |
| Emission     | Bursts     |      |
| Shape     | Shape     | Box     |
| Color     | Color Over Lifetime     | 見後文N1     |

### N1 - Color Over Lifetime

創建一個從黃色到橙色的漸變，並在最後淡出

![](assets/fire_gradient.png)

效果：

![](assets/fire_effect.gif)

最後將其保存爲名爲 **FireEffect** 的 Prefab

# 助手腳本

實例化粒子 Prefabs 和實例化 射擊物是完全一樣的。然後，你需要記住，它們將在不再用到時被刪除掉。

而且我們將把火焰粒子和煙霧粒子結合起來作爲我們的爆炸效果。讓我們創建一個名 **SpecialEffectsHelper** 的腳本

```
#info="SpecialEffectsHelper.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialEffectsHelper : MonoBehaviour
{
    // 單例以供其它腳本使用
    public static SpecialEffectsHelper Instance;
    // 煙霧 prefab
    public ParticleSystem smokeEffect;
    // 火焰 prefab
    public ParticleSystem fireEffect;
    // Start is called before the first frame update
    void Awake()
    {
        // 此腳本應該只被添加到一個單獨實例上
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of SpecialEffectsHelper!");
        }
        // 初始化單例
        Instance = this;
    }

    // 創建爆炸效果
    public void Explosion(Vector3 position)
    {
        // 實例化煙霧
        instantiate(smokeEffect, position);

        // 實例化火焰
        instantiate(fireEffect, position);
    }
    // 實例化 粒子系統
    private ParticleSystem instantiate(ParticleSystem prefab, Vector3 position)
    {
        ParticleSystem newParticleSystem = Instantiate(
          prefab,
          position,
          Quaternion.identity
        ) as ParticleSystem;

        // Make sure it will be destroyed
        Destroy(
          newParticleSystem.gameObject,
          newParticleSystem.main.startLifetimeMultiplier
        );

        return newParticleSystem;
    }
}
```

把 **SpecialEffectsHelper** 添加到 Scripts 節點並設置好 爆炸用的 Prefab

![](assets/filling_script.png)

# 炸掉那個東西

最後我們編譯下 **HealthScript** 腳本 在角色死亡時使用 SpecialEffectsHelper 單例創建粒子效果

```
#info="HealthScript.cs"
	public void Damage(int damageCount)
	{
			hp -= damageCount;
			if (hp <= 0)
			{
					// 粒子效果
					SpecialEffectsHelper.Instance.Explosion(transform.position);
					// 死亡
					Destroy(gameObject);
			}
	}
```

運行遊戲去消滅敵人或被敵人消滅來看看效果

![](assets/explosions.png)