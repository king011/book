# [container](https://golang.org/pkg/container/)

container 提供了一些常用的容器

* heap 最小/大堆
* list 雙向鏈表
* ring 環形鏈表

# [heap 最小/大堆](https://golang.org/pkg/container/heap/)

* 堆 總是一顆完整的二叉樹 通常以數組形式存儲
* 對於每個元素值 如果始終小於其子節點的值則爲最小堆
* 對於每個元素值 如果始終大於其子節點的值則爲最大堆
* 索引 0 的元素 對於最小堆是最小值 對於最大堆是最大值
* 堆很適合用來製作優先隊列

golang 的最小堆需要實現 sort.Interface 接口 如果需要最大堆 反向實現 sort.Interface 即可

```
type Interface interface {
    sort.Interface
    Push(x interface{}) // add x as element Len()
    Pop() interface{}   // remove and return element Len() - 1.
}
```

```
package main

import (
	"container/heap"
	"fmt"
)

type Task struct {
	Priority int
}
type TaskHeap []Task

func (a TaskHeap) Len() int      { return len(a) }
func (a TaskHeap) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

// 最大堆
func (a TaskHeap) Less(i, j int) bool { return a[i].Priority > a[j].Priority }
func (h *TaskHeap) Push(x interface{}) {
	*h = append(*h, x.(Task))
}
func (h *TaskHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func main() {
	var tasks TaskHeap
	for i := 0; i < 10; i++ {
		tasks = append(tasks, Task{Priority: i})
	}
	// 初始化堆
	heap.Init(&tasks)
	fmt.Println(tasks)

	// 在堆中元素值變化後 調用 Fix 調整堆
	tasks[5].Priority = 20
	heap.Fix(&tasks, 5)
	fmt.Println(tasks)

	// 刪除堆中元素
	heap.Remove(&tasks, 5)
	fmt.Println(tasks)

	// 向堆中添加一個元素
	heap.Push(&tasks, Task{Priority: 7})
	fmt.Println(tasks)

	// 刪除 堆中元素
	for len(tasks) != 0 {
		fmt.Println(heap.Pop(&tasks))
	}
}
```

# [list 雙向鏈表](https://golang.org/pkg/container/list/)

* 雙向鏈表 節點(Element) 互相連接構成
* 每個節點的 Next 指向 下個節點
* 每個節點的 Prev 指向上個節點
* 雙向連接很適合隨機刪除和插入元素(只需要 調整與元素關聯的 上個元素和下個元素 以及本元素的 Next Prev 即可)
* 鏈表的問題是 節點的 創建釋放需要頻繁申請釋放內存  此外鏈表不支持隨機訪問 只能通過 Next Prev 順序訪問

```
// Element is an element of a linked list.
type Element struct {
	// Next and previous pointers in the doubly-linked list of elements.
	// To simplify the implementation, internally a list l is implemented
	// as a ring, such that &l.root is both the next element of the last
	// list element (l.Back()) and the previous element of the first list
	// element (l.Front()).
	next, prev *Element

	// The list to which this element belongs.
	list *List

	// The value stored with this element.
	Value interface{}
}
```

```
package main

import (
	"container/list"
	"fmt"
)

func main() {
	l := list.New()
	for i := 0; i < 10; i++ {
		l.PushBack(i)
	}
	for ele := l.Front(); ele != nil; ele = ele.Next() {
		fmt.Println(ele.Value)
	}
}
```

# [ring 環形鏈表](https://golang.org/pkg/container/ring/)

* 環形鏈表 是一個首尾相連的 雙向鏈表

ring 和上文鏈表的 element 結構類似


```
type Ring struct {
	next, prev *Ring
	Value      interface{} // for use by client; untouched by this library
}
```

```
package main

import (
	"container/ring"
	"fmt"
)

func main() {
	// 初始化一個包含3個元素的環
	r := ring.New(3)
	for i := 0; i < r.Len(); i++ {
		r.Value = i
		r = r.Next()
	}

	// 從當前位置遍歷環
	r.Do(func(i interface{}) {
		fmt.Print(i, `,`)
	})
	fmt.Println()

	// 從當前位 移動環
	r = r.Move(-1)
	r.Do(func(i interface{}) {
		fmt.Print(i, `,`)
	})
	fmt.Println()

	// 將連個環合併 到當前位置之後
	r.Link(ring.New(2))
	r.Do(func(i interface{}) {
		fmt.Print(i, `,`)
	})
	fmt.Println()

	// 刪除當前位置之後的 n個元素
	r.Unlink(2)
	r.Do(func(i interface{}) {
		fmt.Print(i, `,`)
	})
	fmt.Println()
}
```