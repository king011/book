# 由容器創建映像檔
使用 sudo docker commit 可以將 容器當前狀態 創建爲一個 映像檔
```sh
$ sudo docker commit -m "date >> home/a" -a "測試" a3cf463b0669 king/ubuntu:v1
sha256:a7b600fc2829cad9c96094d1176e91c679d4b3a0e7afcf797d4686aaa52a1dcb
```
* -m 指定提交信息
* -a 通常指定修改了的內容
* a3cf463b0669 是 容器 id
* sha256:a7b600fc2829cad9c96094d1176e91c679d4b3a0e7afcf797d4686aaa52a1dcb 是新 映像檔 id

# 複製一個 映像檔
使用 **docker&nbsp;tag&nbsp;SOURCE\_IMAGE\[:TAG\]&nbsp;TARGET\_IMAGE\[:TAG\]** 可以 創建一個 映像檔的 副本

```sh
# 將 映像檔 ubuntu:test 改名 爲 ubuntu:v1
docker tag ubuntu:test ubuntu:v1
docker rmi ubuntu:test
```

# Dockerfile
Dockerfile 是一個 docker 腳本 可以自動 以某個 映像檔 作爲基礎 建立新的 映像檔

## Dockerfile 基本的語法是
1. 使用#來註釋
2. FROM 指令告訴 Docker 使用哪個映像檔作為基底
3. MAINTAINER 指定 commint 信息
4. RUN開頭的指令會在建立中執行
```sh
mkdir v1 && vi v1/Dockerfile
```
**Dockerfile**
```txt
# 測試 Dockerfile
FROM ubuntu:18.04
MAINTAINER 後面是 commint 信息
# 執行 的指令
RUN echo `date` > /home/a
```

執行 docker build  創建新 映像檔
```sh
$ sudo docker build -t="king/ubuntu:v1" v1
Sending build context to Docker daemon 2.048 kB
Step 1/3 : FROM ubuntu:18.04
 ---> 16508e5c265d
Step 2/3 : MAINTAINER 後面是 commint 信息
 ---> Running in e61971aeb94b
 ---> efe4f63e3115
Removing intermediate container e61971aeb94b
Step 3/3 : RUN echo `date` > /home/a
 ---> Running in af9502295f35
 ---> 0ddfde02d309
Removing intermediate container af9502295f35
Successfully built 0ddfde02d309
```

> -t 指定 tag
> 
> v1 是 Dockerfile 檔案夾所在目錄

# 存儲 與 載入
使用 dockr save 可以將 一個 映像檔 保存爲 tar 檔案
```sh
sudo docker save -o v1.tar king/ubuntu:v1
```
docker load 可以 將 tar 檔案 恢復爲 映像檔
```sh
sudo docker load --input v1.tar
```