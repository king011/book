# Dockerfile

Dockerfile 是一個文本檔案 記錄類 如果 構造 docker 的指令，每條指令 都會構建一層

```
FROM nginx
RUN echo '<h1>Hello, Docker!</h1>' > /usr/share/nginx/html/index.html
```

# FROM 指定基礎鏡像

FROM 可以指定一個基礎鏡像 讓構建工作以此爲基礎

scratch 是一個虛擬概念 這會讓你從一個 空白的鏡像開始構建

```
FROM scratch
...
```

向 etcd 等 golng 實現程序 不需要操作系統提供運行時支持 所有需要的都包含在 可執行檔案裏了 通常會從 scratch 開始構建，這樣可以讓鏡像更小巧

# RUN 執行指令

RUN 指令用來執行命令 RUN 接受兩種使用方式

1. shell模式 -> RUN 命令 -> 類似在 shell 中輸入命令

	```
	RUN echo '<h1>Hello, Docker!</h1>' > /usr/share/nginx/html/index.html
	```
	
2. exec模式 -> RUN ["命令","參數1","參數2"...] -> 類似 函數調用的方式


因爲 每條指令 docker 都會構建一層 故下面的寫法不推薦 會創建太多無效的層 增加鏡像體積並增加構建時間

```
#info="錯誤寫法"
FROM debian:stretch

RUN apt-get update
RUN apt-get install -y gcc libc6-dev make wget
RUN wget -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz"
RUN mkdir -p /usr/src/redis
RUN tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1
RUN make -C /usr/src/redis
RUN make -C /usr/src/redis install
```

應該改成如下寫法 使用 docker 只創建一層來執行命令

```
#info="正確寫法"
FROM debian:stretch

RUN set -x; buildDeps='gcc libc6-dev make wget' \
    && apt-get update \
    && apt-get install -y $buildDeps \
    && wget -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz" \
    && mkdir -p /usr/src/redis \
    && tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1 \
    && make -C /usr/src/redis \
    && make -C /usr/src/redis install \
    && rm -rf /var/lib/apt/lists/* \
    && rm redis.tar.gz \
    && rm -r /usr/src/redis \
    && apt-get purge -y --auto-remove $buildDeps
```

# 構建鏡像

執行 docker build 構建鏡像

```
docker build -t nginx:v3 .
```

# Copy 複製檔案

Copy 將構建上下文的檔案 複製到 容器中 同樣支持兩種寫法

* Copy COPY \[\-\-chown=&lt;user&gt;:&lt;group&gt;\] 源路徑1 源路徑2 ... 目標路徑
* COPY \[\-\-chown=&lt;user&gt;:&lt;group&gt;\] \["源路徑1", "源路徑2", ... ,"目標路徑"\]

1. 源路徑 可以是檔案或檔案夾 並且可以有多個源 同時支持 通配符
2. 如果源爲檔案夾，則將檔案夾的內容複製到目標路徑

```
COPY package.json /usr/src/app/
```

通配符要符合 go 的 filepath.Match 規則

```
COPY hom* /mydir/
COPY hom?.txt /mydir/
```

```
COPY --chown=55:mygroup files* /mydir/
COPY --chown=bin files* /mydir/
COPY --chown=1 files* /mydir/
COPY --chown=10:11 files* /mydir/
```

# Add 下檔案

Add 用法類型 Copy 但是
1. 源路徑可以是一個 URL docker 會自動下載 並設置默認權限 0600
2. 如果 源是 tar打包的 gz bz2 xz 會自動解壓內容到目標路徑
3. ADD 會使構建緩存失效 從而使用構建變慢 故不甚推薦

```
FROM scratch
ADD ubuntu-xenial-core-cloudimg-amd64-root.tar.gz /
...
```

```
ADD --chown=55:mygroup files* /mydir/
ADD --chown=bin files* /mydir/
ADD --chown=1 files* /mydir/
ADD --chown=10:11 files* /mydir/
```

# CMD 容器啓動命令

CMD 指令格式類似 RUN 用於 指定容器的默認啓動命令

```
# 啓動容器時 可以傳入 命令替代默認的 CMD 命令
docker run -it ubuntu cat /etc/os-release
```

* shell 模式 -> CMD 命令
* exec 模式 -> CMD ["命令","參數1","參數2"...]

```
CMD echo $HOME
```
上述 shell 模式 實際會被轉換爲

```
CMD [ "sh", "-c", "echo $HOME" ]
```

故 推薦 直接寫 exec 模式

# ENTRYPOINT 入口點

ENTRYPOINT 格式同 RUN 用於指定 容器入口點 docker run 時可以通過 --entrypoint 來修改

ENTRYPOINT 用於把容器 改造的看起來像一個普通的命令

的 指定了 ENTRYPOINT 則 CMD 的內容將作爲 參數 傳遞給 容器啓動進程 此時 容器 實際上執行的是
```
<ENTRYPOINT> "<CMD>"
```


1. 創建一個 新鏡像

	```
	FROM ubuntu:18.04
	RUN apt-get update \
			&& apt-get install -y curl \
			&& rm -rf /var/lib/apt/lists/*
	ENTRYPOINT [ "curl", "-s", "http://myip.ipip.net" ]
	```
2. 構造

	```
	docker build -t myip .
	```
	
3. 此時 可以將CMD 作爲參數傳遞給容器進程

	```
	docker run myip curl -s http://myip.ipip.net -i
	```
	
另外一個使用場景是 先使用 root 去執行一些初始化操作 此時可以將 初始化腳本 寫入到 ENTRYPOINT，在腳本最後 使用 CMD 內容作爲進容器進程運行

```
FROM alpine:3.4
...
RUN addgroup -S redis && adduser -S -G redis redis
...
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 6379
CMD [ "redis-server" ]
```

```
#info="docker-entrypoint.sh"
#!/bin/sh
...
# allow the container to be started with `--user`
if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
    find . \! -user redis -exec chown redis '{}' +
    exec gosu redis "$0" "$@"
fi

exec "$@"
```

# ENV 環境變量

ENV 用於定義環境變量 支持兩種寫法

1. ENV key value
2. ENV key1=value1 key2=value2 ...

```
ENV VERSION=1.0 DEBUG=on \
    NAME="Happy Feet"
```

ADD、COPY、ENV、EXPOSE、FROM、LABEL、USER、WORKDIR、VOLUME、STOPSIGNAL、ONBUILD、RUN 指令都支持 環境變量的展開

# ARG 構建參數

ARG 類似 ENV 但 ARG 構建的環境變量只在構建時可用 在最終運行的容器進程中無法訪問

> 不要使用 ARG 保存密碼 因爲 在 docker history 中依然可以查看 ARG 設定

在 docker build 時 使用 --build-arg key=value 可以覆蓋 ARG 默認設定

下述構建無法打印DOCKER_USERNAME 因爲在 FROM 前指定 ARG 到 FROM 結束就會失效
```
ARG DOCKER_USERNAME=library

FROM ${DOCKER_USERNAME}/alpine

RUN set -x ; echo ${DOCKER_USERNAME}
```

```
# 只在 FROM 中有效
ARG DOCKER_USERNAME=library

FROM ${DOCKER_USERNAME}/alpine

# 要想在 FROM 之後使用，必須再次指定
ARG DOCKER_USERNAME=library

RUN set -x ; echo ${DOCKER_USERNAME}
```

#  VOLUME 定義匿名卷

通常會爲數據庫之類的持久化容器 定義匿名卷 這樣當用戶運行容器時忘記指定 掛接的卷 就會將內容 寫入到匿名卷中 而非容器中 從而保證不會向容器存儲寫入大量數據

* VOLUME ["路徑1", "路徑2"...]
* VOLUME 路徑

```
VOLUME /data
```

# EXPOSE 暴露端口

* EXPOSE 端口1 端口2 ...

EXPOSE 用來聲明 容器打算提供的 服務端口，但不會執行映射

`docker run -P` 會讀取 EXPOSE 將宿主機的隨機端口 映射到 EXPOSE 暴露的端口

# WORKDIR 工作目錄

* WORKDIR 工目錄

WORKDIR 用於指定 後續容器構造時 容器內部的工作目錄,如果目錄不存在會自動創建


```
WORKDIR /a
WORKDIR b
WORKDIR c

RUN pwd
```
pwd 輸出爲 /a/b/c

# USER

* USER 用戶名:用戶組

USER 用來指定後續構建時 容器使用的 用戶

```
RUN groupadd -r redis && useradd -r -g redis redis
USER redis
RUN [ "redis-server" ]
```

# HEALTHCHECK 健康檢測

* HEALTHCHECK 選項 CMD 命令 -> 設置健康檢測
* HEALTHCHECK NONE -> 屏蔽設置的健康檢測

docker 1.2 加入了 健康檢測

HEALTHCHECK 支持下述 選項

* --interval=间隔 -> 設置健康檢測間隔 默認30秒
* --timeout=時長 -> 健康檢測超時時間默認30秒
* --retries=次數 -> 當連續失敗多少次任務容器不健康 默認3次

檢測指令返回值:
* 0 成功
* 1 失敗
* 2 保留

# ONBUILD

* ONBUILD 其它指令

ONBUILD 是一個特殊指令 用 ONBUILD 指定的指令 在本身構建時不會執行 只有在 其它鏡像以當前鏡像爲 基礎構建時才會被執行

```
FROM node:slim
RUN mkdir /app
WORKDIR /app
ONBUILD COPY ./package.json /app
ONBUILD RUN [ "npm", "install" ]
ONBUILD COPY . /app/
CMD [ "npm", "start" ]
```

下面的 新容器構建時 才會執行 ONBUILD 將 當前 node 項目 拷貝到 容器中
```
FROM my-node
```

# LABEL 元數據

LABEL 用來給鏡像以鍵值對的形式 增加一些元數據 比如 作者 協議之類的

```
LABEL <key>=<value> <key>=<value> <key>=<value> ...
```

```
LABEL org.opencontainers.image.authors="king"

LABEL org.opencontainers.image.documentation="https://xxx"
```

# SHELL

SHELL 指令用戶指定 構建時使用的 shell 環境 linux中默認爲 `["/bin/sh","-c"]`

```
SHELL ["/bin/sh", "-c"]
```