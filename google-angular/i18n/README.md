# 國際化

angular 提供了國際化工具 來把 網頁翻譯爲多種語言

```sh

ng add @angular/localize
```

build 時需要添加 --localize 參數
```sh
ng build --prod  --base-href "/angular/" --localize --lazy-modules
```

# 翻譯模板
在需要翻頁的 html 標籤上加入 i18n 屬性 以便告知 Angular 需要翻譯其內容

## 複數 候選 嵌套
可以 將 plural select 嵌套使用

```
<span i18n>Updated: {minutes, plural,
  =0 {just now}
  =1 {one minute ago}
  other {{{minutes}} minutes ago by {gender, select, male {male} female {female} other {other}}}}
</span>
```

## select 候選文本

有時要將組建屬性 翻譯爲不同 文本 這可以使用 select 語法 和 plural 類似  
不過 此時是將 字符串 翻譯爲 其它值

```
<span i18n>The author is {gender, select, male {male} female {female} other {other}}</span>
```

## plural 翻譯複數

在某些語言中 對於 單數 和 複數 使用了 不同的 詞彙 Angular 對此 提供了很好的 支持

比如 要翻譯
```
<span i18n>Updated {{minutes}} minutes ago</span>
```
通常 
* minutes==0 英文顯示 Updated just now
* minutes==1 英文顯示 Updated one minute ago
* minutes>1  英文顯示 Updated x minutes ago

可以使用 複數 翻譯 語法
```
<span i18n>Updated {minutes, plural, =0 {just now} =1 {one minute ago} other {{{minutes}} minutes ago}}</span>
```
使用 {} 將 複數內容 擴起來 之後 使用 組建屬性  plural **複數值 {翻譯內容}** 指定 複數翻譯

## 翻譯屬性
Angular 也支持翻譯 屬性 在要 翻譯的 屬性 前加上 i18n-屬性名 即可

i18n-屬性名 的用法 完全同 i18n
```
<img [src]="logo" i18n-title title="Angular logo" />
```

## 翻譯文本 而非標籤

你可以將 待翻譯文件放到 ng-containe 標籤中 這樣 html 不會創建一個 新標籤  
ng-container 會被 Angular 轉爲註釋
```
<ng-container i18n>I don't output any element</ng-container>
```

## id
Angular 通過 id 來識別 待翻譯條目 id的值由 上下文自動生成 這意味這 如果 改變了代碼 可以id會變化  
這意味着 翻譯文件中 已經翻譯內容 需要再次 翻譯 你可以 通過 設置 id 來 強制 Angular 和 翻譯工具 使用 你指定的id 而非 自動生成

**一定要 保證 不同 待翻譯內容 使用 不同的id 如果 使用來相同 id 只有 第一個找到的id 指定的內容 會被翻譯**

指定 id
```
<h1 i18n="@@introductionHeader">Hello i18n!</h1>
```

使用描述的同時 指定id
```
<h1 i18n="An introduction header for this sample@@introductionHeader">Hello i18n!</h1>
```

使用 含義 描述 時指定id
```
<h1 i18n="site header|An introduction header for this sample@@introductionHeader">Hello i18n!</h1>
```
## 含義
很多含義 在某種語言中 都用相同的文字描述 而在其他她它語言中 使用不同的文字  
你可以 再 i18n 中 寫上 含義  
Angular 這樣理解 i18n **i18n="<含義>|<描述>@@id"**
```
<h1 i18n="site header|An introduction header for this sample">Hello i18n!</h1>
```
Angular 會認爲 相同 含義的 相同待翻譯 文字 是相同的 爲其生成同一個 id 而爲 不同 含義的 相同待翻譯 文字 各自生成其自己的 id

## 描述
可以再 i18n 中 寫入待翻譯文本的描述 以便 翻譯人員 更好的理解要 翻譯的 內容

```
<h1 i18n="An introduction header for this sample">Hello i18n!</h1>
```

# angular.json
需要在 angular.json 中 配置必要選項

projects.view.i18n 配置 支持的 i18n

```
{
  "projects": {
    "view": {
      "i18n": {
        "sourceLocale": "en",
        "locales": {
          "en-US": "src/locale/en-US.xlf",
          "zh-Hant": "src/locale/zh-Hant.xlf",
          "zh-Hans": "src/locale/zh-Hans.xlf"
        }
      },
  }
	...
}
```

projects.architect.build.builder.options 可以配置 開發時使用的翻譯檔案

```
{
  "projects": {
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "localize": [
              "zh-Hant"
            ],
            "i18nMissingTranslation": "warning",
          },
       }
    }
  },
}
```

