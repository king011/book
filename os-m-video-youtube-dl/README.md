# youtube-dl

youtube-dl 是 python 寫的一個開源工具，最初用於從 youtube 下載視頻，現在也支持 youtube 之外的網站

* 官網 [https://yt-dl.org/](https://yt-dl.org/)
* 源碼 [https://github.com/ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl)
