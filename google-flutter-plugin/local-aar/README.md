# local aar

如果要一個 插件使用了 aar 庫 flutter 不支持 local aar 因爲本地的 aar 不會作爲資源被正確的打包

此時有兩個方案

1. 把 aar 部署到 遠程倉庫
2. 在本地建立一個倉庫 並上傳 aar 以供一些私有aar的使用

本文主要說明如何建立本地的私有倉庫

1. 首先需要安裝 maven 工具

	```
	sudo apt install maven
	```

2. 將 aar 複雜到 **~/.m2/repository**

	```
	cp jscoll.aar ~/.m2/repository/ && cd ~/.m2/repository
	```

3. 在當前目錄下創建 庫版本

	```
	 mvn deploy:deploy-file -Dfile=jscoll.aar -Durl="file://." -DgroupId="king011.com" -DartifactId="jscoll" -Dversion="0.0.2"
	```
	
	* -Dfile arr 檔案名
	* -Durl 倉庫根路徑
	* -DgroupId 組名
	* -DartifactId 標識名
	* -Dversion 版本號

現在有了私有倉庫 並創建好了版本 修改 **your\_plugin/android/build.gradle** 中的配置

```
rootProject.allprojects {
    repositories {
        // 這是新加的行 優先從 maven local 查找依賴
        mavenLocal()

        google()
        mavenCentral()
    }
}

...

dependencies {
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    // 這是新加的行 指定 maven local 中的 軟體版本
    implementation "king011.com:jscoll:0.0.2"
}
```
