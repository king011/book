# [sort](https://pkg.go.dev/sort)

sort 包提供了排序相關功能

# Interface

這個包最核心的功能就是 Sort 函數它將實現了 Interface 接口的元素進行[快速排序](https://arxiv.org/pdf/2106.05123.pdf)(pattern-defeating quicksort 但是沒有 BlockQuicksort 優化 golang v1.19.5)

```
type Interface interface {
	// 返回數組長度
	Len() int

	// 如果數組索引 i 的元素 小於 索引 j 的元素 就返回 true
	Less(i, j int) bool

	// 交互索引 i 和 j  處的元素
	Swap(i, j int)
}
```

```
package main

import (
	"fmt"
	"sort"
)

func main() {
	vals := []int{5, 4, 7, 543, 231, 123, 43}
	// 對 int float64 string 提供了快捷函數
	sort.Ints(vals)
	// sort.Float64s()
	// sort.Strings()
	fmt.Println(vals)

	// Reverse 可以逆反一個 Interface 接口，相當於反向排序
	sort.Sort(sort.Reverse(sort.IntSlice(vals)))
	fmt.Println(vals)

}
```

# Slice

在 go 支持模板後(go1.8)，添加了 Slice/SliceStable SliceIsSorted 函數使用模板來爲數組排序

```
package main

import (
	"fmt"
	"sort"
)

func main() {
	vals := []int{5, 4, 7, 543, 231, 123, 43}
	sort.Slice(vals, func(i, j int) bool {
		return vals[i] < vals[j]
	})

	fmt.Println(vals)
}
```

# Search

```
func Search(n int, f func(int) bool) int
```

Search 在已排序的數組中，使用二分法在 **\[0,n\)** 的範圍內查找 f 返回 true 的最小索引值，注意未找到返回值不是 -1 而是 **\[0,n\]**

```
x := 23
i := sort.Search(len(data), func(i int) bool { return data[i] >= x })
if i < len(data) && data[i] == x {
	// 找到了 x ，它在數組中
} else {
	// x 不在數組中，
	// 但是如果要加入到數組中，可以將它插入到 i 處以保持數組有序
}
```

一個常見的場景是數組中保存了一系列規格，你需要從這些規格中找出一個與實際需求最接近的規則，比如可以修建 3m 6m 9m 12m 的車道保證車輛通行，已經必須保證寬度爲 x 的車輛通行，求車道最小寬度

```
package main

import (
	"fmt"
	"sort"
)

func main() {
	var x int
	vals := []int{3, 6, 9, 12}
	for {
		fmt.Print("input x: ")
		fmt.Scan(&x)
		i := sort.Search(len(vals), func(i int) bool { return vals[i] >= x })
		if i >= len(vals) {
			fmt.Println(`not matched`)
		} else {
			// 找到的值是 >= x 中最小的一組
			fmt.Println(`matched:`, vals[i])
		}
	}
}
```

類似的如果要找到 <= x 中最大的一組值(例如有一個籃子x，要向裏面裝入一個目前已有的最大尺寸能放進去的球)，則在 f 函數使用 <= 並且數組按照降序排列即可

```
package main

import (
	"fmt"
	"sort"
)

func main() {
	var x int
	vals := []int{12, 9, 6, 3}
	for {
		fmt.Print("input x: ")
		fmt.Scan(&x)
		i := sort.Search(len(vals), func(i int) bool { return vals[i] <= x })
		if i >= len(vals) {
			fmt.Println(`not matched`)
		} else {
			// 找到的值是 <= x 中最大的一組
			fmt.Println(`matched:`, vals[i])
		}
	}
}
```

# Find

```
func Find(n int, cmp func(int) int) (i int, found bool)
```

Find 類似 Search 只是它多了給返回值表示元素是否在數組中，同時 f 函數 返回一個 int 來比較是否相等

```
x := 23
i, found := sort.Find(len(data), func(i int) int {
	fmt.Println(i)
	return x - data[i]
})
if found {
	// 找到了 x ，它在數組中
} else {
	// x 不在數組中，
	// 但是如果要加入到數組中，可以將它插入到 i 處以保持數組有序
}
```

上面 Search 中找到的值是 >= x 中最小的一組可以使用如下代碼

```
package main

import (
	"fmt"
	"sort"
)

func main() {
	var x int
	vals := []int{3, 6, 9, 12}
	for {
		fmt.Print("input x: ")
		fmt.Scan(&x)
		i, equal := sort.Find(len(vals), func(i int) int { return x - vals[i] })
		if i >= len(vals) {
			fmt.Println(`not matched`)
		} else {
			// 找到的值是 >= x 中最小的一組
			fmt.Println(`matched:`, vals[i], equal)
		}
	}
}
```

上面 Search 中找到的值是 <= x 中最大的一組可以使用如下代碼

```
package main

import (
	"fmt"
	"sort"
)

func main() {
	var x int
	vals := []int{12, 9, 6, 3}
	for {
		fmt.Print("input x: ")
		fmt.Scan(&x)
		i, equal := sort.Find(len(vals), func(i int) int { return vals[i] - x })
		if i >= len(vals) {
			fmt.Println(`not matched`)
		} else {
			// 找到的值是 <= x 中最大的一組
			fmt.Println(`matched:`, vals[i], equal)
		}
	}
}
```