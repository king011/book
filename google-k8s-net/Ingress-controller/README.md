# [Ingress 控制器](https://kubernetes.io/zh/docs/concepts/services-networking/ingress-controllers/)

爲了讓 Ingress 資源正常工作，集羣必須有一個正在運行的 Ingress 控制器

Kubernetes 目前維護了 [AWS](https://github.com/kubernetes-sigs/aws-load-balancer-controller#readme) [GCE](https://git.k8s.io/ingress-gce/README.md) [Nginx Ingress](https://git.k8s.io/ingress-nginx/README.md#readme) 三個控制器

當然也可以選擇一些第三方的控制器

# [ingress-nginx](https://github.com/kubernetes/ingress-nginx/blob/main/README.md#readme)

[ingress-nginx](https://github.com/kubernetes/ingress-nginx/blob/main/docs/deploy/index.md) 是一個使用 nginx 作爲反向代理的 Ingress 控制器

使用下述指令安裝 控制器，可以將[版本號](https://github.com/kubernetes/ingress-nginx/blob/main/README.md#support-versions-table)換成一個你需要的版本
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.3/deploy/static/provider/cloud/deploy.yaml
```

你可以執行下述指令等待 ingress-nginx 部署成功

```
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=120s
```

如果部署成功執行 get pods 會看到類似的 ingress-nginx pods

```
$ kubectl get pods --namespace ingress-nginx 
NAME                                        READY   STATUS      RESTARTS         AGE
ingress-nginx-admission-create-ddk8b        0/1     Completed   0                10h
ingress-nginx-admission-patch-v6pcz         0/1     Completed   1                10h
ingress-nginx-controller-69fbbf9f9c-j85hz   1/1     Running     13 (8m42s ago)   10h
```

* ingress-nginx-controller 處於 Running 狀態
* ingress-nginx-admission-patch 和 ingress-nginx-admission-create 處於 Completed 狀態

## 測試 ingress-nginx

1. 創建一個名爲 demo 的 web pod 並暴露爲 demo 服務

	```
	kubectl create deployment demo --image=httpd --port=80
	kubectl expose deployment demo
	```
	
2. 使用 ingress-nginx 創建一個 ingress 並將向域名 demo.localdev.me 的請求轉發給 demo:80

	```
	kubectl create ingress demo-localhost --class=nginx \
		--rule=demo.localdev.me/*=demo:80
	```
	
3. 使用 port-forward 將 主機 8080端口 轉發到 ingress-nginx-controller  的 80端口

	```
	kubectl port-forward --namespace=ingress-nginx service/ingress-nginx-controller 8080:80
	```
	
4. 通過 **http://demo.localdev.me:8080/** 訪問服務，如果正常應該會看到輸出 It works

## 在線測試

如果是一個真實的生成用的 Kubernetes 集羣並且支持 LoadBalancer 類型的服務，它將爲入口控制器分配一個外部 IP 地址 或 FQDN

使用下述命令查看 ip 地址或 FQDN
```
kubectl get service ingress-nginx-controller --namespace=ingress-nginx
```

你可以通過外部ip 來訪問服務，如果不支持則可以通過 nodePort 訪問

> 默認的 ingress-nginx 配置沒有指定 nodePort，所以 k8s 會使用 \[30000,32767\] 間的一個隨機端口

# [gloo](https://docs.solo.io/gloo-edge/latest/)

gloo 是一個基於 envoy 的 Ingress 控制器，此外 gloo 也支持以 Ingress 控制器之外的方式進行運行，但本文章以 linux 下安裝 [gloo ingress](https://docs.solo.io/gloo-edge/latest/installation/ingress/) 爲例

1. 安裝 **[glooctl](https://docs.solo.io/gloo-edge/latest/installation/preparation/)** 工具

	```
	curl -sL https://run.solo.io/gloo/install | sh
	export PATH=$HOME/.gloo/bin:$PATH
	```
	
	升級 glooctl
	```
	glooctl upgrade --release
	```

2. 安裝 Ingress

	```
	glooctl install ingress
	```

3. 查看 gloo 資源是否正常運行

	```
	kubectl get all -n gloo-system
	```

	```
	NAME                                       READY   STATUS    RESTARTS   AGE
	pod/ingress-proxy-6d786fd9f-4k5r4          1/1     Running   0          64s
	pod/discovery-55b8645d77-72mbt             1/1     Running   0          63s
	pod/gloo-9f9f77c8d-6sk7z                   1/1     Running   0          64s
	pod/ingress-85ffc7b77b-z6lsm               1/1     Running   0          64s

	NAME                           TYPE           CLUSTER-IP     EXTERNAL-IP       PORT(S)                      AGE
	service/ingress-proxy          LoadBalancer   10.7.250.225   35.226.24.166     80:32436/TCP,443:32667/TCP   64s
	service/gloo                   ClusterIP      10.7.251.47    <none>            9977/TCP                     4d10h

	NAME                                   DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
	deployment.apps/ingress-proxy          1         1         1            1           64s
	deployment.apps/discovery              1         1         1            1           63s
	deployment.apps/gloo                   1         1         1            1           64s
	deployment.apps/ingress                1         1         1            1           64s

	NAME                                             DESIRED   CURRENT   READY   AGE
	replicaset.apps/ingress-proxy-6d786fd9f          1         1         1       64s
	replicaset.apps/discovery-55b8645d77             1         1         1       63s
	replicaset.apps/gloo-9f9f77c8d                   1         1         1       64s
	replicaset.apps/ingress-85ffc7b77b               1         1         1       64s
	```

使用下述指令可卸載 gloo:

```
glooctl uninstall
```