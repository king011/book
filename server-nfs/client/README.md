# 客戶端配置

1. 確認本地 rpcbind.service 服務已經啓動
2. 如果伺服器啓動了 nfslock  客戶端 也需要啓動


```
#info="查詢指定服務器 可使用的資源"
[root@clientlinux ~]# showmount -e 192.168.100.254
Export list for 192.168.100.254:
/tmp         *
/home/linux  *.centos.vbird
/home/test   192.168.100.10
/home/public (everyone)
```

```
#info="掛接 nfs"
[root@clientlinux ~]# mount -t nfs 192.168.100.254:/home/public  /home/nfs/public
```

```
#info="卸載 nfs"
[root@clientlinux ~]# umount /home/nfs/public
```