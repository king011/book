# 動態檔案

envoy 可以支持動態檔案配置， envoy 會監控檔案當檔案替換時重新加載配置(注意 edit 和 copy 不會觸發 envoy 更新，需要 mv 替換才會觸發 envoy 更新配置，這是爲了保證檔案的完整性)

1. 配置一個 [node](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-field-config-bootstrap-v3-bootstrap-node) 節點
2. 配置 [dynamic\_resources](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/bootstrap/v3/bootstrap.proto#envoy-v3-api-field-config-bootstrap-v3-bootstrap-dynamic-resources) 指定動態檔案路徑

```
#info="envoy.yaml"
admin:
  access_log_path: "/dev/stdout"
  address:
    socket_address: { address: 0.0.0.0, port_value: 8080 }
node:
  cluster: test-cluster
  id: test-id
dynamic_resources:
  cds_config:
    path: /var/lib/envoy/cds.yaml # 配置集羣
  lds_config:
    path: /var/lib/envoy/lds.yaml # 配置監聽器
```

```
#info=" /var/lib/envoy/lds.yaml"
resources:
- "@type": type.googleapis.com/envoy.config.listener.v3.Listener
  name: listener_0
  address:
    socket_address:
      address: 0.0.0.0
      port_value: 10000
  filter_chains:
  - filters:
    - name: envoy.http_connection_manager
      typed_config:
        "@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
        stat_prefix: ingress_http
        http_filters:
        - name: envoy.router
          typed_config:
            "@type": type.googleapis.com/envoy.extensions.filters.http.router.v3.Router
        route_config:
          name: local_route
          virtual_hosts:
          - name: local_service
            domains:
            - "*"
            routes:
            - match:
                prefix: "/"
              route:
                host_rewrite_literal: www.envoyproxy.io
                cluster: example_proxy_cluster
```

```
#info="/var/lib/envoy/cds.yaml"
resources:
- "@type": type.googleapis.com/envoy.config.cluster.v3.Cluster
  name: example_proxy_cluster
  type: STRICT_DNS
  typed_extension_protocol_options:
    envoy.extensions.upstreams.http.v3.HttpProtocolOptions:
      "@type": type.googleapis.com/envoy.extensions.upstreams.http.v3.HttpProtocolOptions
      explicit_http_config:
        http2_protocol_options: {}
  load_assignment:
    cluster_name: example_proxy_cluster
    endpoints:
    - lb_endpoints:
      - endpoint:
          address:
            socket_address:
              address: www.envoyproxy.io
              port_value: 443
  transport_socket:
    name: envoy.transport_sockets.tls
    typed_config:
      "@type": type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.UpstreamTlsContext
      sni: www.envoyproxy.io
```

# [SDS](https://www.envoyproxy.io/docs/envoy/latest/configuration/security/secret)

使用 SDS 用於動態更新 https 證書
```
transport_socket:
    name: "envoy.transport_sockets.tls"
    typed_config:
      "@type": "type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.UpstreamTlsContext"
      common_tls_context:
        tls_certificate_sds_secret_configs:
          name: tls_sds
          sds_config:
            path: /etc/envoy/tls_certificate_sds_secret.yaml
        validation_context_sds_secret_config:
          name: validation_context_sds
          sds_config:
            path: /etc/envoy/validation_context_sds_secret.yaml
```

```
#info="/etc/envoy/tls_certificate_sds_secret.yaml:"
resources:
  - "@type": "type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.Secret"
    name: tls_sds
    tls_certificate:
      certificate_chain:
        filename: /certs/sds_cert.pem
      private_key:
        filename: /certs/sds_key.pem
```

```
#info="/etc/envoy/validation_context_sds_secret.yaml"
resources:
  - "@type": "type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.Secret"
    name: validation_context_sds
    validation_context:
      trusted_ca:
        filename: /certs/cacert.pem
```