# [ContextMenu](https://primeng.org/contextmenu)

```
import {ContextMenuModule} from 'primeng/contextmenu';
import {MenuItem} from 'primeng/api';
```

右鍵單擊目標時，ContextMenu 會顯示一個 overlay 菜單。請注意像 **DataTable** 這樣的組件與 ContextMenu 有特殊的集成。請參閱具有上下文菜單支持的各個文檔

![](assets/1.png)

下面例子中，ContextMenu 需要嵌套的菜單項作爲 model 屬性的值，同時以最簡單的方式附加到全局的 document 元素
```
<p-contextMenu [global]="true" [model]="items"></p-contextMenu>
```

## Target

ContextMenu 可以使用 **target** 指定要附加到的元素

```
<p-contextMenu [target]="img" [model]="items2" ></p-contextMenu>

<img #img src="https://primefaces.org/cdn/primeng/images/primeng.svg" alt="Logo">
```

## Exclusive Integrations

某些組件(如 Table) 需要特別注意，因此它們提供了一種不同的方法來附加 ContextMenu。請參閱具有特殊集成的組件的單獨文檔

```
export class ContextMenuDemo {

    private items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'Project'},
                            {label: 'Other'},
                        ]
                    },
                    {label: 'Open'},
                    {label: 'Quit'}
                ]
            },
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-trash'},
                    {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
                ]
            }
        ];
    }
}
```
# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onShow     | -     | 顯示時回調     |
| onHide     | -     | 隱藏時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單項數組     |
| global     | boolean     | false     | 如果爲 true，將菜單附加到全局 document     |
| target     | string     | null     | 菜單要附加的目標元素     |
| style     | string     | null     | 組件的內聯 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| appendTo     | any     | null     | Target element to attach the overlay, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| baseZIndex     | number     | 0     | 分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| triggerEvent     | string     | contextmenu     | Event for which the menu must be displayed.     |

# Methods

| Name | Parameters | Description |
| -------- | -------- | -------- |
| toggle     | event (optional): mouse event     | 切換顯示狀態     |
| show     | event: browser event     | 顯示彈出菜單     |
| hide     |      | 隱藏彈出菜單     |


# Styling

| Name | Element |
| -------- | -------- |
| p-contextmenu     | Container element.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
| p-submenu-icon     | Arrow icon of a submenu.     |


