# 輸出

```
#info="標準語法"
{{value}}
{{data.key}}
{{data['key']}}
{{a ? b : c}}
{{a || b}}
{{a + b}}
```

```
#info="元素語法"
<%= value %>
<%= data.key %>
<%= data['key'] %>
<%= a ? b : c %>
<%= a || b %>
<%= a + b %>
```

模板一級的特殊變量 使用 **$data** 加霞表訪問

```
{{$data['user list']}}
```

# 原文輸出

```
{{@ value }}
```
```
<%- value %>
```

# 條件

```
{{if value}} ... {{/if}}
{{if v1}} ... {{else if v2}} ... {{/if}}
```

```
<% if (value) { %> ... <% } %>
<% if (v1) { %> ... <% } else if (v2) { %> ... <% } %>
```

# 循環
```
{{each target}}
    {{$index}} {{$value}}
{{/each}}
```
```
<% for(var i = 0; i < target.length; i++){ %>
    <%= i %> <%= target[i] %>
<% } %>
```

1. target 支持 array 與 object 的迭代，其默認值爲 $data。
2. $value 與 $index 可以自定義：{{each target val key}}。

# 變量
```
{{set temp = data.sub.content}}
```
```
<% var temp = data.sub.content; %>
```

# 模板繼承

```
{{extend './layout.art'}}
{{block 'head'}} ... {{/block}}
```

```
<% extend('./layout.art') %>
<% block('head', function(){ %> ... <% }) %>
```

```
<!--layout.art-->
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{block 'title'}}My Site{{/block}}</title>

    {{block 'head'}}
    <link rel="stylesheet" href="main.css">
    {{/block}}
</head>
<body>
    {{block 'content'}}{{/block}}
</body>
</html>
```
```
<!--index.art-->
{{extend './layout.art'}}

{{block 'title'}}{{title}}{{/block}}

{{block 'head'}}
    <link rel="stylesheet" href="custom.css">
{{/block}}

{{block 'content'}}
<p>This is just an awesome page.</p>
{{/block}}
```

渲染後 index.art 將自動應用 layout.art 的佈局骨架

# 子模板

```
{{include './header.art'}}
{{include './header.art' data}}
```

```
<% include('./header.art') %>
<% include('./header.art', data) %>
```

# 過濾器

```
#info="註冊過濾器"

template.defaults.imports.dateFormat = function(date, format){/*[code..]*/};
template.defaults.imports.timestamp = function(value){return value * 1000};
```

```
{{date | timestamp | dateFormat 'yyyy-MM-dd hh:mm:ss'}}
```

```
<%= $imports.dateFormat($imports.timestamp(date), 'yyyy-MM-dd hh:mm:ss') %>
```