# oauth2

```
go get golang.org/x/oauth2
```

golang.org/x/oauth2 包含了 OAuth 2.0 規範實現的客戶端


```
#info="conf.go"
package main

const (
	authorizeURI  = `https://github.com/login/oauth/authorize`
	grantURI      = `https://github.com/login/oauth/access_token`
	redirectURI   = `http://dev.my.web/oauth/redirect`
	clientID      = `xxx`
	clientSecrets = `xxx`
)
```

```
#info="main.go"
package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/oauth2"
)

func main() {
	e := http.ListenAndServe(`:9000`, NewMux())
	if e != nil {
		log.Fatalln(e)
	}
}

type Mux struct {
	*http.ServeMux
	conf *oauth2.Config
}

func NewMux() *Mux {
	mux := &Mux{
		ServeMux: http.NewServeMux(),
		conf: &oauth2.Config{
			ClientID:     clientID,
			ClientSecret: clientSecrets,
			RedirectURL:  redirectURI,
			Scopes:       nil, // 申請權限
			Endpoint: oauth2.Endpoint{
				AuthURL:  authorizeURI, // 獲取授權碼地址
				TokenURL: grantURI,     // 由授權碼獲取訪問令牌地址
			},
		},
	}
	mux.HandleFunc(`/`, mux.home)
	mux.HandleFunc(`/oauth/redirect`, mux.callbackURL)
	mux.HandleFunc(`/user`, mux.user)
	return mux
}

func (mux *Mux) home(w http.ResponseWriter, req *http.Request) {
	w.Header().Set(`Content-Type`, `text/html; charset=utf-8`)
	href := mux.conf.AuthCodeURL(``)
	fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<title>登入</title>
<body>
	<a href="%s">
		Login with github
	</a>
</body>

</html>`, href)
}
func (mux *Mux) callbackURL(w http.ResponseWriter, req *http.Request) {
	token, e := mux.conf.Exchange(context.Background(), req.URL.Query().Get(`code`))
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(e.Error()))
		return
	}
	b, e := json.Marshal(token)
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(e.Error()))
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:  `token`,
		Value: base64.RawURLEncoding.EncodeToString(b),
		Path:  `/`,
	})
	http.Redirect(w, req, `/user`, http.StatusFound)
}

func (mux *Mux) user(w http.ResponseWriter, req *http.Request) {
	token, e := req.Cookie(`token`)
	if e != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(e.Error()))
		return
	}
	b, e := base64.RawURLEncoding.DecodeString(token.Value)
	if e != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(e.Error()))
		return
	}
	var ot oauth2.Token
	e = json.Unmarshal(b, &ot)
	if e != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(e.Error()))
		return
	}
	client := mux.conf.Client(context.Background(), &ot)
	username, statusCode, e := mux.requestUser(client)
	if e != nil {
		w.WriteHeader(statusCode)
		w.Write([]byte(e.Error()))
		return
	}
	fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<title>歡迎</title>
<body>
		歡迎 %s
</body>

</html>`, username)
}

func (mux *Mux) requestUser(client *http.Client) (username string, statusCode int, e error) {
	statusCode = http.StatusInternalServerError
	req, e := http.NewRequest(http.MethodGet, `https://api.github.com/user`, nil)
	if e != nil {
		return
	}
	header := req.Header
	header.Set(`accept`, `application/json`)
	resp, e := client.Do(req)
	if e != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		statusCode = resp.StatusCode
		e = errors.New(resp.Status)
		return
	}

	var tmp struct {
		Username string `json:"login"`
	}
	decoder := json.NewDecoder(resp.Body)
	e = decoder.Decode(&tmp)
	if e != nil {
		return
	}
	username = tmp.Username
	statusCode = http.StatusOK
	return
}
```