# wsl

wsl 既 Windows Subsystem for Linux 的簡寫 

是微軟最初爲了在 windows 10 和 Windows Server 2019 上執行 Linux 二進制檔案的 相容層

* 官網 [https://blogs.msdn.microsoft.com/wsl/](https://blogs.msdn.microsoft.com/wsl/)
* 文檔 [https://docs.microsoft.com/zh-cn/windows/wsl/](https://docs.microsoft.com/zh-cn/windows/wsl/)

# wsl2

最初的 wls 微軟提供了一個轉譯層 來轉換 linux 與 windows 底層系統呼叫 此方案效率頗低已被放棄

新的 wsl2 直接在 Hyper-V 上運行了一個 Linux 核心 速度和啓動速度都快很多