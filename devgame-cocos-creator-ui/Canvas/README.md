# [設計分辨率](http://docs.cocos.com/creator/manual/zh/ui/multi-resolution.html)

移動平臺有 多種 屏幕分辨率 爲此 creator 提供了 Canvas 組件

當屏幕實際分辨率 不同於 Canvas 分辨率時 Canvas 會自動 進行適當的 縮放 

故 應該 設置一個 Canvas 作爲 場景 root 節點 並將其分辨率設置爲 設計分辨率 之後 將渲染內容 都 作爲 Canvas的 子孫節點

# [Canvas](http://docs.cocos.com/creator/manual/zh/components/canvas.html)
[cc.Canvas](http://docs.cocos.com/creator/api/zh/classes/Canvas.html) 是一個 畫布 能夠 適當的將 畫布內容 縮放到 屏幕

![](assets/default.png)



| 選項 | 說明 |
| -------- | -------- |
| Design Resolution     | Canvas 尺寸 通常應該和 設計分辨率一致     |
| Fit Height     | 適配高度     |
| Fit Width     | 適配寬度     |


