# Protocol Buffers

Protocol Buffers 是 google 開源\(BSD\) 的一個 快速 高效 數據緊湊的 序列化 組件 官方提供了 多種語言\(go c++ c\# java python\)的實現

* 官網 [https://developers.google.com/protocol-buffers/](https://developers.google.com/protocol-buffers/)
* wiki [https://zh.wikipedia.org/wiki/Protocol\_Buffers](https://zh.wikipedia.org/wiki/Protocol_Buffers)
* 源碼 [https://github.com/google/protobuf](https://github.com/google/protobuf)

# \*.proto

\*.proto 檔案用於 定義 Protocol Buffers 序列化的數據結構