# libuv

libuv 是爲 node 項目開發的一個開源(MIT)純 c 的異步io 事件循環。對於不同平臺選用該平臺最適合的 異步模型 epoll kqueue iocp ...

* 官網 [http://docs.libuv.org/en/v1.x/](http://docs.libuv.org/en/v1.x/)
* 源碼 [https://github.com/libuv/libuv](https://github.com/libuv/libuv)
* api [http://docs.libuv.org/en/v1.x/api.html](http://docs.libuv.org/en/v1.x/api.html)
* 用戶手冊 [http://docs.libuv.org/en/v1.x/guide.html](http://docs.libuv.org/en/v1.x/guide.html)
