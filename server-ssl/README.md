# SSL 證書

TLS (Transport Layer Security) 傳輸層安全協定 及其前身體 SSL(Secure Sockets Layer) 是一種 安全協定 用於提供安全的網路通信

SSL證書 主要用於 https 加密

```
# 打印證書內容
openssl x509 -in fullchain.pem -noout -text

# 打印證書有效時間
openssl x509 -in fullchain.pem -noout  -startdate -enddate

# 打印證書序列號
openssl x509 -in fullchain.pem -noout -serial
```