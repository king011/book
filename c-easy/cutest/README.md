# cutest

cutest 是一個開源的輕量 c 單元測試庫

* 源碼 https://github.com/ennorehling/cutest

# 環境

從 官網下載 源碼 將 **CuTest.h CuTest.c** 加入項目即可

# Example

```
#include <stdio.h>
#include "CuTest.h"

// 模式測試函數
void TestCuStringNew(CuTest *tc)
{
    CuString *str = CuStringNew();
    CuAssertTrue(tc, 0 == str->length);
    CuAssertTrue(tc, 0 != str->size);
    CuAssertStrEquals(tc, "", str->buffer);
}

CuSuite *createSuite(void)
{
    CuSuite *suite = CuSuiteNew();

    // 添加測試函數
    CuSuiteAdd(suite, CuTestNew("String.TestNew", TestCuStringNew));
    CuSuiteAdd(suite, CuTestNew("TestCuStringNew", TestCuStringNew));

    return suite;
}

int main(int argc, char **argv)
{
    // 創建測試環境
    CuString *output = CuStringNew();
    CuSuite *suite = CuSuiteNew();

    // 添加一組測試
    CuSuiteAddSuite(suite, createSuite());

    // 運行測試
    CuSuiteRun(suite);

    // 獲取測試結果並打印
    CuSuiteSummary(suite, output);
    CuSuiteDetails(suite, output);
    printf("%s\n", output->buffer);
    return suite->failCount;
}
```