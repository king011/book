# 上傳視頻損毀

piwigo 的上傳邏輯存在 bug，當上傳一個大檔案(需要分塊)時，如果中途出現錯誤在下次上傳時，piwigo 不會截斷上次傳輸的檔案但前端會將數據完整再上傳一次，這樣導致的結果就是存儲了錯誤的數據到服務器但卻告訴用戶上傳成功。

比如檔案太大被分塊爲 chunk1 chunk2 chunk3,第一次上傳時傳輸了 chunk1 + chunk2 此時出現錯誤(例如網路斷開)，上傳會失敗。當你下次再傳輸此檔案時，會重新傳輸 chunk1+chunk2+chunk3，服務器卻沒有重置臨時檔案，所以它會存儲 chunk1+chunk2+chunk1+chunk2+chunk3 並且告訴你上傳成功了，但實際上你的數據已經損毀了，特別是上傳視頻時你可能要等待下次你真的播放時才會發現 what fuck 我的視頻怎麼損毀了。此 bug 本喵已經提交到 piwigo 的 [issues](https://github.com/Piwigo/Piwigo/issues/1773) ，但不知道官方何時會去修復。在被修復前你可以手動執行下述操作暫時解決此問題

找到piwigo安裝目錄下的 **include/ws\_functions/pwg.images.php** 檔案,大概在第 1390 行左右,位於 **ws\_images\_upload** 函數下有這樣一段代碼

```php
#info="pwg.images.php"
// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

// file_put_contents('/tmp/plupload.log', "[".date('c')."] ".__FUNCTION__.', '.$fileName.' '.($chunk+1).'/'.$chunks."\n", FILE_APPEND);

// Open temp file
if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb"))
{
	die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}
```
	
fopen 函數正是在創建上傳的臨時檔案，目前 $chunks 爲 true 即存在分塊時使用 ab 創建檔案否則以 wb 創建，需要修改下 $chunks 爲 true 並且 $chunk 不爲 0 時才以 ab 創建檔案否則都以 wb 創建檔案即可。這樣當出現錯誤後,重新上傳第一個分塊時就可以把之前的臨時檔案截斷爲0
	
```php
#info="pwg.images.php"
// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

// file_put_contents('/tmp/plupload.log', "[".date('c')."] ".__FUNCTION__.', '.$fileName.' '.($chunk+1).'/'.$chunks."\n", FILE_APPEND);

// Open temp file
if (!$out = @fopen("{$filePath}.part", $chunks && $chunk != 0 ? "ab" : "wb"))
{
	die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}
```

# 數據庫

**local/config/database.inc.php** 檔案記錄了數據庫連接信息