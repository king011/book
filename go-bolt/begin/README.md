# 事務

* Update 啟動一個 可寫 事務
* View 啟動一個 只讀 事務
* Batch 啟動 批量寫入 事務

上述函數 都 要求 一個 回調函數 func\(t&nbsp;\*bolt\.Tx\)&nbsp;error 作為參數

回調 返回 任務 error 則 自動 Rollback 並將此錯誤 返回給調用者 否則 自動 調用 Commit 

```
package main
 
import (
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"time"
)
 
func main() {
	// 如果 不存在檔案 則 創建數據庫
	// 否則 打開數據庫
	db, e := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
	if e != nil {
		log.Fatal(e)
	}
	// 關閉 數據庫
	defer db.Close()
 
	// 啟動 一個 可寫 事務
	e = db.Update(func(t *bolt.Tx) error {
		// ...
		return e
	})
	if e != nil {
		log.Fatal(e)
	}
 
	// 啟動 一個 只讀 事務
	e = db.View(func(t *bolt.Tx) error {
		// ...
		return nil
	})
	if e != nil {
		log.Fatal(e)
	}
 
	// 批量 寫入
	db.Batch(func(t *bolt.Tx) error {
		// ...
		return nil
	})
	if e != nil {
		log.Fatal(e)
	}
}
```

# 手動管理事務
bolt 推薦使用 Update View Batch 管理事務 但同時 提供了 手動事務的 api

```
package main
 
import (
	"github.com/boltdb/bolt"
	"log"
	"time"
)
 
func main() {
	// 如果 不存在檔案 則 創建數據庫
	// 否則 打開數據庫
	db, e := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
	if e != nil {
		log.Fatal(e)
	}
	// 關閉 數據庫
	defer db.Close()
 
	// true(可寫) false(只讀)
	var t *bolt.Tx
	t, e = db.Begin(true)
	if e != nil {
		log.Fatal(e)
	}
 
	// ...
 
	if e == nil {
		e = t.Commit()
	} else {
		e = t.Rollback()
	}
	if e != nil {
		log.Fatalln(e)
	}
}
```

# Batch
每次 Update 都會分別進行 磁盤io 這可能比較耗時  
Batch 會將 請求 添加到一個 隊列中 等待 MaxBatchDelay 後執行(如果 隊列 達到 MaxBatchSize 則 立刻處理隊列)  
處理 隊列時 Batch 會將 隊列 所有 事件 同時取出執行 並將沒有錯誤的 操作 全部寫入 磁盤 並將 錯誤 返回給 Batch 調用者 以便 Batch 返回

在 同個 goroutine 中 多個 Batch 可能 比 Update 更慢 且沒有 意義 可以選擇 手動 事務

