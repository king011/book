# [碰撞系統](http://docs.cocos.com/creator/manual/zh/physics/collision/)

creator 提供了簡易的 碰撞 系統 並爲之 提供了 三種 碰撞組件

* [cc.CircleCollider](https://docs.cocos.com/creator/api/zh/classes/CircleCollider.html) 圓形碰撞組件
* [cc.BoxCollider](https://docs.cocos.com/creator/api/zh/classes/BoxCollider.html) 矩形碰撞組件
* [cc.PhysicsCollider](https://docs.cocos.com/creator/api/zh/classes/PhysicsCollider.html) 多邊形碰撞組件

# 啓用碰撞

默認 creator 沒有啓用 碰撞系統 需要 在腳本中 啓動

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        // 返回 碰撞系統
        const manager = cc.director.getCollisionManager()
        // 啓用 碰撞系統
        manager.enabled = true
        // 啓用 測試 繪製
        // 會繪製出 碰撞組件的 檢測範圍 以便調試
        manager.enabledDebugDraw = true
        // 會繪製出 碰撞組件的 外包盒 以便調試
        manager.enabledDrawBoundingBox = true;
    }
}
```

# 腳本控制

如果 碰撞節點 掛接的 腳本 實現了 如下 方法 則在發生 碰撞時 會回調這些函數

* onCollisionEnter
* onCollisionStay
* onCollisionExit

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        this.node.runAction(
            cc.sequence(
                cc.moveBy(1, 100, 0),
                cc.moveBy(1, 0, 100),
                cc.moveBy(1, -100, 0),
                cc.moveBy(1, 0, -100),
            ).easing( // 執行 緩動作
                cc.easeInOut(2.5) //傳入一個 緩動作
            ).repeatForever(), // cc.repeatForever 的語法糖
        )
    }

    /**
     * 當碰撞 發生時 回調
     * @param  {cc.Collider} other 產生碰撞的 另外一個碰撞組件
     * @param  {cc.Collider} self  產生碰撞的 自己身的碰撞組件
     */
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        console.log('on collision enter')

        // 碰撞系統會計算出碰撞組件在世界座標系下的 相關值 並存放到 world中
        const world = (self as any).world

        // 碰撞組件 aabb 碰撞框
        const aabb = world.aabb

        // 節點碰撞前上一幀 aabb 碰撞框位置
        const preAabb = world.preAabb

        // 碰撞框的世界矩陣
        const t = world.transform

        // 圓形碰撞組件 特有屬性
        const r = world.radius
        const p = world.position

        // 矩形 多邊形 碰撞組件 特有屬性
        const ps = world.points;
    }
    /**
     * 碰撞 發生後 每次碰撞 回調
     * @param  {cc.Collider} other 產生碰撞的 另外一個碰撞組件
     * @param  {cc.Collider} self  產生碰撞的 自己身的碰撞組件
     */
    onCollisionStay(other: cc.Collider, self: cc.Collider) {
        console.log('on collision stay')
    }
    /**
     * 碰撞 結束後 回調
     * @param  {Collider} other 产生碰撞的另一个碰撞组件
     * @param  {Collider} self  产生碰撞的自身的碰撞组件
     */
    onCollisionExit(other: cc.Collider, self: cc.Collider) {
        console.log('on collision exit')
    }
}
```

# 點擊測試

點擊測試 的原理在 監聽 節點的 觸摸事件 在觸摸回調中 檢測觸摸點 是否在 碰撞範圍內

```
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    collider: cc.Collider = null
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.collider = this.node.getComponent(cc.Collider)
        this.collider.node.on(cc.Node.EventType.TOUCH_START.toString(), this.callback, this)
    }
    callback(touch: cc.Event.EventTouch) {
        // 返回世界坐标
        let touchLoc = touch.getLocation()
        const world = (this.collider as any).world

        // 點擊測試
        if (cc.Intersection.pointInPolygon(touchLoc, world.points)) {
            console.log("Hit!");
        }
        else {
            console.log("No hit");
        }
    }
}
```

# 碰撞分組

在項目設置中 可以添加 分組 並設置 哪些分組之間可以 互相碰撞

分組 只能增加 無法 刪除

# 編輯碰撞組件

* 破綻組件的 Editing 屬性 被勾選時才可以修改
* 點擊 多邊形組件 兩點間的 線段 可以增加一個 點
* 按住 ctrl 點擊 多邊形組件 的點 可以刪除 此點
* Regenerate Points 按鈕可以自動 依據 Sprite 創建 多邊形組件的點 Threshold 指兩點間的最短距離 越小 點越多
* 按住 shift 可以 保存長寬比例 縮放 矩形多邊形尺寸
* 在所有碰撞組件編輯中 可以在各自的 碰撞中心區域 點擊鼠標左鍵 來 快速編輯 組件的 偏移量
