# IFS

IFS 是 Internal Field Separator 的簡寫，是 bash 的一個特殊變量，用於變量內部分隔(IFS 中每個字符都作爲分隔符)，默認值是 **&lt;space&gt;&lt;tab&gt;&lt;newline&gt;**

```
$ echo "$IFS" | od -b
0000000 040 011 012 012
0000004
```

* 040 表示空格
* 011 表示Tab
* 012 表示換行

通常可以改變 IFS 來執行一些有趣的操作，但最好原值進行備份，並在使用完畢後將IFS恢復

```
ifs=$IFS
IFS="xxx"
...
IFS=$ifs
```

也可以使用 unset 直接將 IFS 重置爲 bash 默認值
```
IFS="xxx"
...
unset IFS
```
# for

for 會將字符串使用 IFS 進行分割後進行遍歷

```
str="a b, c d, e f"
for s in $str;do
    echo $s
done
```

```
a
b,
c
d,
e
f
```

可以修改 IFS 使用 , 分割字符串

```
str="a b, c d, e f"
ifs=$IFS
IFS=,
for s in $str;do
    echo $s
done
IFS=$ifs
```

```
a b
 c d
 e f
```

> 現在空格不在是分隔符號所以輸出了 ' c d' 和 ' e f'

# 數組與字符串分割

將一個數組賦值給另外一個數組正確的寫法是

```
dst=("${src[@]}")
```

此外當也允許將一個變量通過 IFS 分割後賦值 給數組

```
str="1,2
3,4"
strs=($str)

for s in "${strs[@]}";do
    echo "* $s"
done
```

```
* 1,2
* 3,4
```

也可以藉助 IFS 來修改分隔字符

```
str="1,2
3,4"

ifs=$IFS
IFS=,
strs=($str)
IFS=$ifs

for s in "${strs[@]}";do
    echo "* $s"
done
```

```
* 1
* 2
3
* 4
```