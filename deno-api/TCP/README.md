# TCP

```
enum State {
  none,
  run,
  one,
  closed,
}

class Conn {
  constructor(readonly conn: Deno.Conn, readonly addr: string) {
  }
  done = false;
}
class Bridge {
  private state_ = State.none;
  private dst_: Conn;
  private src_: Conn;
  private timer_?: number;
  constructor(
    dst: Deno.Conn,
    src: Deno.Conn,
    private readonly seconds_: number,
  ) {
    this.dst_ = new Conn(dst, JSON.stringify(dst.remoteAddr));
    this.src_ = new Conn(src, JSON.stringify(src.remoteAddr));
  }
  serve() {
    if (State.none === this.state_) {
      this.state_ = State.run;
      console.log(`bridge`, this.dst_.addr, `<=>`, this.dst_.addr);

      this._pipe(this.dst_, this.src_);
      this._pipe(this.src_, this.dst_);
    }
  }
  private _done() {
    this.state_ = State.closed;
    const src = this.src_;
    const dst = this.dst_;
    console.log(`done`, src.addr, `<=>`, dst.addr);
    if (!src.done) {
      src.conn.close();
    }
    if (!dst.done) {
      dst.conn.close();
    }
  }
  private _close() {
    switch (this.state_) {
      case State.run:
        this.state_ = State.one;
        if (this.seconds_ > 0) {
          this._done();
        } else {
          this.timer_ = setTimeout(() => {
            if (State.one === this.state_) {
              this._done();
            }
          }, 1000 * this.seconds_);
        }
        break;
      case State.one:
        clearTimeout(this.timer_);
        this._done();
        break;
    }
  }
  private async _pipe(dst: Conn, src: Conn) {
    try {
      const writer = dst.conn;
      const reader = src.conn.readable.getReader();
      while (true) {
        const { done, value } = await reader.read();
        if (done) {
          src.done = true;
          break;
        }
        await writer.write(value);
      }
    } catch (_) {
      //   console.log(e);
    } finally {
      this._close();
    }
  }
}
async function onAccept(src: Deno.Conn, seconds: number) {
  let dst: Deno.Conn;
  try {
    dst = await Deno.connect({
      hostname: "127.0.0.1",
      port: 80,
    });
  } catch (e) {
    console.log(`connect fail`, e);
    src.close();
    return;
  }
  if (seconds < 1) {
    src.readable.pipeTo(dst.writable);
    dst.readable.pipeTo(src.writable);
  } else {
    new Bridge(dst, src, seconds).serve();
  }
}
const port = 9000;
const l = Deno.listen({
  port: port,
});
console.log(`listen on: ${port}`);
while (true) {
  const src = await l.accept();
  onAccept(src, 1);
}
```