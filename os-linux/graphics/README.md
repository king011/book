# 查看顯卡型號

通常第一步需要執行下述指令確定使用的顯卡型號

```
lspci |egrep -i vga
```

# amd

可以在 [amd](https://www.amd.com/en/support) 官網下載對應型號的驅動壓縮包，解壓後執行 **amd-driver-installer-XXX.run** 即可