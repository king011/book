# [outline](https://www.primefaces.org/primeflex/outline)
outline 定義元素接收焦點時顯示的樣式

| Class | Properties |
| -------- | -------- |
| outline-none     | outline: none;     | 

```
<div class="card">
    <div class="m-3 flex">
        <input class="mx-3 p-2">
        <div class="mx-3">Default browser focus styles applied</div>
    </div>
    <div class="m-3 flex">
        <input class="outline-none mx-3 p-2">
        <div class="mx-3">Default focus styles removed</div>
    </div>
</div>
```