# GLOBAL

libev 提供了一些 全局函數 可以在任何時候調用

```c
// 返回當前時間。注意 ev_now 函數更快 並且會返回你真正想知道的時間戳。
// ev_now_update 和 ev_now 組合也會很有趣
ev_tstamp ev_time ();

// 休眠一段時間 如果小於等於0 立刻返回。最大取值爲一天 即 86400
ev_sleep (ev_tstamp interval);

// 獲取版本號 通常可以和 EV_VERSION_MAJOR EV_VERSION_MINOR 比較
// 主版本不同 最好停止程序 可能不兼容 通常 次版本都是兼容的
// assert (("libev version mismatch",ev_version_major () == EV_VERSION_MAJOR && ev_version_minor () >= EV_VERSION_MINOR));
int ev_version_major ();
int ev_version_minor ();


// 用於驗證 支持的後端
// 比如驗證是否支持 epoll assert (("sorry, no epoll, no sex", ev_supported_backends () & EVBACKEND_EPOLL));
unsigned int ev_supported_backends ();
unsigned int ev_recommended_backends ();
unsigned int ev_embeddable_backends ();

// 重設 realloc 函數
void ev_set_allocator (void *(*cb)(void *ptr, long size) throw ());

// 設置 系統錯誤 callback 默認 perror() 後 abort()
void ev_set_syserr_cb (void (*cb)(const char *msg) throw ());

// 模擬一個信號
void ev_feed_signal (int signum);
```