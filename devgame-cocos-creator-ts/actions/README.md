# 動作系統

creator 的動作系統 源自 cocos2d-x [cc.Node](http://docs.cocos.com/creator/api/zh/classes/Node.html) 提供了 如下 api

```
// 創建一個 動作
const action = cc.moveTo(2, 100, 100)
// 執行動作
node.runAction(action)
// 停止一個動作
node.stopAction(action)
// 停止所有動作
node.stopAllActions()
```

動作支持 設置一個 tag 之後通過 tag來 控制動作

```
// 爲 action 設置 tag
var ACTION_TAG = 1;
action.setTag(ACTION_TAG);
// 提供 tag 查找 action
node.getActionByTag(ACTION_TAG);
// 通過 tag 停止 action
node.stopActionByTag(ACTION_TAG);
```

# 基礎動作

基礎動作就是實現各種 變形 位移 動畫的動作 比如 

* cc.moveTo 移動 Node
* cc.rotateBy 旋轉 node 一定角度
* cc.scaleTo 縮放 Node

基礎動作 又分爲

* 事件動作 需要經過一定時間才能完成 全部繼承自 [cc.ActionInterval](http://docs.cocos.com/creator/api/zh/classes/ActionInterval.html)
* 即時動作 立刻生效 比如 cc.callFunc cc.hide 全部 繼承自 [cc.ActionInstant](http://docs.cocos.com/creator/api/zh/classes/ActionInstant.html)

# 容器動作

容器動作 可以以 不同的 方式 將 動作 組織起來

1. [cc.sequence](http://docs.cocos.com/creator/api/zh/modules/cc.html#sequence) 順序動作 可以讓 一系列子動作 按順序 依次執行

    ```ts
    const { ccclass, property } = cc._decorator
    
    @ccclass
    export default class NewClass extends cc.Component {
        // LIFE-CYCLE CALLBACKS:
        onLoad() {
            this.node.runAction(
                // 支持 變長參數 組合 action
                cc.sequence(
                    cc.sequence(
                        // 第一個 參數 如果爲 數組 則將 數組 內的 action 組合 並忽略後續參數
                        [
                            cc.moveBy(1, 100, 0),
                            cc.moveBy(1, 0, 100),
                        ]
                    ),
                    cc.moveBy(1, -100, 0),
                    cc.moveBy(1, 0, -100),
                )
            )
        }
    }
    ```

1. [cc.spawn](http://docs.cocos.com/creator/api/zh/modules/cc.html#spawn) 同步動作 可以同步執行 一系列 子動作 子動作結果會被疊加起來

    ```ts
    const { ccclass, property } = cc._decorator
    
    @ccclass
    export default class NewClass extends cc.Component {
        // LIFE-CYCLE CALLBACKS:
        onLoad() {
            this.node.runAction(
                // 支持 變長參數 組合 action
                cc.sequence(
                    cc.spawn(
                        // 第一個 參數 如果爲 數組 則將 數組 內的 action 組合 並忽略後續參數
                        [
                            cc.moveBy(1, 100, 0),
                            cc.moveBy(1, 0, 100),
                        ]
                    ),
                    cc.spawn(
                        cc.moveBy(1, -100, 0),
                        cc.moveBy(1, 0, -100),
                    ),
                )
            )
        }
    }
    ```
		
1. [cc.repeat](http://docs.cocos.com/creator/api/zh/modules/cc.html#repeat) 重複動作 讓 指定動作 重複執行 n 次

    ```ts
    const { ccclass, property } = cc._decorator
    
    @ccclass
    export default class NewClass extends cc.Component {
        // LIFE-CYCLE CALLBACKS:
        onLoad() {
            this.node.runAction(
                cc.repeat(
                    cc.sequence(
                        cc.moveBy(1, 100, 0),
                        cc.moveBy(1, 0, 100),
                        cc.moveBy(1, -100, 0),
                        cc.moveBy(1, 0, -100),
                    ),
                    2, // 重複次數
                )
            )
        }
    }
    ```

1. [cc.repeatForever](https://docs.cocos.com/creator/api/zh/modules/cc.html#repeatForever) 無限重複 執行 指定動作

    ```ts
    const { ccclass, property } = cc._decorator
    
    @ccclass
    export default class NewClass extends cc.Component {
        // LIFE-CYCLE CALLBACKS:
        onLoad() {
            this.node.runAction(
                cc.repeatForever(
                    cc.sequence(
                        cc.moveBy(1, 100, 0),
                        cc.moveBy(1, 0, 100),
                        cc.moveBy(1, -100, 0),
                        cc.moveBy(1, 0, -100),
                    ),
                )
            )
        }
    }
    ```
		
1. [cc.speed](https://docs.cocos.com/creator/api/zh/modules/cc.html#speed) 修改動作 執行速率 讓 動作 更快/慢 的執行

    ```ts
    const { ccclass, property } = cc._decorator
    
    @ccclass
    export default class NewClass extends cc.Component {
        // LIFE-CYCLE CALLBACKS:
        onLoad() {
            this.node.runAction(
                cc.speed(
                    cc.sequence(
                        cc.moveBy(1, 100, 0),
                        cc.moveBy(1, 0, 100),
                        cc.moveBy(1, -100, 0),
                        cc.moveBy(1, 0, -100),
                    ).repeatForever(), // cc.repeatForever 的語法糖
                    1 / 2, // 以 1/2 的速率執行
                ),
            )
        }
    }
    ```
		
# callFunc 動作 回調

[cc.callFunc](https://docs.cocos.com/creator/api/zh/modules/cc.html#callFunc) 會在動作執行中 插入一個 回調函數 配合 sequence 很容易在 動作執行中 插入 一段 邏輯代碼

cc.callFunc 接收三個 參數 

* 回調函數
* 綁定的 this 指針
* 回調參數

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        this.node.runAction(
            cc.sequence(
                cc.moveBy(1, 100, 0),
                cc.moveBy(1, 0, 100),
                cc.moveBy(1, -100, 0),
                cc.moveBy(1, 0, -100),
                cc.callFunc(function (data: any) {
                    console.log("yes")
                }, this, null)
            ).repeatForever(), // cc.repeatForever 的語法糖
        )
    }
}
```

# 緩動作

緩動作 不可以單獨存在 它永遠是爲了 修飾基礎動作而存在

緩動作 用來修改 基礎動作的 時間曲線 讓動作有 快入 緩入 快出 ... 等特效

只有事件間隔動作才支持 緩動作

![](assets/tweener.png)

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        this.node.runAction(
            cc.sequence(
                cc.moveBy(1, 100, 0),
                cc.moveBy(1, 0, 100),
                cc.moveBy(1, -100, 0),
                cc.moveBy(1, 0, -100),
            ).easing( // 執行 緩動作
                cc.easeInOut(2.5) //傳入一個 緩動作
            ).repeatForever(), // cc.repeatForever 的語法糖
        )
    }
}
```