# [dart:async](https://api.flutter.dev/flutter/dart-async/dart-async-library.html)
```dart
import 'dart:async';
```

async 包的 補充 Stream 的概念過於重要 故 單獨從 async的介紹中 抽離出來 特別介紹
# Stream
Stream 是異步數據的事件流

可以從 Stream 不斷的獲取到 數據事件 或 錯誤事件 當流發出所有要傳遞的事件後 完成事件將被發出

Stream 即時收到 error 也不會 完成 要等到所有 數據事件 和 錯誤事件 都完成 才會完成

## await
dart 的 await 支持 遍歷 Stream 並且 可以使用 try 捕獲 錯誤

```
import 'dart:async';

read(Stream<int> stream) async {
  try {
    await for (var v in stream) {
      print("$v");
    }
  } catch (e) {
    print("error : $e");
  }
}

main() {
  final controller = StreamController<int>();
  final sink = controller.sink;
  final stream = controller.stream;
  read(stream);

  sink
    ..add(1)
    ..add(2)
    ..addError("e0")
    ..add(3)
    ..addError("e1")
    ..add(4)
    ..close();
}
```

await 在第一遇到 異常時就會 結束 無法接收 後續的 錯誤 和 數據

## listen

listen 可以完整 的獲取 Stream 中的數據

```
import 'dart:async';

main() async {
  final controller = StreamController<int>();
  final sink = controller.sink;
  final stream = controller.stream;
  stream.listen(
    (v) => print(v),
    onError: (e) => print("error : $e"),
    onDone: () => print("done"),
    cancelOnError: false, // 如果爲true 在第一次遇到錯誤後 退訂
  );

  sink
    ..add(1)
    ..add(2)
    ..addError("e0")
    ..add(3)
    ..addError("e1")
    ..add(4)
    ..close();
}
```

listen 會 返回一個 StreamSubscription 其提供了 cancel 用來 取消 listen 的訂閱

## asBroadcastStream

通常 Stream.isBroadcast 返回 false 代表 是一個 單訂閱流 不可以被 多個 listen

asBroadcastStream 將 Strem 轉爲 廣播 從而允許多個 listen 訂閱

## map take first ...

dart 的 Strem 提供了 類似 rx 的一些 常用 operators 可用

```
import 'dart:async';

main() {
  Stream.fromIterable([1, 2, 3]).map((v) => v * 10).listen((v) => print(v));
}
```

# StreamSink

Stream 輸出 事件 StreamSink 是事件入口 輸入事件

# StreamController

StreamController 是 StreamSink 和 Stream 的控制器

StreamController 關聯了一個 StreamSink 和 Stream 將 StreamSink 的輸入 流向Stream進行輸出