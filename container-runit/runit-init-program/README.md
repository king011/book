# runit-init program

runit-init 是內核啓動的第一個進程。如果 runit-init 以 PID 1 啓動，它會運行 runit 並用它替代自己

如果在系統啓動時啓動 runit-init，它必須運行在 init 0 或 init 6 級別

* init 0 -> 告訴 pid 1進程關閉並停止系統。爲了向 runit 發出系統停止請求。runit-init 刪除 **/etc/runit/renoot** 的執行權限(chmod 0)，並設置 **/etc/runit/stopit** 的執行權限(chmod 100)。然後將 CONT signal 發送給 runit
* init 6 -> 告訴 pid 1進程關閉並重啓系統。爲了向 runit 發出系統重啓請求，runit-init 設置 **/etc/runit/reboot** 和 **/etc/runit/stopit** 執行權限(chmod 100)。然後將 CONT signal 發送到 runit

# 返回值

runit-init 在出錯時返回 111，其它情況返回 0