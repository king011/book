# docker
syncthing 提供了 [docker](https://hub.docker.com/r/syncthing/syncthing) 鏡像以方便部署。

```
sudo docker run \
		--name syncthing \
		--network private_intranet \
		--network-alias syncthing \
		--restart always \
		-e TZ=Asia/Shanghai \
		-v /home/king/Sync:/var/syncthing/Sync \
		-p 8384:8384 \
		-p 22000:22000/tcp \
		-p 22000:22000/udp \
		-p 21027:21027/udp \
		-d syncthing/syncthing:latest
```

1. 8384 是網頁 gui 端口，另外幾個端口用於設備同步數據，如果設備只連接其它服務器不被連接可以不用映射
2. /var/syncthing/Sync 是要同步的目錄
3. 默認以 UID=1000 GID=1000執行同步，可以使用環境變量更改`-e UID=1000 -e GID=1000`
