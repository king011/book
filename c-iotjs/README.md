# Iot.js

Iot.js 是三星公司推出的一個用於嵌入式物聯網的開源(Apache Version 2.0) javascript 環境，其使用三星爲嵌入式低內存消耗優化的 jerryscript 執行 js 代碼，使用 libuv 作爲異步模型並且提供了基本的 like nodejs api。

* 官網 [https://iotjs.net/](https://iotjs.net/)
* 源碼 [https://github.com/jerryscript-project/iotjs](https://github.com/jerryscript-project/iotjs)
* 文檔 [https://github.com/jerryscript-project/iotjs/blob/master/docs/Getting-Started.md](https://github.com/jerryscript-project/iotjs/blob/master/docs/Getting-Started.md)
* api [https://github.com/jerryscript-project/iotjs/blob/master/docs/api/IoT.js-API-reference.md](https://github.com/jerryscript-project/iotjs/blob/master/docs/api/IoT.js-API-reference.md)