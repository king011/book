# therecipe/qt

* 是 go 對 qt 的 binding 同 qt一樣使用了 LGPL協議
* 爲 go 提供了一個 可用 靠譜的 gui 開發框架
* 同 c++ qt 一樣 支持多平臺 **Linux** Windows macOS IOS Android SailfishOS and Raspberry Pi

源碼 [https://github.com/therecipe/qt](https://github.com/therecipe/qt)

# Example QML

![assets/hello-world-qml.gif](assets/hello-world-qml.gif)

```go
#info="main.go"
package main
 
import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quickcontrols2"
	"os"
)
 
func main() {
	// Create application
	app := gui.NewQGuiApplication(len(os.Args), os.Args)
 
	// Enable high DPI scaling
	app.SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)
 
	// Use the material style for qml
	quickcontrols2.QQuickStyle_SetStyle("material")
 
	// Create a QML application engine
	engine := qml.NewQQmlApplicationEngine(nil)
 
	// Load the main qml file
	engine.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))
 
	// Execute app
	gui.QGuiApplication_Exec()
}
```

```go
#info="qml/main.qml"
import QtQuick 2.7
import QtQuick.Controls 2.1
 
ApplicationWindow {
  id: window
  visible: true
  title: "Hello World Example"
  minimumWidth: 400
  minimumHeight: 400
 
  Column {
    anchors.centerIn: parent
 
    TextField {
      id: input
      anchors.horizontalCenter: parent.horizontalCenter
      placeholderText: "1. write something"
    }
 
    Button {
      anchors.horizontalCenter: parent.horizontalCenter
      text: "2. click me"
      onClicked: dialog.open()
    }
  }
 
  Dialog {
    contentWidth: window.width / 2
    contentHeight: window.height / 4
    id: dialog
    standardButtons: Dialog.Ok
 
    contentItem: Label {
      text: input.text
    }
  }
}
```
