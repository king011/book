# 記錄日誌

1. 創建一個 記錄器
2. 調用 記錄函數
3. 在 退出程式前 調用 Sync 刷新 緩存

```go
package main

import (
	"go.uber.org/zap"
	"time"
)

func main() {
	// 創建 一個 記錄器
	logger, _ := zap.NewProduction()
	// 刷新 緩存
	defer logger.Sync()

	url := "/version"
	// 寫入 日誌
	sugar := logger.Sugar()
	sugar.Infow("failed to fetch URL",
		// 結構化 上下文 的 key-value 對
		"url", url,
		"attempt", 3,
		"backoff", time.Second,
	)
	// 寫入 日誌
	sugar.Infof("Failed to fetch URL: %s", url)
}
```

# Sugar
在 結構化 上下文 時 zap 提供了 Sugar 語法糖 但在需要 效率的 地方 應該 直接使用 記錄器方法

不使用 此語法糖 會快 4~10 倍 

```go
package main

import (
	"go.uber.org/zap"
	"time"
)

func main() {
	// 創建 一個 記錄器
	logger, _ := zap.NewProduction()
	// 刷新 緩存
	defer logger.Sync()

	url := "/version"
	// 寫入 日誌
	logger.Info("failed to fetch URL",
		zap.String("url", url),
		zap.Uint8("attempt", 8),
		zap.Duration("backoff", time.Second),
	)

	// 寫入 日誌
	logger.Info("Failed to fetch URL: " + url)
}
```

