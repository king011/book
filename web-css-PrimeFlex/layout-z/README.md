# [layout-z](https://www.primefaces.org/primeflex/zindex)

PrimeFlex 定義了幾個 class 用於指定 z-index

| Class | Properties |
| -------- | -------- |
| z-auto     | z-index: auto;     |
| z-0     | z-index: 0;     |
| z-1     | z-index: 1;     |
| z-2     | z-index: 2;     |
| z-3     | z-index: 3;     |
| z-4     | z-index: 4;     |
| z-5     | z-index: 5;     |

![](assets/example.png)

```
<div class="card">
    <div class="card-container blue-container flex flex-wrap justify-content-center" style="min-height: 200px">
        <div class="z-5 relative flex align-items-center justify-content-center p-4 bg-blue-500 font-bold text-white border-round shadow-1" style="width: 100px; height: 100px; left:125px">z-5</div>
        <div class="z-4 relative flex align-items-center justify-content-center p-4 bg-blue-500 font-bold text-white border-round shadow-1" style="width: 100px; height: 100px; left:100px; top:10px">z-4</div>
        <div class="z-3 relative flex align-items-center justify-content-center p-4 bg-blue-500 font-bold text-white border-round shadow-1" style="width: 100px; height: 100px; left:75px; top:20px">z-3</div>
        <div class="z-2 relative flex align-items-center justify-content-center p-4 bg-blue-500 font-bold text-white border-round shadow-1" style="width: 100px; height: 100px; left:50px; top:30px">z-2</div>
        <div class="z-1 relative flex align-items-center justify-content-center p-4 bg-blue-500 font-bold text-white border-round shadow-1" style="width: 100px; height: 100px; left:25px; top:40px">z-1</div>
        <div class="z-0 relative flex align-items-center justify-content-center p-4 bg-blue-500 font-bold text-white border-round shadow-1" style="width: 100px; height: 100px; top:50px">z-0</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同