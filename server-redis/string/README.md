# 字符串

redis 支持字符串型別 並且 可以存儲 二進制數據

| 命令 | 含義 | 
| -------- | -------- |
| SET key value     | 設置 key 的值     |
| GET key     | 返回 key 的值     |
| GETRANGE key start end      | 返回 key 值的 子串 [start,end]  end 可以用 -1 表示 strlen(value)-1    |
| GETSET key value     | 設置 key 並且返回 舊值     |
| GETBIT key offset     | 返回 key 值 的 offset bit 是否爲 1     |
| SETBIT key offset value     | 設置 key 值 的 offset bit 是否爲 1     |
| MGET key1 [key2..]     | 獲取多個 key 的值     |
| SETEX key seconds value     | 設置 key 的 過期時間 和 值     |
| SETNX key value     | 僅當 key 不存在時 設置新 key     |
| SETRANGE key offset value     | 將 key 從 offset 開始的值 覆蓋爲 value     |
| STRLEN key     | 獲取 key 存儲值的 長度     |
| MSET key value [key value ...]     | 設置多個 key value     |
| MSETNX key value [key value ...]      | 設置多個 key value 有任何一個 key 存在都會失敗     |
| PSETEX key milliseconds value     | 設置 key 的過期毫秒數 和 值     |
| INCR key     | 爲 key 值自增 1 (必須可轉爲整型)     |
| DECR key     | 爲 key 值自減 1 (必須可轉爲整型)     |
| INCRBY key increment     | 爲 key 值+increment (必須可轉爲整型)     |
| DECRBY key decrement     | 爲 key 值-increment (必須可轉爲整型)     |
| INCRBYFLOAT key increment     | 爲 key 值+increment (必須可轉爲浮點)     |
| APPEND key value     | 爲 key 的值 連接子串     |

