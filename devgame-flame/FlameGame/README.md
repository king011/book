# FlameGame

FlameGame 是一個實現了 Component 的 遊戲類。基本上 FlameGame 擁有一個 List&lt;Component&gt; 並且 **update** 和 **render** 的調用會被傳遞到所有被添加了的 Component 上

每當遊戲需要調整大小時，例如當方向改變時，FlameGame將調用所有 Components 的 **onGameResize** 函數，並將此信息傳遞給 camera 和 viewport

**FlameGame.camera** 控制坐標空間中的哪個點應該在屏幕的左上角(它默認爲 \[0,0\] 就像一個普通 的 Canvas)

```
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

void main() {
  final myGame = MyGame();
  runApp(GameWidget(
    game: myGame,
  ));
}

class MyGame extends FlameGame {
  @override
  Future<void> onLoad() async {
    await super.onLoad();

    // 添加一個組件到遊戲中
    add(TuxComponent());
  }
}

// 派生一個 sprite 組件
class TuxComponent extends SpriteComponent {
  @override
  Future<void> onLoad() async {
    await super.onLoad();
    // 加載 sprite
    sprite = await Sprite.load('tux.png');
    anchor = Anchor.center;

    // 設置組件大小 和 sprite 一致
    size = sprite!.srcSize.clone();
  }

  @override
  void onGameResize(Vector2 gameSize) {
    super.onGameResize(gameSize);

    // 不需要在構造函數中設置 位置等信息
    // 可以在此處設置 此函數會在被渲染前被調用一次
    // (當然看名字也知道 遊戲窗口大小發生變化時此函數也會被調用)
    position = gameSize / 2;

    // onGameResize 第一次調用時在 onLoad 之前 所以並且 onLoad的返回值是 Future
    // 所以 sprite 可能還沒完成加載 爲 null
    debugPrint('onGameResize ${sprite == null}');
  }
}
```

* 不要在 Widget 的 build 函數中實例化 Game 類，如果那麼做每當 Flutter 樹被重建時重新實例化 Game。可以像例子中一樣先創建 Game 實例然後在 Widget 構建中傳遞 Game 的引用即可
* Component 提供 add AddAll remove removeAll 來添加和刪除組件
* Component 也提供了 removeFromParent 來將組件從父組件的組件列表中刪除

# Lifecycle 

下圖表示組件的生命週期

![](assets/component_lifecycle.png)

當遊戲第一次被添加到 Flutter Widget tree 時，將依次調用生命週期函數 **onGameResize** **onLoad** **onMount**。之後它再每幀在 update 和 render 間來回調度，直到 Widget 從 tree 中移除。一旦從 tree 中移除 GameWidget 就會調用 **onRemove**，就像從組件樹中移除普通組件一樣。

# changePriority

更改組件優先級

所有組件都存在一個 **final ComponentSet children;** 屬性保存了其子組件 ComponentSet 提供了一個 
bool changePriority(Component component,int priority,)` 函數用於修改子組件的優先級別

優先級越高渲染和更新時間越晚，這將使它在屏幕上看起來更進，因爲將先渲染優先級低的組件而它後渲染將覆蓋到先渲染的畫面之上

```
class MyGame extends FlameGame {
  final _tux0 = TuxComponent(0);
  final _tux1 = TuxComponent(100);
  @override
  Future<void> onLoad() async {
    await super.onLoad();

    add(_tux0);
    add(_tux1);

    run();
  }

  // 此函數 使用兩個 sprite 交替重疊
  run() async {
    var ok = true;
    while (true) {
      if (ok) {
        ok = false;
        children.changePriority(_tux0, 0);
        children.changePriority(_tux1, 1);
      } else {
        ok = true;
        children.changePriority(_tux0, 1);
        children.changePriority(_tux1, 0);
      }
      await Future.delayed(const Duration(seconds: 2));
    }
  }
}
```

# 低級 api 架構
下圖顯示了 Flame api 架構是如何相互依賴與支持的

![](assets/game_mixin.png)


* **mixin Game** 是一個低級別 API 當你想實現遊戲引擎如何構建功能時可以使用它。例如，沒有實現任何更新(暫停遊戲)或渲染功能(只跑遊戲邏輯不用顯卡渲染)
* 如果你想創建自己的 Game class，則必須 **with Loadable,Game** 這就是 OxygenGame 所做的
* **mixin Loadable** 包含了生命週期函數 **onLoad onMount onRemove**，當遊戲 加載 掛載 刪除 時，這些函數會從 GameWidget 或 一個父組件中被調用。
	*  onLoad 僅在第一次將類添加到父級時調用，通常在此 爲 sprite 加載圖像資源
	*  onMount 在每次添加到新的父級時會調用(在 onLoad 之後)，可以在此調整 sprite 組件 的初始位置
	*  onMount 當每次從父類中刪除時被調用

使用 **mixin Game** 可以更自由的創作遊戲，但這會導致你 錯過 Flame 中的所有內置功能，下面是一個官方給的例子

```
class MyGameSubClass with Loadable, Game {
  @override
  void render(Canvas canvas) {
    // ...
  }

  @override
  void update(double t) {
    // ...
  }
}

main() {
  final myGame = MyGameSubClass();
  runApp(
    GameWidget(
      game: myGame,
    )
  );
}
```

# GameLoop

GameLoop 是對遊戲循環概念的簡單抽象。基本上大多數遊戲都建立在兩種方法之上

* render 方法使用畫布來繪製遊戲的當前狀態
* update 方法接收自上次更新以來的增量時間(以秒爲單位)，並允許在此將遊戲移動到下一個狀態

# 暫停 和 恢復 遊戲執行

Flame 可以通過兩種方式 暫停 和 恢復 遊戲執行

1. 調用 Game 提供的 **pauseEngine()** 和 **resumeEngine()** 方法
2. 通過 Game 提供的 **get/set paused** 屬性修改狀態狀態

上述兩種方法效果是一致的 paused 通過 setter 在內部調用了 pauseEngine/resumeEngine

# Flutter Widgets and Game instances

Flame 提供了 GameWidget 派生自 StatefulWidget 可以像普通的 Flutter Widget 一樣將 Game 嵌入到 Flutter Widget Tree，此外 Game.overlays 也允許將 Widget 顯示到遊戲頂部，這使得創建暫停菜單 或 inventory screen 之類的東西變得非常容易

需要通過 **overlays.add** **overlays.remove** 傳入一個字符串名字來 添加或刪除 overlay，之後在 GameWidget 的 overlayBuilderMap 中依據 字符串名字創建不同的 overlay

```
// Inside the game methods:
final pauseOverlayIdentifier = 'PauseMenu';

overlays.add(pauseOverlayIdentifier); // Marks 'PauseMenu' to be rendered.
overlays.remove(pauseOverlayIdentifier); // Marks 'PauseMenu' to not be rendered.
```

```
void main() {
  final myGame = MyGame();
  runApp(GameWidget(
    game: myGame,
    overlayBuilderMap: {
      'PauseMenu': (context, MyGame myGame) {
        return PausedButton(game: myGame);
      },
    },
  ));
}

class PausedButton<T extends Game> extends StatefulWidget {
  const PausedButton({
    Key? key,
    required this.game,
  }) : super(key: key);
  final T game;
  @override
  _PausedButtonState createState() => _PausedButtonState();
}

class _PausedButtonState<T extends Game> extends State<PausedButton<T>> {
  T get game => widget.game;
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          if (game.paused) {
            setState(() {
              game.paused = false;
            });
          } else {
            setState(() {
              game.paused = true;
            });
          }
        },
        child: Text('${game.paused}'));
  }
}
```