# 建立檔案
```cmake
#	(可選) 指定檢查 cmake 最低版本
cmake_minimum_required(VERSION 2.6)
 
#	指定 項目名稱
project(ex2)
 
#	增加要編譯的 源碼到 項目
add_executable(ex2 main.cpp)
```
```txt
add_executable(<name> [WIN32] [MACOSX_BUNDLE]
	[EXCLUDE_FROM_ALL]
	source1 source2 ... sourceN)

WIN32 是可選的 在win32平臺下 會將 WinMain 作爲入口點
其它平臺 忽略此參數
```

> cmake 創建的 makefile 檔案 無法脫離 cmake 單獨存在
> 

# 目錄

make \[選項\]&nbsp;&lt;source&nbsp;tree&nbsp;根目錄&gt;  
cmake \[選項\]&nbsp;&lt;binary&nbsp;tree&nbsp;根目錄&gt;

cmake 最後一項指定的路徑 作爲 source tree 根目錄(不設置 使用當前目錄)  
sorce tree 必須包含 CMakeLists.txt  
當命令列指定的路徑下含有 CMakeCache.txt 時(通常是前次建置時遺留下的)，該路徑也會被當成 binary tree 根目錄，否則 CMake 一律會把目前工作資料夾當成 binary tree 根目錄。

## 變量

* CMAKE\_SOURCE\_DIR

  內容為 source tree 根目錄的完整路徑，也就是 CMake 開始建置過程的進入點。

* CMAKE\_BINARY\_DIR

  內容為 binary tree 根目錄的完整路徑，在 in-source build 的時候值與 CMAKE_SOURCE_DIR 相同。

* PROJECT\_SOURCE\_DIR

  目前所屬專案的原始碼根目錄，即內含 project() 指令的 CMakeLists 所在資料夾。前面的例子沒有階層之分，所以 PROJECT\_SOURCE\_DIR 和 CMAKE\_SOURCE\_DIR 相同；更複雜的專案會包含數個子專案，子專案下面還可能會有多個資料夾，PROJECT\_SOURCE\_DIR 內容為目前正在處理中的專案最上層目錄。

* PROJECT\_BINARY\_DIR

  目前所屬專案的建置根目錄，專案意義同上。在 in-source build 時和 PROJECT_SOURCE_DIR 相同。

* CMAKE\_CURRENT\_SOURCE\_DIR

  目前正在處理的 CMakeLists.txt 所在位置。

* CMAKE\_CURRENT\_BINARY\_DIR  

  目前正在處理的 CMakeLists.txt 對應的建置資料夾位置。當然，在 in-source build 時和 CMAKE_CURRENT_SOURCE_DIR 相同。


# 編譯選項
	
## 顯示更多構建信息
	
```txt
#info=false
在 CMakeList.txt 中加入
set(CMAKE_VERBOSE_MAKEFILE ON)

或者
cmake -DCMAKE_VERBOSE_MAKEFILE=ON ...options...
make

或者
cmake ...options...
make VERBOSE=1
```
## 預設的編譯組態
```txt
#info=false
CMAKE_BUILD_TYPE 默認爲 None 可以指定 編譯組態
	None		編譯器預設
	Debug		產生調試信息
	Release		執行速度最優化
	RelWithDebInfo	執行速度最優化但仍然啓用 debugflag
	MinSizeRel	代碼最小化
```

## 加入編譯選項

CMAKE\_C\_FLAGS 會被傳遞給c編譯器 作用在所有組態上  
如果 要只作用在 Release 上 可以使用 CMAKE\_C\_FLAGS\_RELEASE/CMAKE\_C\_FLAGS\_DEBUG/...

c++ 使用 CMAKE\_CXX\_FLAGS CMAKE\_CXX\_FLAGS\_XXX
```cmake
#禁用 vc的 sprintf 警告
if(MSVC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /wd4996")
endif()
```

## 加入連接選項
CMAKE\_EXE\_LINKER\_FLAGS 指定連接選項
```cmake
#	爲所有組態加上 -static Realse則爲 -static -s
if(MINGW)
    set(CMAKE_EXE_LINKER_FLAGS "-static")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-s")
endif()
```
## Preprocessor 預處理宏
add_definitions 可以用來定義宏 作用範圍爲 所在 檔案夾下的編譯單元

```cmake
add_definitions(-DFOO -DBAR)
add_definitions(-DDEBUG -DCERBERUS="cerberus is an idea")
```

## 加入編譯器 搜索路徑
```cmake
#	include 路徑 
include_directories([AFTER|BEFORE] [SYSTEM] dir1 dir2 ...)

#	庫路徑
link_directories(dir1 dir2 ...)
```

## 使用屬性控制編譯選項
可以對某個 target 指定屬性 以替代全局設定
```txt
#info=false
set_target_properties(target1 target2 ...
    PROPERTIES
    屬性名稱1  值
    屬性名稱2  值
    ...
    )
```
```cmake
set_target_properties(app
    PROPERTIES
    LINK_FLAGS          -static
    LINK_FLAGS_RELEASE  -s
    )
```