# typescript

使用 webpack 可以方便的 整合 typescript revel

```
npm install --save-dev typescript ts-loader
```

## webpack.config.js
```
#info="webpack.config.js"
var path = require('path')

module.exports = {
    mode: 'production',
    entry: {
        "App/Index": './public/app/src/App/Index.ts',
        "Animal/cat": './public/app/src/Animal/cat.ts',
    },
    output: {
        path: path.join(__dirname, 'public/app/dst'),
        filename: "[name].js",
        libraryTarget: 'amd',
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: [
                    path.resolve(__dirname, 'node_modules'),
                ],
                use: () => {
                    return [
                        {
                            loader: ['ts-loader'],
                        },
                    ];
                },
            },
        ]
    },
    externals: {
        angular: 'angular',
    },
    resolve: {
        modules: [
            path.resolve(__dirname, '/public/app/src'),
            'node_modules/',
        ],
        descriptionFiles: ['package.json'],
        extensions: ['.ts'],
    },
}
```

## tsconfig.json

```
#info="tsconfig.json"
{
    "compilerOptions": {
        "target": "es5", /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019' or 'ESNEXT'. */
        "module": "amd", /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */
        "outDir": "./public/app/dst", /* Redirect output structure to the directory. */
        "strict": true, /* Enable all strict type-checking options. */
    },
    "include": [
        "public/app/src/**/*.ts"
    ],
    "exclude": [
        "node_modules"
    ]
}
```

## Index.html
```
#info="app/views/App/Index.html"
[[set . "title" "Home"]]
[[template "header.html" .]]

<div id="app">
  yes
  <div ng-controller="ctrl-0">
    <label>Name:</label>
    <input type="text" ng-model="name" placeholder="請輸入名字">
    <hr>
    <h1>Hellow {{name}}</h1>
    <h1>Hellow <span ng-bind="name"></span></h1>

    this is {{beauty}}
  </div>
  <div ng-controller="ctrl-1">
    {{beauty}}
    {{name}}
  </div>
</div>

<script>
  var pageJS = "../app/dst/App/Index";
  require([pageJS], function (m) {
    var jq = $("#app")
    console.log(m.Init)
    m.Init({
      App: jq.get(),
      Ready: function () {
        jq.show()
      },
    })
  })
</script>

[[template "footer.html" .]]
```

## Index.ts
```
info="public/app/src/App/Index.ts"
import { Cat } from '../Animal/cat'
import * as angular from 'angular'

class App {
    constructor(context: any) {
        const app = angular.module('app', []);
        this.controller0(app)
        this.controller1(app)
        angular.bootstrap(context.App, ['app']);
        if (context.Ready) {
            context.Ready()
        }
    }
    private controller0(app: angular.IModule) {
        app.controller('ctrl-0',
            [
                "$scope",
                function ($scope) {
                    const cat = new Cat()
                    $scope.beauty = cat.name();
                },
            ],
        );
    }
    private controller1(app: angular.IModule) {
        app.controller('ctrl-1',
            [
                "$scope",
                function ($scope) {
                    $scope.beauty = "anna lee";
                },
            ],
        );
    }
}
export function Init(context: any) {
    new App(context)
}
```

## cat.ts
```
info="public/app/src/Animal/cat.ts"
export class Cat {
    name(): string {
        return "cat"
    }
}
```