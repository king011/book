# 自定義過濾器

過濾器是一組應用與特定內容(橫幅 彈出窗口)的過濾規則。AdGuard 團隊維護了一份過濾器標準的過濾器列表來滿足大多數用戶的需求。此外 AdGuard 也允許用戶使用自定義的過濾器來滿足客製化的需求。

過濾器的語法採用 增強的 BNF 規範，但並不總是嚴格遵守這個規範

值得一提的是 AdGuard 提供了 vs code 的插件用於書寫 過濾器，建議在 vs code 插件中搜索 AdGuard 並且安裝後用於書寫自定義規則

# 註釋

任何以 **!** 開頭的行都作爲註釋，在規則列表中將以灰色顯示，註釋通常位於規則上方用於描述規則的作用

```
! 這是一個註釋。在此之下的一行是一個實際過濾的規則
||example.org^
```

# 一些例子

下面是一些例子和解釋，用於學習如何書寫 過濾器 規則

## 通過域名阻止

```
||example.org^
```

1. **||** 用於匹配請求地址的 scheme (例如 http:// 或 https://)
2. **example.org** 是逐字文本。此文本必須存在於要阻止的地址中
3. **^** 是分隔符 **/** 或 **:**

下面的請求會被阻止：

* http://example.org/ad1.gif
* http://subdomain.example.org/ad1.gif
* https://ads.example.org:8000/

下面的請求不會被阻止：

* http://ads.example.org.us/ad1.gif
* http://example.com/redirect/http://ads.example.org/

## 阻止確切的地址

```
http://example.org/
```

可以直接書寫確切的地址來目前的阻止指定地址

下面的請求會被阻止：

* http://example.org/

下面的請求不會被阻止：

* https://example.org/banner/img

## 基本規則修飾

過濾規則支持多種修飾符，允許使用者微調規則

```
||example.org^$script,third-party,domain=example.com
```

* **$** 修飾符分隔符號，用於將地址與修飾符分開
* **,** 多個修飾符使用逗號分隔
* **script** 匹配腳本請求
* **third-party** 第三方請求
* **domain=example.com** 請求來自 example.com

下面的請求會被阻止：

* http://example.org/script.js 如果請求來自 example.com

下面的請求不會被阻止：

* https://example.org/script.js 如果請求來自 example.org
* https://example.org/banner.png 因爲不是一個腳本

## 解鎖地址

```
@@||example.org/banner
```

* **@@** 標記例外，會關閉對此請求的過濾

此規則將解除：

* http://example.org/banner.png 即使有規則阻止了此地址也會被放行

> 帶有 **$important** 修飾符的阻止規則可以覆蓋 **@@** 設定

## 解鎖網站上的所有內容

```
@@||example.com^$document
```

* **document** 網站上所有內容

此規則解鎖了 example.com 上的所有封鎖

## 裝飾規則

```
example.org##.banner
```

* **##** 裝飾規則分隔符號
* **.banner** css 選擇器，將指定目標隱藏