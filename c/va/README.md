# 變長參數

在 c 語言支持了變長參數，並且提供了幾個宏來簡化定義

* **va\_list** 這是一個類型，用於存儲對變長參數的讀取狀態
* **va\_start(vl, l)** 這個宏接受兩個參數分別的 va\_list 的實例，和最後一個固定參數名稱，它會使用 &l 的地址計算出第一個變長參數的存儲地址並將它存儲到 va\_list 中
* **va\_arg(vl, type)** 這個宏會從 va\_lis 中得到當前參數的值並將它強制轉換爲 type 類型返回，最後它還會將 va\_list 中變長參數地址移動到下個參數 
* **va\_copy(dest,src)** 這個給宏將一個已經 va\_start 後的訪問狀態由 src 拷貝到 dest
* **va\_end(vl)** 這個宏將 va\_list 的參數指針設置爲 0(無效)

> 通知最好保證 va\_start 和 va\_end 成對出現，因爲一些平臺可能需要爲它們額外申請和釋放資源。  
> 不要手動計算參數地址，而是使用 va\_arg 才能保證不同平臺都能正常運行代碼

va\_arg 的 type 不要亂添，需要和傳入參數一致，因爲在一些平臺中，下個參數的位置需要由 type 的大小確定


```
#include <stdio.h>
#include <stdarg.h>
int sum(int count, ...)
{
    int sum = 0;

    va_list ap;
    va_start(ap, count);
    for (int i = 0; i < count; i++)
    {
        sum += va_arg(ap, int);
    }
    va_end(ap);

    return sum;
}
int main(int argc, char **argv)
{
    printf("%d\n", sum(3,
                       1, 2, 3));
    return 0;
}
```


# 可變參數長度

c 沒有直接提供可獲可變參數長度的方法，但有一些技巧可以作爲參考:

1. 最簡單的方法是，將最後一固定參數作爲作爲可變參數的長度傳入。(大部分需求可以使用此方案)
2. 類型 printf 這類函數，可以計算 fmt 字符串中要求替換的 % 數量。(組合 sql 語句很適合這種方式)
3. 變長參數的最後一個值選取一個特殊值作爲參數結尾

