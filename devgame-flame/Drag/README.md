# [拖拽事件](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html)

當使用者在裝置螢幕上移動手指或按住滑鼠按鈕時移動滑鼠時，會發生拖曳事件

如果使用者使用多個手指，則可以同時發生多個拖曳事件。 Flame 將正確處理此類情況，您甚至可以使用事件的 pointId 屬性來追蹤事件

對於那些您想要回應拖曳的元件，請新增 DragCallbacks mixin:
* 此 mixin 為您的元件新增了四個可重寫的方法：onDragStart、onDragUpdate、onDragEnd 和 onDragCancel。 預設情況下，這些方法不執行任何操作 - 需要重寫它們才能執行任何功能
* 另外，元件必須實作 containsLocalPoint() 方法（已經在 PositionComponent 中實現，所以大多數時候你不需要在這裡做任何事情）——這個方法允許 Flame 知道事件是否發生在元件內

```
class MyComponent extends PositionComponent with DragCallbacks {
  MyComponent() : super(size: Vector2(180, 120));

   @override
   void onDragStart(DragStartEvent event) {
     // Do something in response to a drag event
   }
}
```

## [onDragStart](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html#ondragstart)

這是拖曳序列中發生的第一個事件。 通常，事件將被傳遞到與 DragCallbacks mixin with 的最頂層元件。 但是，透過將標誌 event.continuePropagation 設為 true，您可以允許事件傳播到下方的元件

與此事件關聯的 DragStartEvent 物件將包含事件發起點的座標。 該點在多個座標系中可用： 在整個裝置的座標系中給出 devicePosition，在遊戲元件的座標系中給出 canvasPosition，localPosition 提供元件在本地座標系中的位置

任何接收 onDragStart 的元件稍後也會接收 onDragUpdate 和 onDragEnd 事件

## [onDragUpdate](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html#ondragupdate)

當使用者在螢幕上拖曳手指時，會連續觸發此事件。 如果使用者保持手指不動，它不會觸發

預設實作將此事件傳遞給所有接收到具有相同指標 id 的先前 onDragStart 的元件。 如果觸控點仍在元件內，則 event.localPosition 將給出該點在本機座標系中的位置。 但是，如果使用者將手指從元件上移開，則屬性 event.localPosition 將傳迴座標為 NaN 的點。 同樣，本例中的 event.renderingTrace 將為空。 但是，事件的 canvasPosition 和 devicePosition 屬性將會有效

此外，DragUpdateEvent 將包含 delta——自上次 onDragUpdate 以來手指移動的量，或自 onDragStart （如果這是拖曳開始後的第一次拖曳更新）以來手指移動的量

event.timestamp 屬性測量自拖曳開始以來經過的時間。 例如，它可用於計算運動的速度

## [onDragEnd](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html#ondragend)

當使用者抬起手指並停止拖曳手勢時，會觸發此事件。 沒有與此事件相關的位置

## [onDragCancel](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html#ondragcancel)

該事件發生時的精確語義尚不清楚，因此我們提供了一個預設實現，只是將此事件轉換為 onDragEnd

# [DragCallbacks](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html#dragcallbacks)

DragCallbacks mixin 可以加入任何元件中，以便元件開始接收拖曳事件

此 mixin 在元件中新增了 onDragStart、onDragUpdate、onDragEnd 和 onDragCancel 方法，預設情況下這些方法不會執行任何操作，但可以重寫以實現任何實際功能

另一個關鍵細節是，元件只會接收源自該元件內部的拖曳事件，如 containsLocalPoint() 函數所判斷的。 常用的 PositionComponent 類別根據其 size 屬性提供了這樣的實作。 因此，如果您的元件派生自 PositionComponent，請確保正確設定其大小。 但是，如果您的元件派生自裸元件，則必須手動實作 containsLocalPoint() 方法

如果您的元件是較大層次結構的一部分，那麼只有在其祖先都正確實作 containsLocalPoint 時，它才會接收拖曳事件

```
class MyComponent extends PositionComponent with DragCallbacks {
  MyComponent({super.size});

  final _paint = Paint();
  bool _isDragged = false;

  @override
  void onDragStart(DragStartEvent event) => _isDragged = true;

  @override
  void onDragUpdate(DragUpdateEvent event) => position += event.delta;

  @override
  void onDragEnd(DragEndEvent event) => _isDragged = false;

  @override
  void render(Canvas canvas) {
    _paint.color = _isDragged? Colors.red : Colors.white;
    canvas.drawRect(size.toRect(), _paint);
  }
}
```
