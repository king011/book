# 運算符

<table class="table table-striped">
  <thead>
    <tr>
      <th>Description</th>
      <th>Operator</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>unary postfix</td>
      <td>
<span><em>expr</em>++</code> &nbsp;&nbsp; <span><em>expr</em>--</code> &nbsp;&nbsp; <span class="highlighter-rouge">()</code> &nbsp;&nbsp; <span class="highlighter-rouge">[]</code> &nbsp;&nbsp; <span class="highlighter-rouge">.</code> &nbsp;&nbsp; <span class="highlighter-rouge">?.</code>
</td>
    </tr>
    <tr>
      <td>unary prefix</td>
      <td>
<span>-<em>expr</em></code> &nbsp;&nbsp; <span>!<em>expr</em></code> &nbsp;&nbsp; <span>~<em>expr</em></code> &nbsp;&nbsp; <span>++<em>expr</em></code> &nbsp;&nbsp; <span>--<em>expr</em></code> &nbsp;&nbsp;</td>
    </tr>
    <tr>
      <td>multiplicative</td>
      <td>
<span class="highlighter-rouge">*</code> &nbsp;&nbsp; <span class="highlighter-rouge">/</code> &nbsp;&nbsp; <span class="highlighter-rouge">%</code>   &nbsp;<span class="highlighter-rouge">~/</code>
</td>
    </tr>
    <tr>
      <td>additive</td>
      <td>
<span class="highlighter-rouge">+</code> &nbsp;&nbsp; <span class="highlighter-rouge">-</code>
</td>
    </tr>
    <tr>
      <td>shift</td>
      <td>
<span class="highlighter-rouge">&lt;&lt;</code> &nbsp;&nbsp; <span class="highlighter-rouge">&gt;&gt;</code>
</td>
    </tr>
    <tr>
      <td>bitwise AND</td>
      <td><span class="highlighter-rouge">&amp;</code></td>
    </tr>
    <tr>
      <td>bitwise XOR</td>
      <td><span class="highlighter-rouge">^</code></td>
    </tr>
    <tr>
      <td>bitwise OR</td>
      <td><span class="highlighter-rouge">|</code></td>
    </tr>
    <tr>
      <td>relational&nbsp;and&nbsp;type&nbsp;test</td>
      <td>
<span class="highlighter-rouge">&gt;=</code> &nbsp;&nbsp; <span class="highlighter-rouge">&gt;</code> &nbsp;&nbsp; <span class="highlighter-rouge">&lt;=</code> &nbsp;&nbsp; <span class="highlighter-rouge">&lt;</code> &nbsp;&nbsp; <span class="highlighter-rouge">as</code> &nbsp;&nbsp; <span class="highlighter-rouge">is</code> &nbsp;&nbsp; <span class="highlighter-rouge">is!</code>
</td>
    </tr>
    <tr>
      <td>equality</td>
      <td>
<span class="highlighter-rouge">==</code> &nbsp;&nbsp; <span class="highlighter-rouge">!=</code> &nbsp;&nbsp;</td>
    </tr>
    <tr>
      <td>logical AND</td>
      <td><span class="highlighter-rouge">&amp;&amp;</code></td>
    </tr>
    <tr>
      <td>logical OR</td>
      <td><span class="highlighter-rouge">||</code></td>
    </tr>
    <tr>
      <td>if null</td>
      <td><span class="highlighter-rouge">??</code></td>
    </tr>
    <tr>
      <td>conditional</td>
      <td><span><em>expr1</em> ? <em>expr2</em> : <em>expr3</em></code></td>
    </tr>
    <tr>
      <td>cascade</td>
      <td><span class="highlighter-rouge">..</code></td>
    </tr>
    <tr>
      <td>assignment</td>
      <td>
<span class="highlighter-rouge">=</code> &nbsp;&nbsp; <span class="highlighter-rouge">*=</code> &nbsp;&nbsp; <span class="highlighter-rouge">/=</code> &nbsp;&nbsp; <span class="highlighter-rouge">~/=</code> &nbsp;&nbsp; <span class="highlighter-rouge">%=</code> &nbsp;&nbsp; <span class="highlighter-rouge">+=</code> &nbsp;&nbsp; <span class="highlighter-rouge">-=</code> &nbsp;&nbsp; <span class="highlighter-rouge">&lt;&lt;=</code> &nbsp;&nbsp; <span class="highlighter-rouge">&gt;&gt;=</code> &nbsp;&nbsp; <span class="highlighter-rouge">&amp;=</code> &nbsp;&nbsp; <span class="highlighter-rouge">^=</code> &nbsp;&nbsp; <span class="highlighter-rouge">|=</code> &nbsp;&nbsp; <span class="highlighter-rouge">??=</code>
</td>
    </tr>
  </tbody>
</table>

# 算數運算符
<table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">+</code></td>
      <td>Add</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">–</code></td>
      <td>Subtract</td>
    </tr>
    <tr>
      <td><span>-<em>expr</em></code></td>
      <td>Unary minus, also known as negation (reverse the sign of the expression)</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">*</code></td>
      <td>Multiply</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">/</code></td>
      <td>Divide</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">~/</code></td>
      <td>Divide, returning an integer result</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">%</code></td>
      <td>Get the remainder of an integer division (modulo)</td>
    </tr>
  </tbody>
</table>

```dart
assert(2 + 3 == 5);
assert(2 - 3 == -1);
assert(2 * 3 == 6);
assert(5 / 2 == 2.5); // Result is a double
assert(5 ~/ 2 == 2); // Result is an int
assert(5 % 2 == 1); // Remainder

assert('5/2 = ${5 ~/ 2} r ${5 % 2}' == '5/2 = 2 r 1');
```

<table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span>++<em>var</em></code></td>
      <td>
<span><em>var</em> = <em>var</em> + 1</code> (expression value is <span><em>var</em> + 1</code>)</td>
    </tr>
    <tr>
      <td><span><em>var</em>++</code></td>
      <td>
<span><em>var</em> = <em>var</em> + 1</code> (expression value is <span><em>var</em></code>)</td>
    </tr>
    <tr>
      <td><span>--<em>var</em></code></td>
      <td>
<span><em>var</em> = <em>var</em> – 1</code> (expression value is <span><em>var</em> – 1</code>)</td>
    </tr>
    <tr>
      <td><span><em>var</em>--</code></td>
      <td>
<span><em>var</em> = <em>var</em> – 1</code> (expression value is <span><em>var</em></code>)</td>
    </tr>
  </tbody>
</table>

```dart
var a, b;

a = 0;
b = ++a; // Increment a before b gets its value.
assert(a == b); // 1 == 1

a = 0;
b = a++; // Increment a AFTER b gets its value.
assert(a != b); // 1 != 0

a = 0;
b = --a; // Decrement a before b gets its value.
assert(a == b); // -1 == -1

a = 0;
b = a--; // Decrement a AFTER b gets its value.
assert(a != b); // -1 != 0
```

# 比較 運算
<table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">==</code></td>
      <td>Equal; see discussion below</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">!=</code></td>
      <td>Not equal</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&gt;</code></td>
      <td>Greater than</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&lt;</code></td>
      <td>Less than</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&gt;=</code></td>
      <td>Greater than or equal to</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&lt;=</code></td>
      <td>Less than or equal to</td>
    </tr>
  </tbody>
</table>

```dart
assert(2 == 2);
assert(2 != 3);
assert(3 > 2);
assert(2 < 3);
assert(3 >= 3);
assert(2 <= 3);
```
# 測試
<table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">as</code></td>
      <td>Typecast (also used to specify <a href="#specifying-a-library-prefix">library prefixes</a>)</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">is</code></td>
      <td>True if the object has the specified type</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">is!</code></td>
      <td>False if the object has the specified type</td>
    </tr>
  </tbody>
</table>

```dart
#info=false
if (emp is Person) {
  // Type check
  emp.firstName = 'Bob';
}
```
```dart
#info=false
(emp as Person).firstName = 'Bob';
```

# 分配運算符
<table class="table">
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">=</code></td>
      <td><span class="highlighter-rouge">–=</code></td>
      <td><span class="highlighter-rouge">/=</code></td>
      <td><span class="highlighter-rouge">%=</code></td>
      <td><span class="highlighter-rouge">&gt;&gt;=</code></td>
      <td><span class="highlighter-rouge">^=</code></td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">+=</code></td>
      <td><span class="highlighter-rouge">*=</code></td>
      <td><span class="highlighter-rouge">~/=</code></td>
      <td><span class="highlighter-rouge">&lt;&lt;=</code></td>
      <td><span class="highlighter-rouge">&amp;=</code></td>
      <td><span class="highlighter-rouge">|=</code></td>
    </tr>
  </tbody>
</table>

```dart
#info=false
// Assign value to a
a = value;
// Assign value to b if b is null; otherwise, b stays the same
b ??= value;
```
<table class="table">
  <thead>
    <tr>
      <th>&nbsp;</th>
      <th>Compound assignment</th>
      <th>Equivalent expression</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>For an operator <em>op</em>:</strong></td>
      <td><span>a <em>op</em>= b</code></td>
      <td><span>a = a <em>op</em> b</code></td>
    </tr>
    <tr>
      <td><strong>Example:</strong></td>
      <td><span class="highlighter-rouge">a += b</code></td>
      <td><span class="highlighter-rouge">a = a + b</code></td>
    </tr>
  </tbody>
</table>

```dart
#info=false
var a = 2; // Assign using =
a *= 3; // Assign and multiply: a = a * 3
assert(a == 6);
```

# 邏輯運算符
<table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span>!<em>expr</em></code></td>
      <td>inverts the following expression (changes false to true, and vice versa)</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">||</code></td>
      <td>logical OR</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&amp;&amp;</code></td>
      <td>logical AND</td>
    </tr>
  </tbody>
</table>

# bit 運算
<table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">&amp;</code></td>
      <td>AND</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">|</code></td>
      <td>OR</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">^</code></td>
      <td>XOR</td>
    </tr>
    <tr>
      <td><span>~<em>expr</em></code></td>
      <td>Unary bitwise complement (0s become 1s; 1s become 0s)</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&lt;&lt;</code></td>
      <td>Shift left</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&gt;&gt;</code></td>
      <td>Shift right</td>
    </tr>
  </tbody>
</table>

# 條件運算
**condition ? expr1 : expr2**

如果 condition 爲 true 返回 expr1 否則 返回 expr2

**expr1 ?? expr2**

如果 expr1 非 null 返回 expr1 否則 返回 expr2

# 級聯 ..

..  允許對同意對象 進行一系列 操作 從而 節省掉 臨時對象的 創建

```dart
querySelector('#confirm') // Get an object.
  ..text = 'Confirm' // Use its members.
  ..classes.add('important')
  ..onClick.listen((e) => window.alert('Confirmed!'));
	
// 和上面 相同功能
var button = querySelector('#confirm');
button.text = 'Confirm';
button.classes.add('important');
button.onClick.listen((e) => window.alert('Confirmed!'));
```

 # 其它運算符
 <table class="table table-striped">
  <thead>
    <tr>
      <th>Operator</th>
      <th>Name</th>
      <th>Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">()</code></td>
      <td>Function application</td>
      <td>Represents a function call</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">[]</code></td>
      <td>List access</td>
      <td>Refers to the value at the specified index in the list</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">.</code></td>
      <td>Member access</td>
      <td>Refers to a property of an expression; example: <span class="highlighter-rouge">foo.bar</code> selects property <span class="highlighter-rouge">bar</code> from expression <span class="highlighter-rouge">foo</code>
</td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">?.</code></td>
      <td>Conditional member access</td>
      <td>Like <span class="highlighter-rouge">.</code>, but the leftmost operand can be null; example: <span class="highlighter-rouge">foo?.bar</code> selects property <span class="highlighter-rouge">bar</code> from expression <span class="highlighter-rouge">foo</code> unless <span class="highlighter-rouge">foo</code> is null (in which case the value of <span class="highlighter-rouge">foo?.bar</code> is null)</td>
    </tr>
  </tbody>
</table>
 
 
