# 使用
 從官網[下載](http://www.jqplot.com/download/)最新源碼
 
在項目中引入 jqery jqplot  
```html
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="excanvas.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.jqplot.min.css" />
```

創建 繪製目標
```html
<div id="chartdiv" style="height:400px;width:300px; "></div>
```

調用 jqplot 傳入 繪製數據
```js
$.jqplot('chartdiv',  [[[1, 2],[3,5.12],[5,13.1],[7,33.6],[9,85.9],[11,219.9]]]);
```

## Example

```html
<!DOCTYPE html>
<html id="app">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>測試</title>

	<!-- 引入 js css -->

	<!-- 不需要兼容 sb的 ie9 以及以下版本 所以不 引入 excanvas -->
	<!-- <script src="jqplot/excanvas.js"></script> -->
	<script src="jquery-2.2.4.min.js"></script>
	<script src="jqplot/jquery.jqplot.min.js"></script>
	<link href="jqplot/jquery.jqplot.min.css">
</head>

<body>
	<!-- 繪製目標 -->
	<div id="chartdiv" style="height:400px;width:300px; "></div>
</body>
<script>
	$(document).ready(function () {
		// 繪製
		$.jqplot(
			'chartdiv', // 目標 id
			// 繪製 數據
			[[
				[1, 2],
				[3, 5.12],
				[5, 13.1],
				[7, 33.6],
				[9, 85.9],
				[11, 219.9]
			]],
			// 可選的 屬性參數
			{
				// 標題
				title: 'Line',
			},
		);
	});
</script>

</html>
```
> 繪製 數據 每個節點是 一個 \[x,y\] 如果 只有 y 則 自動生成 x\(從1開始自增加\)  
> \[\[2,5\.12,13\.1,33\.6\]\]
> 
> plugins 檔案夾下 存放了 jqplot 的插件 包括 柱圖 ... 等額外功能 如果使用 需要 引入對應的 js 檔案
 
# 銷毀 與 重繪
$.jqplot 函數 會返回 jqplot 實例 其 提供了 
* **destroy** 方法 銷毀 實例
* **replot** 方法 重繪 圖表

```html
<!DOCTYPE html>
<!DOCTYPE html>
<html id="app">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>測試</title>

	<!-- 引入 js css -->

	<!-- 不需要兼容 sb的 ie9 以及以下版本 所以不 引入 excanvas -->
	<!-- <script src="jqplot/excanvas.js"></script> -->
	<script src="jquery-2.2.4.min.js"></script>
	<script src="jqplot/jquery.jqplot.min.js"></script>
	<script src="jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
	<link href="jqplot/jquery.jqplot.min.css">
</head>

<body>
	<!-- 繪製目標 -->
	<div id="chartdiv" style="height:400px;width:300px; "></div>
	<button id="destroy">destroy</button><button id="random">random</button>
</body>
<script>
	$(document).ready(function () {
		const arrs = [3, 6, 12, 76, 43];
		const ticks = ['a', 'b', 'c', 'd', 'e'];
		// 繪製
		let jqplot = $.jqplot(
			'chartdiv', // 目標 id
			// 繪製 數據
			[arrs],
			// 可選的 屬性參數
			{
				// 標題
				title: 'Line',
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks // 定義 x 座標 顯示文字
					}
				},
			},
		);
		$("#destroy").bind("click", function () {
			if (!jqplot) {
				console.log("already destroyed,ignore");
				return;
			}
			jqplot.destroy();
			jqplot = null;
			console.log("destroy");
		});
		$("#random").bind("click", function () {
			if (!jqplot) {
				console.log("already destroyed,ignore");
				return;
			}
			// 更新 x 值
			for (let i = 0; i < jqplot.axes.xaxis.ticks.length; i++) {
				jqplot.axes.xaxis.ticks[i] = ticks[i] + Math.floor(Math.random() * 10);
			}
			// 更新 y 值
			for (var i = 0; i < jqplot.series.length; i++) {
				for (var j = 0; j < jqplot.series[i].data.length; j++) {
					jqplot.series[i].data[j][1] = Math.floor(Math.random() * 100);
				}
			}
			// 重繪 圖表
			jqplot.replot({
				resetAxes: true
			});
		});

	});
</script>

</html>
```