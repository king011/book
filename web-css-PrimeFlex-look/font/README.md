# [font-size](https://www.primefaces.org/primeflex/fontsize)

font-size 用於指定元素的字體大小

| Class | Properties |
| -------- | -------- |
| text-xs     | font-size: 0.75rem;     |
| text-sm     | font-size: 0.875rem;     |
| text-base     | font-size: 1rem;     |
| text-lg     | font-size: 1.125rem;     |
| text-xl     | font-size: 1.25rem;     |
| text-2xl     | font-size: 1.5rem;     |
| text-3xl     | font-size: 1.75rem;     |
| text-4xl     | font-size: 2rem;     |
| text-5xl     | font-size: 2.5rem;     |
| text-6xl     | font-size: 3rem;     |
| text-7xl     | font-size: 4rem;     |
| text-8xl     | font-size: 6rem;     |

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center">
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-xs</p>
            <p class="text-xs w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-sm</p>
            <p class="text-sm w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-base</p>
            <p class="text-base w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-lg</p>
            <p class="text-lg w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-xl</p>
            <p class="text-xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-2xl</p>
            <p class="text-2xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-3xl</p>
            <p class="text-3xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-4xl</p>
            <p class="text-4xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-5xl</p>
            <p class="text-5xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-6xl</p>
            <p class="text-6xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-7xl</p>
            <p class="text-7xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
        <div class="flex align-items-center border-bottom-1 surface-border surface-overlay w-full">
            <p class="w-2 text-left font-bold text-blue-500 mr-3">text-8xl</p>
            <p class="text-8xl w-10">Lorem ipsum dolor sit amet</p>
        </div>
    </div>
</div>
```

# [font-weight](https://www.primefaces.org/primeflex/fontweight)
font-weight 指定字體粗細

| Class | Properties |
| -------- | -------- |
| font-light     | font-weight: 300;     |
| font-normal     | font-weight: 400;     |
| font-medium     | font-weight: 500;     |
| font-semibold     | font-weight: 600;     |
| font-bold     | font-weight: 700;     |

![](assets/1.png)

```
<div class="card flex flex-wrap align-items-center justify-content-center">
    <div class="flex border-bottom-1 surface-border w-full">
        <p class="font-light mr-3 text-blue-500 text-left w-2">font-light</p>
        <p class="font-light w-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
    <div class="flex border-bottom-1 surface-border w-full">
        <p class="font-normal mr-3 text-blue-500 w-2">font-normal</p>
        <p class="font-normal w-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
    <div class="flex border-bottom-1 surface-border w-full">
        <p class="font-medium mr-3 text-blue-500 w-2">font-medium</p>
        <p class="font-medium w-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
    <div class="flex border-bottom-1 surface-border w-full">
        <p class="font-semibold mr-3 text-blue-500 w-2">font-semibold</p>
        <p class="font-semibold w-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
    <div class="flex border-bottom-1 surface-border w-full">
        <p class="font-bold mr-3 text-blue-500 w-2">font-bold</p>
        <p class="font-bold w-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
</div>
```

# [font-style](https://www.primefaces.org/primeflex/fontstyle)

font-style 指定字體風格

| Class | Properties |
| -------- | -------- |
| font-italic     | font-style: italic;     |

![](assets/2.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container">
        <p class="font-italic border-round border-1 surface-overlay p-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    </div>
</div>
```

# 響應式佈局

font-size 和 font-weight 的 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同

font-style 的 class 不支持響應式佈局