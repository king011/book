# QUnit

QUnit 是一個開源的 單元測試 框架

* 官網 [http://qunitjs.com/](http://qunitjs.com/)
* 源碼 [https://github.com/qunitjs/qunit](https://github.com/qunitjs/qunit)

# 環境配置
1. 安裝 node qunit 工具

	```sh
	#info=false
	npm install -g qunit
	```

1. 安裝 庫 和 .d.ts

	```sh
	#info=false
	npm install --save-dev qunit

	npm install @types/qunit
	```
	
1. 編寫 測試 代碼
	```ts
	#!/usr/bin/env node


	QUnit.test("test example", (assert) => {
			assert.ok("1" == "1", "Passed!")
	})
	```
1. 執行 單元測試

	```sh
	$ tsc && qunit dist/*.js
	TAP version 13
	ok 1 test example
	1..1
	# pass 1
	# skip 0
	# todo 0
	# fail 0
	```

