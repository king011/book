# [ConfirmPopup](https://primeng.org/confirmpopup)

```
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {ConfirmationService} from 'primeng/api';
```

```
import {ConfirmationService} from 'primeng/api';

@Component({
    templateUrl: './confirmpopupdemo.html',
    providers: [ConfirmationService]
})
export class ConfirmPopupDemo {
}
```

ConfirmPopup 使用方式和 ConfirmDialog 類似，只是它顯示一個確認用到彈出窗口而非模式對話框

使用 p-confirmPopup 來定義彈出窗口

```
<p-confirmPopup></p-confirmPopup>

<button (click)="confirm($event)" pButton icon="pi pi-check" label="Confirm"></button>
```

之後在 ts 中使用 confirmationService 服務來顯示彈出框

```
export class ConfirmPopupDemo {

    constructor(private confirmationService: ConfirmationService) {}

    confirm(event: Event) {
        this.confirmationService.confirm({
            target: event.target,
            message: 'Are you sure that you want to proceed?',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                //confirm action
            },
            reject: () => {
                //reject action
            }
        });
    }
}
```

## Animation Configuration
使用 **showTransitionOptions** 和 **hideTransitionOptions** 來指定彈出窗口的過渡動畫，下面的例子將完全禁用過渡動畫
 
```
<p-confirmPopup [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'"></p-confirmPopup>
```

# Properties

| Column 1 | Column 2 | Column 3 | Column 3 |
| -------- | -------- | -------- | -------- |
| key     | string     | null     | 爲彈出框指定一個匹配用的 key     |
| showTransitionOptions     | string     | 0.12s cubic-bezier(0, 0, 0.2, 1)     | 顯示過渡動畫     |
| hideTransitionOptions     | string     | 0.1s linear     | 隱藏過渡動畫     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| baseZIndex     | number     | 0     | 用於分層的基礎 zindex 值     |
| style     | string     | null     | 爲組件嵌入內聯的 css style     |
| styleClass     | string     | null     | 爲組件嵌入的 css class     |

# Styling

| Name | Element |
| -------- | -------- |
| p-confirm-popup     | 	Container element     | 
| p-confirm-popup-content     | 	Content element.     | 
| p-confirm-popup-icon     | Message icon.     | 
| p-confirm-popup-message     | Message text.     | 
| p-confirm-popup-footer     | Footer element for buttons.     | 

