# StreamBuilder

StreamBuilder 通常和 InheritedWidget 配合使用 

InheritedWidget 如果通過 updateShouldNotify 通知 子樹 變化 所有的子樹都會 重建 這會相對 耗資源

可以 在 InheritedWidget 中 包含一個 Stream

關注 InheritedWidget 狀態的 子節點 通過 訂閱 Stream 來 獲取 數據 變化 通知 StreamBuilder 正式實現了此功能的 子 widget 

![](assets/StreamBuilder.gif)

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // 共享 數據 爲 廣播
  final _controller = StreamController<int>.broadcast();

  // 移除時 釋放 _controller
  @override
  void dispose() {
    super.dispose();
    _controller.close();
  }

  @override
  Widget build(BuildContext context) {
    print("build");
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: ShareDataWidget(
        controller: _controller,
        val: 0,
        child: ListView(
          children: <Widget>[
            CardView(name: "card 1"),
            CardView(name: "card 2"),
          ],
        ),
      ),
    );
  }
}

// stream 廣播的數據
class ShareData {
  int val = 0;
  ShareData(this.val);
}

// 從 InheritedWidget 派生
class ShareDataWidget extends InheritedWidget {
  ShareDataWidget({
    @required StreamController<int> controller,
    @required int val,
    Widget child,
  })  : this._controller = controller,
        this._data = ShareData(val),
        super(child: child);

  // 在子樹中 要共享的 數據
  final StreamController<int> _controller;
  final ShareData _data;

  // 定義一個便捷方法 讓子類獲取到 ShareDataWidget 實例
  static ShareDataWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(ShareDataWidget);
  }

  // 該回調返回 當 data 變化時 是否通知子樹 依賴變化
  @override
  bool updateShouldNotify(ShareDataWidget old) {
    // 如果 返回 ture 子widget 的 state.didChangeDependencies 會被調用
    //
    // 之後會 重建子 widget
    return old._controller != _controller;
  }

  // 返回 stream
  Stream<int> get stream => _controller.stream;

  // +1
  void increment() => _controller.add(++_data.val);
  // -1
  void reduction() => _controller.add(--_data.val);
}

class CardView extends StatelessWidget {
  CardView({this.name, Key key}) : super(key: key);
  final String name;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      // 數據 來源 stream
      stream: ShareDataWidget.of(context).stream,
      // 初始值
      initialData: 0,
      // 異步 build
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        print("$name builder");
        return Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                RaisedButton(
                  textColor: Colors.black,
                  child: new Text('-'),
                  onPressed: () => ShareDataWidget.of(context).reduction(),
                ),
                RaisedButton(
                  textColor: Colors.black,
                  child: new Text('+'),
                  onPressed: () => ShareDataWidget.of(context).increment(),
                ),
              ],
            ),
            Text("$name = ${snapshot.data}"),
          ],
        );
      },
    );
  }
}
```