# docker

mariadb 官方 為 docker 提供了 [鏡像](https://hub.docker.com/_/mariadb)
# 安裝

```
docker pull mariadb
```

# 運行

```
#info="docker-compose.yml"
version: "1"
services:
  main:
    image: mariadb:10.8
    restart: always
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: data
      MYSQL_USER: king
      MYSQL_PASSWORD: 12345678
    volumes:
      - Your_DB_Save_Path:/var/lib/mysql
```

# 使用配置檔案

配置檔案位於 **/etc/mysql/my.cnf**  此檔案 include 了 **/etc/mysql/conf.d**，故創建一個自定義檔案並掛在到 /etc/mysql/conf.d 檔案夾下即可，例如將默認的客戶端編碼由 latin1 改爲 utf8

```
#info="client.cnf"
[client]
default-character-set=utf8mb4
```

```
#info="docker-compose.yml"
version: "1"
services:
  main:
    image: mariadb:10.8
    restart: always
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: data
      MYSQL_USER: king
      MYSQL_PASSWORD: 12345678
    volumes:
      - ./client.cnf:/etc/mysql/conf.d/client.cnf:ro
      - Your_DB_Save_Path:/var/lib/mysql
```