# clipboard.js

clipboard.js 是一個開源(MIT)的 javascript 庫，用於爲瀏覽器提供使用 html5 簡單的將數據拷貝到用戶剪貼板

* 官網 [https://clipboardjs.com/](https://clipboardjs.com/)
* 源碼 [https://github.com/zenorocha/clipboard.js](https://github.com/zenorocha/clipboard.js)