# url_launcher

[url_launcher](https://pub.dev/packages/url_launcher) 是一個 flutter 插件 爲 android/ios 提供了 

* 以默認瀏覽器打開url 
* 創建 email
* 創建 電話撥號
* 創建 SMS 短信發送

等功能

```
dependencies:
  ...
  url_launcher: ^5.4.1
```


```
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(Scaffold(
    body: Center(
      child: RaisedButton(
        onPressed: _launchURL,
        child: Text('Show Flutter homepage'),
      ),
    ),
  ));
}

_launchURL() async {
  const url = 'https://flutter.dev';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
```

建議使用 canLaunch 傳入的 url 是否支持 launch 如果直接調用 launch 不支持會 拋出異常