# [icons](https://primeng.org/icons)

PrimeNG 提供了一些圖標字體，但是需要單獨安裝才能使用

```
npm install primeicons --save
```

通常指定 class 即可使用

```
<i class="pi pi-check"></i>
<i class="pi pi-times"></i>
```

因爲是字體所以設置 font-size 即可調整圖標大小

```
<i class="pi pi-check" style="font-size: 2rem"></i>
```

class **pi-spin** 用來讓圖標支持常見的旋轉動畫

```
<i class="pi pi-spin pi-spinner" style="font-size: 2rem"></i>
```

# [material-icons](https://github.com/marella/material-icons#readme)

primeng 提供的圖標有限，你可以引入包含更多圖標的 [material-icons](https://material.io/tools/icons/?style=baseline)

1. 安裝 material-icons

	```
	npm install material-icons@latest --save
	```
	在 **src/styles.scss** 中 import css
	```
	@import 'material-icons/iconfont/material-icons.css';
	```
	
2. 爲 material-icons 創建 class

	```
	// 使用 mi 作爲 class 前綴
	// mi-home
	// mi-star
	$material-icons-css-prefix: mi;
	// @see https://github.com/twbs/bootstrap/blob/main/scss/_functions.scss
	@function material-icons-str-replace($string, $search, $replace: '') {
			$index: str-index($string, $search);
			@if $index {
					@return (str-slice($string, 1, $index - 1)+$replace)+material-icons-str-replace(str-slice($string, $index + str-length($search)),
							$search,
							$replace );
			}
			@return $string;
	}
	// 創建 material-icons 字體
	// material-icons
	// material-icons-outlined
	// material-icons-round
	// material-icons-sharp
	// material-icons-two-tone 
	@import 'material-icons/iconfont/material-icons.css';
	// 調整字體單位爲 rem
	@each $font-family in ('Material Icons',
			'Material Icons Outlined',
			'Material Icons Round',
			'Material Icons Sharp',
			'Material Icons Two Tone'
	) {
			$full-class-name: to-lower-case($font-family);
			$class-name: material-icons-str-replace($full-class-name, ' ', '-');
			.#{$class-name} {
					font-size: 1.5rem;
			}
	}
	// 使用這個 mixin 來創建 icon class
	@mixin material-icon($icon) {
			&::before {
					content: $icon;
			}
	}
	// 導入 codepoints 定義
	@import 'material-icons/css/_codepoints.scss';
	// 這裏設置要創建的字體風格
	// 設置要創建的 class
	@each $name,
	$codepoint in $material-icons-codepoints {
			.#{$material-icons-css-prefix}-#{$name} {
					@include material-icon("#{$name}");
			}
	}
	```

3. 現在你可以使用 material 提供的 class 替代 PrimeNG 替代的 icon class

	```
	<p-button icon=" material-icons mi-home" pTooltip="Home Page"></p-button>
	```


# [fontawesome](https://fontawesome.com/search)

fontawesome 提供了更多圖標，並且以 PrimeNG 來說使用比 material-icons 更容易

1. 安裝 fortawesome

	```
	npm install @fortawesome/fontawesome-free@latest --save
	```

2. 在 styles.scss 中 import css

	```
	@import "@fortawesome/fontawesome-free/css/all.css";
	```
3. 使用 fontawesome 提供的 class 替代 PrimeNG 替代的 icon class


	```
	<p-button icon="fa-solid fa-house" pTooltip="Home Page"></p-button>
	```