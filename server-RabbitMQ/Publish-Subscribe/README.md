# [Publish/Subscribe](https://www.rabbitmq.com/tutorials/tutorial-three-go.html)

工作隊列將生產者生產的每個工作 分配給一個單獨的消費者處理

發佈/訂閱 模式則 在生產者生產的每個消息 分配給所有訂閱者


# Exchange 

實際上 生產者從來不和 消息隊列交流 生產者 只將消息 傳遞給 Exchange(交換機)

交換機 負責 是否將消息推送到指定隊列

![](assets/exchanges.webp)

RabbitMQ 提供了 多種交換機來 實現不同功能

* direct
* topic
* headers
* fanout 扇出 

在 Work Queues 中 交換機名稱 傳入了空字符串 這將使用 默認交換機 她會將 消息推送到 routing_key 參數指定名稱的消息隊列中

# fanout

扇出交換機 會將消息推給所有綁定的 消息隊列 很適合來打印日誌

* 生產者 創建一個 fanout 交換機 讓後將日誌消息 發送給 交換機
* 一個實時顯示日誌的消費者 創建一個 隊列綁定到交換機 從隊列獲取日誌 並打印到控制檯
* 另外一個記錄日誌的消費者 創建一個 隊列綁定到交換機 從隊列獲取日誌 並記錄到檔案

```
#info="producer"
package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `fanout`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 交換機 只有在交換機不存在時 才會創建
	e = channel.ExchangeDeclare(
		ExchangeName, // 交換機名稱
		"fanout",     //使用 扇出 交換機
		true,         // 是否持久化
		false,        // 是否在沒人使用時 自動刪除
		false,        // 是否爲 internal 如果爲 true 則爲 internal 不會接受消息發佈
		false,        // no-wait
		nil,          // arguments
	)
	if e != nil {
		log.Println(e)
		return
	}

	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {

		// 發佈消息
		e = channel.Publish(
			ExchangeName,
			``,
			false,
			false,
			amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         []byte(strconv.Itoa(i)), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, i)
		} else {
			log.Println(e)
			break
		}
		time.Sleep(time.Millisecond * 1000)
	}
}
```

```
#info="consumer"
package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `fanout`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	consumer(c)
}
func consumer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 交換機 只有在交換機不存在時 才會創建
	e = channel.ExchangeDeclare(
		ExchangeName, // 交換機名稱
		"fanout",     //使用 扇出 交換機
		true,         // 是否持久化
		false,        // 是否在沒人使用時 自動刪除
		false,        // 是否爲 internal 如果爲 true 則爲 internal 不會接受消息發佈
		false,        // no-wait
		nil,          // arguments
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		``,    // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		true,  // 是否持久化
		true,  // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 綁定 隊列到交換機
	e = channel.QueueBind(
		q.Name,       // 隊列名
		``,           // 路由鍵
		ExchangeName, // 交換機
		false,        // no wait
		nil,          // args
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		true, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	for msg := range ch {
		v, e0 := strconv.Atoi(string(msg.Body))
		fmt.Println(`recv <-`, v, e0)
	}
}
```
