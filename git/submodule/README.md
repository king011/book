# 子項目

git 雲允許將 一個 git倉庫 作爲 另外一個 git倉庫 的子項目 並且 保持 commit 獨立

```
# 添加子項目
# 會創建 .gitmodules 檔案 記錄 用到的子項目url 和 保持到本地的 路徑
# 同時會將 子項目 註冊到 .git/config 中
git submodule add xxx
# 添加子模塊 並跟蹤master分支
git submodule add -b master xxx [path]

# 在 clone 帶子項目的 git倉庫後 子項目檔案夾爲空 
# 執行 init 指令 註冊子項目到 .git/config 中
# 執行 update 指令 拉取子項目數據
git submodule init
git submodule update
```
