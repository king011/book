# Cocos Creator

Cocos 是一個 開源(MIT) 的 遊戲開發框架 可以發佈到多個 平臺

Cocos Creator 是一個 可視化的 Cocos 遊戲 客戶端 解決方案

* 官網 [http://www.cocos.com](http://www.cocos.com)
* 文檔 [http://docs.cocos.com/creator/manual/zh/](http://docs.cocos.com/creator/manual/zh/)
* API [http://docs.cocos.com/creator/api/zh/](http://docs.cocos.com/creator/api/zh/)
* 源碼 [https://github.com/cocos2d/cocos2d-x](https://github.com/cocos2d/cocos2d-x)