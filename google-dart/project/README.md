# command line
一個 command line 項目 需要包含 如下 兩個 檔案

1. One top-level **main()** function,通常在 **main.dart** 中,這是 程式入口點
1. **pubspec.yaml** 檔案 定義 項目 名稱 描述 依賴 ...

```
#info="main.dart"
void main(List<String> args) {
  print("cerberus is an idea");
}
```

```
#info="pubspec.yaml"
name: examples
description: dartlang.org example code.
```

# package

對於 package 必須 包含 如下項目
* bin 此檔案夾下是 可執行檔案 源碼
* lib 此檔案夾下是 package 實現代碼
* doc 可選的 api 說明
* test 可選的 測試 代碼
* pubspec.yaml 元信息 記錄了 項目基本信息 和 依賴

```
#info=".gitignore"
# Files and directories created by pub
.dart_tool/
.packages
# Remove the following pattern if you wish to check in your lock file
pubspec.lock

# Conventional directory for build outputs
build/

# Directory created by dartdoc
doc/api/
```
# pub
pub 工具 提供了 對 package 的 管理功能

pub 可以自動 從 [https://pub.dartlang.org/](https://pub.dartlang.org/) 下載 依賴
