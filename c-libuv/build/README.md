# 編譯

libuv支持使用 cmake 編譯故編譯相當容易

1. 從官網[下載](https://dist.libuv.org/dist/)最新源碼 並解壓

2. 創建 編譯用的檔案夾

	```
	mkdir build_linux_amd64 && cd build_linux_amd64
	```
	
3. 使用 cmake 構建

	```
	cmake ../ \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_C_FLAGS="-fPIC"
	```

	```
	#info="交叉編譯"
	cmake ../                 \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_C_FLAGS="-fPIC" \
		-DCMAKE_SYSTEM_NAME=Windows \
		-DCMAKE_SYSTEM_VERSION=6.1  \
		-DCMAKE_C_COMPILER=x86_64-w64-mingw32-gcc
	```
	
4. 編譯項目

	```
	make
	```
	
# FindUV.cmake

cmake 沒有官方的 libuv 查找模塊 可以自己實現個

* 頭文件直接存儲在 CMAKE\_INCLUDE\_PATH 中
* 庫文件存儲在 ${CMAKE\_LIBRARY\_PATH}/uv 中

```
#info="CMakeLists.txt"

# set(UV_STATIC ON) # 鏈接靜態庫 默認鏈接動態庫
find_package(UV
    REQUIRED
)
list(APPEND target_headers ${UV_INCLUDE_DIRS})
list(APPEND target_libs ${UV_LIBRARIES})
```

```
#info="FindUV.cmake"
include(FindPackageHandleStandardArgs)

set(UV_FOUND FALSE)
set(UV_INCLUDE_DIRS)
set(UV_LIBRARIES)

# 查找 頭文件
find_path(UV_INCLUDE_DIRS NAMES uv/version.h)
if(NOT UV_INCLUDE_DIRS)
    if(UV_FIND_REQUIRED)
        message(FATAL_ERROR "libuv : Could not find uv/version.h")
    else()
        message(WARNING "libuv : Could not find uv/version.h")
    endif()
    return()
endif()

# 查找 庫文件
if(DEFINED UV_STATIC)
    find_library(UV_LIBRARIE_UV NAMES uv/libuv_a.a)
    if(NOT UV_LIBRARIE_UV)
        if(UV_FIND_REQUIRED)
            message(FATAL_ERROR "libuv : Could not find lib uv/libuv_a.a")
        else()
            message(WARNING "libuv : Could not find lib uv/libuv_a.a")
        endif()
        return()
    endif()
else()
    if(CMAKE_SYSTEM_NAME STREQUAL Windows)
        find_library(UV_LIBRARIE_UV NAMES uv/libuv.dll.a)
        if(NOT UV_LIBRARIE_UV)
            if(UV_FIND_REQUIRED)
                message(FATAL_ERROR "libuv : Could not find lib uv/libuv.dll.a")
            else()
                message(WARNING "libuv : Could not find lib uv/libuv.dll.a")
            endif()
            return()
        endif()
    else()
        find_library(UV_LIBRARIE_UV NAMES uv/libuv.so)
        if(NOT UV_LIBRARIE_UV)
            if(UV_FIND_REQUIRED)
                message(FATAL_ERROR "libuv : Could not find lib uv/libuv.so")
            else()
                message(WARNING "libuv : Could not find lib uv/libuv.so")
            endif()
            return()
        endif()
    endif()
endif()


list(APPEND UV_LIBRARIES
    "${UV_LIBRARIE_UV}"
)
list(REMOVE_DUPLICATES UV_INCLUDE_DIRS)
list(REMOVE_DUPLICATES UV_LIBRARIES)

message(STATUS "Found libuv: ${UV_LIBRARIES}")
#設置 庫 信息
find_package_handle_standard_args(UV
		DEFAULT_MSG
		UV_INCLUDE_DIRS
		UV_LIBRARIES
)
```