# Browser

瀏覽其中 有多種 安裝方案

## script 標籤
```
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs@2.0.0/dist/tf.min.js"></script>
```

## node 套件

```
yarn add @tensorflow/tfjs
```
or
```
npm install @tensorflow/tfjs
```

# Node

node 環境 同樣 提供了 多種 安裝方案 但和瀏覽器不同 不過方案 將顯著影響運效能

## tfjs-node

tfjs-node 是 C++ bindings 版本

```
yarn add @tensorflow/tfjs-node
```
or
```
npm install @tensorflow/tfjs-node
```

## tfjs-node-gpu (Linux Only)

tfjs-node-gpu 同樣是 C++ bindings 版本 但提供了 NVIDIA® GPU with CUDA support 加速

```
yarn add @tensorflow/tfjs-node-gpu
```
or
```
npm install @tensorflow/tfjs-node-gpu
```

## tfjs
純js實現 最慢的版本

```
yarn add @tensorflow/tfjs
```
or
```
npm install @tensorflow/tfjs
```

# typescirpt

如果使用 typescirpt 開發 需要設置  **skipLibCheck: true** 到 **tsconfig.json**  否則 使用嚴格的 null checking 可能會編譯出錯

