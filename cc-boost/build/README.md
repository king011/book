# bootstrap.sh

bootstrap.sh 是 boost 提供的幫助腳本 用於構建 boost

```
# 查看詳細使用說明
./bootstrap.sh -h 

# 編譯 b2 並指定排除一些本喵不用的庫
./bootstrap.sh

# 顯示所有 需要編譯的庫
./bootstrap.sh --show-libraries 
```

# b2 

b2 是 boost 提供的 構建工具

```
# 查看詳細使用說明
./b2 --help
```
```
./b2 variant=release link=static threading=multi \
	install --prefix=../linux
```

目前(1.76.0)下列庫需要編譯
* atomic
* chrono
* container
* context
* contract
* coroutine
* date_time
* exception
* fiber
* filesystem
* graph
* graph_parallel
* headers
* iostreams
* json
* locale
* log
* math
* mpi
* nowide
* program_options
* python
* random
* regex
* serialization
* stacktrace
* system
* test
* thread
* timer
* type_erasure
* wave
# 交叉編譯

在執行 b2 指令前 修改 **project-config.jam** 第 12 行 指定使用的 g++ 編譯器即可
```
#info=10
if ! gcc in [ feature.values <toolset> ]
{
	using gcc ;
}
```

```
#info=10
if ! gcc in [ feature.values <toolset> ]
{
	using gcc : : x86_64-w64-mingw32-g++-posix ;
}
```

```
./b2 variant=release link=static threading=multi \
	install --prefix=../windows
```