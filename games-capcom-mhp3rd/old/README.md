# 峯山龍
![](assets/峯山龍.jpeg)

## 基本抗性
◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>颚<br>口内<br>背鳍(裂缝)</center></div>
</td><td><div><center>颚<br>口内<br>背鳍(裂缝)</center></div>
</td><td><div><center>口内<br>背鳍(裂缝)</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 牙 </td><td> 18 </td><td> 18 </td><td> 18 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 15 </td><td> 15 </td><td> 0 </td><td> 左600/右600
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 21 </td><td> 21 </td><td> 20 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 8 </td><td> 8 </td><td> 0 </td><td> 1000
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颚 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">55 </font></td><td> 38 </td><td> 0 </td><td> 12 </td><td> 12 </td><td> 22 </td><td> 22 </td><td> 0 </td><td> 1000
</td></tr>
<tr style="background:#f9f9f9;">
<td> 口内 </td><td> <font color="#ff0000">90 </font></td><td> <font color="#ff0000">80 </font></td><td> <font color="#ff0000">60 </font></td><td> 0 </td><td> 20 </td><td> 20 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 1000
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腕 </td><td> 33 </td><td> 33 </td><td> 25 </td><td> 0 </td><td> 8 </td><td> 8 </td><td> 15 </td><td> 15 </td><td> 0 </td><td> 左750/右750
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背鳍 </td><td> 35 </td><td> 35 </td><td> 40 </td><td> 0 </td><td> 10 </td><td> 5 </td><td> 15 </td><td> 15 </td><td> 0 </td><td> 900
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背鳍(裂缝) </td><td> <font color="#ff0000">68 </font></td><td> <font color="#ff0000">78 </font></td><td> <font color="#ff0000">48 </font></td><td> 0 </td><td> 15 </td><td> 15 </td><td> 22 </td><td> 22 </td><td> 0 </td><td> 前750/后500
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> - </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> - </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> - </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> - </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> - </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果

※陷阱、道具以及肉类全都无效
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体(嘴附近) </td><td> 4 </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E7%94%B2%E5%B2%A9%E5%A3%B3" title="峯山龙的甲岩壳">峯山龙的甲岩壳</a> <font color="#008000"><b>52%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%9D%9A%E7%A1%AC%E7%9A%84%E9%BE%99%E7%89%99" title="坚硬的龙牙">坚硬的龙牙</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E5%9D%9A%E5%B2%A9%E5%A3%B3" title="峯山龙的坚岩壳">峯山龙的坚岩壳</a> <font color="#008000"><b>46%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E4%B8%8A%E8%8B%8D%E9%B3%9E" title="峯山龙的上苍鳞">峯山龙的上苍鳞</a> <font color="#008000"><b>28%</b></font><br><a href="/%E5%87%B6%E7%8C%9B%E7%9A%84%E9%BE%99%E9%94%90%E7%89%99" title="凶猛的龙锐牙">凶猛的龙锐牙</a> <font color="#008000"><b>12%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a> <font color="#008000"><b>4%</b></font> </td><td> 嘴部附近剥取
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体(腕附近) </td><td> 4 </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E7%94%B2%E5%B2%A9%E5%A3%B3" title="峯山龙的甲岩壳">峯山龙的甲岩壳</a> <font color="#008000"><b>52%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%9D%9A%E7%A1%AC%E7%9A%84%E9%BE%99%E7%89%99" title="坚硬的龙牙">坚硬的龙牙</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E5%9D%9A%E5%B2%A9%E5%A3%B3" title="峯山龙的坚岩壳">峯山龙的坚岩壳</a> <font color="#008000"><b>46%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E4%B8%8A%E8%8B%8D%E9%B3%9E" title="峯山龙的上苍鳞">峯山龙的上苍鳞</a> <font color="#008000"><b>28%</b></font><br><a href="/%E5%87%B6%E7%8C%9B%E7%9A%84%E9%BE%99%E9%94%90%E7%89%99" title="凶猛的龙锐牙">凶猛的龙锐牙</a> <font color="#008000"><b>12%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a> <font color="#008000"><b>4%</b></font> </td><td> 身体腕部附近剥取
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背上采矿 </td><td> 8 </td><td> <a href="/%E5%BD%A9%E6%B0%B4%E6%99%B6" title="彩水晶">彩水晶</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E7%94%B2%E5%B2%A9%E5%A3%B3" title="峯山龙的甲岩壳">峯山龙的甲岩壳</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E6%B8%8A%E6%B0%B4%E6%99%B6" title="渊水晶">渊水晶</a> <font color="#008000"><b>40%</b></font><br><a href="/%E5%BD%A9%E6%B0%B4%E6%99%B6" title="彩水晶">彩水晶</a> <font color="#008000"><b>26%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E7%94%B2%E5%B2%A9%E5%A3%B3" title="峯山龙的甲岩壳">峯山龙的甲岩壳</a> <font color="#008000"><b>14%</b></font> </td><td> 上背部，左右各两个挖矿点，一点可挖两次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 嘴内 </td><td> 2 </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a> <font color="#008000"><b>49%</b></font><br><a href="/%E6%80%AA%E7%89%A9%E7%9A%84%E8%82%9D" title="怪物的肝">怪物的肝</a> <font color="#008000"><b>35%</b></font><br><a href="/%E5%B7%A8%E5%A4%A7%E9%B3%8D" title="巨大鳍">巨大鳍</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a> <font color="#008000"><b>1%</b></font> </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E4%B8%8A%E8%8B%8D%E9%B3%9E" title="峯山龙的上苍鳞">峯山龙的上苍鳞</a> <font color="#008000"><b>19%</b></font><br><a href="/%E4%BC%98%E8%B4%A8%E9%B3%8D" title="优质鳍">优质鳍</a> <font color="#008000"><b>27%</b></font><br><a href="/%E6%80%AA%E7%89%A9%E7%9A%84%E8%82%9D" title="怪物的肝">怪物的肝</a> <font color="#008000"><b>23%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a> <font color="#008000"><b>1%</b></font> </td><td> 在决战场中将其击倒以后，到其嘴里剥取
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 牙 </td><td> <a href="/%E5%9D%9A%E7%A1%AC%E7%9A%84%E9%BE%99%E7%89%99" title="坚硬的龙牙">坚硬的龙牙</a>*1 <font color="#008000"><b>83%</b></font><br><a href="/%E5%9D%9A%E7%A1%AC%E7%9A%84%E9%BE%99%E7%89%99" title="坚硬的龙牙">坚硬的龙牙</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a>*1 <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E5%87%B6%E7%8C%9B%E7%9A%84%E9%BE%99%E9%94%90%E7%89%99" title="凶猛的龙锐牙">凶猛的龙锐牙</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E5%9D%9A%E7%A1%AC%E7%9A%84%E9%BE%99%E7%89%99" title="坚硬的龙牙">坚硬的龙牙</a>*2 <font color="#008000"><b>14%</b></font><br><a href="/%E5%87%B6%E7%8C%9B%E7%9A%84%E9%BE%99%E9%94%90%E7%89%99" title="凶猛的龙锐牙">凶猛的龙锐牙</a>*2 <font color="#008000"><b>12%</b></font><br><a href="/%E5%A4%A7%E5%9C%B0%E7%9A%84%E9%BE%99%E7%8E%89" title="大地的龙玉">大地的龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 破坏一只或两只长牙，胜利后奖励获得
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腕 </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="峯山龙的腕甲">峯山龙的腕甲</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="峯山龙的腕甲">峯山龙的腕甲</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E5%8F%A4%E9%BE%99%E4%B9%8B%E8%A1%80" title="古龙之血">古龙之血</a>*2 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E4%B8%8A%E8%85%95%E7%94%B2" title="峯山龙的上腕甲">峯山龙的上腕甲</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E4%B8%8A%E8%8B%8D%E9%B3%9E" title="峯山龙的上苍鳞">峯山龙的上苍鳞</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="峯山龙的腕甲">峯山龙的腕甲</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E5%8F%A4%E9%BE%99%E4%B9%8B%E8%A1%80" title="古龙之血">古龙之血</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 主要在决战之地，破坏一只或两只前爪，胜利后奖励获得
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背(弱点石) </td><td> <a href="/%E5%BD%A9%E6%B0%B4%E6%99%B6" title="彩水晶">彩水晶</a>*1 <font color="#008000"><b>16%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E8%8B%8D%E9%B3%9E" title="峯山龙的苍鳞">峯山龙的苍鳞</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E7%94%B2%E5%B2%A9%E5%A3%B3" title="峯山龙的甲岩壳">峯山龙的甲岩壳</a>*1 <font color="#008000"><b>49%</b></font><br><a href="/%E5%8F%A4%E9%BE%99%E4%B9%8B%E8%A1%80" title="古龙之血">古龙之血</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E5%9D%9A%E5%B2%A9%E5%A3%B3" title="峯山龙的坚岩壳">峯山龙的坚岩壳</a>*1 <font color="#008000"><b>43%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E4%B8%8A%E8%8B%8D%E9%B3%9E" title="峯山龙的上苍鳞">峯山龙的上苍鳞</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E5%8F%A4%E9%BE%99%E4%B9%8B%E8%A1%80" title="古龙之血">古龙之血</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E6%B8%8A%E6%B0%B4%E6%99%B6" title="渊水晶">渊水晶</a>*1 <font color="#008000"><b>14%</b></font><br><a href="/%E5%B3%AF%E5%B1%B1%E9%BE%99%E7%9A%84%E7%94%B2%E5%B2%A9%E5%A3%B3" title="峯山龙的甲岩壳">峯山龙的甲岩壳</a>*1 <font color="#008000"><b>8%</b></font> </td><td> 上背上后破坏前后各一块弱点石，胜利后奖励获得
</td></tr></tbody></table>
## 捕獲
※本怪物不可捕获
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>大砂漠の宴！！
</p>
</td><td>
<p>讨伐一头峯山龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>峯山龍ジエン・モーラン
</p>
</td><td>
<p>击退一头峯山龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>大砂漠の宴！！
</p>
</td><td>
<p>讨伐一头峯山龙
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体撞击 </td><td> 侧身撞击船体，在船头以及船尾边缘不会撞到
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙牙冲撞 </td><td> 峯山龙正面全局判定，贯穿全船，可在正面以外区域或飞扑闪避
</td></tr>
<tr style="background:#f9f9f9;">
<td> 投岩 </td><td> 4~5块岩石，落点以猎人位置未定，翻滚闪躲
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】 </td><td> 范围超大，可防御，不可翻滚闪避，属于高吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙牙横扫 </td><td> 范围判定峯山龙头部为圆心270°，后脚贴近峯山龙不会被攻击到，可用飞扑躲避
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞跃 </td><td> 从船体的一侧跃到另一侧
</td></tr>
<tr style="background:#f9f9f9;">
<td> 挣扎 </td><td> 挣扎身体，将猎人从身体上晃下去，有攻击判定
</td></tr></tbody></table>