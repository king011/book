# gob: registering duplicate names for xx.Xxx

如果使用了緩存，兩個 同名 package 中的 bean 不能同名，因爲 xorm 會將其使用 gob 註冊，這會導致 gob panic 上述異常

此外即時是相同的包，也不能 struct 和 &struct 混用 insert get find，建議按照如下寫法寫入代碼

* Insert(&struct) Insert([]\*struct)
* InsertMulti([]\*struct)
* Get(&struct)
* Find(&[]struct)
* XXX(&struct)

因爲 Get 必須使用指針形式所以只能全部 bean 都使用指針形式，只有在 Find 時使用 struct 切片(這個不會引發問題)，需要特別注意的是 Insert InsertMulti 需要傳入切片的元素也是 指針 否則，同樣會出現 gob panic
