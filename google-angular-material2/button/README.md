# MatButtonModule

MatButtonModule 中 包含多種不同風格的 按鈕

```typescript
import {MatButtonModule} from '@angular/material/button';
```

# Example

```html
<button mat-button>Basic</button>
<button mat-button disabled>disabled</button>
<a mat-button routerLink=".">Link</a>
<button mat-raised-button>Basic</button>
<button mat-stroked-button>Basic</button>
<button mat-flat-button>Basic</button>
<button mat-icon-button>
  <mat-icon aria-label="Example icon-button with a heart icon">favorite</mat-icon>
</button>
<button mat-fab>Basic</button>
<button mat-mini-fab>Basic</button>
```
![buttons](assets/buttons.png)