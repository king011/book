# [docker](https://hub.docker.com/_/alpine)

docker 官方提供了 alpine 的鏡像，但是 alpine 使用了 musl 而非 glibc 這導致一些依賴 glibc 的程序可能會出現問題，並且 alpine 對包都是按照最小標準集成的，如果要在裏面運行大量依賴的項目比如一個 python 這可能不是一個好主意，但對於 golang 完全靜態打包的程序或許是一個不錯的注意