# Cursor

Bucket 提供了一個 Cursor 函數 可以 返回一個 遊標 用於 遍歷 Bucket

```
// 返回 關聯的 Bucket
func (c *Cursor) Bucket() *Bucket

// 刪除 當前 遊標
func (c *Cursor) Delete() error
// .
func (c *Cursor) First() (key []byte, value []byte)
// .
func (c *Cursor) Last() (key []byte, value []byte)
// .
func (c *Cursor) Next() (key []byte, value []byte)
// .
func (c *Cursor) Prev() (key []byte, value []byte)
// 將遊標 移動到指定 key 如果key不存在 相對於調用 next
func (c *Cursor) Seek(seek []byte) (key []byte, value []byte)
```

# Example
```
package main
 
import (
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"time"
)
 
func main() {
	TableName := []byte("users")
 
	// 如果 不存在檔案 則 創建數據庫
	// 否則 打開數據庫
	db, e := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
	if e != nil {
		log.Fatal(e)
	}
	// 關閉 數據庫
	defer db.Close()
 
	// 啟動 一個 只讀 事務
	e = db.View(func(t *bolt.Tx) error {
		b := t.Bucket(TableName)
 
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			fmt.Printf("key=%s, value=%s\n", k, v)
		}
		return nil
	})
	if e != nil {
		log.Fatal(e)
	}
}
```

# ForEach

Bucket 提供了 ForEach 用於遍歷 所有 元素

```
package main
 
import (
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"time"
)
 
func main() {
	TableName := []byte("users")
 
	// 如果 不存在檔案 則 創建數據庫
	// 否則 打開數據庫
	db, e := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
	if e != nil {
		log.Fatal(e)
	}
	// 關閉 數據庫
	defer db.Close()
 
	// 啟動 一個 只讀 事務
	e = db.Update(func(t *bolt.Tx) error {
		b := t.Bucket(TableName)
 
		return b.ForEach(func(k, v []byte) error {
			fmt.Printf("key=%s, value=%s\n", k, v)
			if string(k) == "5" {
				if e = b.Delete(k); e != nil {
					return e
				}
			}
			return nil
		})
	})
	if e != nil {
		log.Fatal(e)
	}
}
```

# 注意
上述 兩種 range 的方法 都會將 子 Bucket 遍歷出來 此時 val 將 為nil

如果 要操作 子 Bucket 此時可以調用 Bucket 函數 獲取子 Bucket

