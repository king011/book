# Intl

Luxon 使用本機 Intl API 來提供易於使用的國際化

```
DateTime.now()
  .setLocale("el")
  .toLocaleString(DateTime.DATE_FULL); //=>  '24 Σεπτεμβρίου 2017'
```

## 確保可以訪問其它語言環境

請參閱 [安裝指南](https://moment.github.io/luxon/#/install) 以確保你的平臺可以訪問 Intl API 和 ICU 數據。這對 node 來說尤其重要，默認情況下它不附帶 ICU 數據

## locales 如何工作

Luxon 的 DateTimes 可以使用 [BCP 47](https://www.rfc-editor.org/rfc/rfc5646) 語言環境字符串進行配置，指定要使用的語言來生成或解析字符串。本機 Intl API 提供實際的國際化字符串；Luxon 只爲其提供了一層便利，並將本地化功能集成到 Luxon 中。[Mozilla MDN Intl 文檔](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl#Locale_identification_and_negotiation) 很好的描述了 locale 參數的工作方式。在 Luxon 中，方法名不同但語義相同，只是 Luxon 允許你指定編號系統並獨立與語言環境字符串輸出日曆

# 設置 locale
locale 是 Luxon 對象的屬性。一般可以在構建時設置

```
var dt = DateTime.fromISO("2017-09-24", { locale: "fr" });
dt.locale; //=> 'fr'
```

在這種情況下，指定語言環境並沒有改變解析的工作方式，但它在創建的實例中設置了語言環境。對於其它的工廠方法，例如 fromFormat，locale 參數會影響字符串的解析方式。

你可以使用 setLocale 來創建一個使用新語言環境的 DateTime 副本

```
DateTime.now().setLocale("fr").locale; //=> 'fr'
```

setLocale 只是 reconfigure 更改 locale 的語法糖
```
DateTime.now().reconfigure({ locale: "fr" }).locale;
```

# 默認 locale

默認情況下，新的 DateTime 和 Duration 的 locale 是系統語言環境。在瀏覽器上 這通常是用戶瀏覽器或操作系統設置的值。在 node 上通常是 en-US

因此 toLocaleString toLocaleParts 和其它人類可讀的字符串將默認在用戶語言環境中生成字符串

但是 fromFormat toFormat 等依賴與 en-US。因爲這些方法通常不關心用戶的區域設置。但是有一個特例 toFormat 的宏格式，例如 "D" 它會生成本地化的字符串

## Settings.defaultLocale 

你可以通過 Settings.defaultLocale  設置默認的語言環境

```
import { DateTime, Settings } from "luxon";

Settings.defaultLocale = "fr";
DateTime.now().locale; //=> 'fr'
```

> toFormat 和 fromFormat 的行爲不會被改變

## 使用語言系統解析

通常不希望 fromFormat 和 toFormat 使用語言環境，因爲你的格式對語言環境的字符串順序不敏感。這就是爲什麼 Luxon 默認不這樣做的原因。但是如果你真的想要這種行爲你可以這樣做

```
Settings.defaultLocale = DateTime.now().resolvedLocaleOptions().locale;
```

## 檢查你得到了什麼

本地環境可能不支持你要求的確切語言環境。本機 Intl API 將嘗試找到最佳匹配。如果你想找到匹配到了什麼，使用 resolvedLocaleOpts 獲取

```
DateTime.local({ locale: "fr-co" }).resolvedLocaleOptions(); //=> { locale: 'fr',
//     numberingSystem: 'latn',
//     outputCalendar: 'gregory' }
```

# 受環境影響的方法

一些方法將收到語言環境所影響

## Formatting

受語言環境影響最重要的方法是 toLocaleString ，它將生成國際化的人類可讀字符串

```
dt.setLocale("fr").toLocaleString(DateTime.DATE_FULL); //=> '25 septembre 2017'
```

除了通常的將 local 設置到 DateTime 本身，也可以直接告訴 toLocaleString 要輸出的語言環境

```
dt.toLocaleString({ locale: "es" , ...DateTime.DATE_FULL }); //=> '25 de septiembre de 2017'
```

Ad-hoc formatting  也 支持 locale

```
dt.setLocale("fr").toFormat("MMMM dd, yyyy GG"); //=> 'septembre 25, 2017 après Jésus-Christ'
```

## Parsing

你可以解析本地字符串

```
DateTime.fromFormat("septembre 25, 2017 après Jésus-Christ", "MMMM dd, yyyy GG", { locale: "fr" });
```

## Listing

一些方法列出的名稱例如 Info 提供的函數會收到 locale 影響

```
Info.months("long", { locale: "fr" }); //=> [ 'janvier', 'février', ...
Info.weekdays("long", { locale: "fr" }); //=> [ 'lundi', 'mardi', ...
Info.eras("long", { locale: "fr" }); //=> [ 'avant Jésus-Christ', 'après Jésus-Christ' ]
```

# numberingSystem

DateTimes 還具有 numberingSystem 設置，可讓你控制格式化中使用的數字系統。通常，你不應該覆蓋區域設置提供的編號系統。例如，不需要額外的工作就可以讓阿拉伯數字出現在將阿拉伯語的語言環境中:
```
var dt = DateTime.now().setLocale("ar");

dt.resolvedLocaleOptions(); //=> { locale: 'ar',
//     numberingSystem: 'arab',
//     outputCalendar: 'gregory' }

dt.toLocaleString(); //=> '٢٤‏/٩‏/٢٠١٧'
```

處於這個原因，Luxon 將自己的 numberingSystem 屬性默認爲 null，這意謂著讓 Intl API 決定。但是，你可以根據需要覆蓋它。這個例子無疑是荒謬的：

```
const dt = DateTime.local().reconfigure({ locale: "it", numberingSystem: "beng" });
dt.toLocaleString(DateTime.DATE_FULL); //=> '২৪ settembre ২০১৭'
```

與 locale 設置類似，你可以爲新的實例設置默認的編號系統

```
Settings.defaultNumberingSystem = "beng";
```