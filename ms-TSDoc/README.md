# [TSDoc](https://tsdoc.org/)

TSDoc 是對 TypeScript 註釋的一個標準化提議，以便不同的工具可以提取內容而不會被彼此的標記混系，它和 JSDoc 很類似

```
export class Statistics {
  /**
   * Returns the average of two numbers.
   *
   * @remarks
   * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
   *
   * @param x - The first input number
   * @param y - The second input number
   * @returns The arithmetic mean of `x` and `y`
   *
   * @beta
   */
  public static getAverage(x: number, y: number): number {
    return (x + y) / 2.0;
  }
}
```

你可以 [在線嘗試](https://tsdoc.org/play) 嘗試 TSDoc