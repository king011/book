# web
和 万维网 相關的技巧

# 工具
* [webpack](web-webpack/0)

# html
* [webapi](webapi/0)
* [html5](web-html5/0)
* [css](web-css/0)
* [RESTful](web-RESTful/0)

# javascript
* [xterm.js](web-js-xterm/0)
* [Chart.js](web-js-chartjs/0)
* [js-xlsx](web-js-xlsx/0)
* [PixiJS](web-js-PixiJS/0) 2d 引擎
* [Broadway.js](web-js-Broadway/0) h264 解碼器
* [video.js](web-videojs/0) 視頻播放器
* [clipboard.js](web-clipboard/0) 剪貼板
* [qrcode](web-qrcode/0) 二維碼生成
## jquery
### 插件
* [jqplot](web-jquery-jqplot/0)

# css
* [scss](web-css-scss/0)
* [PrimeFlex](web-css-PrimeFlex/0)