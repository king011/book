# [一般音頻](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/audio.html)

播放音訊對於大多數遊戲來說都是必不可少的，所以我們讓它變得簡單！

首先，您必須將 Flame\_audio 加入到 pubspec.yaml 檔案中的依賴項清單中：

```
dependencies:
  flame_audio: VERSION
```

最新版本可以在 [pub.dev](https://pub.dev/packages/flame_audio/install) 上找到

安裝 Flame\_audio 套件後，您可以在 pubspec.yaml 檔案的 asset 部分新增音訊檔案。 確保音訊檔案存在於您提供的路徑中

FlameAudio 的預設目錄是 asset/audio （可透過提供您自己的 AudioCache 實例來更改）

對於下面的範例，您的 pubspec.yaml 檔案需要包含以下內容：

```
flutter:
  assets:
    - assets/audio/explosion.mp3
    - assets/audio/music.mp3
```

那麼您可以使用以下方法：


```
import 'package:flame_audio/flame_audio.dart';

// 對於較短的重複使用的音訊剪輯，例如音效
FlameAudio.play('explosion.mp3');

// 用於循環播放音訊文件
FlameAudio.loop('music.mp3');

// 用於播放較長的音訊文件
FlameAudio.playLongAudio('music.mp3');

// 用於循環較長的音訊文件
FlameAudio.loopLongAudio('music.mp3');

// 暫停/恢復遊戲時應暫停/播放的背景音樂
FlameAudio.bgm.play('music.mp3');
```

play/loop 和 playLongAudio/loopLongAudio 之間的區別在於，play/loop 使用最佳化功能，允許聲音在迭代之間無間隙地循環，並且幾乎不會發生遊戲幀速率下降的情況。 您應該盡可能選擇前一種方法

playLongAudio/loopLongAudio 允許播放任意長度的音頻，但它們確實會導致幀速率下降，並且循環音頻在迭代之間會有很小的間隙

您可以使用 Bgm 類別（透過 FlameAudio.bgm）來播放循環背景音樂曲目。 當遊戲發送到後台或返回前台時，Bgm 類別允許 Flame 自動管理背景音樂曲目的暫停和恢復

如果您想以非常有效的方式發出快速聲音效果，您可以使用 AudioPool 類別。 AudioPool 將保留一個預先載入給定聲音的音訊播放器池，並允許您快速連續地播放它們

我們推薦的一些跨裝置工作的檔案格式包括：MP3、OGG 和 WAV

這個橋接庫（flame\_audio）使用 [audioplayers](https://github.com/bluefireteam/audioplayers) 來允許同時播放多個聲音（在遊戲中至關重要）。 您可以查看連結以獲取更深入的解釋

在播放和循環中，您都可以傳遞一個額外的可選雙參數，即音量（預設為 1.0）

play 和loop 方法都從audioplayers 庫傳回一個AudioPlayer 實例，它允許您停止、暫停和配置其他參數

事實上，您始終可以直接使用 AudioPlayers 來完全控制音訊的播放方式 - FlameAudio 類別只是常見功能的包裝

# [Caching](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/audio.html#caching)

您可以預先載入您的資源。 音訊在第一次請求時需要儲存在記憶體中； 因此，第一次播放每個 mp3 時可能會出現延遲。 為了預先載入音頻，只需使用：

```
await FlameAudio.audioCache.load('explosion.mp3');
```

您可以在遊戲的 onLoad 方法開始時加載所有音頻，以便它們始終能夠流暢播放。 若要載入多個音訊文件，請使用 loadAll 方法：

```
await FlameAudio.audioCache.loadAll(['explosion.mp3', 'music.mp3']);
```

最後，您可以使用clear方法來刪除已載入到快取中的檔案：

```
FlameAudio.audioCache.clear('explosion.mp3');
```

還有一個clearCache方法，可以清除整個快取。

例如，如果您的遊戲有多個級別並且每個級別都有一組不同的聲音和音樂，這可能會很有用。