# find_package

find_package 指令 用來 查找一個 軟件包

```sh
#info=false
# 查看 支持的 軟件包
$ cmake --help-module-list | egrep ^Find
FindALSA
FindASPELL
FindAVIFile
FindArmadillo
FindBISON
FindBLAS
FindBZip2
FindBacktrace
FindBoost
...
```

官方 自帶的 cmake 通常位於  **/usr/share/cmake/Modules/ **

# 查找 指定包
```cmake
cmake_minimum_required(VERSION 3.0)
project(console)
# 查找 Boost 包
# 可選的 1.55 指定了 版本 至少 1.55 或 以上
# 可選的 REQUIRED 指定了 如果 未找到 結束 cmake
# 可選的 COMPONENTS 指定了 需要 查找的 子庫
find_package(Boost 
    1.55 
    REQUIRED
    COMPONENTS 
        date_time
        system
)

# ${Name}_FOUND 記錄了 是否找到 包
message(${Boost_FOUND})

# ${Name_INCLUDE_DIRS} 記錄了 包的 include 目錄
message(${Boost_INCLUDE_DIRS})

# ${Name_INCLUDE_DIRS} 記錄了 需要連接的 庫 應該加入到 target_link_libraries 中
message(${Boost_LIBRARIES})

add_executable(console 
    main.cpp
)
target_link_libraries(console 
    ${Boost_LIBRARIES}
)
```

> 不同的 find 模塊可能會 稍有不同 應該 參考官方文檔
> 

# 編寫 find 模塊
find_package 傳入 的模塊 其實是 一個 cmake 檔案 完全可以自己寫個 FindXXX.cmake 即可

## CMAKE\_MODULE\_PATH
CMAKE\_MODULE\_PATH 定義了 find\_package 到哪裏去 查找 模塊 

通常 需要 把自定義的 查找模塊 加入到 CMAKE\_MODULE\_PATH

```cmake
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")
```

