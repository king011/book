# File System

namespace Deno 下存在一些操作檔案系統的函數,這些函數

```
// 修改檔案權限
// await Deno.chmod("/path/to/file", 0o664);
// await Deno.chmod("/path/to", 0o775);
function chmod(path: string | URL, mode: number): Promise<void>;

// 修改檔案擁有者
function chown(
    path: string | URL,
    uid: number | null,
    gid: number | null,
  ): Promise<void>;

// 複製檔案
function copyFile(
    fromPath: string | URL,
    toPath: string | URL,
  ): Promise<void>;

// 創建檔案
function create(path: string | URL): Promise<FsFile>

// 獲取檔案信息
function fstat(rid: number): Promise<FileInfo>;

// 將檔案截斷或擴展到指定長度(擴展數據使用 0 填充)
// 注意需要傳入 檔案 id 而非名稱
function ftruncate(rid: number, len?: number): Promise<void>

// 修改 檔案 時間 
function futime(
    rid: number,
    atime: number | Date,
    mtime: number | Date,
  ): Promise<void>;
	
// 創建硬鏈接
function link(oldpath: string, newpath: string): Promise<void>

// 類似 stat 但對於硬鏈接 會返回 硬鏈接 本身信息而非其指向檔案的信息
function lstat(path: string | URL): Promise<FileInfo>

// 創建臨時檔案夾，默認在系統臨時目錄下創建
function makeTempDir(options?: MakeTempOptions): Promise<string>;

// 創建臨時檔案，默認在系統臨時目錄下創建
function makeTempFile(options?: MakeTempOptions): Promise<string>;

// 創建檔案夾
function mkdir(
    path: string | URL,
    options?: MkdirOptions,
  ): Promise<void>;

// 打開檔案
function open(
    path: string | URL,
    options?: OpenOptions,
  ): Promise<FsFile>;
	
// 讀取檔案夾
function readDir(path: string | URL): AsyncIterable<DirEntry>

// 讀取檔案
function readFile(
    path: string | URL,
    options?: ReadFileOptions,
  ): Promise<Uint8Array>
	
// 讀取軟連接的指向路徑,如果不是軟鏈接 拋出異常
function readLink(path: string | URL): Promise<string>

// 將檔案以 utf8 讀取
function readTextFile(
    path: string | URL,
    options?: ReadFileOptions,
  ): Promise<string>;
	
// 解析路徑到絕對路徑，如果時軟鏈接會解析其指向的路徑
function realPath(path: string | URL): Promise<string>

// 刪除檔案
function remove(
    path: string | URL,
    options?: RemoveOptions,
  ): Promise<void>;

// 重新命名檔案
function rename(
    oldpath: string | URL,
    newpath: string | URL,
  ): Promise<void>;

// 獲取檔案信息
function stat(path: string | URL): Promise<FileInfo>

// 創建軟鏈接
function symlink(
    oldpath: string | URL,
    newpath: string | URL,
    options?: SymlinkOptions,
  ): Promise<void>

// 截斷/擴展檔案 檔案不存在拋出異常
function truncate(name: string, len?: number): Promise<void>

// 修改檔案時間
function utime(
    path: string | URL,
    atime: number | Date,
    mtime: number | Date,
  ): Promise<void>
	
// 監控檔案/檔案夾
function watchFs(
    paths: string | string[],
    options?: { recursive: boolean },
  ): FsWatcher
	
// 寫入檔案
function writeFile(
    path: string | URL,
    data: Uint8Array,
    options?: WriteFileOptions,
  ): Promise<void>

// 以 utf8 字符串寫入檔案
function writeTextFile(
    path: string | URL,
    data: string,
    options?: WriteFileOptions,
  ): Promise<void>
```
# watchFs

watchFs 可以用來監控 檔案/檔案夾的 變化

目前測試(deno 1.28.1 (release, x86_64-unknown-linux-gnu))傳入檔案無法持續監控到檔案的寫入變化可能存在bug，替代解決方案是監控檔案所在的檔案夾然後自己過濾掉其它檔案的事件

```
const w = Deno.watchFs("src");
for await (const evt of w) {
  console.log(evt.kind, evt.paths);
}
```

