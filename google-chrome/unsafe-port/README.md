# 訪問非安全端口

chrome 禁用了對一些非安全端口的訪問 如果需要訪問 需要在啓動時加上啓動參數 `--explicitly-allowed-ports=xxx,xxx`

firefox 同樣禁用了一些非安全端口 需要訪問 `about:config` 爲 `network.security.ports.banned.override` 屬性設置允許訪問的非安全端口 `xxx,xxx`