# Form

flutter 提供了 Form 來實現 表單功能 需要創建 一個 GlobalKey 設置到 Form 來存儲 表單 狀態

![](assets/Form.png)

```dart
import 'package:flutter/material.dart';
import './server.dart';
import '../db/SharedPreferences.dart' show Remote;
import '../i18n/generated_i18n.dart';

class MyLoginPage extends StatefulWidget {
  MyLoginPage({@required this.remote, Key key}) : super(key: key);
  final Remote remote;

  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  // 保存 表單 狀態
  GlobalKey<FormState> _form = new GlobalKey<FormState>();
  // 保存 用戶輸入的 用戶名/密碼
  String _name;
  String _password;
  // 是否隱藏密碼
  bool _obscure = true;
  // 定義 提交表單 函數
  void _submit() {
    // 獲取 表單 當前 狀態
    final form = _form.currentState;

    // 驗證 表單
    if (form.validate()) {
      // 保存表單
      form.save();

      debugPrint("submit\nname = $_name\npassword = $_password");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Tooltip(
          message: S.of(context).loginSwitch,
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                  builder: (context) {
                    return MyServerPage();
                  },
                ),
                (route) => false,
              );
            },
          ),
        ),
        title: Text(widget.remote.name),
      ),
      body: Form(
        // 創建一個 form 並且設置 GlobalKey 保存 表單狀態
        key: _form,
        child: Container(
          padding: EdgeInsets.all(16),
          alignment: FractionalOffset(0.5, 0.2),
          child: ListView(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: S.of(context).loginName,
                ),
                autofocus: true, //true 自動獲取 焦點
                onSaved: (v) {
                  // onSaved 會在表單 保存時 回調
                  // 在此 更新表單值到 自定義的 變量中
                  _name = v;
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: S.of(context).loginPassword,
                  suffixIcon: IconButton(
                    icon: _obscure
                        ? Icon(Icons.visibility_off)
                        : Icon(Icons.visibility),
                    onPressed: () {
                      setState(() {
                        _obscure = !_obscure;
                      });
                    },
                  ),
                ),
                obscureText: _obscure, // 如果爲true 不顯示文本
                autovalidate: true, // 如果爲true 自動 驗證
                validator: (v) {
                  // validator 會在 表單驗證時 觸發 返回 非 null 代表 錯誤
                  return v.length < 4 ? "密碼長度必須>=4" : null;
                },
                onSaved: (v) {
                  // onSaved 會在表單 保存時 回調
                  // 在此 更新表單值到 自定義的 變量中
                  _password = v;
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _submit,
        child: new Text(S.of(context).loginSubmit),
      ),
    );
  }
}
```