# duktape

duktape 是一個 用c實現的 嵌入式 開源 js運行 環境

可以方便的集成到 c/c++ 項目中

* 官網 [https://duktape.org/index.html](https://duktape.org/index.html)
* API [https://duktape.org/api.html](https://duktape.org/api.html)
* 源碼 [https://github.com/svaarala/duktape](https://github.com/svaarala/duktape)

# 環境配置
1. 從官網下載 最新源碼
2. 將 src 下的 duk_config.h duktape.h duktape.c 加入項目

```c++
#include "duktape.h"

int main()
{
    // 初始化 一個 js 運行環境
    duk_context *ctx = duk_create_heap_default();

    // 安全模式下 加載並執行 一段 js
    duk_int_t rs = duk_peval_string(ctx, "1+2");

    // 驗證 是否執行 成功
    if(0 == rs)
    {
        // 打印成功 信息
        printf("1+2=%d\n", (int) duk_get_int(ctx, -1));
    }
    else
    {
        // 打印 錯誤信息
        puts(duk_safe_to_string(ctx, -1));
    }
    // 將 返回值 出棧
    duk_pop(ctx);


    // 釋放 環境
    duk_destroy_heap(ctx);
    return 0;
}
```

# 編譯動態庫

對於 win32 在 duk_config.h 中 添加 導出聲明 即可

```c++
#define DUK_EXTERNAL_DECL  _declspec(dllexport)
```

