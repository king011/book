# [justify-content](https://www.primefaces.org/primeflex/justifycontent)

justify-content 用於定義 flex 內部的元素如何在主軸上對齊

| Class | Properties |
| -------- | -------- |
| justify-content-start     | justify-content: flex-start;     |
| justify-content-end     | justify-content: flex-end;     |
| justify-content-center     | justify-content: center;     |
| justify-content-between     | justify-content: space-between;     |
| justify-content-around     | justify-content: space-around;     |
| justify-content-evenly     | justify-content: space-evenly;     |

## justify-content-start

justify-content-start 讓 flex 的子項位於容器開頭

![](assets/start.png)

```
<div class="card">
    <div class="flex justify-content-start flex-wrap card-container blue-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-blue-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

## justify-content-end 
justify-content-end  讓 flex 的子項位於容器結尾

![](assets/end.png)

```
<div class="card">
    <div class="flex justify-content-end flex-wrap card-container green-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-green-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-green-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-green-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

## justify-content-center

justify-content-center 讓 flex 的子項位於容器中心

![](assets/center.png)

```
<div class="card">
    <div class="flex justify-content-center flex-wrap card-container yellow-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-yellow-500 font-bold text-gray-900 border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-yellow-500 font-bold text-gray-900 border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-yellow-500 font-bold text-gray-900 border-round m-2">3</div>
    </div>
</div>
```

## justify-content-between

justify-content-between 使 flex 子項之間保持間隔均勻的分佈

![](assets/between.png)

```
<div class="card">
    <div class="flex justify-content-between flex-wrap card-container purple-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-purple-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-purple-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-purple-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

## justify-content-around

justify-content-around 類似 justify-content-between 但同時使子項和 flex 盒也保持間距

flex 子項與 flex 盒子的間距是 flex 子項與子項間 間距的 1/2

![](assets/around.png)

```
<div class="card">
    <div class="flex justify-content-around flex-wrap card-container indigo-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-indigo-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-indigo-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-indigo-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

## justify-content-evenly

justify-content-evenly 類似 justify-content-around，但 flex 子項 與 子項/盒子 的間距 相等

![](assets/evenly.png)

```
<div class="card">
    <div class="flex justify-content-evenly flex-wrap card-container orange-container">
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-orange-500 font-bold text-white border-round m-2">1</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-orange-500 font-bold text-white border-round m-2">2</div>
        <div class="flex align-items-center justify-content-center w-4rem h-4rem bg-orange-500 font-bold text-white border-round m-2">3</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同