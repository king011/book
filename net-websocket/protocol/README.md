[rfc6455](https://www.rfc-editor.org/rfc/rfc6455.html)

# 握手

websocket 爲了能夠兼容原有的 http 設備，需要先向服務器發送一個 http GET 請求，如果服務器支持會返回 101，對於老舊設備會返回其它內容，這樣客戶端就可以對不支持的老舊服務器做出錯誤處理。


首先客戶端向服務器發送一個 GET 的升級請求
```text
GET /example/ws HTTP/1.1
Host: localhost:9443
User-Agent: ws-example-client/1.1
Origin: https://localhost:9443
Connection: Upgrade
Upgrade: websocket
Sec-WebSocket-Version: 13
Sec-WebSocket-Key: 3NVXWnSDPE8n3EIOtnGTag==

```

* **HTTP/1.1** 指定http 協議至少要 1.1 及以上，但實際上目前 http2 http3 都支持流和重用的傳輸通道，故基本上很少有服務器支持 websocket，並且 websocket 基本上和單獨的 tcp 連接對標，用重用的傳輸通道實現 websocket 不一定合適反正現在沒看到有服務器客戶端這麼做。所以這裏沒意外的化都會一直是 1.1
* **Host** 不是必須的但通常應該設置上，因爲在同一端口重用多個站點是很平常的事情，服務器路由需要 host 來區分重用的站點
* **User-Agent** 不是必須的，但個人推薦設置上，這樣方便服務器統計客戶都是通過什麼訪問的請求
* **Origin** 不是必須的但通常最好設置上，因爲 websocket 服務器爲了安全通常不允許跨站訪問，它需要這個字段判斷客戶端請求是否是跨站請求
* **Connection** 這裏必須是 **Upgrade**  表示要升級 http 協議
* **Upgrade** 這裏必須是 **websocket**  表示要升級到 websocket 協議
* **Sec-WebSocket-Version** 這裏指定 websocket 協議版本，目前版本是 13
* **Sec-WebSocket-Key** 是一個隨機16bytes數據被 base64 化後的 24 bytes 字符串，稍後服務器使用計算 hash 後通過 Sec-WebSocket-Accept 返回。這是爲了防止緩存代理重發送以前的 websocket 對話
* 最後注意 http1.1 協議最後是以多一個 \r\n 代表消息結束的(上面例子中 Sec-WebSocket-Key 這行末尾應該有兩個 \r\n)

如果成功服務器會響應 101

```
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: CrZIXlZyku/584dy2eVOzRU9TSA=
```

* **101** 是協議升級成功的響應代碼
* **Upgrade** 固定爲 websocket 表示升級到 websocket 協議
* **Connection** 固定爲 Upgrade 表示協議升級
* **Sec-WebSocket-Accept** 由請求 Sec-WebSocket-Key 計算的值，計算方式爲 `base64(sha1(Sec-WebSocket-Key + '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'))`

# 數據幀

一旦完成握手，就可以在 websocket 上進行雙向的[數據幀](https://www.rfc-editor.org/rfc/rfc6455.html#section-5.2)傳輸，幀是 websocket 傳輸的最小單位它解決了 tcp 流沒有邊界引起的粘包問題，一個完整的消息是由一個或多個幀組成的

```
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-------+-+-------------+-------------------------------+
|F|R|R|R| opcode|M| Payload len |    Extended payload length    |
|I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
|N|V|V|V|       |S|             |   (if payload len==126/127)   |
| |1|2|3|       |K|             |                               |
+-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
|     Extended payload length continued, if payload len == 127  |
+ - - - - - - - - - - - - - - - +-------------------------------+
|                               |Masking-key, if MASK set to 1  |
+-------------------------------+-------------------------------+
| Masking-key (continued)       |          Payload Data         |
+-------------------------------- - - - - - - - - - - - - - - - +
:                     Payload Data continued ...                :
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|                     Payload Data continued ...                |
+---------------------------------------------------------------+
```

* **FIN** 1bit，如果是 1 表示這是消息的最後一幀，否則表示消息還有後續幀
* **RSV1 RSV2 RSV3** 各佔 1bit，如果沒有定義 websocket 擴展必須爲0，否則由擴展進行定義其含義
* **opcode** 4bit，定義了如何解析後續的 Payload Data
	* **0x0** 表示一個延續幀
	* **0x1** 表示一個文本幀
	* **0x2** 表示二進制幀
	* **0x3~0x7** 爲進一步的非控制幀保留
	* **0x8** 表示斷開連接
	* **0x9** 表示 ping
	* **0xA** 表示 pong
	* **0xB~0xF** 保留用於進一步的控制幀
* **Mask** 1bit 如果爲 1 表示是否要對 Payload Data 進行掩碼操作，客戶端發送的數據必須設置爲1，服務器發送的數據必須設置爲0
* **Payload len** Payload Data 的長度，它本身佔用 7bit or 7+16bit or 7+64bit。假設 Payload len 的值是 x 那麼:
	1. **x < 126** ，佔用 7bit，長度就是 x
	2. **x = 126**，佔用 7bit+16bit，後續 16 bit 以網路序記錄了長度
	2.  **x = 127**，佔用 7bit+64bit，後續 64 bit 以網路序記錄了長度(最高 bit 必須爲 0)
* **Masking-key** 當 Mask 爲 1 時才存在佔用32 bit 隨機的掩碼值，否則佔用 0 bit，存在時需要需要對它進行掩碼操作
* **Payload Data** 數據載荷。包含了應用數據和擴展數據，擴展數據要存在的化必須在握手階段協商好且必須能夠計算出長度

# [客戶端到服務器掩碼算法](https://www.rfc-editor.org/rfc/rfc6455.html#section-5.3)

* **original-octet-i** 表示原始數據第 i 字節
* **transformed-octet-i** 表示轉換後的數據第 i 字節
* **j** 是 i 對 4 取餘的結果
* **masking-key-octet-j** 爲 Masking-key 的第 j 字節

```
j = i MOD 4
transformed-octet-i = original-octet-i XOR masking-key-octet-j
```

