# android

要 編譯 android 的 apk 需要 安裝 設置一些 環境

1. 安裝 依賴的一些庫

	```
	#info=false
	sudo apt-get install lib32z1 lib32ncurses5 lib32bz2-1.0 libstdc++6:i386
	```

	如果 提示 Unable to locate package lib32bz2-1.0 則 可以 安裝 下面的 套件

	```
	#info=false
	sudo apt-get install lib32z1 lib32ncurses5 libbz2-1.0:i386 libstdc++6:i386
	```
		
1. 安裝 g++

	```
	#info=false
	sudo apt-get install g++
	```
	
1. 安裝 java jdk8 並設置環境變量

	```
	sudo apt-get install openjdk-8-jdk
	sudo update-alternatives --config java
	export JAVA_HOME=$(update-alternatives --query javac | sed -n -e 's/Best: *\(.*\)\/bin\/javac/\1/p')
	```
	
1. 安裝 android sdk 並設置環境變量

	```
	export ANDROID_HOME="/usr/lib/android/sdk"
	export PATH="${PATH}:${ANDROID_HOME}tools/:${ANDROID_HOME}platform-tools/"
	```
	
1. 安裝 android 模擬器 AVD

	```
	#info=false
	sudo $ANDROID_HOME/tools/bin/sdkmanager "tools" "emulator" "platform-tools" "platforms;android-28" "build-tools;28.0.3" "extras;android;m2repository" "extras;google;m2repository"
	```

1. 檢查 環境是否 正確

	```
	#info=false
	tns doctor
	```
	
	# 編譯 apk
	[tns build android](https://docs.nativescript.org/tooling/docs-cli/project/testing/build-android#tns-build-android)