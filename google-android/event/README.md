# 按鍵處理

處理 按鍵處理 需要重載 Activity 的方法 onKeyDown onKeyUp onKeyLongPress

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_BACK:
                // 屏蔽 back
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
```

| keyCode | 註釋 |
| -------- | -------- |
| KEYCODE\_POWER     | 電源     |
| KEYCODE\_BACK     | 返回     |
| KEYCODE\_MENU     | 菜單 顯示當前app可用菜單     |
| KEYCODE\_HOME     | HOME     |
| KEYCODE\_SEARCH     | 搜索     |
| KEYCODE\_CAMERA     | 相機     |
| KEYCODE\_VOLUME\_UP     | +音量     |
| KEYCODE\_VOLUME\_DOWN     | -音量     |
| KEYCODE\_DPAD\_CENTER     | 方向鍵 用於移動 光標      |
| KEYCODE\_DPAD\_LEFT     | ...     |
| KEYCODE\_DPAD\_RIGHT     | ...     |
| KEYCODE\_DPAD\_UP     | ...     |
| KEYCODE\_DPAD\_DOWN     | ...     |
| KEYCODE\_0 ... KEYCODE\_9     | [0-9]     |
| KEYCODE\_A ... KEYCODE\_Z     | [a-z]     |


# view 事件

不同的 view 提供了 多種 setOnXXXListener 方法 來 註冊 事件 除了

| Listener | 註釋 |
| -------- | -------- |
| setOnClickListener     | 單擊     |
| setOnLongClickListener     | 長事件 單擊     |
| setOnTouchListener     | 觸摸     |

# 手勢
