# 內置型別

* numbers
* strings
* booleans
* lists (also known as arrays)
* sets
* maps
* runes (for expressing Unicode characters in a string)
* symbols

# numbers 數字

**int** 表示整數 取值 在 Dart Vm 中 爲  \[\\(-2 ^ {63}\\),\\(2 ^ {63}\\)-1] 如果編譯爲 javascript 則爲  \[\\(-2 ^ {53}\\),\\(2 ^ {53}\\)-1]

**double** 是 64bit的 浮點 符合 IEEE 754 standard

```
var x = 1;
var hex = 0xDEADBEEF;

var y = 1.1;
var exponents = 1.42e5;

// dart 2.1 之前 必須寫成 z = 1.0
double z = 1; // Equivalent to double z = 1.0.
```

```
#info="和 字符串 轉換"
// String -> int
var one = int.parse('1');
assert(one == 1);

// String -> double
var onePointOne = double.parse('1.1');
assert(onePointOne == 1.1);

// int -> String
String oneAsString = 1.toString();
assert(oneAsString == '1');

// double -> String
String piAsString = 3.14159.toStringAsFixed(2);
assert(piAsString == '3.14');
```

```
#info="bit 操作"
assert((3 << 1) == 6); // 0011 << 1 == 0110
assert((3 >> 1) == 1); // 0011 >> 1 == 0001
assert((3 | 4) == 7); // 0011 | 0100 == 0111
```

# strings 字符串

dart 字符串 是 UTF-16 code units

使用 **' ''' " """** 定義 字符串

```dart
void main(final List<String> args) {
  var s1 = 'king';
  var s2 = '''這種方式

支持 換行
''';
  var s3 = "ok";
  // 測試 dart 2.1 linux amd64 前兩行 換行會被忽略
  // 所以 加了個 \n 換行
  var s4 = """
\ncerberus is an idea
""";

  print(s1 + s2 + s3 + s4);
}
```

字符串中使用 $\{expression\} 可以 動態字符串中的 內容 如果 是標識符 可以忽略 \{\} 類似 bash 最終會調用 object's toString 替換字符串

```dart
void main(final List<String> args) {
  var name = "king";
  var lv = 10;
  print('''
name = $name
lv = ${lv + 1}
''');
}
```

使用 r 定義 raw 字符串
```dart
void main(final List<String> args) {
  var s = r'在定義前間 r 代表定義 raw 字符串 內容不會被轉義 \n $name';
  print(s);
}
```

字符串 常量
```dart
void main(final List<String> args) {
  var name = 'king';
  var s0 = 'name=$name\n這不是 常量 因爲 \$name 不是 常量';

  const lv = 10;
  const s1 = 'lv=$lv\n這是 常量 因爲 \$lv 是常量 所以即時插入了 s1 也不影響 編譯時 創建常量';

  print('$s0\n$s1');
}
```
# booleans 布爾

布爾的型別名爲 bool 取值是 true/flase 的常量

dart 類似 go 必須 明確的 使用 bool 作爲 判別式

```dart
// Check for an empty string.
var fullName = '';
assert(fullName.isEmpty);

// Check for zero.
var hitPoints = 0;
assert(hitPoints <= 0);

// Check for null.
var unicorn;
assert(unicorn == null);

// Check for NaN.
var iMeantToDoThis = 0 / 0;
assert(iMeantToDoThis.isNaN);
```

# Lists 數組

```
void main(final List<String> args) {
  var list = [11, 12, 13];
  List<int> list1 = list;
  for (var v in list1) {
    print(v);
  }

  for (var i = 0; i < list.length; i++) {
    print("list[$i] = ${list[i]}");
  }
}
```

# Sets 無序集合

```
void main(final List<String> args) {
  var set = {'king', 'anita', 'anna'};
  Set<String> set1 = set;
  for (var v in set1) {
    print(v);
  }
}
```

# Maps key/val 無序集合

```
void main(final List<String> args) {
  var m = Map();
  m["king"] = 1;
  m["anna"] = 2;
  m[3] = "ok";
  Map<dynamic, dynamic> m0 = m;
  for (var k in m0.keys) {
    print("m0[$k]=${m0[k]}");
  }

  var m1 = {
    "id": "xxx-xxx",
    "name": "king",
  };
  for (var k in m1.keys) {
    print("m1[$k]=${m1[k]}");
  }
}
```

# Runes 

dart 使用 UTF-16 編碼字符串 Runges 是 UTF-32 編碼

類似 js 使用 \u{xxx} 標識 UTF-32

使用 字符串的 runes 返回 其 Runes

```dart
void main(final List<String> args) {
  var clapping = '\u{1f44f}';
  print(clapping);
  print(clapping.codeUnits);
  print(clapping.runes.toList());

  Runes input = new Runes(
      '\u2665  \u{1f605}  \u{1f60e}  \u{1f47b}  \u{1f596}  \u{1f44d}');
  print(new String.fromCharCodes(input));
}
```

# Symbols 符號

使用 # 獲取一個 標識符的 符號

```dart
main(final List<String> args) {
  var str = "cerberus is an idea";
  print(#str);
}
```