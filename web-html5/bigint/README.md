# bigint

新的類型 bigint 用來表示一個超級大的數字，使用函數 BigInt 創建

```
let x = BigInt(123)
console.log(typeof x === "bigint")

try {
    BigInt("abc")
} catch (e) {
    console.log(e)
}
```

# 創建 bigint
有多種創建 bigint 的方式

```
const previouslyMaxSafeInteger = 9007199254740991n

const alsoHuge = BigInt(9007199254740991)
// 9007199254740991n

const hugeString = BigInt("9007199254740991")
// 9007199254740991n

const hugeHex = BigInt("0x1fffffffffffff")
// 9007199254740991n

const hugeOctal = BigInt("0o377777777777777777")
// 9007199254740991n

const hugeBin = BigInt("0b11111111111111111111111111111111111111111111111111111")
// 9007199254740991n
```

數字加上後綴n 表示 bigint 常量，但這種寫法要求 js 的目標版本在 ES2020 或以上

# 比較

bigint 可以正確的處理比較符號，可以安全的在 Set Map 中使用

```
const k0: any = BigInt(1)
const k1 = BigInt("1")
console.log(k0 === k1) // true
console.log(k0 == 1)  // true
console.log(1 == k0)  // true
console.log(k0 === 1)  // false

console.log('-------- set ------')
const m = new Set<any>
m.add(k0)
console.log(m.has(1)) // false
console.log(m.has(k1)) // true
```

```
0n === 0
// false

0n == 0
// true
```

```
1n < 2
// true

2n > 1
// true

2 > 2
// false

2n > 2
// false

2n >= 2
// true
```

```
const mixed = [4n, 6, -12n, 10, 4, 0, 0n]
// [4n, 6, -12n, 10, 4, 0, 0n]

mixed.sort() // default sorting behavior
// [ -12n, 0, 0n, 10, 4n, 4, 6 ]

mixed.sort((a, b) => a - b)
// won't work since subtraction will not work with mixed types
// TypeError: can't convert BigInt value to Number value

// sort with an appropriate numeric comparator
mixed.sort((a, b) => (a < b) ? -1 : ((a > b) ? 1 : 0))
// [ -12n, 0, 0n, 4n, 4, 6, 10 ]
```
# 算術

bigint 也支持 **\+&nbsp;\*&nbsp;\-&nbsp;%&nbsp;** 等算術運算符

```
const previousMaxSafe = BigInt(Number.MAX_SAFE_INTEGER)
// 9007199254740991n

const maxPlusOne = previousMaxSafe + 1n
// 9007199254740992n

const theFuture = previousMaxSafe + 2n
// 9007199254740993n, this works now!

const multi = previousMaxSafe * 2n
// 18014398509481982n

const subtr = multi - 10n
// 18014398509481972n

const mod = multi % 10n
// 2n

const bigN = 2n ** 54n
// 18014398509481984n

bigN * -1n
// -18014398509481984n
```

bigint 也支持 >> << 的位操作符，但不支持 >>> 的無符號位移

# bool

只有 0n 是 false 其它 bigint 都是一個 true 值