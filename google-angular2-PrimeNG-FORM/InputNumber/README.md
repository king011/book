# [InputNumber](https://primeng.org/inputnumber)

```
import {InputNumberModule} from 'primeng/inputnumber';
```

InputNumber 是提供數值輸入的輸入組件

![](assets/primeng.org_inputnumber.png)


```
<p-inputNumber [(ngModel)]="val"></p-inputNumber>
```
## Decimal Mode

mode 指定數值類型

* **decimal** 這是默認值，代表十進制數字
* **currency** 這表示貨幣

```
<p-inputNumber [(ngModel)]="val" mode="decimal"></p-inputNumber>
```

可以使用 **minFractionDigits/maxFractionDigits** 指定數值的最大值

```
<p-inputNumber [(ngModel)]="val" mode="decimal" [minFractionDigits]="2"></p-inputNumber>
<p-inputNumber [(ngModel)]="val" mode="decimal" [minFractionDigits]="2" [maxFractionDigits]="2"></p-inputNumber>
```

使用 **locale** 屬性可以設置數值的本地化信息，例如分組和小數點符號，默認值爲瀏覽器的區域設置。語言環境是根據 [BCP](https://www.rfc-editor.org/rfc/rfc5646) 語言標籤定義的

```
User Locale
<p-inputNumber [(ngModel)]="value1" mode="decimal" [minFractionDigits]="2"></p-inputNumber>

United State Locale
<p-inputNumber [(ngModel)]="value2" mode="decimal" locale="en-US" [minFractionDigits]="2"></p-inputNumber>

German Locale
<p-inputNumber [(ngModel)]="value3" mode="decimal" locale="de-DE" [minFractionDigits]="2"></p-inputNumber>

Indian Locale
<p-inputNumber [(ngModel)]="value4" mode="decimal" locale="en-IN" [minFractionDigits]="2"></p-inputNumber>
```

## Currency

* 將 **mode** 設置爲 **currency** 將數值模式設置爲貨幣
* 設置 **currency** 屬性指定貨幣格式例如 USD 表示美元，TWD 表示 臺幣
* **currencyDisplay** 設定貨幣符號如何顯示(默認是 symbol)


```
User Locale
<p-inputNumber [(ngModel)]="value1" mode="currency"></p-inputNumber>

United State Locale
<p-inputNumber [(ngModel)]="value2" mode="currency" currency="USD" locale="en-US"></p-inputNumber>

German Locale
<p-inputNumber [(ngModel)]="value3" mode="currency" currency="EUR" locale="de-DE"></p-inputNumber>

Indian Locale
<p-inputNumber [(ngModel)]="value4" mode="currency" currency="INR" locale="en-IN"></p-inputNumber>
```

> currency 默認使用瀏覽器設定，但瀏覽器可能沒有此值，故最好明確設定

## Prefix and Suffix

可以爲輸入框自定義 前綴 和 後綴

```
Mile
<p-inputNumber [(ngModel)]="value1" suffix=" mi"></p-inputNumber>

Percent
<p-inputNumber [(ngModel)]="value2" prefix="%"></p-inputNumber>

Expiry
<p-inputNumber [(ngModel)]="value3" prefix="Expires in " suffix=" days"></p-inputNumber>

Temperature
<p-inputNumber [(ngModel)]="value4" prefix="↑ " suffix="℃" :min="0" :max="40"></p-inputNumber>
```

## Buttons

使用 **showButtons** 選項啓用微調按鈕，並使用 **buttonLayout** 定義佈局:
* **stacked** 這時默認值
* **horizontal**
* **vertical**

請注意即時沒有按鈕，鍵盤向上向下箭頭鍵也可用於微調數值

```
Stacked
<p-inputNumber [(ngModel)]="value1" [showButtons]="true" mode="currency" currency="USD"></p-inputNumber>

Horizontal
<p-inputNumber [(ngModel)]="value2" [showButtons]="true" buttonLayout="horizontal" spinnerMode="horizontal"
    decrementButtonClass="p-button-danger" incrementButtonClass="p-button-success" incrementButtonIcon="pi pi-plus" decrementButtonIcon="pi pi-minus" mode="currency" currency="EUR"></p-inputNumber>

Vertical
<p-inputNumber [(ngModel)]="value3" mode="decimal" [showButtons]="true" buttonLayout="vertical" spinnerMode="vertical"
    decrementButtonClass="p-button-secondary" incrementButtonClass="p-button-secondary" incrementButtonIcon="pi pi-plus" decrementButtonIcon="pi pi-minus"></p-inputNumber>
```

### Step

使用 **step** 屬性定義微調的步進因子

```
<p-inputNumber [(ngModel)]="val" [step]="0.25"></p-inputNumber>
```

##  Min and Max Boundaries

使用 **min/max** 屬性來限定輸入值的範圍

```
<p-inputNumber [(ngModel)]="val" [min]="0" [max]="100"></p-inputNumber>
```
# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onFocus     | event: Browser event     | 獲取到焦點時回調     |
| onBlur     | event: Browser event     | 失去焦點時回調     |
| onInput     | event.originalEvent: Browser event<br>event.value: New value     | 輸入值時回調     |
| onClear     | -     | 清除輸入字段時回調     |
# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| value     | number     | null     | 組件值     |
| format     | boolean     | true     | 是否格式化值     |
| showButtons     | boolean     | false     | 顯示微調按鈕     |
| buttonLayout     | string     | stacked     | 微調按鈕佈局: stacked horizontal vertical     |
| incrementButtonClass     | string     | null     | 增量按鈕的 css class     |
| decrementButtonClass     | string     | null     | 減量按鈕的 css class     |
| incrementButtonIcon     | string     | pi pi-chevron-up     | 增量圖標     |
| decrementButtonIcon     | string     | pi pi-chevron-down     | 減量圖標     |
| locale     | string     | null     | 用於格式化的語言環境     |
| localeMatcher     | string     | best fit     | 要使用的語言環境匹配[算法](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl#Locale_negotiation)     |
| mode     | string     | decimal     | 數值模式: decimal currency     |
| prefix     | string     | null     | 顯示前綴     |
| suffix     | string     | null     | 顯示後綴     |
| currency     | string     | null     | 使用的貨幣例如: USD EUR CNY，如果瀏覽器沒有提供則必須顯示設定     |
| currencyDisplay     | string     | symbol     | 如何顯示貨幣符號: symbol code     |
| useGrouping     | boolean     | true     | 是否使用分組分隔符，例如千位分隔符或 千/萬/千萬 分隔符     |
| minFractionDigits     | number     | null     | 要使用的最小小數位數。有效值是從 0 到 20；普通數字和百分比格式的默認值是 0；貨幣格式的默認值是 [ISO 4217](https://www.currency-iso.org/en/home/tables/table-a1.html) 貨幣代碼列表提供的次要單位位數(如果列表爲提供，則爲 2)     |
| maxFractionDigits     | number     | null     | 要使用的最大大數位數。 有效值是從 0 到 20；普通數字的默認值是 minimumFractionDigits 和 3 中的較大值；貨幣格式的默認值是 [ISO 4217](https://www.currency-iso.org/en/home/tables/table-a1.html) 貨幣代碼列表提供的次要單位位數和minimumFractionDigits的較大值(如果列表爲提供，則爲 2)     |
| min     | number     | null     | 最小邊界值     |
| max     | number     | null     | 最大邊界值     |
| step     | number     | 1     | 步進增量     |
| allowEmpty     | boolean     | true     | 是否允許輸入爲空     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| inputId     | string     | null     | 焦點輸入的標識符，以匹配爲組件定義的標籤     |
| inputStyle     | string     | null     | 輸入字段的內聯 css style     |
| inputStyleClass     | string     | null     | 輸入字段的 css class     |
| placeholder     | string     | null     | 輸入時顯示的資訊信息     |
| size     | number     | null     | 輸入字段大小     |
| maxlength     | number     | null     | 輸入字段允許的最大字符數     |
| tabindex     | number     | null     | 指定元素的 Tab 鍵順序     |
| disabled     | boolean     | false     | 是否禁用組件     |
| readonly     | boolean     | false     | 組件是否只讀     |
| title     | string     | null     | 輸入文本的標題文本     |
| ariaLabel     | string     | null     | 輸入框的 aria label     |

| ariaRequired     | boolean     | false     | 使用 aria 指定表單元素必須被輸入     |
| name     | string     | null     | 輸入字段的名稱     |
| autocomplete     | string     | null     | 定義一個字符串，該字符串自動完成當前元素     |
| showClear     | boolean     | false     | 啓用後，會顯示一個清除圖標以清除該值     |

# Methods

| Name | Parameters | Description |
| -------- | -------- | -------- |
| getFormatter     | -     | 返回 Intl.NumberFormat 對象     |

# Styling

| Name | Element |
| -------- | -------- |
| p-inputnumber     | Container element     |
| p-inputnumber-stacked     | Container element with stacked buttons.     |
| p-inputnumber-horizontal     | Container element with horizontal buttons.     |
| p-inputnumber-vertical     | Container element with vertical buttons.     |
| p-inputnumber-input     | Input element     |
| p-inputnumber-button     | Input element     |
| p-inputnumber-button-up     | Increment button     |
| p-inputnumber-button-down     | Decrement button     |
| p-inputnumber-button-icon     | Button icon     |

