# [應用外部可見](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/)

k8s 的 Pod 是轉瞬即逝的。當一個工作 Node 掛掉後後，在 Node 上運行的 Pod 也會消亡。ReplicaSet 會自動地通過創建新的 Pod 驅動集羣回到目標狀態，以保證應用程序正常運行。

ks8 中的 服務(Service)是一種抽象概念，它定義了 Pod 的邏輯集和訪問 Pod 的協議。Service 使用 Pod 之間的松耦合成爲可能。Service 使用 YAML 或 JSON 來定義。Service 下的一組 Pod 通常由 LabelSelector 來標記。

儘管每個 Pod 都有一個唯一的 ip地址，但如果沒有 Service，這些 IP 不會暴露在集羣外部。Service 允許應用接收流量。Service 也可以用在 ServiceSpec 標記 type 的方式暴露

* ClusterIP -> 默認值。在集羣內部 ip 上公開 Service。這種類型使得 Service 只能從集羣內部訪問。
* NodePort -> 使用 NAT 在集羣中每個選定 Node 的相同端口上公佈 Service。使用 &lt;NodeIP&gt;:&lt;NodePort&gt; 從集羣外部訪問 Service。是 ClusterIP的超集。
* LoadBalancer -> 在當前雲中創建一個外部負載均衡器(如果支持的化)，並爲 Service 分配一個固定的外部 ip。是 NodePort 的超集。
* ExternalName -> 通過返回帶有該名稱的 CNAME 記錄，使用任意名稱(由 spec 中的 externalName 指定) 公開 Service。不使用代理。這種類型需要 kube-dns 的 1.7 或更高版本。

# Service 和 Label
![](assets/module_04_services.svg)


service 通過一組 Pod 路由通信。Service是一種抽象，它允許 Pod 死亡 並在 k8s 中複製，而不會影響應用程式。在依賴的 Pod 之間(如應用程序中的前端和後端組件)進行發現和路由是 k8s Service 處理的。

Service 匹配一組 Pod 是使用 標籤(Label) 和 選擇器(Selector)，它們是允許對 k8s 中調度對象進行邏輯操作的一種分組原語。Label 是附加在對象上的 key/value 對，可以以多種方式使用：
* 指定用於開發，測試和生成的對象
* 嵌入版本標籤
* 使用 Label 將對象進行分類

![](assets/module_04_labels.svg)

Label 可以在創建時或之後附加到對象上。它們可以隨時被修改。

# 使用 Service 發佈應用 並添加 Label

## 發佈服務
獲取 服務

```
kubectl get services
```

如果使用 minikube 則會看一個名爲 kubernetes 的服務是由 minikube 默認創建的

要創建一個服務並且將其公開給外部流量，需要使用帶有 NodePort 作爲參數的暴露命名 (minikube 尚不支持 LoadBalancer)
```
kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

使用 describe service 來獲取 服務詳情
```
kubectl describe services/kubernetes-bootcamp
```

創建一個名爲 NODE\_PORT 的環境變量，該變量具有分配的節點端口

```
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT
```

現在可以通過 節點 ip 和 外部暴露的端口來測試 應用
```
curl $(minikube ip):$NODE_PORT
```

## 使用標籤

部署會自動爲 Pod 創建一個標籤 可以使用 `describe deployment` 指令來查看

```
kubectl describe deployment
```

設置 -l 參數 可以使用標籤來查詢 Pod 列表

```
kubectl get pods -l run=kubernetes-bootcamp
```

也可以使用 標籤來查詢 Service

```
kubectl get services -l run=kubernetes-bootcamp
```

將 Pod 那麼 設置到 環境變量

```
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME
```

設置 新標籤

```
kubectl label pod $POD_NAME app=v1
```

查看新標籤
```
kubectl describe pods $POD_NAME
```

# 刪除服務

可以使用 標籤或者名稱 刪除服務

```
kubectl delete service kubernetes-bootcamp
```

```
kubectl delete service -l run=kubernetes-bootcamp
```

重新查詢服務已經不存在 kubernetes-bootcamp 服務
```
kubectl get services
```

此時使用之前暴露的 ip 端口 已經無法訪問

```
curl $(minikube ip):$NODE_PORT
```

但是此時程序依然在內部運行
```
kubectl exec -ti $POD_NAME -- curl localhost:8080
```