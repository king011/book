# 資源

* android 提供了 資源檔案的 支持
* 資源檔案是 位於 res 檔案夾下 xml 檔案
* 資源檔案 名稱 必須是 **^\[a\-z\]\[a\-z0\-9\_\]\+** 的形式


# 字符串 資源

* 字符串 資源 通常 位於 **res/values/strings.xml** 中 使用 tag **string** 指定
* 在xml中 使用 **@string/name** 使用
* 在 java中 使用 **R.string.name** 獲取資源id
* 在 java中 使用 **getResources().getString(id)** 獲取資源 字符串值

```
<resources>
    <string name="app_name">Example</string>
    <string name="title_activity_love">LoveActivity</string>

    <string name="name">name 屬性 需要唯一 這是使用此資源時的 訪問 索引</string>
    <string name="file">資源 檔案名 可以 任意, 主要使用 對應的tag進行即可? ,不過 一般資源都有一個 默認推薦的 檔案名</string>
</resources>
```

# i18n

android 爲所有 程式 都提供了 完善的 i18n支持

* 只需要 在創建一個 values-XXX 檔案夾 並在裏面 設置好 對應 語言的 翻譯 即可 android 會使用 系統語言環境 翻譯當前 app
* 系統切換語言時 會自動 爲app 切換 i18n 資源
* 所有的 string 都 必須被 翻譯

```text
#info="一些 語言 環境的 資源檔案夾名"

// 中文 中華民國
values-zh-rTW

// 中文 香港淪陷區
values-zh-rHK

// 中文 淪陷區
values-zh-rCN

// 英文 美國
values-en-rUS

// 英文 英國
values-en-rGB

// 日文
values-ja-rJP

// 德語 德國 
values-de-rDE

// 德語 瑞士
values-de-rCH
```

# 顏色 資源

* 顏色 資源 通常 位於 **res/values/colors.xml** 中 使用 tag **color** 指定
* 在xml中 使用 **@color/name** 使用
* 在 java中 使用 **R.color.name** 獲取資源id
* 在 java中 使用 **getResources().getColor(id,theme)** 獲取資源 AARRGGBB 值

> getColor(id,theme) theme 可以爲 null
> 

color 的值 允許 多種 方式進行 定義

* #RGB
* #ARGB
* #RRGGBB
* #AARRGGBB

> 對於 Alpha f/ff 表示完全不透明 0 表示完全透明

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="colorPrimary">#008577</color>
    <color name="colorPrimaryDark">#00574B</color>
    <color name="colorAccent">#D81B60</color>
</resources>
```

# 尺寸 資源

* 尺寸 資源 通常 位於 **res/values/dimens.xml** 中 使用 tag **dimen** 指定
* 在xml中 使用 **@dimen/name** 使用
* 在 java中 使用 **R.dimen.name** 獲取資源id
* 在 java中 使用 **getResources().getDimension/getDimensionPixelOffset/getDimensionPixelSize(id)** 返回尺寸

尺寸 支持 多種 單位

* px 
* in
* pt
* dip/dp 屏幕密度的抽象單位 在每英寸160點的顯示器上 1dp==1px
* sp
* mm

```
<resources>
    <dimen name="fab_margin">16dp</dimen>
    <dimen name="text_margin">16dp</dimen>
</resources>
```

# 佈局資源

* 尺寸 資源 通常 位於 **res/layout/xxx.xml** 中 
* 在xml中 使用 `<include layout="@layout/filename" />` 可以 包含一個 佈局
* 在 java中 使用 **R.layout.filename** 獲取資源id
* 在 java中 使用 **Context.setContentView(id)** 爲Context當前 佈局資源

> include 相當於 把 include 標籤 替換爲 目標佈局檔案中的 內容
> 
> 故 如果要多次 include 要注意 組件 id 不能 重複
> 

# 數組 資源
* 數組 資源 通常 位於 **res/values/arrays.xml** 中 
* 在xml中 使用 **@array/name** 使用
* 在 java中 使用 **R.array.name** 獲取資源id
* 在 java中 使用 **getResources().getIntArray/getStringArray/getTextArray(id)** 返回數組

數組 資源 允許 使用 三種不同 tag 定義 三種類型的 數組 子元素都用 item 定義

* array 平臺類似的數組
* integet-array 整型數組
* string-array 字符串數組

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string-array name="loves">
        <item>anita</item>
        <item>anna</item>
        <item>kate</item>
        <item>j0</item>
    </string-array>
</resources>
```

# 圖像 資源
* 圖像 資源 通常 位於 **res/drawable/** 中 直接將圖片 複製到此即可
* 在xml中 使用 **@drawable/filename** 使用

android 圖像 支持 png jpg gif9.png

# StateListDrawable 資源

* StateListDrawable 資源 可以在 不同狀態下 返回 不同的 資源 使用 selector 定義
* StateListDrawable 同樣位於 **res/drawable/** 中
* 在xml中 使用 **@drawable/filename** 使用

```
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
		 <item android:state_pressed="true"
								 android:drawable="@drawable/button_pressed" /> <!-- pressed -->
		 <item android:state_focused="true"
								 android:drawable="@drawable/button_focused" /> <!-- focused -->
		 <item android:drawable="@drawable/button_normal" /> <!-- default -->
</selector>
```

> 處理 使用 android:drawable 也可以使用 android:color 定義顏色
> 

| 狀態屬性 | 含義 |
| -------- | -------- |
| android:state\_active     | 是否處於激活狀態     |
| android:state\_checked     | 是否處於勾選狀態     |
| android:state\_enabled     | 是否處於可用狀態     |
| android:state\_first     | 是否處於開始狀態     |
| android:state\_focused     | 是否處於獲得焦點狀態     |
| android:state\_last     | 是否處於結束狀態     |
| android:state\_middle     | 是否處於中間狀態     |
| android:state\_pressed     | 是否處於被按下狀態     |
| android:state\_selected     | 是否處於被選擇狀態     |
| android:state\_window\_focused     | 窗口是否已經得到焦點     |


# 樣式 主題

樣式 和 主題 通常 定義在 

* 數組 資源 通常 位於 **res/values/styles.xml** 中 使用 tag **style** 指定
* 在xml中 使用 **@style/name** 使用

主題 基本上和 和樣式 沒有 區別 只是 主題 只能作用與 整個 app 或 Activity 而 樣式 可以作用與單個 view 上

要 使用 style 值需要 數組 view 的 android:style 屬性 即可

要 使用 主題 需要在 AndroidManifest.xml 中 爲 application 指定 android:theme 屬性 或者 爲 activity 指定 android:theme 屬性

在 java中可以 調用 setTheme(id) 來指定 主題

```
<resources>

    <!-- Base application theme. -->
    <style name="AppTheme" parent="Theme.AppCompat.Light.DarkActionBar">
        <!-- Customize your theme here. -->
        <item name="colorPrimary">@color/colorPrimary</item>
        <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
        <item name="colorAccent">@color/colorAccent</item>
    </style>

    <style name="AppTheme.NoActionBar">
        <item name="windowActionBar">false</item>
        <item name="windowNoTitle">true</item>
    </style>

    <style name="AppTheme.AppBarOverlay" parent="ThemeOverlay.AppCompat.Dark.ActionBar" />

    <style name="AppTheme.PopupOverlay" parent="ThemeOverlay.AppCompat.Light" />

</resources>
```

> style 支持 派生 只要 指定 parent 即可 子 style 會有 父style 的全部 定義
> 
> 如果 在 子 style 中 定義了 父style 中存在的 項目 將 覆蓋父style 設置
> 

# 元素 XML 資源

android 亦提供了對元素 xml 資源的 支持

# 菜單 資源

```
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@+id/open"
        android:title="open"/>
    <item android:id="@+id/love" android:title="love">
        <!--定義 子菜單-->
        <menu>
            <!--定義 菜單組-->
            <group android:id="@+id/group_love" android:checkableBehavior="all">
                <item android:id="@+id/anita"
                    android:title="anita"/>
                <item android:id="@+id/kate"
                    android:title="kate"/>
            </group>
        </menu>
    </item>
    <item android:id="@+id/exit"
        android:title="exit"
        android:onClick="onExit" />
</menu>
```


| item 屬性 | 含義 |
| -------- | -------- |
| android:id     | 菜單唯一標識     |
| android:title     | 菜單項設置標題     |
| android:alphabeticShortcut     | 字符快捷鍵     |
| android:numericShortcut     | 數字快捷鍵     |
| android:icon     | 圖標     |
| android:enabled     | 是否可用     |
| android:checkable     | 是否可選     |
| android:checked     | 選中狀態     |
| android:visible     | 是否可見     |

| group 屬性 | 含義 |
| -------- | -------- |
| android:id     | 菜單組唯一標識     |
| android:checkableBehavior     | 組內成員 選擇行爲 none(不可選) all(可多選) single(單選)     |
| android:menuCategory     | 對菜單進行分類 指定 優先級 container system secondary alternative     |
| android:enabled     | 是否可用     |
| android:visible     | 是否可見     |


## 選項菜單

選項菜單 是 Activity 的主菜單 實現 onCreateOptionsMenu 函數 即可

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;



public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    // 定義 菜單 單擊 方法 以供 菜單 綁定
    public void onExit(MenuItem item){
        finish();
    }

    // 重載 此方法 創建 菜單
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    // 重載 此方法 處理 菜單 選中
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // 修改 選中 狀態
        if(item.isEnabled() && item.isCheckable() && R.id.group_love == item.getGroupId()){
            item.setChecked(!item.isChecked());
        }

        // 判斷 選中 項目
        switch (item.getItemId()){
            case R.id.anita:
                Log.i(TAG,"selected anita");
                break;
            case R.id.kate:
                Log.i(TAG,"selected kate");
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
```

## 上下文菜單 是 當長按一個 view 時 彈出的 菜單

需要 調用 registerForContextMenu 爲 view 註冊 並且 重載 onCreateContextMenu 創建菜單

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private MenuInflater inflater = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.btn);
        registerForContextMenu(button);
    }

    // 定義 菜單 單擊 方法 以供 菜單 綁定
    public void onExit(MenuItem item){
        finish();
    }
    // 重載 此方法 創建 上下文菜單
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu,menu);
        // 設置 菜單 圖標
        //menu.setHeaderIcon()

        // 設置 菜單 文本
        menu.setHeaderTitle("測試");
        Log.i(TAG,"onCreateContextMenu");
    }

    // 重載 此方法 處理 菜單 選中
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // 修改 選中 狀態
        if(item.isEnabled() && item.isCheckable() && R.id.group_love == item.getGroupId()){
            item.setChecked(!item.isChecked());
        }
        // 判斷 選中 項目
        switch (item.getItemId()){
            case R.id.anita:
                Log.i(TAG,"selected anita");
                break;
            case R.id.kate:
                Log.i(TAG,"selected kate");
                break;
        }
        return super.onContextItemSelected(item);
    }
}
```

> 上述 選項菜單/上下文菜單 在 Activity 重建時 會重建菜單 此時會 失去 菜單選中 狀態
> 
> 對於 上下文菜單 每次顯示 都會 失去 菜單選中 狀態
> 
> 故 如要在項目中 使用菜單選中狀態 需要 在創建時 自行恢復狀態