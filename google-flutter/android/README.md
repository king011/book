# 發佈android程式

```
# 編譯 android apk 包 
flutter build apk
```

build 會將apk打包到 **build/app/outputs/apk/release/app-release.apk**

# AndroidManifest.xml
* **android/app/src/main/AndroidManifest.xml**  是android 應用程式清單 需要在裏面定義 程式需要使用的 功能
* **android/app/src/main/debug/AndroidManifest.xml** 是調試時使用的 清單

```
#info="AndroidManifest.xml"
<manifest>
    <!-- 要使用網路功能->
    <uses-permission android:name="android.permission.INTERNET"/>
</manifest>
```

# 查看構造配置
**android/app/build.gradle** 包含 構造配置 

defaultConfig 包含如下主要配置

* applicationId 指定唯一的 Application ID
* versionCode 指定 版本號
* versionName 指定版本名稱字符串
* minSdkVersion 最低 api 級別
* targetSdkVersion 運行 api 級別

# 添加啓動圖標

1. 創建圖標資源 放入到 /android/app/src/main/res 中
2. 編輯 AndroidManifest.xml 清單 在 application 項目指定 android:icon

```
#info="AndroidManifest.xml"
<manifest>
    <application android:icon="@mipmap/ic_launcher">
    </application>
</manifest>
```

# app 簽名
## 創建 keystore

```
# 創建 keystore 檔案 key.jks 到 home 目錄下
keytool -genkey -v -keystore ~/key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias key
``` 

## 創建 key.properties 引用 keystore
創建檔案 **android/app/key.properties** 添加如下內容

```
#info="key.properties"
storePassword=<password from previous step>
keyPassword=<password from previous step>
keyAlias=key
storeFile=<location of the key store file, e.g. /Users/<user name>/key.jks>
```

> 記得要將 此檔案設置到 .gitignore 中 以免 keystore 密碼泄漏 
> 

## 添加 keystore 設置
編輯 **android/app/build.gradle** 添加如下設置

```
#info="build.gradle"
def keystorePropertiesFile = rootProject.file("app/key.properties")
def keystoreProperties = new Properties()
keystoreProperties.load(new FileInputStream(keystorePropertiesFile))

android {
	signingConfigs {
			release {
					keyAlias keystoreProperties['keyAlias']
					keyPassword keystoreProperties['keyPassword']
					storeFile file(keystoreProperties['storeFile'])
					storePassword keystoreProperties['storePassword']
			}
	}
	buildTypes {
			release {
					signingConfig signingConfigs.release
			}
	}
}
```

# 開啓混淆

默認 flutter 不會開啓 android 混淆

創建 **android/app/proguard-rules.pro** 並添加如下規則

```
#Flutter Wrapper
-keep class io.flutter.app.** { *; }
-keep class io.flutter.plugin.**  { *; }
-keep class io.flutter.util.**  { *; }
-keep class io.flutter.view.**  { *; }
-keep class io.flutter.**  { *; }
-keep class io.flutter.plugins.**  { *; }
```

> 上述只添加了 flutter 引擎的混淆  對於其它庫需要在此 添加 額外 規則
> 

編輯 android/app/build.gradle 中的 buildTypes 塊  
在 release 中 將 minifyEnabled useProguard 配置爲 true 並指定 混淆檔案到上述的 **android/app/proguard-rules.pro**

```
#info="build.gradle"
android {

    ...

    buildTypes {

        release {

            signingConfig signingConfigs.release

            minifyEnabled true
            useProguard true

            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'

        }
    }
}
```
