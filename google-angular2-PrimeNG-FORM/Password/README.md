# [Password](https://primeng.org/password)

```
import {PasswordModule} from 'primeng/password';
```

Password 用於輸入密碼

可以使用 **pPassword** 指令或者 **p-password** 標籤來創建密碼組件

```
<input type="password" pPassword />
<p-password></p-password>
```
## Model Binding

支持標準的 ngModel

```
<p-password [(ngModel)]="value1"></p-password>
```

## Customization

密碼組件使用如下默認值來驗證密碼強度

### Medium

```
^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,}).
```

* 至少一個小寫字母
* 至少一個大寫字母或數字
* 至少6個字符

### Strong

```
^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})
```

* 至少一個小寫字母
* 至少一個大寫字母
* 至少一個數字
* 至少8個字符

# Templating
包含三個自定義的模板 **header content footer**

```
<p-password [(ngModel)]="value4">
    <ng-template pTemplate="header">
        <h6>Pick a password</h6>
    </ng-template>
    <ng-template pTemplate="footer">
        <p-divider></p-divider>
        <p class="mt-2">Suggestions</p>
        <ul class="pl-2 ml-2 mt-0" style="line-height: 1.5">
            <li>At least one lowercase</li>
            <li>At least one uppercase</li>
            <li>At least one numeric</li>
            <li>Minimum 8 characters</li>
        </ul>
    </ng-template>
</p-password>
```

# Properties

| Name | Type | Default | Default |
| -------- | -------- | -------- | -------- |
| promptLabel     | string     | null     | 輸入密碼的提示文本     |
| mediumRegex     | string     | 正則表達式用於匹配中等強度的密碼     | `^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,}).`     |
| strongRegex     | string     | 正則表達式用於匹配高強度的密碼     | `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})`     |
| weakLabel     | string     | null     | 弱密碼的文本提示     |
| mediumLabel     | string     | null     | 中等強度密碼的提示文本     |

| strongLabel     | string     | null     | 高強度密碼的提示文本     |
| feedback     | boolean     | true     | 是否顯示強度指標     |
| toggleMask     | boolean     | false     | 是否顯示圖標以將密碼顯示爲純文本     |
| appendTo     | string     | null     | 應該附加覆蓋的元素 id 或 "body"     |
| inputStyle     | string     | null     | input field 的內聯 css style     |

| inputStyleClass     | string     | null     | input field 的內聯 css class     |
| inputId     | string     | null     | input 元素的 id     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| panelStyle     | string     | null     | panel 的內聯 css style     |

| panelStyleClass     | string     | null     | panel 的 css class     |
| placeholder     | string     | null     | 輸入時顯示的諮詢信息     |
| label     | string     | null     | 輸入框的 label     |
| ariaLabel     | string     | null     | Defines a string that labels the input for accessibility.     |
| ariaLabelledBy     | string     | null     | Specifies one or more IDs in the DOM that labels the input field.     |

| showClear     | boolean     | false     | 如果爲true顯示一個圖標用於清空值     |
| maxLength     | number     | null     | 輸入的最大長度     |

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onBlur     | event: Blur event     | Callback to invoke on blur of input field.     |
| onFocus     | event: Focus event     | Callback to invoke on focus of input field.     |
| onClear     | -     | Callback to invoke when input field is cleared.     |

# Templates

| Name | Parameters |
| -------- | -------- |
| header     | -     |
| content     | -     |
| footer     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-password-panel     | Container of password panel     |
| p-password-meter     | Meter element of password strength     |
| p-password-info     | Text to display strength     |

