# example

```
package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"

	"github.com/mojocn/base64Captcha"
)

var store = base64Captcha.DefaultMemStore

func main() {
	addr := `:9000`
	l, e := net.Listen(`tcp`, addr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(`http work at`, addr)

	mux := http.NewServeMux()
	var srv Server
	mux.HandleFunc(`/verify`, srv.verify)
	mux.HandleFunc(`/get/digit`, srv.generateDigit)   // 生成數字驗證碼
	mux.HandleFunc(`/get/string`, srv.generateString) // 生成字符串驗證碼
	mux.HandleFunc(`/get/math`, srv.generateMath)     // 生成一個簡單的數學計算題目 +-*/

	e = http.Serve(l, mux)
	if e != nil {
		log.Fatalln(e)
	}

}

type Server struct {
}

func (s *Server) error(w http.ResponseWriter, statusCode int, msg string) {
	w.Header().Set(`Content-Type`, "text/plain; charset=utf-8")
	w.Write([]byte(msg))
	w.WriteHeader(statusCode)
}
func (s *Server) reader(w http.ResponseWriter, title, id, b64s string) {
	fmt.Fprintf(w, `<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>`+title+`</title>
    <style>
        .label {
            width: 36px;
            display: inline-block;
            text-align: end;
        }
    </style>
</head>
<body>
<form action="/verify" method="post">
	<p>
		<label for="id" class="label">id:</label>
		<input readonly type="string" name="id" value="`+id+`">
	</p>
	<img src="`+b64s+`" alt="captcha" />
	<p>
		<label for="val" class="label">val:</label>
		<input type="string" name="val">
		<button type="submit">submit</button>
	</p>
</form>
</body>
</html>`)
}
func (s *Server) formInt64(r *http.Request, key string, def int64) (val int64, e error) {
	value := r.FormValue(key)
	if value == `` {
		val = def
	} else {
		val, e = strconv.ParseInt(value, 10, 64)
	}
	return
}
func (s *Server) formFloat64(r *http.Request, key string, def float64) (val float64, e error) {
	value := r.FormValue(key)
	if value == `` {
		val = def
	} else {
		val, e = strconv.ParseFloat(value, 64)
	}
	return
}

func (s *Server) verify(w http.ResponseWriter, r *http.Request) {
	e := r.ParseForm()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}
	id := r.FormValue(`id`)
	val := r.FormValue(`val`)
	var ok string
	if store.Verify(id, val, true) {
		ok = `true`
	} else {
		ok = `false`
	}
	w.Write([]byte(`<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>verify</title>
    <style>
        .label {
            width: 36px;
            display: inline-block;
            text-align: end;
        }
    </style>
</head>
<body>
<form action="/verify" method="post">
	<p>
		<label for="id" class="label"> id:</label>
		<input readonly type="string" name="id" value="` + id + `">
	</p>
	<p>
		<label for="val" class="label">val:</label>
		<input readonly type="string" name="val" value="` + val + `">
	</p>
	<p>` + ok + `</p>
</form>
</body>
</html>`))
}
func (s *Server) generateDigit(w http.ResponseWriter, r *http.Request) {
	e := r.ParseForm()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}

	length, e := s.formInt64(r, `n`, 4)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if length < 1 {
		length = 4
	}

	width, e := s.formInt64(r, `w`, 150)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if width < 50 {
		width = 50
	}

	height, e := s.formInt64(r, `h`, 50)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if height < 40 {
		height = 40
	}
	skew, e := s.formFloat64(r, `skew`, 0.7)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}

	dot, e := s.formInt64(r, `dot`, 50)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if dot < 0 {
		dot = 0
	}

	var driver base64Captcha.Driver = &base64Captcha.DriverDigit{
		Length: int(length),
		Width:  int(width),
		Height: int(height),
		// 最大傾斜因子
		MaxSkew: skew,
		// 生成多少個圓點作爲干擾
		DotCount: int(dot),
	}
	c := base64Captcha.NewCaptcha(driver, store)
	id, b64s, e := c.Generate()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}
	s.reader(w, `digit`, id, b64s)
}
func (s *Server) generateString(w http.ResponseWriter, r *http.Request) {
	e := r.ParseForm()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}

	length, e := s.formInt64(r, `n`, 4)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if length < 1 {
		length = 4
	}

	width, e := s.formInt64(r, `w`, 150)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if width < 50 {
		width = 50
	}

	height, e := s.formInt64(r, `h`, 50)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if height < 40 {
		height = 40
	}

	noise, e := s.formInt64(r, `noise`, 50)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}
	// * 2 base64Captcha.OptionShowHollowLine
	// * 4 base64Captcha.OptionShowSlimeLine
	// * 8 base64Captcha.OptionShowSineLine,
	line, e := s.formInt64(r, `line`, 0)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}

	var driver base64Captcha.Driver = &base64Captcha.DriverString{
		Length: int(length),
		Width:  int(width),
		Height: int(height),
		// 隨機字符
		Source: `1234567890qwertyuiopasdfghjklzxcvbnm`,
		// 背景色，如果爲則生成隨機背景色
		BgColor: nil,
		// 在背景中生成多少噪音
		NoiseCount: int(noise),
		// 一些干擾線
		// ShowLineOptions: base64Captcha.OptionShowHollowLine | base64Captcha.OptionShowSlimeLine | base64Captcha.OptionShowSineLine,
		ShowLineOptions: int(line),
		// 字體，如果爲 nil 則使用全部默認字體
		Fonts: nil,
	}
	c := base64Captcha.NewCaptcha(driver, store)
	id, b64s, e := c.Generate()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}
	s.reader(w, `string`, id, b64s)
}
func (s *Server) generateMath(w http.ResponseWriter, r *http.Request) {
	e := r.ParseForm()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}

	width, e := s.formInt64(r, `w`, 150)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if width < 50 {
		width = 50
	}

	height, e := s.formInt64(r, `h`, 50)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	} else if height < 40 {
		height = 40
	}

	noise, e := s.formInt64(r, `noise`, 30)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}
	// * 2 base64Captcha.OptionShowHollowLine
	// * 4 base64Captcha.OptionShowSlimeLine
	// * 8 base64Captcha.OptionShowSineLine,
	line, e := s.formInt64(r, `line`, 0)
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}

	var driver base64Captcha.Driver = &base64Captcha.DriverMath{
		Width:  int(width),
		Height: int(height),

		// 背景色，如果爲則生成隨機背景色
		BgColor: nil,
		// 在背景中生成多少噪音
		NoiseCount: int(noise),
		// 一些干擾線
		// ShowLineOptions: base64Captcha.OptionShowHollowLine | base64Captcha.OptionShowSlimeLine | base64Captcha.OptionShowSineLine,
		ShowLineOptions: int(line),
		// 字體，如果爲 nil 則使用全部默認字體
		Fonts: nil,
	}
	c := base64Captcha.NewCaptcha(driver, store)
	id, b64s, e := c.Generate()
	if e != nil {
		s.error(w, http.StatusBadRequest, e.Error())
		return
	}
	s.reader(w, `math`, id, b64s)
}

```