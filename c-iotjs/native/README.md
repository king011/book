# native 模塊

1. 創建一個檔案夾用於存儲 模塊代碼

	```
	mkdir my-module && cd my-module
	```

2. 創建一個 modules.json 檔案 定義模塊信息

	```
	{
			"modules": {
					"mymodule": {
							"native_files": [
									"my_module.c"
							],
							"init": "InitMyNativeModule"
					}
			}
	}
	```
	
3. 創建模塊定義中指定的 c 源碼檔案並創建設定的 init 函數

	```
	#include "iotjs_def.h"

	JS_FUNCTION(native_add) {
		double num = 0;
		for (size_t i = 0; i < jargc; i++) {
			if (jerry_value_is_number(jargv[i])) {
				num += iotjs_jval_as_number(jargv[i]);
			} else {
				// 返回異常
				return JS_CREATE_ERROR(COMMON, "only support number");
			}
		}
		return jerry_create_number(num);
	}

	jerry_value_t InitMyNativeModule() {
		jerry_value_t mymodule = jerry_create_object();
		iotjs_jval_set_property_string_raw(mymodule, "message", "Hello world!");
		iotjs_jval_set_method(mymodule, "add", native_add);
		return mymodule;
	}
	```

4. 使用編譯 iotjs 並使用 --external-modules=my-module 指定 外部模塊路徑 以及要啓用的模塊

	```
	./tools/build.py \
		--buildtype=release \
		--external-modules=./my-module \
		--cmake-param=-DENABLE_MODULE_MYMODULE=ON
	```