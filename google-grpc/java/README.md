# java

要 由 \*.proto 生成 java代碼 需要 **protoc-gen-grpc-java** 插件

1. 下載 [https://search.maven.org/search?q=g:io.grpc](https://search.maven.org/search?q=g:io.grpc) 對應平臺插件 並且 改名爲 protoc-gen-grpc-java

1. 將 protoc-gen-grpc-java 放到 工作 目錄

1. 生成 grpc java 代碼

	```
	protoc --plugin=protoc-gen-grpc-java --proto_path=$root --grpc-java_out=$out $_SOURCES
	```

# gradle

gradle 需要 添加 以下配置

```txt
#info={"name":"java","noline":true}
compile 'io.grpc:grpc-netty-shaded:1.18.0'
compile 'io.grpc:grpc-protobuf:1.18.0'
compile 'io.grpc:grpc-stub:1.18.0'
```

```txt
#info={"name":"android","noline":true}
compile 'io.grpc:grpc-okhttp:1.18.0'
compile 'io.grpc:grpc-protobuf-lite:1.18.0'
compile 'io.grpc:grpc-stub:1.18.0'
```

```
#info="build.gradle"
// Apply the java plugin to add support for Java
apply plugin: 'java'

// Apply the application plugin to add support for building an application
apply plugin: 'application'

// In this section you declare where to find the dependencies of your project
repositories {
    // Use jcenter for resolving your dependencies.
    // You can declare any Maven/Ivy/file repository here.
    jcenter()
}

dependencies {
    // This dependency is found on compile classpath of this component and consumers.
    compile 'com.google.guava:guava:20.0'

    compile 'io.grpc:grpc-netty-shaded:1.18.0'
    compile 'io.grpc:grpc-protobuf:1.18.0'
    compile 'io.grpc:grpc-stub:1.18.0'

    // Use JUnit test framework
    testCompile 'junit:junit:4.12'
}

// Define the main class for the application
mainClassName = 'App'

jar {
    from {
        // 將 依賴的 第三方 jar 一起 打包
        configurations.runtime.collect{zipTree(it)}
    }
    manifest {
        // Main-Class 指定 main 函數 所在 class
        attributes 'Main-Class': 'App'
    }
}
```

# 客戶端代碼

```
#info="App.java"
import java.util.Scanner;

public class App {
    static final String HOST = "127.0.0.1";
    static final int Port = 1102;

    public static void main(String[] args) {
        Client client = new Client(HOST,Port);
        Scanner scanner = new Scanner(System.in);
        String cmd;
        while (true) {
            System.out.print("#>");
            cmd = scanner.next();
            if(cmd.equalsIgnoreCase("e")){
                break;
            }else if(cmd.equalsIgnoreCase("p")){
                client.requestPenguin();
            }else if(cmd.equalsIgnoreCase("d")){
                client.requestDog();
            }else if(cmd.equalsIgnoreCase("c")){
                client.requestCat();
            }else if(cmd.equalsIgnoreCase("f")){
                client.requestFish();
            }
        }
    }
}
```

```
#info="Client.java"
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.examples.zoo.*;
import io.grpc.stub.StreamObserver;

import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

public class Client {
    // 通道 管理者 管理到 服務器間的 tcp 連接
    protected ManagedChannel channel;

    // blockingStub 用於 同步調用 服務器
    // 不支持 stream request
    ServiceGrpc.ServiceBlockingStub blockingStub;
    // stub 用於 異步調用 服務器
    // stub 支持 所有調用 方式
    ServiceGrpc.ServiceStub stub;

    public Client(String host,int port) {
        // 創建 通道 管理者
        channel = ManagedChannelBuilder.forAddress(host,port)
                .usePlaintext()// 使用 h2c
                .build();

        // 創建 stub
        // java 不能像 go 一樣 在需要時 創建 client 而是 在最開始 創建 stub
        // 依據不同 調用 上下文 使用 withXXX 創建 新的 stub 類似 go 的 context.Context
        blockingStub = ServiceGrpc.newBlockingStub(channel);
        stub = ServiceGrpc.newStub(channel);
    }

    public void requestPenguin(){
        // 同步調用
        try {
            PenguinRequest request = PenguinRequest.newBuilder().build();
            blockingStub.penguin(request);
            System.out.println("blockingStub Penguin Success");
        }catch (StatusRuntimeException e) {
            System.out.println(e.getStatus());
        }

        // 異步調用
        CountDownLatch latch = new CountDownLatch(1);
        PenguinRequest request = PenguinRequest.newBuilder().build();
        StreamObserver<PenguinResponse> responseObserver = new StreamObserver<PenguinResponse>(){
            protected int i = 0;
            @Override
            public void onNext(PenguinResponse response) {
                System.out.print("async penguin ");
                System.out.println(++i);
            }
            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                System.out.println(status);

                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
            @Override
            public void onCompleted() {
                System.out.println("stub Penguin Success");
                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
        };
        stub.penguin(request, responseObserver);

        // 等待 調用 完成
        try {
            latch.await();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
    public void requestDog(){
        CountDownLatch latch = new CountDownLatch(1);
        StreamObserver<DogResponse> responseObserver = new StreamObserver<DogResponse>(){
            protected int i = 0;
            @Override
            public void onNext(DogResponse response) {
                System.out.print("async dog ");
                System.out.println(++i);
            }
            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                System.out.println(status);

                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
            @Override
            public void onCompleted() {
                System.out.println("stub Dog Success");
                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
        };
        StreamObserver<DogRequest> requestObserver = stub.dog(responseObserver);


        // 發送 請求
        DogRequest request = DogRequest.newBuilder().build();
        requestObserver.onNext(request);
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){

        }
        requestObserver.onNext(request);

        // 通知 服務器 完成
        requestObserver.onCompleted();

        // 等待 調用 完成
        try {
            latch.await();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
    public void requestCat(){
        // 同步調用
        try {
            CatRequest request = CatRequest.newBuilder().build();
            Iterator<CatResponse> cats = blockingStub.cat(request);
            int i = 0;
            while (cats.hasNext()){
                //CatResponse cat = cats.next();
                cats.next();
                System.out.print("cat ");
                System.out.println(++i);
            }
            System.out.println("blockingStub Cat Success");
        }catch (StatusRuntimeException e){
            System.out.println(e.getStatus());
        }

        // 異步調用
        CountDownLatch latch = new CountDownLatch(1);
        CatRequest request = CatRequest.newBuilder().build();
        StreamObserver<CatResponse> responseObserver = new StreamObserver<CatResponse>(){
            protected int i = 0;
            @Override
            public void onNext(CatResponse response) {
                System.out.print("async cat ");
                System.out.println(++i);
            }
            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                System.out.println(status);

                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
            @Override
            public void onCompleted() {
                System.out.println("stub Cat Success");
                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
        };
        stub.cat(request, responseObserver);

        // 等待 調用 完成
        try {
            latch.await();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
    public void requestFish(){
        CountDownLatch latch = new CountDownLatch(1);
        StreamObserver<FishResponse> responseObserver = new StreamObserver<FishResponse>(){
            protected int i = 0;
            @Override
            public void onNext(FishResponse response) {
                System.out.print("async fish ");
                System.out.println(++i);
            }
            @Override
            public void onError(Throwable t) {
                Status status = Status.fromThrowable(t);
                System.out.println(status);

                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
            @Override
            public void onCompleted() {
                System.out.println("stub Fish Success");
                // 減少 計數 使之爲0 以便通知 操作完成
                latch.countDown();
            }
        };
        StreamObserver<FishRequest> requestObserver = stub.fish(responseObserver);


        // 發送 請求
        FishRequest request = FishRequest.newBuilder().build();
        requestObserver.onNext(request);
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){

        }
        requestObserver.onNext(request);

        // 通知 服務器 完成
        requestObserver.onCompleted();

        // 等待 調用 完成
        try {
            latch.await();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
```

* request 爲流式時 只能使用 異步調用
* 對於響應 StreamObserver 
    * onNext 處理 服務器響應數據
    * onError 處理 錯誤 
    * onCompleted 處理 完成
    * 一旦 onError/onCompleted onNext 就不會再被調用 onError/onCompleted 只會被調用一次
* 對於請求 StreamObserver
    * 調用 onNext 向服務器 發送請求
    * 可以同時調用 onNext 但不能 在 onCompleted 之後 繼續調用 onNext(會產生異常)
    * 調用 onError 通知 框架 出錯
    * 調用 onCompleted 通知 框架 完成
    * 必須調用 一次 onCompleted 以便框架 通知 服務器 完成

# TLS
在 build.gradle 中 增加 依賴

```
#info=false
compile 'io.grpc:grpc-netty:1.18.0'
```
	
使用 TLS 創建 ManagedChannel

```
#info=false
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.examples.zoo.*;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NegotiationType;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.shaded.io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.grpc.stub.StreamObserver;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslProvider;

import javax.net.ssl.SSLException;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

public class Client {
    // 通道 管理者 管理到 服務器間的 tcp 連接
    protected ManagedChannel channel;

    // blockingStub 用於 同步調用 服務器
    // 不支持 stream request
    ServiceGrpc.ServiceBlockingStub blockingStub;
    // stub 用於 異步調用 服務器
    // stub 支持 所有調用 方式
    ServiceGrpc.ServiceStub stub;

    public Client(String host,int port)  {
        // 創建 ssl 環境
        SslContext context;
        try {
            // context = GrpcSslContexts.forClient().build();
            context = GrpcSslContexts
                    .forClient()
                    .sslProvider(SslProvider.JDK)
                    .trustManager(InsecureTrustManagerFactory.INSTANCE) // 忽略證書驗證
                    .build();
        }catch (SSLException e) {
            e.printStackTrace();
            return;
        }

        // 創建 通道 管理者
        channel = NettyChannelBuilder.forAddress(host, port)
                .negotiationType(NegotiationType.TLS)
                .sslContext(context)
                .build();

        // 創建 stub
        // java 不能像 go 一樣 在需要時 創建 client 而是 在最開始 創建 stub
        // 依據不同 調用 上下文 使用 withXXX 創建 新的 stub 類似 go 的 context.Context
        blockingStub = ServiceGrpc.newBlockingStub(channel);
        stub = ServiceGrpc.newStub(channel);
    }

    ...
}
```

# metadata

在 build.gradle 中 增加 依賴

```
#info=false
compile 'io.grpc:grpc-auth:1.18.0'
```

實現 io.grpc.CallCredentials 接口

使用 withCallCredentials 創建帶 元信息的 stub

```
#info="withCallCredentials"
public void requestPenguin() {
    try {
        PenguinRequest request = PenguinRequest.newBuilder().build();
        blockingStub
                .withCallCredentials(
                        new MyCallCredential()
                )
                .penguin(request);
        System.out.println("blockingStub Penguin Success");
    }catch (StatusRuntimeException e) {
        System.out.println(e.getStatus());
    }
}
```

```
#info="CallCredentials"
import io.grpc.CallCredentials2;
import io.grpc.Metadata;
import io.grpc.Status;

import java.util.concurrent.Executor;

// CallCredentials2 已經實現了 CallCredentials接口
//
// 可以 直接重載 applyRequestMetadata 方法 即可
public class MyCallCredential extends CallCredentials2    {
    @Override
    public void applyRequestMetadata(RequestInfo requestInfo, Executor appExecutor, CallCredentials2.MetadataApplier applier) {
        String authority = requestInfo.getAuthority();
        // 目前是 host:port
        System.out.println(authority);

        // 在新的Runnable 中異步 設置 元信息
        appExecutor.execute(()-> {
            try {
                // 創建 元信息
                Metadata headers = new Metadata();

                Metadata.Key<String> key = Metadata.Key.of("name", Metadata.ASCII_STRING_MARSHALLER);
                headers.put(key, "illusive man");

                key = Metadata.Key.of("idea", Metadata.ASCII_STRING_MARSHALLER);
                headers.put(key, "cerberus is an idea");

                // 將 元信息 應用到 發送請求中
                applier.apply(headers);
            } catch (Throwable e) {
                // 錯誤
                applier.fail(Status.UNAUTHENTICATED.withCause(e));
            }
        });
    }

    @Override
    public void thisUsesUnstableApi() {
        // 應該爲空的實現 且不要被調用
        //
        // 只是爲了讓使用者 知道 這是 不穩定的 api
    }
}
```

