# 安裝

```
go get -u gorm.io/gorm
go get -u gorm.io/driver/sqlite
```

```
package main

import (
	"log"
	"os"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// Product 定義一個模型
type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func main() {
	db, err := gorm.Open(sqlite.Open(`test.db`), &gorm.Config{
		Logger: logger.New(
			log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
			logger.Config{
				SlowThreshold: time.Second, // 慢 SQL 闊值
				LogLevel:      logger.Info, // Log level
				Colorful:      true,        // 彩色打印
			},
		),
	})
	if err != nil {
		panic(`failed to connect database`)
	}

	db.AutoMigrate(&Product{})

	// Create
	db.Create(&Product{Code: `D42`, Price: 100})

	// Read
	var product Product
	db.First(&product, 1)                 // 根據整型主鍵查找
	db.First(&product, "code = ?", "D42") // 查找 code 字段值爲 D42 的記錄

	// Update - 將 product 的 price 更新爲 200
	db.Model(&product).Update("Price", 200)
	// Update - 更新多個字段
	db.Model(&product).Updates(Product{Price: 200, Code: "F42"}) // 僅更新非零值字段
	db.Model(&product).Updates(map[string]interface{}{"Price": 200, "Code": "F42"})

	// Delete - 刪除 product
	db.Delete(&product, 1)
}
```