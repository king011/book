# [Secret](https://kubernetes.io/docs/concepts/configuration/secret/)

Secret 類似 ConfigMap 但用來保存敏感信息,可以使用三種方式來使用 Secret

1. 作爲掛接到一個或多個容器上的 **卷** 中的檔案
2. 作爲容器的 **環境變量**
3. 由 kubelet 在爲 Pod 拉取鏡像時使用

在爲 Secret 編寫配置時可以使用 **data** 或 **stringData** 字段，這兩個字段都是可選的。data 字段中的所有鍵值都必須是 base64 編碼的字符串。stringData 字段可以使用任何字符串作爲鍵值。

* 當 Secret 映射到 volumes 時 如果 ConfigMap 更新了 k8s 會在一個 緩存 ttl 時間內/watch傳播延遲 將 volumes 更新
* 使用 Secret 作爲 subPath 掛接的 volumes 不會收到更新
* 使用 Secret 設置的環境變量 不會收到更新
# Secret 類型

在創建 Secret 對象時，可以使用 Secret 資源的 type 字段爲其設置類型。

k8s 提供了若干內置的類型，用於一些常見的使用場景，針對這些類型，k8s 會執行合法性檢查以及實行不同的限制


| 類型 | 含義 |
| -------- | -------- |
| Opaque     | 用戶定義的任意數據     |
| kubernetes.io/service-account-token     | 服務賬號令牌     |
| kubernetes.io/dockercfg     | ~/.dockercfg 檔案的序列化形式     |
| kubernetes.io/dockerconfigjson     | ~/.docker/config.json 檔案的序列化形式     |
| kubernetes.io/basic-auth     | 用於基本身份認證的憑證     |
| kubernetes.io/ssh-auth     | 用於 ssh 身份認證的憑證     |
| kubernetes.io/tls     | 用於 tls 客戶端 或者 服務器的數據     |
| bootstrap.kubernetes.io/token     | 啓動引導令牌數據     |

提供爲 Secret 對象的 type 字段設置一個非空字符串，也可以定義並使用自己的 Secret 類型。如同 type 爲空字符串 則被視爲 Opaque 類型。k8s 不對類型名稱作任何限制。但是如果使用內置類型，則必須滿足該類型定義的要求。


## Opaque

當配置檔案中 沒有顯示設置 type 時，Secret 的類型是 Opaque

使用 `create secret generic` 創建一個 空的 Opaque Secret

```
kubectl create secret generic empty-secret
kubectl get secret empty-secret
```


## kubernetes.io/service-account-token

kubernetes.io/service-account-token 用作服務賬號令牌 此類型 type 需要

* 設置 註解 **kubernetes.io/service-account.name** 爲某個k8s已有服務的賬號名稱

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-sa-sample
  annotations:
    kubernetes.io/service-account.name: "default"
type: kubernetes.io/service-account-token
data:
  # 你可以設置 你自定義的 額外 key value
  extra: YmFyCg==
```

data 會被自動填充上 令牌信息 例如下面的屬性

* ca.crt
* namespace
* token

## docker 配置

docker 依據版本不同 會使用 配置檔案 **~/.dockercfg** 或 **~/.docker/config.json**

k8s 提供了兩個 type 的 來設置 這個檔案

* kubernetes.io/dockercfg -> 需要將檔案內容設置到屬性 **.dockercfg**
* kubernetes.io/dockerconfigjson -> 需要將檔案內容設置到屬性 **.dockerconfigjson**

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-dockercfg
type: kubernetes.io/dockerconfigjson
data:
  .dockercfg: |
        "<base64 encoded ~/.docker/config.json file>"
```

## kubernetes.io/basic-auth

kubernetes.io/basic-auth 用來存儲基本身份認證所需要的憑證信息。此類型 Secret 需要設置如下兩個鍵

* username -> 用於身份認證的用戶名
* password -> 用於身份認證的密碼或令牌

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-basic-auth
type: kubernetes.io/basic-auth
stringData:
  username: admin
  password: t0p-Secret
```

## kubernetes.io/ssh-auth

kubernetes.io/ssh-auth 用於存放 ssh 身份認證中所需要的 privatekey 憑證 需要設置如下鍵

* ssh-privatekey -> ssh 私鑰

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-ssh-auth
type: kubernetes.io/ssh-auth
data:
  # 此示例中的實際數據被截斷
  ssh-privatekey: |
          MIIEpQIBAAKCAQEAulqb/Y ...
```

ssh 私鑰無法建立與服務器之間的可信任連接。需要其它方式來建立信任關係，以緩解 中間人 攻擊，例如向 ConfigMap 中添加一個 known_hosts 檔案

## kubernetes.io/tls

kubernetes.io/tls 用於存放 tls 證書，需要設置如下鍵

* tls.key -> 證書密鑰
* tls.crt -> 證書

## bootstrap.kubernetes.io/token

bootstrap.kubernetes.io/token 用於創建啓動引導令牌類型的 Secret。這種 Secret 被設計用來支持節點啓動引導過程。

此類令牌通常 創建與 **kube-system** 名字空間內，並以 **bootstrap-token-<令牌 ID>** 的形式命名。令牌 ID 是一個由6個字符組成的字符串 用作令牌的標識

此令牌需要設置如下的鍵
* token-id -> 由6個隨機字符組成的字符串，作爲令牌的標識符。必須
* token-secret -> 由16個隨機字符組成的字符串，包含實際的令牌機密。必須
* description -> 供用戶閱讀的字符串，描述令牌用途。可選
* expiration -> 一個使用 RFC3339 來編碼的 UTC 絕對時間，給出令牌過期時間。可選
* usage-bootstrap-&lt;usage&gt; -> 布爾類型標誌，用來標明啓動引導令牌的其它用途
* auth-extra-groups -> 用逗號分隔的組名列表，身份認證時除被認證爲 system:bootstrappers 組外，還會被添加到所列的用戶組中

```
apiVersion: v1
kind: Secret
metadata:
  # 注意 Secret 的命名方式
  name: bootstrap-token-5emitj
  # 启动引导令牌 Secret 通常位于 kube-system 名字空间
  namespace: kube-system
type: bootstrap.kubernetes.io/token
stringData:
  auth-extra-groups: "system:bootstrappers:kubeadm:default-node-token"
  expiration: "2020-09-13T04:39:10Z"
  # 此令牌 ID 被用于生成 Secret 名称
  token-id: "5emitj"
  token-secret: "kq4gihvszzgn1p0r"
  # 此令牌还可用于 authentication （身份认证）
  usage-bootstrap-authentication: "true"
  # 且可用于 signing （证书签名）
  usage-bootstrap-signing: "true"
```

# 使用 Secret

Secret 可以作爲數據卷被掛載，或作爲環境變量暴露出來以供 Pod 中的容器使用

## 將 Secret 數據添加到 volumes 中

Secret 是 一個 key value 可以 將其 添加到 volumes 中

之後 使用 volumeMounts 可以將 Secret 的所有 key 創建爲檔案 並將 value 作爲檔案內容

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mypod
    image: redis
    volumeMounts:
    - name: foo
      mountPath: "/etc/foo"
      readOnly: true
  volumes:
  - name: foo
    secret:
      secretName: mysecret
```

使用 spec.volumes[].secret.items 字段 可以指定 每個鍵對應的目標路徑,並且如果使用了 items 則只有在 items中列出的 鍵會被掛載

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mypod
    image: redis
    volumeMounts:
    - name: foo
      mountPath: "/etc/foo"
      readOnly: true
  volumes:
  - name: foo
    secret:
      secretName: mysecret
      items:
      - key: username
        path: my-group/my-username
```

使用 defaultMode 可以指定掛載的檔案 權限 默認爲 0644

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mypod
    image: redis
    volumeMounts:
    - name: foo
      mountPath: "/etc/foo"
  volumes:
  - name: foo
    secret:
      secretName: mysecret
      defaultMode: 0400
```

## 以環境變量使用 Secret

```
apiVersion: v1
kind: Pod
metadata:
  name: secret-env-pod
spec:
  containers:
  - name: mycontainer
    image: redis
    env:
      - name: SECRET_USERNAME
        valueFrom:
          secretKeyRef:
            name: mysecret
            key: username
      - name: SECRET_PASSWORD
        valueFrom:
          secretKeyRef:
            name: mysecret
            key: password
  restartPolicy: Never
```

Secret 更新後 已經運行的容器內的 環境變量 不會被更新 需要等待容器重啓 或 新的容器 才會更新環境變量

# 不可更改的 Secret

k8s 1.19 beta 提供了 Secret 和 ConfigMap 的不可更改屬性 設置上此 屬性 將無法修改 Secret 和 ConfigMap 需要變更內容時 只能刪除 重建(現有 Pod 將維持對已刪除 Secret 和 ConfigMap 的掛載 建議重寫創建這些 Pod)

不可更改有一些現實的優點:
* 防止意外更新導致應用中斷
* 設置不可更改來關閉 kube-apiserver 對其監視，從而顯著降低 kube-apiserver 的負載，提升集羣性能

```
apiVersion: v1
kind: Secret
metadata:
  ...
data:
  ...
immutable: true
```

# 限制

* Secret 需要先於任何依賴於它的 Pod 創建
* Secret 只能由同一名字空間的 Pod 引用
* Secret 大小限制爲 1MB