# flutter_localizations

flutter_localizations 套件 用來 支持 國際化

1. 在 **pubspec.yaml** 中 添加 對 flutter_localizations 的依賴

	```yaml
	dependencies:
		flutter_localizations:
			sdk: flutter
	```

1. 爲 MaterialApp 設置 localizationsDelegates/supportedLocales 屬性 以及可選的 localeResolutionCallback 屬性

   ```dart
   import 'package:flutter/material.dart';
   import 'package:flutter_localizations/flutter_localizations.dart';
   import 'package:flutter/foundation.dart';
   void main() => runApp(MyApp());
   class MyApp extends StatelessWidget {
       // This widget is the root of your application.
       @override
       Widget build(BuildContext context) {
           return MaterialApp(
               // 定義了 翻譯 資源 如何 獲取
               localizationsDelegates: [
                   // 這兩個 爲默認的 material 提供了 翻譯
                   GlobalMaterialLocalizations.delegate,
                   GlobalWidgetsLocalizations.delegate,
               ],
               // 定義 支持的 語言 如果 系統不支持 則使用 en-US
               supportedLocales: [
                   const Locale('en', 'US'),
                   const Locale('zh', 'TW'),
               ],
               // 可選的 回調 自定義 使用的 語言 
               localeResolutionCallback:(Locale locale, Iterable<Locale> supportedLocales){
                   debugPrint("${locale.languageCode}-${locale.countryCode}");
                   return locale;
               },
               title: 'Flutter Demo',
               theme: ThemeData(
                   primarySwatch: Colors.blue,
               ),
               home: MyHomePage(title: 'Flutter Demo Home Page'),
           );
       }
   }
   ```
	
1. 使用 MaterialLocalizations.of(context).XXX 來獲取 翻譯

# 自定義 本地化 內容

可以 通過 map 簡單的 實現  本地化 並將其 加入到 localizationsDelegates 數組 即可

```dart
#info="localizations.dart"
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
class AppLocalizations {
  final Locale locale;
  AppLocalizations(this.locale);

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  // 創建 翻譯 數據
  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'app_name': 'App Name',
      'hello_world': 'Hello World',
    },
    'zh': {
      'app_name': '應用名',
      'hello_world': '你好世界',
    },
  };
  // 返回 對於 語言 map
  Map<String, String> get _stringMap {
    return _localizedValues[locale.languageCode];
  }

  // 開放接口 返回 翻譯條目
  String get helloWorld {
    return _stringMap['hello_world'];
  }
  String get appName {
    return _stringMap['app_name'];
  }
}
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();
  static const AppLocalizationsDelegate delegate = AppLocalizationsDelegate();
  @override
  bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
```
```dart
#info="main.dart"
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'localizations.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // 定義了 翻譯 資源 如何 獲取
      localizationsDelegates: [
        // 這兩個 爲默認的 material 提供了 翻譯
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        AppLocalizationsDelegate.delegate,
      ],
      // 定義 支持的 語言 如果 系統不支持 則使用 en-US
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('zh', 'TW'),
      ],
      // 可選的 回調 自定義 使用的 語言
      localeResolutionCallback:(Locale locale, Iterable<Locale> supportedLocales){
        debugPrint("${locale.languageCode}-${locale.countryCode}");
        return locale;
      },
      // 提供 SimpleLocalizations 獲取 翻譯
      onGenerateTitle: (context)=>AppLocalizations.of(context).appName,
      //title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
```

# intl

dart 提供了 intl 包 來簡化 國際化 操作

1. 添加依賴

   ```
   dependencies:
     flutter_localizations:
       sdk: flutter
   
   dev_dependencies:
     intl_translation: ^0.17.1
   ```

1. 創建一個 AppLocalizations.dart 在裏面 在裏面 調用 Intl.message 導出 要 翻譯的 字符串

   ```dart
	 #info="lib/i18n/AppLocalizations.dart"
   import 'package:intl/intl.dart';
   import 'package:flutter/material.dart';
   import './messages_all.dart';
   
   class AppLocalizations {
     static AppLocalizations of(BuildContext context) {
       return Localizations.of<AppLocalizations>(context, AppLocalizations);
     }
     static Future<AppLocalizations> load(Locale locale) {
       // 獲取 用戶 環境 locale 名稱
       // 不能用 local.toString()
       // 新的 語言環境中 toString() 並不是 ${locale.languageCode}_${locale.countryCode}
       // 比如 zh TW 的名稱是 zh_Hant_TW
       final name = locale.countryCode.isEmpty ? locale.languageCode : "${locale.languageCode}_${locale.countryCode}";
       // 返回 locale 名稱
       final localeName = Intl.canonicalizedLocale(name);
       // 初始化 語言環境
       return initializeMessages(localeName).then((_) {
         Intl.defaultLocale = localeName;
         return AppLocalizations();
       });
     }
   
   
     // 導出要 翻譯字符
     String get appName {
       return Intl.message('App Name');
     }
     String get helloWorld {
       return Intl.message('Hello world');
     }
   }
   class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
     const AppLocalizationsDelegate();
     static const AppLocalizationsDelegate delegate = AppLocalizationsDelegate();
     
     @override
     // 返回 支持的 語言 環境
     bool isSupported(Locale locale) => ['en', 'zh'].contains(locale.languageCode);
   
     @override
     Future<AppLocalizations> load(Locale locale) => AppLocalizations.load(locale);
   
     @override
     bool shouldReload(AppLocalizationsDelegate old) => false;
   }
   ```

1. 執行 intl_translation:extract_to_arb 命令 由 dart 檔案 抽離出 待翻譯的 條目

	```sh
	# 在當前 檔案夾下 創建 intl_messages.arb
	# --output-dir 參數可以指定 輸出 目錄
	flutter pub pub run intl_translation:extract_to_arb lib/i18n/AppLocalizations.dart
	```
	
1. 將 intl_messages.arb copy 多分到 lib/i18n 作爲 對應 語言的翻譯 條目

	```sh
	copy intl_messages.arb lib/i18n/zh_TW.arb
	copy intl_messages.arb lib/i18n/en_US.arb
	```
	
	編輯 \*\.arb 添加 **@@locale** 屬性指定 locale 名稱
	```json
	{
	  "@@last_modified": "2019-03-01T16:24:53.333067",
	  "@@locale":"zh_TW",
		...
	}
	```
1. 編輯 .arb 完成 翻譯

1. 執行 intl_translation:generate_from_arb 由 arb 創建 dart 檔案

	```sh
	flutter pub pub run intl_translation:generate_from_arb --output-dir=lib/i18n --no-use-deferred-loading lib/main.dart lib/i18n/*.arb
	```
	
1. 將 AppLocalizationsDelegate.delegate 註冊到 localizationsDelegates

1. 使用 AppLocalizations.of(context).xxx 獲取 翻譯