# [監聽事件](https://docs.cocos.com/creator/manual/zh/scripting/events.html)

事件處理是在 節點上完成的 使用

* on 監聽事件
* off 關閉監聽 (參數 必須和 on 完整對應)
* once 觸發一次後自動 關閉監聽 (無法 使用 off 關閉)

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    private _listen: boolean = false
    onLoad() {
        // 創建一個 事件處理器
        const handler = new cc.Component.EventHandler()
        handler.target = this.node // handler 目標
        handler.component = "Button" // 腳本 名
        handler.handler = "callback" // 回調 函數

        // 獲取 node 上的 Button 組件
        const button = this.node.getComponent(cc.Button)
        button.clickEvents.push(handler)
    }
    // 實現 處理函數
    callback(evt: Event, data: any) {
        if (this._listen) {
            this.node.off(cc.Node.EventType.MOUSE_ENTER.toString(),
                this.mouseHandler,
                this,
            )
            this.node.off(cc.Node.EventType.MOUSE_LEAVE.toString(),
                this.mouseHandler,
                this,
            )
            this._listen = false
        } else {
            this.node.on(cc.Node.EventType.MOUSE_ENTER.toString(),
                this.mouseHandler,
                this,
            )
            this.node.on(cc.Node.EventType.MOUSE_LEAVE.toString(),
                this.mouseHandler,
                this,
            )
            this._listen = true
        }
        console.log(this._listen)
    }
    private mouseHandler(evt: MouseEvent) {
        console.log(evt)
    }
}
```

on 第三個 參數是 可選的 指定了 回調函數的 this 調用者 默認爲 undefined

# 發射事件

通過 emit 可以把一個 事件 發射給 節點

emit 最多支持將 5 個 參數傳遞給 節點

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.on('foo', function (arg1, arg2, arg3) {
            console.log(arg1, arg2, arg3);  // print 1, 2, 3
        })
    }

    start() {
        let arg1 = 1, arg2 = 2, arg3 = 3;
        // At most 5 args could be emit.
        this.node.emit('foo', arg1, arg2, arg3);
    }
}
```

# 派送事件

提供 dispatchEvent 可以派送一個事件 此事件 會進入到 冒泡派送 除非 處理函數 調用 event.stopPropagation() 停止冒泡

在 事件回調 中 會 得到一個 [cc.Event](https://docs.cocos.com/creator/2.1/api/zh/classes/Event.html)

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.on('foobar', (evt: cc.Event.EventCustom) => {
            console.log(evt.getUserData())
        })
    }

    start() {
        const evt = new cc.Event.EventCustom(
            'foobar',
            true, // true 需要冒泡
        )
        evt.setUserData("ok")
        this.node.dispatchEvent(evt);
    }
}
```