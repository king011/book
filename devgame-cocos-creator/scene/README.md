# [節點與組件](https://docs.cocos.com/creator/manual/zh/content-workflow/node-component.html)

cocos creator 以組件式開發

節點(Node)是組件(Component)的實體 通過爲Node 掛接不同 Component 來讓 Node 有不同的 表現 和 功能

# 場景
場景 通常 保存在 assets/Scene 檔案夾下 用於呈現遊戲中的某個場景 效果

直接在 資源管理器中 右鍵鼠標->新建->Scene 即可創建一個場景

場景會自動創建一個 Canvas 節點 所有子節點 都應該創建在 Canvas中

# 座標系

1. cocos 使用opengl 進行渲染所以 使用了opengl一樣的 笛卡爾右手座標系
 
  原點在 屏幕左下角

1. ocos 只能設置節點的 本地座標(以父節點 錨點座標爲 原點)

  渲染時 由引擎 自動 轉化到世界座標進行渲染

1. 錨點 確定了以節點 那個位置 作爲 整個節點的位置

  範圍 \[0.00,1.00\]

# 層級管理器

1. 層級管理器 中 顯示出了 節點的 父子關係

1. 層級管理器 中 先渲染 父節點 後渲染子節點

1. 先渲染 上面的 兄弟節點 後渲染 下面的 兄弟節點


# 其它

1. 應該以 Canvas 作爲場景 根節點

  Canvas 提供了到 多分辨率的 自動縮放功能

1. 空節點

  通常 創建空節點 用於組織其它節點 或掛接腳本
