# X11

```
apt install libx11-dev
```

```
find_package(X11 REQUIRED)
list(APPEND target_headers ${X11_INCLUDE_DIRS})
list(APPEND target_libs ${X11_LIBRARIES})
```

-lX11

```
#include <iostream>
#include <memory>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(int argc, char *argv[])
{
    Display *display = XOpenDisplay(nullptr);
    if (!display)
    {
        puts("XOpenDisplay error");
        return -1;
    }
    auto close_display = std::shared_ptr<Display>(display, XCloseDisplay);
    Window root = DefaultRootWindow(display);

    XWindowAttributes att;
    if (!XGetWindowAttributes(display, root, &att))
    {
        puts("XGetWindowAttributes error");
        return -1;
    }
    XImage *img = XGetImage(display, root, 0, 0, att.width, att.height, ~0, ZPixmap);
    if (!img)
    {
        puts("XGetImage error");
        return -1;
    }
    auto close_img = std::shared_ptr<XImage>(img, [](XImage *img)
                                             { XDestroyImage(img); });
    return 0;
}
```