# observable

```
package main

import (
	"errors"
	"fmt"

	"github.com/reactivex/rxgo/iterable"
	"github.com/reactivex/rxgo/observable"
	"github.com/reactivex/rxgo/observer"
)

func main() {
	// 創建一個 Observer 獲取 事件回調
	watcher := observer.Observer{

		// 註冊 可選的 next 方法 當有新數據到達時被回調
		NextHandler: func(item interface{}) {
			fmt.Printf("Processing: %v\n", item)
		},

		// 註冊 可選的 error 方法 當 發生 錯誤時被 回調
		//
		// 因爲 go 不支持 拋出異常 如果 沒有註冊 error 回調 使用 默認的 error 處理 -> func(e error){}
		ErrHandler: func(err error) {
			fmt.Printf("Encountered error: %v\n", err)
		},

		// 註冊 可選的 complete 方法 當 沒有數據可取得時 回調
		DoneHandler: func() {
			fmt.Println("Done!")
		},
	}

	// 從數組 創建一個 迭代器
	it, _ := iterable.New([]interface{}{1, 2, 3, 4, errors.New("bang"), 5})
	// 由 迭代器 創建 Observable
	source := observable.From(it)
	// 訂閱 Observable
	sub := source.Subscribe(watcher)
	// 等待 channel 返回 訂閱的 subscription
	// subscription := <-sub
	<-sub
}
```

# subscription

golang 和 js 等單線程語言不同 一旦 調用 Observable.Subscribe Observable 就將開始執行 需要通過 channel 獲取  subscription

也就是說 當獲取到 subscription 時 Observable 已經開始執行 Observer 可能已經獲取到過數據