# 網路架構

docker-compose 很適合在一臺獨立計算機環境下來部署各種應用，比如個人服務器上的服務使用 docker-compose 比 Kubernetes 更簡單且節省資源。這篇文章是記錄本喵在自己服務器上大量使用 docker-compose 的經驗總結。

1. 首先創建一個 ingress 項目在裏面定義一個供所有項目使用的 network  以及運行一個代理網關(envoy nginx ...)，它爲所有後續 docker-compose 項目提供了入口服務。
2. 其它項目都使用 ingress 定義的網關接入 ingress 網路。

現在每個 docker-compose 都可以按照自己需要在它們自己的私有網路中隨意的使用端口，只是同時使用外部 ingress 提供的網路接入 ingress即可。
# Example
下面是一個例子，它定義了 a b 兩個各種獨立的 docker-compose 項目，然後使用 ingress 將它們提供給外部訪問。例子中 ingress 的網關使用了 nginx，因爲 nginx 比較簡單容易理解，但實際部署推薦使用 envoy，因爲 nginx 對 h2 h2c grpc 的支持沒有 envoy 好。簡單來說作爲入口網關 envoy 提供更強大的功能和可能性，某些網關功能 使用 nginx 無法完成，所以一開始就使用 envoy 是最好的選擇。但是如果你的需求簡單明確使用 nginx 也無可厚非，只是如果後續塔尖了太多服務後才切換 envoy 網關可能需要付出點努力去修改 ingress 的配置


## ingress

首先定義一個 ingress 作爲這臺服務器對外訪問的入口，它完成了如下事情

1. 創建了 ingress 網路，供其它服務加入
2. 運行了一個 nginx 處理網路流量，它使用域名進行分流，將 host 爲 ServiceA 的請求發往後端的 A 項目，將 host 爲 ServiceB 的請求發往後端的 B 項目
3. 將網關的 80 端口映射到主機的 9000 端口

> 請根據你的實際需要修改步驟2網關的路由規則，同理修改步驟3開放到主機的端口


```
#info="docker-compose.yml"
version: '1'
networks:
  intranet: # 定義了用於系統內部互聯的內部網路 ingress_intranet
    ipam:
      driver: default
      config:
        - subnet: "172.16.0.0/16" # 指定網路 ip 段
services:
  main: # 入口網關定義
    image: nginx:1.23
    restart: always
    ports:
      - "9000:80"
    volumes:
      - ./ingress.conf:/etc/nginx/conf.d/ingress.conf:ro # 接入路由規則
    networks: # 指定容器接入的網路
      default:
      intranet: # 加入 ingress_intranet 網路並顯示指定 ip
        ipv4_address: 172.16.0.2
```

```
#info="ingress.conf"
server {
    listen  80;
    server_name     ServiceA;
    location / {
        # version
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;

        # Allow websocket connections
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;

        proxy_set_header X-Real-IP              $remote_addr;                         
                                                                                                            
        proxy_set_header X-Forwarded-For        $remote_addr;

        proxy_set_header X-Forwarded-Host       $http_host;                      
        proxy_set_header X-Forwarded-Port       $server_port;                           
        proxy_set_header X-Forwarded-Proto      $scheme;                                  
        proxy_set_header X-Forwarded-Scheme     $scheme;                  
                                                                                                            
        proxy_set_header X-Scheme               $scheme;  

        # Pass the original X-Forwarded-For
        proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;

        # 轉發到後端服務器 A 
        proxy_pass http://172.16.1.2/;
    }
}
server {
    listen  80;
    server_name     ServiceB;
     location / {
        # version
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;

        # Allow websocket connections
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;

        proxy_set_header X-Real-IP              $remote_addr;                         
                                                                                                            
        proxy_set_header X-Forwarded-For        $remote_addr;

        proxy_set_header X-Forwarded-Host       $http_host;                      
        proxy_set_header X-Forwarded-Port       $server_port;                           
        proxy_set_header X-Forwarded-Proto      $scheme;                                  
        proxy_set_header X-Forwarded-Scheme     $scheme;                  
                                                                                                            
        proxy_set_header X-Scheme               $scheme;  

        # Pass the original X-Forwarded-For
        proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;

        # 轉發到後端服務器 B
        proxy_pass http://172.16.2.2/;
    }
}
```

## ServiceA

現在來創建一個模擬的服務 A，它需要顯示的加入 ingress\_intranet 網路設置 ip 爲 172.16.1.2(和上面 ingress 定義的路由 ip 匹配就行)，並對外開放 80 端口

```
#info="docker-compose.yml"
version: '1'
networks:
  ingress_intranet: # 使用外部定義的網關
    external: true
services:
  main: # 項目 A 的入口服務
    image: nginx:1.23
    restart: always
    volumes:
      - ./ingress.conf:/etc/nginx/conf.d/ingress.conf:ro # 接入路由規則
    networks: # 指定容器接入的網路
      default:
      ingress_intranet: # 加入 ingress_intranet 網路並顯示指定 ip
        ipv4_address: 172.16.1.2
```

```
#info="ingress.conf"
server {
    listen  80;
    server_name     ServiceA;
    location / {
        add_header Content-Type "text/plain;charset=utf-8";
        return 200 "this is service a, $date_gmt on $hostname.\n";
    }
}
```

現在你可以使用 curl 來訪問的 ServiceA 提供的功能

```
curl http://127.0.0.1:9000 -H "Host: ServiceA"
```

## ServiceB

如你所見， ServiceA 是一個簡單的服務，它直接由 main 容器向 ingress_intranet 網路提供了功能。但真實項目通常更複製，通過項目可能由多個容器組成，ServiceB 用於演示如何處理。

1. 我們實現在 ServiceB 中也創建一個 ingress 容器，使用 ingress 容器加入 ingress\_intranet 網路提供到 ServiceB 的訪問
2. ingress 容器 路由流量到自己所在項目的其它容器

```
#info="docker-compose.yml"
version: '1'
networks:
  ingress_intranet: # 使用外部定義的網關
    external: true
services:
  ingress: # 項目 B 的入口服務
    image: nginx:1.23
    restart: always
    volumes:
      - ./ingress.conf:/etc/nginx/conf.d/ingress.conf:ro # 接入路由規則
    networks: # 指定容器接入的網路
      default:
      ingress_intranet: # 加入 ingress_intranet 網路並顯示指定 ip
        ipv4_address: 172.16.2.2
  v1:
    image: nginx:1.23
    restart: always
    volumes:
      - ./default.conf:/etc/nginx/conf.d/default.conf:ro
  v2:
    image: nginx:1.23
    restart: always
    volumes:
      - ./default.conf:/etc/nginx/conf.d/default.conf:ro
```

```
#info="ingress.conf"
server {
    listen  80;
    server_name     ServiceB;
    location / {
        add_header Content-Type "text/plain;charset=utf-8";
        return 200 "this is service b, $date_gmt on $hostname.\n";
    }
    location /v1 {
        # version
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;

        # Allow websocket connections
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;

        proxy_set_header X-Real-IP              $remote_addr;                         
                                                                                                            
        proxy_set_header X-Forwarded-For        $remote_addr;

        proxy_set_header X-Forwarded-Host       $http_host;                      
        proxy_set_header X-Forwarded-Port       $server_port;                           
        proxy_set_header X-Forwarded-Proto      $scheme;                                  
        proxy_set_header X-Forwarded-Scheme     $scheme;                  
                                                                                                            
        proxy_set_header X-Scheme               $scheme;  

        # Pass the original X-Forwarded-For
        proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;

        # 轉發到後端服務器 v1 
        proxy_pass http://v1/v1;
    }
    location /v2 {
        # version
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;

        # Allow websocket connections
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;

        proxy_set_header X-Real-IP              $remote_addr;                         
                                                                                                            
        proxy_set_header X-Forwarded-For        $remote_addr;

        proxy_set_header X-Forwarded-Host       $http_host;                      
        proxy_set_header X-Forwarded-Port       $server_port;                           
        proxy_set_header X-Forwarded-Proto      $scheme;                                  
        proxy_set_header X-Forwarded-Scheme     $scheme;                  
                                                                                                            
        proxy_set_header X-Scheme               $scheme;  

        # Pass the original X-Forwarded-For
        proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;

        # 轉發到後端服務器 v2
        proxy_pass http://v2/v2;
    }
}
```

```
#info="default.conf"
server {
    listen  80;
    location / {
        add_header Content-Type "text/plain;charset=utf-8";
        return 200 "this is service b $uri, $date_gmt on $hostname.\n";
    }
}
```

現在可以使用 curl 服務 ServiceB 提供的各種功能

```
curl http://127.0.0.1:9000 -H "Host: ServiceB"
curl http://127.0.0.1:9000/v1 -H "Host: ServiceB"
curl http://127.0.0.1:9000/v2 -H "Host: ServiceB"
```

# 最終

* 現在每個 docker-compose 項目可以完全獨立部署與運行，它們之間也互相隔離不可見，唯一的溝通渠道就是自己開放到 ingress\_intranet 網路的服務
* 如果你有了一個新的項目要部署，只需要把它加入到 ingress\_intranet 網路，並且修改 ingress 項目中的路由規則即可
* 如果你要刪除一個項目，只需 docker-compose down，然後在 ingress 項目中刪除路由規則即可

如你所見增加和刪除項目都需要重新加載 ingress 項目的路由規則，這也是推薦使用 envoy 的原因之一，它可以比 nginx 更高效的重載路由，但對於個人或者把大量服務放在同一台計算機的情況來說這種效率差的影響幾乎可以忽略。所以如果不是有潔癖，通常 nginx 也可以工作的很好(畢竟 envoy 比 nginx 輔助且對人類不友好的多，但本喵是貓咪不是人類所以更愛 envoy)。