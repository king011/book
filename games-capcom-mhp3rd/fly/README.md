# 雌火龍

![](assets/雌火龍.jpg)

## 基本抗性

◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>头<br>腹部</center></div>
</td><td><div><center>头<br>腹部</center></div>
</td><td><div><center>头<br>腹部<br>脚</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">90 </font></td><td> <font color="#ff0000">80 </font></td><td> <font color="#ff0000">70 </font></td><td> 0 </td><td> 15 </td><td> 20 </td><td> 15 </td><td> 35 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 40 </td><td> 40 </td><td> 40 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 90
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 25 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 25 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">45 </font></td><td> 0 </td><td> 5 </td><td> 20 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> 25 </td><td> 20 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 左100/右100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 44 </td><td> 40 </td><td> <font color="#ff0000">45 </font></td><td> 0 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 30 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 25 </td><td> 0 </td><td> 140
</td></tr></tbody></table>

## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 150 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (580) </td><td> 110 (730) </td><td> 100 (580) </td><td> 150 (750) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>

## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★4（容易逃） 摆脱束缚攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> △ </td><td> 猎人与怪物同区，并且没有被怪物发现的情况下会吃
</td></tr></tbody></table>

## 剝取 掉落

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="雌火龙的鳞">雌火龙的鳞</a> <font color="#008000"><b>40%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雌火龙的甲壳">雌火龙的甲壳</a> <font color="#008000"><b>30%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="雌火龙的翼膜">雌火龙的翼膜</a> <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E7%82%8E%E8%A2%8B" title="火炎袋">火炎袋</a> <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E6%A3%98" title="雌火龙的棘">雌火龙的棘</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="雌火龙的上鳞">雌火龙的上鳞</a> <font color="#008000"><b>38%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雌火龙的坚壳">雌火龙的坚壳</a> <font color="#008000"><b>25%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="雌火龙的翼膜">雌火龙的翼膜</a> <font color="#008000"><b>12%</b></font><br><a href="/%E7%88%86%E7%82%8E%E8%A2%8B" title="爆炎袋">爆炎袋</a> <font color="#008000"><b>12%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E6%A3%98" title="雌火龙的棘">雌火龙的棘</a> <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E6%A3%98" title="雌火龙的上棘">雌火龙的上棘</a> <font color="#008000"><b>5%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="雌火龙的鳞">雌火龙的鳞</a> <font color="#008000"><b>62%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E6%A3%98" title="雌火龙的棘">雌火龙的棘</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雌火龙的逆鳞">雌火龙的逆鳞</a> <font color="#008000"><b>3%</b></font> </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="雌火龙的上鳞">雌火龙的上鳞</a> <font color="#008000"><b>55%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E6%A3%98" title="雌火龙的上棘">雌火龙的上棘</a> <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="火龙的延髓">火龙的延髓</a> <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雌火龙的逆鳞">雌火龙的逆鳞</a> <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="雌火龙的鳞">雌火龙的鳞</a> <font color="#008000"><b>7%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="雌火龙的红玉">雌火龙的红玉</a> <font color="#008000"><b>3%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>74%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="雌火龙的鳞">雌火龙的鳞</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E6%A3%98" title="雌火龙的棘">雌火龙的棘</a> <font color="#008000"><b>10%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雌火龙的逆鳞">雌火龙的逆鳞</a> <font color="#008000"><b>1%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="雌火龙的上鳞">雌火龙的上鳞</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E6%A3%98" title="雌火龙的上棘">雌火龙的上棘</a> <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="雌火龙的红玉">雌火龙的红玉</a> <font color="#008000"><b>2%</b></font> </td><td> 破头、在雌火龙在空中时将其击落均有几率掉落
</td></tr></tbody></table>

## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雌火龙的甲壳">雌火龙的甲壳</a>*1 <font color="#008000"><b>71%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="雌火龙的鳞">雌火龙的鳞</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雌火龙的逆鳞">雌火龙的逆鳞</a>*1 <font color="#008000"><b>4%</b></font> </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雌火龙的坚壳">雌火龙的坚壳</a>*1 <font color="#008000"><b>68%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="雌火龙的上鳞">雌火龙的上鳞</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雌火龙的甲壳">雌火龙的甲壳</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="雌火龙的红玉">雌火龙的红玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 头部共可破两次，第二次全破，全破获取几率提升
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E7%88%AA" title="火龙的翼爪">火龙的翼爪</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E9%BE%99%E4%B9%8B%E7%88%AA" title="龙之爪">龙之爪</a>*4 <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="雌火龙的翼膜">雌火龙的翼膜</a>*1 <font color="#008000"><b>15%</b></font> </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E7%88%AA" title="火龙的翼爪">火龙的翼爪</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E9%BE%99%E4%B9%8B%E7%88%AA" title="龙之爪">龙之爪</a>*5 <font color="#008000"><b>15%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="雌火龙的翼膜">雌火龙的翼膜</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 左右两翼全破之后获取几率提升
</td></tr></tbody></table>

## 捕獲

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="雌火龙的甲壳">雌火龙的甲壳</a>*1 <font color="#008000"><b>33%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="雌火龙的鳞">雌火龙的鳞</a>*2 <font color="#008000"><b>30%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E7%82%8E%E8%A2%8B" title="火炎袋">火炎袋</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雌火龙的逆鳞">雌火龙的逆鳞</a>*1 <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="雌火龙的坚壳">雌火龙的坚壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="雌火龙的上鳞">雌火龙的上鳞</a>*2 <font color="#008000"><b>22%</b></font><br><a href="/%E7%88%86%E7%82%8E%E8%A2%8B" title="爆炎袋">爆炎袋</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a>*1 <font color="#008000"><b>13%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="火龙的延髓">火龙的延髓</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E7%81%AB%E7%82%8E%E8%A2%8B" title="火炎袋">火炎袋</a>*2 <font color="#008000"><b>5%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="雌火龙的逆鳞">雌火龙的逆鳞</a>*1 <font color="#008000"><b>4%</b></font><br><a href="/%E9%9B%8C%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="雌火龙的红玉">雌火龙的红玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>

## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>女王、乱立す
</p>
</td><td>
<p>连续讨伐三头<strong class="selflink">雌火龙</strong>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>孤島の来訪者
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>和一头<a href="/%E9%9D%92%E7%86%8A%E5%85%BD" title="青熊兽">青熊兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>天と地の領域！
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>和一头<a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>リオレイア、現る
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>女王リオレイアの狩猟
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>月夜の暗躍
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>和一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>天と地の領域！
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>和一头<a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>リオレイア、現る
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>女王リオレイアの狩猟
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>天と地の領域！
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>和一头<a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>緊迫した渓流の中で
</p>
</td><td>
<p>一头<strong class="selflink">雌火龙</strong>、一头<a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99" title="雷狼龙">雷狼龙</a>、一头<a href="/%E7%8B%97%E9%BE%99%E7%8E%8B" title="狗龙王">狗龙王</a>连续狩猎
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%B8%A9%E6%B3%89%E4%BB%BB%E5%8A%A1" title="温泉任务">温泉任务</a>
</p>
</td><td>
<p>竜の訪れる秘泉
</p>
</td><td>
<p>讨伐一头<strong class="selflink">雌火龙</strong>
</p>
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 朝猎人进行突进攻击，突进后接大前咬
</td></tr>
<tr style="background:#f9f9f9;">
<td> 三连龙车 </td><td> 连续3次突进，前两次突进后都是急刹车，然后重新锁定猎人突进，第三下最后接大前咬
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 前进一小步（这个其实伤害挺高的）后使用啃咬，同时尾部也有判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转身体使用甩尾攻击，可以连续多次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 火球 </td><td> 吐出火球攻击，分直线火球和3连火球（依次按前，右，左方向喷出），命中同时还会使猎人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 爆裂火球 </td><td> 向前方吐出一颗大范围爆炸的火球，命中同时还会使猎人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 毒尾 </td><td> 使用后空翻式的甩尾攻击，命中同时会使猎人陷入中毒异常，可以翻多次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下咬 </td><td> 低空时朝猎人小咬一下，擦伤
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食 </td><td> 低空时冲下来用抓抓取猎物，被抓到即进入捕食状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滑翔 </td><td> 使用滑翔攻击冲向猎人，并附带风压【大】的效果
</td></tr></tbody></table>

# 金火龍

# 雄火龍
![](assets/雄火龍.png)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头<br>脚</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">75 </font></td><td> <font color="#ff0000">70 </font></td><td> <font color="#ff0000">60 </font></td><td> 0 </td><td> 15 </td><td> 20 </td><td> 15 </td><td> 35 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 44 </td><td> 40 </td><td> 20 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 90
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 25 </td><td> 30 </td><td> 20 </td><td> 15 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 25 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> 44 </td><td> 40 </td><td> 15 </td><td> 0 </td><td> 5 </td><td> 20 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> 25 </td><td> 20 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 左100/右100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 30 </td><td> 25 </td><td> <font color="#ff0000">45 </font></td><td> 0 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 30 </td><td>  30 </td><td>  25 </td><td> 0 </td><td>  5 </td><td> 10 </td><td> 5 </td><td> 25 </td><td> 0 </td><td> 140
</td></tr></tbody></table>

## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 150 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (580) </td><td> 110 (730) </td><td> 100 (580) </td><td> 150 (750) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>

## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★5（稍微容易逃） 摆脱束缚攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> △ </td><td> 猎人与怪物同区，并且没有被怪物发现的情况下会吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="火龙的鳞">火龙的鳞</a> <font color="#008000"><b>40%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="火龙的甲壳">火龙的甲壳</a> <font color="#008000"><b>28%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="火龙的翼膜">火龙的翼膜</a> <font color="#008000"><b>19%</b></font><br><a href="/%E7%81%AB%E7%82%8E%E8%A2%8B" title="火炎袋">火炎袋</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="火龙的上鳞">火龙的上鳞</a> <font color="#008000"><b>38%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="火龙的坚壳">火龙的坚壳</a> <font color="#008000"><b>29%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="火龙的翼膜">火龙的翼膜</a> <font color="#008000"><b>13%</b></font><br><a href="/%E7%88%86%E7%82%8E%E8%A2%8B" title="爆炎袋">爆炎袋</a> <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="火龙的延髓">火龙的延髓</a> <font color="#008000"><b>5%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="火龙的尾巴">火龙的尾巴</a> <font color="#008000"><b>60%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="火龙的鳞">火龙的鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a> <font color="#008000"><b>17%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a> <font color="#008000"><b>3%</b></font> </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="火龙的尾巴">火龙的尾巴</a> <font color="#008000"><b>48%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="火龙的上鳞">火龙的上鳞</a> <font color="#008000"><b>22%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="火龙的延髓">火龙的延髓</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="火龙的鳞">火龙的鳞</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="火龙的红玉">火龙的红玉</a> <font color="#008000"><b>3%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>74%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="火龙的鳞">火龙的鳞</a> <font color="#008000"><b>24%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a> <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="火龙的上鳞">火龙的上鳞</a> <font color="#008000"><b>23%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="火龙的红玉">火龙的红玉</a> <font color="#008000"><b>2%</b></font> </td><td> 破头、在雄火龙在空中时将其击落均有几率掉落
</td></tr></tbody></table>

## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%B3%9E" title="火龙的鳞">火龙的鳞</a>*1 <font color="#008000"><b>66%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="火龙的甲壳">火龙的甲壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a>*1 <font color="#008000"><b>4%</b></font> </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="火龙的上鳞">火龙的上鳞</a>*1 <font color="#008000"><b>57%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="火龙的坚壳">火龙的坚壳</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="火龙的甲壳">火龙的甲壳</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a>*1 <font color="#008000"><b>6%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="火龙的红玉">火龙的红玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 头部共可破两次，第二次全破，全破获取几率提升
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E7%88%AA" title="火龙的翼爪">火龙的翼爪</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E9%BE%99%E4%B9%8B%E7%88%AA" title="龙之爪">龙之爪</a>*4 <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="火龙的翼膜">火龙的翼膜</a>*1 <font color="#008000"><b>15%</b></font> </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E7%88%AA" title="火龙的翼爪">火龙的翼爪</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E9%BE%99%E4%B9%8B%E7%88%AA" title="龙之爪">龙之爪</a>*5 <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="火龙的上鳞">火龙的上鳞</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="火龙的翼膜">火龙的翼膜</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 左右两翼全破之后几率提升
</td></tr></tbody></table>

## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="火龙的甲壳">火龙的甲壳</a>*1 <font color="#008000"><b>42%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="火龙的翼膜">火龙的翼膜</a>*1 <font color="#008000"><b>29%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="火龙的尾巴">火龙的尾巴</a>*1 <font color="#008000"><b>9%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a>*1 <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="火龙的坚壳">火龙的坚壳</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BF%BC%E8%86%9C" title="火龙的翼膜">火龙的翼膜</a>*2 <font color="#008000"><b>18%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="火龙的骨髓">火龙的骨髓</a>*1 <font color="#008000"><b>16%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="火龙的尾巴">火龙的尾巴</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="火龙的延髓">火龙的延髓</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E9%80%86%E9%B3%9E" title="火龙的逆鳞">火龙的逆鳞</a>*1 <font color="#008000"><b>5%</b></font><br><a href="/%E7%81%AB%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="火龙的红玉">火龙的红玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>

## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>空の王者を狩猟せよ！
</p>
</td><td>
<p>讨伐一头雄火龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>空の王者リオレウス！
</p>
</td><td>
<p>讨伐一头雄火龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>天と地の領域！
</p>
</td><td>
<p>讨伐一头雄火龙和一头<a href="/%E9%9B%8C%E7%81%AB%E9%BE%99" title="雌火龙">雌火龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>双の焰！
</p>
</td><td>
<p>讨伐一头雄火龙和一头<a href="/%E7%82%8E%E6%88%88%E9%BE%99" title="炎戈龙">炎戈龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>空の王者を狩猟せよ！
</p>
</td><td>
<p>讨伐一头雄火龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>天と地の領域！
</p>
</td><td>
<p>讨伐一头雄火龙和一头<a href="/%E9%9B%8C%E7%81%AB%E9%BE%99" title="雌火龙">雌火龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>孤島の森
</p>
</td><td>
<p>讨伐一头雄火龙和一头<a href="/%E7%8B%97%E9%BE%99%E7%8E%8B" title="狗龙王">狗龙王</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>空の王者を狩猟せよ！
</p>
</td><td>
<p>讨伐一头雄火龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>空の王者リオレウス！
</p>
</td><td>
<p>讨伐一头雄火龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>クローベリング・タイム！
</p>
</td><td>
<p>一头<a href="/%E9%92%A2%E9%94%A4%E9%BE%99" title="钢锤龙">钢锤龙</a>、两头<a href="/%E6%AF%92%E7%8B%97%E9%BE%99" title="毒狗龙">毒狗龙</a>、一头雄火龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>天と地の領域！
</p>
</td><td>
<p>讨伐一头雄火龙和一头<a href="/%E9%9B%8C%E7%81%AB%E9%BE%99" title="雌火龙">雌火龙</a>
</p>
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 朝猎人进行突进攻击，突进后接大前咬
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 前进一小步（这个其实伤害挺高的）后使用啃咬，同时尾部也有判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转身体使用甩尾攻击，可以连续多次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 火球 </td><td> 吐出直线火球攻击，命中同时还会使猎人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 升空 </td><td> 原地踏两步后飞起进入中空状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后跃火球 </td><td> 使用后跃同时朝地面喷一颗火球（命中同时进入会使猎人陷入燃烧异常），然后进入低空状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼，吼完接后跃火球
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下咬 </td><td> 低空时朝猎人小咬一下，擦伤
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食 </td><td> 低空时冲下来用抓抓取猎物，被抓到即进入捕食状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滑翔 </td><td> 使用滑翔攻击冲向猎人，并附带风压【小】的效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 3连火球 </td><td> 中空时朝地面连续喷3可火球，命中同时进入会使猎人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 毒爪 </td><td> 中空时冲下来使用毒爪攻击猎人，命中同时进入会使猎人陷入中毒+气绝异常
</td></tr></tbody></table>


# 銀火龍
# 角龍
![](assets/角龍.jpg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>颈<br>腹部<br>尾巴</center></div>
</td><td><div><center>颈<br>腹部</center></div>
</td><td><div><center>翼膜<br>尾巴</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 25 </td><td> 15 </td><td> 20 </td><td> 0 </td><td> 15 </td><td> 15 </td><td> 20 </td><td> 15 </td><td> 100 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 30 </td><td> 0 </td><td> 10 </td><td> 10 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 23 </td><td> 42 </td><td> 20 </td><td> 10 </td><td> 15 </td><td> 15 </td><td> 25 </td><td> 15 </td><td> 0 </td><td> 350
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">68 </font></td><td> <font color="#ff0000">77 </font></td><td> 40 </td><td> 0 </td><td> 10 </td><td> 10 </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 350
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼骨 </td><td> 23 </td><td> 42 </td><td> 20 </td><td> 10 </td><td> 15 </td><td> 15 </td><td> 20 </td><td> 15 </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼膜 </td><td> 40 </td><td> 30 </td><td> <font color="#ff0000">45 </font></td><td> 0 </td><td> 10 </td><td> 10 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 35 </td><td> 35 </td><td> 35 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 8 </td><td> 5 </td><td> 0 </td><td> 左250/右250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> <font color="#ff0000">50 </font></td><td> 25 </td><td> <font color="#ff0000">60 </font></td><td> 0 </td><td> 10 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 0 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 22 </td><td> 35 </td><td> 10 </td><td> 10 </td><td> 15 </td><td> 15 </td><td> 30 </td><td> 15 </td><td> 0 </td><td> 250
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 200 </td><td> 200 </td><td> 150 </td><td> 150 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 50 (600) </td><td> 100 (700) </td><td> 50 (500) </td><td> 100 (650) </td><td> 100 (800)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 10/5秒 </td><td> 5/15秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 10/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 50秒 </td><td> 15秒 </td><td> 40秒 </td><td> 10秒 </td><td> 90秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 250 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> △ </td><td> 初回10秒（疲劳20秒） → 2回8秒（疲劳10秒） → 3回开始8秒（疲劳8秒） （从地里冲过上浮的话被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> △ </td><td> 初回10秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒） （从地里冲过上浮的话被破坏）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+5秒、发怒-5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> △ </td><td> 初回12秒 （疲劳15秒） （潜地时使用，发怒无效，效果如同落穴）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="角龙的甲壳">角龙的甲壳</a> <font color="#008000"><b>63%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%89%99" title="角龙的牙">角龙的牙</a> <font color="#008000"><b>30%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="角龙的背甲">角龙的背甲</a> <font color="#008000"><b>7%</b></font> </td><td> <a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="角龙的坚壳">角龙的坚壳</a> <font color="#008000"><b>60%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%89%99" title="角龙的牙">角龙的牙</a> <font color="#008000"><b>25%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="角龙的甲壳">角龙的甲壳</a> <font color="#008000"><b>8%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="角龙的坚甲">角龙的坚甲</a> <font color="#008000"><b>7%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="角龙的尾甲">角龙的尾甲</a> <font color="#008000"><b>55%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="角龙的甲壳">角龙的甲壳</a> <font color="#008000"><b>37%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="角龙的骨髓">角龙的骨髓</a> <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="角龙的尾甲">角龙的尾甲</a> <font color="#008000"><b>50%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="角龙的坚壳">角龙的坚壳</a> <font color="#008000"><b>28%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="角龙的骨髓">角龙的骨髓</a> <font color="#008000"><b>12%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="角龙的延髓">角龙的延髓</a> <font color="#008000"><b>6%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>4%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="角龙的背甲">角龙的背甲</a> <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="角龙的坚甲">角龙的坚甲</a> <font color="#008000"><b>20%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="角龙的背甲">角龙的背甲</a> <font color="#008000"><b>5%</b></font> </td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 角 </td><td> <a href="/%E5%8D%B7%E8%A7%92" title="卷角">卷角</a>*1 <font color="#008000"><b>92%</b></font><br><a href="/%E4%BC%98%E8%B4%A8%E7%9A%84%E5%8D%B7%E8%A7%92" title="优质的卷角">优质的卷角</a>*1 <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E5%8D%B7%E8%A7%92" title="卷角">卷角</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E4%BC%98%E8%B4%A8%E7%9A%84%E5%8D%B7%E8%A7%92" title="优质的卷角">优质的卷角</a>*1 <font color="#008000"><b>40%</b></font> </td><td> 双角全破，获取几率提升
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%89%99" title="角龙的牙">角龙的牙</a>*2 <font color="#008000"><b>32%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="角龙的甲壳">角龙的甲壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="角龙的尾甲">角龙的尾甲</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="角龙的背甲">角龙的背甲</a>*1 <font color="#008000"><b>13%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="角龙的骨髓">角龙的骨髓</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="角龙的坚甲">角龙的坚甲</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="角龙的尾甲">角龙的尾甲</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E7%89%99" title="角龙的牙">角龙的牙</a>*3 <font color="#008000"><b>15%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="角龙的骨髓">角龙的骨髓</a>*1 <font color="#008000"><b>14%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="角龙的坚甲">角龙的坚甲</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="角龙的延髓">角龙的延髓</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E8%A7%92%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="角龙的背甲">角龙的背甲</a>*2 <font color="#008000"><b>5%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>砂原の角竜を狩れ！
</p>
</td><td>
<p>讨伐一头角龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>砂原からの救援依頼
</p>
</td><td>
<p>讨伐一头<a href="/%E5%9C%9F%E7%A0%82%E9%BE%99" title="土砂龙">土砂龙</a>和一头角龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>砂原の角竜を狩れ！
</p>
</td><td>
<p>讨伐一头角龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>角竜と土砂竜の猛攻！
</p>
</td><td>
<p>讨伐一头角龙和一头<a href="/%E5%9C%9F%E7%A0%82%E9%BE%99" title="土砂龙">土砂龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>砂原の角竜を狩れ！
</p>
</td><td>
<p>讨伐一头角龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>暴君の時代
</p>
</td><td>
<p>讨伐一头角龙和一头<a href="/%E9%BB%91%E8%A7%92%E9%BE%99" title="黑角龙">黑角龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>戦慄の進軍
</p>
</td><td>
<p>一头角龙和一头<a href="/%E9%BB%91%E8%A7%92%E9%BE%99" title="黑角龙">黑角龙</a>和一头<a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99" title="潜口龙">潜口龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 使用突进攻击猎人，突进后会接角顶或向前滑行一段
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续龙车 </td><td> 突进完后急停，然后重新锁定猎人进行突进，最多能连续3次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 向前一小步后使用啃咬攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大角顶 </td><td> 退二三步后，向左（或先向左后向右）使用大范围角顶攻击，尾部也存在攻击判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 回旋甩尾 </td><td> 旋转身体使用甩尾攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后方甩尾 </td><td> 尾巴朝后方依次甩两下
</td></tr>
<tr style="background:#f9f9f9;">
<td> 潜地 </td><td> 潜入地面
</td></tr>
<tr style="background:#f9f9f9;">
<td> 潜行后突进急袭 </td><td> 潜地后猛地钻出地面攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 潜行后飞扑 </td><td> 潜地后先在地面冒出沙尘，然后突然飞扑出来进行攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】 </td><td> 仰头使用吼叫攻击，属于高吼
</td></tr></tbody></table>

# 黑角龍

# 轟龍
![](assets/轟龍.jpg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">70 (75) </font></td><td> <font color="#ff0000">65 (70) </font></td><td> <font color="#ff0000">55 (60) </font></td><td> 0 </td><td> 10 </td><td> 20 </td><td> 5 </td><td> 15 </td><td> 100 </td><td> 220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 44 </td><td> 44 </td><td> 25 (30) </td><td> 0 </td><td> 10 </td><td> 20 </td><td> 5 </td><td> 15 </td><td> 0 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 44 (40) </td><td> 40 (35) </td><td> 30 (35) </td><td> 0 </td><td> 15 </td><td> 25 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 胸腹 </td><td> 25 (30) </td><td> 30 (35) </td><td> 20 </td><td> 15 </td><td> 15 </td><td> 15 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 25 (30) </td><td> 25 (30) </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 20 (15) </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 左200/右200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 40 </td><td> 40 </td><td> 40 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 左220/右220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 35 </td><td> 30 </td><td> 20 </td><td> 0 </td><td> 5 </td><td> 15 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 240
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 200 </td><td> 180 </td><td> 200 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 110 (620) </td><td> 100 (780) </td><td> 120 (660) </td><td> 140 (760) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 20秒 </td><td> 10秒 </td><td> 80秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回16秒（疲劳20秒） → 2回8秒（疲劳10秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回10秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> ○ </td><td> 疲劳时即使发现猎人也会吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td> 3
</td><td>
<p><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%B3%9E" title="轰龙的鳞">轰龙的鳞</a> <font color="#008000"><b>40%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%88%AA" title="轰龙的爪">轰龙的爪</a> <font color="#008000"><b>37%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="轰龙的甲壳">轰龙的甲壳</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="轰龙的头壳">轰龙的头壳</a> <font color="#008000"><b>3%</b></font>
</p>
</td><td>
<p><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="轰龙的上鳞">轰龙的上鳞</a> <font color="#008000"><b>35%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="轰龙的尖爪">轰龙的尖爪</a> <font color="#008000"><b>30%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="轰龙的坚壳">轰龙的坚壳</a> <font color="#008000"><b>17%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%B3%9E" title="轰龙的鳞">轰龙的鳞</a> <font color="#008000"><b>8%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="轰龙的头壳">轰龙的头壳</a> <font color="#008000"><b>7%</b></font>
</p>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴
</td><td> 1
</td><td>
<p><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="轰龙的尾巴">轰龙的尾巴</a> <font color="#008000"><b>80%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%B3%9E" title="轰龙的鳞">轰龙的鳞</a> <font color="#008000"><b>20%</b></font>
</p>
</td><td>
<p><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="轰龙的尾巴">轰龙的尾巴</a> <font color="#008000"><b>60%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="轰龙的上鳞">轰龙的上鳞</a> <font color="#008000"><b>40%</b></font>
</p>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 落とし物
</td><td>
</td><td>
<p><a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%B3%9E" title="轰龙的鳞">轰龙的鳞</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%88%AA" title="轰龙的爪">轰龙的爪</a> <font color="#008000"><b>5%</b></font>
</p>
</td><td>
<p><a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="轰龙的上鳞">轰龙的上鳞</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="轰龙的尖爪">轰龙的尖爪</a> <font color="#008000"><b>5%</b></font>
</p>
</td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%89%99" title="轰龙的牙">轰龙的牙</a>*2 <font color="#008000"><b>50%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="轰龙的甲壳">轰龙的甲壳</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="轰龙的头壳">轰龙的头壳</a> <font color="#008000"><b>12%</b></font> </td><td> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="轰龙的锐牙">轰龙的锐牙</a>*1 <font color="#008000"><b>45%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="轰龙的坚壳">轰龙的坚壳</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%89%99" title="轰龙的牙">轰龙的牙</a>*3 <font color="#008000"><b>15%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="轰龙的头壳">轰龙的头壳</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E4%B9%8B%E9%A2%9A" title="轰龙之颚">轰龙之颚</a>*1 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 爪 </td><td> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%88%AA" title="轰龙的爪">轰龙的爪</a>*2 <font color="#008000"><b>80%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%88%AA" title="轰龙的爪">轰龙的爪</a>*1 <font color="#008000"><b>20%</b></font> </td><td> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="轰龙的尖爪">轰龙的尖爪</a>*1 <font color="#008000"><b>80%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%88%AA" title="轰龙的爪">轰龙的爪</a>*3 <font color="#008000"><b>20%</b></font> </td><td>
</td></tr></tbody></table>

## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="轰龙的甲壳">轰龙的甲壳</a>*1 <font color="#008000"><b>36%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%89%99" title="轰龙的牙">轰龙的牙</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%B3%9E" title="轰龙的鳞">轰龙的鳞</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="轰龙的尾巴">轰龙的尾巴</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="轰龙的头壳">轰龙的头壳</a>*1 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="轰龙的坚壳">轰龙的坚壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="轰龙的锐牙">轰龙的锐牙</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="轰龙的上鳞">轰龙的上鳞</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="轰龙的甲壳">轰龙的甲壳</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="轰龙的尾巴">轰龙的尾巴</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E8%BD%B0%E9%BE%99%E4%B9%8B%E9%A2%9A" title="轰龙之颚">轰龙之颚</a> <font color="#008000"><b>7%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>轟竜迎撃戦
</p>
</td><td>
<p>讨伐一头轰龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>凍土に轟く咆哮
</p>
</td><td>
<p>讨伐一头轰龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>凍土の大合戦
</p>
</td><td>
<p>讨伐一头轰龙和一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>轟竜迎撃戦
</p>
</td><td>
<p>讨伐一头轰龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>凍土に轟く咆哮
</p>
</td><td>
<p>讨伐一头轰龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>砂原の大食い選手権
</p>
</td><td>
<p>讨伐一头轰龙和一头<a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99" title="潜口龙">潜口龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>砂原の大食い選手権
</p>
</td><td>
<p>讨伐一头轰龙和一头<a href="/%E6%BD%9C%E5%8F%A3%E9%BE%99" title="潜口龙">潜口龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>砂原戦線・轟きあり！
</p>
</td><td>
<p>讨伐一头轰龙和一头<a href="/%E9%BB%91%E8%BD%B0%E9%BE%99" title="黑轰龙">黑轰龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>凍土に轟く咆哮
</p>
</td><td>
<p>讨伐一头轰龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>狩人舞闘曲
</p>
</td><td>
<p>一头轰龙和一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>和一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>終焉を喰らう者
</p>
</td><td>
<p>一头<a href="/%E6%81%90%E6%9A%B4%E9%BE%99" title="恐暴龙">恐暴龙</a>和一头轰龙和一头<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 使用突进攻击猎人，有时突进后会直接转身锁定猎人继续一次突进
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续龙车 </td><td> 怒后使用的做多连续3次的突进
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 向前使用一次啃咬攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续啃咬 </td><td> 向前连续使用两次啃咬攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 360 </td><td> 原地旋转360°身体进行攻击，全身都有判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前进360 </td><td> 向前踏一大步后借助惯性使用会向前移动的360攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 推岩块 </td><td> 用前脚推出3块岩块攻击，同时尾巴也存在判定（砂原岩块附带水属性，冻土附带冰属性）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 使用飞扑攻击猎人，怒后能扑2次，附带风压【小】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】 </td><td> 发现猎人时使用的威吓攻击，属于高吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【特大】 </td><td> 以发现猎人的状态时使用的咆哮攻击，身下一定范围存在攻击判定，攻击范围以外的一定范围内存在高吼判定
</td></tr></tbody></table>
# 黑轟龍

# 迅龍
![](assets/迅龍.jpeg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">50 (55) </font></td><td> <font color="#ff0000">50 (55) </font></td><td> <font color="#ff0000">55 (60) </font></td><td> 20 </td><td> 10 </td><td> 25 (30) </td><td> 15 </td><td> 15 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 36 </td><td> 40 </td><td> 30 </td><td> 15 </td><td> 5 </td><td> 15 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 25 </td><td> 25 </td><td> 30 </td><td> 5 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 刃翼 </td><td> 20 </td><td> 20 </td><td> 20 </td><td> 25 </td><td> 5 </td><td> 35 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 左80/右80
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 30 </td><td> 30 </td><td> 25 </td><td> 5 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 30 </td><td> 30 </td><td> 35 </td><td> 10 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 36 </td><td> 36 </td><td> 25 </td><td> 5 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 25 </td><td> 25 </td><td> 30 </td><td> 25 </td><td> 5 </td><td> 30 </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr></tbody></table>

## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 200 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 110 (620) </td><td> 150 (780) </td><td> 150 (780) </td><td> 150 (800) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 20秒 </td><td> 10秒 </td><td> 100秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>

## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> △ </td><td> 初回20秒 → 2回10秒 → 3回开始8秒 （愤怒时才有效）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> △ </td><td> 连续飞扑前用倒地5秒后发怒并出现掉落物（发怒有效），平时使用硬直后发怒，疲劳时无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★5（稍微容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>

## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td>
<p>3
</p>
</td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%B3%9E" title="迅龙的鳞">迅龙的鳞</a> <font color="#008000"><b>45%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%BB%91%E6%AF%9B" title="迅龙的黑毛">迅龙的黑毛</a> <font color="#008000"><b>25%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E7%89%99" title="迅龙的牙">迅龙的牙</a> <font color="#008000"><b>15%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%88%83%E7%BF%BC" title="迅龙的刃翼">迅龙的刃翼</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="迅龙的尾巴">迅龙的尾巴</a> <font color="#008000"><b>5%</b></font><br>
</td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="迅龙的上鳞">迅龙的上鳞</a> <font color="#008000"><b>40%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%BB%91%E6%AF%9B" title="迅龙的上黑毛">迅龙的上黑毛</a> <font color="#008000"><b>28%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="迅龙的锐牙">迅龙的锐牙</a> <font color="#008000"><b>12%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%B3%9E" title="迅龙的鳞">迅龙的鳞</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E5%88%83%E7%BF%BC" title="迅龙的锐刃翼">迅龙的锐刃翼</a> <font color="#008000"><b>8%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a> <font color="#008000"><b>2%</b></font><br>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴
</td><td>
<p>1
</p>
</td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="迅龙的尾巴">迅龙的尾巴</a> <font color="#008000"><b>82%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E6%A3%98" title="迅龙的尾棘">迅龙的尾棘</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="迅龙的骨髓">迅龙的骨髓</a> <font color="#008000"><b>8%</b></font><br>
</td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="迅龙的尾巴">迅龙的尾巴</a> <font color="#008000"><b>72%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E6%A3%98" title="迅龙的尾棘">迅龙的尾棘</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="迅龙的骨髓">迅龙的骨髓</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a> <font color="#008000"><b>8%</b></font><br>
</td><td> 攻击尾巴出4次硬直？
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物
</td><td>
<p>1
</p>
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%B3%9E" title="迅龙的鳞">迅龙的鳞</a> <font color="#008000"><b>25%</b></font><br>
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font> <br> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="迅龙的上鳞">迅龙的上鳞</a> <font color="#008000"><b>25%</b></font><br>
</td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E7%89%99" title="迅龙的牙">迅龙的牙</a>*2 <font color="#008000"><b>70%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%B3%9E" title="迅龙的鳞">迅龙的鳞</a>*2 <font color="#008000"><b>30%</b></font> </td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="迅龙的锐牙">迅龙的锐牙</a>*2 <font color="#008000"><b>65%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="迅龙的上鳞">迅龙的上鳞</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E7%89%99" title="迅龙的牙">迅龙的牙</a>*4 <font color="#008000"><b>7%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%88%83%E7%BF%BC" title="迅龙的刃翼">迅龙的刃翼</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%88%83%E7%BF%BC" title="迅龙的刃翼">迅龙的刃翼</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%BB%91%E6%AF%9B" title="迅龙的黑毛">迅龙的黑毛</a>*2 <font color="#008000"><b>15%</b></font> </td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E5%88%83%E7%BF%BC" title="迅龙的锐刃翼">迅龙的锐刃翼</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E5%88%83%E7%BF%BC" title="迅龙的锐刃翼">迅龙的锐刃翼</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%BB%91%E6%AF%9B" title="迅龙的上黑毛">迅龙的上黑毛</a>*2 <font color="#008000"><b>12%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%88%83%E7%BF%BC" title="迅龙的刃翼">迅龙的刃翼</a>*3 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%BB%91%E6%AF%9B" title="迅龙的黑毛">迅龙的黑毛</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E7%89%99" title="迅龙的牙">迅龙的牙</a>*1 <font color="#008000"><b>23%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%B3%9E" title="迅龙的鳞">迅龙的鳞</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="迅龙的骨髓">迅龙的骨髓</a>*1 <font color="#008000"><b>4%</b></font> </td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%BB%91%E6%AF%9B" title="迅龙的上黑毛">迅龙的上黑毛</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="迅龙的锐牙">迅龙的锐牙</a>*1 <font color="#008000"><b>21%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="迅龙的上鳞">迅龙的上鳞</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="迅龙的尾巴">迅龙的尾巴</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%BB%91%E6%AF%9B" title="迅龙的黑毛">迅龙的黑毛</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>紅煌流星
</p>
</td><td>
<p>讨伐一头迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>漆黒の影
</p>
</td><td>
<p>讨伐一头迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>終焉を喰らう者
</p>
</td><td>
<p>一头<a href="/%E6%81%90%E6%9A%B4%E9%BE%99" title="恐暴龙">恐暴龙</a>、一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>、一头迅龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>迅速?雷光?太刀ふさぐ壁
</p>
</td><td>
<p>讨伐一头迅龙和一头<a href="/%E9%9B%B7%E7%8B%BC%E9%BE%99" title="雷狼龙">雷狼龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>紅煌流星
</p>
</td><td>
<p>讨伐一头迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>ブルファンゴたちの大集会
</p>
</td><td>
<p>讨伐20头<a href="/%E9%87%8E%E7%8C%AA" title="野猪">野猪</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>漆黒の影
</p>
</td><td>
<p>讨伐一头迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>漆黒の双風
</p>
</td><td>
<p>讨伐两头迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>森に棲むもの
</p>
</td><td>
<p>一头<a href="/%E9%9D%92%E7%86%8A%E5%85%BD" title="青熊兽">青熊兽</a>、一头<a href="/%E9%87%8E%E7%8C%AA%E7%8E%8B" title="野猪王">野猪王</a>、一头迅龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>暗夜のナルガクルガ
</p>
</td><td>
<p>讨伐一头迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>暗中?迅速?太刀打ちの影
</p>
</td><td>
<p>讨伐一头 迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>増援要求
</p>
</td><td>
<p>讨伐一头迅龙和一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>闇の中に影ふたつ
</p>
</td><td>
<p>讨伐一头迅龙和一头<a href="/%E7%BB%BF%E8%BF%85%E9%BE%99" title="绿迅龙">绿迅龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>集え！水没林の大連続戦
</p>
</td><td>
<p>一头迅龙、一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>、一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%A5%AE%E6%96%99%E4%BB%BB%E5%8A%A1" title="饮料任务">饮料任务</a>
</p>
</td><td>
<p>黒き殺意の奔流
</p>
</td><td>
<p>连续讨伐三头迅龙
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前咬 </td><td> 向前使用啃咬，整个前半身都有判定，前爪部分有擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 使用飞扑和刃翼攻击，可以连续多次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 斜一下身体后使用甩尾攻击，侧面和正面都存在攻击判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大回旋 </td><td> 用力旋转身体并使用360°的大范围甩尾
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾锤 </td><td> 背向猎人跃起后用尾巴猛地砸向地面
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前进 </td><td> 迅速爬向猎人，擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 侧跃 </td><td> 跃到侧方，可以派生其他很多招式
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼
</td></tr></tbody></table>

# 綠迅龍
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 (65) </font></td><td> <font color="#ff0000">60 (65) </font></td><td> <font color="#ff0000">65 (70) </font></td><td> 20 </td><td> 10 </td><td> 25 (30) </td><td> 15 </td><td> 15 </td><td> 100 (120) </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 35 </td><td> 35 </td><td> 30 </td><td> 15 </td><td> 5 </td><td> 15 (20) </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 25 </td><td> 25 </td><td> 30 </td><td> 5 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 240
</td></tr>
<tr style="background:#f9f9f9;">
<td> 刃翼 </td><td> 20 </td><td> 20 </td><td> 20 </td><td> 25 </td><td> 5 </td><td> 35 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 左80/右80
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 25 </td><td> 25 </td><td> 25 </td><td> 5 </td><td> 5 </td><td> 10 (15) </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 30 </td><td> 30 </td><td> 35 </td><td> 10 (15) </td><td> 0 </td><td> 15 (20) </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 35 </td><td> 30 </td><td> 25 </td><td> 5 (10) </td><td> 0 </td><td> 10 (15) </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 20 (25) </td><td> 20 (25) </td><td> 20 (30) </td><td> 25 </td><td> 5 </td><td> 30 </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 150
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 200 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 110 (620) </td><td> 150 (780) </td><td> 150 (780) </td><td> 150 (800) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 20秒 </td><td> 10秒 </td><td> 100秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> △ </td><td> 初回20秒 → 2回10秒 → 3回开始8秒 （愤怒时才有效）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> △ </td><td> 连续飞扑前用倒地5秒后发怒并出现掉落物（发怒有效），平时使用硬直后发怒，疲劳时无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★7（难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td>
<p>3
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="绿迅龙的上鳞">绿迅龙的上鳞</a> <font color="#008000"><b>45%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E6%96%91%E6%AF%9B" title="绿迅龙的上斑毛">绿迅龙的上斑毛</a> <font color="#008000"><b>28%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="绿迅龙的锐牙">绿迅龙的锐牙</a> <font color="#008000"><b>15%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E5%88%83%E7%BF%BC" title="绿迅龙的锐刃翼">绿迅龙的锐刃翼</a> <font color="#008000"><b>10%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a> <font color="#008000"><b>2%</b></font>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴
</td><td>
<p>1
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="迅龙的尾巴">迅龙的尾巴</a> <font color="#008000"><b>52%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E6%96%91%E6%AF%9B" title="绿迅龙的上斑毛">绿迅龙的上斑毛</a> <font color="#008000"><b>20%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E6%A3%98" title="迅龙的尾棘">迅龙的尾棘</a> <font color="#008000"><b>10%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="迅龙的骨髓">迅龙的骨髓</a> <font color="#008000"><b>10%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a> <font color="#008000"><b>8%</b></font>
</td><td> 攻击尾巴出4次硬直？
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物
</td><td>
<p>-
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="绿迅龙的上鳞">绿迅龙的上鳞</a> <font color="#008000"><b>25%</b></font>
</td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 下位无此怪物 </td><td> <a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="绿迅龙的锐牙">绿迅龙的锐牙</a>*2 <font color="#008000"><b>66%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="绿迅龙的上鳞">绿迅龙的上鳞</a>*2 <font color="#008000"><b>26%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a>*1 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> 下位无此怪物 </td><td> <a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E5%88%83%E7%BF%BC" title="绿迅龙的锐刃翼">绿迅龙的锐刃翼</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E5%88%83%E7%BF%BC" title="绿迅龙的锐刃翼">绿迅龙的锐刃翼</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E6%96%91%E6%AF%9B" title="绿迅龙的上斑毛">绿迅龙的上斑毛</a>*2 <font color="#008000"><b>15%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾棘 </td><td> 下位无此怪物 </td><td> <a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E6%A3%98" title="迅龙的尾棘">迅龙的尾棘</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E6%96%91%E6%AF%9B" title="绿迅龙的上斑毛">绿迅龙的上斑毛</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="迅龙的骨髓">迅龙的骨髓</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a>*1 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> <a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E6%96%91%E6%AF%9B" title="绿迅龙的上斑毛">绿迅龙的上斑毛</a>*1 <font color="#008000"><b>45%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E9%94%90%E7%89%99" title="绿迅龙的锐牙">绿迅龙的锐牙</a>*1 <font color="#008000"><b>21%</b></font><br><a href="/%E7%BB%BF%E8%BF%85%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="绿迅龙的上鳞">绿迅龙的上鳞</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="迅龙的尾巴">迅龙的尾巴</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E8%BF%85%E9%BE%99%E7%9A%84%E5%BB%B6%E9%AB%93" title="迅龙的延髓">迅龙的延髓</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>疾風に緑迅を知る
</p>
</td><td>
<p>讨伐一头绿迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>暗中・迅速・太刀打ちの影
</p>
</td><td>
<p>讨伐一头绿迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>集え！孤島の大連続戦
</p>
</td><td>
<p>一头<a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>、两头<a href="/%E9%9D%92%E7%86%8A%E5%85%BD" title="青熊兽">青熊兽</a>、一头绿迅龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>闇の中に影ふたつ
</p>
</td><td>
<p>讨伐一头<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>和一头绿迅龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>フラッシュフラッド
</p>
</td><td>
<p>讨伐一头绿迅龙和一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前咬 </td><td> 向前使用啃咬，整个前半身都有判定，前爪部分有擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 使用飞扑和刃翼攻击，可以连续多次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 斜一下身体后使用甩尾攻击，侧面和正面都存在攻击判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大回旋 </td><td> 用力旋转身体并使用360°的大范围甩尾
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾锤 </td><td> 背向猎人跃起后用尾巴猛地砸向地面，能连续砸2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前进 </td><td> 迅速爬向猎人，擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 侧跃 </td><td> 跃到侧方，能连续跃2次，并可以派生其他很多招式
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼
</td></tr></tbody></table>

# 毒怪龍
![](assets/毒怪龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>头(非发怒状态)<br>腹部<br>尾巴(发怒状态)</center></div>
</td><td><div><center>头(非发怒状态)<br>腹部<br>尾巴(发怒状态)</center></div>
</td><td><div><center>头(非发怒状态)<br>腹部<br>尾巴(发怒状态)</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">45</font> (19) </td><td> <font color="#ff0000">55</font> (19) </td><td> <font color="#ff0000">50</font> (10) </td><td> 30 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 20 </td><td> 100 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 40 (24) </td><td> 40 (25) </td><td> 40 (15) </td><td> 25 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 35 </td><td> 30 </td><td> 25 </td><td> 20 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 30 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 左130/右130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 40 </td><td> 40 </td><td> 35 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 左100/右100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 19 <font color="#ff0000">(50) </font></td><td> 19 <font color="#ff0000">(55) </font></td><td> 10 <font color="#ff0000">(50) </font></td><td> 30 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 180
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 240 </td><td> 180 </td><td> 180 </td><td> 200 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 110 (620) </td><td> 150 (780) </td><td> 150 (780) </td><td> 150 (800) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 30秒 </td><td> 10秒 </td><td> 40秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 75 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★4（容易逃） 摆脱束缚攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td> 3
</td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒怪龙的皮">毒怪龙的皮</a> <font color="#008000"><b>38%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a> <font color="#008000"><b>21%</b></font><br> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%88%AA" title="毒怪龙的爪">毒怪龙的爪</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E6%AF%92%E8%85%BA" title="可怕的毒腺">可怕的毒腺</a> <font color="#008000"><b>12%</b></font> <br> <a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a> <font color="#008000"><b>9%</b></font>
</td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒怪龙的上皮">毒怪龙的上皮</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="毒怪龙的锐爪">毒怪龙的锐爪</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%8C%9B%E6%AF%92%E8%85%BA" title="可怕的猛毒腺">可怕的猛毒腺</a> <font color="#008000"><b>10%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a> <font color="#008000"><b>7%</b></font>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物
</td><td> -
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br> <a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a> <font color="#008000"><b>5%</b></font>
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br> <a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a> <font color="#008000"><b>17%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a> <font color="#008000"><b>8%</b></font>
</td><td> 腹部的部位破坏时掉落可能
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头(毒腺) </td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒怪龙的皮">毒怪龙的皮</a>*1 <font color="#008000"><b>41%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a>*1 <font color="#008000"><b>12%</b></font> </td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒怪龙的上皮">毒怪龙的上皮</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒怪龙的皮">毒怪龙的皮</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 头部打出两次硬直后，破
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹(毒腺) </td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒怪龙的皮">毒怪龙的皮</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E6%AF%92%E8%85%BA" title="可怕的毒腺">可怕的毒腺</a>*1 <font color="#008000"><b>36%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a>*1 <font color="#008000"><b>24%</b></font> </td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒怪龙的上皮">毒怪龙的上皮</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%8C%9B%E6%AF%92%E8%85%BA" title="可怕的猛毒腺">可怕的猛毒腺</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>14%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E6%AF%92%E8%85%BA" title="可怕的毒腺">可怕的毒腺</a>*2 <font color="#008000"><b>14%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>12%</b></font> </td><td> 腹部打出一次硬直后，破
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴(毒腺) </td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒怪龙的皮">毒怪龙的皮</a>*1 <font color="#008000"><b>41%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E6%AF%92%E8%85%BA" title="可怕的毒腺">可怕的毒腺</a>*1 <font color="#008000"><b>36%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a>*1 <font color="#008000"><b>24%</b></font> </td><td> <a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒怪龙的上皮">毒怪龙的上皮</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%8C%9B%E6%AF%92%E8%85%BA" title="可怕的猛毒腺">可怕的猛毒腺</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 尾巴打出一次硬直后，破
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E6%AF%92%E8%85%BA" title="可怕的毒腺">可怕的毒腺</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%88%AA" title="毒怪龙的爪">毒怪龙的爪</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a>*1 <font color="#008000"><b>21%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>16%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a>*1 <font color="#008000"><b>8%</b></font>
</td><td> <a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%8C%9B%E6%AF%92%E8%85%BA" title="可怕的猛毒腺">可怕的猛毒腺</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="毒怪龙的锐爪">毒怪龙的锐爪</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>17%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>11%</b></font><br><a href="/%E6%AF%92%E6%80%AA%E9%BE%99%E7%9A%84%E7%88%AA" title="毒怪龙的爪">毒怪龙的爪</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E6%AF%92%E8%85%BA" title="可怕的毒腺">可怕的毒腺</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font>
</td><td> 体力%以下<br> 体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>凍土の大合戦
</p>
</td><td>
<p>讨伐一头毒怪龙和一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D2%E6%98%9F" title="集会所下位2星">集会所下位2星</a>
</p>
</td><td>
<p>暗闇にうごめく猛毒！
</p>
</td><td>
<p>讨伐一头毒怪龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>毒怪竜ギギネブラを追え！
</p>
</td><td>
<p>讨伐一头毒怪龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>極寒の地の採掘依頼
</p>
</td><td>
<p>缴纳8个<a href="/index.php?title=%E8%A1%80%E7%9F%B3&amp;action=edit&amp;redlink=1" class="new" title="血石（尚未撰写）">血石</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>ドクターの猛毒毒研究
</p>
</td><td>
<p>讨伐一头毒怪龙和一头<a href="/%E7%94%B5%E6%80%AA%E9%BE%99" title="电怪龙">电怪龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>凍土の採取ツアー
</p>
</td><td>
<p>缴纳<a href="/index.php?title=%E7%8C%AB%E5%AE%85%E5%88%B8&amp;action=edit&amp;redlink=1" class="new" title="猫宅券（尚未撰写）">猫宅券</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>暗闇にうごめく猛毒！
</p>
</td><td>
<p>讨伐一头毒怪龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>狩人舞闘曲
</p>
</td><td>
<p>一头毒怪龙、一头<a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99" title="尾槌龙">尾槌龙</a>、一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 朝猎人进行突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 2连啃咬 </td><td> 伸长脖子依次向右和左方向使用啃咬，第二下范围较广，尾部也能使用这种攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 跳起使用飞扑攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 喷毒 </td><td> 全身喷出剧毒气体，被打中还会陷入中毒异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 吐毒 </td><td> 向前吐出一堆毒液，落地后还能存留一段时间，毒异常附加
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下压 </td><td> 直起并张开身体，然后压下地面攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 产毒（巢） </td><td> 从尾部产出一颗毒爆弹或巢：毒爆弹会定时爆炸，附带毒属性；巢会产出小毒龙攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 跳跃 </td><td> 分转身跳跃和后跳，落地有伤害判定并附带风压【小】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后跳吐毒 </td><td> 后跳同时像地面吐出毒液，能存留一段时间，毒异常附加
</td></tr>
<tr style="background:#f9f9f9;">
<td> 上墙 </td><td> 跳到墙或天花板上，能在那里自由爬行
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑（上墙） </td><td> 上墙后突然扑下来攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食 </td><td> 使用飞扑扑倒猎人，然后进入捕食状态 ，没来得及逃脱的话最后还会接毒液攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食（上墙） </td><td> 用尾巴吸住天花板，倒吊这身体，然后伸长脖子回卷攻击，然后进入捕食状态 ，没来得及逃脱的话最后还会接毒液攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】 </td><td> 使用吼叫威吓猎人，属于高吼
</td></tr></tbody></table>

# 電怪龍
![](assets/電怪龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>头(非发怒状态)<br>腹部<br>尾巴(发怒状态)</center></div>
</td><td><div><center>头(非发怒状态)<br>腹部<br>尾巴(发怒状态)</center></div>
</td><td><div><center>头(非发怒状态)<br>腹部<br>尾巴(发怒状态)</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">45</font> (19) </td><td> <font color="#ff0000">55</font> (19) </td><td> <font color="#ff0000">50</font> (10) </td><td> 5 </td><td> 30 </td><td> 0 </td><td> 0 </td><td> 20 </td><td> 100 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 40 (24) </td><td> 40 (25) </td><td> 40 (15) </td><td> 5 </td><td> 25 </td><td> 0 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 35 </td><td> 30 </td><td> 25 </td><td> 5 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 5 </td><td> 30 </td><td> 0 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 左130/右130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 40 </td><td> 40 </td><td> 35 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 左100/右100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 19 <font color="#ff0000">(50) </font></td><td> 19 <font color="#ff0000">(55) </font></td><td> 10 <font color="#ff0000">(50) </font></td><td> 5 </td><td> 30 </td><td> 0 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 180
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 200 </td><td> 240 </td><td> 200 </td><td> 180 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (700) </td><td> 140 (940) </td><td> 120 (800) </td><td> 100 (580) </td><td> 100 (580)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 90秒 </td><td> 8秒 </td><td> 40秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 225 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回6秒（疲劳12秒） → 2回5秒（疲劳7秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃） 摆脱束缚攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td>
<p>3
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E7%94%B5%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="电怪龙的上皮">电怪龙的上皮</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E7%94%B5%E6%80%AA%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="电怪龙的锐爪">电怪龙的锐爪</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a> <font color="#008000"><b>15%</b></font><br> <a href="/%E7%94%B5%E5%87%BB%E8%A2%8B" title="电击袋">电击袋</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a> <font color="#008000"><b>7%</b></font><br>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 落とし物
</td><td>
<p>-
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br> <a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a> <font color="#008000"><b>17%</b></font><br> <a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a> <font color="#008000"><b>8%</b></font><br>
</td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头(发光纹) </td><td> 下位无此怪 </td><td> <a href="/%E7%94%B5%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="电怪龙的上皮">电怪龙的上皮</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a> <font color="#008000"><b>12%</b></font> </td><td> 头部打出两次硬直后，破
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹(发光纹) </td><td> 下位无此怪 </td><td> <a href="/%E7%94%B5%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="电怪龙的上皮">电怪龙的上皮</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>28%</b></font><br><a href="/%E7%94%B5%E5%87%BB%E8%A2%8B" title="电击袋">电击袋</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>12%</b></font> </td><td> 腹部打出一次硬直后，破
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴(发光纹) </td><td> 下位无此怪 </td><td> <a href="/%E7%94%B5%E6%80%AA%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="电怪龙的上皮">电怪龙的上皮</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E7%94%B5%E5%87%BB%E8%A2%8B" title="电击袋">电击袋</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>22%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E7%9A%AE" title="可怕的皮">可怕的皮</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 尾巴打出一次硬直后，破
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物
</td><td> <a href="/%E7%94%B5%E5%87%BB%E8%A2%8B" title="电击袋">电击袋</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E7%94%B5%E6%80%AA%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="电怪龙的锐爪">电怪龙的锐爪</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%8F%AF%E6%80%95%E7%9A%84%E4%B8%8A%E7%9A%AE" title="可怕的上皮">可怕的上皮</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%99%BD%E5%8C%96%E6%B5%B8%E5%87%BA%E7%89%A9" title="白化浸出物">白化浸出物</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E6%81%90%E6%80%96%E7%9A%84%E5%98%B4" title="恐怖的嘴">恐怖的嘴</a>*1 <font color="#008000"><b>7%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font>
</td><td> 体力%以下<br> 体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>ドクターの猛毒毒研究
</p>
</td><td>
<p>讨伐一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>和一头电怪龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>ビリビリするらしいです
</p>
</td><td>
<p>讨伐一头电怪龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>地獄の大雪合戦
</p>
</td><td>
<p>一头<a href="/%E5%86%B0%E7%89%99%E9%BE%99" title="冰牙龙">冰牙龙</a>、一头<a href="/%E7%99%BD%E5%85%94%E5%85%BD" title="白兔兽">白兔兽</a>、一头电怪龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>ハンターからの依頼
</p>
</td><td>
<p>讨伐一头电怪龙和一头<a href="/%E5%86%BB%E6%88%88%E9%BE%99" title="冻戈龙">冻戈龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>選ばれし者とは
</p>
</td><td>
<p>一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>、一头电怪龙、一头<a href="/%E7%82%8E%E6%88%88%E9%BE%99" title="炎戈龙">炎戈龙</a>、一头<a href="/%E5%86%B0%E7%A2%8E%E9%BE%99" title="冰碎龙">冰碎龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 朝猎人进行突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 2连啃咬or甩尾 </td><td> 伸长脖子依次左右方向使用啃咬，第二下范围较广，尾部也能使用这种攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑 </td><td> 跳起使用飞扑攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 放电 </td><td> 全身瞬间放出电击，麻痹异常附加
</td></tr>
<tr style="background:#f9f9f9;">
<td> 雷球 </td><td> 向前吐出一颗雷球，落地后分成3方向3颗，麻痹异常附加
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下压 </td><td> 直起并张开身体，然后压下地面攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 雷爆弹 </td><td> 从尾部产出一颗雷爆弹，会定时爆炸，附带麻痹属性
</td></tr>
<tr style="background:#f9f9f9;">
<td> 跳跃 </td><td> 分转身跳跃和后跳，落地有伤害判定并附带风压【小】
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后跳雷球 </td><td> 后跳同时像地面吐出雷球，落地后分成3方向3颗，麻痹异常附加
</td></tr>
<tr style="background:#f9f9f9;">
<td> 上墙 </td><td> 跳到墙或天花板上，能在那里自由爬行
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞扑（上墙） </td><td> 上墙后突然扑下来攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食 </td><td> 使用飞扑扑倒猎人，然后进入捕食状态 ，没来得及逃脱的话最后还会接麻痹攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食（上墙） </td><td> 用尾巴吸住天花板，倒吊这身体，然后伸长脖子回卷攻击，然后进入捕食状态 ，没来得及逃脱的话最后还会接麻痹攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】 </td><td> 使用吼叫威吓猎人，属于高吼
</td></tr></tbody></table>
# 冰牙龍
![](assets/冰牙龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头<br>腹部</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">65 </font></td><td> <font color="#ff0000">70 </font></td><td> 30 </td><td> 10 </td><td> 25 </td><td> 0 </td><td> 15 </td><td> 100 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 30 </td><td> 25 </td><td> 30 </td><td> 5 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">45 </font></td><td> 25 </td><td> 40 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> 25 </td><td> 30 </td><td> 25 </td><td> 30 </td><td> 5 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 30 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 35 </td><td> 35 </td><td> 35 </td><td> 10 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 40 </td><td> 40 </td><td> 30 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 130 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 150 (780) </td><td> 150 (780) </td><td> 150 (780) </td><td> 130 (650) </td><td> 75 (480)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/15秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★5（稍微容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> △ </td><td> 与猎人同区且没有发现猎人时会吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td>
<p>3
</p>
</td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="冰牙龙的甲壳">冰牙龙的甲壳</a> <font color="#008000"><b>43%</b></font><br> <a href="/%E5%86%B0%E7%BB%93%E8%A2%8B" title="冰结袋">冰结袋</a> <font color="#008000"><b>28%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%AF%9B%E7%9A%AE" title="冰牙龙的毛皮">冰牙龙的毛皮</a> <font color="#008000"><b>19%</b></font><br> <a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E7%89%99" title="琥珀色的牙">琥珀色的牙</a> <font color="#008000"><b>10%</b></font><br>
</td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冰牙龙的坚壳">冰牙龙的坚壳</a> <font color="#008000"><b>35%</b></font><br> <a href="/%E5%86%BB%E7%BB%93%E8%A2%8B" title="冻结袋">冻结袋</a> <font color="#008000"><b>25%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E4%B8%8A%E6%AF%9B%E7%9A%AE" title="冰牙龙的上毛皮">冰牙龙的上毛皮</a> <font color="#008000"><b>18%</b></font><br> <a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E9%94%90%E7%89%99" title="琥珀色的锐牙">琥珀色的锐牙</a> <font color="#008000"><b>12%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%AF%9B%E7%9A%AE" title="冰牙龙的毛皮">冰牙龙的毛皮</a> <font color="#008000"><b>10%</b></font><br>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴
</td><td>
<p>1
</p>
</td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="冰牙龙的尾巴">冰牙龙的尾巴</a> <font color="#008000"><b>12%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="冰牙龙的甲壳">冰牙龙的甲壳</a> <font color="#008000"><b>24%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a> <font color="#008000"><b>16%</b></font><br>
</td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="冰牙龙的尾巴">冰牙龙的尾巴</a> <font color="#008000"><b>44%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冰牙龙的坚壳">冰牙龙的坚壳</a> <font color="#008000"><b>30%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a> <font color="#008000"><b>12%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="冰牙龙的甲壳">冰牙龙的甲壳</a> <font color="#008000"><b>10%</b></font><br> <a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>4%</b></font><br>
</td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物
</td><td>
<p>1
</p>
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a> <font color="#008000"><b>15%</b></font><br> <a href="/%E5%86%B0%E7%BB%93%E8%A2%8B" title="冰结袋">冰结袋</a> <font color="#008000"><b>10%</b></font><br>
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br> <a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br> <a href="/%E5%86%BB%E7%BB%93%E8%A2%8B" title="冻结袋">冻结袋</a> <font color="#008000"><b>20%</b></font><br> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a> <font color="#008000"><b>5%</b></font><br>
</td><td> 濒死或者疲劳移动时、空中急袭攻击后、部位破坏时几率掉落
</td></tr></tbody></table>

## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 牙 </td><td> <a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E7%89%99" title="琥珀色的牙">琥珀色的牙</a>*1 <font color="#008000"><b>55%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="冰牙龙的甲壳">冰牙龙的甲壳</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%86%B0%E7%BB%93%E8%A2%8B" title="冰结袋">冰结袋</a>*1 <font color="#008000"><b>20%</b></font> </td><td> <a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E9%94%90%E7%89%99" title="琥珀色的锐牙">琥珀色的锐牙</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冰牙龙的坚壳">冰牙龙的坚壳</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%86%B0%E7%BB%93%E8%A2%8B" title="冰结袋">冰结袋</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E7%89%99" title="琥珀色的牙">琥珀色的牙</a>*2 <font color="#008000"><b>10%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 棘 </td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%88%AA" title="冰牙龙的爪">冰牙龙的爪</a>*1 <font color="#008000"><b>78%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%AF%9B%E7%9A%AE" title="冰牙龙的毛皮">冰牙龙的毛皮</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冰牙龙的锐爪">冰牙龙的锐爪</a>*1 <font color="#008000"><b>74%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E4%B8%8A%E6%AF%9B%E7%9A%AE" title="冰牙龙的上毛皮">冰牙龙的上毛皮</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%88%AA" title="冰牙龙的爪">冰牙龙的爪</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a>*1 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%AF%9B%E7%9A%AE" title="冰牙龙的毛皮">冰牙龙的毛皮</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E5%86%B0%E7%BB%93%E8%A2%8B" title="冰结袋">冰结袋</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E7%89%99" title="琥珀色的牙">琥珀色的牙</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E7%88%AA" title="冰牙龙的爪">冰牙龙的爪</a>*1 <font color="#008000"><b>15%</b></font>
</td><td> <a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E4%B8%8A%E6%AF%9B%E7%9A%AE" title="冰牙龙的上毛皮">冰牙龙的上毛皮</a>*1 <font color="#008000"><b>26%</b></font><br><a href="/%E5%86%BB%E7%BB%93%E8%A2%8B" title="冻结袋">冻结袋</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%90%A5%E7%8F%80%E8%89%B2%E7%9A%84%E9%94%90%E7%89%99" title="琥珀色的锐牙">琥珀色的锐牙</a>*1 <font color="#008000"><b>16%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冰牙龙的锐爪">冰牙龙的锐爪</a>*1 <font color="#008000"><b>14%</b></font><br><a href="/%E5%86%B0%E7%89%99%E9%BE%99%E7%9A%84%E6%A3%98" title="冰牙龙的棘">冰牙龙的棘</a>*2 <font color="#008000"><b>12%</b></font><br><a href="/%E5%86%B0%E7%BB%93%E8%A2%8B" title="冰结袋">冰结袋</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font>
</td><td> 体力%以下<br> 体力%以下(上位)
</td></tr></tbody></table>

## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>氷牙竜ベリオロス！
</p>
</td><td>
<p>讨伐一头冰牙龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>凍土に舞う白き風！
</p>
</td><td>
<p>讨伐一头冰牙龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>氷牙竜ベリオロス！
</p>
</td><td>
<p>讨伐一头冰牙龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>開催！ポポノタン品評会？
</p>
</td><td>
<p>缴纳5个<a href="/index.php?title=%E6%B3%A2%E6%B3%A2%E7%9A%84%E8%88%8C%E5%A4%B4&amp;action=edit&amp;redlink=1" class="new" title="波波的舌头（尚未撰写）">波波的舌头</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>疾風と猛毒
</p>
</td><td>
<p>讨伐一头冰牙龙和一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>氷牙獣ベリオロス！
</p>
</td><td>
<p>讨伐一头冰牙龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>凍土戦線?氷塊あり！
</p>
</td><td>
<p>讨伐一头冰牙龙和一头<a href="/%E5%86%B0%E7%A2%8E%E9%BE%99" title="冰碎龙">冰碎龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>地獄の大雪合戦
</p>
</td><td>
<p>一头冰牙龙、一头<a href="/%E7%99%BD%E5%85%94%E5%85%BD" title="白兔兽">白兔兽</a>、一头<a href="/%E7%94%B5%E6%80%AA%E9%BE%99" title="电怪龙">电怪龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 使用突进冲向猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身用身体撞击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前扑 </td><td> 小距离的前扑攻击，比较突然
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 再身体左侧或右侧进行的180°范围的甩尾攻击，附加冰异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 起飞 </td><td> 使自己进入低空状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 雪暴 </td><td> 吐出一颗雪球，落地后产生冰风暴，附加雪人异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 威吓 </td><td> 停在原地晃动尾巴并小吼一声，无任何判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下扑 </td><td> 低空状态时扑下来攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 雪暴（空） </td><td> 低空状态时朝地面吐出一颗雪球，落地后产生冰风暴，附加雪人异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 急袭 </td><td> 低空状态时先上升一定距离，然后急速俯冲下来攻击猎人
</td></tr></tbody></table>

# 風牙龍
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头<br>腹部</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">65 </font></td><td> <font color="#ff0000">70 </font></td><td> 0 </td><td> 15 </td><td> 25 </td><td> 30 </td><td> 10 </td><td> 100 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 30 </td><td> 25 </td><td> 30 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 5 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹 </td><td> <font color="#ff0000">45 </font></td><td> 25 </td><td> 40 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> 25 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 20 </td><td> 25 </td><td> 30 </td><td> 10 </td><td> 0 </td><td> 左200/右200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前脚 </td><td> 30 </td><td> 30 </td><td> 30 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 15 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 35 </td><td> 35 </td><td> 35 </td><td> 0 </td><td> 5 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 左180/右180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 40 </td><td> 40 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 10 </td><td> 15 </td><td> 5 </td><td> 0 </td><td> 200
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 130 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 150 (780) </td><td> 150 (780) </td><td> 150 (780) </td><td> 130 (650) </td><td> 75 (450)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/15秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★7（难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> △ </td><td> 与猎人同区且没有发现猎人时会吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体
</td><td>
<p>3
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="风牙龙的坚壳">风牙龙的坚壳</a> <font color="#008000"><b>43%</b></font><br><a href="/%E5%BC%BA%E9%9F%A7%E7%9A%84%E8%83%B8%E8%86%9C" title="强韧的胸膜">强韧的胸膜</a> <font color="#008000"><b>25%</b></font><br><a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E4%B8%8A%E6%AF%9B%E7%9A%AE" title="风牙龙的上毛皮">风牙龙的上毛皮</a> <font color="#008000"><b>20%</b></font>]<br><a href="/%E7%BE%A4%E9%9D%92%E8%89%B2%E7%9A%84%E9%94%90%E7%89%99" title="群青色的锐牙">群青色的锐牙</a> <font color="#008000"><b>12%</b></font>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴
</td><td>
<p>1
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="风牙龙的尾巴">风牙龙的尾巴</a> <font color="#008000"><b>75%</b></font><br><a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="风牙龙的坚壳">风牙龙的坚壳</a> <font color="#008000"><b>25%</b></font>
</td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物
</td><td>
<p>1
</p>
</td><td> 下位无此怪物
</td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%BC%BA%E9%9F%A7%E7%9A%84%E8%83%B8%E8%86%9C" title="强韧的胸膜">强韧的胸膜</a> <font color="#008000"><b>25%</b></font>
</td><td>
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 牙 </td><td> 下位无此怪物 </td><td> <a href="/%E7%BE%A4%E9%9D%92%E8%89%B2%E7%9A%84%E9%94%90%E7%89%99" title="群青色的锐牙">群青色的锐牙</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="风牙龙的坚壳">风牙龙的坚壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E5%BC%BA%E9%9F%A7%E7%9A%84%E8%83%B8%E8%86%9C" title="强韧的胸膜">强韧的胸膜</a>*1 <font color="#008000"><b>20%</b></font>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 棘 </td><td> 下位无此怪物 </td><td> <a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="风牙龙的锐爪">风牙龙的锐爪</a>*1 <font color="#008000"><b>74%</b></font><br><a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E4%B8%8A%E6%AF%9B%E7%9A%AE" title="风牙龙的上毛皮">风牙龙的上毛皮</a> <font color="#008000"><b>26%</b></font>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物
</td><td> <a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E4%B8%8A%E6%AF%9B%E7%9A%AE" title="风牙龙的上毛皮">风牙龙的上毛皮</a>*1 <font color="#008000"><b>34%</b></font><br><a href="/%E5%BC%BA%E9%9F%A7%E7%9A%84%E8%83%B8%E8%86%9C" title="强韧的胸膜">强韧的胸膜</a> <font color="#008000"><b>26%</b></font><br><a href="/%E7%BE%A4%E9%9D%92%E8%89%B2%E7%9A%84%E9%94%90%E7%89%99" title="群青色的锐牙">群青色的锐牙</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%A3%8E%E7%89%99%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="风牙龙的锐爪">风牙龙的锐爪</a> <font color="#008000"><b>16%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>4%</b></font>
</td><td> 体力%以下<br> 体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>砂原で発生、風牙竜巻
</p>
</td><td>
<p>讨伐一头风牙龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>今そこにある恐怖
</p>
</td><td>
<p>一头<a href="/%E5%9C%9F%E7%A0%82%E9%BE%99" title="土砂龙">土砂龙</a>、一头<a href="/%E8%B5%A4%E7%94%B2%E5%85%BD" title="赤甲兽">赤甲兽</a>、一头风牙龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>砂塵の牙、地殻の角
</p>
</td><td>
<p>讨伐一头风牙龙和一头<a href="/%E9%BB%91%E8%A7%92%E9%BE%99" title="黑角龙">黑角龙</a>
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 使用突进冲向猎人，可以接侧跃后继续突进，最多连续3次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身用身体撞击，撞玩还可能会修正站位
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前扑 </td><td> 小距离的前扑攻击，比较突然
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 再身体左侧或右侧进行的180°范围的甩尾攻击，附加冰异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 起飞 </td><td> 使自己进入低空状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 砂暴 </td><td> 吐出一颗砂球，落地后产生砂风暴，能持续较长时间
</td></tr>
<tr style="background:#f9f9f9;">
<td> 威吓 </td><td> 停在原地晃动尾巴并小吼一声，无任何判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下扑 </td><td> 低空状态时扑下来攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 砂暴（空） </td><td> 低空状态时朝地面吐出一颗雪球，落地后产生砂风暴，能持续较长时间
</td></tr>
<tr style="background:#f9f9f9;">
<td> 急袭 </td><td> 低空状态时先上升一定距离，然后急速俯冲下来攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 回旋升空&amp;急袭 </td><td> 砂风暴存在时移动向风暴，借助气流回旋上升后急速俯冲下来猛地攻击猎人
</td></tr></tbody></table>
# 霸龍

# 崩龍
