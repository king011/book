# boolean
布爾 取值 **true false**
```ts
let isDone: boolean = false;
```

# number
所有數字 都是 number 包括 了 整型和浮點

ts 支持多種 數字 寫法 包括
* 十進制 
* 十六進制 **0x**
* 八進制 **0o**
* 二進制 **0b**

```ts
// 10 進制寫法 1911-10-10 (19111010)
let decLiteral: number = 19111010;

// 16 進制寫法 1911-10-10
let hexLiteral: number = 0x1239C62;

// 2 進制寫法 1911-10-10
let binaryLiteral: number = 0b1001000111001110001100010;
// 8 進制寫法 1911-10-10
let octalLiteral: number = 0o110716142;

console.log(decLiteral, hexLiteral, binaryLiteral, octalLiteral);
```

# string
ts 的字符串 寫法 同 js 使用 **" '** 將其擴起來

```ts
let str: string = "cerberus is an idea"
console.log(str == 'cerberus is an idea')
```

此外 ts 字符串 還 支持 es6 的模板 表達式 使用 **\`** 將內容 擴起來 使用 **$\{expr\}&nbsp;** 嵌入 表達式

```ts
let persion = "kate"
let lv = 10
console.log(`${persion} is beauty ${lv + 1}`)
```

# Array
ts 支持 兩種 數組定義形式
1. 型別\[\]
2. Array&lt;型別&gt;

```ts
let list0: number[] = [1, 2, 3]
console.log(list0)
 
// 另外一種寫法
let list1: Array<string> = ["kate", "anna", "anita"]
console.log(list1)
 
let list2: (number | string)[] = [1, 2, "ok"]
console.log(list2)
 
let list3: Array<string | number> = ["kate", "anna", 10]
console.log(list3)
```

# Tuple
元組 用來 表示 已知 大小的 數組
```ts
const tuple: [string | number, number] = [1, 2]
// 其實就是 數組 
// 對於 超過 未定義的 索引 型別 使用 已定義型別的 聯合型別
tuple.push(3)
tuple[0] = "kate"
console.log(tuple.length) // 3
console.log(tuple) // [ 'kate', 2, 3 ]
```

# enum
使用 enum 定義 枚舉 和大多數 語言 一樣
```ts
// Red 下標 從 0 開始
enum Color {Red, Green, Blue}
let c: Color = Color.Green;
```
```ts
enum Color {Red = 1, Green, Blue}
let c: Color = Color.Green;
```
```
enum Color {Red = 1, Green = 2, Blue = 4}
let c: Color = Color.Green;
```
ts 的 枚舉 通過索 可以 獲得 枚舉的 名稱 字符串
```ts
enum Color {Red, Green, Blue}
const c: Color = Color.Green
const str:string = Color[1]
console.log(c,str == "Green") //1 true
```

# any
ts 會在編譯時 檢測 數據型別 使用 any 可以移除編譯時的 型別 檢測

你可以 將 任何東西 儲存到 any 並且 可以在 any上 調用 任何方法\(當然 這只會通過編譯 運行時調用了 不存在的 方法 js會 拋出異常\)

```ts
let a: any = {}
try {
    a.speak()
} catch (e) {
    console.log(e)
}
```

# void
void 通常 在 函數中 來表示 一個 函數 沒有 返回 值 其值可以被 賦值爲 **undefined**
```ts
function speak():void{
    console.log("cerberus is an idea")
}
let v:void = speak();
v = undefined;
```

# null undefined
通常 null 和 undefined 兩個型別 只能 賦值給 本身 undefined 還能賦值給 void

如果 數組 strictNullChecks:false 則 null 和 undefined 是所有型別的 子類 所以 可以 賦值給 任何 型別 \(通常 這會導致很多意料之外的問題 不建議 設置 false\)
```ts
let u: undefined = undefined;
let n: null = null;
```

# never
never 通常用來表示 函數 永遠不會 返回 比如 throw 異常 會 不會退出
```ts
function ThrowError(str): never {
    throw new Error(str)
}
function Busy(): never {
    while (true) {
 
    }
}
```

# 型別斷言
ts 會 自動推導 數據 型別 ide 也會 爲 型別 提供 提示

然 自動推導的型別 可以和 你的 意圖不同 此時 可以使用 型別 斷言 明確的 告訴 ts 數據型別

ts 支持 如下兩種 斷言 寫法
1. &lt;型別&gt;someValue
2. someValue as 型別

```ts
let str: any = "cerberus is an idea";
console.log((<string>str).length)
console.log((str as string).length)
```