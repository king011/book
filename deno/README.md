# deno

Deno 是 Ryan Dahl 創建的一個開源(MIT) javascript 運行環境，以彌補它在 Node.js 彙總感到後悔的一些問題

deno 內置的 tsc 編譯器可以直接執行 typescript，並且可以從 url 導入包，此外官方還移植了 golang 部分標準庫作爲 deno 的標準庫，故雖然目前(2022年) deno 生態尚未成熟，但本喵覺得可以用它來替代 bash/python 將是一個不錯的選擇

* 官網 [https://deno.land/](https://deno.land/)
* 源碼 [https://github.com/denoland/deno](https://github.com/denoland/deno)