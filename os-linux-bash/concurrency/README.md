# 併發

bash script 不支持 併發 但 可以支持 子進程 可以創建多個 子進程 來實現 併發 功能

bash 提供了 多種 內置 功能 來 和 子進程 交互

# wait
使用 &amp; 將 子進程 在後臺運行 便可以實現併發

wait 指令 可以 使當前 shell 等待一個 進程/job 直到其返回 

* $$ 記錄了 當前 進程 pid 
* $! 記錄了 最後運行的 後臺進程 pid

```
#info="child.sh"
#/bin/bash

for ((x=0;x<10;x=x+1))
do
    sleep $1
    echo $$ : $x
done
echo exit child $$
exit $2
```

```
#info="main.sh"
#!/bin/bash

# 啓動 後臺進程 0
./child.sh 1 10 &
child_0=$!

# 啓動 後臺進程 1
./child.sh 0.1 1 &
child_1=$!


echo child_0 = $child_0
echo child_1 = $child_1

wait $child_1 # 等待 後臺進程
echo child_1 return $? # 獲取後臺進程 返回值
wait $child_0
echo child_0 return $?

echo exit main $$
```

wait 也支持 使用 job 編號 而非 pid

```
#!/bin/bash

./child.sh 0.1 0 &

wait %1

echo exit main $$
```

> wait 如果不指定 pid/job 則會 等待所有子進程 結束 但此時無法 通過 $? 來獲取 子進程的 返回值
> 

# kill

後臺進程不會 因爲前臺的 shell 停止 而結束 所以 如果 沒有 wait 則可能 需要 使用 kill 來來結束掉 後臺進程

```
#!/bin/bash

./child.sh 1 0 &

sleep 2.2

echo exit main $$
kill -SIGKILL %1
```