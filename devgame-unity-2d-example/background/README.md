# 添加背景

我們將使用下圖 作爲背景

![](assets/background.png)

在將此圖片導入項目後，將圖片資源拖到 Hierarchy 窗口的 Background 節點下 並將其重命名爲 Background1，記得將其坐標改爲 (0,0,0)

之後將 Background1 複製一份爲 Background2 並將 Background2 坐標改爲(20,0,0) 這樣你會發現 Background1 和 Background2 拼接的很好

![](assets/background2_in_place.png)

# 圖層

我們正在構建一個 2d世界，這意味所有圖片都在同一個深度0，在 unity5 之前我們可以修改對象的 Z 坐標來確定哪些對象在前哪些在後。

unity5 提供了圖層概念，"Sprite Renderer" 組件有一個設置 **Sorting Layer** 默認爲 **Default** 點擊可以創建圖層和設置 精靈所在的圖層，我們創建一些圖層並將背景 Background1 和 Background2 設置到 Background 圖層(越下面的圖層越後渲染，所以下圖最先渲染 Background 層 最後渲染 Default 層)

![](assets/sorting_layers.png)
![](assets/sorting_layers_add.png)
![](assets/sorting_layers_set.png)

# 添加背景元素

背景元素又被稱爲 props(道具，和電影中叫法類似)，這些元素不是用來改變玩法，而是用來增強視覺效果的

這裏有一些簡單的道具：

![](assets/platforms.png)

如你所見在一張圖片中包含了兩個平臺道具，這正是嘗試 unity 工具來切割精靈的機會

1. 將 **platforms.png** 導入 Sprites 檔案夾
2. 選擇 platforms 並在 **Inspector** 窗口將 **Sprite Mode** 改爲 **Multiple**

	![](assets/sprite_multiple.png)

3. 點擊 **Sprite Editor** 按鈕

	![](assets/sprite_editor.png)
	
	點擊 左上角的 **Slice** 按鈕可以快速自動完成分隔，將分隔好的道具分別命名爲 platform1 和 platform2
	
	
現在將 platform1 和 platform2 添加到場景的 Middleground 對象下並將 **Sprite Layer** 設置爲 **Platforms**

![](assets/adding_platforms.png)


# 預製

將設置好的 platform1 和 platform2 對象從 Hierarchy窗口拖到 資源窗口即可創建 預製 Prefabs

現在你可以將 Prefabs 拖動到 Hierarchy窗口來創建遊戲對象，你可以方一些到 Background Middleground Foreground 節點下

你可以修改它們的位置和縮放屬性，記得給較遠的 對象設置更小的 **Order in Layer** 值

![](assets/platform_background.png)
