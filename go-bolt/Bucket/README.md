# Bucket

Bucket 是bolt 的存儲桶 每個 Bucket 都有自己獨立的 key/value pair

Bucket 可以看作是傳統數據庫中的 表

```
package main
 
import (
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"time"
)
 
func main() {
	TableName := []byte("users")
 
	// 如果 不存在檔案 則 創建數據庫
	// 否則 打開數據庫
	db, e := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
	if e != nil {
		log.Fatal(e)
	}
	// 關閉 數據庫
	defer db.Close()
 
	e = db.Update(func(t *bolt.Tx) error {
		// 查詢 Bucket 是否存在
		b := t.Bucket(TableName)
		var e error
		if b != nil {
			// 刪除 已經存在的 Bucket
			e = t.DeleteBucket(TableName)
			if e != nil {
				return e
			}
		}
 
		// 創建 Bucket
		b, e = t.CreateBucket(TableName)
		//b, e = t.CreateBucketIfNotExists(TableName)
		if e != nil {
			return e
		}
 
		// 插入值
		for i := 0; i < 10; i++ {
			id, _ := b.NextSequence()
			key := []byte(fmt.Sprint(id))
			val := []byte(fmt.Sprintf("name-%v", id))
			// write
			e = b.Put(key, val)
			if e != nil {
				return e
			}
 
			// read
			tmp := b.Get(key)
			// 如果 不存在 則返回 nil
			if tmp == nil {
				log.Fatalln("fault bolt bug")
			} else if string(val) != string(tmp) {
				log.Fatalln("fault bolt bug")
			}
		}
		return nil
	})
	if e != nil {
		log.Fatal(e)
	}
}
```

# NextSequence
NextSequence 可以返回一個 唯一的 uint64 值作為 key

類似 數據庫的 自增，第一次調用會返回 1

在 Update中 不需要判斷 失敗 因為 NextSequence 只在 非 write 事務 和 數據庫已經關閉時 返回 錯誤 

bolt 數據按照 bytes 排序所以 可以利用這點 使用 NextSequence 和 BigEndian 來實現排序

```
package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"log"
	"math"
	"time"

	"github.com/boltdb/bolt"
)

func main() {
	db, e := bolt.Open(`a.db`, 0600, nil)
	if e != nil {
		log.Fatalln(e)
	}
	defer db.Close()
	last := time.Now()
	e = db.Update(func(t *bolt.Tx) error {
		e = t.DeleteBucket([]byte("session"))
		if e != nil {
			return e
		}
		bucket, e := t.CreateBucket([]byte("session"))
		if e != nil {
			return e
		}
		for i := 0; i < math.MaxUint16; i++ {
			id, _ := bucket.NextSequence()
			key := make([]byte, 8)
			val := make([]byte, 8)
			binary.BigEndian.PutUint64(key, id)
			binary.BigEndian.PutUint64(val, id+1)
			e = bucket.Put(key, val)
			if e != nil {
				return e
			}
		}
		return nil
	})
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`update`, time.Since(last))

	last = time.Now()
	e = db.View(func(t *bolt.Tx) error {
		bucket := t.Bucket([]byte(`session`))
		c := bucket.Cursor()
		var i uint64
		for k, v := c.First(); k != nil; k, v = c.Next() {
			k0 := binary.BigEndian.Uint64(k)
			v0 := binary.BigEndian.Uint64(v)
			if k0+1 != v0 {
				return fmt.Errorf(`not equal(%v,%v)`, k0, v0)
			}
			if i >= k0 {
				return errors.New(`not sorted`)
			}
			i = k0
		}
		fmt.Println(`count`, i)
		return nil
	})
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(`view`, time.Since(last))
}
```

# 嵌套 Bucket

在一個 Bucket 中 已可以 嵌套 另外一個 Bucket
API 同 bolt.Tx 的 Bucket 操作

