# global
global 用於保存 全局 變量 所有 全局變量 都是 global 的 屬性  
在 node 無法 直接 定義 全局變量 變量 最外層作用域只能到 模塊

一定要定義 全局變量 只能 global.XXX =
```js
global.alert = console.log
```

# 常用變量
```txt
//  當前腳本 所在檔案夾
__dirname

// 當前腳本 全路徑
__filename
```
# process
process 是一個全局變量 其包含了 當前進程相關信息
```txt
// pid
process.pid

// 進程參數 
// [0] = node
// [1] = xxx.js
// [2:] = params
process.argv

// stdout
process.stdout

// stdin
process.stdin

// 下次執行 事件循環時 調用 callback
process.nextTick(callback)

// 退出進程
process.exit([code])

// 當前平臺 linux
process.platform
```
## stdin
stdin 默認關閉  
如果要使用 需要 手動 打開 並編輯 事件響應
```js
"use strict"
global.alert=console.log
 
process.stdin.resume();
process.stdin.on('data',function(data){
	var str = data.toString();
	if(str == "exit\n"){
		process.exit();
	}
	process.stdout.write('read : ' + str);
	process.stdout.write('$>');
});
process.stdout.write('$>');
 
process.on('exit', (code) => {
	console.log(`About to exit with code: ${code}`);
});
```
> process.exit 和 c 的exit 不同 雖然會強制結束結束進程  
> 但 process.exit 會先 通知 所有 註冊了 process exit 事件的回調 

# console
全局變量 console 提供了控制檯的 標準輸出

```txt
//類似 printf 
//不過 多餘的 參數 會自動 以空格 隔開 輸出
//會在結尾 輸出 換行
console.log([data], [...])

//同 log 不過 輸出到 stderr
console.error([data], [...])

//同 error 不過同時會輸出 當前 棧信息
console.trace([data], [...])
```