# 透明代理

1. 實現一個 redir 程式 監聽一個端口
1. 使用 linux 提供 iptables 將所有 tcp 重定向到 redir 程式
1. 在 redir 中使用 getsockopt 函數 獲取到 原目標地址
1. redir 代理訪問 目標地址 完成 通信


# getsockopt

getsockopt 可以獲取到一個 socket 的各種信息

```c
#info="c"
#include <arpa/inet.h>
#include <linux/netfilter_ipv4.h>

#ifndef IP6T_SO_ORIGINAL_DST
#define IP6T_SO_ORIGINAL_DST 80
#endif

static int getdestaddr(int fd, struct sockaddr_storage *destaddr)
{
    socklen_t socklen = sizeof(*destaddr);
    int error = getsockopt(fd, SOL_IPV6, IP6T_SO_ORIGINAL_DST, destaddr, &socklen);
    if (error) { // Didn't find a proper way to detect IP version.
        error = getsockopt(fd, SOL_IP, SO_ORIGINAL_DST, destaddr, &socklen);
        if (error) {
            return -1;
        }
    }
    return 0;
}
```

```go
#info="go"
package sys

/*
#include <memory.h>
#include <arpa/inet.h>
#include <linux/netfilter_ipv4.h>

#ifndef IP6T_SO_ORIGINAL_DST
#define IP6T_SO_ORIGINAL_DST 80
#endif

static int getdestaddr(int fd, struct sockaddr_storage *destaddr)
{
    socklen_t socklen = sizeof(*destaddr);
    int error = getsockopt(fd, SOL_IPV6, IP6T_SO_ORIGINAL_DST, destaddr, &socklen);
    if (error) { // Didn not find a proper way to detect IP version.
        error = getsockopt(fd, SOL_IP, SO_ORIGINAL_DST, destaddr, &socklen);
        if (error) {
            return -1;
        }
    }
    return 0;
}
static int native_getdestaddr(int fd,char* ip,int* n,in_port_t* port)
{
	struct sockaddr_storage addr;
	int error = getdestaddr(fd,&addr);
	if(error){
		return -1;
	}
	if(addr.ss_family == AF_INET6){
		*n = 16;
        struct sockaddr_in6 *addr_v6 = (struct sockaddr_in6 *)&addr;
		memcpy(ip,&(addr_v6->sin6_addr),16);
		*port = ntohs(addr_v6->sin6_port);
    }else{
		*n = 4;
		struct sockaddr_in *addr_v4 = (struct sockaddr_in *)&addr;
		memcpy(ip,&(addr_v4->sin_addr),4);
		*port = ntohs(addr_v4->sin_port);
    }
	return 0;
}
*/
import "C"
import "net"
import "unsafe"

// GetDestAddr .
func GetDestAddr(fd int) *net.TCPAddr {
	var n int
	b := make([]byte, 16)
	var port uint16
	v := C.native_getdestaddr(
		C.int(fd),
		(*C.char)(unsafe.Pointer(&b[0])),
		(*C.int)(unsafe.Pointer(&n)),
		(*C.in_port_t)(unsafe.Pointer(&port)),
	)
	if v != 0 {
		return nil
	}
	var ip net.IP
	if v == 4 {
		ip = net.IPv4(b[0], b[1], b[2], b[3])
	} else {
		ip = net.IP(b[:n])
	}
	return &net.TCPAddr{
		IP:   ip,
		Port: int(port),
	}
}
```

# iptables
```sh
#info="開啓iptables tcp轉發功能"
# 檢查 是否啓用 網卡轉發 如果爲1 則已開啓
cat /proc/sys/net/ipv4/ip_forward
# 如果爲0 編輯 /etc/sysctl.conf 添加如下設置
net.ipv4.ip_forward=1
# 執行 如下指令使用 修改立刻生效
sysctl -p
```

```
#info="iptables 規則"
#!/bin/bash

# 設置 redir 監聽端口
Redir_Port=10900
# 設置 redir 訪問的 外部地址 以便放行
Redir_Dst=xxx.xxx.xxx.xxx

# 清空 iptables 規則
iptables -t nat -F
iptables -t filter -F
iptables -t mangle -F

# 創建 nat/tcp 轉發鏈 用於 轉發 tcp流
iptables -t nat -N SS-TCP

# 放行 到 redir 的tcp連接
iptables -A INPUT -p tcp -m state --state NEW -m tcp --dport $Redir_Port -j ACCEPT

# 放行 環回地址 保留地址 特殊地址
iptables -t nat -A SS-TCP -d 0/8 -j RETURN
iptables -t nat -A SS-TCP -d 127/8 -j RETURN
iptables -t nat -A SS-TCP -d 10/8 -j RETURN
iptables -t nat -A SS-TCP -d 169.254/16 -j RETURN
iptables -t nat -A SS-TCP -d 172.16/12 -j RETURN
iptables -t nat -A SS-TCP -d 192.168/16 -j RETURN
iptables -t nat -A SS-TCP -d 224/4 -j RETURN
iptables -t nat -A SS-TCP -d 240/4 -j RETURN

# 放行 發往 ss-server 服務器的 數據 需要將 -d 地址 改爲服務器 ip
iptables -t nat -A SS-TCP -d $Redir_Dst -j RETURN

# 重定向 tcp 數據包到 redir 監聽端口
iptables -t nat -A SS-TCP -p tcp -j REDIRECT --to-ports $Redir_Port

# 本機 tcp 數據包流向 SS-TCP 鏈
iptables -t nat -A OUTPUT -p tcp -j SS-TCP

# 內網 tcp 數據包流向 SS-TCP 鏈
iptables -t nat -A PREROUTING -p tcp -s 192.168/16 -j SS-TCP


# 內網數據包源 NAT
iptables -t nat -A POSTROUTING -s 192.168/16 -j MASQUERADE
```

