# [flex-grow](https://www.primefaces.org/primeflex/flexgrow)

flex-grow 用於指定元素在 flex 中的增長因子，默認值爲 0

| Class | Properties |
| -------- | -------- |
| flex-grow-0     | flex-grow: 0;     |
| flex-grow-1     | flex-grow: 1;     |

下面代碼只有第二項會隨 flex 增長

```
<div class="card">
    <div class="flex card-container blue-container overflow-hidden">
        <div class="flex-none flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">PrimeFlex</div>
        <div class="flex-grow-1 flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">PrimeFlex</div>
        <div class="flex-none flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">PrimeFlex</div>
    </div>
</div>
```

# [flex-shrink](https://www.primefaces.org/primeflex/flexshrink)

flex-shrink 用於指定元素在 flex 中的收縮因子，默認值爲 1

| Class | Properties |
| -------- | -------- |
| flex-shrink-0     | flex-shrink: 0;     |
| flex-shrink-1     | flex-shrink: 1;     |

下面代碼只有第二項會不隨 flex 收縮

```
<div class="card">
    <div class="flex card-container blue-container">
        <div class="flex-grow-1 flex-shrink-1 flex align-items-center justify-content-center bg-blue-500 font-bold text-white p-4 m-3 border-round">1</div>
        <div class="flex-shrink-0 flex align-items-center justify-content-center bg-blue-500 font-bold text-white p-4 m-3 border-round">shrink item</div>
        <div class="flex-grow-1 flex-shrink-1 flex align-items-center justify-content-center bg-blue-500 font-bold text-white p-4 m-3 border-round">3</div>
    </div>
</div>
```

# [flex](https://www.primefaces.org/primeflex/flex)

flex 用於同時指定 flex-grow flex-shrink flex-basis，默認值爲 0 1 auto

| Class | Properties |
| -------- | -------- |
| flex-initial     | flex: 0 1 auto;     |
| flex-1     | flex: 1 1 0%;     |
| flex-auto     | flex: 1 1 auto;     |
| flex-none     | flex: none;     |


## flex-initial

flex-initial 是元素的默認 flex 值，其不會增長，使用 1 作爲收縮因子

```
<div class="card">
    <div class="card-container blue-container overflow-hidden">
        <div class="flex">
            <div class="flex-initial flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">Prime</div>
            <div class="flex-initial flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">Prime and PrimeFlex</div>
        </div>
        <div class="flex">
            <div class="flex-initial flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">Prime</div>
            <div class="flex-initial flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">Prime and PrimeFlex</div>
            <div class="flex-initial flex align-items-center justify-content-center bg-blue-500 font-bold text-white m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
        </div>
    </div>
</div>
```

## flex-1

flex-1 將增長和收縮因子都設置爲 1，將元素初始化大小(flex-basis)設置爲 0% 所以這些元素大小一致且跟隨 flex 一起增長或收縮

```
<div class="card">
    <div class="card-container yellow-container overflow-hidden">
        <div class="flex">
            <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 px-5 py-3 border-round">Prime</div>
            <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 px-5 py-3 border-round">Prime and PrimeFlex</div>
            <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
        </div>
        <div class="flex">
            <div class="flex-1 flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 px-5 py-3 border-round">Prime</div>
            <div class="flex-1 flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 px-5 py-3 border-round">Prime and PrimeFlex</div>
            <div class="flex-1 flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
        </div>
    </div>
</div>
```

## flex-auto

flex-auto 將增長和收縮因子都設置爲 1，將元素初始化大小(flex-basis)設置爲 auto 所以這些元素跟隨 flex 一起增長或收縮，但它們各自的初始大小不同

```
<div class="card">
    <div class="card-container green-container overflow-hidden">
        <div class="flex">
            <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 px-5 py-3 border-round">Prime</div>
            <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 px-5 py-3 border-round">Prime and PrimeFlex</div>
            <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
        </div>
        <div class="flex">
            <div class="flex-auto flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 px-5 py-3 border-round">Prime</div>
            <div class="flex-auto flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 px-5 py-3 border-round">Prime and PrimeFlex</div>
            <div class="flex-auto flex align-items-center justify-content-center bg-green-500 font-bold text-white m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
        </div>
    </div>
</div>
```

## flex-none

flex-none 相當於設置 0 0 auto，所以元素不會改變大小，將保持初始的 auto 獲取的大小

```
<div class="card">
    <div class="card-container purple-container overflow-hidden">
        <div class="flex">
            <div class="flex-1 flex align-items-center justify-content-center bg-purple-500 font-bold text-white m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
            <div class="flex-none flex align-items-center justify-content-center bg-purple-500 font-bold text-white m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
            <div class="flex-1 flex align-items-center justify-content-center bg-purple-500 font-bold text-white m-2 px-5 py-3 border-round">Lorem ipsum dolor sit amet</div>
        </div>
    </div>
</div>
```
# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同