# nginx

nginx 是一個 c語言 實現的 高效 開源(BSD-like) 跨平臺 的 http 服務器 亦可作爲 反向代理 負載均衡 郵件代理(TLS/SSL STARTTLS SMTP/POP3/IMAP)

* 官網 [https://nginx.org/](https://nginx.org/)
* 源碼 [https://github.com/nginx/nginx](https://github.com/nginx/nginx)

# 環境
```sh
# 安裝 nginx
sudo apt install nginx -y

# 編輯配置檔案
sudo vi /etc/nginx/nginx.conf
```

> 雖然 nginx 可以運行在 windows 上  
> 然不要在 windows上 運行 nginx 在 windows上 效率並不理想  
> 而且不會有sb在windows上用 nginx 故其在windows上的穩定性 也是不可保證的
> 

# 常用命令
傳入 配置檔 啓動 nginx  
nginx -c /etc/nginx/nginx.conf

向 nginx master 發送一個 強制關閉 信號  
nginx -s stop
* stop	強制關閉
* quit	優雅退出 會等待 worker 完成當前 任務後退出
* reopen	重新運行
* reload	重載配置

```sh
king@king-Inspiron-N4010 ~ $ nginx -h
nginx version: nginx/1.10.3 (Ubuntu)
Usage: nginx [-?hvVtTq] [-s signal] [-c filename] [-p prefix] [-g directives]
 
Options:
  -?,-h         : this help
  -v            : show version and exit
  -V            : show version and configure options then exit
  -t            : test configuration and exit
  -T            : test configuration, dump it and exit
  -q            : suppress non-error messages during configuration testing
  -s signal     : send signal to a master process: stop, quit, reopen, reload
  -p prefix     : set prefix path (default: /usr/share/nginx/)
  -c filename   : set configuration file (default: /etc/nginx/nginx.conf)
  -g directives : set global directives out of configuration file
```
> 除了 使用 nginx -s  
> 亦可直接使用 kill 向 master 進程 發送 signal  
> 其實 nginx -s 也不過是對 kill進行了包裝 省略了 使用者 查詢 master pid 的麻煩罷了
> 


# 默認配置
```
#info=false
nginx 配置選項 可分爲 簡單 複合  
簡單 配置 由 key v0 v1 ...; 組成
複合 配置 由 { 
	key v0 v1 ...;
	key v0 v1 ...;
	...
	key v0 v1 ...{
		...
	}
} 組成
```

```
# worker 進程 使用的 用戶 
user www-data;
# worker 進程 數量 auto 依據 cpu 數量自動確定(通常 就是 cpu 數量)
worker_processes auto;
# master 進程 pid 記錄到 哪個檔案下
pid /run/nginx.pid;
 
events {
	# 每個 worker 最大 連接數量
	worker_connections 768;
 
	# 如果 on worker 將一次接受全部 連接
	# 建議 不要啓用 master 可以 高效的 分配 連接
	# multi_accept on;
}
 
# 配置 http
http {
 
	##
	# Basic Settings
	##
 
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	# server_tokens off;
 
	# server_names_hash_bucket_size 64;
	# server_name_in_redirect off;
 
	include /etc/nginx/mime.types;
	default_type application/octet-stream;
 
	##
	# SSL Settings
	##
 
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;
 
	##
	# Logging Settings
	##
 
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;
 
	##
	# Gzip Settings
	##
 
	gzip on;
	gzip_disable "msie6";
 
	# gzip_vary on;
	# gzip_proxied any;
	# gzip_comp_level 6;
	# gzip_buffers 16 8k;
	# gzip_http_version 1.1;
	# gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
 
	##
	# Virtual Host Configs
	##
 
	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}
 
#郵件配置
#mail {
#	# See sample authentication script at:
#	# http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
# 
#	# auth_http localhost/auth.php;
#	# pop3_capabilities "TOP" "USER";
#	# imap_capabilities "IMAP4rev1" "UIDPLUS";
# 
#	server {
#		listen     localhost:110;
#		protocol   pop3;
#		proxy      on;
#	}
# 
#	server {
#		listen     localhost:143;
#		protocol   imap;
#		proxy      on;
#	}
#}
```

> 配置中的 字符串 如果 由 空格 等 需要用 **'** 或 **"** 擴起來
> 
> worker_connections 是每個 worker 的 最大 連接數量  
> nginx 最大連接數量 是 worker\_connections\*worker\_processes  
> 如果 作爲 反向代理 或 負載服務器 則需要 **/2** 因爲到 後端服務器 和 客戶端 各佔用一個 tcp 一個 請求現在 佔用了2個連接
> 