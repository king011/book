# grpc

grpc 提供了 4中 rpc 模式

1. 基礎方法
2. 請求流
3. 響應流
4. 雙向流

## 創建 proto 檔案定義服務

創建 **test/grpc/protocol/zoo/Service.proto** 檔案 在裡面 使用 protoc3 語法 描述 服務  
並且使用 protoc 創建 對應語言的 代碼 

```bash
protoc -I pb/ --go_out=plugins=grpc:/home/king/project/go/src/  test/grpc/protocol/zoo/Service.proto
```

```
syntax = "proto3";
package king011_xsd_grpc.zoo;

// 設置 java 選項 只對 java 有效
option java_multiple_files = true;
option java_package = "io.grpc.examples.zoo"; // java 包名
// 默認名稱爲 ServiceOuterClass
//option java_outer_classname = "KingTestZooProto";
//option objc_class_prefix = "KTZ";

// Service 動物園
service Service {
    // Penguin 基礎 方法
    rpc Penguin (PenguinRequest) returns (PenguinResponse);
    // Dog 請求流
    rpc Dog (stream DogRequest) returns (DogResponse);
    // Cat 響應流
    rpc Cat (CatRequest) returns (stream CatResponse);
    // Fish 雙向流
    rpc Fish (stream FishRequest) returns (stream FishResponse);
}
message PenguinRequest{

}
message PenguinResponse{

}
message DogRequest{

}
message DogResponse{

}
message CatRequest{

}
message CatResponse{

}
message FishRequest{

}
message FishResponse{
}
```

## 服務器代碼

```go
#info="test/grpc/s/main.go"
package main

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	grpc_zoo "test/grpc/protocol/zoo"
)

const (
	// LAddr .
	LAddr = ":1102"
)

func main() {
	//創建 監聽 Listener
	l, e := net.Listen("tcp", LAddr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("work at", LAddr)

	//創建 rpc 服務器
	s := grpc.NewServer()

	//註冊 服務
	grpc_zoo.RegisterServiceServer(s, Zoo{})

	//註冊 反射 到 服務 路由
	reflection.Register(s)

	//讓 rpc 在 Listener 上 工作
	if e := s.Serve(l); e != nil {
		log.Fatalln(e)
	}
}
```

```go
#info="test/grpc/s/Zoo.go"
package main

import (
	"context"
	"fmt"
	"io"
	grpc_zoo "test/grpc/protocol/zoo"
)

// Zoo 服務器
type Zoo struct {
}

// Penguin 基礎 方法
func (s Zoo) Penguin(ctx context.Context, request *grpc_zoo.PenguinRequest) (response *grpc_zoo.PenguinResponse, e error) {
	// 設置響應
	response = &grpc_zoo.PenguinResponse{}
	return
}

// Dog 請求流
func (s Zoo) Dog(stream grpc_zoo.Service_DogServer) (e error) {
	n := 0
	for {
		_, e = stream.Recv()
		if e == io.EOF { // 客戶端 CloseSend
			e = nil
			break
		}
		if e == nil {
			n++
			fmt.Println("Dog <-", n)
		} else {
			fmt.Println(e)
			return
		}
	}
	// 設置響應
	e = stream.SendMsg(&grpc_zoo.DogResponse{})
	return
}

// Cat 響應流
func (s Zoo) Cat(request *grpc_zoo.CatRequest, stream grpc_zoo.Service_CatServer) (e error) {
	for i := 0; i < 2; i++ {
		// 響應
		e = stream.Send(&grpc_zoo.CatResponse{})
		if e == nil {
			fmt.Println("Cat -> ", i)
		} else {
			fmt.Println("Cat -> ", i, e)
			return
		}
	}
	return
}

// Fish 雙向流
func (s Zoo) Fish(stream grpc_zoo.Service_FishServer) (e error) {
	n := 0
	var request grpc_zoo.FishRequest
	var response grpc_zoo.FishResponse
	for {
		n++
		e = stream.RecvMsg(&request)
		if e == io.EOF { // 客戶端 CloseSend
			e = nil
			break
		} else if e == nil {
			fmt.Println("Fish <-", n)
		} else {
			fmt.Println(e)
			return
		}

		// 設置響應
		e = stream.SendMsg(&response)
		if e == nil {
			fmt.Println("Fish ->", n)
		} else {
			fmt.Println(e)
			return
		}
	}
	return
}
```

## 客戶端代碼

```go
#info="test/grpc/c/main.go"
package main

import (
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"io"
	"log"
	grpc_zoo "test/grpc/protocol/zoo"
)

const (
	// Addr 服務器 地址
	Addr = "127.0.0.1:1102"
)

func main() {
	// 連接 服務器
	conn, e := grpc.Dial(Addr, grpc.WithInsecure())
	if e != nil {
		log.Fatalln(e)
	}
	defer conn.Close()

	// 創建 rpc 服務 客戶端
	client := grpc_zoo.NewServiceClient(conn)
	var cmd string
	for {
		fmt.Print("#>")
		fmt.Scan(&cmd)
		if cmd == "e" {
			break
		} else if cmd == "p" {
			// 基礎 方法
			requestPenguin(client)
		} else if cmd == "d" {
			// 請求流
			requestDog(client)
		} else if cmd == "c" {
			// 響應流
			requestCat(client)
		} else if cmd == "f" {
			// 雙向流
			requestFish(client)
		}
	}
}
func requestPenguin(client grpc_zoo.ServiceClient) {
	_, e := client.Penguin(context.Background(), &grpc_zoo.PenguinRequest{})
	if e == nil {
		fmt.Println("Penguin Success")
	} else {
		fmt.Println(e)
	}
}
func requestDog(client grpc_zoo.ServiceClient) {
	stream, e := client.Dog(context.Background())
	if e != nil {
		fmt.Println(e)
		return
	}
	defer stream.CloseSend()
	// -> 1
	e = stream.Send(&grpc_zoo.DogRequest{})
	if e == nil {
		fmt.Println("Dog -> 1")
	} else {
		fmt.Println("Dog -> 1", e)
		return
	}
	// -> 2
	e = stream.Send(&grpc_zoo.DogRequest{})
	if e == nil {
		fmt.Println("Dog -> 2")
	} else {
		fmt.Println("Dog -> 2", e)
		return
	}

	// stream.CloseSend() && stream.RecvMsg()
	_, e = stream.CloseAndRecv()
	if e == nil {
		fmt.Println("Dog <-")
	} else {
		fmt.Println("Dog <-", e)
		return
	}

	fmt.Println("Dog Success")
}
func requestCat(client grpc_zoo.ServiceClient) {
	stream, e := client.Cat(context.Background(), &grpc_zoo.CatRequest{})
	if e != nil {
		fmt.Println(e)
		return
	}
	stream.CloseSend()
	n := 0
	for {
		_, e = stream.Recv()
		if e == io.EOF {
			fmt.Println("Cat Success")
			break
		} else if e != nil {
			fmt.Println("Cat <-", n, e)
			return
		}
		fmt.Println("Cat <-", n)
	}
}
func requestFish(client grpc_zoo.ServiceClient) {
	stream, e := client.Fish(context.Background())
	if e != nil {
		fmt.Println(e)
		return
	}
	defer stream.CloseSend()
	// -> 1
	e = stream.Send(&grpc_zoo.FishRequest{})
	if e == nil {
		fmt.Println("Fish -> 1")
	} else {
		fmt.Println("Fish -> 1", e)
		return
	}
	_, e = stream.Recv()
	if e == nil {
		fmt.Println("Fish <- 1")
	} else {
		fmt.Println("Fish <- 1", e)
		return
	}

	// -> 2
	e = stream.Send(&grpc_zoo.FishRequest{})
	if e == nil {
		fmt.Println("Fish -> 2")
	} else {
		fmt.Println("Fish -> 2", e)
		return
	}
	_, e = stream.Recv()
	if e == nil {
		fmt.Println("Fish <- 2")
	} else {
		fmt.Println("Fish <- 2", e)
		return
	}
	fmt.Println("Fish Success")
}
```

* grpc 會自己維護 和服務器的 tcp 連接 使用 grpc 時 不需要使用者去 瞎操心 grpc.Dial 不會建立 tcp 而是 當 發送 rpc 請求時 才安需\(如果 已有可用連接 復用 否則 創建新連接\) 連接
* proto3中 定義的 rpc 方法 必須是 一個 傳入 參數 同 一個 返回值 且在服務器實現 和客戶端請求 時 這兩個值 不能為nil 除非服務 返回了錯誤
* 使用 service 來定義 一個 服務 grpc 使用 package 指定的 包名 和 service 指定的 服務名 來唯一確定一個 服務名稱 故 如果 兩個 服務 package 和 service 都指定來相同名稱 在 服務端在註冊 時將 出現錯誤 提示 服務被註冊到兩個實現中 package.service
* 同樣 grpc 使用來 package 指定的 包名 和 message 指定的 型別名 來唯一確定一個 型別 故 不要讓 package.message 同名
* 建於 以上 兩點 最合適的做法是 不要 出現同名的 package 你不應該有一個 web/data 包 和 game/data 包 這樣 package 就重名了 你應該使用類似 web.data 和 game.data 的包名

> 所有 stream 當 對方 調用 CloseSend 時 Recv 會 返回 io.EOF
> 
> 服務器 會在 實現的回調 結束後自動 對 send stream 調用 CloseSend
> 
> 客戶端 必須 自己調用 CloseSend 否則 服務器的 Recv 可能會 無法退出 直達 請求取消/超時/發送錯誤/socket 斷開

# status

grpc 請求返回時 可以 包含一個 錯誤碼 和 錯誤描述  
google.golang.org/grpc/status 包 提供了 將 錯誤 包裝的 函數

```go
func Error(c codes.Code, msg string) error
func Errorf(c codes.Code, format string, a ...interface{}) error
```

## grpc.Code

google.golang.org/grpc 包的 Code 函數可以 返回 錯誤碼

```go
func Code(err error) codes.Code
```

## codes

google.golang.org/grpc/codes 包定義了 grpc 佔用了的 狀態碼

```go
// A Code is an unsigned 32-bit error code as defined in the gRPC spec.
type Code uint32
 
const (
	// OK is returned on success.
	OK Code = 0
 
	// Canceled indicates the operation was canceled (typically by the caller).
	Canceled Code = 1
 
	// Unknown error. An example of where this error may be returned is
	// if a Status value received from another address space belongs to
	// an error-space that is not known in this address space. Also
	// errors raised by APIs that do not return enough error information
	// may be converted to this error.
	// 未知錯誤 通常是由 API 返回的 錯誤 沒有足夠的信息將錯誤進行分類 
	Unknown Code = 2
 
	// InvalidArgument indicates client specified an invalid argument.
	// Note that this differs from FailedPrecondition. It indicates arguments
	// that are problematic regardless of the state of the system
	// (e.g., a malformed file name).
	// 調用者 傳入了 無效的 參數 系統無法 繼續執行此請求
	InvalidArgument Code = 3
 
	// DeadlineExceeded means operation expired before completion.
	// For operations that change the state of the system, this error may be
	// returned even if the operation has completed successfully. For
	// example, a successful response from a server could have been delayed
	// long enough for the deadline to expire.
	// 通常是 任務 超時 已經過期
	DeadlineExceeded Code = 4
 
	// NotFound means some requested entity (e.g., file or directory) was
	// not found.
	// 通常是 調用者請求的 資源 不存在
	NotFound Code = 5
 
	// AlreadyExists means an attempt to create an entity failed because one
	// already exists.
	// 通常是 調用者 請求創建的 資源 已經存在 所以無法 創建
	AlreadyExists Code = 6
 
	// PermissionDenied indicates the caller does not have permission to
	// execute the specified operation. It must not be used for rejections
	// caused by exhausting some resource (use ResourceExhausted
	// instead for those errors). It must not be
	// used if the caller cannot be identified (use Unauthenticated
	// instead for those errors).
	// 調用者 沒有權限
	PermissionDenied Code = 7
 
	// ResourceExhausted indicates some resource has been exhausted, perhaps
	// a per-user quota, or perhaps the entire file system is out of space.
	// 通常是 系統資源 或 分配給調用者的 資源已經耗盡 所以無法 執行 請求
	ResourceExhausted Code = 8
 
	// FailedPrecondition indicates operation was rejected because the
	// system is not in a state required for the operation's execution.
	// For example, directory to be deleted may be non-empty, an rmdir
	// operation is applied to a non-directory, etc.
	//
	// A litmus test that may help a service implementor in deciding
	// between FailedPrecondition, Aborted, and Unavailable:
	//  (a) Use Unavailable if the client can retry just the failing call.
	//  (b) Use Aborted if the client should retry at a higher-level
	//      (e.g., restarting a read-modify-write sequence).
	//  (c) Use FailedPrecondition if the client should not retry until
	//      the system state has been explicitly fixed. E.g., if an "rmdir"
	//      fails because the directory is non-empty, FailedPrecondition
	//      should be returned since the client should not retry unless
	//      they have first fixed up the directory by deleting files from it.
	//  (d) Use FailedPrecondition if the client performs conditional
	//      REST Get/Update/Delete on a resource and the resource on the
	//      server does not match the condition. E.g., conflicting
	//      read-modify-write on the same resource.
	// 通常 表示 錯誤 並且 客戶端 不應該 進行 重試操作 具體參數 如上  a b c d
	FailedPrecondition Code = 9
 
	// Aborted indicates the operation was aborted, typically due to a
	// concurrency issue like sequencer check failures, transaction aborts,
	// etc.
	//
	// See litmus test above for deciding between FailedPrecondition,
	// Aborted, and Unavailable.
	// 通常 類似 Unavailable  但如果要重試操作的化 應該使用 比 Unavailable 更高級別的 重試操作
	Aborted Code = 10
 
	// OutOfRange means operation was attempted past the valid range.
	// E.g., seeking or reading past end of file.
	//
	// Unlike InvalidArgument, this error indicates a problem that may
	// be fixed if the system state changes. For example, a 32-bit file
	// system will generate InvalidArgument if asked to read at an
	// offset that is not in the range [0,2^32-1], but it will generate
	// OutOfRange if asked to read from an offset past the current
	// file size.
	//
	// There is a fair bit of overlap between FailedPrecondition and
	// OutOfRange. We recommend using OutOfRange (the more specific
	// error) when it applies so that callers who are iterating through
	// a space can easily look for an OutOfRange error to detect when
	// they are done.
	// 通常表示 請求的數據已經越界 如操作數組期望的索引 讀取檔案尾之後的 數據
	OutOfRange Code = 11
 
	// Unimplemented indicates operation is not implemented or not
	// supported/enabled in this service.
	// 通常表示 一個 請求 系統 還未實現 可能會在將來實現
	Unimplemented Code = 12
 
	// Internal errors. Means some invariants expected by underlying
	// system has been broken. If you see one of these errors,
	// something is very broken.
	// 通常 這個錯誤 意味者 系統已經崩潰 但你不想讓用戶知道 所以還在讓程序 繼續假裝 運行 但整個系統 已經被破壞 無法如預期工作
	Internal Code = 13
 
	// Unavailable indicates the service is currently unavailable.
	// This is a most likely a transient condition and may be corrected
	// by retrying with a backoff.
	//
	// See litmus test above for deciding between FailedPrecondition,
	// Aborted, and Unavailable.
	//
	// 通常表示 當前服務不可用 但不可用的狀態是短暫的 客戶可以在之後進行 重試操作(例如 網路異常 tcp斷線都會返回 此錯誤)
	Unavailable Code = 14
 
	// DataLoss indicates unrecoverable data loss or corruption.
	// 通常表示 無法恢復數據 或 數據已經 損毀
	DataLoss Code = 15
 
	// Unauthenticated indicates the request does not have valid
	// authentication credentials for the operation.
	// 調用者的憑證 有問題 無法識別 調用者的 身份
	Unauthenticated Code = 16
)
```

如果 tcp 斷開了 通常會 返回 Unavailable 但 grpc 會自動重建 tcp  
所以你完全沒必要 重建 grpc 客戶端 或tcp 連接 只需要 重新發送失敗的請求進行 重試 會 繼續 請求其它功能即可


# grpc.sh

grpc.sh 是我寫的一個 bash腳本 用於 自動 查找 檔案夾下 proto 定義並自動 調用 proto 編譯 grpc

如果有個項目 gitlab.com/king011/auto-deploy 其中 gitlab.com/king011/auto-deploy/pb 檔案夾作爲根目錄 定義了 grpc 則可以執行如下指令 生成 grpc 代碼

```sh
#info="build-pb.sh"
#!/bin/bash

dir=$(cd $(dirname $BASH_SOURCE)/../../../ && pwd)
if [[ "$OSTYPE" == "msys" ]]; then
    output=${dir:1:1}:${dir:2}/
else
    output=$dir/
fi

./grpc.sh go pb/ $output
```

```bash
#info="grpc.sh"
#!/bin/bash
#Program:
#       自動 查找 編譯 google's grpc 代碼
#History:
#       2018-09-18 king first release support golang/c++
#       2019-02-24 king support java
#       2019-02-28 king support dart
#Email:
#       zuiwuchang@gmail.com

# 顯示幫助信息
function ShowHelp(){
    echo "help              : show help"
    echo "go root output    : build src/*.proto for golang" 
    echo "dart root output  : build src/*.proto for dartlang"
    echo "cpp root output   : build src/*.proto for c++"
    echo "java root output  : build src/*.proto for java"
}

# 遞歸 查詢所有的 檔案夾/包
# $1 根本目錄
function find_package(){
    _DIRS=""
    files=`find $1 -type d`
    ok=$?
    if [[ $ok != 0 ]];then
        exit $ok
    fi

    for str in $files
    do
        str=${str#$1}
        str=${str#.}
        str=${str#/}
        if [ "$str" ];then
            _DIRS="$_DIRS $str"
        fi
    done
}
# 查找 proto 檔案
# $1 根本目錄
# $2 包路徑
function find_proto(){
    _SOURCES=""

    files=`find $1/$2 -maxdepth 1 -name *.proto -type f`
    ok=$?
    if [[ $ok != 0 ]];then
        exit $ok
    fi

    for str in $files
    do
        str=${str#$1}
        str=${str#.}
        str=${str#/}
        if [ "$str" ];then
            _SOURCES="$_SOURCES $str"
        fi
    done
}

function print_source(){
    if [ "$2" ];then
        for str in $2
        do
            str=${str#$1}
            str=${str#.}
            str=${str#/}
            echo "   $str"
        done
    else
        echo "  warning : not found any source"
    fi
    echo "}"
    echo
}
function check_params(){
    if [ ! "$2" ];then
        echo "need param root directory"
        echo "exmaple : grpc.sh $1 proto protocol/$1"
        exit 1
    fi
    if [ ! "$3" ];then
        echo "need param output"
        echo "exmaple : grpc.sh $1 proto protocol/$1"
        exit 1
    fi

    if [ ! -d "$3" ];then
        echo "directory not exist : $3"
        exit 1
    fi
}
# 自動 查找 並編譯 grpc 到 go 代碼
# $1 protoc 根目錄
# $2 輸出目錄
function BuildGo(){
    check_params go $1 $2
    root=$1
    out=$2

    find_package $root
    for dir in $_DIRS
    do
        echo
        echo "package $dir {"
        find_proto $root $dir
        print_source $dir $_SOURCES
        if [ "$_SOURCES" ];then
            echo "protoc -I $root --go_out=plugins=grpc:$out $_SOURCES"
            protoc -I $root --go_out=plugins=grpc:$out $_SOURCES
            ok=$?
            if [[ $ok != 0 ]];then
                exit $ok
            fi
        fi
    done
}

# 自動 查找 並編譯 grpc 到 dart 代碼
# $1 protoc 根目錄
# $2 輸出目錄
function BuildDart(){
    check_params dart $1 $2
    root=$1
    out=$2

    find_package $root
    for dir in $_DIRS
    do
        echo
        echo "package $dir {"
        find_proto $root $dir
        print_source $dir $_SOURCES
        if [ "$_SOURCES" ];then
            echo "protoc -I $root --dart_out=grpc:$out $_SOURCES"
            protoc -I $root --dart_out=grpc:$out $_SOURCES
            ok=$?
            if [[ $ok != 0 ]];then
                exit $ok
            fi
        fi
    done
}

# 自動 查找 並編譯 grpc 到 c++ 代碼
# $1 protoc 根目錄
# $2 輸出目錄
function BuildCpp(){
    check_params cpp $1 $2
    root=$1
    out=$2

    GrpcCppPlugin=`which grpc_cpp_plugin`
    if [ -f "$GrpcCppPlugin".exe ];then
        GrpcCppPlugin="$GrpcCppPlugin".exe
    fi

    if [ ! -f "$GrpcCppPlugin" ];then
        echo grpc_cpp_plugin not found
        exit 1
    fi

    find_package $root
    for dir in $_DIRS
    do
        echo
        echo "package $dir {"
        find_proto $root $dir
        print_source $dir $_SOURCES
        if [ "$_SOURCES" ];then
            # pb
            echo "protoc -I $root --cpp_out=$out $_SOURCES"
            protoc -I $root --cpp_out=$out $_SOURCES
            ok=$?
            if [[ $ok != 0 ]];then
                exit $ok
            fi

            # grpc
            echo "protoc -I $root --plugin=protoc-gen-grpc=$GrpcCppPlugin --grpc_out=$out $_SOURCES"
            protoc -I $root --plugin=protoc-gen-grpc=$GrpcCppPlugin --grpc_out=$out $_SOURCES
            ok=$?
            if [[ $ok != 0 ]];then
                exit $ok
            fi
        fi
    done
}
# 自動 查找 並編譯 grpc 到 java 代碼
# $1 protoc 根目錄
# $2 輸出目錄
function BuildJava(){
    check_params java $1 $2
    root=$1
    out=$2

    find_package $root
    for dir in $_DIRS
    do
        echo
        echo "package $dir {"
        find_proto $root $dir
        print_source $dir $_SOURCES
        if [ "$_SOURCES" ];then
            # pb
            echo "protoc -I $root --java_out=$out $_SOURCES"
            protoc -I $root --java_out=$out $_SOURCES
            ok=$?
            if [[ $ok != 0 ]];then
                exit $ok
            fi

            # grpc
            echo "protoc --plugin=protoc-gen-grpc-java --proto_path=$root --grpc-java_out=$out $_SOURCES"
            protoc --plugin=protoc-gen-grpc-java --proto_path=$root --grpc-java_out=$out $_SOURCES
            ok=$?
            if [[ $ok != 0 ]];then
                exit $ok
            fi
        fi
    done
}

ok=0
case $1 in
    go)
        BuildGo $2 $3
        ok=$?
    ;;

    dart)
        BuildDart $2 $3
        ok=$?
    ;;

    cpp)
        BuildCpp $2 $3
        ok=$?
    ;;

    java)
        BuildJava $2 $3
        ok=$?
    ;;

    *)
        ShowHelp
        ok=$?
    ;;
esac
exit $ok
```
