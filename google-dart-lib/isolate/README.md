# dart:isolate

```dart
import 'dart:isolate';
```

isolate 提供了 隔離的併發執行環境 類似線程 但不能共享內存只能通過 消息通信

# Isolate

class 代表一個獨立執行的 隔離對象 的引用

```
import 'dart:async';
import 'dart:io';

import 'dart:isolate';

void main() async {
  /// 創建 isolate 通信管道
  final receivePort = ReceivePort();

  /// 創建 並且執行一個 並且的獨立環境 Isolate
  final isolate = await Isolate.spawn(generator, receivePort.sendPort);

  /// 監聽返回數據 ReceivePort 是單播的不可重複訂閱
  /// ReceivePort 會緩存沒有接收的數據 直達有人獲取 故不用擔心 listen前有數據遺落
  await Future.delayed(Duration(milliseconds: 1500));
  receivePort.listen((i) {
    if (i == null) {
      // 關閉之後 SendPort.send 會直接返回 不再繼續傳輸數據
      // 但不會影響 Isolate 繼續執行
      receivePort.close();
    } else {
      print("get $i");
      if (i > 3) {
        receivePort.close();
        isolate.kill();
      }
    }
  });
  final wait = ReceivePort();

  isolate.addOnExitListener(wait.sendPort);
  await wait.first;
}

void generator(SendPort sendPort) async {
  print("send -1");
  sendPort.send(-1);
  for (var i = 0; i < 10; i++) {
    await Future.delayed(Duration(seconds: 1));
    print("send $i");
    sendPort.send(i);
  }
  print("send exit");
  sendPort.send(null);
}
```

# 服務器

使用 Isolate 服務器可以利用多cpu來工作 但是 Socket 等句柄是無法通過 SendPort 傳遞的 

ServerSocket/HttpServer ... 等服務器 在bind時 提供了 shared 參數 如果設置爲 true 則可以在 多個 Isolate中 監聽同個端口 連接過來的socket會被 平衡的 分配到 多個Isolate中的 Server

```
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'dart:isolate';
import 'package:rxdart/rxdart.dart';

enum Status {
  Stopped,
  Stopping,
  Started,
  Starting,
}

class Manager {
  Manager();
  final _status = BehaviorSubject<Status>()..add(Status.Stopped);
  final _workers = List<Worker>();
  Stream<Status> get stream => _status.stream;
  Status get status => _status.value;
  Future<void> start(String address, int count) async {
    if (!this.isStopped) {
      throw Exception('not stopped');
    }
    if (count < 1) {
      throw Exception('not supported count');
    }
    _status.add(Status.Starting);
    try {
      final index = address.lastIndexOf(':');
      if (index == -1) {
        throw Exception('not supported address');
      }
      dynamic host = address.substring(0, index).trim();
      final port = int.parse(address.substring(index + 1).trim());
      if (host == '') {
        host = InternetAddress.anyIPv6;
      }
      if (port < 0 || port > 65535) {
        throw Exception('not supported port');
      }
      final workers = List<Worker>(count);
      Worker worker;
      for (var i = 0; i < count; i++) {
        try {
          worker = await Worker.create(host, port, i);
          await worker.bind();
        } catch (e) {
          for (var j = 0; j <= i; j++) {
            try {
              await workers[j]?.close();
            } catch (e) {
              print('$e');
            }
          }
          throw e;
        }
        workers[i] = worker;
      }
      for (var i = 0; i < workers.length; i++) {
        await workers[i].run();
      }
      _workers.addAll(workers);
      _status.add(Status.Started);
    } catch (e) {
      _status.add(Status.Stopped);
      throw e;
    }
  }

  Future<void> stop() async {
    if (!this.isStarted) {
      throw Exception('not started');
    }
    _status.add(Status.Stopping);
    for (var i = 0; i < _workers.length; i++) {
      await _workers[i].close();
    }
    _workers.clear();
    _status.add(Status.Stopped);
  }

  get isStopped => _status.value == Status.Stopped;
  get isStarted => _status.value == Status.Started;
}

class Worker {
  final SendPort sendPort;
  final ReceivePort receivePort;
  final dynamic address;
  final int port;
  final dynamic tag;
  Worker._({
    this.sendPort,
    this.receivePort,
    this.address,
    this.port,
    this.tag,
  })  : assert(sendPort != null),
        assert(receivePort != null),
        assert(address != null),
        assert(port != null),
        assert(tag != null);
  static Future<Worker> create(dynamic address, int port, dynamic tag) {
    final receivePort = ReceivePort();
    var completer = Completer<Worker>();
    final future = completer.future;
    Worker worker;
    receivePort.listen((message) {
      if (completer == null) {
        // 響應 消息
        worker?.complete(message);
      } else {
        // 建立完成 控制信道
        worker = Worker._(
          sendPort: message,
          receivePort: receivePort,
          address: address,
          port: port,
          tag: tag,
        );
        completer.complete(worker);
        completer = null;
      }
    });
    Isolate.spawn(startWorker, receivePort.sendPort).catchError((error) {
      completer.completeError(error);
    });
    return future;
  }

  Completer _completer;
  complete(message) {
    if (_completer != null) {
      _completer?.complete(message);
      _completer = null;
    }
  }

  Completer _mutex;
  void _unlock() async {
    final completer = _mutex;
    _mutex = null;
    completer.complete();
  }

  Future<void> _lock() async {
    while (_mutex != null) {
      try {
        await _mutex.future;
      } catch (error) {}
    }
    _mutex = Completer();
  }

  Future<void> bind() async {
    await _lock();
    final completer = Completer();
    _completer = completer;
    sendPort.send(requestBind(
      address: address,
      port: port,
      tag: tag,
    ));
    final result = await completer.future;
    _unlock();

    if (result != Status.Started) {
      throw result;
    }
  }

  Future<void> run() async {
    await _lock();
    final completer = Completer();
    _completer = completer;
    sendPort.send(requestRun());
    final result = await completer.future;
    _unlock();

    if (result != null) {
      throw result;
    }
  }

  Future<void> close() async {
    await _lock();
    final completer = Completer();
    _completer = completer;
    sendPort.send(requestClose());
    final result = await completer.future;
    _unlock();
    if (result != Status.Stopped) {
      throw result;
    }
    receivePort.close();
  }
}

class requestBind {
  final dynamic address;
  final int port;
  final dynamic tag;
  requestBind({
    this.address,
    this.port,
    this.tag,
  })  : assert(address != null),
        assert(port != null),
        assert(tag != null);
}

class requestRun {}

class requestClose {}

void startWorker(SendPort sendPort) async {
  final receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);
  wrapperServer srv;
  await for (var message in receivePort) {
    if (message is requestBind) {
      if (srv == null) {
        srv = wrapperServer();
        final requestBind request = message;
        srv.bind(request.address, request.port, request.tag).then(
          (value) {
            sendPort.send(Status.Started);
          },
          onError: (error) {
            sendPort.send(error);
          },
        );
      } else {
        sendPort.send(Exception('server already exists'));
      }
    } else if (message is requestRun) {
      if (srv == null) {
        sendPort.send(Exception('server null'));
      } else if (srv.isNotReady) {
        sendPort.send(Exception('server not ready'));
      } else {
        srv.run();
        sendPort.send(null);
      }
    } else if (message is requestClose) {
      if (srv == null) {
        sendPort.send(Exception('server null'));
      } else if (srv.isNotReady) {
        sendPort.send(Exception('server not ready'));
      } else {
        srv.clsoe().then(
          (value) {
            srv = null;
            sendPort.send(Status.Stopped);
          },
          onError: (error) {
            sendPort.send(error);
          },
        );
      }
    }
  }
}

class wrapperServer {
  ServerSocket _srv;
  dynamic _tag;
  wrapperServer();
  Future<ServerSocket> bind(dynamic address, int port, dynamic tag) {
    _tag = tag;
    return ServerSocket.bind(address, port, shared: true).then((value) {
      print('$tag work at $address:$port');
      _srv = value;
      return value;
    });
  }

  bool get isReady => _srv != null;
  bool get isNotReady => _srv == null;
  Future<ServerSocket> clsoe() {
    return _srv.close().then((value) {
      _closed = true;
      return value;
    });
  }

  bool _closed = false;
  run() {
    if (isNotReady) {
      throw Exception('server not ready');
    }
    if (_closed) {
      throw Exception('server already closed');
    }

    print('$_tag run');
    _srv.listen(
      (s) {
        _read(s);
      },
      onError: (e) {
        print(e);
      },
    );
  }

  Future<void> _read(Socket s) async {
    final addr = '${s.remoteAddress.address}:${s.remotePort}';
    print('$_tag one in $addr');
    try {
      // 讀取 數據
      // s.listen(onData)
      await for (var data in s) {
        final str = utf8.decode(data);
        print('$_tag from $addr : $str');
        // 發送數據
        s.add(data);
      }
    } on SocketException catch (e) {
      print(e.toString());
    }
    await s.close();
    print('$_tag one out $addr');
  }
}

void main() async {
  final manager = Manager();
  await manager.start(':9000', 3);
  await Future.delayed(Duration(seconds: 5));
  await manager.stop();
  print('closed');
}
```