# 配置外部 郵件服務器

## 163

1. 添加 配置 **/etc/gitlab/gitlab.rb**

   ```
	 #info=false
   gitlab_rails['smtp_enable'] = true
   gitlab_rails['smtp_address'] = "smtp.163.com"
   gitlab_rails['smtp_port'] = 465
   gitlab_rails['smtp_user_name'] = "xxx@163.com"
   gitlab_rails['smtp_password'] = "xxx"
   gitlab_rails['smtp_domain'] = "163.com"
   gitlab_rails['smtp_authentication'] = "login"
   gitlab_rails['smtp_enable_starttls_auto'] = true
   gitlab_rails['smtp_tls'] = true

   gitlab_rails['gitlab_email_from'] = 'xxx@163.com'
   user['git_user_email'] = 'xxx@163.com'
   ```

1.  重新加載配置

   ```
   #info=false
   gitlab-ctl reconfigure
   ```

1.  測試郵件發送

   ```
   #info=false
   gitlab-rails console
   ```
	 
   ```
   #info=false
   Notify.test_email('sendto@xxx.com', 'Message Subject', 'Message Body').deliver_now
   ```