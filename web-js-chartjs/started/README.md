# started

```
<div class="view">
    <mat-card>
        <mat-card-header>
            <mat-card-title>Chart.js</mat-card-title>
        </mat-card-header>
        <mat-card-content>
            <!-- chart 需要使用 canvas 進行繪圖 -->
            <canvas #chart></canvas>
        </mat-card-content>
        <mat-card-actions>
            <button mat-stroked-button type="button" (click)="onClickDraw()">Draw</button>
            <button mat-stroked-button type="button" (click)="onClickDestory()">Destory</button>
        </mat-card-actions>
    </mat-card>
</div>
```

```
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import 'chart.js/auto'
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnDestroy {
  private subscription_?: Subscription
  constructor() { }

  ngOnInit(): void {
    // 窗口大小改變時調整 圖表大小
    this.subscription_ = fromEvent(window, 'resize').pipe(
      debounceTime(100),
    ).subscribe(() => {
      this.chart_?.resize()
    })
  }
  ngOnDestroy(): void {
    this.subscription_?.unsubscribe()

    // 釋放掉所有資源
    this.onClickDestory()
  }
  @ViewChild("chart")
  private readonly chartView_?: ElementRef
  private chart_?: Chart
  onClickDraw() {
    this.chart_ = new Chart(this.chartView_!.nativeElement,
      {
        type: 'bar', // 設置圖表類型爲柱形圖
        data: {
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'], // 每個柱子的文本標籤
          // 數據
          datasets: [ // 這是一個數組，所以可以爲每個 label 設置多個數據值
            {
              label: '# of Votes', // 鼠標覆蓋柱子時顯示的文本說明 `${label}: ${data}`
              data: [12, 19, 3, 5, 2, 3], // 每個柱子對應的數據
              borderWidth: 1, // 每個柱子的寬度
            },
          ]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        },
      })
  }
  onClickDestory() {
    const chart = this.chart_
    if (chart) {
      chart.destroy()
      this.chart_ = undefined
    }
  }
}
```

![](https://www.chartjs.org/docs/latest/assets/img/preview.0cc909a8.png)