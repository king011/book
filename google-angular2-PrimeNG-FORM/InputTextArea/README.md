# [Textarea](https://primeng.org/inputtextarea)
```
import {InputTextareaModule} from 'primeng/inputtextarea';
```

Textarea 是一個多行文本輸入組件

![](assets/primeng.org_inputtextarea.png)

```
<textarea pInputTextarea [(ngModel)]="property"></textarea>
```

## AutoResize

設定 **autoResize** 屬性，textarea會自動增長而不是顯示滾動條。啓用此功能後，需要定義 **rows** 和 **cols**

```
<textarea [rows]="5" [cols]="30" pInputTextarea [autoResize]="true"></textarea>
```

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onResize     | event: Event object     | 調整元素大小時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| autoResize     | boolean     | false     | 是否自動調整組件大小     |
| disabled     | boolean     | false     | 是否禁用組件     |

# Styling

| Name | Element |
| -------- | -------- |
| p-inputtextarea     | Textarea element     |

