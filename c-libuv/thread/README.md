# 線程

libuv 爲了實現線程池來異步操作檔案 爲此封裝了線程和一些同步工具，這些api也可供用戶當作跨平臺 線程組件使用

* uv\_thread\_create 啓動線程
* uv\_thread\_join 等待線程結束
* uv\_thread\_self 返回當前線程id

```
#include <iostream>
#include <uv.h>
#include <thread>
void work_thread(void *arg)
{
    std::cout << "work: " << uv_thread_self() << std::endl;
}
int main(int argc, char *argv[])
{
    std::cout << "main: " << uv_thread_self() << std::endl;
    uv_thread_t id;
    uv_thread_create(&id, work_thread, 0);
    uv_thread_join(&id);
    std::cout << "work: " << id << std::endl;
    return 0;
}
```

# 同步

mutex 是 pthread 的等效直接映射

```
int uv_mutex_init(uv_mutex_t* handle);
int uv_mutex_init_recursive(uv_mutex_t* handle);
void uv_mutex_destroy(uv_mutex_t* handle);
void uv_mutex_lock(uv_mutex_t* handle);
int uv_mutex_trylock(uv_mutex_t* handle);
void uv_mutex_unlock(uv_mutex_t* handle);
```

uv_mutex_init_recursive 支持遞歸互斥但最好不要依賴這點，並且不應該與 uv_cond_t 一起使用

如果鎖定互斥鎖的線程嘗試再次鎖定，默認的 BSD 互斥鎖實現將引發錯誤。例如像這樣將引發錯誤：
```
uv_mutex_init(a_mutex);
uv_mutex_lock(a_mutex);
uv_thread_create(thread_id, entry, (void *)a_mutex);
uv_mutex_lock(a_mutex);
// more things here
```
在 windows平臺上 mutex 總是遞歸的

# rwlock

rwlock 是一個玩具性的 讀寫鎖，請勿用在實際生成項目中
```
#include <stdio.h>
#include <uv.h>

uv_barrier_t blocker;
uv_rwlock_t numlock;
int shared_num;

void reader(void *n)
{
    int num = *(int *)n;
    int i;
    for (i = 0; i < 20; i++) {
        uv_rwlock_rdlock(&numlock);
        printf("Reader %d: acquired lock\n", num);
        printf("Reader %d: shared num = %d\n", num, shared_num);
        uv_rwlock_rdunlock(&numlock);
        printf("Reader %d: released lock\n", num);
    }
    uv_barrier_wait(&blocker);
}

void writer(void *n)
{
    int num = *(int *)n;
    int i;
    for (i = 0; i < 20; i++) {
        uv_rwlock_wrlock(&numlock);
        printf("Writer %d: acquired lock\n", num);
        shared_num++;
        printf("Writer %d: incremented shared num = %d\n", num, shared_num);
        uv_rwlock_wrunlock(&numlock);
        printf("Writer %d: released lock\n", num);
    }
    uv_barrier_wait(&blocker);
}

int main()
{
    uv_barrier_init(&blocker, 4);

    shared_num = 0;
    uv_rwlock_init(&numlock);

    uv_thread_t threads[3];

    int thread_nums[] = {1, 2, 1};
    uv_thread_create(&threads[0], reader, &thread_nums[0]);
    uv_thread_create(&threads[1], reader, &thread_nums[1]);

    uv_thread_create(&threads[2], writer, &thread_nums[2]);

    uv_barrier_wait(&blocker);
    uv_barrier_destroy(&blocker);

    uv_rwlock_destroy(&numlock);
    return 0;
}
```

# 其它
此外 還支持其它 pthread 中類似的  semaphores, condition variables and barriers

libuv 提供了 uv\_once 函數，讓多線程程式只會執行一次某些操作 
```
/* Initialize guard */
static uv_once_t once_only = UV_ONCE_INIT;

int i = 0;

void increment() {
    i++;
}

void thread1() {
    /* ... work */
    uv_once(once_only, increment);
}

void thread2() {
    /* ... work */
    uv_once(once_only, increment);
}

int main() {
    /* ... spawn threads */
}
```

# uv\_async\_t

* uv\_async\_t 被提供來讓工作線程 環境 loop 並將消息傳回給 loop 
* 工作線程使用 uv\_async\_send 向 loop 發送消息(uv\_async\_send 是異步的會立刻返回)
* loop 會被喚醒並合併send發來的消息 只會調用註冊的回調

uv\_async\_send 和 loop 的回調並不是一一對應的，loop 會合併多次send，libuv 只保證 send 後至少會回調一次

此外如果要多次 send 要注意 第一次之後的 send 和 loop 回調之間的 同步

```
#include <iostream>
#include <uv.h>
#include <thread>
void work(void *arg)
{
    puts("work start");
    std::this_thread::sleep_for(std::chrono::seconds(2));
    uv_async_t *async = (uv_async_t *)arg;
    for (size_t i = 0; i < 10; i++)
    {
        async->data = (void *)(i);
        uv_async_send(async);
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
    async->data = (void *)(100);
    uv_async_send(async);
}
void async_cb(uv_async_t *handle)
{
    size_t v = (size_t)(handle->data);
    std::cout << "async_cb: " << v << std::endl;
    if (v == 100)
    {
        // 取消對句柄的引用
        uv_unref((uv_handle_t *)handle);
    }
}
int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();

    uv_async_t async;
    uv_async_init(loop, &async, async_cb); // 註冊 async 觀察者

    // 啓動一個工作線程
    uv_thread_t id;
    uv_thread_create(&id, work, &async);

    uv_run(loop, UV_RUN_DEFAULT);
    return 0;
}
```