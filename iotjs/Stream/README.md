# [Stream](https://github.com/jerryscript-project/iotjs/blob/master/docs/api/IoT.js-API-Stream.md)

Stream 是處理數據的接口，Stream 可以是 可讀 可寫 或 可讀寫的

# Readable

可讀流是讀取數據的抽象，它有兩個工作模式:

* **flowing**
* **paused**

所有流都是以 paused 模式開始

在 paused 模式下，每當有新的數據可讀時，流都會發出 readable 事件，你可以在事件中調用 read 函數來讀取數據。調用 pause 函數將流切換到此模式

在 flowing 模式下，每次接收到數據時，流都會發出 data 事件。數據緩存被傳遞給回調函數。調用 resume 函數或添加 data 事件監聽器將流切換到 flowing 模式

```
interface ReadableOptions {
		/**
		 * @default utf8
		 */
		defaultEncoding?: string
}
class Readable extends EventEmitter {
		constructor(options?: ReadableOptions) { }
		/**
		 * 返回是否處於 paused 模式
		 */
		isPaused(): boolean
		/**
		 * 將流設置爲 paused 模式
		 */
		pause(): Readable
		/**
		 * 從內部緩衝區讀取一些數據，如果沒有數據返回 null
		 * @param size 
		 */
		read(size?: number): Buffer | null
		/**
		 * 將流設置爲 flowing 模式
		 */
		resume(): Readable
		/**
		 * 將數據添加給流進行讀取
		 * @param chunk 
		 */
		push(chunk: Buffer | string): void
		/**
		 * 將可讀流設置爲 flowing 模式，並且自動將讀取內容推送到 destination
		 * @remarks
		 * 默認情況下在可讀流結束後會調用 destination.end() 方法通知附加目標結束，
		 * 可以設置 options.end 爲 false 來禁止此行爲。
		 * 此外如果出現流錯誤也不會調用 destination.end
		 * @param destination 
		 * @param options 
		 */
		pipe(destination: Writable | Duplex, options?: {
				/**
				 * @default true
				 */
				end?: boolean
		}): Writable | Duplex
		/**
		 * 分流附加的到 pipe 的流
		 * @param destination 如果目標不存在則什麼都不做
		 */
		unpipe(destination?: Writable | Duplex): Readable

		/**
		 * 當底層資源已關閉並且不會再發出任何事件時發出。 並非所有可讀流都會發出 close 事件。
		 */
		on(event: 'close', listener: () => void): Readable
		/**
		 * 當流將數據的所有權傳遞給消費者時調用，只有在 flowing 模式下才會被調用 
		 */
		on(event: 'data', listener: (chunk: Buffer | string) => void): Readable
		/**
		 * 當流中沒有更多數據時調用 
		 */
		on(event: 'end', listener: () => void): Readable
		/**
		 * 當流檢測到錯誤時調用 
		 */
		on(event: 'error', listener: (e) => void): Readable
		/**
		 * 當流中有數據可讀時發出。一旦流到達末尾也會在發出 end 事件之前發送 readable 事件
		 */
		on(event: 'readable', listener: () => void): Readable

		/**
		 * 當底層資源已關閉並且不會再發出任何事件時發出。 並非所有可讀流都會發出 close 事件。
		 */
		once(event: 'close', listener: () => void): Readable
		/**
		 * 當流將數據的所有權傳遞給消費者時調用，只有在 flowing 模式下才會被調用 
		 */
		once(event: 'data', listener: (data: Buffer | string) => void): Readable
		/**
		 * 當流中沒有更多數據時調用 
		 */
		once(event: 'end', listener: () => void): Readable
		/**
		 * 當流檢測到錯誤時調用 
		 */
		once(event: 'error', listener: (e) => void): Readable
		/**
		 * 當流中有數據可讀時發出。一旦流到達末尾也會在發出 end 事件之前發送 readable 事件
		 */
		once(event: 'readable', listener: () => void): Readable

		removeListener(event: 'close', listener: () => void): Readable
		removeListener(event: 'data', listener: (data: Buffer | string) => void): Readable
		removeListener(event: 'end', listener: () => void): Readable
		removeListener(event: 'error', listener: (e) => void): Readable
		removeListener(event: 'readable', listener: () => void): Readable
}
```

```
import { Readable } from "stream";
const readable = new Readable({
    defaultEncoding: 'utf8'
})
readable.on('data', (s) => {
    console.log('data', `${s}`)
})
readable.once('end', () => {
    console.log('end')
})
readable.once('close', () => {
    console.log('close')
})
for (let i = 0; i < 10; i++) {
    readable.push(`${i}`)
}
// 通知寫入完成
readable.emit('end')
```

# Writable

可寫流是數據寫入目標的抽象，它有多個可重寫的 virtual 函數

```
interface WritableOptions {
		/**
		 * 內部緩衝區 bytes 大小，當寫入數據達到此值，write 會返回 false，並當內部數據被耗盡後發送 drain 事件
		 * @default 128
		 */
		highWaterMark: number
}
/**
 * 可寫流
 */
class Writable extends EventEmitter {
		constructor(options?: WritableOptions)
		/**
		 * 完成寫入，此後流將不再可寫
		 * @param chunk 要最後寫入的數據
		 * @param callback 自動註冊到 finish 事件
		 */
		end(chunk?: Buffer | string, callback?: () => void): void

		/**
		 * 
		 * @param chunk 要寫入的數據
		 * @param callback
		 * @returns 如果返回 false 表示不應該繼續寫入數據(chunk 後續會被自動寫入)，當緩存數據被寫完後會發送 drain 事件
		 */
		write(chunk: Buffer | string, callback?: () => void): boolean

		/**
		 * 此方法通知 Writable 流實現已準備好接收數據
		 * @virtual
		 */
		_readyToWrite: () => void
		/**
		 * 
		 * @param chunk 流要寫入的數據
		 * @param callback flushed 時的回調函數，外部回調，在寫入後應該調用此函數通知外部調用者
		 * @param onwrite flushed 時的回調函數，系統內部回調，在寫入後應該調用此函數通知系統
		 * @virtual
		 * @example
		 * ```
		 * var Writable = require('stream').Writable;
		 * 
		 * var writable = new Writable();
		 * 
		 * writable._write = function(chunk, callback, onwrite) {
		 *   // prints: message
		 *   console.log(chunk);
		 * 
		 *   onwrite();
		 *   if (callback)
		 *     callback();
		 * }
		 * 
		 * writable._readyToWrite();
		 * 
		 * writable.write('message');
		 * ```
		 */
		_write: (chunk: Buffer | string, callback?: () => void, onwrite: () => void) => void
		/**
		 * 如果 write 返回 false，則在可寫時會發送 drain 事件
		 */
		on(event: 'drain', listener: () => void): Writable
		/**
		 * 如果在寫入期間發生錯誤，則發送 error 事件 
		 */
		on(event: 'error', listener: (e: Error) => void): Writable
		/**
		 * 在 end() 被調用後，並且所有緩存數據被處理後發送 finish 事件
		 */
		on(event: 'finish', listener: () => void): Writable
		/**
		 * 如果 write 返回 false，則在可寫時會發送 drain 事件
		 */
		once(event: 'drain', listener: () => void): Writable
		/**
		 * 如果在寫入期間發生錯誤，則發送 error 事件 
		 */
		once(event: 'error', listener: (e: Error) => void): Writable
		/**
		 * 在 end() 被調用後，並且所有緩存數據被處理後發送 finish 事件
		 */
		once(event: 'finish', listener: () => void): Writable
		removeListener(event: 'drain', listener: () => void): Writable
		removeListener(event: 'error', listener: (e: Error) => void): Writable
		removeListener(event: 'finish', listener: () => void): Writable
}
```


```
import { Writable } from "stream";

/**
 * 實現一個新的流
 */
class Stdout extends Writable {
    constructor() {
        super({
            highWaterMark: 20,
        })
        // 重寫流寫入
        this._write = (chunk, callback, onwrite) => {
            // 模拟耗时写入
            setTimeout(() => {
                // 寫入數據
                console.log(chunk.toString())

                // 通知系統寫入完成
                onwrite()
                // 通知調用者寫入完成
                if (callback) {
                    callback()
                }
            }, 1000)
        }
        this._readyToWrite()
    }

    writeN(i: number) {
        return new Promise<void>((resolve) => {
            const next = this.write(`message ${i}`, () => {
                resolve()
            })
            console.log(`next ${i}: ${next}`)
        })

    }
}

async function main() {
    const stdout = new Stdout()
    for (let i = 0; i < 10; i++) {
        await stdout.writeN(i)
        console.log(`ok: ${i}`)
    }

    setTimeout(() => {

    }, 10);
}
main()
```

# Duplex

Duplex 同時實現了 Readable Writable 的雙向流

```
class Duplex implements Readable, Writable
```

```
import { Duplex } from "stream";
const d = new Duplex()
d._readyToWrite()
d._write = (chunk, callback, onwrite) => {
    d.push(chunk)

    onwrite()
    if (callback) {
        callback()
    }
}

// d.on('data', (chunk) => {
//     console.log(`chunk: ${chunk}`)
// })
d.on('readable', () => {
    const chunk = d.read()
    console.log(`chunk: ${chunk}`)
})
for (let i = 0; i < 10; i++) {
    d.write(`message ${i}`)
}
```
