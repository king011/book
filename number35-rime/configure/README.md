# 配置

rime 使用 *.yaml 的YAML文本 配置

## 共享 資料夾

* 【中州韻】 **/usr/share/rime-data/**
* 【小狼毫】 **"安裝目錄\data"**
* 【鼠鬚管】 **"/Library/Input Methods/Squirrel.app/Contents/SharedSupport/"**

此目錄 一般不要 做升級外的 修改 如果修改錯誤 可能導致 無法正常運行 rime

## 用戶資料夾
* 【中州韻】 **~/.config/ibus/rime/** （0.9.1 以下版本爲 ~/.ibus/rime/）
* 【小狼毫】 **"%APPDATA%\Rime"**
* 【鼠鬚管】 **~/Library/Rime/**

用戶資料夾 可以用戶 用戶 自定義 rime  
通常包含如下 檔案
*  〔全局設定〕 **default.yaml**
* 〔發行版設定〕 **weasel.yaml**
* 〔預設輸入方案副本〕 **&lt;方案標識&gt;.schema.yaml**
*  〔安裝信息〕 **installation.yaml**
* 〔用戶狀態信息〕 **user.yaml**

# 同步詞庫
1. 安裝 rime 配置 工具

  sudo apt install librime-bin
	
1. vi ~/.config/ibus/rime/**installation.yaml** 添加 如下 內容

	```yaml
	# 設置 同步 檔案夾 路徑
	sync_dir: "/home/king/Dropbox/rime"
	```
	
1. 執行 同步

	```sh
	rime_dict_manager -s
	```
	
```sh
king@king-company ~/Dropbox/rime $ cat ~/.config/ibus/rime/installation.yaml 
distribution_code_name: "ibus-rime"
distribution_name: Rime
distribution_version: 1.2
install_time: "Thu Jan 25 12:50:17 2018"
installation_id: "cfa930c6-e7ff-4ea9-a340-aa32d784c597"
rime_version: 1.2.0
# 設置 同步 檔案夾 路徑
sync_dir: "/home/king/Dropbox/rime"
```

* installation_id 是隨機生產 的 的uuid
* 備份檔案 會存儲到 id-\`installation_id\` 檔案夾下 以區分 備份 來源
* 可以 自己將其 修改爲 便於 自己 識別的 值 \(如 **installation_id: "king-ubuntu64-company-xsd"**\)

> 執行 命令時 需要 關閉 rime
> rime\_dict\_manager 命令 需要 到 ~/.config/ibus/rime/ 檔案夾下執行
> 
	
