# 共享網路

docker 允許讓一個容器A 共享另外一個容器B 的網路，只要使用 **--network=container:XXX** 參數即可，XXX 是容器B 的名稱，這樣可以在容器A中通過 127.0.0.1 訪問到 容器B中的一些私有網路接口


在 Docker Compose 中也允許這種行爲並且更簡單，指定 network\_mode 到服務即可 
```
version: '3.8'
services:
  mycontainer1:
    image: myregistry/my-container1:latest
    ports: ['6666:7777']
  mycontainer2:
    image: myregistry/my-container2:latest
    network_mode: service:mycontainer1     # <---
```