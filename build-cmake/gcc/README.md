# gcc 函數改名
即使 添加了 **extern "C"** 如果指定來 函數 調用約定 gcc 依然會爲 **cdecl** 之外的 調用 添加 前綴/後綴 以區分不同的 調用規則

如果 要禁用 改名 

* 在 編譯庫時 爲export導出的 庫 指定 **-Wl,--kill-at** 參數  禁用改名

* 在 使用庫時 爲import引入的庫 指定 **-Wl,--enable-stdcall-fixup** 參數 爲沒有改名的函數 建立名稱映射


```cmake
#info="CMakeLists.txt"
cmake_minimum_required(VERSION 3.0)

# 動態庫 禁用 調用約定 改名
add_library(cat SHARED
    Cat.cpp
)
target_link_libraries(cat
    "-Wl,--kill-at"
)

# 動態庫 使用 默認 改名規則
add_library(dog SHARED
    Dog.cpp
)

# exe
add_executable(animal
    main.cpp
)
target_link_libraries(animal
    "-Wl,--enable-stdcall-fixup libcat.dll" # 禁用 改名
    dog
)
```


```c++
#info="Cat.h"

#ifndef ___CAT_H__
#define ___CAT_H__

#ifdef __cplusplus
extern "C"
{
#endif

    int __stdcall CatSpeak(const char *str);
    
#ifdef __cplusplus
}
#endif

#endif // ___CAT_H__
```
```c++
#info="Cat.cpp"
#include "Cat.h"
#include <iostream>

#ifdef __cplusplus
extern "C"
{
#endif

    int __stdcall CatSpeak(const char *str)
    {
        std::cout << "cat :" << str << std::endl;
        return 0;
    }

#ifdef __cplusplus
}
#endif
```

```c++
#info="Dog.h"
#ifndef ___DOG_H__
#define ___DOG_H__

#ifdef __cplusplus
extern "C"
{
#endif

    int __stdcall DogSpeak(const char *str);
    
#ifdef __cplusplus
}
#endif

#endif // ___DOG_H__
```
```c++
#info="Dog.cpp"
#include "Dog.h"

#include <iostream>

#ifdef __cplusplus
extern "C"
{
#endif

    int __stdcall DogSpeak(const char *str)
    {
        std::cout << "dog :" << str << std::endl;
        return 0;
    }

#ifdef __cplusplus
}
#endif
```

```c++
#info="main.cpp"
#include "Cat.h"
#include "Dog.h"

int main(int argc, char *argv[])
{
    CatSpeak("喵~");
    DogSpeak("汪~");

    return 0;
}
```