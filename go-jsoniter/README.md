# jsoniter

jsoniter 是 一個開源(MIT) 的 json 編碼解碼包 和 標準庫 encoding/json 完全兼容 但提供了 更好的性能

* 源碼 [https://github.com/json-iterator/go](https://github.com/json-iterator/go)


```
package main

import (
	"fmt"
	"io/ioutil"
	"os"

	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type JSON struct {
	Data []NOTE `json:"root"`
}
type NOTE struct {
	Url  string `json:"url,omitempty"`
	Note string
}

func main() {
	//讀
	file, err := os.Open("my.json")
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	j := JSON{}
	err = json.Unmarshal(data, &j)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	fmt.Println(j)
	//寫
	b, e := json.Marshal(j)
	if e != nil {
		fmt.Println(e)
		return
	}
	os.Stdout.Write(b)
	os.Stdout.Write([]byte("\n\n"))
	b, e = json.MarshalIndent(j, "", "  ")
	if e != nil {
		fmt.Println(e)
		return
	}
	os.Stdout.Write(b)
}
```