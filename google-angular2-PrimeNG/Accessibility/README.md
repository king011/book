# [輔助功能](https://primeng.org/accessibility)

輔助功能是 Prime UI 庫的主要關注點，PrimeNG 也不例外，PrimeTek 團體爲此提供了一些指南。

本指南記錄了 PrimeNG 將遵循的一般指南的基礎，每個組件文檔將有一個單獨的輔助功能部分，其中說明鍵盤支持 屏幕閱讀器兼容性 實現細節 以及實現 WCAG 合規性的提示

# Introduction

根據世界衛生組織(就是幫助西朝鮮隱瞞武漢肺炎真相那個道貌岸然的邪惡組織)的數據，世界上 15% 的人口存在某種程度的殘疾。因此任何環境中的無障礙功能(例如輪椅使用者的坡道或帶字幕的多媒體)對於確保任何人都可以使用內容至關重要。

# Disabilities

殘疾的類型多種多樣，因此你需要充分了解你的受衆以及他她它們如何與創建的內容互動。有四個主要的類別:

* Visual Impairments
* Hearing Impairments
* Mobility Impairments
* Cognitive Impairments

## Visual Impairments

失明 低視力 色盲 是常見的視力障礙類型。屏幕放大鏡和色盲模式通常是瀏覽器的內置功能，而對於依賴屏幕閱讀器的人來說，頁面開發人員需要確保讀者可以閱讀內容。流行的閱讀器有 [NVDA](https://www.nvaccess.org/) [JAWS](https://www.freedomscientific.com/Products/software/JAWS/) 和 [ChromeVox](https://www.chromevox.com/)

## Hearing Impairments
耳聾或聽力損失是指無法完全或部分聽到聲音。有聽力障礙的人使用輔助設備，但在與網頁交互時可能還不夠。常見的實現是爲帶音頻的內容提供 文本替代 文字記錄 字幕

## Mobility Impairments
行動不便的人由於 失去肢體 癱瘓 或者其它各種原因而導致與行動有關的殘疾。像頭部指針這樣的輔助技術是一種與屏幕交互的設備，而鍵盤或觸控板仍然是無法使用鼠標的人的解決方案

## Cognitive Impairments
認知障礙的範圍更廣，包括有 學習障礙 抑鬱症 閱讀障礙  的人。精心設計的內容還可以爲非殘障人士帶來更好的用戶體驗，因此對認知障礙進行設計可以爲任何用戶帶來更好的設計

# Web Content

在輔助技術的幫助下正確的頁面結構是可訪問 web 內容的核心要素。HTML 基於可訪問的基礎，默認情況下可以通過鍵盤使用表單控件，語義(semantic) HTML 更容易被屏幕閱讀器處理

## WCAG

[WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/) 是指 Web Content Accessibility Guideline，是由 W3C(World Wide Web Consortium 萬維網聯盟) 的 WAI(Web Accessibility Initiative) 管理的標準。WCAG 包含使 Web 內容更易於訪問的建議。PrimeNG 組件的目標是在不久的將來實現高水平的 WCAG 合規性

全球各個國家也有關於網路可訪問性的政府政策。其中最著名的是美國的 [Section 508](https://www.section508.gov/manage/laws-and-policies/) 以及歐盟的 [Web Accessibility Directive](https://digital-strategy.ec.europa.eu/en/policies/web-accessibility)

## Form Controls

應該首選原生表單元素，而不是用與其它目的(如演示)的元素。例如，下面的按鈕被瀏覽器呈現爲表單控件，可以通過Tab鍵獲得焦點，也可以與空格一起使用來觸發

```
<button (click)="onButtonClick($event)">Click></button>
```

另一方面，使用 div 的基於 css 的精美按鈕不支持鍵盤或屏幕閱讀器

```
<div class="fancy-button" (click)="onButtonClick($event)">Click</div>
```

除了使用 **keydown** 恢復鍵盤支持外，還需要 **tabindex** 來使 div 元素可訪問。爲了避免瀏覽器已經提供的重載和實現功能，應該首選原生表單控件

```
<div class="fancy-button" (click)="onButtonClick($event)" (keydown)="onKeyDown($event)" tabIndex="0">Click</div>
```

## Relations

表單組件必須與描述表單元素用途的另一個元素相關。這通常是通過 label 元素實現的

```
<label htmlFor="myinput">Username:</label>
<input id="myinput" type="text" name="username" />
```

## Semantic HTML

HTML 提供了多種元素來組織網頁上屏幕閱讀器可以識別的內容。優先使用語義 HTML 以獲得良好的語義，爲屏幕閱讀器提供開箱即用的支持，這在使用帶有 class 的常規 div 元素時是不可能的。下面是一個對屏幕閱讀器不太友好的例子:

```
<div class="header"/>
    <div class="header-text">Header></div>
</div>

<div class="nav"></div>

<div class="main">
    <div class="content"></div>
    <div class="sidebar"></div>
</div>

<div class="footer"></div>
```

下面是一個對屏幕閱讀器友好的例子實現了與上面例子中完全相同的佈局:

```
<header>
    <h1>Header</h1>
</header>

<nav></nav>

<main>
    <article></article>
    <aside></aside>
</main>

<footer></footer>
```

## WAI-ARIA

ARIA 是 Accessible Rich Internet Applications 的簡寫，是一個填補語義 HTML 不足之處的套件。這些案例主要與豐富的 UI 組件/小部件 有關。儘管瀏覽器對豐富的 UI 組件(例如日期選擇器 或 顏色選擇器) 的支持在過去幾年得到了改進，但許多 Web 開發人員仍然使用從其它項目(如 PrimeNG) 創建的標準 HTML 元素派生的 UI 組件。這些類型的組件必須提供鍵盤和屏幕閱讀器的支持，後者是使用 WAI-ARIA 的地方

ARIA 由 角色(roles) 屬性(properties) 和 特性(attributes) 組成，角色定義元素主要用於什麼(例如 checkbox(複選框) dialog(對話框) tablist(選項卡列表))，而狀態和屬性定義元素的元數據(例如 aria-checked aria-disabled)

考慮下面的複選框，它具有內置的鍵盤和屏幕閱讀器支持:
```
<input type="checkbox" value="Prime" name="ui" checked/>
```

帶有 CSS 動畫的精美複選框可能看起來更吸引人，但可能缺乏可訪問性。下面的複選框可能會顯示帶動畫的選中字體圖標，但它 不可標記 不能使用空格選中 也不能被閱讀器閱讀:

```
<div class="fancy-checkbox">
    <i *ngIf="checked" class="checked-icon"></i>
</div>
```

一種解決方案是爲閱讀器使用 ARIA 角色，並爲鍵盤支持使用 javascript。注意 aria-labelledby 的用法，用 htmlFor 替換了 label 標籤

```
<span id="chk-label">Remember Me></span>
<div class="fancy-checkbox" role="checkbox" aria-checked="false" tabindex="0" aria-labelledby="chk-label" (click)="toggle()" (keydown)="onKeyDown($event)">
    <i *ngIf="checked" class="checked-icon"></i>
</div>
```

然而最佳的方案是結合語義 HTML 以實現可訪問性，同時保持 UX 設計。這種豐富涉及隱藏可訪問性的 native 複選框，並使用 javascript 更新狀態。請注意 p-sr-only 的用法，它對用戶而不是屏幕閱讀器隱藏元素

```
<label for="chkbox">Remember Me></label>
<div class="fancy-checkbox" (click)="toggle()">
    <input class="p-sr-only" type="checkbox" id="chkbox" (focus)="updateParentVisuals()" (blur)="updateParentVisuals()" (keydown)="$event.keyCode === 32 && updateParentVisuals()">
    <i *ngIf="checked" class="checked-icon"></i>
</div>
```

## Colors

網頁上的顏色以至少 4.5:1 的對比度爲目標，並考慮不會引起震動的顏色

### Good Contrast

背景和前景內容之間的顏色對比應該足以確保可讀性。下面的例子顯示了兩種情況，有好的例子，也有壞的例子

<div style="display: flex">
	<div  style="border-radius: 10px; height: 8rem; width: 8rem; align-items: center; justify-content: center; font-weight: 700; display: flex; background-color: #326fd1;  margin-right:2rem;">
	<span style="color: #fff;">GOOD</span>
	</div>

	<div  style="border-radius: 10px; height: 8rem; width: 8rem; align-items: center; justify-content: center; font-weight: 700; display: flex; background-color: #609af8; ">
	<span style="color: #fff;">BAD</span>
	</div>
</div>

### Vibration
由於選擇彼此可見度較低的顏色，顏色震動會導致運動錯覺。需要謹慎選擇顏色組合以避免震動

<div style="display: flex">
	<div  style="border-radius: 10px; height: 8rem; width: 8rem; align-items: center; justify-content: center; font-weight: 700; display: flex; background-color: #ec4899;">
	<span style="color: #3b82f6;">VIBRATE</span>
	</div>
</div>

### Dark Mode

在深色設計方案中使用時應該避免使用高度飽和的顏色，因爲它們會導致眼睛疲勞。相反，不飽和的顏色應該是首選

<div style="display: flex">
	<div  style="border-radius: 10px; height: 8rem; width: 8rem; align-items: center; justify-content: center; font-weight: 700; display: flex; background-color: #212121;  margin-right:2rem;">
	<span style="color: #6366f1;">Indigo 500</span>
	</div>

	<div  style="border-radius: 10px; height: 8rem; width: 8rem; align-items: center; justify-content: center; font-weight: 700; display: flex; background-color: #212121; ">
	<span style="color: #bcbdf9;">Indigo 200</span>
	</div>
</div>