# [點擊事件](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html)

> 本文檔描述了新的事件 API。 [手勢輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html)中描述了仍受支援的舊（傳統）方法

點擊事件是與 Flame 遊戲互動最基本的方法之一。 當使用者用手指觸摸螢幕、用滑鼠點擊或用觸筆點擊時，會發生這些事件。 點擊可能很“長”，但手指在手勢過程中不應該移動。 因此，觸摸螢幕，然後移動手指，然後鬆開——不是點擊，而是拖曳。 同樣，在滑鼠移動時點擊滑鼠按鈕也將被註冊為拖曳

多個點擊事件可以同時發生，特別是當使用者有多個手指時。 Flame 將正確處理此類情況，您甚至可以使用事件的 pointId 屬性來追蹤事件

需要爲想要回應點擊的元件，加入 TapCallbacks mixin

* 此 mixin 為您的元件添加了四個可重寫的方法：onTapDown、onTapUp、onTapCancel 和 onLongTapDown。 預設情況下，這些方法中的每一個都不執行任何操作，需要重寫它們才能執行任何功能
* 另外，元件必須實作 containsLocalPoint() 方法（已經在 PositionComponent 中實現，所以大多數時候你不需要在這裡做任何事情）——這個方法允許 Flame 知道事件是否發生在元件內

```
class MyComponent extends PositionComponent with TapCallbacks {
  MyComponent() : super(size: Vector2(80, 60));

  @override
  void onTapUp(TapUpEvent event) {
    // Do something in response to a tap event
  }
}
```

## [onTapDown](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#ontapdown)

每次點擊都以「點擊向下」事件開始，您可以透過 void onTapDown(TapDownEvent) 處理程序接收該事件。 該事件被傳遞到位於觸控點且具有 TapCallbacks mixin 的第一個元件。 通常情況下，事件會停止傳播。 但是，您可以透過將 event.continuePropagation 設為 true 來強制將事件傳遞到下方的元件

傳遞給事件處理程序的 TapDownEvent 物件包含有關該事件的可用資訊。 例如，event.localPosition 將包含事件在目前元件的本機座標系中的座標，而 event.canvasPosition 則包含整個遊戲畫布的座標系中的座標

每個接收到 onTapDown 事件的元件最終都會收到具有相同 pointId 的 onTapUp 或 onTapCancel

## [onLongTapDown](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#onlongtapdown)

如果使用者按住手指一段時間（由 MultiTapDispatcher 中的 .longTapDelay 屬性配置），將觸發「長按」。 此事件會呼叫先前接收到 onTapDown 事件的元件上的 void onLongTapDown(TapDownEvent) 處理程序

## [onTapUp](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#ontapup)

此事件表示輕擊序列成功完成。 它保證只傳遞給那些先前接收到具有相同指標 id 的 onTapDown 事件的元件

傳遞給事件處理程序的 TapUpEvent 物件包含有關事件的訊息，其中包括事件的座標（即使用者在抬起手指之前觸摸螢幕的位置）以及事件的 pointId

請注意，點擊事件的裝置座標將與對應點擊事件的裝置座標相同（或非常接近）。 然而，對於局部座標卻不能這樣說。 如果您點擊的元件正在移動（就像遊戲中經常發生的那樣），那麼您可能會發現本地點擊座標與本地點擊座標有很大不同

在極端情況下，當元件遠離觸控點時，根本不會產生 onTapUp 事件：它將被 onTapCancel 取代。 但請注意，在這種情況下，onTapCancel 將在使用者抬起或移動手指時生成，而不是在組件移離觸控點時生成

## [onTapCancel](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#ontapcancel)

當點擊未能實現時，會發生此事件。 大多數情況下，如果使用者移動手指，就會發生這種情況，從而將手勢從「點擊」轉換為「拖曳」。 當被點擊的組件從使用者手指下方移開時，可能會發生這種情況，但這種情況不太常見。 更罕見的是，當另一個小部件在遊戲小部件上彈出時，或當設備關閉時，或類似情況時，會發生 onTapCancel

TapCancelEvent 物件僅包含現在被取消的前一個 TapDownEvent 的pointerId。 沒有與點擊取消關聯的位置

# [Mixins](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#mixins)

本節更詳細地描述了點擊事件處理所需的幾個 mixin

## [TapCallbacks](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#tapcallbacks)

TapCallbacks mixin 可以新增到任何元件中，以便該元件開始接收點擊事件

該 mixin 為元件添加了 onTapDown、onLongTapDown、onTapUp 和 onTapCancel 方法，預設情況下這些方法不執行任何操作，但可以重寫以實現任何實際功能。 也無需覆蓋所有這些：例如，如果您只想回應「真實」點擊，則可以僅覆蓋 onTapUp

另一個關鍵細節是，元件只會接收該元件內發生的點擊事件，由 containsLocalPoint() 函數判斷。 常用的 PositionComponent 類別根據其 size 屬性提供了這樣的實作。 因此，如果您的元件派生自 PositionComponent，請確保正確設定其大小。 但是，如果您的元件派生自裸元件，則必須手動實作 containsLocalPoint() 方法

如果您的元件是較大層次結構的一部分，那麼只有在其父元件正確實作 containsLocalPoint 時，它才會接收點擊事件

```
class MyComponent extends Component with TapCallbacks {
  final _rect = const Rect.fromLTWH(0, 0, 100, 100);
  final _paint = Paint();
  bool _isPressed = false;

  @override
  bool containsLocalPoint(Vector2 point) => _rect.contains(point.toOffset());

  @override
  void onTapDown(TapDownEvent event) => _isPressed = true;

  @override
  void onTapUp(TapUpEvent event) => _isPressed = false;

  @override
  void onTapCancel(TapCancelEvent event) => _isPressed = false;

  @override
  void render(Canvas canvas) {
    _paint.color = _isPressed? Colors.red : Colors.white;
    canvas.drawRect(_rect, _paint);
  }
}
```

## [DoubleTapCallbacks](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#tapcallbacks)

Flame也提供了一個名為DoubleTapCallbacks的mixin來接收來自元件的雙擊事件。 若要開始在元件中接收雙擊事件，請將 DoubleTapCallbacks mixin 新增至您的 PositionComponent

```
class MyComponent extends PositionComponent with DoubleTapCallbacks {
  @override
  void onDoubleTapUp(DoubleTapEvent event) {
    /// Do something
  }

  @override
  void onDoubleTapCancel(DoubleTapCancelEvent event) {
    /// Do something
  }

  @override
  void onDoubleTapDown(DoubleTapDownEvent event) {
    /// Do something
  }
}
```

# [Migration](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html#migration)

如果您有一個使用 Tappable/Draggable mixins 的現有遊戲，那麼本節將介紹如何轉換到本文檔中所述的新 API。 您需要執行以下操作：

取得所有使用這些 mixin 的元件，並將它們替換為 TapCallbacks/DragCallbacks。 onTapDown、onTapUp、onTapCancel 和 onLongTapDown 方法需要針對新 API 進行調整：

* 參數對（例如（int pointId，TapDownDetails 詳細資訊））已替換為單一事件物件 TapDownEvent 事件
* 不再有回傳值，但如果您需要讓元件將點擊傳遞到下面的元件，請將 event.continuePropagation 設為 true。 僅 onTapDown 事件需要這樣做 - 所有其他事件將自動傳遞
* 如果您的元件需要知道觸控點的座標，請使用 event.localPosition 而不是手動計算。 屬性 event.canvasPosition 和 event.devicePosition 也可用
* 如果元件附加到自訂祖先，請確保該祖先也具有正確的大小或實作 containsLocalPoint()