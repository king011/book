# [padding](https://www.primefaces.org/primeflex/padding)

padding 用於指定內容與邊框之間的距離
<table class="doc-table">
	<thead>
	<tr>
			<th>Class</th>
			<th>Properties</th>
	</tr>
	</thead>
	<tbody>
	<tr>
			<td>p-0</td>
			<td>padding: 0;</td>
	</tr>
	<tr>
			<td>p-1</td>
			<td>padding: 0.25rem;</td>
	</tr>
	<tr>
			<td>p-2</td>
			<td>padding: 0.5rem;</td>
	</tr>
	<tr>
			<td>p-3</td>
			<td>padding: 1rem;</td>
	</tr>
	<tr>
			<td>p-4</td>
			<td>padding: 1.5rem;</td>
	</tr>
	<tr>
			<td>p-5</td>
			<td>padding: 2rem;</td>
	</tr>
	<tr>
			<td>p-6</td>
			<td>padding: 3rem;</td>
	</tr>
	<tr>
			<td>p-7</td>
			<td>padding: 4rem;</td>
	</tr>
	<tr>
			<td>p-8</td>
			<td>padding: 5rem;</td>
	</tr>
	<tr>
			<td>pt-0</td>
			<td>padding-top: 0;</td>
	</tr>
	<tr>
			<td>pt-1</td>
			<td>padding-top: 0.25rem;</td>
	</tr>
	<tr>
			<td>pt-2</td>
			<td>padding-top: 0.5rem;</td>
	</tr>
	<tr>
			<td>pt-3</td>
			<td>padding-top: 1rem;</td>
	</tr>
	<tr>
			<td>pt-4</td>
			<td>padding-top: 1.5rem;</td>
	</tr>
	<tr>
			<td>pt-5</td>
			<td>padding-top: 2rem;</td>
	</tr>
	<tr>
			<td>pt-6</td>
			<td>padding-top: 3rem;</td>
	</tr>
	<tr>
			<td>pt-7</td>
			<td>padding-top: 4rem;</td>
	</tr>
	<tr>
			<td>pt-8</td>
			<td>padding-top: 5rem;</td>
	</tr>
	<tr>
			<td>pr-0</td>
			<td>padding-right: 0;</td>
	</tr>
	<tr>
			<td>pr-1</td>
			<td>padding-right: 0.25rem;</td>
	</tr>
	<tr>
			<td>pr-2</td>
			<td>padding-right: 0.5rem;</td>
	</tr>
	<tr>
			<td>pr-3</td>
			<td>padding-right: 1rem;</td>
	</tr>
	<tr>
			<td>pr-4</td>
			<td>padding-right: 1.5rem;</td>
	</tr>
	<tr>
			<td>pr-5</td>
			<td>padding-right: 2rem;</td>
	</tr>
	<tr>
			<td>pr-6</td>
			<td>padding-right: 3rem;</td>
	</tr>
	<tr>
			<td>pr-7</td>
			<td>padding-right: 4rem;</td>
	</tr>
	<tr>
			<td>pr-8</td>
			<td>padding-right: 5rem;</td>
	</tr>
	<tr>
			<td>pb-0</td>
			<td>padding-bottom: 0;</td>
	</tr>
	<tr>
			<td>pb-1</td>
			<td>padding-bottom: 0.25rem;</td>
	</tr>
	<tr>
			<td>pb-2</td>
			<td>padding-bottom: 0.5rem;</td>
	</tr>
	<tr>
			<td>pb-3</td>
			<td>padding-bottom: 1rem;</td>
	</tr>
	<tr>
			<td>pb-4</td>
			<td>padding-bottom: 1.5rem;</td>
	</tr>
	<tr>
			<td>pb-5</td>
			<td>padding-bottom: 2rem;</td>
	</tr>
	<tr>
			<td>pb-6</td>
			<td>padding-bottom: 3rem;</td>
	</tr>
	<tr>
			<td>pb-7</td>
			<td>padding-bottom: 4rem;</td>
	</tr>
	<tr>
			<td>pb-8</td>
			<td>padding-bottom: 5rem;</td>
	</tr>
	<tr>
			<td>pl-0</td>
			<td>padding-left: 0;</td>
	</tr>
	<tr>
			<td>pl-1</td>
			<td>padding-left: 0.25rem;</td>
	</tr>
	<tr>
			<td>pl-2</td>
			<td>padding-left: 0.5rem;</td>
	</tr>
	<tr>
			<td>pl-3</td>
			<td>padding-left: 1rem;</td>
	</tr>
	<tr>
			<td>pl-4</td>
			<td>padding-left: 1.5rem;</td>
	</tr>
	<tr>
			<td>pl-5</td>
			<td>padding-left: 2rem;</td>
	</tr>
	<tr>
			<td>pl-6</td>
			<td>padding-left: 3rem;</td>
	</tr>
	<tr>
			<td>pl-7</td>
			<td>padding-left: 4rem;</td>
	</tr>
	<tr>
			<td>pl-8</td>
			<td>padding-left: 5rem;</td>
	</tr>
	<tr>
			<td>px-0</td>
			<td>padding-left: 0;<br>
					padding-right: 0;
			</td>
	</tr>
	<tr>
			<td>px-1</td>
			<td>padding-left: 0.25rem;<br>
					padding-right: 0.25rem;
			</td>
	</tr>
	<tr>
			<td>px-2</td>
			<td>padding-left: 0.5rem;<br>
					padding-right: 0.5rem;
			</td>
	</tr>
	<tr>
			<td>px-3</td>
			<td>padding-left: 1rem;<br>
					padding-right: 1rem;
			</td>
	</tr>
	<tr>
			<td>px-4</td>
			<td>padding-left: 1.5rem;<br>
					padding-right: 1.5rem;
			</td>
	</tr>
	<tr>
			<td>px-5</td>
			<td>padding-left: 2rem;<br>
					padding-right: 2rem;
			</td>
	</tr>
	<tr>
			<td>px-6</td>
			<td>padding-left: 3rem;<br>
					padding-right: 3rem;
			</td>
	</tr>
	<tr>
			<td>px-7</td>
			<td>padding-left: 4rem;<br>
					padding-right: 4rem;
			</td>
	</tr>
	<tr>
			<td>px-8</td>
			<td>padding-left: 5rem;<br>
					padding-right: 5rem;
			</td>
	</tr>
	<tr>
			<td>py-0</td>
			<td>padding-top: 0;<br>
					padding-bottom: 0;
			</td>
	</tr>
	<tr>
			<td>py-1</td>
			<td>padding-top: 0.25rem;<br>
					padding-bottom: 0.25rem;
			</td>
	</tr>
	<tr>
			<td>py-2</td>
			<td>padding-top: 0.5rem;<br>
					padding-bottom: 0.5rem;
			</td>
	</tr>
	<tr>
			<td>py-3</td>
			<td>padding-top: 1rem;<br>
					padding-bottom: 1rem;
			</td>
	</tr>
	<tr>
			<td>py-4</td>
			<td>padding-top: 1.5rem;<br>
					padding-bottom: 1.5rem;
			</td>
	</tr>
	<tr>
			<td>py-5</td>
			<td>padding-top: 2rem;<br>
					padding-bottom: 2rem;
			</td>
	</tr>
	<tr>
			<td>py-6</td>
			<td>padding-top: 3rem;<br>
					padding-bottom: 3rem;
			</td>
	</tr>
	<tr>
			<td>py-7</td>
			<td>padding-top: 4rem;<br>
					padding-bottom: 4rem;
			</td>
	</tr>
	<tr>
			<td>py-8</td>
			<td>padding-top: 5rem;<br>
					padding-bottom: 5rem;
			</td>
	</tr>
	</tbody>
</table>

## Single Side Padding

使用 class p-{number} 指定四個方向的 padding，也可以使用 pt/pb/pr/pl 來指定特定方向的 padding

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="pt-5 bg-blue-100 w-12rem m-3 border-round">
            <div class="border-round-bottom bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">pt-5</div>
        </div>
        <div class="pr-8 bg-blue-100 w-12rem m-3 border-round">
            <div class="border-round-left bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">pr-8</div>
        </div>
        <div class="pb-3 bg-blue-100 w-12rem m-3 border-round">
            <div class="border-round-top bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">pb-3</div>
        </div>
        <div class="pl-6 bg-blue-100 w-12rem m-3 border-round">
            <div class="border-round-right bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">pl-6</div>
        </div>
    </div>
</div>
```

## Horizontal Padding

使用 px 來指定水平方向的 padding

![](assets/1.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container yellow-container">
        <div class="px-6 bg-yellow-100 w-12rem m-3 border-round">
            <div class="border-round bg-yellow-500 text-gray-900 font-bold p-3 flex align-items-center justify-content-center">px-6</div>
        </div>
    </div>
</div>
```

## Vertical Padding

使用 py 來指定垂直方向的 padding

![](assets/2.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container green-container">
        <div class="py-5 bg-green-100 w-12rem m-3 border-round">
            <div class="border-round bg-green-500 text-white font-bold p-3 flex align-items-center justify-content-center">py-5</div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同