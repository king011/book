# 模型綁定和驗證

gin 提供了 Bind 和 ShouldBind 來綁定數據源和驗證 

* Bind 在出錯時 爲http返回 400 錯誤碼 和錯誤描述
* ShouldBind 在出錯時 只返回 error 需要調用者 自己做進一步處理

```
package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
)

const addr = `localhost:8080`

func main() {
	l, e := net.Listen(`tcp`, addr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(`work at`, addr)
	// 創建 默認設置的 gin 引擎
	router := gin.Default()

	// 註冊 路由
	router.POST(`/body`, bindBody)
	router.GET(`/url/:id/:lv`, bindURL)
	router.GET(`/query`, bindQuery)

	go request()
	// 運行 服務器
	router.RunListener(l)
}

func request() {
	fv := url.Values{
		"user":     {"kate"},
		"password": {"form"},
		"lv":       {"7"},
	}
	post(`/body`, "application/x-www-form-urlencoded", fv.Encode())
	post(`/body`, "application/json", `{
	"user":"kate",
	"password":"json",
	"lv":8
}`)
	post(`/body`, "application/xml", `<map>
<user>kate</user>
<password>xml</password>
<lv>9</lv>
</map>
}`)
	post(`/body`, "application/x-yaml", `user:    kate
password:    yaml
lv:	10
`)

	get(`/url/game/2`, "")
	qv := url.Values{
		"user":     {"kate"},
		"password": {"query"},
		"lv":       {"7"},
	}
	get(`/query`, qv.Encode())
}
func get(path, query string) {
	u := url.URL{
		Scheme: `http`,
		Host:   addr,
		Path:   path,
	}
	var getURL string
	if query == "" {
		getURL = u.String()
	} else {
		getURL = u.String() + "?" + query
	}
	r, e := http.Get(getURL)
	if e != nil {
		log.Fatalln(e)
		return
	}
	ioutil.ReadAll(r.Body)
	r.Body.Close()
}
func post(path, contentType, body string) {
	u := url.URL{
		Scheme: `http`,
		Host:   addr,
		Path:   path,
	}
	r, e := http.Post(u.String(), contentType, bytes.NewBufferString(body))
	if e != nil {
		log.Fatalln(e)
		return
	}
	ioutil.ReadAll(r.Body)
	r.Body.Close()
}
func bindBody(c *gin.Context) {
	var obj struct {
		User     string `form:"user" json:"user" xml:"user" yaml:"user" binding:"required"`
		Password string `form:"password" json:"password" xml:"password"  yaml:"password" binding:"required"`
		Lv       int32  `form:"lv" json:"lv" xml:"lv"  yaml:"lv"`
	}
	// Bind 會依據 ContentType 進行 bind
	// 如果明確知道類型 可以使用 BindJSON ... 綁定
	e := c.Bind(&obj)
	if e != nil {
		return
	}
	fmt.Println(obj)
	c.String(http.StatusOK, `success`)
}
func bindURL(c *gin.Context) {
	var obj struct {
		ID string `uri:"id" binding:"required"`
		Lv string `uri:"lv" binding:"required,min=1"`
	}
	e := c.BindUri(&obj)
	if e != nil {
		return
	}
	fmt.Println(obj)
	c.String(http.StatusOK, `success`)
}

func bindQuery(c *gin.Context) {
	var obj struct {
		User     string `form:"user" binding:"required"`
		Password string `form:"password"  binding:"required"`
		Lv       int32  `form:"lv"`
	}
	e := c.BindQuery(&obj)
	if e != nil {
		return
	}
	fmt.Println(obj)
	c.String(http.StatusOK, `success`)
}
```

# binding 指定了如何 驗證數據
[go-playground/validator.v10](https://godoc.org/gopkg.in/go-playground/validator.v10#hdr-Baked_In_Validators_and_Tags) 文檔有 binding 的詳細使用說明

binding 也允許註冊自定義的 驗證器

```
package main

import (
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator"
)

// Booking contains binded and validated data.
type Booking struct {
	CheckIn time.Time `form:"check_in" binding:"required,bookabledate" time_format:"2006-01-02"`
}

var bookableDate validator.Func = func(fl validator.FieldLevel) bool {
	date, ok := fl.Field().Interface().(time.Time)
	if ok {
		today := time.Now()
		if date.After(today) {
			// 返回 false 無法通過
			return false
		}
	}
	// 返回 true 通過驗證
	return true
}

const addr = `localhost:8080`

func main() {
	l, e := net.Listen(`tcp`, addr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(`work at`, addr)
	// 創建 默認設置的 gin 引擎
	router := gin.Default()
	// 註冊自定義 驗證器
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("bookabledate", bookableDate)
	}
	// 註冊 路由
	router.GET("/bookable", getBookable)

	go request()
	// 運行 服務器
	router.RunListener(l)
}

func request() {

	qv := url.Values{
		"check_in": {"2020-05-15"},
	}
	get(`/bookable`, qv.Encode())
}
func get(path, query string) {
	u := url.URL{
		Scheme: `http`,
		Host:   addr,
		Path:   path,
	}
	var getURL string
	if query == "" {
		getURL = u.String()
	} else {
		getURL = u.String() + "?" + query
	}
	r, e := http.Get(getURL)
	if e != nil {
		log.Fatalln(e)
		return
	}
	ioutil.ReadAll(r.Body)
	r.Body.Close()
}

func getBookable(c *gin.Context) {
	var b Booking
	e := c.Bind(&b)
	if e != nil {
		log.Println(e)
		return
	}
	log.Println(b)
	c.String(http.StatusOK, `success`)
}
```
