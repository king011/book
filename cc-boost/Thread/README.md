# [Thread](https://www.boost.org/doc/libs/1_76_0/doc/html/thread.html)

```
#include <boost/thread.hpp>
```
```
find_package(Boost
    1.76.0
    REQUIRED
    COMPONENTS
        thread
)
```

* Author(s) -> Anthony Williams and Vicente J. Botet Escriba
* First Release -> 1.25.0
* Categories -> Concurrent Programming, System
* Portable C++ multi-threading -> C++03, C++11, C++14, C++17

Thread 庫提供了 跨平臺的 線程以及配套工具的支持

