# [應用探索](https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-intro/)

在應用部署中 創建 Deployment 時，k8s添加了一個 Pod 來托管應用實例。Pod 是 k8s 抽象出來的，表示一組一個或多個應用程序容器，以及這些容器的一些共享資源。這些資源包括：

* 共享存儲，當作卷
* 網路，作爲唯一的集羣 ip 地址
* 有關每個容器如何運行的信息，例如容器鏡像版本或要使用的特定端口

Pod 是 k8s 平臺上的原子單元。當在 k8s 創建 Deployment 時，該 Deployment 會在其中創建包含容器的 Pod。每個 Pod 都與調度它的工作節點綁定，並保存在那裏直到終止或刪除。如果工作節點發送故障，則會在集羣中的其它可用工作節點上調度相同的 Pod。

# Pod 概述

![](assets/module_03_pods.svg)

# 工作節點

一個 Pod 總是運行在 工作節點。工作節點是 k8s 中參與計算的機器，可以是虛擬機或物理計算機。每個工作節點由主節點管理。工作節點可以由多個 Pod ， k8s 主節點會自動處理在集羣中的工作節點上調度 Pod 。主節點的自動調度考量了每個工作節點上的可用資源。

每個工作節點至少要運行：
* kubectl，負責 k8s 主節點和工作節點之間的通信；它管理 Pod 個機器上運行的容器
* 容器運行時負責從倉庫中提取容器鏡像，解壓縮容器以及運行應用

![](assets/module_03_nodes.svg)

# 使用 kubectl 進行故障排除

kubectl 提供了一些命令行用於 故障排除 常用的由:

* kubectl get -> 列出資源
* kubectl describe -> 顯示有關資源的詳細信息
* kubectl logs -> 打印 pod 和 其中容器的日誌
* kubectl exec -> 在 pod 中的容器上執行命令

# 探索

1. 使用 `kubectl get` 指令來獲取現有的 Pod

	```
	kubectl get pods
	```
	
	使用 `kubectl describe` 指令來獲取 Pod 詳情
	
	```
	kubectl describe pods
	```
	
	```
	kubectl describe pods/$POD_NAME
	```
	
1. 啓動好 代理 以便和 應用交互

	```
	kubectl proxy
	```

	設置 POD_NAME
	
	```
	export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
	echo Name of the Pod: $POD_NAME
	```
	
	查看應用輸出
	
	```
	curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/
	```

1. 通常應用發送到 stdout 的內容 都會作爲 Pod 日誌，使用  `kubectl logs` 查看

	```
	kubectl logs $POD_NAME
	```
	
1. 使用 `kubectl exec` 可以直接在 Pod 的容器上執行命令

	```
	kubectl exec $POD_NAME -- env
	```
	
	```
	kubectl exec -ti $POD_NAME -- bash
	```