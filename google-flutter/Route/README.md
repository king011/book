# 路由管理

flutter 提供了路由 來實現頁面間的 轉換

flutter 維護了一個 路由棧 使用 push 將頁面入棧並顯示 使用 pop 彈出當前頁面顯示 棧中的 上個頁面

```dart
Navigator.of(context).push(
	MaterialPageRoute(
		builder: (context) {
			return Text("new page");
		},
	),
);
```

flutter 提供了 pushAndRemoveUntil 和 push 用法一樣 但在入棧前先刪除 其它 路由

pushAndRemoveUntil 多接收一個 **bool Function(Route&lt;dynamic&gt; route)** 參數

flutter 會沿着棧頂 依次調用此函數 如果 返回 false 刪除 路由 返回 true 不刪除 並且停止 對後續路由調用 刪除判斷

```dart
Navigator.of(context).push(
	MaterialPageRoute(
		builder: (context) {
			return Text("new page");
		},
		(route) => false,
	),
);
```

# 命名路由

可以在 MaterialApp.routes 爲路由註冊名稱 之後使用 名稱 來調整 pushNamed

```
return new MaterialApp(
  title: 'Flutter Demo',
  theme: new ThemeData(
    primarySwatch: Colors.blue,
  ),
  // 註冊路由表
  routes:{
   "new_page":(context)=>NewRoute(),
  } ,
  home: new MyHomePage(title: 'Flutter Demo Home Page'),
);
```