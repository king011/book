# [flutter](https://appimage-builder.readthedocs.io/en/latest/examples/flutter.html)

1. 首先需要安裝 [appimage-builder](https://github.com/AppImageCrafters/appimage-builder/releases)，直接下載對應平臺的 appimage-builder-\*.AppImage 到 $PATH 路徑中
2. 使用 flutter 編譯好一個 linux 平臺的輸出目標，並將其拷貝(**build/linux/x64/release/bundle**)出來作爲 AppImage 的根目錄
3. 執行 `appimage-builder –generate` 分析工程

	```
	appimage-builder --generate
	```
	
	```
	Basic Information :
	? ID [Eg: com.example.app] : com.example.flutter_hello
	? Application Name : Flutter Hello
	? Icon : utilities-terminal
	? Version : latest
	? Executable path relative to AppDir [usr/bin/app] : hello_flutter
	? Arguments [Default: $@] : $@
	? Update Information [Default: guess] : guess
	? Architecture :  amd64
	```
	
# 本喵自己的解決方案

上述官方方案太麻煩了，並且沒有打包 glibc 在 glibc 不兼容的系統上依然無法正常運行，本喵的方案是寫如下述腳本

1. 它使用 ldd 把所有依賴到拷貝到到 AppDir 包括 glibc
2. 使用 patchelf 修復 elf 強制使用打包版本的 ld-linux-x86-64.so.2 加載動態庫

> 這個方法並不完美它會在 /tmp/ld-linux.so.hook.d 檔案夾下釋放打包的 ld-linux-x86-64.so.2 刪除應用時需要手動刪除，好在 ld-linux-x86-64.so.2 很小對用戶的影響幾乎可以忽略

你可以[在此](https://blog.king011.com/?p=351)查看到更詳細的分析過程

使用前記得將第12到38行的變量修改爲你真實情況(除了 flutter 你也可以參考這個方案打包其它app)

```
#!/usr/bin/env bash
#Program:
#	flutter 自動打包到 AppImage
#History:
#       2025-02-07 
#Email:
#	zuiwuchang@gmail.com
#

# 請將下述變量修改爲你的真實環境

# appimagetool 工具路徑，請從此處下載 https://github.com/AppImage/AppImageKit/releases/latest
Appimagetool="appimagetool-x86_64"
# patchelf 工具路徑，請從此處下載 https://github.com/NixOS/patchelf/releases/latest
Patchelf="patchelf"

# 要創建的 AppImage 檔案夾位置
AppDir="bin/myapp"
# 要寫入的 AppImage 應用名稱
AppName="com.king011.myapp"
# 桌面圖片，會自動拷貝到 "$AppDir/myapp.png"
AppLogo="assets/logo.png"
# 要執行的進程名稱(flutter 打包輸出的可執行程式)
AppExec="myapp"
# flutter 產出的軟件包
# 如果是目錄，會自動複製到 "$AppDir/usr/bin" 下面，如果是壓縮包(僅支持常見的 tar 系列)會自動解壓
AppSource="bin/myapp.tar.gz"
# 要輸出的 AppImage 名稱
AppOutput="bin/myapp.AppImage"

# 要打包的動態庫連接程序，通常不需要填寫，腳本會嘗試自動獲取
# /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
DynamicLinkerPath=""
# 動態庫連接程序版號，通常不需要填寫，腳本會嘗試自動獲取
# /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 --version
DynamicLinkerVersion=""
# 要將連接器釋放的檔案夾路徑，默認爲 /tmp/ld-linux.so.hook.d
DynamicLinkerDir=""

set -e
BashDir=$(cd "$(dirname $BASH_SOURCE)" && pwd)
if [[ "$Command" == "" ]];then
    Command="$0"
fi

function help(){
    echo "use AppImage packaging for linux"
    echo
    echo "Usage:"
    echo "  $Command [flags]"
    echo
    echo "Flags:"
    echo "  -c, --create       create AppImage dir"
}

ARGS=`getopt -o hc --long help,create -n "$Command" -- "$@"`
eval set -- "${ARGS}"
CREATE=0

while true
do
    case "$1" in
        -h|--help)
            help
            exit 0
        ;;
        -c|--CREATE)
            CREATE=1
            shift
        ;;
        --)
            shift
            break
        ;;
        *)
            echo Error: unknown flag "$1" for "$Command"
            echo "Run '$Command --help' for usage."
            exit 1
        ;;
    esac
done

SO=0
doDeps(){
    local str="`ldd "$1" |egrep '\(0x[0-9a-fA-F]+\)'`"
    ifs=$IFS
    IFS="
"
    local strs=($str)
    IFS=$ifs
    local s
    local name
    local path
    for s in "${strs[@]}";do
    
        name=${s%=>*}
        name=${name%(0x*}
        name="${name#*[[:space:]]}"
        name=${name##*/}        
        name="${name%*[[:space:]]}"
        
        path=${s##*=>}
        path=${path%(0x*}
        path="${path#*[[:space:]]}"
        path="${path%*[[:space:]]}"

        if [[ "$path" =~ ^\/.+ ]];then
            if [[ ! -f "$2/$name" ]];then
                SO=$((SO+1))
                echo " $SO. '$path' => '$2/$name'"
                cp "$path" "$2/$name"

                doDeps "$path" "$2"
            fi
        fi
    done
}

doCreateDir(){
    if [[ -d "$AppDir" ]];then
        rm "$AppDir" -rf
    fi
    
    mkdir "$AppDir/lib" -p
    if [[ -d "$AppSource" ]];then
        cp "$AppSource" "${AppDir}/bin" -r
    elif [[ -f "$AppSource" ]];then
        mkdir "$AppDir/bin" -p
        if [[ "$AppSource" = *.tar ]];then
            tar -xvf "$AppSource" -C "${AppDir}/bin/"
        elif [[ "$AppSource" = *.tgz ]] || [[ "$AppSource" = *.tar.gz ]];then
            tar -zxvf "$AppSource" -C "${AppDir}/bin/"
        elif [[ "$AppSource" = *.tbz ]] || [[ "$AppSource" = *.tar.bz2 ]];then
            tar -jxvf "$AppSource" -C "${AppDir}/bin/"
        elif [[ "$AppSource" = *.txz ]] || [[ "$AppSource" = *.tar.xz ]];then
            tar -jxvf "$AppSource" -C "${AppDir}/bin/"
        else
            echo "AppSource format unknow: $AppSource"
            exit 1
        fi
    else
        echo "AppSource not found: $AppSource"
        exit 1
    fi
   

    # 寫入配置
    local filepath="$AppDir/myapp.desktop"
    echo "[Desktop Entry]" > "$filepath"
    echo "Name=$AppName" >> "$filepath"
    echo "Exec=$AppExec" >> "$filepath"
    echo "Icon=myapp" >> "$filepath"
    echo "Type=Application" >> "$filepath"
    echo "Categories=Utility" >> "$filepath"

    cp "$AppLogo" "$AppDir/myapp.png"

    # 拷貝直接依賴
    doDeps "$AppDir/bin/$AppExec" "$AppDir/lib"

    local ifs=$IFS
    IFS="
"
    # 拷貝 lib 檔案夾下的間接依賴
    local files=(`find "$AppDir/bin/lib" -maxdepth 1 -type f`)
IFS=$ifs
    for file in "${files[@]}";do
        if [[ -f "$file" ]];then
            doDeps "$file" "$AppDir/lib"
        fi
    done

    # 刪除與 bin/lib 中重複的依賴檔案
    for file in "${files[@]}";do
        name=${file##*/}
        if [[ -f "$AppDir/lib/$name" ]];then
            echo rm "$AppDir/lib/$name"
            rm "$AppDir/lib/$name"
        fi
    done

    # 拷貝動態庫連接程序
    if [[ "$DynamicLinkerPath" == "" ]];then
        DynamicLinkerPath="/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2"
    fi
    if [[ ! -f "$DynamicLinkerPath" ]];then
        echo "DynamicLinkerPath not found: $DynamicLinkerPath"
        exit 1
    fi
    local name=${DynamicLinkerPath##*/}
    if [[ "$name" == "" ]];then
        echo "DynamicLinkerPath not found: $DynamicLinkerPath"
        exit 
    fi
    echo "DynamicLinkerPath: $DynamicLinkerPath"
    if [[ ! -f "$AppDir/lib/$name" ]];then
        cp "$DynamicLinkerPath" "$AppDir/lib/"
    fi
    if [[ "$DynamicLinkerVersion" == "" ]];then
        DynamicLinkerVersion=`"$DynamicLinkerPath" --version | egrep version`
        DynamicLinkerVersion=${DynamicLinkerVersion##*version }
    fi
    if [[ "$DynamicLinkerVersion" =~ ^[0-9]+\.[0-9]+\.[0-9]*$ ]];then
        echo "DynamicLinkerVersion: $DynamicLinkerVersion"
    else
        echo "DynamicLinkerVersion not supported: '$DynamicLinkerVersion'"
        exit 1
    fi
    if [[ "$DynamicLinkerDir" == "" ]];then
        DynamicLinkerDir="/tmp/ld-linux.so.hook.d"
    fi
    if [[ "$DynamicLinkerDir" != */ ]];then
        DynamicLinkerDir="$DynamicLinkerDir/"
    fi
    echo "DynamicLinkerDir: $DynamicLinkerDir"

    # 修補 flutter 可執行檔案，指定使用專用的動態庫加載器
    local ld="${DynamicLinkerDir}ld-linux-x86-64.so.$DynamicLinkerVersion"
    echo "'$Patchelf' --set-interpreter '$ld' '$AppDir/bin/$AppExec'"
    "$Patchelf" --set-interpreter "$ld" "$AppDir/bin/$AppExec"

    # 寫入依賴啓動腳本
    filepath="$AppDir/AppRun"
    echo '#!/bin/bash' > "$filepath"
    echo 'set -e' >> "$filepath"
    echo 'if [[ "$APPDIR" == "" ]];then' >> "$filepath"
    echo '  APPDIR=$(cd "$(dirname "$BASH_SOURCE")" && pwd)' >> "$filepath"
    echo 'fi' >> "$filepath"
    echo 'if [[ ! -f "'"$ld"'" ]];then' >> "$filepath"
    echo '  mkdir -p "'"$DynamicLinkerDir"'"' >> "$filepath"
    echo '  chmod 777 "'"$DynamicLinkerDir"'"' >> "$filepath"
    echo '  cp "$APPDIR/lib/'"$name"'" "'"$ld"'"' >> "$filepath"
    echo 'fi' >> "$filepath"
    echo 'if [[ "$LD_LIBRARY_PATH" == "" ]];then' >> "$filepath"
    echo '  export LD_LIBRARY_PATH="$APPDIR/lib"' >> "$filepath"
    echo 'else' >> "$filepath"
    echo '  export LD_LIBRARY_PATH="$APPDIR/usr/lib:$LD_LIBRARY_PATH"' >> "$filepath"
    echo 'fi' >> "$filepath"
    echo '"$APPDIR/bin/'"$AppExec"'" "$@"' >> "$filepath"
    chmod a+x "$filepath"

    echo Appimage dir ready
}
if [[ -d "$AppDir" ]];then
    if [[ $CREATE == 1 ]] ;then
        doCreateDir
    fi
else
    doCreateDir
fi

"$Appimagetool" "$AppDir" "$AppOutput"
du "$AppOutput" -h
sha256sum "$AppOutput" > "$AppOutput.sha256.txt"
cat "$AppOutput.sha256.txt"
```