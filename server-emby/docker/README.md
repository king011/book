# docker

emby 官方提供了一個 [docker](https://hub.docker.com/r/emby/embyserver) 鏡像，linuxserver 團隊也維護了一份 [docker](https://hub.docker.com/r/linuxserver/emby) 鏡像，推薦使用 linuxserver 維護的版本

```
docker run -d \
    --name emby \
    -e LANG=en_US.UTF-8 \
    -e TZ=Asia/Shanghai \
    --device /dev/dri:/dev/dri \
    -e PUID=1000 \
    -e PGID=1000 \
    -v your_data:/data \
    -v your_config:/config \
    -p 12345:8096 \
    linuxserver/emby:latest
```