# 集合

集合 set 是 string 類型的無序集合

集合中最大成員數爲 pow(2,32-1) 個 (4294967295,超過40億)

| 命令 | 含義 |
| -------- | -------- |
| SADD key member1 [member2]     | 向集合添加成員     |
| SCARD key     | 獲取集合成員     |
| SDIFF key1 [key2]     | 返回給定所有集合的差集     |
| SDIFFSTORE destination key1 [key2]     | 返回給定集合差集並存儲到 destination     |
| SINTER key1 [key2]     | 返回給定集合的交集     |
| SINTERSTORE destination key1 [key2]     | 返回給定集合的交集並存儲到 destination     |
| SISMEMBER key member     | 判斷 member 是否是集合的成員     |
| SMEMBERS key     | 返回集合中的所有成員     |
| SMOVE source destination member     | 將 member 元素從 source 集合移動到 destination 集合     |
| SPOP key     | 移除並返回集合中的一個隨機元素     |
| SRANDMEMBER key [count]     | 返回集合中的一個或多個隨機元素     |
| SREM key member1 [member2]     | 移除集合成員     |
| SUNION key1 [key2]     | 發揮集合並集     |
| SUNIONSTORE destination key1 [key2]     | 所有給定集合的並集存儲在 destination 集合中     |
| SSCAN key cursor [MATCH pattern] [COUNT count]     | 迭代集合中的元素     |

