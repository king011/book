# [k8s集羣](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-intro/)

k8s 協調一個告可用的 計算機集羣 每個計算機作爲獨立的單元相互連接工作

一個 k8s 集羣包含兩種類型的資源:
* Master 調度整個集羣
* Nodes 負責運行應用

![](assets/module_01_cluster.svg)

**Master 負責管理整個集羣。** Master協調集羣中的所有活動，例如調度應用、維護應用的所需狀態、應用擴容以及推出更新。

**Node 是一個虛擬極或者物理機，它在 k8s 集羣中充當工作機器的角色** 每個 Node 都有 kubelet，它管理 Node 而且是 Node 與 Master 通信的代理。 Node 還應該具有用於處理容器的工具，例如 Docker 或 rkt 。 處理生產級流量的 k8s 集羣至少應該具有 三個 Node。

在 k8s 上部署應用時，你告訴 Master 啓動容器。 Master 就編排容器在集羣的 Node 上運行。 **Node 使用 Master 暴露的 k8s api 與 Master 通信。**終端用戶也可以使用 k8s api 與集羣交互。

# minikube

minikube 是一種輕量級的 k8s 實現 ，可以在本地計算機上創建 vm 並部署僅包含一個節點的簡單集羣，以便開始學習或開發。

```
# 打印 minikube 版本信息
minikube version

# 啓動 k8s 集羣
minikube start

# 打印 k8s 版本信息
kubectl version

# 打印 集羣 詳細信息
kubectl cluster-info

# 打印 集羣 節點信息
kubectl get nodes
```