# Sunshine

[Sunshine](https://github.com/LizardByte/Sunshine) 是開源(GPL3.0)的串流解決方案支持使用 [Moonlight](https://github.com/moonlight-stream) 進行連接

目前(2024-09-27) 更新了最新 NVIDIA 管理程序的已經完全沒有 GAMESTREAM 了，目前可行方案只有使用 Sunshine

# Windows
從[官方倉庫](https://github.com/LizardByte/Sunshine/releases) 下載最新的 sunshine-windows-installer.exe 安裝即可，它將以服務運行

也可以下載 sunshine-windows-portable.zip 但不推薦，這需要自己輸入命令啓動 Sunshine，這不符合串流遊戲的正常使用方案

# [設置](https://docs.lizardbyte.dev/projects/sunshine/en/latest/about/advanced_usage.html#configuration)

Sunshine 默認提供了比 NVIDIA 服務器更多的設置，你可以通過網頁設置或者修改設定檔案

下面是各平臺默認設定檔案夾路徑

| 平臺 | 路徑 |
| -------- | -------- |
| Docker     | /config/     |
| Linux     | ~/.config/sunshine/     |
| macOS     | ~/.config/sunshine/     |
| Windows     | %ProgramFiles%\Sunshine\config     |

默認核心設定檔案是一個名爲 sunshine.conf 的檔案


## [port](https://docs.lizardbyte.dev/projects/sunshine/en/latest/about/advanced_usage.html#port)


```
port = 47989
```

Sunshine 提供了一個 web ui，默認運行在 https 47990 端口上，使用瀏覽器進入即可設定。當新設備連接時也可以在此允許設備添加

> 第一次登入會要求創建默認用戶密碼，後續訪問需要使用此用戶名和密碼

下面是 Sunshine 需要使用的端口，記得設置好防火牆規則放行


| 端口 | 描述 | 偏移 |
| -------- | -------- | -------- |
| ----TCP---- |  |  |
| 47984 TCP     | HTTPS     | -5     |
| 47989 TCP     | HTTP     | 0     |
| 47990 TCP     | WebUI     | +1     |
| 48010 TCP     | RTSP     | +21     |
| ----UDP---- |  |  |
| 47998 UDP     | Video     | +9     |
| 47999 UDP     | Control     | +10     |
| 48000 UDP     | Audio     | +11     |
| 48002 UDP     | Mic (unused)     | +13     |

* 默認端口: **47989**
* 路由轉發: **47984-48010**

## [高級設置](https://docs.lizardbyte.dev/projects/sunshine/en/latest/about/advanced_usage.html#advanced)

高級設置下的設定直接影響了遠程串流的質量，建議依據自己情況多設置一些值測試找出對自己來說最合適的值

### [fec\_percentage](https://docs.lizardbyte.dev/projects/sunshine/en/latest/about/advanced_usage.html#fec-percentage)

```
fec_percentage = 20
```

有效範圍是 1-255，這個值設置了每個視訊影格中每個資料包的糾錯包百分比

較高的值可以修正更多的網路封包遺失，但代價是增加頻寬使用量。意思也就是說值越大，丟包造成的影響越小，但越耗寬帶。理論上不考慮寬帶和運營商值越大串流效果越好，但實際上是
1. 不能讓寬帶佔用超過你實際的寬帶，不然顯然會有大量丟包
2. 假設運營商分配給你 100Mbps 寬帶，但無良運營商可能實際只給了你 50Mbps，或者 udp(串流時主要是 udp 流量，運營商很不喜歡 udp) 流量時只有 20Mbps，所以你需要測試自己真實的寬帶來調整 fec 設定
3. 高峰期，運營商給你的寬帶可能會打折扣
4. 運營商不太喜歡 udp，所以如果你佔用大量 udp 寬帶，運營商可能會大量丟棄你的數據包

所以你需要自己評估上述問題，並測試來找到一個對自己來說合適的 fec 值

