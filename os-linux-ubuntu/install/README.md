# windows下 硬盤安裝

1. 安裝EasyBCD

2. 使用EasyBCD增加引導

3. 編輯引導內容如下

    ```
    title Install Ubuntu
    root (hd0,0)
    kernel (hd0,0)/vmlinuz boot=casper iso-scan/filename=/ubuntu-13.10-desktop-i386.iso ro quiet splash locale=zh_CN.UTF-8
    initrd (hd0,0)/initrd.lz
    ```
	 
4. 將安裝鏡像複製到(hd0,0)

5. 將鏡像中的 **casper/vmlinuz casper/initrd.lz** 複製到(hd0,0)

6. 重啓計算機 進入ubuntu

7. 執行命令 **sudo umount -l /isodevice** 後 開始安裝

## windows下 刪除 linux

```
MbrFix64.exe /drive 0 fixmbr /yes
```

[mbrfix.zip](assets/mbrfix.zip)

# u盤安裝
1. 將 iso鏡像 製作爲u盤 啓動鏡像
2. 使用 u盤 啓動系統 安裝即可

## u盤安裝卡死在 logo
1. 在 grub 界面 安 **e** 編輯 啓動設置
2. 將第二行的 **quiet splash** 改爲 **nomodeset**
3. F10 保存 並啓動

## 安裝後 卡死在 logo

1. 重啓 光標選中 **Ubuntu** 按 **e** 在 上述修改那行 加上 **acpi_osi=Linux nomodeset** 
2. F10 保存 並啓動
3. 進入系統 安裝最新顯卡驅動
4. 將 grub 改回默認值

上述步驟1 中添加內容是針對 nvidia显卡 若使用不同顯卡需修改成相應值

| 顯卡 | 取值 |
| -------- | -------- |
| nvidia     | acpi\_osi=Linux nomodeset     |
| amd     | acpi\_osi=linux xforcevesa 或 acpi\_osi=linux radeon.modeset=0 xforcevesa     |
| intel     | acpi\_osi=linux i915.modeset=1 或 acpi\_osi=linux i915.modeset=0     |


# uefi

如果 使用 uefi 引導 出現 **Press ESC in 5 seconds to skip startup.nsh, any other key to continue** 後無法啓動

執行   
fs0:  
edit startup.nsh  
輸入 **\efi\ubuntu\grubx64.efi**   
ctrl + s 之後 enter 保存  
ctrl + q 之後 enter 退出


**另一種 處理方式** 是在 shell中執行 exit 進入 管理界面 

1. Boot Maintenance Manager
2. Boot Options
3. Add Boot Option
4. 為 grubx64.efi 創建一個 引導列
5. 將 grubx64.efi 設置到 啟動順序的 前面


## 管理

在 linux 執行 sudo efibootmgr 命令 可以 對 uefi 進行管理

```
# 打印 當前 啟動 順序
sudo efibootmgr -v

# 調整 啟動先後 順序
sudo efibootmgr -o 0,1,2

# 將 boot manager 超時設置為1秒
sudo efibootmgr -t 1
```

> 在 vbox 下 無法修改 uefi 
> 
> 冷重啟後 設置會被 還原 只能通過 編譯 startup.nsh 檔案的 方式啟動

# 必要設定

通常 ubuntu 已經默認設定好大多數 配置 基本可以開箱即用 不過依然有一些常用設置可能需要自己設定

## vim

ubuntu 自動的 vim 是不完整的 故需要自行安裝 完整版

```bash
sudo apt install vim -y
```

ubuntu 默認使用 nano 作爲編輯器 執行如下指令修改配置

```bash
sudo update-alternatives --config editor
```

## sudo 免密碼

執行 `sudo visudo` 後填入如下內容

```
用戶名    ALL=(ALL:ALL) NOPASSWD:ALL
```

需要 將上述內容 添加到 如下設置之後 否則會被 此設置覆蓋
```
%admin ALL=(ALL) ALL		
%sudo ALL=(ALL:ALL) ALL
``` 

## 修改時區

如果時區不對 需要 將 **/etc/localtime** 設置軟鏈接到 **/usr/share/zoneinfo** 目錄中的 時區設置

```
sudo ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
```

另外 ubuntu 提供了 timedatectl 指令來 設置 和查看時區

```
sudo timedatectl set-timezone Asia/Shanghai
```

```
king@xsd-ubuntu18:~$ timedatectl 
                      Local time: Fri 2020-09-11 11:04:13 CST
                  Universal time: Fri 2020-09-11 03:04:13 UTC
                        RTC time: Fri 2020-09-11 03:04:13
                       Time zone: Asia/Shanghai (CST, +0800)
       System clock synchronized: yes
systemd-timesyncd.service active: yes
                 RTC in local TZ: no
```

## 開機等待時間
如果安裝了多系統，那麼在開機時默認會有一個10秒的等待時間讓你選擇要進入的操作系統，可以編輯 **/etc/default/grub** 並且將 **GRUB\_TIMEOUT** 設置爲需要等待的秒數(最小值是 1)

```
GRUB_TIMEOUT=1
```


## 禁用客戶會話

編輯 **/etc/lightdm/lightdm.conf** 添加如下內容 後重啓生效

```
[SeatDefaults]
allow-guest=false
```

## nautilus-open-terminal

在右鍵菜單增加 在當前文件夾打開終端功能 安裝上 nautilus-open-terminal 即可

```
sudo apt install nautilus-open-terminal
```