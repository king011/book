# console

console 提供了一個  log warn error 三個函數 用於 輸出 一個 字符串到控制檯

* 同樣 太過簡陋 不建議使用
* 另外 console 依賴 util.format 格式化 輸出


```
package main

import (
	"fmt"
	"log"

	"github.com/dop251/goja"
	_ "github.com/dop251/goja_nodejs/console"
	"github.com/dop251/goja_nodejs/require"
)

func main() {
	// 創建 模塊 登記處
	registry := require.NewRegistry()

	// 創建 虛擬機
	vm := goja.New()
	vm.Set(`println`, func(args ...interface{}) {
		fmt.Println(args...)
	})
	registry.Enable(vm) // 爲 Runtime 啓用模塊
	_, e := vm.RunScript(`main.js`, `
var console = require('console')
console.log('log')
console.warn('warn')
console.error('error')
	`)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(registry)
}
```