# [手勢輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html)

這是直接附加在遊戲類別上的手勢輸入的文檔，大多數時候您想要檢測組件上的輸入，請參閱 [TapCallbacks](https://docs.flame-engine.org/1.16.0/flame/inputs/tap_events.html) 和 [DragCallbacks](https://docs.flame-engine.org/1.16.0/flame/inputs/drag_events.html) 等。

其他輸入文檔，另請參閱：

* [鍵盤輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/keyboard_input.html)：用於擊鍵
* [其他輸入](https://docs.flame-engine.org/1.16.0/flame/inputs/other_inputs.html)：用於操縱桿、遊戲手把等。

# [Intro](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#intro)


在 **package:flame/gestures.dart** 中，你可以找到一整套的 mixins，它們可以 with 到 Game 中，以便能夠接收觸摸輸入事件。下面是這些 mixins 及其方法的完整列表

## [Touch and mouse detectors](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#touch-and-mouse-detectors)
```
- TapDetector
  - onTap
  - onTapCancel
  - onTapDown
  - onLongTapDown
  - onTapUp

- SecondaryTapDetector
  - onSecondaryTapDown
  - onSecondaryTapUp
  - onSecondaryTapCancel

- TertiaryTapDetector
  - onTertiaryTapDown
  - onTertiaryTapUp
  - onTertiaryTapCancel

- DoubleTapDetector
  - onDoubleTap

- LongPressDetector
  - onLongPress
  - onLongPressStart
  - onLongPressMoveUpdate
  - onLongPressUp
  - onLongPressEnd

- VerticalDragDetector
  - onVerticalDragDown
  - onVerticalDragStart
  - onVerticalDragUpdate
  - onVerticalDragEnd
  - onVerticalDragCancel

- HorizontalDragDetector
  - onHorizontalDragDown
  - onHorizontalDragStart
  - onHorizontalDragUpdate
  - onHorizontalDragEnd
  - onHorizontalDragCancel

- ForcePressDetector
  - onForcePressStart
  - onForcePressPeak
  - onForcePressUpdate
  - onForcePressEnd

- PanDetector
  - onPanDown
  - onPanStart
  - onPanUpdate
  - onPanEnd
  - onPanCancel

- ScaleDetector
  - onScaleStart
  - onScaleUpdate
  - onScaleEnd

- MultiTouchTapDetector
  - onTap
  - onTapCancel
  - onTapDown
  - onTapUp

- MultiTouchDragDetector
  - onReceiveDrag
```

Mouse only events:

```
 - MouseMovementDetector
  - onMouseMove
 - ScrollDetector
  - onScroll
```

無法將高階偵測器 (MultiTouch\*) 與同類基本偵測器混合使用，因為高階偵測器將始終贏得手勢，而基本偵測器永遠不會被觸發。 例如，您不能同時使用 MultiTouchTapDetector 和 PanDetector，因為後者不會觸發任何事件（對此也有斷言）

Flame 的 GestureApi 是由 Flutter 的 Gesture Widgets 提供的，包括 [GestureDetector widget](https://api.flutter.dev/flutter/widgets/GestureDetector-class.html)、[RawGestureDetector widget](https://api.flutter.dev/flutter/widgets/RawGestureDetector-class.html) 和 [MouseRegion widget](https://api.flutter.dev/flutter/widgets/MouseRegion-class.html)，您也可以在[這裡](https://api.flutter.dev/flutter/gestures/gestures-library.html)閱讀更多關於 Flutter 手勢的資訊


# [PanDetector and ScaleDetector](PanDetector and ScaleDetector)

如果您將 PanDetector 與 ScaleDetector 一起添加，系統會提示您一條來自 Flutter 的相當的斷言：

> 同時擁有平移手勢辨識器和縮放手勢辨識器是多餘的； scale 是 pan 的超集。  
> 只需使用 ScaleDetector 即可。

這可能看起來很奇怪，但 onScaleUpdate 不僅在應更改比例時觸發，而且在所有平移/拖曳事件中也會觸發。 因此，如果您需要使用這兩個偵測器，則必須在 onScaleUpdate（+onScaleStart 和 onScaleEnd）內處理它們的邏輯

例如，如果您想在平移事件上移動相機並在比例事件上縮放，您可以執行以下操作：

```
  late double startZoom;

  @override
  void onScaleStart(_) {
    startZoom = camera.zoom;
  }

  @override
  void onScaleUpdate(ScaleUpdateInfo info) {
    final currentScale = info.scale.global;
    if (!currentScale.isIdentity()) {
      camera.zoom = startZoom * currentScale.y;
    } else {
      camera.translateBy(-info.delta.global);
      camera.snap();
    }
  }
```

在上面的範例中，平移事件使用 info.delta 處理，縮放事件使用 info.scale 處理，儘管理論上它們都來自底層縮放事件。

這也可以在[縮放範例](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/camera_and_viewport/zoom_example.dart)中看到

# [Mouse cursor](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#mouse-cursor)

也可以變更 GameWidget 區域上顯示的目前滑鼠遊標。 為此，可以在 Game 類別中使用以下程式碼

```
mouseCursor.value = SystemMouseCursors.move;
```

若要使用自訂遊標初始化 GameWidget，可以使用 mouseCursor 屬性

```
GameWidget(
  game: MouseCursorGame(),
  mouseCursor: SystemMouseCursors.move,
);
```

# [事件座標系](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#event-coordinate-system)

對於具有位置的事件（例如 Tap\* 或 Drag），您會注意到 eventPosition 屬性包含 2 個欄位：global 和 widget。 下面您將找到有關它們的簡要說明。

## [global](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#global)

以整個螢幕爲參考，事件發生的位置，與 Flutter 原生事件中的 globalPosition 相同

## [widget](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#widget)

事件發生的位置相對於 GameWidget 的位置和大小，與 Flutter 原生事件中的 localPosition 相同

# [Example](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#example)

```
class MyGame extends FlameGame with TapDetector {
  // Other methods omitted

  @override
  bool onTapDown(TapDownInfo info) {
    print("Player tap down on ${info.eventPosition.widget}");
    return true;
  }

  @override
  bool onTapUp(TapUpInfo info) {
    print("Player tap up on ${info.eventPosition.widget}");
    return true;
  }
}
```

您還可以在[此處](https://github.com/flame-engine/flame/tree/main/examples/lib/stories/input/)查看更完整的範例

# [GestureHitboxes](https://docs.flame-engine.org/1.16.0/flame/inputs/gesture_input.html#gesturehitboxes)

GestureHitboxes mixin 用於更準確地識別組件頂部的手勢。 例如，假設您有一塊相當圓形的岩石作為 SpriteComponent，那麼您不想註冊位於未顯示岩石的圖像角落的輸入，因為 PositionComponent 預設是矩形的。 然後，您可以使用 GestureHitboxes mixin 定義更準確的圓形或多邊形（或其他形狀），輸入應位於其中，以便在元件上註冊事件

您可以為具有 GestureHitboxes mixin 的元件新增新的 hitbox，就像在下面的 Collidable 範例中新增它們一樣

有關如何定義碰撞盒的更多資訊可以在碰撞檢測文件的[碰撞盒](https://docs.flame-engine.org/1.16.0/flame/collision_detection.html#shapehitbox)部分找到

可以在[此處](https://github.com/flame-engine/flame/blob/main/examples/lib/stories/input/gesture_hitboxes_example.dart)查看如何使用它的範例