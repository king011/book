# image

[image](https://pub.dev/packages/image) 提供了 圖像 讀取 和 保存的 功能 支持多種格式

image 完全使用 dart 實現不依賴 'dart:io' 所以也可以運行在 web 和 服務器，但也因此它不會執行原生代碼或庫所以性能可能會是一個問題，不過用來寫寫腳本處理圖片資源則非常合適

```
import 'dart:io';
import 'package:image/image.dart';
import 'package:path/path.dart' as path;

void main() async {
  final m = CCMerge(maxWidth: 2048, maxHeight: 2048, width: 45, height: 62);

  var index = 0;
  for (var i = 1; i <= 221; i++) {
    final name = i.toString().padLeft(3, '0');
    final file = File(path.join(Directory.current.path, 'assets', '$name.gif'));
    final src = decodeImage(file.readAsBytesSync())!;
    final state = m.put(src);
    print(' $index ${state.id}');
    if (state.completed) {
      await File(path.join(Directory.current.path, 'ok', '$index.png'))
          .writeAsBytes(encodePng(state.image));
      index++;
    }
  }
  final image = m.complete();
  if (image != null) {
    await File(path.join(Directory.current.path, 'ok', '$index.png'))
        .writeAsBytes(encodePng(image));
  }
}

/// 合併狀態
class CCMergeState {
  /// 合併器
  final CCMerge merge;

  /// 圖集
  final Image image;

  /// 圖片 id
  final int id;

  /// 返回是否完成合併
  bool get completed => !merge.isCurrent(image);

  CCMergeState({
    required this.merge,
    required this.image,
    required this.id,
  });
}

/// 用於將尺寸相同的碎圖合併爲圖集
class CCMerge {
  /// 圖集最大寬度
  final int maxWidth;

  /// 圖集最大高度
  final int maxHeight;

  /// 碎圖寬度
  final int width;

  /// 碎圖高度
  final int height;

  /// 水平方式圖片數量
  late final int cols;

  /// 垂直方向圖片數量
  late final int rows;

  CCMerge({
    required this.maxWidth,
    required this.maxHeight,
    required this.width,
    required this.height,
  })  : assert(maxWidth >= width),
        assert(width > 0),
        assert(maxHeight >= height),
        assert(height > 0) {
    cols = maxWidth ~/ width;
    rows = maxHeight ~/ height;
  }

  /// 圖集是否是當前合併對象
  bool isCurrent(Image image) => image == _image;

  /// 臨時圖集
  Image? _image;
  int _id = 0;

  /// 添加一個碎圖
  CCMergeState put(Image src) {
    // 驗證碎圖尺寸
    if (src.width != width) {
      throw Exception(
          'image width not matched, expected $width but got ${src.width}');
    } else if (src.height != height) {
      throw Exception(
          'image height not matched, expected $height but got ${src.height}');
    }

    /// 計算 id
    final dst = _image ??= Image(width * cols, height * rows);
    final id = _id;
    final r = id ~/ cols;
    final c = id % cols;
    // 複製碎圖
    copyInto(dst, src, dstX: c * width, dstY: r * height);
    _id++;
    if (_id == cols * rows) {
      _id = 0;
      _image = null;
    }
    return CCMergeState(
      merge: this,
      image: dst,
      id: id,
    );
  }

  /// 完成合併
  Image? complete() {
    var result = _image;
    if (result != null) {
      _image = null;
      _id = 0;
    }
    return result;
  }
}
```