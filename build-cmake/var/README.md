# 系統變量
| 變量名| 值 |
| -------- | -------- | 
| CMAKE\_SOURCE\_DIR     | CMakeLists.txt 所在目錄     | 
| CMAKE\_BUILD\_TYPE | 編譯組態 (None Debug Release RelWithDebInfo MinSizeRel) | 

# ENV
cmake 提供了 ENV 用來 讀寫 系統環境變量

```cmake
# 判斷 是否 定義了 環境變量
# if(NOT DEFINED ENV{CMAKE_INCLUDE})
if(DEFINED ENV{CMAKE_INCLUDE})
    # 讀取 環境 變量
    message($ENV{CMAKE_INCLUDE})
endif()

# 設置 環境變量
set( ENV{CMAKE_INCLUDE} /usr/include )

message($ENV{CMAKE_INCLUDE})
```