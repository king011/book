# [Effects](https://docs.flame-engine.org/1.16.0/flame/effects.html#effects)

Effect 是一種特殊組件，可以附加到另外一個組件以修改其屬性或外觀

例如，假設您正在製作一款具有可收集的強化物品的遊戲。 您希望這些能量提昇在地圖周圍隨機生成，然後在一段時間後消失。 顯然，您可以為道具製作一個精靈組件，然後將該組件放置在地圖上，但我們可以做得更好！

讓我們加入一個 ScaleEffect，以便在首次出現道具時將物品從 0% 增大到 100%。 添加另一個無限重複的交替 MoveEffect，以使項目稍微上下移動。 然後添加一個 OpacityEffect，該效果將「閃爍」該專案 3 次，此效果將具有 30 秒的內建延遲，或無論您希望您的道具保持在原位多久。 最後，新增一個RemoveEffect，它將在指定時間後自動從遊戲樹中刪除該項目（您可能希望在OpacityEffect結束後立即進行計時）。

正如您所看到的，透過一些簡單的效果，我們將一個簡單的無生命的精靈變成了一個更有趣的項目。 更重要的是，它並沒有導致程式碼複雜性增加：效果一旦添加，就會自動起作用，然後在完成後從遊戲樹中自行刪除。

# [Overview](https://docs.flame-engine.org/1.16.0/flame/effects.html#overview)

Effect 的功能是使某些組件的屬性隨時間發生變化。爲了實現這一點，Effect必須知道屬性的 初始值 最終值 以及它應該如何隨着時間的推移而發展。初始值通常由效果自動確定 最終值由調用者明確提供 隨着時間推移由 EffectControllers 處理

Flame 提供了多種 Effects，你也可以創建自己的 Effect。Flame 提供效果包括：

* MoveByEffect
* MoveToEffect
* MoveAlongPathEffect
* RotateEffect.by
* RotateEffect.to
* ScaleEffect.by
* ScaleEffect.to
* SizeEffect.by
* SizeEffect.to
* AnchorByEffect
* AnchorToEffect
* OpacityToEffect
* OpacityByEffect
* ColorEffect
* SequenceEffect
* RemoveEffect

EffectController 是一個對象，它描述了 Effect 應該如何隨時間演變。如果你將效果的初始值視爲 0% 進度，而將最終值視爲 100% 進度，那麼 EffectController 的工作就是將 "物理時間"(以秒爲單位) 映射到 "邏輯時間"，從 0 變爲 1

Flame 提供了多種 effect controllers：

* EffectController
* LinearEffectController
* ReverseLinearEffectController
* CurvedEffectController
* ReverseCurvedEffectController
* PauseEffectController
* RepeatedEffectController
* InfiniteEffectController
* SequenceEffectController
* SpeedEffectController
* DelayedEffectController
* NoiseEffectController
* RandomEffectController
* SineEffectController
* ZigzagEffectController

# [Built-in effects](https://docs.flame-engine.org/1.16.0/flame/effects.html#built-in-effects)

基類 Effect 是抽象的不能直接使用，但它提供了一些方法被子類繼承

* 使用 effect.pause() 和 effect.resume() 暫停/恢復 effect。你可以使用 effect.isPaused 檢測當前是否處於暫停狀態
* removeOnFinish 屬性默認爲 true，將導致 effect 組件在進度到100%後自動被移除以進行垃圾回收，如果打算重複使用，需要設置爲 false
* 可選的用戶提供的 onComplete，將在 effect 剛剛完成執行但從遊戲中刪除之前調用
* reset() 將 effect 恢復到原始狀態，使用其再次運行

## [MoveByEffect](#https://docs.flame-engine.org/1.16.0/flame/effects.html#movebyeffect)

此 Effect 用於將 PositionComponent 移動指定的偏移量，這個偏移是相對目標的當前位置

```
final effect = MoveByEffect(
  Vector2(0, -10),
  EffectController(duration: 0.5),
);
```

如果目標組件當前位於 Vector2(250, 200),則在效果結束時其位置將變成 Vector2(250, 190)

多個移動效果可以同時應用與同一個目標組件，結果將是多個 Effect 效果的疊加

## [MoveToEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#movetoeffect)

此 Effect 用於將 PositionComponent 從當前位置沿直線移動到指定的目標點

```
final effect = MoveToEffect(
  Vector2(100, 500),
  EffectController(duration: 3),
);
```

可以但不建議將多個此 Effect 附加到同一目標組件

## [MoveAlongPathEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#movealongpatheffect)

此 Effect 用於將 PositionComponent 沿相對於組件當前位置的指定路徑移動 。路徑可以有 non-linear(非線性) 段，但必須單獨連接。建議在 Vector2.zero() 開始一個路徑，以避免組件位置的突然跳躍

```
final effect = MoveAlongPathEffect(
  Path()..quadraticBezierTo(100, 0, 50, -50),
  EffectController(duration: 1.5),
);
```

可選標誌 **absolute: true** 會將 Effect 中的路徑聲明爲絕對路徑。也就是說，目標將在開始時 瞬間轉移 到路徑開頭，然後在畫布上像繪製曲線一樣跟隨該路徑

另一個標誌 **oriented: true** 指示目標不僅沿着曲線移動，而且還沿着曲線在每個點面向的方向旋轉自身。使用此標誌， Effect 同時變爲 移動+旋轉 Effect

## [RotateEffect.by](https://docs.flame-engine.org/1.16.0/flame/effects.html#rotateeffect-by)

將目標相對於其當前方向順時針旋轉指定弧度。下面的例子將目標順時針旋轉90角度

```
final effect = RotateEffect.by(
  pi/2,
  EffectController(duration: 2),
);
```

## [RotateEffect.to](https://docs.flame-engine.org/1.16.0/flame/effects.html#rotateeffect-to)

將目標順時針旋轉到指定弧度

```
final effect = RotateEffect.to(
  pi/4,
  EffectController(duration: 2),
);
```

## [ScaleEffect.by](https://docs.flame-engine.org/1.16.0/flame/effects.html#scaleeffect-by)

按指定量更改目標的比例，例如下面的例子將導致組件以當前比例再 擴大50%

scale\*= value

```
final effect = ScaleEffect.by(
  Vector2.all(1.5),
  EffectController(duration: 0.3),
);
```

## [ScaleEffect.to](https://docs.flame-engine.org/1.16.0/flame/effects.html#scaleeffect-to)

將目標的 scale 屬性設置爲指定值

```
final effect = ScaleEffect.to(
  Vector2.all(0.5),
  EffectController(duration: 0.5),
);
```

## [SizeEffect.by](https://docs.flame-engine.org/1.16.0/flame/effects.html#sizeeffect-by)

將目標的 size 設置 += 指定值

```
final effect = SizeEffect.by(
   Vector2(-15, 30),
   EffectController(duration: 1),
);
```

目標 PositionComponent 的大小不能爲負數，如果 Effect 嘗試將大小設置爲負數，將被限制爲 0 

請注意，要使此效果發揮作用，目標元件必須實作 SizeProvider 介面並在渲染時考慮其大小。 只有少數內建元件實作了此 API，但您始終可以透過在類別聲明中新增 Implements SizeEffect 來使自己的元件具有尺寸效果。此外更改組件大小不會傳播到其子組件

SizeEffect 的替代方案是 ScaleEffect，它的工作方式更普遍，可以縮放目標元件及其子元件

## [SizeEffect.to](https://docs.flame-engine.org/1.16.0/flame/effects.html#sizeeffect-to)

將目標的 size 設置爲 指定值

```
final effect = SizeEffect.to(
  Vector2(90, 80),
  EffectController(duration: 1),
);
```

## [AnchorByEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#anchorbyeffect)

將目標錨點的位置變更指定的偏移量。 也可以使用 AnchorEffect.by() 來建立此效果

```
final effect = AnchorByEffect(
  Vector2(0.1, 0.1),
  EffectController(speed: 1),
);
```

## [AnchorToEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#anchortoeffect)

更改目標錨點的位置。 也可以使用 AnchorEffect.to() 來建立此效果

```
final effect = AnchorToEffect(
  Anchor.center,
  EffectController(speed: 1),
);
```

## [OpacityToEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#opacitytoeffect)

此效果將隨著時間的推移將目標的不透明度變更為指定的 alpha 值。 它只能應用於實作 OpacityProvider 的元件

```
final effect = OpacityEffect.to(
  0.2,
  EffectController(duration: 0.75),
);
```

如果組件使用多種繪畫，則該效果可以使用目標參數來定位其中的另一種繪畫。 HasPaint mixin 實作 OpacityProvider 並公開 API，以便輕鬆地為所需的 PaintId 建立提供者。 對於單一paintId，可以使用opacityProviderOf；對於多個paintId，可以使用opacityProviderOfList

```
final effect = OpacityEffect.to(
  0.2,
  EffectController(duration: 0.75),
  target: component.opacityProviderOfList(
    paintIds: const [paintId1, paintId2],
  ),
);
```

不透明度值為 0 對應於完全透明的組件，不透明度值為 1 則對應於完全不透明。 便捷的建構子 OpacityEffect.fadeOut() 和 OpacityEffect.fadeIn() 將分別將目標動畫設定為完全透明/完全可見


## [OpacityByEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#opacitybyeffect)

此效果將改變目標相對於指定 alpha 值的不透明度。 例如，以下效果會將目標的不透明度變更 90%：

```
final effect = OpacityEffect.by(
  0.9,
  EffectController(duration: 0.75),
);
```

目前此效果只能套用於具有 HasPaint mixin 的元件。 如果目標元件使用多個繪製，則效果可以使用paintId參數定位任何單一顏色。

## [GlowEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#gloweffect)

> 此效果目前處於實驗階段，其 API 未來可能會發生變化

此效果將相對於指定的發光強度在目標周圍施加發光陰影。 陰影的顏色將是目標油漆顏色。 例如，以下效果將以 10 的強度在目標周圍施加發光陰影：

```
final effect = GlowEffect(
  10.0,
  EffectController(duration: 3),
);
```

目前此效果只能套用於具有 HasPaint mixin 的元件。


## [SequenceEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#sequenceeffect)

此效果可用於依序運行多個其他效果。 構成效應可能有不同的類型

序列效果也可以是交替的（序列會先向前運行，然後向後運行）； 並且也重複一定的預定次數，或無限次

```
final effect = SequenceEffect([
  ScaleEffect.by(
    Vector2.all(1.5),
    EffectController(
      duration: 0.2,
      alternate: true,
    ),
  ),
  MoveEffect.by(
    Vector2(30, -50),
    EffectController(
      duration: 0.5,
    ),
  ),
  OpacityEffect.to(
    0,
    EffectController(
      duration: 0.3,
    ),
  ),
  RemoveEffect(),
]);
```

## [RemoveEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#removeeffect)

此 effect 用於延遲一段時間後將目標組件從 遊戲中 移除

```
final effect = RemoveEffect(delay: 3.0);
```

## [ColorEffect](https://docs.flame-engine.org/1.16.0/flame/effects.html#coloreffect)

此 effect 將更改 paint 的基色，導致渲染的組件在提供的範圍內被提供的顏色着色

```
final effect = ColorEffect(
  const Color(0xFF00FF00),
  EffectController(duration: 1.5),
  opacityFrom = 0.2,
  opacityTo: 0.8,
);
```

opacityFrom 和 opacityTo 參數將決定將套用到元件的「多少」顏色。 在此範例中，效果將從 20% 開始，直到達到 80%

**注意** 由於該組件的實現方式以及 flutter 的 ColorFilter 的工作方式，該 Effect 不能與其它的 ColorEffect 混合使用，當組件添加了多個 ColorEffect 時，只有最後一個有效果

# Creating new effects

儘管 Flame 提供了多種內置的 effect，但最終你可能會發現它們不夠用。幸運的是，創建新的 effect 非常簡單

所有的 effect 都i派生自 **abstract class Effect**，但也可以從更專門的抽象類派生 比例 ComponentEffect&lt;T&gt; 或 Transform2DEffect

Effect 都構造函數需要一個 **EffectController** 實例作爲參數。在大多數情況下，你可能希望從自己的構造函數傳遞該控制器。幸運的是，EffectController 封裝了 effect 實現的大部分複雜性，因此你無需擔心重寫創建該功能

最後，你只需要實現一個 **void apply(double progress)** 方法，當 effect 處於活動狀態時，該方法將在每 tick 被調用。在此方法中，你應該對 Effect 目標進行更改

此外，如果在 Effect 開始或結束時必須執行任何操作，你可以實現 **onStart()** 和 **onFinish()** 回調

在實現 apply() 方法時，建議僅使用相對更新。也就是說，通過 增加/減少 目標的當前值來更改目標屬性，而不是直接將該屬性設置爲固定值。這樣，多個 effect 將能夠作用於同一組件而不會互相干擾

# [EffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#effect-controllers)

EffectController 基底類別提供了一個能夠建立各種通用控制器的工廠建構子。 建構函式的語法如下：


```
EffectController({
    required double duration,
    Curve curve = Curves.linear,
    double? reverseDuration,
    Curve? reverseCurve,
    bool alternate = false,
    double atMaxDuration = 0.0,
    double atMinDuration = 0.0,
    int? repeatCount,
    bool infinite = false,
    double startDelay = 0.0,
    VoidCallback? onMax,
    VoidCallback? onMin,
});
```

* **duration** effect 主要部分的長度，既從 0 到 100% 需要多長時間。此參數必須大於等於0.如果這是唯一指定的參數，effect 將在持續時間秒內線性增長
* **curve** 如果給定，則根據提供的[曲線](https://api.flutter.dev/flutter/animation/Curves-class.html)創建 0 到 100% 的非線性增長
* **reverseDuration** 如果提供，則向控制器添加一個額外的步驟：在 effect 從 0 增加到 100% 後，將在 reverseDuration 秒內從 100% 倒退到 0。此外，效果將在進度 0 處完成(通常效果在進度 1 處完成)
* **reverseCurve** 在 effect "反向" 步驟中使用的曲線。如果沒有給出，默認爲 curve.flipped
* **alternate** 如果爲 true 則相對於設置 reverseDuration 爲 duration，當如果 reverseDuration 不爲 null 則會忽略 alternate
* **atMaxDuration** 如果非0，這會在效果達到其最大進度之後和反向階段之前插入一個暫停。在此期間，進度保持在 100%。如果沒有反向階段，那麼這將指示 effect 完成之前的暫停
* **atMinDuration** 如果非0，則在反向階段結束時達到其最低進度(0)後插入暫停。在此期間 effect 進度爲 0%。如果沒有反向階段，則此暫停仍將插入在 atMaxDuration 暫停之後(如果存在)，否則將插入到正向階段之後。此外 effect 將在進度爲 0 處 完成
* **repeatCount** 如果大於 1，它將導致 effect 重複 repeatCount 次。每次迭代將包括 正向階段 atMaxDuration階段 反向階段 atMinDuration階段
* **infinite** 如果爲 true，則 effect 將無限重複並且永遠不會完成。這相對於設置 repeatCount 爲無限大
* **startDelay** 在 effect 開始之前插入額外的等待時間。這個等待時間只執行一次，即使 effect 是重複的。在此期間 effect 的 .started 屬性返回 false。該 effect 的 onStart 回調將在等待結束時回調
* **onMax** 回調函數將在達到最大進度後、可選的暫停和反向階段之前呼叫
* **onMin** 回調函數將在反向階段結束時達到最低進度後以及可選的暫停和前進階段之前呼叫

此工廠構造函數傳回的效果控制器將由多個更簡單的效果控制器組成，以下將進一步描述。 如果這個建構函式對於您的需求來說太有限，您始終可以從相同的建置區塊中建立自己的組合

除了工廠建構函式之外，EffectController 類別還定義了許多所有效果控制器通用的屬性。 這些屬性是：

* **started** 如果已經開始則爲 true。對於大多數控制器，此屬性始終爲 true。唯一的另外是 DelayedEffectController，它在 effect 處於等待階段時返回 false
* **completed** 當控制器執行完成時變爲 true
* **progress** 控制器當前值，一個從 0 到 1 的浮點。這個變量時效果控制器的主要 "輸出值"
* **duration** effect 的總持續時間，如果持續時間無法確定(例如，如果持續時間時 隨機 或 無限)則爲 null

## [LinearEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#lineareffectcontroller)

這是最簡單的控制器，它在指定的持續時間內從 0 線性增長到 1

```
final ec = LinearEffectController(3);
```

## [ReverseLinearEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#reverselineareffectcontroller)

與 LinearEffectController 類似，但以相反的方向運行，既在指定的持續時間內從 1 線性降低到 0

```
final ec = ReverseLinearEffectController(1);
```

## [CurvedEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#curvedeffectcontroller)

在持續時間內依據給出的曲線從 0 非線性增長到 1

```
final ec = CurvedEffectController(0.5, Curves.easeOut);
```

## [ReverseCurvedEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#reversecurvedeffectcontroller)

在持續時間內依據給出的曲線從 1 非線性降低到 0

```
final ec = ReverseCurvedEffectController(0.5, Curves.bounceInOut);
```

## [PauseEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#pauseeffectcontroller)

此控制器在指定的持續時間內將進度保持在恆定值。通常進度爲 0 或 1

```
final ec = PauseEffectController(1.5, progress: 0);
```

## [RepeatedEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#repeatedeffectcontroller)

這時一個複合效果的控制器。它將另外一個效果控制器作爲子項，並重複多次，在每個下一個循環開始之前重置

```
final ec = RepeatedEffectController(LinearEffectController(1), 10);
```

> 子效果控制器不能是無限的。 如果子級是隨機的，那麼它將在每次迭代時使用新的隨機值重新初始化


## [InfiniteEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#infiniteeffectcontroller)

類似 RepeatedEffectController 但無限重複子控制器

```
final ec = InfiniteEffectController(LinearEffectController(1));
```

## [SequenceEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#sequenceeffectcontroller)

一個接一個的執行一系列控制器，控制器列表不能爲空

```
final ec = SequenceEffectController([
  LinearEffectController(1),
  PauseEffectController(0.2),
  ReverseLinearEffectController(1),
]);
```

## [SpeedEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#speedeffectcontroller)

變更其子效果控制器的持續時間，以便效果以預先定義的速度進行。 子 EffectController 的初始持續時間是無關的。 子控制器必須是 DurationEffectController 的子類別

SpeedEffectController 只能應用於速度概念明確定義的效果。 此類效果必須實作 MeasurableEffect 介面。 例如，以下效果符合條件：MoveByEffect、MoveToEffect、MoveAlongPathEffect、RotateEffect.by、RotateEffect.to

參數 speed 以秒為單位，其中“單位”的概念取決於目標效果。 例如，對於移動效果，它們指的是行進的距離； 對於旋轉效果，單位是弧度

```
final ec1 = SpeedEffectController(LinearEffectController(0), speed: 1);
final ec2 = EffectController(speed: 1); // same as ec1
```

## [DelayedEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#delayedeffectcontroller)

在指定的延遲時間後執行其子控制器。當控制器執行“延遲”階段時，effect被視爲“未啓動”，即其 .started 屬性將返回 false

```
final ec = DelayedEffectController(LinearEffectController(1), delay: 5);
```

## [NoiseEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#noiseeffectcontroller)

此效果控制器表現出雜訊行為，即它在零附近隨機振盪。 這樣的效果控制器可以用來達到多種抖動效果

```
final ec = NoiseEffectController(duration: 0.6, frequency: 10);
```

## [RandomEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#randomeffectcontroller)

此控制器包裝另一個控制器並使其持續時間隨機。 每次重置時都會重新產生持續時間的實際值，這使得該控制器在重複上下文中特別有用，例如 RepeatedEffectController 或 InfiniteEffectController

```
final ec = RandomEffectController.uniform(
  LinearEffectController(0),  // duration here is irrelevant
  min: 0.5,
  max: 1.5,
);
```

使用者能夠控制使用哪個隨機來源，以及產生的隨機持續時間的精確分佈。 包括兩個分佈 - .uniform 和 .exponential，使用者可以實現任何其他分佈。

## [SineEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#sineeffectcontroller)

代表正弦函數單一週期的效果控制器。 用它來創造看起來自然的諧波振盪。 由具有不同週期的 SineEffectController 控制的兩個垂直移動效果將建立一條利薩如曲線

```
final ec = SineEffectController(period: 1);
```

## [ZigzagEffectController](https://docs.flame-engine.org/1.16.0/flame/effects.html#zigzageffectcontroller)

簡單的交替效果控制器。 在一個週期的過程中，該控制器將從 0 線性進行到 1，然後到 -1，然後返回到 0。將此用於振盪效果，其中起始位置應該是振盪的中心，而不是極端（由標準交替EffectController 提供）

```
final ec = ZigzagEffectController(period: 2);
```