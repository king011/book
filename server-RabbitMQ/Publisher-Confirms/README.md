# [Publisher Confirms](https://www.rabbitmq.com/tutorials/tutorial-seven-java.html)

* 發佈者確認 實現了 類似消費者 確認的 機制 用於保證 發佈的消息 不會丟失
* 發佈者確認 可能會等待 服務器 fsync RabbitMQ 每幾百毫秒 執行依次 fsync 所以 發佈者確認每次可能 會延遲 幾百毫秒
* 一旦一個 channel 進入 發佈者確認模式 變不可更改 且 無法執行 事務
* 要使用 channel 進入 發佈者確認模式 需要調用 Confirm 函數 並且只能調用一次

# Publishing Messages Individually

此模式 分別發送 消息 每次 發送完成 都等待服務器 確認

```
package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-one`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()

	// 設置 未完成的 發送緩存 爲 1
	confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))

	// 進入發佈者確認描述
	e = channel.Confirm(false)
	if e != nil {
		log.Println(e)
		return
	}

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {
		// 發佈消息
		e = channel.Publish(
			``,
			q.Name,
			false,
			false,
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(strconv.Itoa(i)), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, i)
		} else {
			log.Println(e)
			break
		}

		// 等待發送確認
		confirmed := <-confirms
		if confirmed.Ack {
			fmt.Println("ack success", confirmed.DeliveryTag)
		} else {
			fmt.Println("ack error", confirmed.DeliveryTag)
		}
	}
}
```

# Publishing Messages in Batches

批量發佈 一次 發佈多次消息後 等待所有消息確認

```
package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-one`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()

	// 設置 未完成的 發送緩存 爲
	wait := 10
	confirms := channel.NotifyPublish(make(chan amqp.Confirmation, wait))

	// 進入發佈者確認描述
	e = channel.Confirm(false)
	if e != nil {
		log.Println(e)
		return
	}

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {
		// 發佈消息
		e = channel.Publish(
			``,
			q.Name,
			false,
			false,
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(strconv.Itoa(i)), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, i)
		} else {
			log.Println(e)
			break
		}
	}

	// 等待發送確認
	for wait != 0 {
		wait--
		confirmed := <-confirms
		if confirmed.Ack {
			fmt.Println("ack success", confirmed.DeliveryTag)
		} else {
			fmt.Println("ack error", confirmed.DeliveryTag)
		}
	}
}
```

DeliveryTag 是 從 1 開始的 計數 每次新的 Publish 會使 DeliveryTag +1

在 批量 或 異步確認時 需要使用 DeliveryTag 來確認 哪個消息得到了確認
# Handling Publisher Confirms Asynchronously

對於golang 來說 異步確認 值需要啓動一個單獨的協程 去處理確認即可

似乎 RabbitMQ 沒有 返回 Publish 對應的 DeliveryTag 故 不要用 多個 協程 去 Publishing 消息 否則會難以 計算 當前 Publishing 確認消息的 DeliveryTag

# 事務模式

事務模式 極爲低效 而且 官方文檔沒看到 example 而且 基本可以使用 Publishing Messages Individually 替代 應該首選 發佈確認模式

```
package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-one`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {
		txPublish(channel, q.Name, i)
		time.Sleep(time.Second)
	}
}
func txPublish(channel *amqp.Channel, name string, i int) (e error) {
	if i%3 == 0 {
		// 啓動事務
		e = channel.Tx()
		if e != nil {
			log.Println(e)
			return
		}
		defer func() {
			if e == nil {
				// 提交事務
				channel.TxCommit()
			} else {
				// 回滾事務
				channel.TxRollback()
			}
		}()
	}
	// 發佈消息
	e = channel.Publish(
		``,
		name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(strconv.Itoa(i)), //消息內容
		},
	)
	if e == nil {
		fmt.Println(`send ->`, i)
	} else {
		log.Println(e)
	}
	return
}
```