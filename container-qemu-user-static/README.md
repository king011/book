# qemu-user-static

qemu-user-static 通過 QEMU [1] 和 binfmt_misc [2] 啓用不同多種架構的容器執行

* 源碼 [https://github.com/multiarch/qemu-user-static](https://github.com/multiarch/qemu-user-static)
* docker hub [https://hub.docker.com/r/multiarch/qemu-user-static/](https://hub.docker.com/r/multiarch/qemu-user-static/)