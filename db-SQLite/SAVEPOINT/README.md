# [SAVEPOINT](https://www.sqlite.org/lang_savepoint.html)

SAVEPOINT 是一種創建 Transaction 的方法，類似與 BEGIN 和 COMMIT，不同之處在於 SAVEPOINT 和 RELEASE 命名被命名並且可以嵌套

SAVEPOINT 命令使用一個 名字 開始一個新的 Transaction。Transaction 名稱不必唯一。SAVEPOINT 可以在 BEGIN...COMMIT 之內或之外啓動。當 SAVEPOINT 是最外層的保存點並且它不在 BEGIN...COMMIT 中時，其行爲和 BEGIN DEFERRED TRANSACTION 相同

ROLLBACK TO 命令將數據庫的狀態恢復到相應的 SAVEPOINT 之後的狀態。請注意，與普通的 ROLLBACK 命令(沒有 TO 關鍵字) 不同，ROLLBACK TO 不會取消 Transaction。

RELEASE 命令類似 SAVEPOINT 的 COMMIT。RELEASE 命令會刪除名稱指定的命名點，如果 RELEASE 釋放了最外層的保存點則它會執行 COMMIT。如果 RELEASE 指定的名稱不存在則會返回錯誤