# Accept-Ranges

http1.1 加入了 Range 頭

良好的 http 服務器在響應檔案下載時 通常會 加上 Accept-Ranges 頭 來表示 自己 支持 Range

瀏覽器 發現服務器支持 Range 會利用 Range 來恢復異常的下載...

```
HTTP/1.1 200 OK
Server: nginx/1.14.0 (Ubuntu)
Date: Mon, 16 Mar 2020 03:45:10 GMT
Content-Type: audio/mpeg
Content-Length: 5800938
Last-Modified: Mon, 14 Oct 2019 02:29:06 GMT
Connection: keep-alive
ETag: "5da3dd72-5883ea"
Accept-Ranges: bytes
```

# Range

客戶端 通過 添加 Range 來請求服務器 下載 檔案的 指定分塊

比如 有如下 四次請求

* Range: bytes=0-1199 第一個1200字節
* Range: bytes=1200-2399 第二個1200字節
* Range: bytes=2400-3599 第三個1200字節
* Range: bytes=3600-5000 第四個1200字節

假設檔案大小爲 5000字節 服務器 分別如下響應

* Content-Length：1200
* Content-Range：bytes 0-1199/5000

* Content-Length：1200
* Content-Range：bytes 1200-2399/5000

* Content-Length：1200
* Content-Range：bytes 2400-3599/5000

* Content-Length：1400
* Content-Range：bytes 3600-4999/5000

當 客戶端 設置 Range 此時正常的響應碼 應該是 206 而非200 如果 範圍無法滿足 返回 416

如果 服務器 不支持 則響應中 不設置 Content-Range 此時服務器通常會 從頭開始傳輸


* Range: bytes=0- 第0個字節以及以後的 所有字節
* Range: bytes=-500 最後500個字節
# ETag Last-Modified

如果器 在返回 Accept-Ranges時 還可以返回 一個 ETag 以及 Last-Modified 字段

客戶端 可以在 Range 請求時 把 ETag/Last-Modified 任一個值 設置到 If-Range 中 此時 服務器 會檢測 檔案是否變化 變化了 響應 200 並從頭開始傳輸 沒變化 返回 206 範圍有誤返回416
