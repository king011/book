# Web Worker

Web Worker 允許在後臺線程中運行 CPU 密集計算。

```
ng generate web-worker app
```

上述指令會執行兩部操作

1. 爲項目配置使用 Web Worker
2. 把 下述腳手架代碼添加到 **src/app/app.worker.ts**

	```
	addEventListener('message', ({ data }) => {
		const response = `worker response to ${data}`;
		postMessage(response);
	});
	```

要使用 web worker 可以使用下述代碼

```
if (typeof Worker !== 'undefined') {
  // Create a new
  const worker = new Worker('./app.worker', { type: 'module' });
  worker.onmessage = ({ data }) => {
    console.log(`page got message: ${data}`);
  };
  worker.postMessage('hello');
} else {
  // Web workers are not supported in this environment.
  // You should add a fallback so that your program still executes correctly.
}
```

# 注意

在某些平臺例如 服務器渲染中使用的  @angular/platform-server  不支持 Web Worker，爲了能夠在這些平臺工作應該提供一個回退機制來執行 Web Worker 執行的計算