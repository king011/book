# fsnotify

fsnotify 是一個 開源(BSD) 的 檔案夾 監控庫 可以監控 指定檔案夾下 檔案的 變化 並通知給 調用者

fsnotify 可以 運行在 linux windows mac 中 android ios 則未經測試

源碼 [https://github.com/fsnotify/fsnotify](https://github.com/fsnotify/fsnotify)

```go
#info=false
go get -u -v github.com/fsnotify/fsnotify
```
