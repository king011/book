# [AlignComponent](https://docs.flame-engine.org/1.16.0/flame/layout/align_component.html)

```
class AlignComponent extends PositionComponent
```

AlignComponent 是一個佈局元件，它使用相對位置將其子元件定位在自身內部。 它類似於 Flutter 的 [Align](https://api.flutter.dev/flutter/widgets/Align-class.html) 小部件。

該元件需要一個子元件，它將作為該元件對齊的目標。 當然，其他子元件也可以添加到該元件中，但只有初始子元件會對齊。

alignment 參數描述了子組件應放置在目前組件中的位置。 例如，如果 alignment 為 Anchor.center，則子項將居中。

通常，該組件的大小將與其父組件的大小相符。 但是，如果您提供屬性 widthFactor 或 heightFactor，則該元件在該方向上的大小將等於子元件的大小乘以相應的因子。 例如，如果將 heightFactor 設為 1，則該元件的寬度將等於父元件的寬度，但高度將與子元件的高度相符。

```
AlignComponent(
  child: TextComponent('hello'),
  alignment: Anchor.centerLeft,
);
```

預設情況下，子項的錨點設定為等於 alignment。 這實現了傳統的對齊行為：例如，子項的中心將放置在目前組件的中心，或者子項的右下角可以放置在組件的右下角。 然而，也可以透過給孩子一個不同的錨並將 keepChildAnchor 設為 true 來實現更奢侈的放置。 例如，如果將對齊設定為 topCenter，並將子項的錨點設定為 BottomCenter，則子項將有效地放置在目前元件的上方：

```
PlayerSprite().add(
  AlignComponent(
    child: HealthBar()..anchor = Anchor.bottomCenter,
    alignment: Anchor.topCenter,
    keepChildAnchor: true,
  ),
);
```

# [Constructors](https://docs.flame-engine.org/1.16.0/flame/layout/align_component.html#constructors)

```
AlignComponent({PositionComponent? child, Anchor alignment = Anchor.topLeft, this.widthFactor, this.heightFactor, this.keepChildAnchor = false})
```

建立一個元件，使其子元件根據該元件邊界框內的對齊方式保持定位。

更準確地說，子元件將被放置在目前元件邊界框內的對齊相對位置。 子項的錨點也將設定為對齊方式，除非 keepChildAnchor 參數為 true。

# [Properties](https://docs.flame-engine.org/1.16.0/flame/layout/align_component.html#properties)

* **child ←→ PositionComponent?** 將由此組件定位的組件。 子組件將自動安裝到目前組件
* **alignment ←→ Anchor** 子級將如何在目前組件中定位。** 注意**：與 Flutter 的 Alignment 不同，組件的左上角相對座標為 (0, 0)，右下角相對座標為 (1, 1)。
* **widthFactor : double?** 如果為 null，則元件的寬度將等於父元件的寬度。 否則，寬度將等於子項的寬度乘以該因子。
* **heightFactor : double?** 如果為 null，則組件的高度將等於父組件的高度。 否則，身高將等於孩子的身高乘​​以該係數。
* **keepChildAnchor : bool** 如果為 false（預設值），則子層級的錨點將保持等於對齊值。 如果為 true，則子層級將被允許擁有獨立於父級的自己的錨值。

