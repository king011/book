# 路由

v2ray 內置了一個 路由 routing 通過路由 可以指定 對不同的 域名/ip 使用 不同的 出站方式

routing 由一個 [RoutingObject](https://www.v2ray.com/chapter_02/03_routing.html) 定義

```
{
  "domainStrategy": "AsIs",
  "rules": [],
  "balancers": []
}
```