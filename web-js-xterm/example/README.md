# html view
```
npm install xterm
npm install --save xterm-addon-web-links
npm install --save xterm-addon-fit
```

```
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Terminal } from 'xterm'
import { FitAddon } from 'xterm-addon-fit';
import { WebLinksAddon } from 'xterm-addon-web-links';

@Component({
  selector: 'app-xterm',
  templateUrl: './xterm.component.html',
  styleUrls: ['./xterm.component.scss']
})
export class XtermComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor() { }
  private _xterm: Terminal
  private _fitAddon = new FitAddon()
  private _webLinksAddon = new WebLinksAddon()
  ngOnInit(): void {
  }
  ngOnDestroy() {
    this._xterm.dispose()
    this._fitAddon.dispose()
    this._webLinksAddon.dispose()
  }
  @ViewChild("xterm")
  xterm: ElementRef
  ngAfterViewInit() {
    const xterm = new Terminal({
      // 光標閃爍
      cursorBlink: true,
      screenReaderMode: true,
    })
    this._xterm = xterm
    xterm.loadAddon(this._fitAddon)
    xterm.loadAddon(this._webLinksAddon)
    xterm.open(this.xterm.nativeElement)
    this._fitAddon.fit()

    let websocket = new WebSocket(`ws://127.0.0.1:9000/ws/xterm?rows=${xterm.rows}&cols=${xterm.cols}`)
    websocket.binaryType = "arraybuffer"

    window.onresize = (evt) => {
      this._fitAddon.fit()
    }

    websocket.onopen = function (evt) {
      xterm.onData(function (data) {
        console.log(`send data`, data)
        websocket.send(new TextEncoder().encode("\x00" + data));
      })
      xterm.onResize(function (evt) {
        console.log(`send resize`)
        websocket.send(new TextEncoder().encode("\x01" + JSON.stringify({ cols: evt.cols, rows: evt.rows })))
      })
      xterm.onTitleChange(function (title) {
        document.title = title;
      })

      websocket.onmessage = function (evt) {
        if (evt.data instanceof ArrayBuffer) {
          xterm.write(new Uint8Array(evt.data))
        } else {
          console.log(evt.data)
        }
      }
      websocket.onclose = function (evt) {
        xterm.write("Session terminated")
        //term.dispose()
      }
      websocket.onerror = function (evt) {
        console.log(evt)
      }
    }
  }
}
```

# server
```
package main

import (
	"encoding/json"
	"errors"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"sync/atomic"

	"github.com/creack/pty"
	"golang.org/x/net/websocket"
)

const addr = `127.0.0.1:9000`

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	l, e := net.Listen(`tcp`, addr)
	if e != nil {
		log.Fatalln(e)
	}
	defer l.Close()
	log.Println(`work at`, addr)
	mux := http.NewServeMux()
	mux.HandleFunc(`/`, index)
	mux.HandleFunc(`/ws/xterm`, websocket.Handler(ws).ServeHTTP)
	http.Serve(l, mux)
}
func index(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, `main.html`)
}
func ws(ws *websocket.Conn) {
	// 解析 窗口大小
	request := ws.Request()
	keys := request.URL.Query()
	cols, e := strconv.ParseUint(keys.Get("cols"), 10, 16)
	rows, e := strconv.ParseUint(keys.Get("rows"), 10, 16)
	if cols < 1 || rows < 1 {
		ws.Write([]byte(`window size not support`))
		ws.Close()
		return
	}

	ws.PayloadType = websocket.BinaryFrame
	// 創建 shell
	cmd := exec.Command("/bin/bash", "-l")
	cmd.Env = append(os.Environ(), "TERM=xterm")
	// 模擬 tty 運行 shell
	tty, e := pty.StartWithSize(cmd, &pty.Winsize{
		Cols: uint16(cols),
		Rows: uint16(rows),
	})
	if e != nil {
		ws.Write([]byte(e.Error()))
		ws.Close()
		return
	}
	// 橋接 shell 和 websocket
	shell := Shell{
		conn: ws,
		cmd:  cmd,
		tty:  tty,
	}
	shell.Run()

}

// Shell .
type Shell struct {
	conn   *websocket.Conn
	cmd    *exec.Cmd
	tty    *os.File
	closed int32
}

// Run .
func (s *Shell) Run() {
	go s.readTTY()

	s.readWebsocket()
	s.Close()
}
func (s *Shell) readWebsocket() {
	var msg []byte
	var e error
	for {
		if e = websocket.Message.Receive(s.conn, &msg); e != nil {
			break
		} else if len(msg) == 0 {
			continue
		}
		switch msg[0] {
		case 0:
			_, e = s.tty.Write(msg[1:])
			if e != nil {
				s.conn.Write([]byte(e.Error()))
				break
			}
		case 1:
			var size pty.Winsize
			e = json.Unmarshal(msg[1:], &size)
			if e == nil {
				e = pty.Setsize(s.tty, &size)
				if e != nil {
					log.Println(e)
				}
			} else {
				log.Println(e)
			}
		default:
			s.conn.Write([]byte(`unknown data type`))
			return
		}
	}
}
func (s *Shell) readTTY() {
	b := make([]byte, 1024)
	for {
		n, e := s.tty.Read(b)
		if e != nil {
			s.conn.Write([]byte(e.Error()))
			break
		}
		_, e = s.conn.Write(b[:n])
		if e != nil {
			break
		}
	}
	s.Close()
}

// Close .
func (s *Shell) Close() (e error) {
	if atomic.CompareAndSwapInt32(&s.closed, 0, 1) {
		s.cmd.Process.Kill()
		s.cmd.Process.Wait()
		s.tty.Close()
		s.conn.Close()
	} else {
		e = errors.New("already closed")
	}
	return
}
```