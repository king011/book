# TypeScript
TypeScript 是微軟 2012年10月1日開始發佈的一個 開源\(Apache&nbsp;License&nbsp;2\.0\) 腳本語言

ts 是 js 的嚴格 超集合 爲 js 提供了 額外的 功能支持  
> 源碼 通常以 **.ts** 爲後綴
> 
> ms 提供了工具 將 ts 代碼 編譯到 等效的 js代碼 因此 可以在 任何支持 js 的環境中 使用 

* 官網 [http://www.typescriptlang.org/](http://www.typescriptlang.org/)
* wiki [https://zh.wikipedia.org/wiki/TypeScript](https://zh.wikipedia.org/wiki/TypeScript)
* 中文社區 [https://www.tslang.cn/index.html](https://www.tslang.cn/index.html)
* 源碼 [https://github.com/Microsoft/TypeScript](https://github.com/Microsoft/TypeScript)

# 安裝
1. 安裝好 node 環境
2. npm install -g typescript
3. npm install --save @types/node

# 常用 指令

\# 顯示 版本  
tsc -v

\# 將 ts 編譯為 js  
tsc 1.ts 2.ts ...

\# 創建 ts 項目 配置檔案 **tsconfig.json**  
tsc --init

\# 依據 配置檔案 **tsconfig.json** 中的 配置 編譯 項目  
tsc

# tsconfig.json
tsconfig.json 是 TypeScript 項目的配置 檔案 執行  
tsc --init 可以在當前目錄下創建默認的 tsconfig.json

## Example
下面是一個 nodejs 的項目 配置 自動 執行 tsc 會將 src 檔案夾下的 ts 代碼 編譯為js後輸出到 dist 下
```json
{
  "compilerOptions": {
    /* Basic Options */
    "target": "es6",                          /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017','ES2018' or 'ESNEXT'. */
    "module": "commonjs",                     /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */
    // "lib": [],                             /* Specify library files to be included in the compilation. */
    // "allowJs": true,                       /* Allow javascript files to be compiled. */
    // "checkJs": true,                       /* Report errors in .js files. */
    // "jsx": "preserve",                     /* Specify JSX code generation: 'preserve', 'react-native', or 'react'. */
    "declaration": true,                   /* Generates corresponding '.d.ts' file. */
    "declarationMap": true,                /* Generates a sourcemap for each corresponding '.d.ts' file. */
    "sourceMap": true,                     /* Generates corresponding '.map' file. */
    // "outFile": "./",                       /* Concatenate and emit output to single file. */
    "outDir": "dist",                        /* Redirect output structure to the directory. */
    // "rootDir": "./",                       /* Specify the root directory of input files. Use to control the output directory structure with --outDir. */
    // "composite": true,                     /* Enable project compilation */
    // "removeComments": true,                /* Do not emit comments to output. */
    // "noEmit": true,                        /* Do not emit outputs. */
    // "importHelpers": true,                 /* Import emit helpers from 'tslib'. */
    // "downlevelIteration": true,            /* Provide full support for iterables in 'for-of', spread, and destructuring when targeting 'ES5' or 'ES3'. */
    // "isolatedModules": true,               /* Transpile each file as a separate module (similar to 'ts.transpileModule'). */
 
    /* Strict Type-Checking Options */
    "strict": true,                           /* Enable all strict type-checking options. */
    // "noImplicitAny": true,                 /* Raise error on expressions and declarations with an implied 'any' type. */
    // "strictNullChecks": true,              /* Enable strict null checks. */
    // "strictFunctionTypes": true,           /* Enable strict checking of function types. */
    // "strictPropertyInitialization": true,  /* Enable strict checking of property initialization in classes. */
    // "noImplicitThis": true,                /* Raise error on 'this' expressions with an implied 'any' type. */
    // "alwaysStrict": true,                  /* Parse in strict mode and emit "use strict" for each source file. */
 
    /* Additional Checks */
    // "noUnusedLocals": true,                /* Report errors on unused locals. */
    // "noUnusedParameters": true,            /* Report errors on unused parameters. */
    // "noImplicitReturns": true,             /* Report error when not all code paths in function return a value. */
    // "noFallthroughCasesInSwitch": true,    /* Report errors for fallthrough cases in switch statement. */
 
    /* Module Resolution Options */
    // "moduleResolution": "node",            /* Specify module resolution strategy: 'node' (Node.js) or 'classic' (TypeScript pre-1.6). */
    // "baseUrl": "./",                       /* Base directory to resolve non-absolute module names. */
    // "paths": {},                           /* A series of entries which re-map imports to lookup locations relative to the 'baseUrl'. */
    // "rootDirs": [],                        /* List of root folders whose combined content represents the structure of the project at runtime. */
    // "typeRoots": [],                       /* List of folders to include type definitions from. */
    // "types": [],                           /* Type declaration files to be included in compilation. */
    // "allowSyntheticDefaultImports": true,  /* Allow default imports from modules with no default export. This does not affect code emit, just typechecking. */
    "esModuleInterop": true                   /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */
    // "preserveSymlinks": true,              /* Do not resolve the real path of symlinks. */
 
    /* Source Map Options */
    // "sourceRoot": "./",                    /* Specify the location where debugger should locate TypeScript files instead of source locations. */
    // "mapRoot": "./",                       /* Specify the location where debugger should locate map files instead of generated locations. */
    // "inlineSourceMap": true,               /* Emit a single file with source maps instead of having a separate file. */
    // "inlineSources": true,                 /* Emit the source alongside the sourcemaps within a single file; requires '--inlineSourceMap' or '--sourceMap' to be set. */
 
    /* Experimental Options */
    // "experimentalDecorators": true,        /* Enables experimental support for ES7 decorators. */
    // "emitDecoratorMetadata": true,         /* Enables experimental support for emitting type metadata for decorators. */
  },
  "include": [
    "src/**/*.ts"
  ],
  "exclude": [
    "node_modules"
  ]
}
```

# types

[https://microsoft.github.io/TypeSearch/](https://microsoft.github.io/TypeSearch/) 提供了 js 库的 .d.ts 档案

# ts-node

ts-node 將 tsc 和 node 指令進行了包裝 可以直接 執行 typescript 代碼

```
# 安裝
npm install -g ts-node

# 執行腳本
ts-node script.ts
```



