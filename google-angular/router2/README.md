# ActivatedRoute
ActivatedRoute d 當前激活路由對象 主要用於保存路由 獲取路由傳遞的參數

1. 獲取 查詢參數

   ```
   /product?id=1&name=2
   ActivatedRoute.queryParams['id']
   ```

2. 獲取路由參數

   ```
   // 路由配置
   {path:'/product/:id'}
   /product/1 
   ActivatedRoute.params['id']
   ```
	
3. 路由配置 傳遞數據

   ```
   // 路由配置
   {path:'/product',component:ProductComponent,data:[{isProd:true}]}
   ActivatedRoute.data[0][isProd]
   ```

## 參數 快照

```
import{ActivatedRoute,Params} from  '@angular/router';

export class XXX {
    ngOnInit() {
        let id:string= this.activatedRoute_.snapshot.params["id"]
        console.log(id)
    }
}
```

## 訂閱參數變化

```
import{ActivatedRoute,Params} from  '@angular/router';

export class XXX {
    ngOnInit() {
        this.activatedRoute_.params.subscribe(
            (params:Params)=>{
                    this.productId=params["id"];
                    console.log(this.productId);
            }
        )
    }
}
```

# HttpUrlEncodingCodec

class HttpUrlEncodingCodec 提供了 url 路徑 的編碼和解碼功能

```
import { HttpUrlEncodingCodec } from '@angular/common/http';

const codec = new HttpUrlEncodingCodec()
const key = codec.encodeKey('鋼之鍊金術師')
const path = `/download/${key}`
console.log(path)
console.log(codec.decodeKey(key))
console.log(codec.decodeValue(path))
```

# HttpParams

class HttpParams 提供了對 請求參數的編碼解碼功能

```
import { HttpParams } from '@angular/common/http';

let params = new HttpParams({
	fromObject: {
		name: "鋼之鍊金術師",
		index: "2",
	},
})
const fromString = params.toString()
params = new HttpParams({
	fromString: fromString,
})
console.log(params.has("name"))
console.log(params.get("name"))
///...
```