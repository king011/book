# 快速上手

```
package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func main() {
	// 打開源
	f, e := os.Open("a.mp3")
	if e != nil {
		log.Fatal(e)
	}
	defer f.Close()
	// 調用解碼器創建 streamer
	streamer, format, e := mp3.Decode(f)
	if e != nil {
		log.Fatal(e)
	}
	defer streamer.Close()
	fmt.Println(format)

	// 初始化揚聲器
	// 更大的緩衝區意味着更低的 cpu 使用率和更可靠的播放，更少的緩衝區意味着更快的響應和更少的延遲
	e = speaker.Init(
		format.SampleRate,                   // 採樣率
		format.SampleRate.N(time.Second/10), // N 計算 持續時間內的 樣本數，此處傳入了 100ms 的樣本數
	)
	if e != nil {
		log.Fatalln(e)
	}

	// player 函數會立刻返回它是異步執行的
	// speaker.Play(streamer)
	done := make(chan bool)
	speaker.Play(
		// Seq 接收任意多個 Streamer 並返回一個 Streamer，它會一個一個地流式傳輸它們而不會暫停
		// Seq 不會從 Streamers 傳播錯誤
		beep.Seq(
			streamer,
			// // 當我們播放多首音樂時它們 採樣率可以能不同調用 Resample 它將重新調整採樣率
			// beep.Resample(
			// 	4,                 // 質量
			// 	format.SampleRate, // 原採樣率
			// 	sr,                // 新採樣率
			// 	streamer,          // 要播放的 streamer
			// ),
			// 將一個 函數回調包裝爲 Streamer，它不會傳入 任何樣本，只是回調函數並且回調時 揚聲器被 locked
			beep.Callback(func() {
				// 利用 回調函數通知 播放結束
				done <- true
			}),
		),
	)

	for {
		select {
		case <-done:
			return
		case <-time.After(time.Second):
			// 每秒打印播放進度
			speaker.Lock()
			fmt.Println(format.SampleRate.D(streamer.Position()).Round(time.Second))
			speaker.Unlock()
		}
	}
}
```

# 循環

**beep.Loop** 函數包裝一個 streamer 並使它循環播放，如果傳入 複數 作爲循環次數則它將無限循環

```
done := make(chan bool)
// 循環 3 次
loop := beep.Loop(3, streamer)
speaker.Play(beep.Seq(
	loop,
	beep.Callback(func() {
		done <- true
	}),
))
<-done
```

# 加速播放
**beep.ResampleRatio** 函數包裝一個 streamer 並按照 ratio float64 去加速或減少播放

```
done := make(chan bool)
fast := beep.ResampleRatio(
	4, // 質量
	5, // 加速 x5
	streamer,
)
speaker.Play(beep.Seq(
	fast,
	beep.Callback(func() {
		done <- true
	}),
))
<-done
```
# 控制

```
package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func main() {
	// 打開源
	f, e := os.Open("a.mp3")
	if e != nil {
		log.Fatal(e)
	}
	defer f.Close()
	// 調用解碼器創建 streamer
	streamer, format, e := mp3.Decode(f)
	if e != nil {
		log.Fatal(e)
	}
	defer streamer.Close()
	fmt.Println(format)

	// 初始化揚聲器
	// 更大的緩衝區意味着更低的 cpu 使用率和更可靠的播放，更少的緩衝區意味着更快的響應和更少的延遲
	e = speaker.Init(
		format.SampleRate,                   // 採樣率
		format.SampleRate.N(time.Second/10), // N 計算 持續時間內的 樣本數，此處傳入了 100ms 的樣本數
	)
	if e != nil {
		log.Fatalln(e)
	}

	loop := beep.Loop(-1, streamer)
	// 創建一個 ctrl 用於 暫停與播放
	ctrl := &beep.Ctrl{Streamer: loop, Paused: false}
	// 創建一個 Volume 用於控制音量 音量將以符合人類習慣的指數形式 增強或減弱
	volume := &effects.Volume{
		Streamer: ctrl,
		Base:     2,     // 基數
		Volume:   0,     // 0 代表音量無變化 正數代表增強 負數代表減弱
		Silent:   false, // 是否禁音標記，因爲指數形式無法將音量減弱到 0
	}
	// 創建一個 ResampleRatio 用於控制播放速度
	speedy := beep.ResampleRatio(4, 1, volume)
	speaker.Play(speedy)
	var cmd string
	for {
		fmt.Println("p	pause/resume")
		fmt.Println("+	volume + 0.5")
		fmt.Println("-	volume - 0.5")
		fmt.Println("f	speedy + 0.1")
		fmt.Println("l	speedy - 0.1")
		fmt.Println("d	duration + 0.5")
		fmt.Println("a	duration - 0.5")
		fmt.Println("i	print info")
		fmt.Scanln(&cmd)
		fmt.Println(cmd)
		speaker.Lock()
		switch cmd {
		case "p":
			ctrl.Paused = !ctrl.Paused
		case "+":
			volume.Volume += 0.5
		case "-":
			volume.Volume -= 0.5
		case "f":
			speedy.SetRatio(speedy.Ratio() + 0.1)
		case "l":
			speedy.SetRatio(speedy.Ratio() - 0.1)
		case "d":
			current := streamer.Position()
			t := format.SampleRate.D(current).Round(time.Second) + time.Second*5
			offset := format.SampleRate.N(t)
			if offset != current {
				streamer.Seek(offset)
			}
		case "a":
			current := streamer.Position()
			t := format.SampleRate.D(current).Round(time.Second) - time.Second*5
			if t > 0 {
				offset := format.SampleRate.N(t)
				streamer.Seek(offset)
			}
		case "i":
			fmt.Println("offset: ", format.SampleRate.D(streamer.Position()).Round(time.Second))
			fmt.Println("volume: ", volume.Volume, volume.Silent)
			fmt.Println("speedy: ", speedy.Ratio())
		}
		speaker.Unlock()
	}
}
```