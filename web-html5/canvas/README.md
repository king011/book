# canvas

canvas 是 html5 新組的組件 提供了一個 畫布 可以使用 js 進行 繪圖 以左上角爲座標原點

使用 canvas 標籤創建 如果沒指定 width hight 則 默認爲 300 150

> css 只能調整 顯示 大小 而 canvas 畫布的繪製大小 只能通過 width/height 實現設置 (或者 在js中設置 width/height)
> 

```
#info="index.html"
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <title>html5 test</title>
    <style>
        #view {
            background: darkgray;
        }
    </style>
</head>

<body>
    <canvas id="view" width="400" height="400">
        請使用 現代瀏覽器 你是遠古怪獸嗎?
    </canvas>
</body>
<script src="dist/main.js"></script>

</html>
```

```ts
#info="main.ts"
class Point2D {
    x: number = 0
    y: number = 0
    constructor(x?: number, y?: number) {
        if (x) {
            this.x = x
        }
        if (y) {
            this.y = y
        }
    }
}
class Draw2D {
    canvas2d: CanvasRenderingContext2D
    constructor(element: HTMLCanvasElement) {
        // 返回 2d 繪製環境
        if (!element.getContext) {
            throw "不支持 canvas"
        }
        this.canvas2d = element.getContext("2d") as CanvasRenderingContext2D
    }
    // 保存 繪製環境
    private saveFillStyle(fillStyle?: null | string | CanvasGradient | CanvasPattern): null | string | CanvasGradient | CanvasPattern {
        if (fillStyle != null
            && fillStyle != undefined
            && fillStyle != this.canvas2d.fillStyle
        ) {
            const save = this.canvas2d.fillStyle
            this.canvas2d.fillStyle = fillStyle
            return save
        }
        return null
    }
    private saveStrokeStyle(strokeStyle?: null | string | CanvasGradient | CanvasPattern): null | string | CanvasGradient | CanvasPattern {
        if (strokeStyle != null
            && strokeStyle != undefined
            && strokeStyle != this.canvas2d.strokeStyle
        ) {
            const save = this.canvas2d.strokeStyle
            this.canvas2d.strokeStyle = strokeStyle
            return save
        }
        return null
    }
    private restoreFillStyle(fillStyle?: null | string | CanvasGradient | CanvasPattern) {
        if (fillStyle != null
            && fillStyle != undefined
            && this.canvas2d.fillStyle != fillStyle
        ) {
            this.canvas2d.fillStyle = fillStyle
        }
    }
    private restoreStrokeStyle(strokeStyle?: null | string | CanvasGradient | CanvasPattern) {
        if (strokeStyle != null
            && strokeStyle != undefined
            && this.canvas2d.strokeStyle != strokeStyle
        ) {
            this.canvas2d.strokeStyle = strokeStyle
        }
    }
    // 繪製 矩形
    fillRect(x: number, y: number,
        width: number, height: number,
        fillStyle?: null | string | CanvasGradient | CanvasPattern
    ) {
        const canvas2d = this.canvas2d
        const backup = this.saveFillStyle(fillStyle)
        canvas2d.fillRect(x, y, width, height)
        this.restoreFillStyle(backup)
    }
    // 繪製 線條
    stroke(points?: Array<Point2D>,
        strokeStyle?: null | string | CanvasGradient | CanvasPattern
    ) {
        if (!points || points.length < 1) {
            return
        }
        const backup = this.saveStrokeStyle(strokeStyle)
        const canvas2d = this.canvas2d
        for (let i = 0; i < points.length; i++) {
            const point = points[i]
            if (i == 0) {
                // 首先移動的 繪製起點
                canvas2d.moveTo(point.x, point.y)
            } else {
                // 依次繪製 線段
                canvas2d.lineTo(point.x, point.y)
            }
        }

        // 繪製線段
        canvas2d.stroke()
        // canvas2d.fill() // 填充
        this.restoreStrokeStyle(backup)
    }
    // 繪製 圓形
    arc(x: number, y: number,
        radius: number,
        startAngle: number, endAngle: number,
        anticlockwise?: boolean,
        strokeStyle?: null | string | CanvasGradient | CanvasPattern,
    ) {
        const backup = this.saveStrokeStyle(strokeStyle)
        const canvas2d = this.canvas2d

        canvas2d.beginPath()
        canvas2d.arc(x, y, radius, startAngle, endAngle, anticlockwise)

        // 繪製線段
        canvas2d.stroke()
        // canvas2d.fill() // 填充

        this.restoreStrokeStyle(backup)
    }
    // 繪製 文本
    text(str: string, x: number, y: number, maxWidth?: number) {
        const canvas2d = this.canvas2d
        const backup = {
            shadowOffsetX: canvas2d.shadowOffsetX,
            shadowOffsetY: canvas2d.shadowOffsetY,
            shadowBlur: canvas2d.shadowBlur,
            shadowColor: canvas2d.shadowColor,

            font: canvas2d.font,
            fillStyle: canvas2d.fillStyle,
        }
        // 設置陰影
        canvas2d.shadowOffsetX = 4
        canvas2d.shadowOffsetY = 4
        canvas2d.shadowBlur = 4
        canvas2d.shadowColor = '#FF0000'
        // 設置 顏色 字體
        canvas2d.font = '30px Arial'
        canvas2d.fillStyle = '#333333'

        canvas2d.fillText(str, x, y + 16, maxWidth)        // 繪製文本
        canvas2d.strokeText(str, x, y + 16 + 30, maxWidth) // 繪製中空文本

        // restore
        canvas2d.shadowOffsetX = backup.shadowOffsetX
        canvas2d.shadowOffsetY = backup.shadowOffsetY
        canvas2d.shadowBlur = backup.shadowBlur
        canvas2d.shadowColor = backup.shadowColor

        canvas2d.font = backup.font
        canvas2d.fillStyle = backup.fillStyle
    }
    // path
    path(x: number, y: number, width: number, height: number) {
        const canvas2d = this.canvas2d
        const backup = {
            strokeStyle: canvas2d.strokeStyle,
        }

        // 可以 定義一個 path 傳遞給 stroke fill
        const rect = new Path2D()
        rect.rect(x, y, width, height)
        const arc = new Path2D()
        const radius = Math.min(width - 30, height - 30) / 2
        arc.arc(x + width / 2, y + height / 2, radius, 0, Math.PI * 2)
        rect.addPath(arc)
        canvas2d.strokeStyle = "yellow"
        canvas2d.stroke(rect)

        // restore
        canvas2d.strokeStyle = backup.strokeStyle
    }
}
const element = document.getElementById("view") as HTMLCanvasElement
// 獲取 繪圖 對象 canvasRenderingContext
if (element.getContext) {
    console.log(element.width, element.height)
    const draw = new Draw2D(element)
    let x = 10, y = 10, width = 50, hegith = 50
    draw.fillRect(x, y, width, hegith, "#ff0000")
    x += width + 10

    draw.stroke(
        [
            new Point2D(x + width / 2, y),
            new Point2D(x + width, y + hegith),
            new Point2D(x, y + hegith),
            new Point2D(x + width / 2, y),
        ],
        "yellow"
    )
    x += width + 10

    draw.arc(x + width / 2, y + hegith / 2, width / 2, 0,
        Math.PI * 2,
        undefined,
        "blue",
    )

    x = 10
    y = 90
    draw.text("cerberus is an idea", x, y)

    width = 180
    hegith = 200
    draw.path(100, 150, width, hegith)
} else {
    alert("不支持 canvas")
}
```