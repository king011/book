# qml 訪問 c++

可以將 一個 QObject 設置爲 到QQmlContext  
qml 可以直接 調用 QObject 中的 所有 slots 並且可以 訪問 QObject 的屬性 以及 監聽其 signals

## Bridge

```
#info="bridge.h"
#ifndef BRIDGE_H
#define BRIDGE_H
 
#include <QObject>
 
class Bridge : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString val READ getVal WRITE setVal NOTIFY valChanged)
public:
    explicit Bridge(QObject *parent = nullptr);
 
signals:
    void valChanged(const QString& val);
 
public slots:
 
private:
    QString _val;
public:
    QString getVal();
    void setVal(const QString& val);
};
 
#endif // BRIDGE_H
```

```
#info="bridge.cpp"
#include "bridge.h"
#include <QDebug>
Bridge::Bridge(QObject *parent) : QObject(parent)
{
 
}
 
QString Bridge::getVal()
{
    return _val;
}
void Bridge::setVal(const QString& val)
{
    if(val == _val)
    {
        return;
    }
    _val = val;
    emit valChanged(val);
}
```

## code

```
#info="main.cpp"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include  "bridge.h"
int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
 
    QGuiApplication app(argc, argv);
 
 
    QQmlApplicationEngine engine;
 
    QQmlContext* context = engine.rootContext();
    //實例化 橋 並設置 到qml環境
    Bridge* bridge = new Bridge();
    context->setContextProperty("Bridge",bridge);
 
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
 
    return app.exec();
}
```

```
#info="main.qml"
import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
ApplicationWindow {
  id: window
  visible: true
  title: "Hello World Example"
  minimumWidth: 400
  minimumHeight: 400
 
  ColumnLayout{
      TextField{
          id:textVal
      }
 
      Button{
          text: "set"
          onClicked: Bridge.val = textVal.text //設置屬性
      }
 
      Button{
          text: "get"
          onClicked: console.log("now val =",Bridge.val) //訪問屬性
      }
      //連接 signals
      Connections{
          target: Bridge
          onValChanged:console.log("new val =",val)
      }
  }
}
```

> new Bridge 沒有 delete 因爲 程式結束時 os 會自動釋放 所有資源
> 
> 要 確保 Bridge 的 資源 在 engine 生命期內 都有效
> 
# c++ 訪問 qml

qt 同樣提供了 qml 訪問 c++ 的 接口  
然 這 既不 符合設計美學 又 麻煩 用處也不大 最重要 孤用不到 添加此文字 只是爲了 說明的 完整性  
詳情 查看 qt 幫助