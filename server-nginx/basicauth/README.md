# basic auth

nginx 支持了 http的 basic auth

只需要設置 auth\_basic auth\_basic\_user\_file 即可
```
location / {
	# nginx 顯示給用戶的 用戶密碼 輸入 提示
	auth_basic        "input you user name and password";
	# 用戶密碼 配置 檔案
	auth_basic_user_file    /etc/nginx/basic_password;			
}

```

## 用戶密碼 檔案 

每個 用戶 一行 以 **用戶名:密碼** 形式 保存

使用 openssl 即可 創建 加密 密碼

```
echo -n 'king:' >> basic_password
openssl passwd -apr1 >> basic_password
```