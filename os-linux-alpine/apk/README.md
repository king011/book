# [apk](https://pkgs.alpinelinux.org/packages)

alpine 使用 apk 管理軟件包

```
# 安裝 curl 包
apk add curl
apk add --no-cache curl # docker 中加上 no-cache 避免創建緩存

# 安裝 時區數據
apk add tzdata

# 刪除 curl 包
apk del curl

# 更新包索引
apk update

# 查找  curl 包
apk search  curl 
```

# drill

```
apk add drill 
```

drill 替代了 dig 可以執行 dns 查詢

# su-exec

類似 gosu 通常用來 docker 中，可以使其運行的命名作爲 pid 1
```
apk add --no-cache curl su-exec
```

```
su-exec username sleep 3600
```
