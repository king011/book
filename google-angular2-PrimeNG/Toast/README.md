# [Toast](https://primeng.org/toast)

Toast 用於在 overlay 層中顯示消息

```
import {ToastModule} from 'primeng/toast';
```

```
<p-toast></p-toast>
```

單個消息 Message 接口指定

```
export interface Message {
  /**
   * 消息嚴重性
   */
  severity?: "success" | "info" | "warn" | "error"
  /**
   * 消息概要
   */
  summary?: string
  /**
   * 消息詳細文本
   */
  detail?: string
  /**
   * 消息 id
   */
  id?: any
  /**
   * 與 MessageService 進行匹配的值
   */
  key?: string
  /**
   * 等待多久關閉消息
   * @defaultValue 3000
   */
  life?: number
  /**
   * 如果爲 true 不要自動關閉消息
   * @defaultValue false
   */
  sticky?: boolean
  /**
   * 允許關閉
   */
  closable?: boolean
  /**
   * 與消息關聯的任意對象
   */
  data?: any
  /**
   * 組件內聯的 css syle
   */
  styleClass?: string
  /**
   * 內容內聯的 css class
   */
  contentStyleClass?: string
}
```
```
@Component({
  providers: [MessageService],
})
export class ToastDemoComponent {
  addSingle(key?: string) {
    this.messageService.add(
      {
        // 指定要在哪個 p-messages 中顯示
        key: key,
        // 消息嚴重性
        severity: 'success',
        // 消息概括文本
        summary: 'Service Message',
        // 消息詳細文本 
        detail: 'Via MessageService',
      }
    )
  }

  addMultiple(key?: string) {
    this.messageService.addAll([
      { key: key, severity: 'success', summary: 'Service Message', detail: 'Via MessageService' },
      { key: key, severity: 'info', summary: 'Info Message', detail: 'Via MessageService' },
    ])
  }
  hide(key?: string) {
    // 要清除哪個 p-messages
    // 這裏與 add 表現有點不一致，如果 key 爲 undefined 會清空所有 p-messages
    this.messageService.clear(key)
  }
}
```

## Positions

**position** 屬性用於指定消息顯示位置

* top-right
* top-left
* bottom-right
* bottom-left
* top-center
* bottom-center
* center

```
<p-toast position="top-left"></p-toast>
```

## Templating

允許使用 template 自定義消息

```
<p-toast position="center" key="c" (onClose)="onReject()" [baseZIndex]="5000">
    <ng-template let-message pTemplate="message">
        <div class="flex flex-column" style="flex: 1">
            <div class="text-center">
                <i class="pi pi-exclamation-triangle" style="font-size: 3rem"></i>
                <h4>{{message.summary}}</h4>
                <p>{{message.detail}}</p>
            </div>
            <div class="grid p-fluid">
                <div class="col-6">
                    <button type="button" pButton (click)="onConfirm()" label="Yes" class="p-button-success"></button>
                </div>
                <div class="col-6">
                    <button type="button" pButton (click)="onReject()" label="No" class="p-button-secondary"></button>
                </div>
            </div>
        </div>
    </ng-template>
</p-toast>
```

## Responsive

你可以使用 **breakpoints** 屬性來支持響應式佈局，屏幕寬度低於 961px 寬度爲 100% 同時 left/right 爲 0。（breakpoints 的 key 是屏幕最大尺寸，value 是對應屏幕 toast 的寬度和位置）

```
<p-toast [breakpoints]="{'920px': {width: '100%', right: '0', left: '0'}}"></p-toast>
```

## Animation Configuration

* **showTransitionOptions/hideTransitionOptions** 屬性指定過渡動畫
* **showTransformOptions/hideTransformOptions** 屬性指定轉爲動畫

下面例子將完全禁止過渡動畫

```
<p-toast [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'"></p-toast>
```

下面例子動畫從左到右出現
```
<p-toast [showTransformOptions]="'translateX(100%)'"></p-toast>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| key     | string     | null     | 與 MessageService 進行匹配的 key     |
| style     | string     | null     | 組件內聯的 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| position     | string     | top-right     | 組件顯示位置: "top-right", "top-left", "bottom-left", "bottom-right", "top-center, "bottom-center" and "center"     |
| baseZIndex     | number     | 0     | 分層的基礎 zIndex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| showTransitionOptions     | string     | 300ms ease-out     | 顯示過渡動畫     |
| hideTransitionOptions     | string     | 250ms ease-in     | 隱藏過渡動畫     |
| showTransformOptions     | string     | translateY(100%)     | 出現過渡動畫     |
| hideTransformOptions     | string     | translateY(-100%)     | 離開過渡動畫     |
| preventOpenDuplicates     | boolean     | false     | 當設置爲true時如果已經顯示了相同內容則不會添加消息     |
| breakpoints     | object     | null     | 用於響應式佈局     |
| preventDuplicates     | boolean     | false     | 當設置爲true時僅顯示一次具有相同內容的消息     |

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onClose     | event.message: Removed message     | Callback to invoke when a message is closed.     |

# Templates

| Name | Parameters |
| -------- | -------- |
| message     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-toast     | Main container element.     |
| p-toast-message     | Container of a message item.     |
| p-toast-icon-close     | Close icon of a message.     |
| p-toast-icon     | Severity icon.     |
| p-toast-message-content     | Container of message texts.     |
| p-toast-summary     | Summary of the message.     |
| p-toast-detail     | Detail of the message.     |


