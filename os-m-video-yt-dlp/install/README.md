# 安裝

yt-dlp 使用 python3 開發最簡單的安裝方式是使用 pip

```
sudo apt-get install python3-pip
```

```
python3 -m pip install -U yt-dlp
```

# 更新
重新運行一次 pip 指令即可更新

```
python3 -m pip install -U yt-dlp
```

# 配置

yt-dlp 類似 youtube-dl 同樣提供了配置檔案但全局配置在 **/etc/yt-dlp.conf** 用戶配置在 **~/yt-dlp.conf**

```
# 總是 提取音頻
-x

# 不要使用 Last-modified  設置到下載檔案的 mtime
#--no-mtime

# 使用代理
# --proxy 127.0.0.1:8118
--proxy socks5://127.0.0.1:1080

# 將視頻保存到 youtube 檔案夾下
-o ~/youtube/%(title)s.%(ext)s
```