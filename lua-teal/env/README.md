# tl

tl 是 teal 的編譯器，對於 linux-x86_64 和 windows-x86_64，你可以在 [releases](https://github.com/teal-language/tl/releases)此下載二進制檔案並將其加入 PATH 環境即可

tl 提供了幾個常用命令

* **tl run script.tl** 運行一個 Teal 腳本
* **tl check module.tl** 檢查一個 Teal 模塊，報告任何錯誤並退出
* **tl gen module.tl** 將一個 Teal 模塊編程爲 lua 腳本(其中刪除了所有類型註釋)
* **tl build** 依據 **tlconfig.lua** 中的定義規則編譯項目
* **tl warnings** 列出編譯器可以生成的所有警告

# vscode

如果使用 vscode 可以安裝官方提供的插件

```
ext install pdesaulniers.vscode-teal
```

# tlconfig.lua

在項目根目錄下創建一個 [tlconfig.lua](https://github.com/teal-language/tl/blob/master/docs/compiler_options.md) 檔案用於設定 teal 編譯器如何工作

```
#info="tlconfig.lua"

return {
    gen_compat="off", --不需要生成兼容代碼
    gen_target="5.1",--生成 lua 版本
    source_dir="src",--源碼目錄
    build_dir="dist",--輸出目錄
    include_dir = {
        "types/",--聲明檔案
    },
}
```
