# px

px 是固定像素，是相對於顯示器分辨率而言的，一旦設置就無法因爲適應頁面大小而改變。故通常不要使用這個單位，這算是舊時代的遺留產物。

通常瀏覽器字體的默認像素是 16px

# em rem

em 被定義爲當前字體大小 比如1.2em 代表當前字體大小\*1.2

em 每次都相對當前字體 逐層放大
```
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <title>html test</title>

</head>
<style>
    body {
        font-size: 14px;
    }

    div {
        font-size: 1.2em;
    }
</style>

<body>
    <div> Test
        <!-- 14 * 1.2 = 16.8px -->
        <div> Test
            <!-- 16.8 * 1.2 = 20.16px -->
            <div> Test
                <!-- 20.16 * 1.2 = 24.192px -->
            </div>
        </div>
    </div>
</body>

</html>
```

rem 的 r 代表 root 通常是 html 元素 用來將長度 按照固定根節點來 縮放

```
html {
		font-size: 14px;
}

div {
		font-size: 1.2rem;
}
```

# vh vw

vh vw 將 屏幕寬高 分成 100 等分 很適合用來 按照屏幕大小佈局

```
.slide {
    height: 100vh;
}
```

# vmin vmax

* vmin 相當於 min(vh,vw)
* vmax 相當於 max(vh,vw)

