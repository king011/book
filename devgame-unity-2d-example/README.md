# unity 2d 遊戲示例

這基本上是在看 [https://pixelnest.io/tutorials/2d-game-unity/](https://pixelnest.io/tutorials/2d-game-unity/) 時做的中文翻譯記錄以便本喵下次可以簡單的查詢到中文資料

此外本系列文章修改了原文中部分內容 以使其在 linux-unity-2020.3.0f1 上可用

[示例代碼](https://github.com/zuiwuchang/example/tree/main/unity/2d/2d-game-unity)