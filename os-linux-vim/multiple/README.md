# 多檔案編輯

* vim file1 file2 file3 ... 同時打開多個檔案 
* :open file 或 :e file  打開新檔案
* :ls 顯示已打開的檔案
* :f file 修改編輯檔案名稱


# 多屏幕顯示檔案

* :split 簡寫 **:sp** 或 **ctrl+w+n** 在下面添加一個顯示窗口
* :vsplit 簡寫 **:vps** 或 **ctrl+w+v** 在右面添加一個顯示窗口
* ctrl+w+w 切換到下個屏幕
* ctrl+w+h/j/k/l 切換到指定方向的屏幕
* :res +n 當前窗口高度增加n (-n 則減小)
* :vertical res +n 當前窗口寬度增加n (-n 則減小)

# 多檔案間切換

* :bp 切換到上個檔案
* :bn 切換到下個檔案
* :b索引 切換到指定檔案