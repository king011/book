# Operators

rxjs 提供了一些 Operators 函數 這些函數工作原理很簡單 

Operators函數 接收一個 源Observable 作爲參數 然後返回一個新的 目標Observable

目標Observable 會訂閱 源Observable 並將數據進行處理 之後 進行 push

```
import { interval, Observable } from 'rxjs'

// 創建一個 Operators 函數
function map<T>(source: Observable<T>, callback: (v: T) => T) {
    return new Observable((subscriber) => {
        return source.subscribe(
            (value) => {
                try {
                    subscriber.next(callback(value))
                } catch (e) {
                    subscriber.error(e)
                }
            },
            (err) => {
                subscriber.error(err)
            },
            () => {
                subscriber.complete()
            },
        )
    })
}

// 創建源 對象
const source = interval(1000)
map(source, (x: number) => {
    return x * 2
}).subscribe(
    (v) => {
        console.log(v)
    }
)
```

rxjs 默認已經提供了 很多有用的 Operators 函數


# pipe

rxjs5.5 開始 提供了 新的 pipe 風格的 Operators 

```
import { interval, Observable } from 'rxjs'

function map<T>(callback: (v: T) => T) {
    return (source: Observable<T>) => {
        return new Observable<T>(subscriber => {
            source.subscribe({
                next(v) {
                    try {
                        subscriber.next(callback(v))
                    } catch (e) {
                        subscriber.error(e)
                    }
                },
                error(e) {
                    subscriber.error(e)
                },
                complete() {
                    subscriber.complete()
                },
            })
        })
    }
}

interval(1000).pipe(map((v) => {
    return v * 3
})).subscribe(
    (v) => {
        console.log(v)
    },
)
```

Operators 函數 返回一個 creation 函數 creation 函數接收一個 源Observable 並返回 一個 目標Observable


# map

map 需要傳入一個回調函數 每次將 Observable 的值 使用 回調函數進行處理

```
import { interval } from 'rxjs'
import { map } from 'rxjs/operators'

interval(1000).pipe(map((v) => {
    return v * 2
})).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# mapTo

mapTo 將 push 的值 改寫爲固定值

```
import { interval } from 'rxjs'
import { mapTo } from 'rxjs/operators'

interval(1000).pipe(
    mapTo(2)
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# filter

filter 傳入一個 返回 boolean 的 回調函數 只有此回調返回 true的值 纔會被 push

```
import { interval } from 'rxjs'
import { filter } from 'rxjs/operators'

interval(1000).pipe(
    filter((v) => {
        return v % 2 == 0
    })
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# take

take 從 源Observable中 最多取 指定次數 數據 後 complete

```
import { interval } from 'rxjs'
import { take } from 'rxjs/operators'

interval(1000).pipe(
    take(2)
).subscribe({
    next(v) {
        console.log(v)
    },
    complete() {
        console.log("complete")
    },
})
```

# first
first 是 take(1) 的語法糖

```
import { interval } from 'rxjs'
import { first } from 'rxjs/operators'

interval(1000).pipe(
    first()
).subscribe({
    next(v) {
        console.log(v)
    },
    complete() {
        console.log("complete")
    },
})
```

# takeUntil
takeUntil 直到某個事件發生前 否則 一直 push 數據

```
import { interval, fromEvent } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

interval(1000).pipe(
    takeUntil(fromEvent(document.body, "click"))
).subscribe({
    next(v) {
        console.log(v)
    },
    complete() {
        console.log("complete")
    },
})
```

# concatAll
有時 Observable 發送的數據 本身也是 Observable concatAll 在此時可以將 發送的Observable攤平一般數據發送

```
import { of } from 'rxjs'
import { concatAll } from 'rxjs/operators'

of(
    of(1, 2, 3),
    of(4, 5, 6),
).pipe(
    concatAll()
).subscribe({
    next(v) {
        console.log(v)
    },
    complete() {
        console.log("complete")
    },
})
```

# skip

skip 可以 跳過 Observable 前面 指定個數據

```
import { interval } from 'rxjs'
import { skip } from 'rxjs/operators'

interval(1000).pipe(
    skip(2)
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# takeLast
takeLast 可以用來 獲取 最後n個數據 但 takeLast 必須等到 Observable complete 才知道哪些是最後數據 並同步送出

```
import { interval } from 'rxjs'
import { take, takeLast } from 'rxjs/operators'

// 3 4 會在同一時間 依次打印
interval(1000).pipe(
    take(5),
    takeLast(2),
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# last

last 是 takeLast(1) 的語法糖

# concat
concat 會把 多個 Observable 合併在一起 依次 執行 後一個需要等待前一個 complete 才會開始執行

```
import { of, interval } from 'rxjs'
import { concat, take } from 'rxjs/operators'

interval(1000).pipe(
    take(1),
    concat(of(1, 2, 3), of(4, 5, 6))
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# merge
merge 合併 多個 Observable 但 與 concat 不同的是 多個 Observable 會同時執行 而非等待其中一個完成 才執行 另外一個

```
import { empty, interval } from 'rxjs'
import { merge, take, map } from 'rxjs/operators'

empty().pipe(
    merge(
        interval(1000).pipe(
            take(3)
        ),
        interval(1000).pipe(
            take(3),
            map((v) => v + 3)
        ),
    )
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# startWith

startWith 可以在 Observable 發送數據前 塞入額外 指定數據

```
import { interval } from 'rxjs'
import { startWith, take } from 'rxjs/operators'

// -2 -1 0 1
interval(1000).pipe(
    take(2),
    startWith(-2, -1)
).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# combineLatest

combineLatest 當 多個 Observable 都有值時 發送 各個 Observable 最近的 值

這通常可以用來計算人體比例 比如 身高變化了 就用最近的 身高 體重算比例 如果 體重變了 則同樣用最近的 身高 體重算比例

combineLatest 必須等到 多個 Observable 最近都出現了 值 才會 next 發送數據

```
import { interval } from 'rxjs'
import { combineLatest, map } from 'rxjs/operators'

interval(1000).pipe(
    map((v) => {
        return 1000 * v
    }),
    combineLatest(
        interval(100),
        (a, b) => {
            return a + b
        }
    )
).subscribe(
    (v) => {
        console.log(v)
    }
)
```

# withLatestFrom
withLatestFrom 類似 combineLatest 但 withLatestFrom 有主次關係 只有在 主 Observable 發生變化時 才會 next 值

當然 依然要 多個 Observable 都有值 才會push數據

```
import { interval } from 'rxjs'
import { withLatestFrom, map } from 'rxjs/operators'

interval(1000).pipe(
    map((v) => {
        return 1000 * v
    }),
    withLatestFrom(
        interval(100),
        (a, b) => {
            return a + b
        }
    )
).subscribe(
    (v) => {
        console.log(v)
    }
)
```

# zip

zip 將 多個 Observable 中的值 按照相同順序 調用回調 處理 

如果有兩個 Observable a b 

a.next 了 第一個 數據 zip 會等待 b.next 第一個 數據 之後才 next 出 第一個 發送的數據

```
import { interval } from 'rxjs'
import { zip, map } from 'rxjs/operators'

interval(1000).pipe(
    map((v) => {
        return 1000 * v
    }),
    zip(
        interval(100),
        (a, b) => {
            return a + b
        }
    )
).subscribe(
    (v) => {
        console.log(v)
    }
)
```

zip 必須 緩存沒有 處理 的數據 必須 a.next 了 但 b.next 沒有發來 此時 必須 緩存 a.next 來的數據

如果 此時 多個 Observable next 頻率不一致 很可能導致 內存問題 故要慎重選擇 zip


