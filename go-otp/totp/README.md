# totp

totp 即基於時間戳的 otp，它通常要求服務器和客戶端時間大概一致(上下偏移30秒)，然後使用共享的密鑰每隔30秒產生一個一次性密碼

```
package main

import (
	"fmt"
	"log"
	"time"

	"github.com/pquerna/otp/totp"
)

func main() {
	// 創建一個 totp
	key, e := totp.Generate(totp.GenerateOpts{
		Issuer:      "Example.com",
		AccountName: "alice@example.com",
	})
	if e != nil {
		log.Fatalln(e)
	}
	// 獲取密鑰，你可以把它導入到 google authenticator 等程式中用於生成一次性密碼
	secret := key.Secret()

	// 生成當前的一次性密碼，使用 google authenticator 等程式會得到此值
	once, e := totp.GenerateCode(secret, time.Now())
	if e != nil {
		log.Fatalln(e)
	}

	// 服務器使用 Validate 來驗證一次性密碼
	fmt.Println(totp.Validate(once, secret))
}
```