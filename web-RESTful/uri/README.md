# uri名詞

uri名稱用來標識資源 通常由 prefix + API endpoint 組成

# prefix 
prefix 是可選 例如

* /api 
* /api/v1

通常 不使用 vX.X.X 形式 因爲 通常 RESTful 不會有如此頻繁的更新

另外也有人 提倡 將 version 由 uri 移動到 HEADER 中

# API endpoint

* 一般使用複數名詞 /books 或 /books/123
* 唯一資源用 單數名稱 例如 [GitHub watching API](https://developer.github.com/v3/activity/watching/#list-repositories-being-watched) 的 /user/subscriptions 中 user 指目前驗證的使用者
* 資源的層級架構 可以適當反應在API endpoint設計上 例如 /books/123/chapters/2
* Utility API與resource API性質不同，它的endpoint設計只要合理即可，例如/search?q={keywords}
* 建議 uri 使用全小寫模式 多個關鍵字用 - 或 _ 分隔
