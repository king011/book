# Divider 分隔線

[Divider](https://docs.flutter.io/flutter/material/Divider-class.html) 是一個 邏輯1px的水平分隔線

```dart
#info=false
Divider()
```

# ListTile

[ListTile](https://docs.flutter.io/flutter/material/ListTile-class.html) 是一個 固定高度的行 通常帶有

* 一個行前 圖標
* 一些文本
* 一些文本 附標
* 一個行尾 圖標

```dart
ListTile(
	dense: true, // 密集 排列
	leading: Icon(Icons.group), // 前置圖標
	title: Text('分享對象'), // 標題
	subtitle: Text('副標題'),
	trailing: Icon(Icons.group), //  後置圖標
	onTap: () => debugPrint("分享對象"),
	onLongPress: () => debugPrint("分享對象 LongPress"),
)
```

# Stepper

[Stepper](https://api.flutter.dev/flutter/material/Stepper-class.html) 是一個步驟指示器 用於引導用戶按步驟完成工作

![](assets/Stepper.png)

```
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentStep = 0;
  @override
  Widget build(BuildContext context) {
    final steps = <Step>[
      Step(
        title: Text('步驟一'),
        content: Text('內容一'),
        isActive: _currentStep == 0,
        state: StepState.indexed, // 狀態 索引
      ),
      Step(
        title: Text('步驟二'),
        content: Text('內容二'),
        isActive: _currentStep == 1,
        state: StepState.editing, // 狀態 編輯中
      ),
      Step(
        title: Text('步驟三'),
        content: Text('內容三'),
        isActive: _currentStep == 2,
        state: StepState.complete, // 狀態 完成
      ),
      Step(
        title: Text('步驟四'),
        content: Text('內容四'),
        isActive: _currentStep == 3,
        state: StepState.disabled, // 狀態 禁用
      ),
      Step(
        title: Text('步驟五'),
        content: Text('內容五'),
        isActive: _currentStep == 3,
        state: StepState.error, // 狀態 錯誤
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stepper(
        //type: StepperType.vertical,
        currentStep: _currentStep,
        steps: steps,
        onStepTapped: (step) =>
            step == _currentStep ? null : setState(() => _currentStep = step),
        onStepCancel:
            _currentStep < 1 ? null : () => setState(() => _currentStep--),
        onStepContinue: _currentStep + 1 >= steps.length
            ? null
            : () => setState(() => _currentStep++),
      ),
    );
  }
}
```

> Stepper 的 steps 屬性一旦指定了 Step 數組 則其長度就不可改變(步驟數不可變)
>
> Step 的 state 屬性 應該按照業務邏輯設置到 正確的狀態以使 Step 顯的更友好