# TreeView

* Import Statement: **import QtQuick.Controls 1.4**
* Inherits: **Qt 5.5**
* Inherits: **ScrollView**

TreeView 可以顯示 實現了 QAbstractItemModel 的 模型 故 使用 TreeView 通常 需要 如下 步驟

1. 創建一個 class 存儲 樹 節點 (後文示例命名爲 TreeItem)
2. 創建一個 實現了 QAbstractItemModel 的 model 來 操作 TreeItem 樹 (後文示例命名爲 TreeModel)
3. 將 TreeModel 實例化 並註冊到 qml
4. 在 qml 中 定義 TreeView 並指定 model

TreeView 在一個 table中 將其 最左邊的 列 以 樹顯示 形如 ...  
![](assets/view.png)
# TreeItem

TreeItem 是自定義的 節點數據 需要 提供數據訪問接口 以及管理 樹結構 

```cpp
#ifndef TREEITEM_H
#define TREEITEM_H
#include <QVector>
#include <QVariant>

class TreeItem
{
protected:
    // 存儲 當前節點的 數據
    // 對應 table 中的 column
    QVector<QVariant> data_;
    // 是否被選中
    bool checked_ = false;

    // 存儲子 節點
    QVector<TreeItem*> children_;

    // 存儲 父節點
    TreeItem* parent_;
public: // 操作 當前 節點
    explicit TreeItem(const QVector<QVariant>& data,TreeItem* parent=nullptr);
    virtual ~TreeItem();

    // 返回 當前節點 column 值
    QVariant Data(int column) const;
    // 設置 當前節點 column 值
    // 如果 column 不存在 返回 false
    bool Data(int column, const QVariant &value);

    // 返回 data_.size()
    // 對應 table 中 指定行的 列數
    inline int Column()const
    {
        return data_.size();
    }
    inline void Checked(bool yes)
    {
        checked_ = yes;
    }
    inline bool Checked()const
    {
        return checked_;
    }
    inline bool Toggle()
    {
        checked_ = !checked_;
        return checked_;
    }
    // 返回 自己 在 父節點中的 row 位置
    inline int Row()const
    {
       return parent_? parent_->children_.indexOf(const_cast<TreeItem*>(this)):-1;
    }

public: // 操作 父子節點
    // 返回 父節點
    TreeItem* Parent()const;

    // 通過 索引 返回子節點/nullptr
    TreeItem* Child(int i)const;
    // 返回 子節點數量
    int ChildrenCount() const;

    // 添加 子節點
    void PushBack(TreeItem* children);
    void PushFront(TreeItem* children);
    // 刪除 子節點
    void Remove(TreeItem* children);
};
#endif // TREEITEM_H
```

```cpp
#info="treeitem.cpp"

#include "treeitem.h"
#include <QDebug>
TreeItem::TreeItem(const QVector<QVariant> &data,TreeItem* parent):data_(data),parent_(parent)
{
}
TreeItem::~TreeItem()
{
    // 釋放 子節點 指針
    qDeleteAll(children_);
}
QVariant TreeItem::Data(int column) const
{
    return data_.value(column);
}
bool TreeItem::Data(int column, const QVariant &value)
{
    if(column < 0 || column>=data_.size())
    {
        return false;
    }
    data_[column] = value;
    return true;
}
TreeItem* TreeItem::Parent()const
{
    return parent_;
}
TreeItem* TreeItem::Child(int i)const
{
    if(i<0 || i>= children_.size())
    {
        return nullptr;
    }
    return children_.at(i);
}

int TreeItem::ChildrenCount() const
{
    return children_.size();
}

void TreeItem::PushBack(TreeItem* children)
{
    children->parent_ = this;
    children_.push_back(children);
}
void TreeItem::PushFront(TreeItem* children)
{
    children->parent_ = this;
    children_.push_front(children);
}
void TreeItem::Remove(TreeItem* children)
{
    children_.removeOne(children);
}
```

# TreeModel

* TreeModel 需要實現 QAbstractItemModel 接口 以便 viem 能夠 獲取數據
* 需要 提供 一些接口 供 qml 修改 數據

> * 需要實現 roleNames 才能 將 節點數據 返回到 view
> * 插入 新節點 需要調用 beginInsertRows endInsertRows 通知 view 更新視圖
> * 刪除 節點 需要調用 beginRemoveRows endRemoveRows 通知 view 更新視圖
> * 修改 節點 顯示數據 需要 發送 dataChanged 信號 通知 view 更新視圖
> 

```
#info="treemodel.h"
#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QHash>
#include <QByteArray>
#include "treeitem.h"
class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit TreeModel(QObject *parent = nullptr);
protected:
    TreeItem root_;
public: // 實現 QAbstractItemModel
    // rowCount 是實現需要實現的 函數
    // view 調用此 函數來獲取 tree 節點 有多少 子節點
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    // columnCount 返回 當前節點 對應的 TableViewColumn 數量
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    // parent 用來 返回 index 的父節點
    virtual QModelIndex parent(const QModelIndex &index) const override;

    // index 傳入 當前 父節點 和 row column 來 定位一個 節點
    virtual QModelIndex index(int row, int column,const QModelIndex &parent = QModelIndex()) const override;
    // roleNames 返回一個 QHash QByteArray 和 TableViewColumn.role 一一對應
    virtual QHash<int, QByteArray> roleNames() const override;
    // data 返回 指定節點的 指定角色數據(TableViewColumn.role)
    virtual QVariant data(const QModelIndex &index, int role) const override;

public slots: // 一些 操作 model 的 對外接口
    void setChecked(const QModelIndex &index,bool yes);
    bool isChecked(const QModelIndex &index) const;
    bool toggleChecked(const QModelIndex &index);
    bool pushBack(const QModelIndex &index,const QString& name,const QString& lv);
    bool modify(const QModelIndex &index,const QString& name,const QString& lv);
    bool remove(const QModelIndex &index);
};

#endif // TREEMODEL_H

```

```
#info="treemodel.cpp"
#include "treemodel.h"
#include <QDebug>

// 定義 數據 角色
#define ROLE_NAME_ID   1
#define ROLE_NAME_VAL  "name"
#define ROLE_LEVEL_ID  2
#define ROLE_LEVEL_VAL "level"

TreeModel::TreeModel(QObject *parent) :QAbstractItemModel(parent),root_(QVector<QVariant>())
{
    QVector<QVariant> v;
    v.push_back(QString("kate"));
    v.push_back(QString("10"));
    TreeItem* node = new TreeItem(v);
    root_.PushBack(node);

    v[0]=QString("anita");
    v[1]=QString("12");
    node = new TreeItem(v);
    root_.PushBack(node);
    {
        TreeItem* p = node;

        v[0]=QString("cat");
        v[1]=QString("3");
        node = new TreeItem(v);
        p->PushBack(node);
        {
            TreeItem* p = node;
            v[0]=QString("fish");
            v[1]=QString("1");
            node = new TreeItem(v);
            p->PushBack(node);
        }

        v[0]=QString("dog");
        v[1]=QString("4");
        node = new TreeItem(v);
        p->PushBack(node);
    }
}
int TreeModel::rowCount(const QModelIndex &parent ) const
{
    const TreeItem *parentItem;
    if (parent.isValid())
    {
        // internalPointer 返回 QAbstractItemModel::createIndex 創建時 管理的 指針
        parentItem = static_cast<TreeItem*>(parent.internalPointer());
    }
    else
    {
        // parent 無效 則說明 是 獲取 根節點信息
        parentItem = &root_;
    }
    return parentItem->ChildrenCount();
}
int TreeModel::columnCount(const QModelIndex &parent ) const
{
    const TreeItem *parentItem;
    if (parent.isValid())
    {
        // internalPointer 返回 QAbstractItemModel::createIndex 創建時 管理的 指針
        parentItem = static_cast<TreeItem*>(parent.internalPointer());
    }
    else
    {
        // parent 無效 則說明 是 獲取 根節點信息
        parentItem = &root_;
    }

    return parentItem->Column();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
    {
        return QModelIndex();
    }

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->Parent();
    if (parentItem == &root_)
    {
        return QModelIndex();
    }

    return createIndex(parentItem->Row(), 0, parentItem);
}
QModelIndex TreeModel::index(int row, int column,const QModelIndex &parent ) const
{
    const TreeItem* parentItem;
    if (parent.isValid())
    {
        parentItem = static_cast<TreeItem*>(parent.internalPointer());
    }
    else
    {
        parentItem = &root_;
    }

    // 返回子節點 指針
    TreeItem *childItem = parentItem->Child(row);
    if (childItem)
    {
        return createIndex(row, column, childItem);
    }

    return QModelIndex();
}
QHash<int, QByteArray> TreeModel::roleNames() const
{
    QHash<int, QByteArray> names(QAbstractItemModel::roleNames());
    names[ROLE_NAME_ID] = ROLE_NAME_VAL;
    names[ROLE_LEVEL_ID] = ROLE_LEVEL_VAL;
    return names;
}
QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    switch (role)
    {
    case ROLE_NAME_ID:

        return static_cast<TreeItem*>(index.internalPointer())->Data(0);
    case ROLE_LEVEL_ID:
        return static_cast<TreeItem*>(index.internalPointer())->Data(1);
    }
    return QVariant(QString("unknow"));
}
void TreeModel::setChecked(const QModelIndex &index,bool yes)
{
    qDebug()<<"SetChecked"<<index<<yes;
    if(!index.isValid())
    {
        return;
    }

    static_cast<TreeItem*>(index.internalPointer())->Checked(yes);
}
bool TreeModel::isChecked(const QModelIndex &index) const
{
    qDebug()<<"IsChecked"<<index;
    if(!index.isValid())
    {
        return false;
    }

    return static_cast<TreeItem*>(index.internalPointer())->Checked();
}
bool TreeModel::toggleChecked(const QModelIndex &index)
{
    qDebug()<<"ToggleCheckedd"<<index;
    if(!index.isValid())
    {
        return false;
    }

    return static_cast<TreeItem*>(index.internalPointer())->Toggle();
}
bool TreeModel::pushBack(const QModelIndex &index,const QString& name,const QString& lv)
{
    if(!index.isValid())
    {
        return false;
    }

    TreeItem* parent = static_cast<TreeItem*>(index.internalPointer());

    // 插入前 需要 調用 此 函數 通知 view 開始 插入 rows 數據
    beginInsertRows(index, // 父節點 索引
        parent->ChildrenCount(), // 插入 啓始位置
        parent->ChildrenCount()  // 插入 結束位置
    );
    {
        QVector<QVariant> v(2);
        v[0]=name;
        v[1]=lv;
        TreeItem* children = new TreeItem(v,parent);
        parent->PushBack(children);
    }
    // 插入 完成 需要 調用 此函數 通知 view
    endInsertRows();
    return true;
}
bool TreeModel::modify(const QModelIndex &index,const QString& name,const QString& lv)
{
    if(!index.isValid())
    {
        return false;
    }

    TreeItem* node = static_cast<TreeItem*>(index.internalPointer());
    node->Data(0,name);
    node->Data(1,lv);

    // 發送 信號 通知 view 數據 已變化
    QVector<int> roles(2);
    roles[0] = ROLE_NAME_ID;
    roles[1] = ROLE_LEVEL_ID;
    emit dataChanged(index,index,roles);
    return true;
}
bool TreeModel::remove(const QModelIndex &index)
{

    if(!index.isValid())
    {
        return false;
    }

    QModelIndex indexParent = index.parent();
    int row = index.row();
    // 刪除前 需要 調用 此 函數 通知 view 開始 刪除 rows 數據
    beginRemoveRows(indexParent,row,row);
    {
        qDebug()<<"beginRemoveRows"<<indexParent<<row;
        TreeItem* node = static_cast<TreeItem*>(index.internalPointer());
        TreeItem* parent = node->Parent();
        parent->Remove(node);
    }
    // 刪除 完成 需要 調用 此函數 通知 view
    endRemoveRows();
    return true;
}

```

# main
```cpp
#info="main.cpp"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "treemodel.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QQmlContext* context = engine.rootContext();

    //實例化 橋 並設置 到qml環境

    TreeModel* treeModel = new TreeModel();
    context->setContextProperty("treeModel",treeModel);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
```

```
#info="qrc:/main.qml"
import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Controls 1.4 as Controls14
import QtQuick.Layouts 1.11
ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: "test TreeView"

    ColumnLayout
    {
        anchors.fill: parent
        GridLayout {
            Layout.fillWidth: true;
            id: grid
            columns: 4

            Label{
                text: "Name"
            }
            TextField {
                id:textName
                placeholderText: "Enter Name"
            }

            Button {
                text: "New Children"
                onClicked: {
                    treeModel.pushBack(treeView.currentIndex,textName.text,textLevel.text);
                }
            }
            Button {
                text: "Modify"
                onClicked: {
                    treeModel.modify(treeView.currentIndex,textName.text,textLevel.text);
                }
            }

            Label{
                text: "Level"
            }
            TextField {
                id:textLevel
                placeholderText: "Enter Level"
            }

            Button {
                text: "Remove"
                onClicked: {
                    treeModel.remove(treeView.currentIndex);
                }
            }
        }

        Controls14.TreeView{
            id:treeView
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            Controls14.TableViewColumn {
                title: "Name"
                role: "name"
                width: 400
            }
            Controls14.TableViewColumn {
                title: "Level"
                role: "level"
                width: 200
            }
            model: treeModel
            itemDelegate: Item {
                RowLayout{
                    Controls14.CheckBox {
                        checked: treeModel.isChecked(styleData.index)
                        visible: styleData.column===0
                        MouseArea {
                            anchors.fill: parent
                            onClicked:{
                                parent.checked = treeModel.toggleChecked(styleData.index)
                            }
                        }
                    }
                    Text {
                        text: styleData.value
                    }
                }
            }
        }
    }
}
```

