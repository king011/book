# Promise

Promise 是js的異步解決 方案 ES2015 已經將其 納入 標準 提供 語言層的原生支持

現代的 js 執行環境 都已支持 可以直接使用


Promise 的狀態 必須是 

* pending(等待中)
* fulfilled(已經實現)
* rejected(已經拒絕)

```ts
const test: number = 0;
// 實例化 一個 Promise
const promise = new Promise((resolve, reject) => {
    switch (test) {
        case 0:
            resolve("test resolve");
            break;
        case 1:
            reject("test reject");
            break;
    }
});
// 處理結果
promise.then(
    (result) => {
        console.log(result)
    },
    (e) => {
        console.error(e);
    },
)
```

> resolve  reject 函數 會分別以 實現和 拒絕 通知 then 回調  
> 但 resolve  reject 不會使當前 函數 返回 如果 不想讓 resolve  reject 之後的 代碼執行 需要自己 return 函數
> 
> 注意 Promise 在 new 的 時候 就已經執行 了 傳入的 函數 代碼  
> 後面 執行 then 只是 依據 執行 結果 進行 函數 回調

# then catch
可以書寫 多個 then 進行 鏈式操作 且 then的 回調函數 返回值 將 作為 下個 then的 傳入值 
而 通常 可以 不寫 reject 回調 而是 使用 catch 進行 錯誤 處理

```ts
let test: number = 1;
// 實例化 一個 Promise
const promise = new Promise((resolve, reject) => {
    switch (test) {
        case 0:
            resolve(test);
            break;
        case 1:
            reject("test reject");
            break;
    }
});
 
promise.then(
    (result) => {
        let val = result as number
        val++;
        console.log(val)
        return val;
    },
)
.then(
    (result) => {
        let val = result as number
        val++;
        console.log(val)
        return val;
    },
).then(
    (result) => {
        let val = result as number
        val++;
        console.log(val)
        return val;
    },
).catch((e) => {
    console.error("catch", e);
})
```

> reject 和 catch 捕獲到錯誤 後 依然可以 return 一個值 給 後面的 then 執行

# async await
ES2017 開始 如果在 函數前 加上 async 就可以 使用 await

await 接收一個 Promise 使 當前函數 讓出 執行權 並返回  
直到 Promise 完成 或 拒絕後 繼續執行 後續代碼

```ts
async function f() {
    console.log(1)
    await new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, 1000);
    })
    console.log(3)
    await new Promise((resolve, reject) => {
        setTimeout(async () => {
            console.log(4)
 
            await new Promise((r)=>{
                setTimeout(() => {
                    r()
                }, 1000);
            })
            console.log(5);
            resolve();
        }, 1000);
    })
    console.log(6)
}
f()
console.log(2)
```

> await 會把 Promise resolve 的值 作為 返回值  
> 如果 Promise reject 則將 reject 內容 使用 throw 拋出異常 以便 當前函數 能 catch