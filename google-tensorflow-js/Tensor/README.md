# Tensor

張量 Tensor 是一組 n維數組列表 是 TensorFlow.js 中的主要單位

張量 有 三個 基本屬性

* rang 定義張量包含多少個維度
* shape 定義每個維度的大小
* dtype 定義張量的數據類型

# 創建張量

可以使用 tf.tensor 函數 創建 張量

```
import * as tf from '@tensorflow/tfjs-node'
// 創建 rank-2 矩形張量
const a = tf.tensor([[1, 2], [3, 4]])
console.log('shape:', a.shape)
a.print()

// 也可以使用 平面數組 創建 張量
const shape = [2, 2];
const b = tf.tensor([1, 2, 3, 4], shape)
console.log('shape:', b.shape)
b.print()
```

默認 使用 float32 創建張量 也可以顯示指定 **bool int32 complex64 string**

```ts
import * as tf from '@tensorflow/tfjs-node'
const a = tf.tensor([[1, 2], [3, 4]], [2, 2], 'int32')
console.log('shape:', a.shape)
console.log('dtype', a.dtype)
a.print()
```

張量中元素數量是其形狀大小的乘積 通常由多個有相同大小的形狀 使用 reshape 可以改變 形狀

```
import * as tf from '@tensorflow/tfjs-node'
const a = tf.tensor([[1, 2], [3, 4]])
console.log('a shape:', a.shape)
a.print()

const b = a.reshape([4, 1])
console.log('b shape:', b.shape)
b.print()
```

使用 array 或 data 方法 獲取張量中的值

```
import * as tf from '@tensorflow/tfjs-node'
const a = tf.tensor([[1, 2], [3, 4]])
// Returns the multi dimensional array of values.
a.array().then(array => console.log(array))
// Returns the flattened data that backs the tensor.
a.data().then(data => console.log(data))
```

張量提供了 sync 方法 但若非測試 通常不要使用 以免阻塞主線程
```
import * as tf from '@tensorflow/tfjs-node'
const a = tf.tensor([[1, 2], [3, 4]])
// Returns the multi dimensional array of values.
console.log(a.arraySync())
// Returns the flattened data that backs the tensor.
console.log(a.dataSync())
```

# 運算

Tensor 提供了多個方法 對 張量進行運算

```
import * as tf from '@tensorflow/tfjs-node'
const x = tf.tensor([[1, 2], [3, 4]])
const y = x.square() // 計算 平方
y.array().then((arrs) => console.log(arrs))

// 計算 和
const sum = x.add(y)
sum.array().then((arrs) => console.log(arrs))
```

# 內存管理

當使用 **WebGL** 後端時 必須顯示釋放 Tensor 的內存 可以 通過 調用 **tf.dispose()** 或 成員函數**dispose()** 來顯示釋放內存

tf.memory()) 會返回 包含 內存分配情況的 MemoryInfo 實例

```
import * as tf from '@tensorflow/tfjs-node'
const a = tf.tensor([[1, 2], [3, 4]])
console.log(tf.memory())
// tf.dispose(a)
a.dispose()
console.log(tf.memory())
```

tf.tidy 需要傳入一個 回調函數 tidy會在函數返回時 將 沒有返回的 臨時 Tensor 都自動調用 dispose 進行釋放

```
import * as tf from '@tensorflow/tfjs-node'
const a = tf.tensor([[1, 2], [3, 4]])
const y = tf.tidy(() => {
    const result = a.square().log().neg()
    return result
})
console.log(tf.memory())
a.dispose()
console.log(tf.memory())
y.dispose()
console.log(tf.memory())
```

