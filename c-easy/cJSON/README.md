# cJSON

cJSON 是一個開源的(MIT) c  json 解析編碼器

* 源碼 [https://github.com/DaveGamble/cJSON](https://github.com/DaveGamble/cJSON)

# 環境

從 官網下載 源碼 將 **cJSON.h cJSON.c** 加入項目即可

# Example
```c
#include <stdio.h>
#include <stdlib.h>

#include "cJSON.h"
char *jsonMarshalExample();
void jsonUnmarshalExample(const char *str);
#define CheckResult(p) \
    if (!p)            \
        goto tagEnd;

int main(void)
{
    // 自定義 內存分配器 如果傳入NULL 使用 c 的 malloc free realloc
    cJSON_InitHooks(NULL);

    // 測試創建 json
    char *str = jsonMarshalExample();
    if (str)
    {
        jsonUnmarshalExample(str);
        free(str);
    }
    return 0;
}

char *jsonMarshalExample()
{
    char *result = NULL;
    //創建一個 object 作爲 toot節點
    cJSON *root = cJSON_CreateObject();
    if (!root)
    {
        puts("bad create root");
        goto tagEnd;
    }

    // 添加基本屬性
    CheckResult(cJSON_AddBoolToObject(root, "bool", 1));
    cJSON_DeleteItemFromObject(root, "bool"); // 刪除屬性
    CheckResult(cJSON_AddBoolToObject(root, "bool", 0));

    CheckResult(cJSON_AddNumberToObject(root, "int", 123));
    CheckResult(cJSON_AddNumberToObject(root, "float", 123.456));
    CheckResult(cJSON_AddStringToObject(root, "str", "cerberus is an idea"));

    // 添加 數組
    cJSON *arrs = cJSON_AddArrayToObject(root, "arrs");
    if (!arrs)
    {
        puts("bad create array");
        goto tagEnd;
    }
    cJSON *item0 = cJSON_CreateString("cerberus is an idea");
    if (!cJSON_AddItemToArray(arrs, item0))
    {
        cJSON_Delete(item0);
        goto tagEnd;
    }
    cJSON *item1 = cJSON_CreateBool(1);
    if (!cJSON_AddItemToArray(arrs, item1))
    {
        cJSON_Delete(item1);
        goto tagEnd;
    }
    cJSON *item2 = cJSON_CreateNumber(789.123);
    if (!cJSON_AddItemToArray(arrs, item2))
    {
        cJSON_Delete(item2);
        goto tagEnd;
    }

    // 添加 object
    cJSON *obj = cJSON_AddObjectToObject(root, "obj");
    if (!obj)
    {
        puts("bad create object");
        goto tagEnd;
    }

    // 輸出到字符串
    result = cJSON_Print(root);
    // result = cJSON_PrintUnformatted(root); // 同cJSON_Print但不需要美化輸出
    if (!result)
    {
        goto tagEnd;
    }
    puts("--- Marshal Example ---");
    puts(result);
tagEnd:
    if (root)
    {
        // 釋放資源
        cJSON_Delete(root);
    }
    return result;
}

void jsonUnmarshalExample(const char *str)
{
    puts("--- Unmarshal Example ---");

    // 解析 json
    cJSON *root = cJSON_Parse(str);
    // cJSON *root = cJSON_ParseWithLength(str, strlen(str));

    if (!cJSON_IsObject(root))
    {
        goto tagEnd;
    }
    // 獲取屬性
    cJSON *tmp = cJSON_GetObjectItem(root, "bool");
    CheckResult(tmp);
    if (cJSON_IsTrue(tmp))
    {
        puts("bool true");
    }
    else if (cJSON_IsFalse(tmp))
    {
        puts("bool false");
    }
    tmp = cJSON_GetObjectItem(root, "int");
    CheckResult(tmp);
    int d = (int)cJSON_GetNumberValue(tmp);
    printf("int %d\n", d);
    tmp = cJSON_GetObjectItem(root, "float");
    CheckResult(tmp);
    double f = cJSON_GetNumberValue(tmp);
    printf("int %f\n", f);
    tmp = cJSON_GetObjectItem(root, "str");
    CheckResult(tmp);
    str = cJSON_GetStringValue(tmp);
    printf("str %s\n", str);

    // 獲取數組
    cJSON *arrs = cJSON_GetObjectItem(root, "arrs");
    if (cJSON_IsArray(arrs))
    {
        int count = cJSON_GetArraySize(arrs);
        printf("arrs [");
        for (int i = 0; i < count; i++)
        {
            tmp = cJSON_GetArrayItem(arrs, i);
            if (i != 0)
            {
                printf(",");
            }
            if (cJSON_IsTrue(tmp))
            {
                printf("true");
            }
            else if (cJSON_IsFalse(tmp))
            {
                printf("false");
            }
            else if (cJSON_IsString(tmp))
            {
                str = cJSON_GetStringValue(tmp);
                printf("%s", str);
            }
            else if (cJSON_IsNumber(tmp))
            {
                f = cJSON_GetNumberValue(tmp);
                printf("%f", f);
            }
        }
        printf("]\n");
    }
tagEnd:
    if (root)
    {
        // 釋放資源
        cJSON_Delete(root);
    }
}
```