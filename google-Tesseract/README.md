# Tesseract

Tesseract  是一個開源(Apache License 2.0)的文字識別引擎(OCR  optical character recognition)。最初是由  Hewlett-Packard  與 1980 年開發的專有軟體，2005年開源，並由2006年得到 google 贊助同時轉由 google 繼續開發

Tesseract 4.0新增了基於LSTM神經網路的新OCR引擎。它在 x86/Linux 上運作良好，官方語言模型資料可用於 100 多種語言和 35 多種腳本。目前最新版本是 5.4.1（2024-07-03）

* 官網 [https://tesseract-ocr.github.io/](https://tesseract-ocr.github.io/)
* 源碼 [https://github.com/tesseract-ocr/tesseract](https://github.com/tesseract-ocr/tesseract)