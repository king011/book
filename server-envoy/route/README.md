# [config.route.v3.Route](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-route)

路由既是如何匹配請求的規範，也是下一步做什麼的指示（例如，重定向、轉發、重寫等

```typescript
interface Route {
	/**
	 * 路由名稱
	 */
	name?: string
	/**
	 * 路由匹配參數
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routematch config.route.v3.RouteMatch}
	 */
	match: RouteMatch
	/**
	 * 將請求路由到某個上游集群
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction config.route.v3.RouteAction}
	 */
	route?: RouteAction
	/**
	 * 返回重定向
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-redirectaction config.route.v3.RedirectAction}
	 */
	redirect?: RedirectAction
	/**
	 * 直接返回任意響應
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-directresponseaction config.route.v3.DirectResponseAction}
	 */
	direct_response?: DirectResponseAction
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-metadata config.core.v3.Metadata}
	 */
	metadata?: config.core.v3.Metadata
	/**
	 * 匹配路由的裝飾器
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-decorator config.route.v3.Decorator}
	 */
	decorator?: Decorator
	/**
	 * 用於為過濾器提供特定於路由的配置。
	 */
	typed_per_filter_config?: Record<string, any>
	/**
	 * 指定一組標頭，這些標頭將添加到與此路由匹配的請求中
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-headervalueoption config.core.v3.HeaderValueOption}
	 */
	request_headers_to_add?: Array<config.core.v3.HeaderValueOption>
	/**
	 * 指定應從與此路由匹配的每個請求中刪除的 HTTP 標頭列表
	 */
	request_headers_to_remove?: Array<string>
	/**
	 * 指定一組標頭，這些標頭將添加到與此路由匹配的請求的響應中
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-headervalueoption config.core.v3.HeaderValueOption}
	 */
	response_headers_to_add?: Array<config.core.v3.HeaderValueOption>
	/**
	 * 指定應從匹配此請求的每個響應中刪除的 HTTP 標頭列表
	 */
	response_headers_to_remove?: Array<string>
	/**
	 * 對象的存在定義連接管理器的跟踪配置是否被該路由特定實例覆蓋
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-tracing config.route.v3.Tracing}
	 */
	tracing?: Tracing
	/**
	 * uint32 將為重試和重試緩衝的最大字節數
	 * @remarks
	 * 如果設置，實際緩衝的字節將是這個和偵聽器 per_connection_buffer_limit_bytes 的最小值
	 */
	per_request_buffer_limit_bytes?: number
	/**
	 * 為該端點發出統計信息時使用的人類可讀前綴
	 * @remarks
	 * 統計信息以 vhost.<虛擬主機名>.route.<stat_prefix> 為根。
	 * 這應該為人們希望獲得“每條路線”統計數據的高度關鍵端點設置。 
	 * 如果未設置，則不會生成端點統計信息。
	 * 發出的統計信息與為虛擬集群記錄的統計信息相同。
	 */
	stat_prefix?: string
}
```

## match

([config.route.v3.RouteMatch](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routematch)) 路由匹配參數

```typescript
interface RouteMatch {
	/**
	 * 如果指定，路由是一個前綴規則，意味著前綴必須匹配 :path 標頭的開頭
	 */
	prefix?: string
	/**
	 * 如果指定，則該路由是一個精確路徑規則，這意味著一旦刪除查詢字符串，該路徑必須與 :path 標頭完全匹配
	 */
	path?: string
	/**
	 * 如果指定，路由是一個正則表達式規則，這意味著一旦刪除查詢字符串，正則表達式必須匹配 :path 標頭。 
	 * @remarks
	 * 整個路徑（不含查詢字符串）必須與正則表達式匹配。 
	 * 如果只有 :path 標頭的子序列與正則表達式匹配，則該規則將不匹配
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/regex.proto#envoy-v3-api-msg-type-matcher-v3-regexmatcher type.matcher.v3.RegexMatcher}
	 */
	safe_regex?: type.matcher.v3.RegexMatcher
	/**
	 * 如果將其用作匹配器，則匹配器將僅匹配 CONNECT 請求
	 * 
	 * @remarks
	 * 請注意，這將不匹配 HTTP/2 升級樣式的 CONNECT 請求（WebSocket 等），因為它們在 Envoy 中被規範化為 HTTP/1.1 樣式升級。 
	 * 這是匹配 HTTP/1.1 的 CONNECT 請求的唯一方法。 對於 HTTP/2，Extended CONNECT 請求可能有路徑，如果存在路徑，路徑匹配器將工作。 
	 * 請注意，CONNECT 支持目前在 Envoy 中被視為 alpha
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routematch-connectmatcher config.route.v3.RouteMatch.ConnectMatcher}
	 */
	connect_matcher?: any
	/**
	 * 如果指定，路由是路徑分隔的前綴規則，這意味著 :path 標頭（不帶查詢字符串）必須與 path_separated_prefix 完全匹配或將其作為前綴，後跟 /
	 * @remarks
	 * 例如，/api/dev 會匹配 /api/dev、/api/dev/、/api/dev/v1 和 /api/dev?param=true 但不會匹配 /api/developer
	 */
	path_separated_prefix?: string
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig config.core.v3.TypedExtensionConfig}
	 */
	path_match_policy?: config.core.v3.TypedExtensionConfig
	/**
	 * 表示前綴/路徑匹配應該區分大小寫。 不會影響 safe_regex 匹配
	 * @defaultValue true
	 */
	case_sensitive?: boolean
	/**
	 * 指示路由還應匹配運行時密鑰
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-runtimefractionalpercent config.core.v3.RuntimeFractionalPercent}
	 */
	runtime_fraction?: config.core.v3.RuntimeFractionalPercent
	/**
	 * 指定路由應匹配的一組標頭
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-headermatcher config.route.v3.HeaderMatcher}
	 */
	headers?: Array<HeaderMatcher>
	/**
	 * 指定路由應匹配的一組 URL 查詢參數
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-queryparametermatcher config.route.v3.QueryParameterMatcher}
	 */
	query_parameters?: Array<QueryParameterMatcher>
	/**
	 * 如果指定，則只會匹配 gRPC 請求。 
	 * 路由器將檢查 content-type 標頭是否具有 application/grpc 或各種 application/grpc+ 值之一
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routematch-grpcroutematchoptions config.route.v3.RouteMatch.GrpcRouteMatchOptions}
	 */
	grpc?: config.route.v3.RouteMatch.GrpcRouteMatchOptions
	/**
	 * 如果指定，客戶端 tls 上下文將根據定義的匹配選項進行匹配
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routematch-tlscontextmatchoptions config.route.v3.RouteMatch.TlsContextMatchOptions}
	 */
	tls_context?: config.route.v3.RouteMatch.TlsContextMatchOptions
	/**
	 * 指定路由應匹配的一組動態元數據匹配器。 路由器將根據所有指定的動態元數據匹配器檢查動態元
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/metadata.proto#envoy-v3-api-msg-type-matcher-v3-metadatamatcher type.matcher.v3.MetadataMatcher}
	 */
	dynamic_metadata?: Array<type.matcher.v3.MetadataMatcher>
}
```

### headers

([config.route.v3.HeaderMatcher](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-headermatcher)) 指定路由應匹配的一組標頭

```typescript
interface HeaderMatcher {
	/**
	 * 要匹配的 header key 名稱
	 */
	name: string
	/**
	 * 如果指定，將根據標頭的值執行標頭匹配
	 * @deprecated 使用 string_match 替代
	 */
	exact_match?: string
	/**
	 * 如果指定，此正則表達式字符串是一個正則表達式規則，這意味著整個請求標頭值必須與正則表達式匹配。 
	 * 如果只有請求標頭值的子序列與正則表達式匹配，則規則將不匹配
	 * @deprecated 使用 string_match 替代
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/regex.proto#envoy-v3-api-msg-type-matcher-v3-regexmatcher type.matcher.v3.RegexMatcher}
	 */
	safe_regex_match?: type.matcher.v3.RegexMatcher
	/**
	 * 如果指定，將根據范圍執行標頭匹配。 
	 * @remarks
	 * 如果請求標頭值在此範圍內，則規則將匹配。 
	 * 整個請求標頭值必須表示以 10 為基數的整數：由可選的加號或減號組成，後跟一串數字。 
	 * 如果標頭值不表示整數，則規則將不匹配。 對於空值、浮點數或只有標頭值的子序列是整數，匹配將失敗
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/v3/range.proto#envoy-v3-api-msg-type-v3-int64range type.v3.Int64Range}
	 */
	range_match?: type.v3.Int64Range
	/**
	 * 如果指定為 true，將根據請求中是否包含請求頭來匹配請求頭。 
	 * 如果指定為false，則根據header是否缺失進行header匹配。
	 */
	present_match?: boolean
	/**
	 * 如果指定，將根據標頭值的前綴執行標頭匹配。 注意：不允許使用空前綴
	 * @deprecated 使用 string_match 替代
	 */
	prefix_match?: string
	/**
	 * 如果指定，將根據標頭值的後綴執行標頭匹配。 注意：不允許使用空後綴
	 * @deprecated 使用 string_match 替代
	 */
	suffix_match?: string
	/**
	 * 如果指定，將根據標頭值是否包含給定值來執行標頭匹配。 注意：空包含匹配是不允許的
	 * @deprecated 使用 string_match 替代
	 */
	contains_match?: string
	/**
	 * 如果指定，則根據header值的字符串匹配進行header匹配
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/string.proto#envoy-v3-api-msg-type-matcher-v3-stringmatcher type.matcher.v3.StringMatcher}
	 */
	string_match?: type.matcher.v3.StringMatcher
	/**
	 * 如果指定，匹配結果將在檢查之前被反轉
	 * @defaultValue false
	 */
	invert_match?: boolean
	/**
	 * 如果指定，對於任何一個header匹配規則，如果header匹配規則指定的header不存在，這個header值將被視為空
	 * @defaultValue false
	 */
	treat_missing_header_as_empty?: boolean
}
```

#### string\_match

([type.matcher.v3.StringMatcher](https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/string.proto#envoy-v3-api-msg-type-matcher-v3-stringmatcher)) 字符串匹配

```typescript
interface StringMatcher {
	/**
	 * 輸入字符串必須與此處指定的字符串完全匹配
	 */
	exact?: string
	/**
	 * 輸入字符串必須具有此處指定的前綴。 注意：不允許使用空前綴，請改用正則表達式
	 */
	prefix?: string
	/**
	 * 輸入字符串必須具有此處指定的後綴。 注意：不允許使用空前綴，請改用正則表達式
	 */
	suffix?: string
	/**
	 * 輸入字符串必須匹配此處指定的正則表達式
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/regex.proto#envoy-v3-api-msg-type-matcher-v3-regexmatcher type.matcher.v3.RegexMatcher}
	 */
	safe_regex?: RegexMatcher
	/**
	 * 輸入字符串必須具有此處指定的子字符串。 注意：不允許空包含匹配，請改用正則表達式
	 */
	contains?: string
	/**
	 * 如果為真，則表示 exact/prefix/suffix/contains 匹配應該不區分大小寫。 這對 safe_regex 匹配沒有影響。
	 */
	ignore_case?: boolean
}
```

## route
([config.route.v3.RouteAction](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction)) 將請求路由到某個上游集群

route, redirect, direct\_response 中必須且只能設置一個屬性

```typescript
interface RouteAction {
	/**
	 * 指示應將請求路由到的上游集群
	 */
	cluster?: string
	/**
	 * Envoy 將通過從請求標頭中讀取由 cluster_header 命名的 HTTP 標頭的值來確定要路由到的集群。 
	 * 如果找不到標頭或引用的集群不存在，Envoy 將返回 404 響應
	 */
	cluster_header?: string
	/**
	 * 可以為給定路由指定多個上游集群。 根據分配給每個集群的權重，請求被路由到上游集群之一
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-weightedcluster config.route.v3.WeightedCluster}
	 */
	weighted_clusters?: WeightedCluster
	/**
	 * 集群說明符插件的名稱，用於確定此路由上請求的集群
	 */
	cluster_specifier_plugin?: string
	/**
	 * 自定義集群說明符插件配置，用於確定此路由上請求的集群
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-clusterspecifierplugin config.route.v3.ClusterSpecifierPlugin}
	 */
	inline_cluster_specifier_plugin?: ClusterSpecifierPlugin
	/**
	 * 找不到配置的集群時使用的 HTTP 狀態代碼。 默認響應代碼是 503 服務不可用
	 * - SERVICE_UNAVAILABLE 503
	 * - NOT_FOUND 404
	 * - INTERNAL_SERVER_ERROR 500
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-enum-config-route-v3-routeaction-clusternotfoundresponsecode config.route.v3.RouteAction.ClusterNotFoundResponseCod}
	 */
	cluster_not_found_response_code: 'SERVICE_UNAVAILABLE' | 'NOT_FOUND' | 'INTERNAL_SERVER_ERROR'
	/**
	 * 子集負載均衡器使用的可選端點元數據匹配條件。 
	 * 只有元數據與此字段中設置的內容匹配的上游集群中的端點才會被考慮用於負載平衡
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-metadata config.core.v3.Metadata}
	 */
	metadata_match?: config.core.v3.Metadata
	/**
	 * 指示在轉發期間，應將匹配的前綴（或路徑）與此值交換 
	 * @remarks
	 * 此選項允許應用程序 URL 的根路徑與在反向代理層公開的路徑不同。 
	 * 路由器過濾器將在重寫到 x-envoy-original-path 標頭之前放置原始路徑
	 */
	prefix_rewrite?: string
	/**
	 * 指示在轉發期間，應重寫與模式匹配的路徑部分，甚至允許將模式中的捕獲組替換為重寫替換字符串指定的新路徑
	 * @remarks
	 * 這對於允許以識別具有可變內容（如標識符）的段的方式重寫應用程序路徑很有用。 
	 * 路由器過濾器會將重寫之前的原始路徑放入 x-envoy-original-path 標頭中
	 * 
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/regex.proto#envoy-v3-api-msg-type-matcher-v3-regexmatchandsubstitute type.matcher.v3.RegexMatchAndSubstitute}
	 */
	regex_rewrite?: type.matcher.v3.RegexMatchAndSubstitute
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig config.core.v3.TypedExtensionConfig}
	 */
	path_rewrite_policy?: config.core.v3.TypedExtensionConfig
	/**
	 * 指示在轉發期間，host 標頭將與此值交換
	 * @remarks
	 * 如果設置了 append_x_forwarded_host，則使用此選項將附加 x-forwarded-host 標頭
	 */
	host_rewrite_literal?: string
	/**
	 * 指示在轉發期間，主機標頭將與集群管理器選擇的上游主機的主機名交換
	 * @remarks
	 * 僅當路由的目標集群是 strict_dns 或 logical_dns 類型時，此選項才適用。 
	 * 對於其他集群類型將此設置為 true 無效。 
	 * 如果設置了 append_x_forwarded_host，則使用此選項將附加 x-forwarded-host 標頭。
	 */
	auto_host_rewrite?: boolean
	/**
	 * 指示在轉發期間，主機標頭將與給定下游或自定義標頭的內容交換
	 * @remarks
	 * 如果標頭值為空，則主機標頭保持不變。 
	 * 如果設置了 append_x_forwarded_host，則使用此選項將附加 x-forwarded-host 標頭。
	 */
	host_rewrite_header?: string
	/**
	 * 指示在轉發期間，主機標頭將與對路徑值執行正則表達式替換並刪除查詢和片段的結果進行交換
	 * @remarks
	 * 這對於在路徑段和子域之間轉換變量內容很有用。 
	 * 如果設置了 append_x_forwarded_host，則使用此選項將附加 x-forwarded-host 標頭。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/regex.proto#envoy-v3-api-msg-type-matcher-v3-regexmatchandsubstitute type.matcher.v3.RegexMatchAndSubstitute}
	 */
	host_rewrite_path_regex?: type.matcher.v3.RegexMatchAndSubstitute
	/**
	 * 如果設置，則主機重寫操作（host_rewrite_literal、auto_host_rewrite、host_rewrite_header 或 host_rewrite_path_regex 之一）會導致主機標頭的原始值（如果有）附加到 x-forwarded-host HTTP 標頭
	 */
	append_x_forwarded_host?: boolean
	/**
	 * 指定路由的上游超時
	 * @remarks
	 * 這跨越整個下游請求（即流結束）已被處理和上游響應已被完全處理的時間點。
	 * 值為 0 將禁用路由的超時
	 * 
	 * @defaultValue 15s
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	timeout?: string
	/**
	 * 指定路由的空閒超時
	 * @remarks
	 * 如果未指定，則沒有每個路由的空閒超時，儘管連接管理器範圍內的 stream_idle_timeout 仍然適用。 
	 * 值為 0 將完全禁用路由的空閒超時，即使配置了連接管理器流空閒超時也是如此
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	idle_timeout?: string
	/**
	 * 指定如何通過 TLS 早期數據發送請求。 如果不存在，則允許在早期數據上發送安全的 HTTP 請求
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/extension.proto#envoy-v3-api-msg-config-core-v3-typedextensionconfig config.core.v3.TypedExtensionConfig}
	 */
	early_data_policy?: config.core.v3.TypedExtensionConfig
	/**
	 * 表示該路由有重試策略。 請注意，如果設置了此項，它將完全優先於虛擬主機級別的重試策略
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-retrypolicy config.route.v3.RetryPolicy}
	 */
	retry_policy?: config.route.v3.RetryPolicy
	/**
	 * 指定一組路由請求鏡像策略。 它優先於虛擬主機和路由配置鏡像策略。 即不合併策略，最具體的非空策略成為鏡像策略。
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction-requestmirrorpolicy config.route.v3.RouteAction.RequestMirrorPolicy}
	 */
	request_mirror_policies?: Array<config.route.v3.RouteAction.RequestMirrorPolicy>
	/**
	 * 指定路由優先級
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-enum-config-core-v3-routingpriority config.core.v3.RoutingPriority}
	 */
	priority?: config.core.v3.RoutingPriority
	/**
	 * 指定一組可應用於路由的速率限製配置
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-ratelimit config.route.v3.RateLimit}
	 */
	rate_limits?: Array<config.route.v3.RateLimit>
	/**
	 * 指定速率限製過濾器是否應包括虛擬主機速率限制。 默認情況下，如果路由配置了速率限制，則虛擬主機 rate_limits 不會應用於請求
	 */
	include_vh_rate_limits?: boolean
	/**
	 * 指定用於環哈希負載平衡的哈希策略列表
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction-hashpolicy config.route.v3.RouteAction.HashPolicy}
	 */
	hash_policy?: Array<config.route.v3.RouteAction.HashPolicy>
	/**
	 * 指示路由具有 CORS 策略
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-corspolicy config.route.v3.CorsPolicy}
	 */
	cors?: config.route.v3.CorsPolicy
	/**
	 * @deprecated 使用 grpc_timeout_header_max  替代
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	max_grpc_timeout?: string
	/**
	 * @deprecated 使用 grpc_timeout_header_max  替代
	 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
	 */
	grpc_timeout_offset?: string
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction-upgradeconfig config.route.v3.RouteAction.UpgradeConfig}
	 */
	upgrade_configs?: Array<config.route.v3.RouteAction.UpgradeConfig>
	/**
	 * 如果存在，Envoy 將嘗試遵循上游重定向響應，而不是將響應代理回下游。 
	 * 上游重定向響應由 redirect_response_codes 定義
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-internalredirectpolicy config.route.v3.InternalRedirectPolicy}
	 */
	internal_redirect_policy?: config.route.v3.InternalRedirectPolicy
	/**
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-enum-config-route-v3-routeaction-internalredirectaction config.route.v3.RouteAction.InternalRedirectAction}
	 */
	internal_redirect_action?: config.route.v3.RouteAction.InternalRedirectAction
	/**
	 * uint32 如果下游請求遇到的先前內部重定向的數量低於此值，則處理內部重定向，並且 internal_redirect_action 設置為 HANDLE_INTERNAL_REDIRECT 在下游請求通過內部重定向在多個路由之間反彈的情況下，第一個路由 達到此閾值，或將 internal_redirect_action 設置為 PASS_THROUGH_INTERNAL_REDIRECT 會將重定向傳遞回下游。
	 */
	max_internal_redirects?: number
	/**
	 * 表示路由有對沖策略。 請注意，如果設置了此項，它將完全優先於虛擬主機級別的對沖策略
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-hedgepolicy config.route.v3.HedgePolicy}
	 */
	hedge_policy?: config.route.v3.HedgePolicy
	/**
	 * 指定此路由的最大流持續時間
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-routeaction-maxstreamduration config.route.v3.RouteAction.MaxStreamDuration}
	 */
	max_stream_duration?: config.route.v3.RouteAction.MaxStreamDuration
}
```

## redirect
([config.route.v3.RedirectAction](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-redirectaction)) 返回重定向

route, redirect, direct\_response 中必須且只能設置一個屬性

```typescript
interface RedirectAction {
	/**
	 * URL.scheme 替換爲 https
	 * @remarks
	 * 如果源 URI.scheme 是 http 並且端口明確設置為 :80，則端口將在重定向後刪除
	 * 如果源 URI.scheme 是 https 並且端口明確設置為 :443，則端口將在重定向後刪除
	 */
	https_redirect?: boolean
	/**
	 * URL.scheme 替換爲 此值
	 * @remarks
	 * 如果源 URI.scheme 是 http 並且端口明確設置為 :80，則端口將在重定向後刪除
	 * 如果源 URI.scheme 是 https 並且端口明確設置為 :443，則端口將在重定向後刪除
	 */
	scheme_redirect?: string
	/**
	 * URL.host  替換爲 此值
	 */
	host_redirect?: string
	/**
	 * (uint32) URL.port  替換爲 此值
	 */
	port_redirect?: number
	/**
	 * URL.path 替換爲 此值
	 * @remarks
	 * 請注意 path_redirect 中的查詢字符串將覆蓋請求的查詢字符串並且不會被刪除
	 */
	path_redirect?: string
	/**
	 * 指示在重定向期間，匹配的前綴（或路徑）應與此值交換。 
	 * 此選項允許根據請求動態創建重定向 URL
	 */
	prefix_rewrite?: string
	/**
	 * 指示在重定向期間，應重寫與模式匹配的路徑部分，甚至允許將模式中的捕獲組替換為重寫替換字符串指定的新路徑。 
	 * 這對於允許以識別具有可變內容（如標識符）的段的方式重寫應用程序路徑很有用
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/regex.proto#envoy-v3-api-msg-type-matcher-v3-regexmatchandsubstitute type.matcher.v3.RegexMatchAndSubstitute}
	 */
	regex_rewrite?: type.matcher.v3.RegexMatchAndSubstitute
	/**
	 * 在重定向響應中使用的 HTTP 狀態代碼。 默認響應代碼是 MOVED_PERMANENTLY (301)
	 * - MOVED_PERMANENTLY 301
	 * - FOUND 302
	 * - SEE_OTHER 303
	 * - TEMPORARY_REDIRECT 307
	 * - PERMANENT_REDIRECT 308
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-enum-config-route-v3-redirectaction-redirectresponsecode config.route.v3.RedirectAction.RedirectResponseCode}
	 */
	response_code?: 'MOVED_PERMANENTLY' | 'FOUND' | 'SEE_OTHER' | 'TEMPORARY_REDIRECT' | 'PERMANENT_REDIRECT'
	/**
	 * 指示在重定向期間，URL 的查詢部分將被刪除
	 * @defaultValue false
	 */
	strip_query?: boolean
}
```

## direct\_response
([config.route.v3.DirectResponseAction](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#envoy-v3-api-msg-config-route-v3-directresponseaction)) 直接返回任意響應

route, redirect, direct\_response 中必須且只能設置一個屬性

```typescript
interface DirectResponseAction {
	/**
	 * 指定要返回的 HTTP 響應狀態
	 */
	status?: number
	/**
	 * 指定響應主體的內容。 如果省略此設置，則生成的響應中不包含正文
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource config.core.v3.DataSource}
	 */
	body?: config.core.v3.DataSource
}
```
