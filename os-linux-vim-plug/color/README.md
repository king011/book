# lightline

lightline 以高亮 已經更加清晰漂亮的 顯示 vim 狀態欄

```
Plug 'itchyny/lightline.vim'
```

```
" 需要打開 vim 狀態欄顯示
set laststatus=2
```

# gruvbox 
gruvbox 是一個 配色方案 提供了對 多種語言的支持

* 源碼 [https://github.com/morhetz/gruvbox](https://github.com/morhetz/gruvbox)

```
Plug 'morhetz/gruvbox'
```


```
" 設置 背景色即可 在兩種風格間切換
set background=dark    " Setting dark mode
set background=light   " Setting light mode
```