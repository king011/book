# 安裝

從 官網 下載 安裝包 安裝即可

# 添加 右鍵 菜單

編輯註冊表

```
Windows Registry Editor Version 5.00
 
[HKEY_CLASSES_ROOT\Directory\Background\shell\MSYS2 MSYS]
"Icon"="F:\\msys64\\msys2.ico"
 
[HKEY_CLASSES_ROOT\Directory\Background\shell\MSYS2 MSYS\command]
@="F:\\msys64\\msys2_shell.cmd -msys -here"
```

```
Windows Registry Editor Version 5.00
 
[HKEY_CLASSES_ROOT\Directory\Background\shell\MSYS2 MinGW 32-bit]
"Icon"="F:\\msys64\\msys2.ico"
 
[HKEY_CLASSES_ROOT\Directory\Background\shell\MSYS2 MinGW 32-bit\command]
@="F:\\msys64\\msys2_shell.cmd -mingw32 -here"
```

```
Windows Registry Editor Version 5.00
 
[HKEY_CLASSES_ROOT\Directory\Background\shell\MSYS2 MinGW 64-bit]
"Icon"="F:\\msys64\\msys2.ico"
 
[HKEY_CLASSES_ROOT\Directory\Background\shell\MSYS2 MinGW 64-bit\command]
@="F:\\msys64\\msys2_shell.cmd -mingw64 -here"
```

# pacman

MSYS2 從 archlinux 移植了 pacman 來作爲 MSYS2 的包管理工具

## 常用命令
```sh
# 設置 下載代理
export http_proxy="127.0.0.1:8118"

# 更新軟件包數據庫和核心系統軟件包
pacman -Syu

# 查找 包
pacman -Ss 包名

# 安裝 包
pacman -S 包名

# 刪除 包
pacman -R 包名

//查詢 已安裝的 包
pacman -Q 包名
pacman -Qs 包名
```