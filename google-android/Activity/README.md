# Activity

Activity 是 android 中的 核心模塊 提供了與用戶交互的界面 每個 Activity 都被分配一個 可繪製的 窗口

Activity 存在如下 狀態

* Running 一個新的 Activity 啓動後 它位於屏幕最前端 處於棧頂 此時可見 並且可和用戶交互
* Paused 當 Activity 被另外一個 透明或者 Dialog 樣式的 Activity 覆蓋時 其依然可見 但已經失去了焦點 無法和 用戶 交互
* Stopped 當 Activity 不可見時 此時如果系統內存 不夠 可能會回收 此Activity 所以通常應該在進入 Stopped 前 將 狀態 成員信息 保存 以便激活時 恢復狀態
* Killed 當 Activity 被殺死 會 沒有啓動

> 雖然 規範文檔中說 Stopped 時 Activity 可能會被回收 但 Dianne Hackborn 在 2011年 表示 Android的 實現代碼 其實是 直接回收了整個進程
> 
> 當 內存 嚴重不足時 Paused Running 狀態的 進程 也可能會被回收
> 

## 重建顯示 狀態

因爲 Activity 可能會被 回收 android 提供了 一些方法 來重間 應用 行爲

1. 在 Activity 從 Running 編爲 Paused/Stopped onSaveInstanceState 會被回調 應該在此 函數中 通過 傳入的 Bundle 保存 顯示狀態

  Bundle 以 key/value 形式 存儲數據

   ```
   static final String STATE_SCORE = "playerScore";
   static final String STATE_LEVEL = "playerLevel";
   ...
   
   @Override
   public void onSaveInstanceState(Bundle savedInstanceState) {
       // Save the user's current game state
       savedInstanceState.putInt(STATE_SCORE, mCurrentScore);
       savedInstanceState.putInt(STATE_LEVEL, mCurrentLevel);
   
       // Always call the superclass so it can save the view hierarchy state
       super.onSaveInstanceState(savedInstanceState);
   }
   ```
	
1. 在 onCreate 中 進行 恢復 onCreate 會 傳如 一個 Bundle 
 
 如果 Activity 是第一次 運行 則 onCreate 爲 null 否則 則爲 onSaveInstanceState 時 存儲的 Bundle 可以用此Bundle 恢復 Activity 的狀態

   ```
   @Override
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState); // Always call the superclass first
   
       // Check whether we're recreating a previously destroyed instance
       if (savedInstanceState != null) {
           // Restore value of members from saved state
           mCurrentScore = savedInstanceState.getInt(STATE_SCORE);
           mCurrentLevel = savedInstanceState.getInt(STATE_LEVEL);
       } else {
           // Probably initialize members with default values for a new instance
       }
       ...
   }
   ```
	 
1. 在 onRestoreInstanceState 中 恢復

  如果 不想 onCreate 代碼變得太輔助 也可以在 onRestoreInstanceState 中 進行恢復 在 onStart 被回調結束後 如果 Activity 不是第一次運行 則 回調 onRestoreInstanceState

   ```
   public void onRestoreInstanceState(Bundle savedInstanceState) {
       // Always call the superclass so it can restore the view hierarchy
       super.onRestoreInstanceState(savedInstanceState);
   
       // Restore state members from saved instance
       mCurrentScore = savedInstanceState.getInt(STATE_SCORE);
       mCurrentLevel = savedInstanceState.getInt(STATE_LEVEL);
   }
   ```
	 
* 應該 始終調用 super.onSaveInstanceState super.onRestoreInstanceState 因爲 android 會爲 有 id 的 view 自動 保存/恢復 顯示狀態 這些功能 有 超類實現

* 你不必 爲所有顯示狀態 進行 onSaveInstanceState onRestoreInstanceState 大部分 ui效果 如 文本輸入框內容 等 android 會自動 保存/恢復 但想 滾動條位置 和 自定義的 變量 才需要 自行保存

    > 只要 被指定了 唯一 id 的 view 才會被 自動 保存/恢復

* 旋轉屏幕 android 會 重建 Activity 可以藉此來 查看 onSaveInstanceState onRestoreInstanceState 是否正確的 保存/恢復 了 Activity

* 用戶點擊 返回結束 了 Activity 或 Activity 主動調用 finish 來結束 Activity 時 代表 應用結束 此時 不需要 進行 狀態恢復 android 也不會進行 onSaveInstanceState onRestoreInstanceState 這類處理
## 生命週期 與 回調

android 爲 Activity 提供了 一些 回調函數 來 監控 Activity 的生命 週期

* onCreate 在創建 Activity 時 回調 (可能是第一次 運行 也可能是 被系統回收後 重建 Activity) 通常在此初始化資源
* onStart 啓動 Activity 時 (Activity 變成顯示時)
* onRestart 重新啓動 Activity 該函數 總是在 onStart 之後執行
* onPause 當 Activity 變爲 Paused 
* onResume 當 Activity 由 Paused 變爲 Running 
* onStop 當 Activity 變爲 Stopped
* onDestroy 當 Activity 被殺死時 通常在此 釋放 資源

## 配置 Activity

所有 Activity 必須 在 AndroidManifest.xml 中 進行 配置 才可 使用 

| 屬性 | 含義 |
| -------- | -------- |
| android:name     | 指示 Activity 對應的 類名     |
| android:theme     | 指示應用主題     |
| android:label     | 顯示名稱     |
| android:icon     | 顯示的圖標     |
| android:screenOrientation     | 顯示橫豎等     |
| android:allowTaskReparenting     | 是否運行更換 從屬任務 (比如從短信任務 切換到 瀏覽器任務)     |
| android:alwaysRetainTaskState     | 當用戶離開 Task 一段時間後 系統會 清除掉 Task 中 根 Activity 外的 Activity , 如果設置 alwaysRetainTaskState 爲 true 則 不會進行 清理    |
| android:clearTaskOnLaunch     | 如果爲true 當用戶離開 Task 返回時 會清除直到 根 Activity    |
| android:configChanges     | 當 配置 list 發送修改時 是否調用 onConfigurationChanged     |
| android:excludeFromRecents     | 是否顯示在最近打開的 Activity 列表中     |
| android:exported     | 是否允許 Activity 被其它 程式調用     |
| android:launchMode     | 設置啓動模式  standard singleTop singleTask singleInstance     |
| android:finishOnTaskLaunch     | 當用戶重新啓動這個任務時 是否關閉已經打開的 Activity     |
| android:noHistory     | 當用戶切換到其它屏幕時 是否移除此 Activity     |
| android:taskAffinity     | Activity 的親屬關係 默認情況下 同個應用下的 Activity 有相同的 關係     |
| android:process     | 進程名     |
| android:windowSoftInputMode     | 軟鍵盤彈出模式     |

# 啓動 結束 Activity
* 調用 startActivity 啓動一個 Activity

   ```
   public void onClick(View view){
           // 創建一個 Intent 對象
           Intent intent = new Intent(MainActivity.this,LoveActivity.class);
           // 可選 設置 傳入 數據
           {
                   Bundle bundle = new Bundle();
                   bundle.putString("love","anita");
                   intent.putExtras(bundle);
           }
           // 啓動 Activity
           startActivity(intent);
   }
   ```
	 
   ```
   protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_love);
   
       // 可選 獲取 傳入 信息
       Intent intent = getIntent();
       Bundle bundle = intent.getExtras();
       Log.i(TAG,bundle.getString("love"));
   }
   ```

* 使用 startActivityForResult 啓動 Activity 在 Activity 結束時 onActivityResult 會被回調

   ```
	 #info="調用者"
   public void onClick(View view){
       // 創建一個 Intent 對象
       Intent intent = new Intent(MainActivity.this,LoveActivity.class);
       // 啓動 Activity 並且傳入 請求常量
       startActivityForResult(intent,CODE);
   }
   
   @Override
   protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
       super.onActivityResult(requestCode, resultCode, data);
   
       // 依據 requestCode 請求常量 判斷時哪個 Activity 返回
       switch (requestCode){
           case CODE:
               if(data == null) {
                   // 用戶 點擊 返回 時 通常 data 爲空 resultCode 爲 0
                   Log.i(TAG,"resultCode = "+Integer.toString(resultCode));
               }else{
                   Bundle bundle = data.getExtras();
                   String str = bundle.getString("love");
                   Log.i(TAG,"resultCode = "+Integer.toString(resultCode) + " msg = "+str);
               }
               break;
           default:
               Log.e(TAG,"unknow requestCode " + Integer.toString(requestCode));
               break;
       }
   }
   ```

   ```
	 #info="被調者"
   public void onClick(View view){
       // 設置 返回值
       // setResult(1);
       Intent intent = new Intent();
       Bundle bundle = new Bundle();
       bundle.putString("love","anita");
       intent.putExtras(bundle);
       setResult(1,intent);
       // 結束 Activity 返回 到調用者
       finish();
   }
   ```

* 在 Activity 中 調用 finish() 結束自己

# Fragment 
* Fragment 用來 將 Activity 進行 模塊化/重用 
* Fragment 必須綁定一個 Activity 並且和其生命週期關聯
* Fragment 可以 靜態/動態 添加/移除 
* Fragment 必須實現 onCreateView 來返回一個 View 或 null

![](assets/Fragment.png)

## 靜態 使用 Fragment

1. 從 Fragment 派生子類 實現 onCreateView 返回 view

   ```
	<?xml version="1.0" encoding="utf-8"?>
	<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
			xmlns:tools="http://schemas.android.com/tools"
			android:layout_width="match_parent"
			android:layout_height="match_parent"
			tools:context=".ViewFragment">
			<LinearLayout
					android:gravity="center"
					android:background="@color/colorAccent"
					android:orientation="vertical"
					android:layout_width="match_parent"
					android:layout_height="match_parent">
					<EditText
							android:id="@+id/editText"
							android:layout_width="match_parent"
							android:layout_height="wrap_content"
							android:hint="input any text"/>
					<Button
							android:id="@+id/buttonSet"
							android:layout_width="wrap_content"
							android:layout_height="wrap_content"
							android:text="set"/>
			</LinearLayout>
	</FrameLayout>
   ```

   ```
   package com.king011.example;
   
   import android.os.Bundle;
   import android.support.v4.app.Fragment;
   import android.view.LayoutInflater;
   import android.view.View;
   import android.view.ViewGroup;
   import android.widget.Button;
   import android.widget.EditText;
   
   public class ViewFragment extends Fragment {
       private static final String TAG="ViewFragment";
       private int n = 0;
       @Override
       public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
           // Inflate the layout for this fragment
           View view = inflater.inflate(R.layout.fragment_view, container, false);
           // 設置 事件
           Button btn = view.findViewById(R.id.buttonSet);
           btn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   EditText editText = getActivity().findViewById(R.id.editText);
                   n++;
                   editText.setText(Integer.toString(n));
               }
           });
           // 恢復狀態
           if(savedInstanceState != null){
               n = savedInstanceState.getInt("n");
           }
           return view;
       }
       @Override
       public void onSaveInstanceState(Bundle savedInstanceState) {
           savedInstanceState.putInt("n",n);
           super.onSaveInstanceState(savedInstanceState);
       }
   }
   ```
	
1. 在主佈局中 使用 fragment 標籤 引入 Fragment

   ```
	<?xml version="1.0" encoding="utf-8"?>
	<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
			xmlns:app="http://schemas.android.com/apk/res-auto"
			xmlns:tools="http://schemas.android.com/tools"
			android:layout_width="match_parent"
			android:layout_height="match_parent"
			tools:context=".MainActivity">
			<LinearLayout
					android:orientation="vertical"
					android:gravity="center"
					android:layout_width="match_parent"
					android:layout_height="match_parent">
					<fragment
							android:id="@+id/fragment"
							android:name="com.king011.example.ViewFragment"
							android:layout_width="match_parent"
							android:layout_height="wrap_content"
							android:layout_weight="1" />
			</LinearLayout>
	</android.support.constraint.ConstraintLayout>
   ```

> Fragment 只能在 onCreateView 中 恢復狀態 沒有提供 onRestoreInstanceState 回調
> 
> Fragment 中的 Bundle savedInstanceState 和 Activity 中的 是 獨立分開的

## 動態 使用 Fragment

* 在 Activity 中 getSupportFragmentManager() 函數 會返回 一個 FragmentManager 用來 管理 Fragment
* 提供了 FragmentManager 提供了 beginTransaction 返回一個 FragmentTransaction
* FragmentTransaction 提供了 add replace remove 函數 用來 動態 修改 Fragment

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:orientation="vertical"
        android:gravity="center"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <FrameLayout android:id="@+id/frameLayout"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_weight="1" />


        <Button android:onClick="onClick"
            android:text="Load"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content" />
    </LinearLayout>

</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onClick(View view){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        {
            ViewFragment viewFragment = new ViewFragment();
            fragmentTransaction.replace(R.id.frameLayout,viewFragment);
        }
        fragmentTransaction.commit();
    }
}
```

## 回退棧 addToBackStack

FragmentTransaction 提供了addToBackStack(String name) 函數 將 當前 Fragment 壓入 回退棧 當 用戶 點擊 返回時 會彈出 當前 Fragment 如果 棧爲空 則退出 Activity

> name 是可選的名稱 通常 傳入 null 即可

## Fragment Activity 互相訪問

* Fragment 提供了 getActivity 函數來返回 管理的 Activity
* FragmentManager 提供了 findFragmentById 來返回 Fragment

