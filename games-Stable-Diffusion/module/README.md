# 模型

不同的模型確定了畫出圖像的風格例如 chilloutmix 是一個畫真人AI美女的大模型

你可以從 [https://civitai.com/](https://civitai.com/) 尋找並下載自己喜歡的模型

# 識別模型

不同的模型需要放置到不同的位置，你可以將模型拖動到 [https://spell.novelai.dev/](https://spell.novelai.dev/) 網頁上，會分析並打印模型信息以及應該放入的位置