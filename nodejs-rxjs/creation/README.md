# 創建 Observable

除了 構造函數 外 rxjs 提供了 許多 輔助函數 來創建 Observable

* of
* from
* fromEvent
* fromPromise
* never
* empty
* throw
* interval
* timer

# of

of 創建一個 Observable 並且 將 參數 依次 push 進去

```
import { of } from 'rxjs'

of("kate", "anita").subscribe({
    next(name) {
        console.log(name)
    },
    complete() {
        console.log("complete")
    }
})
```

# from

from 可以從 list 任意可列舉的 比如 Array Set Iterator 創建 Observable

from 還支持由 Promise 創建 Observable

```
import { from } from 'rxjs'

// 從 數組創建 Observable
from(["kate", "anita"]).subscribe({
    next(name) {
        console.log(name)
    },
    complete() {
        console.log("complete")
    }
})

// 從字符串(相當於字符數組) 創建 Observable 
from("cerberus is an idea").subscribe({
    next(c) {
        console.log(c)
    },
    complete() {
        console.log("complete")
    }
})

// 由 Promise 創建
from(new Promise<string>((resolve, reject) => {
    resolve("anita")
})).subscribe({
    next(name) {
        console.log(name)
    },
    complete() {
        console.log("complete")
    }
})
```

# fromEvent
fromEvent 需要 傳入一個 DOM 物件 和 事件名 由 DOM 事件 創建 Observable

```
import { fromEvent } from 'rxjs'

fromEvent(document.body, 'click').subscribe({
    next(evt) {
        console.log(evt)
    },
    error(e) {
        console.log(e)
    },
    complete() {
        console.log("complete")
    },
})
```

# fromEventPattern
fromEventPattern 提供了由 類事件(既提供了 addEventListener removeEventListener 接口的事件系統) 創建 Observable

```
import { fromEventPattern } from 'rxjs'
import { isFunction } from 'util'

// 創建一個事件系統
class Producer {
    private listeners_ = new Array<(msg: string) => void>()
    constructor() {
    }
    addListener(listener: (msg: string) => void) {
        if (isFunction(listener)) {
            this.listeners_.push(listener)
        } else {
            throw new Error('listener 必須是 function')
        }
    }
    removeListener(listener: (msg: string) => void) {
        this.listeners_.splice(this.listeners_.indexOf(listener), 1)
    }
    notify(message: string) {
        this.listeners_.forEach(listener => {
            listener(message)
        })
    }
}
const producer = new Producer()

// 由 類事件 創建 Observable
fromEventPattern(
    (handler) => {
        producer.addListener(handler)
    },
    (handler) => {
        producer.removeListener(handler)
    },
).subscribe({
    next: (evt) => {
        console.log(evt)
    },
    complete() {
        console.log("complete")
    },
})

// 發生事件
producer.notify("anita")
```

# empty

empty 相當於數學的0 創建一個 空的 Observable 觀察者會立刻得到 complete

```
import { empty } from 'rxjs'
empty().subscribe({
    complete() {
        console.log("complete")
    },
})
```

# never
never 相當於 數學的 無窮 訂閱 never 不會發送 任何事 是一個 永遠聲明事都不做的 Observable

```
import { never } from 'rxjs'
never().subscribe({
    next() {
        console.log("never call next")
    },
    error() {
        console.log("never call error")
    },
    complete() {
        console.log("never call complete")
    },
})
```

# throwError

throwError 創建一個 立刻 error 的 Observable

```
import { throwError } from 'rxjs'
throwError("err test").subscribe({
    error(e) {
        console.log("error :", e)
    },
})
```

# interval
interval 創建一個 Observable 每隔固定時間 push一個 自增的整數(從0開始計數)

```
import { interval } from 'rxjs'

// 每秒自增
interval(1000).subscribe(
    (v) => {
        console.log(v)
    },
)
```

# timer
timer 相當於 interval 的語法糖 timer 可以在等待 一段事件後 使用 interval 創建一個 Observable

timer 第一個參數 可以是要等待的時間長度/一個指定的 Date/undefined

傳入 undefined 相當於 直接調用 interval

```
import { timer } from 'rxjs'

// 等待五秒後 創建 interval Observable
timer(5000, 1000).subscribe(
    (v) => {
        console.log(v)
    },
)
```