# [flutter linux](https://docs.flutter.dev/get-started/install/linux)

flutter 已經正式支持了 桌面平臺 linux 也作爲了第一支持的平臺 此外 windows 和 mac 等也被支持 此處只是一提

linux 需要一些額外的工具用於編譯原生代碼

```
sudo apt-get install clang cmake ninja-build pkg-config libgtk-3-dev
```

此外還需要執行下述命令啓用對桌面平臺的支持
```
flutter config --enable-linux-desktop
```

```
# 爲當前項目添加對於桌面平臺的代碼
flutter create --platforms=windows,macos,linux .

# 打印當前項目支持的平臺
flutter devices

# 以 linux 平臺運行
flutter run -d linux

# 發佈 linux 平臺應用
flutter build linux --release
```
