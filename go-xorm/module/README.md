# 模型

xorm 支持將一個 struct 作爲模型 映射爲數據庫中的一張表 通過模型直接操作表

# 名稱映射規則

根名稱相關的函數包含在 `xorm.io/xorm/names` 中，interface Mapper 定義了如何映射名稱

```
// Mapper represents a name convertation between struct's fields name and table's column name
type Mapper interface {
	Obj2Table(string) string
	Table2Obj(string) string
}
```

xorm 提供了 三種內置 Mapper

* SnakeMapper 默認規則 將駝峯命名轉爲數據庫習慣命名 全小寫 多個關鍵字以 _ 連接
* SameMapper 以 go 中保持一致的名稱
* GonicMapper 類似 SnakeMapper 但 對go 中特殊單詞 比如 ID 會映射爲 id 而非 i_d

使用 SetMapper 設置映射規則
```
engine.SetMapper(names.GonicMapper{})
```

此外也可以爲 表名 和 字段名 指定不同的 規則

```
engine.SetTableMapper(names.SameMapper{})
engine.SetColumnMapper(names.SnakeMapper{})
```

# 前綴 後綴 緩存

xorm 提供了三中 裝飾 Mapper 來裝飾原始 Mapper 提供額外功能

* `func NewPrefixMapper(mapper Mapper, prefix string) PrefixMapper` 創建一個新 Mapper 爲 所有映射名稱添加一個前綴
* `func NewSuffixMapper(mapper Mapper, suffix string) SuffixMapper` 創建一個新 Mapper 爲 所有映射名稱添加一個後綴
* `func NewCacheMapper(mapper Mapper) *CacheMapper` 爲名稱映射添加一個內存緩存 已使用過的名稱直接從緩存獲取映射名

# 使用 Table 和 Tag 改變名稱映射

xorm 提供了幾種方法來改變映射名稱

* 如果 struct 擁有 TableName() string 成員方法 將使用此方法的返回值作爲數據庫表名
* engine.Table() 改變 struct 對應的數據庫表名
* 在tag中使用 `xorm:"'column_name'"` 來指定字段名

表名優先級:

1. engine.Table()
2. TableName() string
3. Mapper

字段名優先級:

1. tag
2. Mapper

# Column 字段定義

在 field 對應的 tag 中對 Column 可以進行一些定義

```
type User struct {
    ID   int64 `xorm:"pk autoincr 'id'"`
    Name string  `xorm:"varchar(25) notnull unique 'usr_name' comment('姓名')"`
}
```

對應不同的數據庫 數據型別存在差異。xorm 對數據型別有自己的定義，基本原則是儘量兼容各種數據庫字段類型

tag的關鍵字不區分大小寫 但字段名根據不同數據庫是區分大小寫的



| tag | 定義 |
| -------- | -------- |
| 'column_name'     | 可選，默認使用  Mapper生成，如果與其它關鍵字衝突需要使用**'** 擴起來     |
| pk     | 設置主鍵 如果度昂字段使用pk 則構成複合主鍵 (當前支持go型別 int32 int int64 uint32 uint uint64 string)     |
| type_name     | 可選，默認自動識別，字段在數據庫中的型別     |
| autoincr     | 設置自增加     |
| [not] null 或 notnull     | 是否可以爲空     |
| unique 或 unique(uniquename)     | 是否唯一 如果指定了 uniquename 多個字段擁有相同的 uniquename 將組成聯合唯一索引     |
| index 或 index(indexname)     | 是否所有 如果指定了 indexname 多個字段擁有相同的 indexname 將組成聯合索引     |
| extends     | 引用與一個匿名成員結構或非匿名成員結構體之上，標識此結構體的所有成員也，extends可無效嵌套     |
| -     | 忽略此字段     |
| ->     | 此字段只寫入數據庫而不從數據庫中讀取     |
| <-     | 此字段只從數據庫讀取而不寫入     |
| created     | insert 時自動賦值爲當前時間     |
| updated     | insert 或 update 時自動賦值爲當前時間     |
| deleted     | delete時設置爲當前時間，並且不實際刪除記錄(軟刪除)     |
| version     | insert 時默認爲1 每次update自增1(樂觀鎖)     |
| default value 或 default(value)     | 設置默認值 字符串需要加上 **'**     |
| json     | 先將內容轉爲json然後存儲到數據庫，數據庫中型別可以爲 text 或 二進制     |
| comment     | 設置字段註釋 (當前僅支持 mysql)     |

額外工作

1. 如果 field 名稱爲 ID 且 型別爲 int64 而且沒有定義 tag 將被視爲主鍵
2. string 默認映射爲 varchar(255)
3. 支持 type MyString string 等自定義 field 支持 slice map 等，這些成員默認存儲爲Text 並且默認使用 json 序列化。也支持 Blob 如果是 Blob 先 json 編碼 在轉爲 []byte。如果是 []byte 或 []uint8 則不做 json 轉碼 直接以二進制存儲
4. 實現了 Conversion 接口的 將調用此接口 轉碼，這個接口優先級是最高的

	```
	type Conversion interface {
			FromDB([]byte) error
			ToDB() ([]byte, error)
	}
	```

1. 如果 struct 包含一個 Conversion 接口，那麼在獲取數據時，必須要預先設置一個實現此接口的 struct 或 struct 指針。此時可以在 struct 中實現 BeforeSet(name string, cell xorm.Cell) 來設置 。

	```
	package main

	import (
		"encoding/json"
		"fmt"
		_ "github.com/mattn/go-sqlite3"
		"xorm.io/xorm"
	)

	type User struct {
		ID     int64   `xorm:"pk autoincr 'id'"`
		Name   string  `xorm:"unique notnull"`
		Label  Label   `xorm:"extends"` // extends 將 Label 的字段直接嵌入
		Health *Health // Text
		Point  Point   // Text
	}

	func (u *User) BeforeSet(name string, cell xorm.Cell) {
		if name == `health` {
			// 指針類型 在 get 前需要 主動創建
			u.Health = new(Health)
		}
	}

	type Label struct {
		Key   string
		Value string
	}
	type Health struct {
		HP int32
		MP int32
	}

	func (h *Health) FromDB(b []byte) error {
		return json.Unmarshal(b, &h)
	}
	func (h *Health) ToDB() ([]byte, error) {
		return json.Marshal(h)
	}

	type Point struct {
		X int32
		Y int32
	}

	func (p *Point) FromDB(b []byte) error {
		return json.Unmarshal(b, &p)
	}
	func (p *Point) ToDB() ([]byte, error) {
		return json.Marshal(p)
	}

	type Company struct {
		Name string
		URL  string `xorm:"'rul'"`
	}

	func checkEror(e error) {
		if e != nil {
			panic(e)
		}
	}
	func main() {
		engine, e := xorm.NewEngine(`sqlite3`, `my.db`)
		checkEror(e)
		engine.ShowSQL(true)
		e = engine.CreateTables(&User{})
		checkEror(e)
		_, e = engine.Insert(&User{Name: `kate`,
			Point:  Point{X: 101, Y: 102},
			Health: &Health{HP: 10, MP: 20},
		}, &User{Name: `anita`})
		checkEror(e)
		var users []User
		e = engine.Find(&users)
		checkEror(e)
		fmt.Println(users[0].Health)
		fmt.Println(users)
	}
	```
	
# xorm 數據庫 型別 對應表


| xorm | mysql | sqlite3 | postgres | remark |
| -------- | -------- | -------- | -------- | -------- |
| BIT     | BIT     | INTEGER     | BIT     |      |
| TINYINT     | TINYINT     | INTEGER     | SMALLINT     |      |
| SMALLINT     | SMALLINT     | INTEGER     | SMALLINT     |      |
| MEDIUMINT     | MEDIUMINT     | INTEGER     | INTEGER     |      |
| INT     | INT     | INTEGER     | INTEGER     |      |
| INTEGER     | INTEGER     | INTEGER     | INTEGER     |      |
| BIGINT     | BIGINT     | INTEGER     | BIGINT     |      |
| CHAR     | CHAR     | TEXT     | CHAR     |      |
| VARCHAR     | VARCHAR     | TEXT     | VARCHAR     |      |
| TINYTEXT     | TINYTEXT     | TEXT     | TEXT     |      |
| TEXT     | TEXT     | TEXT     | TEXT     |      |
| MEDIUMTEXT     | MEDIUMTEXT     | TEXT     | TEXT     |      |
| LONGTEXT     | LONGTEXT     | TEXT     | TEXT     |      |
| BINARY     | BINARY     | BLOB     | BYTEA     |      |
| VARBINARY     | VARBINARY     | BLOB     | BYTEA     |      |
| DATE     | DATE     | NUMERIC     | DATE     |      |
| DATETIME     | DATETIME     | NUMERIC     | TIMESTAMP     |      |
| TIME     | TIME     | NUMERIC     | TIME     |      |
| TIMESTAMP     | TIMESTAMP     | NUMERIC     | TIMESTAMP     |      |
| TIMESTAMPZ     | TEXT     | TEXT     | TIMESTAMP with zone     | timestamp with zone info     |
| REAL     | REAL     | REAL     | REAL     |      |
| FLOAT     | FLOAT     | REAL     | REAL     |      |
| DOUBLE     | DOUBLE     | REAL     | DOUBLE PRECISION     |      |
| DECIMAL     | DECIMAL     | NUMERIC     | DECIMAL     |      |
| NUMERIC     | NUMERIC     | NUMERIC     | NUMERIC     |      |
| TINYBLOB     | TINYBLOB     | BLOB     | BYTEA     |      |
| BLOB     | BLOB     | BLOB     | BYTEA     |      |
| MEDIUMBLOB     | MEDIUMBLOB     | BLOB     | BYTEA     |      |
| LONGBLOB     | LONGBLOB     | BLOB     | BYTEA     |      |
| BYTEA     | BLOB     | BLOB     | BYTEA     |      |
| BOOL     | TINYINT     | INTEGER     | BOOLEAN     |      |
| SERIAL     | INT     | INTEGER     | SERIAL     | auto increment     |
| BIGSERIAL     | BIGINT     | INTEGER     | BIGSERIAL     | auto increment     |

# go xorm 型別 對應表


| go type's kind | value method | xorm type |
| -------- | -------- | -------- |
| implemented Conversion     | Conversion.ToDB / Conversion.FromDB     | TEXT     |
| int, int8, int16, int32, uint, uint8, uint16, uint32     |      | INT     |
| int64, uint64     |      | BIGINT     |
| float32     |      | FLOAT     |
| float64     |      | DOUBLE     |
| complex64, complex128     | json.Marshal / json.UnMarshal     | VARCHAR(64)     |
| []uint8     |      | BLOB     |
| array, slice, map except []uint8     | json.Marshal / json.UnMarshal     | TEXT     |
| bool     | 1 or 0     | BOOL     |
| string     |      | VARCHAR(255)     |
| time.Time     |      | DATETIME     |
| cascade struct     | primary key field value     | BIGINT     |
| struct     | json.Marshal / json.UnMarshal     | TEXT     |
| Others     |      | TEXT     |

