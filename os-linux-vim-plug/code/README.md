# neoformat

neoformat 是一個 代碼格式化插件 

* 源碼 [https://github.com/sbdchd/neoformat](https://github.com/sbdchd/neoformat)

指令 :Neoformat 格式化代碼

```
" 保存時 自動 格式化
augroup fmt
  autocmd!
  autocmd BufWritePre * undojoin | Neoformat
augroup END
```

# coc

coc 是一個 LS 語法提示 插件

* 源碼 [https://github.com/neoclide/coc.nvim](https://github.com/neoclide/coc.nvim)

```
Plug 'neoclide/coc.nvim', {'branch': 'release'}
```

* :CocInfo 查詢coc版本信息
* :CocConfig coco 配置檔案

## golang

執行 `:CocInstall coc-json coc-tsserver` 安裝 coc 擴展
執行 `:CocConfig` 寫入如下配置 

```
{
  "languageserver": {
    "go": {
      "command": "gopls",
      "rootPatterns": ["go.mod"],
      "trace.server": "verbose",
      "filetypes": ["go"]
    }
  }
}
```


# vim-go

vim-go 提供了 golang的 vim 開放環境

* 源碼 [https://github.com/fatih/vim-go](https://github.com/fatih/vim-go)

```
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
```
