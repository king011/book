# dart:convert

```dart
#info=false
import 'dart:convert';
```

dart:convert 提供了 格式轉換 的工具

# json

jsonEncode/jsonDecode 提供了 將 json字符串 和 dart 數據結構間轉換的 功能

```dart
import 'dart:convert';

const jsonStr = """
[
  {
    "name":"google",
    "domain":"google.com:443"
  },
  {
    "name":"gitlab",
    "domain":"gitlab.com:443"
  }
]
""";

class Item {
  String name;
  String domain;
  Item.fromJson(Map<String, dynamic> json)
      : this.name = json["name"],
        this.domain = json["domain"];

  Map<String, dynamic> toJson() => {
        "name": name,
        "domain": domain,
      };
}

class ServerList {
  List<Item> list;
  ServerList.fromJson(List<dynamic> json) {
    if (json == null || json.length == 0) {
      return;
    }
    list = List<Item>();
    for (var i = 0; i < json.length; i++) {
      list.add(Item.fromJson(json[i]));
    }
  }
  List<dynamic> toJson() => list;
}

main() {
  // 解析 json
  ServerList serverList;
  try {
    List<dynamic> json = jsonDecode(jsonStr);
    serverList = ServerList.fromJson(json);
    for (var item in serverList.list) {
      print("name = ${item.name}\ndomain = ${item.domain}");
    }
  } on FormatException catch (e) {
    // 不是合法的 jso字符串
    print("foramt error : $e");
  } on TypeError catch (e) {
    // json 和返回的 dart 內存結構 不兼容
    print("type error : $e");
  }

  // 生成 json
  var str = jsonEncode(serverList);
  print(str);
  str = JsonEncoder.withIndent(" ").convert(serverList);
  print(str);
}
```