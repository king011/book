# cloudflare

Cloudflare 是美國一家總部位於舊金山的跨國科技公司，以客戶提供基於反向代理的內容傳遞網路(Content Delivery Network, CDN) 及分佈式域名解析服務(Distributed Domain Name Server)

* 官網 [https://cloudflare.com/](https://cloudflare.com/)