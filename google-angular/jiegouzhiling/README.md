# ngIf

結構指令 主要 負責 重新 佈局 刪除 添加 DOM 比如 ngIf 在條件爲true時 添加DOM 爲false將DOM刪除

**\*** 放在 結構指令 之前 \* 是Angular 提供的 語法糖 以ngIf 爲例 下面兩種 寫法 是同個意思

```html
<div *ngIf="hero" class="name">{{hero.name}}</div>
```

```html
<ng-template [ngIf]="hero">
  <div class="name">{{hero.name}}</div>
</ng-template>
```

# ngFor

ngFor 通常 用來 對數組 執行 重複操作

```html
<div *ngFor="let hero of heroes; let i=index; let odd=odd; trackBy: trackById" [class.odd]="odd">
  ({{i}}) {{hero.name}}
</div>
 
<ng-template ngFor let-hero [ngForOf]="heroes" let-i="index" let-odd="odd" [ngForTrackBy]="trackById">
  <div [class.odd]="odd">({{i}}) {{hero.name}}</div>
</ng-template>
```

```ts
import { Component } from '@angular/core';
 
import { Hero, heroes } from './hero';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent {
  heroes = heroes;
  hero = this.heroes[0];
 
  condition = false;
  logs: string[] = [];
  showSad = true;
  status = 'ready';
 
  trackById(index: number, hero: Hero): number { return hero.id; }
}
```

# ngSwitch
ngSwitch 從多個分支中 渲染一個

```html
<div [ngSwitch]="hero?.emotion">
  <ng-template [ngSwitchCase]="'happy'">
    <app-happy-hero [hero]="hero"></app-happy-hero>
  </ng-template>
  <ng-template [ngSwitchCase]="'sad'">
    <app-sad-hero [hero]="hero"></app-sad-hero>
  </ng-template>
  <ng-template [ngSwitchCase]="'confused'">
    <app-confused-hero [hero]="hero"></app-confused-hero>
  </ng-template >
  <ng-template ngSwitchDefault>
    <app-unknown-hero [hero]="hero"></app-unknown-hero>
  </ng-template>
</div>
```

# ng-template
ng-template 通常被用在 結構指令中 如果 不存在 結構指令  
Angular 會 把其內容 轉爲 註釋

# ng-container

ng-container 也是一個 不會被添加 到 DOM中的 標籤  
通常 用在 ngFor 的 兄弟元素 之間

```html
<div>
  Pick your favorite hero
  (<label><input type="checkbox" checked (change)="showSad = !showSad">show sad</label>)
</div>
<select [(ngModel)]="hero">
  <ng-container *ngFor="let h of heroes">
    <ng-container *ngIf="showSad || h.emotion !== 'sad'">
      <option [ngValue]="h">{{h.name}} ({{h.emotion}})</option>
    </ng-container>
  </ng-container>
</select>
```

# [ngTemplateOutlet](https://angular.io/api/common/NgTemplateOutlet)

ngTemplateOutlet 用於將一段準備好的模板嵌入到當前位置,通常在嵌入層數太多時可以使用 ngTemplateOutlet 來使代碼變得清晰,或者是需要在頁面顯示重複內容時(例如在數據列表的上端和下端都要顯示分頁)

```
@Component({
  selector: 'ng-template-outlet-example',
  template: `
    <ng-container *ngTemplateOutlet="greet"></ng-container>
    <hr>
    <ng-container *ngTemplateOutlet="eng; context: myContext"></ng-container>
    <hr>
    <ng-container *ngTemplateOutlet="svk; context: myContext"></ng-container>
    <hr>

    <ng-template #greet><span>Hello</span></ng-template>
    <ng-template #eng let-name><span>Hello {{name}}!</span></ng-template>
    <ng-template #svk let-person="localSk"><span>Ahoj {{person}}!</span></ng-template>
`
})
export class NgTemplateOutletExample {
  myContext = {$implicit: 'World', localSk: 'Svet'};
}
```

# 自定義結構指令

1. 定義一個指令
2. 注入 TemplateRef 可取得 ng-template的內容 注入 ViewContainerRef 可取得 視圖容器

下面的例子 是一個 和 ngIf 執行相反工作的 結構指令
```ts
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/**
 * Add the template content to the DOM unless the condition is true.
 */
@Directive({ selector: '[appUnless]'})
export class UnlessDirective {
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  @Input() set appUnless(condition: boolean) {
    if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
```