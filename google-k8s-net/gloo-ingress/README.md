# [gloo ingress](https://docs.solo.io/gloo-edge/latest/guides/integrations/ingress/)

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: http-ingress
    annotations:
      # note: this annotation is only required if you've set 
      # REQUIRE_INGRESS_CLASS=true in the environment for 
      # the ingress deployment
      kubernetes.io/ingress.class: gloo
spec:
    rules:
        - http:
              paths:
                  - pathType: Prefix
                    path: /version
                    backend:
                        service:
                            name: apps-meta
                            port:
                                number: 80
```

默認情況下 gloo 會忽略 kubernetes.io/ingress.class 並爲所有 ingress 都啓用路由規則，如果像改變這一行爲需要爲 ingress 指定環境變量 **REQUIRE\_INGRESS\_CLASS=true**

```yaml
#info="kubectl edit deployments.apps -n gloo-system ingress"
apiVersion: apps/v1
kind: Deployment
spec:
  template:
   spec:
      containers:
      - env:
        - name: REQUIRE_INGRESS_CLASS
          value: "true"
```

如果設置了 **REQUIRE\_INGRESS\_CLASS=true** 則 gloo 只會爲添加了註解 **kubernetes.io/ingress.class: gloo** 的 ingress 設置路由，此外你也可以爲 ingress 指定環境變量 **CUSTOM\_INGRESS\_CLASS=VALUE** 來指定一個 gloo 要匹配的 ingress.class 而非默認名稱 gloo

```yaml
#info="kubectl edit deployments.apps -n gloo-system ingress"
apiVersion: apps/v1
kind: Deployment
spec:
  template:
   spec:
      containers:
      - env:
        - name: REQUIRE_INGRESS_CLASS
          value: "true"
        - name: CUSTOM_INGRESS_CLASS
          value: YOUR_INGRESS_CLASS_NAME
```