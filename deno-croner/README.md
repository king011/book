# croner

croner 是一個開源(MIT)的 cron 庫，沒有依賴，可以在多種環境中運行 node deno bun browser

* 源碼 [https://github.com/Hexagon/croner](https://github.com/Hexagon/croner)
* 文檔 [https://hexagon.github.io/croner/Cron.html](https://hexagon.github.io/croner/Cron.html)