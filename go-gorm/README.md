# gorm

gorm 是一個開源(MIT) 的 golang 關係型數據庫 orm

* 官網 [https://gorm.io](https://gorm.io)
* 源碼 [https://github.com/go-gorm/gorm](https://github.com/go-gorm/gorm)
