# example

```
package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/RussellLuo/timingwheel"
)

func main() {
	var wait sync.WaitGroup
	wheel := timingwheel.NewTimingWheel(time.Second, 60)
	wheel.Start()
	wait.Add(2)
	t := wheel.AfterFunc(time.Second*2, func() {
		fmt.Println("2")
	})
	wheel.AfterFunc(time.Second+1, func() {
		if t.Stop() {
			fmt.Println("stop 2 ok")
		} else {
			fmt.Println("nerver")
			// 如果 返回 false 計時器已經到期或已經停止
			//
			// 如果 計時器已經到期 並且 go t.task() 已經執行 Stop 不會 等待 t.task() 結束
			// 如果需要 知道 t.task() 合適結束 需要自行 同步
		}

		fmt.Println("1")
		wait.Done()
	})
	wheel.AfterFunc(time.Second+time.Second/2, func() {
		fmt.Println("ok")
		wait.Done()
	})
	wait.Wait()
}
```