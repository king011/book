# 錯誤處理

lua 提供了兩個函數用於引發運行錯誤

| 簽名 | 描述 |
| -------- | -------- |
| function error(message: any, level?: integer)     | error 永遠不會返回用於結束當前函數並引發一個運行錯誤，message 是錯誤信息，level 指示如何添加額外錯誤信息 1(默認行爲 添加 檔案名+行號) 0(不要額外添加錯誤信息)     |
| function assert(v, message)     | assert 斷言 v 爲真，如果爲真返回 v，否則調用 error(message)      |

# pcall
pcall 函數接收一個函數以及傳遞給函數的參數，在保護模式下調用函數，如果函數沒有錯誤返回 true + 函數返回值，如果函數拋出運行錯誤 返回 false + 經處理過的錯誤

# xpcall
xpcall 類似 pcall 不過多接受一個處理處理函數用於自定義如何 封裝錯誤信息

# tray catch
利用pcall error 我們就可以實現類似 tray catch 的功能

```
#info="errors.lua"

---@class Catch
--- 定義一個元表用於識別出 Catch 代碼塊
local Catch={}
---@class Finally
--- 定義一個元表用於識別出 Finally 代碼塊
local Finally={}
local function createBlock(f,mt)
    local block={
        f=f,
    }
    setmetatable(block,mt)
    return block
end
local function getBlock(v)
    local mt=getmetatable(v)
    if mt==Catch then
        return v,Catch
    elseif mt==Finally then
        return v,Finally
    end
end
local function getBlocks(v0,v1)
    local catch,finally
    local v,mt=getBlock(v0)
    if v then
        if mt==Catch then
            catch=v
        else
            finally=v
        end
    end
    v,mt=getBlock(v1)
    if v then
        if mt==Catch then
            if catch then
                error('catch block duplicate ',0)
            end
            catch=v
        else
            if finally then
                error('finally block duplicate ',0)
            end
            finally=v
        end
    end
    return catch,finally
end
---以 try 模式運行函數 block
---@overload fun(block:  async fun())
---@overload fun(block:  async fun(),catch:Catch)
---@overload fun(block:  async fun(),finally:Finally)
---@overload fun(block:  async fun(),catch:Catch,finally:Finally)
---@param block async fun()
---@param catchBlock?   Catch | Finally
---@param finallyBlock? Catch | Finally
local function tryCatchFinally(block,catchBlock,finallyBlock)
    -- 獲取各個代碼塊
    if type(block) ~= "function" then
        error('try block must be a function',0)
    end
    local catch,finally=getBlocks(catchBlock,finallyBlock)
    -- 調用 try 塊
    local ok,err=pcall(block)
    -- 調用 catch 塊
    if not ok then
        if catch then
            ok,err=pcall(catch.f,err)
        end
    end

    -- 調用 finally 塊
    if finally then
        local finallyOk,finallyErr=pcall(finally.f)
        if not finallyOk then
            error(finallyErr)
        end
    end

    -- 有未處理異常 向外拋出
    if not ok then
        error(err,0)
    end
end
return {
    --- 創建一個 catch 代碼塊
    ---@return Catch
    ---@param f async fun()
    catch=function (f)
        return createBlock(f,Catch)
    end,
    --- 創建一個 finally 代碼塊
    ---@param f async fun()
    ---@return Finally
    finally=function (f)
        return createBlock(f,Finally)
    end,
    try=tryCatchFinally
}
```
# example

有了上述 errors 模塊使用 try-catch-finally 就會很簡單

```
local errors=require('errors')

errors.try(
    function ()
        errors.try (
            function ()
                print("try")
                error(1,0)
            end,
            errors.catch(function (e)
                print('catch',type(e),e)
                error(2,0)
            end),
            errors.finally(function ()
                print('finally')
            end)
        )
    end,
    errors.catch(function (e)
        print('catch',type(e),e)
    end)
)
```
