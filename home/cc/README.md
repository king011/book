
# 開發工具
* [conan](cc-conan/0) 開源包管理器 
* [valgrind](cc-valgrind/0) 內存泄漏

# c++
* 標準庫
    * [c++98](cc-98/0) 1998年發佈的 第一個標準
    * c++03 2003年發佈的 第二個標準
    * [c++11](cc-11/0) 2011年發佈的 第三個標準
    * c++14 2014年發佈的 第四個標準
    * c++17 2017年發佈的 第五個標準 
    * [boost](cc-boost/0) 
* 常用庫
    * [CLI11](cc-cli11/0) 命令行解析
* gui
    * [Qt](qt/0)
    * [QtQuick](qt-quick/0)
* 音頻處理
    * [lame](cc-lame/0) mp3 編碼解碼
    * [faad](cc-faad/0) aac 解碼器
    * faac aac 編碼器

# os
* [linux](cc-linux/0) 
# c
* [c](c/0)
* [http-parser](c-http-parser/0) http 解析
* [mbedtls](c-mbedtls/0) tls
* [curl](c-curl/0)
* [libtomcrypt](c-libtomcrypt/0) 加密解密
* [libevent](c-libevent/0) 事件循環
* [libev](c-libev/0) 事件循環
* [libuv](c-libuv/0) 事件循環
* [輕量庫](c-easy/0) 一些很簡單的輕量c庫
* 腳本引擎
    * [duktape](c-duktape/0) 腳本引擎
    * [iotjs](c-iotjs/0) 物聯網 js 環境