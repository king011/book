# 名字空間
ts 支持 namespace 定義 名字 空間 namespace 中 必須使用 export 的才能在 namespace 之外訪問
**animal.ts**
```ts
export interface IAnimal {
    Speak(): void
}
export namespace Animal{
    export class Cat implements IAnimal {
        Speak() {
            console.log("喵~")
        }
    }
    export class Dog implements IAnimal {
        Speak() {
            console.log("汪~")
        }
    }
}
```
**main.ts**
```ts
import { IAnimal, Animal } from './animal'
function speak(a: IAnimal) {
    a.Speak()
}
speak(new Animal.Cat())
speak(new Animal.Dog())
```

# 注意
雖然 namespace 可以作爲 模塊 導出 但通常 不會這麼做 因爲 模塊本身就是完成類類似 功能

namespace 通常 是用處處理模塊中 重名問題
**animal.ts**
```ts
export interface IAnimal {
    Speak(): void
}
namespace cat {
    const name: string = "Cat"
    export class Animal implements IAnimal {
        Speak() {
            console.log(name, "喵~")
        }
    }
}
namespace dog {
    const name: string = "Dog"
    export class Animal implements IAnimal {
        Speak() {
            console.log(name, "汪~")
        }
    }
}

export class Cat extends cat.Animal{
    
}
export class Dog extends dog.Animal{
    
}
```
**main.ts**
```ts
import { IAnimal, Cat, Dog } from './animal'
function speak(a: IAnimal) {
    a.Speak()
}
speak(new Cat())
speak(new Dog())
```