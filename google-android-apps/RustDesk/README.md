# RustDesk

一個多平臺的開源遠程桌面工具，是 TeamViewer 最好的替代品，允許自建中繼服務器

* 官網 [https://rustdesk.com/](https://rustdesk.com/)
* 源碼 [https://github.com/rustdesk/rustdesk](https://github.com/rustdesk/rustdesk)
* 文檔 [https://rustdesk.com/docs/en/](https://rustdesk.com/docs/en/)

# 中继服务

```
services:
  hbbs:
    container_name: hbbs
    network_mode: "host"
    image: rustdesk/rustdesk-server-pro:latest
    command: hbbs -k _
    volumes:
      - /opt/data/rustdesk:/root
    depends_on:
      - hbbr
    restart: unless-stopped

  hbbr:
    container_name: hbbr
    network_mode: "host"
    image: rustdesk/rustdesk-server-pro:latest
    command: hbbr -k _
    volumes:
      - /opt/data/rustdesk:/root
    restart: unless-stopped
```

客户端需要配置:
* **ID 服务器** your\_ip:21116
* **中继服务器** your\_ip:21117
* **key** 存储在 **/opt/data/rustdesk/id_ed25519.pub** 中

> 自建服務器似乎必須直接接入網路，測試時發現如果經過了路由器設備則客戶端會無法接入 rustdesk 網路