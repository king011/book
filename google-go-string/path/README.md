# [path](https://pkg.go.dev/path)

```
import "path"
```

path 實現了純詞法分析的路徑處理(以 **/** 作爲路徑分隔符)

# [var ErrBadPattern](https://pkg.go.dev/path#pkg-variables)

```
var ErrBadPattern = errors.New("syntax error in pattern")
```
ErrBadPattern 表示模式格式錯誤

# [func Base](https://pkg.go.dev/path#Base)

```
func Base(path string) string
```

Base 返迴路徑的最後一個元素。在提取最後一個元素之前刪除尾部斜線。如果路徑為空，Base 回傳「.」。如果路徑完全由斜線組成，則 Base 會傳回「/」

# [func Clean](https://pkg.go.dev/path#Clean)

```
func Clean(path string) string
```

Clean 以純詞法處理傳回與 path 等效的最短路徑名。它迭代地應用以下規則，直到無法進行進一步的處理：

1. 將多個斜線替換為單一斜線
2. 消除每一個 .路徑名元素（目前目錄）
3. 消除每個內部 .. 路徑名元素（父目錄）及其前面的非 .. 元素
4. 消除開始根路徑的 .. 元素：即將路徑開頭的“/..”替換為“/”
5. 僅當返回的路徑是根“/”時，才以斜線結尾

如果此過程的結果是空字串，Clean 將傳回字串「.」

# [func Dir](https://pkg.go.dev/path#Dir)

```
func Dir(path string) string
```

Dir 返迴路徑中除最後一個元素之外的所有元素，通常是路徑的目錄。使用 Split 刪除最後一個元素後，路徑將被清理，並且尾部斜線將被刪除。如果路徑為空，Dir 返回“.”。如果路徑完全由斜線後跟非斜線位元組組成，則 Dir 會傳回單一斜線。在任何其他情況下，返回的路徑都不以斜線結尾。

# [func Ext](https://pkg.go.dev/path#Ext)

```
func Ext(path string) string
```

Ext 返迴路徑使用的檔案副檔名。擴展名是從路徑最後一個斜線分隔元素中的最後一個點開始的後綴；如果沒有點，則為空。


# [func IsAbs](https://pkg.go.dev/path#IsAbs)

```
func IsAbs(path string) bool
```

IsAbs 報告路徑是否為絕對路徑

# [func Join](https://pkg.go.dev/path#Join)

```
func Join(elem ...string) string
```

Join 將任意數量的路徑元素連接到單一路徑中，並用斜線分隔它們。空元素將被忽略。結果就乾淨了。但是，如果參數清單為空或其所有元素均為空，則 Join 將傳回空字串

# [func Match](https://pkg.go.dev/path#Match)

```
func Match(pattern, name string) (matched bool, err error)
```

Match 返回 name 是否與指定的 shell pattern 匹配

shell 模式語法是：

```
pattern:
    { term }
term:
    '*'         匹配任何數量的非 non-/ 字符
    '?'         匹配單個非 non-/ 字符
    '[' [ '^' ] { character-range } ']'
                character class (must be non-empty)
    c           匹配指定字符 (字符不能是 '*', '?', '\\', '[')
    '\\'       \ 轉義匹配特殊的指定字符 *?\[

character-range:
    c           匹配指定字符 c (c != '\\', '-', ']')
    '\\'       轉義匹配特殊的指定字符 *?\[
    lo '-' hi   匹配字符範圍 c for lo <= c <= hi
```

匹配需要模式來匹配所有名稱，而不僅僅是子字串。當模式格式錯誤時，唯一可能傳回的錯誤是 ErrBadPattern

```
package main

import (
    "fmt"
    "path"
)

func main() {
    fmt.Println(path.Match("abc", "abc")) // true
    fmt.Println(path.Match("a*", "abc")) // true
    fmt.Println(path.Match("a*/b", "a/c/b")) // false

    // 指定範圍
    fmt.Println(`[]`)
    fmt.Println(path.Match(`[a-zA-Z]`, `c`)) // true
    fmt.Println(path.Match(`[a-zA-Z]`, `C`)) // true
    fmt.Println(path.Match(`[a-zA-Z]`, `1`)) // false
    fmt.Println(path.Match(`[a-zA-Z]`, `2`)) // false

    // 排除特定值
    fmt.Println(`[^]`)
    fmt.Println(path.Match(`[^A-Z]`, `c`)) // true
    fmt.Println(path.Match(`[^AB]`, `A`))  // false
    fmt.Println(path.Match(`[^AB]`, `B`))  // false
    fmt.Println(path.Match(`[^AB]`, `C`))  // true
}
```

# [func Split](https://pkg.go.dev/path#Split)

```
func Split(path string) (dir, file string)
```

Split 緊跟在最後一個斜線之後分割路徑，將其分成目錄和檔案名稱部分。如果路徑中沒有斜杠，則 Split 會傳回一個空目錄並將檔案設為路徑。傳回值具有 path = dir+file 的屬性