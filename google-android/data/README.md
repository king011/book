# 數據存儲
android 提供了多個 標準的 數據存儲方案

* SharedPreferences 以 key/value 形式 存儲私有 基本數據
* Internal Storage 保存 私有數據 到 設備內部 存儲
* External Storage 保存 共有數據 到 設備外部 存儲 比如 SD卡
* SQLite Database 保存 結構化數據 到 私有 數據庫

# SharedPreferences
[android.content.SharedPreferences](https://developer.android.com/reference/android/content/SharedPreferences)  提供了以 key/value 形式 持久化數據 到 xml 檔案的 功能

官方 對 SharedPreferences 的定位是 通常 用來保存 套件的 用戶設置信息 比如
* app 使用 皮膚
* app 語言偏好

Context 提供了 getSharedPreferences 函數 用來 返回/創建 當前 app 管理的 SharedPreferences
```java
#info=false

// name 指定了 xml 不帶後綴名的 檔案名稱 
//
// mode 以前提供了 多個 值但都已經被 廢棄 現在只有 MODE_PRIVATE(0) 還被使用
public abstract SharedPreferences getSharedPreferences (String name, int mode)
```

> SharedPreferences 存儲在 /data/data/套件包名/shared_prefs 檔案夾中
>
> name 必須是合法的 檔案名 否則 getSharedPreferences 失敗會 返回 null
> 
> xml 檔案 只能被 當前 app 讀取 但 對於有 root 權限的 程式來說 無視這條規則

android 爲 Context.getSharedPreferences 提供了 幾個 語法糖
* Activity.getPreferences(int mode) 使用 當前 class name 作爲 xml 檔案名 (比如 MainActivity)
* PreferenceManager.getDefaultSharedPreferences(Context) 使用 當前 Context 定義的 包名\_preferences 作爲 xml 檔案名 (比如 pcom.king011.example\_preferences)

```
package com.king011.example;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static public final String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onGet(View view){
        // 打開 SharedPreferences
        SharedPreferences sharedPreferences = this.getSharedPreferences("person",MODE_PRIVATE);
        Log.i(TAG,sharedPreferences.getString("name",""));
        Log.i(TAG,sharedPreferences.getString("love","kate beckinsale"));
        Log.i(TAG,Integer.toString(sharedPreferences.getInt("lv",0)));
    }
    public void onPut(View view){
        // 打開 SharedPreferences
        SharedPreferences sharedPreferences = this.getSharedPreferences("person",MODE_PRIVATE);

        // 返回 寫入器
        SharedPreferences.Editor editor = sharedPreferences.edit();
        {
            // 寫入 數據
            editor
                    .putString("name","king") // 支持 鏈式寫法
                    .putInt("lv",10)
                    .putInt("lv",12) // 以 k/v 存儲 最後put的key會覆蓋掉 前面的
            ;
        }
        // 提交 寫入
        editor.commit();
    }
}
```

# Internal Storage
openFileOutput/openFileInput 需要傳入一個 檔案名 分別 java.io.FileOutputStream/java.io.FileInputStream

用來 寫入/讀取 此 檔案 

> 檔案 保存在 /data/data/套件包名/files 檔案夾中
>
> 類似 SharedPreferences 失敗 name 必須 符合 檔案名 規範 
> 
> 同樣 只可被當前 app 讀取 不過 對 root 無能爲力

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    static public final String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onGet(View view){
        FileInputStream r = null;
        try {
            r = openFileInput("cerberus");
            byte[] b = new byte[r.available()];
            r.read(b);
            Log.i(TAG,new String(b));
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(r != null){
                try {
                    r.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
    public void onPut(View view){
        FileOutputStream w = null;
        try {
            //w = openFileOutput("pp",MODE_PRIVATE|MODE_APPEND);
            w = openFileOutput("cerberus",MODE_PRIVATE);
            w.write(("cerberus is an idea").getBytes());
        }catch (FileNotFoundException e){
           e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(w != null){
                try {
                    w.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
```

# External Storage
Environment.getExternalStorageXXX 提供了 對 外部存儲卡的訪問
```
        File root = Environment.getExternalStorageDirectory();
        if(root.exists() && root.canWrite()){
            File file = new File(root,"a.txt");
        }
```

# SQLite Database

android 提供了 對 Sqlite 的 原生支持

需要 從 SQLiteOpenHelper 派生一個 類 來 實現 數據庫的 創建

> 數據庫 存儲在 /data/data/包名/databases 檔案夾中

```
package com.king011.example;


import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class Manipulator extends SQLiteOpenHelper {
    public static final String TAG = "Manipulator";
    public static final String DatabaseName = "cerberus";
    public static final int DatabaseVersion = 1;
    Manipulator(Context context){
        // 調用 super 傳入 數據庫名 版本
        super(context,DatabaseName,null,DatabaseVersion);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // 當 數據庫 不存在時 會 調用 此 方法 應該在 此處 初始化 表
        db.execSQL("CREATE TABLE IF NOT EXISTS person (name VARCHAR(20), lv INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 當 數據庫 升級 時 此方法 被調用
        //
        // 通常 應該 修改 表 結構 會 保存舊數據 刪除表 升級後 恢復 數據
        // 此處 爲力 簡單 直接 刪除表後 重建新表

        // 啓動 一個 事務
        db.beginTransaction();
        try{
            db.execSQL("DROP TABLE IF EXISTS person");

            db.execSQL("CREATE TABLE IF NOT EXISTS person (id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR(20), lv INTEGER)");
            // 設置 事務 成功
            db.setTransactionSuccessful();
        }finally {
            // 結束 事務
            // 如果 設置了 setTransactionSuccessful 則 提交事務 否則 回滾 事務
            db.endTransaction();
        }
    }

    public void put(){
        // 返回 可寫 庫
        SQLiteDatabase db = null;
        SQLiteStatement statement = null;
        try {
            // getWritableDatabase 成功 會 被 緩存 因此可以 在每次使用時 調用 getWritableDatabase 而不是 自己 在 爲 db 創建 連接 緩存
            db = getWritableDatabase();

            // 創建 statement
            statement = db.compileStatement("insert into person values (?,?),(?,?)");
            statement.bindString(1, "kate");
            statement.bindLong(2, 9);
            statement.bindString(3, "anita");
            statement.bindLong(4, 10);
            // 執行 插入
            statement.executeInsert();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(statement != null){
                try {
                    statement.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
            if(db != null){
                try {
                    db.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }
    public void get(){
        // 返回 可寫 庫
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = getReadableDatabase();
            // 執行 查詢
            cursor = db.rawQuery("select * from person where lv > ?", new String[]{"9"});
            while (cursor.moveToNext()) {
                Log.i(TAG, cursor.getString(0) + " " + Integer.toString(cursor.getInt(1)));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            if(cursor!=null){
                try {
                    cursor.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
            if(db != null){
                try {
                    db.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
```