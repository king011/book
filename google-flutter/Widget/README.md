# Widget

flutter 是組件式的界面框架 所有界面元素 都是由 Widget 所組成的

abstract class [Widget](https://docs.flutter.io/flutter/widgets/Widget-class.html) 定義了 組件的基本接口

Widget 是不可變的 界面 元素的 描述

* StatelessWidget 是 Widget 的無狀態 的細化 描述
* StatefulWidget 是 Widget  帶有 一組 可變化狀態 的細化 描述

flutter 的入口點是 runApp 函數 此函數 需要 接收一個 Widget 實例

# StatelessWidget

abstract class [StatelessWidget](https://docs.flutter.io/flutter/widgets/StatelessWidget-class.html) 是無狀態的組件 始終以相同 方式 構建

通知作爲 最頂級的 入口 組件 定義 app 的 主題 風格 等

要 實現 StatelessWidget 需要重新 Widget build(BuildContext context) 來構建可視化的 組件

```dart
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
```

# StatefulWidget
abstract class [StatefulWidget](https://docs.flutter.io/flutter/widgets/StatefulWidget-class.html) 組件 帶有 一組 可變化的 狀態 可以在運行時 改變 這些 狀態 來 改變 渲染的界面

StatefulWidget 需要 實現 State createState() 方法 來 創建 一個 狀態

可以 從 State 派生 實現 Widget build(BuildContext context) 即可

```dart

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  // 不可變化的 屬性 通過 final 定義在 StatefulWidget 中
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // 定義 可變 狀態
  int _counter = 0;

  void _incrementCounter() {
    // 在 setState 函數中 修改 可變狀態
    // setState 會 通知 flutter framework 狀態變化 更新 ui
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),// widget 取得泛型的引用 訪問是其中定義的屬性
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',//在ui組件中 訪問 可變狀態
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,//用戶單擊調用 函數 修改狀態
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
```

# State

State 提供了 一些 回調函數 在 State 的 生命週期 變化時 被調用

1. initState 當 Widget 第一次 被插入到 Widget 樹時 被調用 一次
    * 所以通常在此 調用 一次性的 初始化 操作 如 訂閱事件通知 初始化 狀態
    * 不能在此 訪問 widget 樹 因爲 Widget 還沒完成 初始化 如果需要 應該將代碼 移到 build
1. didChangeDependencies 當依賴發送 變化時 比如 祖先包含的 InheritedWidget 發生了變化
1. build 構建 Widget
    * 在調用 initState() 之後
    * 在調用 didUpdateWidget() 之後
    * 在調用 setState() 之後
    * 在調用 didChangeDependencies() 之後
    * 在調用 State 對象從一個位置 移除後 又此虛擬插入到其它位置之後
1. reassemble 供開發者使用 只在調試模式下 熱重載市會被調用
1. didUpdateWidget 在重新構建widget時 flutter 會調用 Widget.canUpdate 來檢查決定是否調用此函數
1. deactivate 當 Statue 對象 被移除時 會調用 比如 flutter 在一定情況下 會將 對象所在子樹從一個位置移到新位置
1. dispose 當 State 被永久移除時 通常應該在此 進行 資源釋放
