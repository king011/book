# 拖動

flutter 提供了 Draggable/LongPressDraggable 來創建一個 拖動源 DragTarget 來創建 拖動 目標

![](assets/draggable.gif)

```
import 'package:flutter/material.dart';
import '../widget/drawer.dart';
import '../../Fontello.dart';

class ExamplePage extends StatefulWidget {
  ExamplePage({Key key}) : super(key: key);
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  final _items = ["king", "kate", "anita", "anna"];

  Widget _buildItem(BuildContext context, int index) {
    final theme = Theme.of(context);
    final name = _items[index];
    // Draggable/LongPressDraggable 創建一個 可拖動的 源
    return LayoutBuilder(
      builder: (context, constraint) {
        return Draggable(
          // data 指定 拖動的 綁定數據
          data: index,
          // child 指定 源 顯示ui
          // DragTarget 創建一個 拖動目標 來完成 拖動
          child: DragTarget<int>(
            builder: (context, candidate, rejects) {
              return SizedBox(
                child: Card(
                  child: ListTile(
                    title: Text(name),
                  ),
                ),
              );
            },
            onLeave: (data) {
              // 當 拖動源 離開當前 拖動目標時 回調
              debugPrint("onLeave $data -> $index");
            },
            onWillAccept: (data) {
              // 當源被 拖動到此 覆蓋 當前 拖動目標時 回調
              //
              // 如果返回 true 將 接受 拖動 否則 拒絕拖動
              debugPrint("onWillAccept $data -> $index");
              return _items != null && data >= 0 && data < _items.length;
            },
            onAccept: (data) {
              debugPrint("onAccept $data -> $index");
              // onWillAccept 返回 true 且 在此拖動目標上 鬆開手指時 回調
              //
              // 回調完成 後 通知 Draggable.onDragCompleted
              setState(() {
                if (data < index) {
                  final tmp = _items[data];
                  for (var i = data; i < index; i++) {
                    _items[i] = _items[i + 1];
                  }
                  _items[index] = tmp;
                } else {
                  final tmp = _items[data];
                  for (var i = data; i > index; i--) {
                    _items[i] = _items[i - 1];
                  }
                  _items[index] = tmp;
                }
              });
            },
          ),
          // childWhenDragging 指定了被拖動後 源如何顯示 如果爲null 顯示 child
          childWhenDragging: SizedBox(
            child: Card(
              child: ListTile(
                title: Text(name, style: TextStyle(color: theme.accentColor)),
              ),
            ),
          ),
          // feedback 指定了 源被 拖動走時 跟隨手指移動的 顯示ui
          feedback: SizedBox(
            width: constraint.maxWidth - 4,
            height: 64,
            child: Card(
              child: ListTile(
                title: Text(name),
              ),
            ),
          ),
          onDragStarted: () {
            print("onDragStarted 拖動開始回調");
          },
          onDraggableCanceled: (Velocity velocity, Offset offset) {
            debugPrint("onDraggableCanceled 拖動取消回調");
          },
          onDragCompleted: () {
            debugPrint("onDragCompleted 拖動完成回調");
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final children = List<Widget>(_items.length);
    for (var i = 0; i < _items.length; i++) {
      children[i] = _buildItem(context, i);
    }
    return Scaffold(
      drawer: MyDrawer(home: true),
      appBar: AppBar(
        title: Text("Example"),
      ),
      body: ListView(
        children: children,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: null,
      ),
    );
  }
}
```

# 列表 跟隨拖動 變化

如果要讓 被拖動的列表 跟隨 拖動變化 而非在 最後才變化 可以 在 onDragStarted 備份顯示數據 onDraggableCanceled 恢復數據 即可

當然 還需要 配合 DragTarget 的 onLeave/onWillAccept 設置 當前 顯示的 列表 數據

![](assets/draggable2.gif)

```
import 'package:flutter/material.dart';
import '../widget/drawer.dart';
import '../../Fontello.dart';

class ExamplePage extends StatefulWidget {
  ExamplePage({Key key}) : super(key: key);
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  var _items = ["king", "kate", "anita", "anna"];
  List<String> _backup;
  _updateTarget(source, target) {
    setState(() {
      if (source < target) {
        final tmp = _items[source];
        for (var i = source; i < target; i++) {
          _items[i] = _items[i + 1];
        }
        _items[target] = tmp;
      } else {
        final tmp = _items[source];
        for (var i = source; i > target; i--) {
          _items[i] = _items[i - 1];
        }
        _items[target] = tmp;
      }
      _source = target;
    });
  }

  // 記錄 拖動 源數據
  // 重構了 list 傳給 DragTarget 回調的 源數據 不會改變 所以只好自己記錄
  int _source;

  Widget _buildItem(BuildContext context, int index) {
    final theme = Theme.of(context);
    final name = _items[index];
    // Draggable/LongPressDraggable 創建一個 可拖動的 源
    return LayoutBuilder(
      builder: (context, constraint) {
        return Draggable(
          // data 指定 拖動的 綁定數據
          data: index,
          // child 指定 源 顯示ui
          // DragTarget 創建一個 拖動目標 來完成 拖動
          child: DragTarget<int>(
            builder: (context, candidate, rejects) {
              return SizedBox(
                child: Card(
                  child: ListTile(
                    title: Text(
                      name,
                      style: _source == index
                          ? TextStyle(color: theme.accentColor)
                          : null,
                    ),
                  ),
                ),
              );
            },
            onLeave: (data) {
              // 當 拖動源 離開當前 拖動目標時 回調
              debugPrint("onLeave $data -> $index");
            },
            onWillAccept: (data) {
              // 當源被 拖動到此 覆蓋 當前 拖動目標時 回調
              //
              // 如果返回 true 將 接受 拖動 否則 拒絕拖動
              debugPrint("onWillAccept $data -> $index");
              final source = _source ?? data;
              if (_items != null && source >= 0 && source < _items.length) {
                _updateTarget(source, index);
                return true;
              }
              return false;
            },
            onAccept: (data) {
              debugPrint("onAccept $data -> $index");
              // onWillAccept 返回 true 且 在此拖動目標上 鬆開手指時 回調
              //
              // 回調完成 後 通知 Draggable.onDragCompleted
              if (_source != null) {
                setState(() {
                  _source = null;
                });
              }
            },
          ),
          // feedback 指定了 源被 拖動走時 跟隨手指移動的 顯示ui
          feedback: SizedBox(
            width: constraint.maxWidth - 4,
            height: 64,
            child: Card(
              child: ListTile(
                title: Text(name),
              ),
            ),
          ),
          onDragStarted: () {
            print("onDragStarted 拖動開始回調");
            // 備份 數據 以便 取消時  恢復 原始顯示狀態
            _backup = _items.sublist(0);
          },
          onDraggableCanceled: (Velocity velocity, Offset offset) {
            debugPrint("onDraggableCanceled 拖動取消回調");
            // 恢復 原始狀態
            setState(() {
              _items = _backup;
              _source = null;
              _backup = null;
            });
          },
          onDragCompleted: () {
            debugPrint("onDragCompleted 拖動完成回調");
            // 釋放 備份數據
            _backup = null;
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final children = List<Widget>(_items.length);
    for (var i = 0; i < _items.length; i++) {
      children[i] = _buildItem(context, i);
    }
    return Scaffold(
      drawer: MyDrawer(home: true),
      appBar: AppBar(
        title: Text("Example"),
      ),
      body: ListView(
        children: children,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: null,
      ),
    );
  }
}
```
