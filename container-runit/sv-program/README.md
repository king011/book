# sv program

sv 程式擁有管理由 runsv 監控的服務

```
sv [-v] [-w sec] command services
```

# Services

services 有一個或多個服務名稱，服務器是 runsv 使用的檔案夾名。如果服務不以 **.** 或 **/** 開頭且不以 **/** 作爲結尾，則默認從 **/etc/service/** 中搜索，否則相對於當前目錄

# Commands

* status -> 將服務的當前狀態和附加日誌服務(如果可用) 輸出到 stdout
* up -> 如果服務未運行，啓動它。如果服務停止重啓它
* down-> 如果服務正在運行，則向其發送 TERM signal 和 CONT signal。如果 ./run 退出則啓動 ./finish(如果存在)。停止後，不要重啓服務
* once -> 如果服務未運行，啓動它。如果停止，請不要重新啓動它
* pause cont hup alarm interrupt quit 1 2 term kill -> 如果服務正在運行，則分別向其發送 STOP, CONT, HUP, ALRM, INT, QUIT, USR1, USR2, TERM, or KILL 信號
* exit -> 如果服務正在運行，則向其發送 TERM singal 和 CONT signal。不要重新啓動服務。如果服務關閉，並且不存在日誌服務，則 runsv 退出。如果服務關閉並且存在日誌服務，runsv 關閉日誌服務的標準輸入並等待它終止。如果日誌服務關閉 runsv 將退出。如果將此命令提供給附加日誌服務，則忽略此命令

# Commands compatible to LSB init script actions

* status -> Same as status
* start -> Same as up，但最多等待 7 秒以使命令生效。然後報告狀態或超時。如果 ./check 存在於服務目錄中，sv 運行此腳本以檢查服務是否已啓動且可用；如果 check 以 0 退出則認爲它可用
* stop -> Same as down，但最多等待 7 秒以使服務停止。然後報告狀態或超時。
* reload -> Same as hub，並在之後另外報告狀態
* restart -> 將 term cont up 指令發送到服務，並等待最多 7 秒 以使服務重新啓動。然後報告狀態或超時。如果服務目錄下存在 ./check 腳本，sv 運行該腳本檢查服務是否已經啓動並再次可用；如果 check 以 0 退出，則認爲它可用。
* shutdown -> Same as exit，但最多等待 7 秒讓 runsv 進程終止。然後報告狀態或超時
* force-stop -> Same as down，但最多等待 7 秒以使服務停止。然後報告狀態，並在超時時向服務發送 kill 命令
* force-reload -> 向服務器發送 term 和 cont 命令，並等待最多 7 秒以使用服務重新啓動，然後報告狀態，並在超時時向服務發送 kill 命令
* force-restart -> 向服務發送 term cont up 命令，並等待最多 7 秒以使用服務重新啓動。然後報告狀態，並在超時時向服務發送 kill 命令。如果服務目錄下存在 ./check 腳本，sv 運行該腳本檢查服務是否已經啓動並再次可用；如果 check 以 0 退出，則認爲它可用
* force-shutdown -> Same as exit，但最多等待 7 秒讓 runsv 進程終止。然後報告狀態，並在超時時向服務發送 kill 命令
* try-restart -> 如果服務正在運行，向其發送 term 和 cont 命令，並等待最多 7 秒以使服務重新啓動。然後報告狀態或超時

# Additional Commands

* check -> 檢查服務是否處於請求的狀態。等待服務達到請求狀態最多 7 秒，然後報告狀態或超時。如果服務的請求狀態時 up，並且服務目錄下存在 ./check 腳本，sv 運行這個腳本用來檢查服務是否已經啓動並運行；如果 check 以 0 退出，則認爲它已啓動

# Options

* -v -> 如果命令爲 up, down, term, once, cont, or exit，則最多等待 7 秒以使用命令生效。然後報告狀態或超時
* -w sec -> 用 sec 秒覆蓋默認的 7 秒超時。此選項暗示 -v

