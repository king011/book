# alpine

alpine 是社區基於 musl 和 BusyBox 維護的一個 linux 發行版本

alpine 的特殊是體積小但功能完整，注重安全，主要面向 x86路由器 防火牆 虛擬私人網路 ip電話盒 以及伺服器 而設計

alpine 使用 ash 作爲默認的 shell

* [官網](https://alpinelinux.org/)

