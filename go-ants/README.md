# ants

ants 是一個開源(MIT)的 goroutine 池 可以用來 限制併發量 和 重用 goroutine

For v1
```
go get -u github.com/panjf2000/ants
```

For v2
```
go get -u github.com/panjf2000/ants/v2
```

* 源碼 [https://github.com/panjf2000/ants](https://github.com/panjf2000/ants)