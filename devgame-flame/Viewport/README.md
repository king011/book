# [Camera and Viewport](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#camera-component)


相機即組件是建立遊戲的新方式，這種方法可以更靈活地放置相機，甚至可以同時擁有多台相機

為了理解這種方法的工作原理，請想像您的遊戲世界是一個獨立於您的應用程式而存在的實體。 想像一下，您的遊戲只是您可以觀察那個世界的窗戶。 你可以隨時關閉該窗口，而遊戲世界仍然存在。 或者，相反，您可以打開多個窗口，同時查看同一個世界（或不同的世界）

有了這種思維方式，我們現在可以了解相機即組件的工作原理

首先，有 World 類，它包含遊戲世界中的所有組件。 World 組件可以安裝在任何地方，例如遊戲類別的根目錄，就像內建 World 一樣

然後，一個「觀察」世界的 CameraComponent 類別。 CameraComponent 內部有一個 Viewport 和 Viewfinder，既可以靈活地在螢幕上的任何位置渲染世界，也可以控制觀看位置和角度。 CameraComponent 還包含一個在世界下方靜態渲染的背景元件

# [World](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#world)

該組件應用於託管構成遊戲世界的所有其他組件。 World 類別的主要屬性是它不是透過傳統方式渲染 - 而是由一個或多個 CameraComponent 渲染以「查看」世界。 在FlameGame類別中，有一個名為world的 World 實例，它是預設添加的，並與名為 camera 的預設 CameraComponent 實例配對在一起

一個遊戲可以有多個 World 實例，這些實例可以同時或不同時間渲染。 例如，如果您有兩個世界 A 和 B 以及一個攝影機，則將該攝影機的目標從 A 切換到 B 將立即將視圖切換到世界 B，而無需卸載 A 然後安裝 B

就像大多數元件一樣，可以透過在建構函式中使用 Children 參數，或使用 add 或 addAll 方法將子元件加入 World 中

對於許多遊戲，您想要擴展世界並在其中創建邏輯，這樣的遊戲結構可能如下所示：

```
void main() {
  runApp(GameWidget(FlameGame(world: MyWorld())));
}

class MyWorld extends World {
  @override
  Future<void> onLoad() async {
    // Load all the assets that are needed in this world
    // and add components etc.
  }
}
```

# [CameraComponent](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#cameracomponent)

這是一個渲染世界的元件。 它需要在建構過程中引用 World 實例； 但稍後目標世界可以替換為另一個世界。 多個攝影機可以同時觀察同一個世界

FlameGame 類別中有一個名為camera 的預設 CameraComponent，它與預設 World 配對，因此如果您的遊戲不需要，則無需創建或添加自己的 CameraComponent

CameraComponent 內部還有另外兩個組件：視窗和觀景窗。 與 World 物件不同，相機擁有 Viewport(視窗) 和 Viewfinder(取景器)，這意味著這些組件是相機的子組件

還有一個靜態屬性CameraComponent.currentCamera，僅在渲染階段不為null，它會傳回目前執行渲染的相機物件。 僅對於某些高級用例需要這樣做，其中組件的渲染取決於相機設定。 例如，如果某些元件位於相機 Viewport(視窗) 之外，則它們可能會決定跳過自身及其子元件的渲染

FlameGame 類別在其建構函式中有一個相機字段，因此您可以設定所需的預設相機類型，例如：

```
void main() {
  runApp(
    GameWidget(
      FlameGame(
        camera: CameraComponent.withFixedResolution(width: 800, height: 600),
      ),
    ),
  );
}
```

## [CameraComponent.withFixedResolution()](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#cameracomponent-withfixedresolution)

這個工廠構造函數將讓您假裝用戶的設備具有您選擇的固定解析度。 例如：

```
final camera = CameraComponent.withFixedResolution(
  world: myWorldComponent,
  width: 800,
  height: 600,
);
```

這將創建一個視口位於螢幕中間居中的相機，佔用盡可能多的空間，同時仍保持 800:600 的縱橫比，並顯示大小為 800 x 600 的遊戲世界區域

「固定解析度」使用起來非常簡單，但它不會充分利用用戶的可用螢幕空間，除非他們的裝置恰好具有與您選擇的尺寸相同的像素比


## [Viewport](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#viewport)

Viewport 是一個可以看到世界的視窗。 此視窗在螢幕上具有特定的大小、形狀和位置。 有多種可用的視口，您始終可以實現自己的視口。

視口是一個組件，這意味著您可以向其中添加其他組件。 這些子組件將受到視口位置的影響，但不受其剪輯遮罩的影響。 因此，如果視口是遊戲世界的“視窗”，那麼它的子項就是可以放在視窗頂部的東西

將元素新增至視窗是實現「HUD」元件的便捷方法

以下 viewports 可用：

* **MaxViewport** (default) – 這個視窗將擴展到遊戲允許的最大尺寸，即它將等於遊戲畫布的尺寸
* **FixedResolutionViewport** – 保持解析度和寬高比固定，如果與寬高比不匹配，則兩側會出現黑條
* **FixedSizeViewport** – 具有預先定義尺寸的簡單矩形視窗
* **FixedAspectRatioViewport** – 矩形視口，可擴展以適合遊戲畫布，但保持其縱橫比
* **CircularViewport** – 圓形視口，大小固定

如果將子項新增至視口，它們將在世界面前顯示為靜態 HUD

## [Viewfinder](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#viewfinder)

相機的這一部分負責了解我們目前正在查看的底層遊戲世界中的哪個位置。 取景器還控制縮放等級和視圖的旋轉角度

取景器的錨點屬性可讓您指定視窗內的哪個點作為相機的「邏輯中心」。 例如，在橫向捲軸動作遊戲中，通常將攝影機聚焦在主要角色上，該角色不顯示在螢幕中央，而是靠近左下角。 這個偏離中心的位置將是相機的“邏輯中心”，由取景器的錨點控制

如果將子項新增至取景器，它們將出現在世界前面，但在視窗後面，並且具有與應用於世界相同的變換，因此這些元件不是靜態的

您也可以將行為元件作為子元件新增至觀景器，例如效果或其他控制器。 例如，如果您新增 ScaleEffect，您將能夠在遊戲中實現平滑縮放

## [Backdrop](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#backdrop)

若要在世界後面新增靜態元件，您可以將它們新增至背景元件中，或取代背景元件。 例如，如果您希望在可以在其周圍移動的世界下方擁有靜態 ParallaxComponent，則這非常有用

Example:

```
camera.backdrop.add(MyStaticBackground());
```

or

```
camera.backdrop = MyStaticBackground();
```

# [Camera controls](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#camera-controls)

有幾種方法可以在運行時修改相機的設定：

1. 手動執行。 您始終可以重寫 CameraComponent.update() 方法（或取景器或視窗上的相同方法），並在其中根據需要更改取景器的位置或縮放。 這種方法在某些情況下可能可行，但一般情況下不建議這樣做。
2. 將 effects and/or  behaviors 應用到相機的取景器或視窗。 effects 和 behaviors 是特殊類型的元件，其目的是隨著時間的推移修改它們所附加的元件的某些屬性
3. 使用特殊的相機函數，例如 follow()、moveBy() 和 moveTo()。 在幕後，此方法使用與 (2) 相同的 effects/behaviors

相機有幾種控制其行為的方法：

* **Camera.follow()** 將強制相機跟隨提供的目標。 您可以選擇限制相機的最大移動速度，或僅允許其水平/垂直移動
* **Camera.stop()** 將撤銷先前呼叫的效果並將相機停止在目前位置
* **Camera.moveBy()** 可用於將相機移動指定的偏移量。 如果相機已經跟隨另一個組件或朝另一個組件移動，這些行為將會自動取消
* **Camera.moveTo()** 可用於將相機移至世界地圖上的指定點。 如果相機已經跟隨另一個組件或朝另一個點移動，這些行為將會自動取消
* **Camera.setBounds()** 可讓您新增相機允許移動的限制。 這些限制採用 Shape 的形式，通常是矩形，但也可以是任何其他形狀

## [visibleWorldRect](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#visibleworldrect)

相機公開屬性visibleWorldRect，它是一個描述目前透過相機可見的世界區域的矩形。 可以使用該區域來避免渲染視圖之外的元件，或不那麼頻繁地更新遠離玩家的物件

visibleWorldRect 是一個快取的屬性，只要相機移動或視窗改變其大小，它就會自動更新

## [檢查組件相對相機是否可見](https://docs.flame-engine.org/1.16.0/flame/camera_component.html#check-if-a-component-is-visible-from-the-camera-point-of-view)

CameraComponent 有一個名為 canSee 的方法，可用來檢查元件從相機角度是否可見。 例如，這對於剔除不在視圖中的元件很有用

```
if (!camera.canSee(component)) {
   component.removeFromParent(); // Cull the component
}
```
