# [io/ioutil](https://pkg.go.dev/io/ioutil)

io/ioutil 提供了 一些 相當實用的 io 工具

#  Discard NopCloser

## Discard

Discard 是導出的一個 變量 它 類似 linux的 /dev/null
任何 對其的 Write 都直接返回 成功 而不執行任何 具體的 事情

在關閉日誌時 將 Writer 設置 爲Discard 是個標準做法

```go
#info=false
var Discard io.Writer = devNull(0)
```

## NopCloser

* func NopCloser(r io.Reader) io.ReadCloser 將 Reader 包裝爲 ReadCloser
* 其 Close 會直接返回 nil (無論原Reader 是否有 Close 函數)


# ReadXXX

## ReadAll

`func ReadAll(r io.Reader) ([]byte, error)`

ReadAll 從 一個 Reader 中 讀取 數據 直到 **EOF** 或 出錯  
如果 ReadAll **成功 不會返回 EOF 而是返回 nil**


## ReadFile

`func ReadFile(filename string) ([]byte, error)`

ReadFile 完全是 ReadAll 的語法糖 相當於 如下代碼
```go
f,e:=os.Open(filename)
...
defer f.Close()
...
return ReadAll(f)
```

## ReadDir

`func ReadDir(dirname string) ([]os.FileInfo, error)`

* ReadDir 自動幫你 列出 檔案架下的所有 條目 並安名稱 排序
* ReadDir 不會列取子目錄內容

# WriteFile

`func WriteFile(filename string, data []byte, perm os.FileMode) error`

WriteFile 自動創建檔案 並且 將 data 全部 寫入到 檔案中

# TempDir TempFile

## TempDir

`func TempDir(dir, prefix string) (name string, err error)`

* TempDir 在 dir 檔案夾下 自動創建 以prefix爲名稱前綴的 不重名 的臨時檔案夾 並返回 創建時使用的 臨時檔案夾名
* 調用者 需要 自己 調用 os.RemoveAll(name) 在不用時 刪除 臨時 檔案夾

## TempFile

`func TempFile(dir, prefix string) (f *os.File, err error)`

TempFile 和 TempDir 使用相同的模式 工作 只是 現在是 以 讀寫模式 創建 臨時檔案

## 注意

* 如果 dir 爲空字符串 則使用 創建在 os.TempDir() 目錄下
* TempDir TempFile 的調用者 需要 自己刪除 臨時 檔案
* 多個 程式 同時 調用 TempDir TempFile 不會創建出相同的 檔案/檔案夾

```go
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	// 創建 臨時檔案夾
	path, e := ioutil.TempDir("", "dir")
	if e != nil {
		log.Fatalln(e)
	}
	defer os.RemoveAll(path) // 刪除
	fmt.Println("dir :", path)

	// 創建臨時檔案
	var f *os.File
	f, e = ioutil.TempFile(path, "file")
	if e != nil {
		log.Println(e)
	}
	defer func() {
		filename := f.Name()
		f.Close()
		// 此處可 省略 因爲 上面 os.RemoveAll 本來就會 刪除到 dir 下的 所有東西
		os.Remove(filename)
	}()
	fmt.Println("file :", f.Name())
}
```

