# vscode

vscode 需要安裝 [Unity Tools](https://marketplace.visualstudio.com/items?itemName=Tobiah.unity-tools) 插件

```
ext install Tobiah.unity-tools
```

此外還需要安裝 .net 環境

* [linux](https://docs.microsoft.com/en-us/dotnet/core/install/linux)

# linux

對於 linux 還需要安裝 [Mono](https://www.mono-project.com/download/stable/#download-lin-ubuntu) 開發環境

並且在 vscode 中配置 omnisharp.useGlobalMono 屬性爲 always

```
#info="settings.json"
{
    "omnisharp.useGlobalMono":"always"
}
```