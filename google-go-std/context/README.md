# [context](https://pkg.go.dev/context)

context 包 定義了 上下文信息 它可以跨 API/進程 傳遞 截至日期/取消信號/其它值

# Context

Context 是上下文的 接口定義

```go
type Context interface {
    // 如果有設置 截至日期 則 返回 截至日期 和 true
    //
    // 否則 返回 false
    Deadline() (deadline time.Time, ok bool)

    // Done 返回一個 channel 當操作完成時 會被關閉,
    // 如果一個操作 永遠沒有完成 狀態 則Done會 返回 nil
    //
    // WithCancel arranges for Done to be closed when cancel is called;
    // WithDeadline arranges for Done to be closed when the deadline
    // expires; WithTimeout arranges for Done to be closed when the timeout
    // elapses.
    //
    // Done is provided for use in select statements:
    //
    //  // Stream generates values with DoSomething and sends them to out
    //  // until DoSomething returns an error or ctx.Done is closed.
    //  func Stream(ctx context.Context, out chan<- Value) error {
    //  	for {
    //  		v, err := DoSomething(ctx)
    //  		if err != nil {
    //  			return err
    //  		}
    //  		select {
    //  		case <-ctx.Done():
    //  			return ctx.Err()
    //  		case out <- v:
    //  		}
    //  	}
    //  }
    //
    // See https://blog.golang.org/pipelines for more examples of how to use
    // a Done channel for cancelation.
    Done() <-chan struct{}

    // 如果 Done 沒有被關閉 返回 nil
    // 如果 Done 已經被關閉 則返回 關閉原因
    Err() error
		
    // Value 返回 類似 map[interface{}]interface{} 的 key 對應的值
    //
    // Value returns the value associated with this context for key, or nil
    // if no value is associated with key. Successive calls to Value with
    // the same key returns the same result.
    //
    // Use context values only for request-scoped data that transits
    // processes and API boundaries, not for passing optional parameters to
    // functions.
    //
    // A key identifies a specific value in a Context. Functions that wish
    // to store values in Context typically allocate a key in a global
    // variable then use that key as the argument to context.WithValue and
    // Context.Value. A key can be any type that supports equality;
    // packages should define keys as an unexported type to avoid
    // collisions.
    //
    // Packages that define a Context key should provide type-safe accessors
    // for the values stored using that key:
    //
    // 	// Package user defines a User type that's stored in Contexts.
    // 	package user
    //
    // 	import "context"
    //
    // 	// User is the type of value stored in the Contexts.
    // 	type User struct {...}
    //
    // 	// key is an unexported type for keys defined in this package.
    // 	// This prevents collisions with keys defined in other packages.
    // 	type key int
    //
    // 	// userKey is the key for user.User values in Contexts. It is
    // 	// unexported; clients use user.NewContext and user.FromContext
    // 	// instead of using this key directly.
    // 	var userKey key
    //
    // 	// NewContext returns a new Context that carries value u.
    // 	func NewContext(ctx context.Context, u *User) context.Context {
    // 		return context.WithValue(ctx, userKey, u)
    // 	}
    //
    // 	// FromContext returns the User value stored in ctx, if any.
    // 	func FromContext(ctx context.Context) (*User, bool) {
    // 		u, ok := ctx.Value(userKey).(*User)
    // 		return u, ok
    // 	}
    Value(key interface{}) interface{}
}
```

# Background

Background 函數 返回一個 默認的 上下文 此上下文 沒有 取消信號/截至日期/其它值

通常作爲 測試/頂級 上下文

```go
package main

import (
	"context"
	"fmt"
	"sync"
)

// 此示例 將創建 5 個數字
func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	gen := func(ctx context.Context) <-chan int {
		dst := make(chan int)
		n := 1
		go func() {
			defer func() {
				close(dst)
				wg.Done()
			}()

			// <-ctx.Done() 永遠不會被執行 所以 加個條件 自己退出 以免 goroutine 泄漏
			for n < 6 { 
				select {
				case <-ctx.Done():
					// 永遠不會執行
					// 因爲 此 Context 永遠不會完成 所以 ctx.Done() 返回 nil
					return
				case dst <- n:
					n++
				}
			}
		}()
		return dst
	}

	// Background 創建一個 默認的 上下文
	ctx := context.Background()
	for n := range gen(ctx) {
		fmt.Println(n)
	}
	fmt.Println("success")
	wg.Wait()
}
```

# WithCancel

WithCancel 創建一個 帶有 退出信號的 上下文 一旦操作完成 應該立刻調用 cancel 函數 通知 工作 goroutine 退出

```go
package main

import (
	"context"
	"fmt"
	"sync"
)

// 此示例 將創建 5 個數字
func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	gen := func(ctx context.Context) <-chan int {
		dst := make(chan int)
		n := 1
		go func() {
			defer func() {
				close(dst)
				wg.Done()
			}()

			for { // 產生數字 直到 操作完成 不想要新數字爲止
				select {
				case <-ctx.Done():
					// 操作已完成 退出 goroutine
					return
				case dst <- n:
					n++
				}
			}
		}()
		return dst
	}

	// WithCancel 創建一個 帶退出信號的 上下文 和 CancelFunc 函數
	ctx, cancel := context.WithCancel(context.Background())
	for n := range gen(ctx) {
		fmt.Println(n)
		if n > 4 {
			break
		}
	}
	// 操作完成後 必須 並且應該 馬上 調用 CancelFunc 通知 處理 goroutine 操作已完成 以便其退出
	cancel()
	fmt.Println("success")
	wg.Wait()
}
```

# WithDeadline WithTimeout

WithTimeout 是 WithDeadline 的語法糖 會創建一個 帶 取消信號 和 截至日期 的上下文

當得到截至日期 如果 此時 操作還未完成(cancel 被調用) 則 設置操作完成 並將錯誤設置爲 DeadlineExceeded

```go
package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// 此示例 將創建 5 個數字
func main() {
	fmt.Println("*** test success ***")
	gen5(false)
	fmt.Println("*** test timeout ***")
	gen5(true)
}
func gen5(timeout bool) {
	var wg sync.WaitGroup
	wg.Add(1)
	gen := func(ctx context.Context) <-chan int {
		dst := make(chan int)
		n := 1
		go func() {
			defer func() {
				close(dst)
				wg.Done()
			}()
			timer := time.NewTimer(time.Second)

			for n < 6 { // 產生數字 [1,5] 直到 操作完成
				select {
				case <-ctx.Done():
					// 操作已完成 退出 goroutine
					if !timer.Stop() {
						<-timer.C
					}
					return
				case <-timer.C:
					select {
					case <-ctx.Done():
						// 操作已完成 退出 goroutine
						return
					case dst <- n:
						n++
						timer.Reset(time.Second)
					}
				}
			}
		}()
		return dst
	}

	var d time.Duration
	if timeout {
		d = time.Second*2 + time.Millisecond*500
	} else {
		d = time.Second * 10
	}
	// WithCancel 創建一個 帶退出信號的 上下文 和 CancelFunc 函數
	ctx, cancel := context.WithTimeout(context.Background(), d)
	for n := range gen(ctx) {
		fmt.Println(n)
	}
	// 操作完成後 必須(並且應該立刻) 調用 CancelFunc 通知 處理 goroutine 操作已完成 以便其退出
	cancel()

	// 驗證 錯誤 是否有錯誤 此示例只會是 超時
	e := ctx.Err()
	if e == context.Canceled { // 不是 context.Canceled 是操作 發送錯誤
		fmt.Println("success", e)
	} else {
		fmt.Println(e)
	}
	wg.Wait()
}
```

# WithValue

WithValue 爲上下文 設置 Value 信息

```go
package main

import (
	"context"
	"fmt"
)

func main() {
	var k, v string
	k = "kate"
	v = "kate is my love"
	ctx := context.WithValue(context.Background(), k, v)
	k = "king"
	v = "i'm king"
	ctx = context.WithValue(ctx, k, v)
	fmt.Println(ctx.Value("kate"))
	fmt.Println(ctx.Value("king"))
}
```