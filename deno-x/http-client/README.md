# [http_client](https://deno.land/x/http_client@v0.0.3)

fetch 雖然提供了瀏覽器兼容的 http client，但是它對於一些特殊用途無能爲力， http\_client 提供了以 Deno.Conn 作爲 http 客戶端的能力

```
import {
  HttpClient,
  HttpMethod,
  HttpRequest,
} from "https://deno.land/x/http_client/mod.ts";

// Docker socket
const conn = await Deno.connect({
  path: "/var/run/docker.sock",
  transport: "unix",
});

// Instanciates the HttpClient
const client = new HttpClient(conn);

// Creates a HttpRequest
const request = new HttpRequest(HttpMethod.GET, "/_ping", {
  "Host": "localhost",
  "Accept": "application/json",
});

// Sends the request
const response = await client.request(request);

console.log(response);
```