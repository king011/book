# 多線程

c++11 提供了多線程相關的基礎組件

```
#include <thread>
```
# std::thread

std::thread 是一個不可拷貝的對象 用於創建一個線程 std::thread 的成員函數提供了對線程的一些操作其中最常用的是:

* join 等待線程執行結束
* detach 分離 thread 和線程 

通常必須調用 join 或 detach 當 thread析構時會釋放一些線程資源但如果此時線程未執行結束 可能會引發未定義行爲，如果不想等待線程執行結束 就可以調用 detach 分離 thread 和線程 

```
#include <iostream>
#include <thread>

int main(int argc, char *argv[])
{
    std::cout << "main id: " << std::this_thread::get_id() << std::endl;
    std::thread t([]
                  {
                      std::this_thread::sleep_for(std::chrono::seconds(1));
                      std::cout << "work id: " << std::this_thread::get_id() << std::endl;
                  });
    std::cout << t.joinable() << std::endl;
    t.join();
    std::cout << t.joinable() << std::endl;
    return 0;
}
```

# 鎖
標準庫 提供了 四種基本的 鎖

* std::mutex 互斥鎖 不能遞歸使用
* std::timed_mutex 類似 mutex 並且帶有超時功能
* std::recursive_mutex 互斥鎖 可重入
* std::recursive_timed_mutex 類似 recursive_mutex 並且帶有超時功能

# lock 
標準庫 提供了兩個類型 用於 RAII 方式加解鎖

* std::unique_lock
* std::lock_guard

lock_guard 比 unique_lock 更加輕量 但少了手動 解鎖的功能

```
#include <iostream>
#include <thread>
#include <mutex>
int main(int argc, char *argv[])
{
    std::mutex m;
    size_t x = 0;
    for (size_t i = 0; i < 100; i++)
    {
        std::thread([&m = m, &x = x, i = i]
                    {
                        std::lock_guard lock(m);
                        x += i;
                    })
            .detach();
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::cout << x << std::endl;
    return 0;
}
```

# std::atomic

std::atomic 模板類 提供了 原子操作

```
#include <iostream>
#include <thread>
#include <atomic>
int main(int argc, char *argv[])
{
    std::atomic<int> x = 0;
    for (size_t i = 0; i < 100; i++)
    {
        std::thread([&x = x, i = i]
                    { x += i; })
            .detach();
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::cout << x << std::endl;
    return 0;
}
```

# std::call_once

std::call\_once 需要和 std::once\_flag 一起使用 保證代碼只執行一次(並且未執行的線程會等待執行結束) 通常用於在工作線程中執行一些初始化代碼

```
#include <iostream>
#include <thread>
#include <mutex>
int main(int argc, char *argv[])
{
    std::once_flag flag;
    size_t x = 0;
    for (size_t i = 0; i < 5; i++)
    {
        std::thread([&flag = flag, i = i]
                    {
                        std::cout << "enter :" << i << std::endl;
                        std::call_once(flag, []
                                       {
                                           std::this_thread::sleep_for(std::chrono::seconds(1));
                                           puts("once");
                                       });
                        std::cout << "exit :" << i << std::endl;
                    })
            .detach();
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1100));
    return 0;
}
```

# std::condition\_variable 條件變量

std::condition\_variable 需要和 std::unique\_lock 一起使用 它可以阻塞一個線程 直到等待通知 或超時 才喚醒

* wait 可以 掛起線程 直達 得到一個 notify\_one/notify\_all 通知
* wait\_for/wait\_until 帶超時的等待
* notify\_one 喚醒一個 wait 線程
* notify\_all 喚醒所有 wait 線程

1. waitXXX 函數 會解鎖 unique\_lock 並且在返回前重新 lock
2. notifyXXX 函數不會對鎖進行修改

```
#include <iostream>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>
#include <vector>
#include <algorithm>
struct notify_on_exit final
{
private:
    std::condition_variable &cv_;

public:
    notify_on_exit(notify_on_exit &) = delete;
    notify_on_exit &operator=(notify_on_exit &) = delete;
    notify_on_exit(std::condition_variable &cv) noexcept : cv_(cv)
    {
    }
    inline ~notify_on_exit() noexcept
    {
        cv_.notify_one();
    }
};
/// like go make(chan,1)
template <typename T>
class channel_t final
{
private:
    std::future<T> future_;
    std::mutex m_;
    std::condition_variable get_;
    std::condition_variable put_;

public:
    typedef T type;
    channel_t() = default;
    ~channel_t() = default;
    channel_t(channel_t &) = delete;
    channel_t &operator=(channel_t &) = delete;

    T get()
    {
        notify_on_exit notify(put_); // 喚醒 put

        std::unique_lock<std::mutex> lock(m_);
        while (!future_.valid()) // 沒有數據
        {
            get_.wait(lock); // 等待數據
        }
        // 返回數據
        return future_.get();
    }
    void put(T &&v)
    {
        notify_on_exit notify(get_); // 喚醒 get

        std::unique_lock<std::mutex> lock(m_);
        while (future_.valid()) // 數據未消化
        {
            put_.wait(lock); // 等待數據被消化
        }
        // 寫入數據
        future_ = std::async(std::launch::deferred, [v = std::move(v)]
                             { return v; });
    }
};

int main(int argc, char *argv[])
{
    channel_t<int> ch;
    ch.put(1);
    std::cout << ch.get() << std::endl;

    size_t count = 10;
    for (size_t i = 0; i < count; i++)
    {
        std::thread([&ch = ch, i = i]
                    { ch.put(i); })
            .detach();
    }
    std::vector<int> vs;
    for (size_t i = 0; i < count; i++)
    {
        auto v = ch.get();
        vs.push_back(v);
    }
    std::sort(vs.begin(), vs.end());
    for (auto v : vs)
    {
        std::cout << v << " ";
    }
    std::cout << std::endl;
    return 0;
}
```