# 安裝

推薦安裝 windows 程式，對於 linux 也可以考慮使用 wine 運行 windows 程式，因爲不同版本的 mame 其 roms 之間是不兼容的，而 mame 官方沒有提供 linux 的預編譯程式對於 linux 用戶通常只能安裝系統軟件包提供的版本或者自己編譯。處於省心考慮在 linux 下使用 wine 會簡單並更容易收集 roms 並且因爲街機遊戲相對現在的 cpu 和 gpu 來說幾乎沒有任何壓力所以使用 wine 也不用擔心效率折損


# windows

對於 windows 提供了預編譯包

1. 從官網下載 [最新版本的壓縮包](https://www.mamedev.org/release.html) 或者 [需要版本的壓縮包](https://www.mamedev.org/oldrel.html)
2. 雙擊運行壓縮包並根據提示解壓到要安裝的路徑

# ubuntu

```
sudo apt install mame
```

**/etc/mame/mame.ini** 是配置檔案，裏面最重要的設定就是指定了 rom 目錄

```
$ cat /etc/mame/mame.ini |egrep rompath
rompath                 $HOME/mame/roms;/usr/local/share/games/mame/roms;/usr/share/games/mame/roms
```

將遊戲 rom 拷貝到 roms 目錄下即可，此外如果 rom 需要特定的 BIOS 將 BIOS 和 rom 放到一起即可

# android

seleuco 提供了開源的 android 版本可以在 android 上方便的使用 mame

* 源碼 [MAME4droid (0.139u1)](https://github.com/seleuco/MAME4droid-0.139u1-)
* 源碼 [MAME4droid 2024 (0.263)](https://github.com/seleuco/MAME4droid-2024)
* playstore [MAME4droid 2024 (0.263)](https://play.google.com/store/apps/details?id=com.seleuco.mame4droid&hl=en_US)
* playstore [MAME4droid 2024 (0.263)](https://play.google.com/store/apps/details?id=com.seleuco.mame4d2024&hl=en_US)