# 備份

bolt 數據庫 就是一個當然 直接備份 檔案即可

然 bolt 亦提供了幾個 api 用於在 數據庫打開時 備份

```
// 備份到 檔案
func (tx *Tx) CopyFile(path string, mode os.FileMode) error

// 備份到 io.Writer
func (tx *Tx) WriteTo(w io.Writer) (n int64, err error)
```