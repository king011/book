# 異步

c++ 提供了一些輔助工具用來簡化 異步編程

```
#include <future>
```

# std::future std::promise

* std::future 提供了一種獲取異步結果的標準方法
* std::promise 提供了一種設置異步結果的標準方法

future 和 promise 都是不可拷貝的 並且 一個 promise 始終與一個 future 對應

```
#include <iostream>
#include <thread>
#include <future>
int main(int argc, char *argv[])
{
    // 創建一個承諾用於異步返回結果
    std::promise<int> promise;
    // 獲取 future 用於得到結果
    auto future = promise.get_future();
    std::thread([&promise = promise]
                {
                    try
                    {
                        // promise.set_value_at_thread_exit(1);
                        throw std::runtime_error("test");
                    }
                    catch (...)
                    {
                        promise.set_exception_at_thread_exit(std::current_exception());
                    }
                })
        .detach();
    try
    {
        std::cout << future.get() << std::endl;
    }
    catch (const std::exception &e)
    {
        std::cerr << "err: " << e.what() << '\n';
    }
    return 0;
}
```

future promise 有一些需要注意的點
1. future 獲取值 與 promise 設置值不能位於相同線程
2. future 只能get一次值 promise 也只能設置一次值或異常 否則將引發異常
3. 可以在 get 前 調用 valid 判斷 future 是否有效

future 除了 get 也提供了對超時的支持

```
#include <iostream>
#include <thread>
#include <future>
int main(int argc, char *argv[])
{
    // 創建一個承諾用於異步返回結果
    std::promise<int> promise;
    // 獲取 future 用於得到結果
    auto future = promise.get_future();
    std::thread([&promise = promise]
                {
                    std::this_thread::sleep_for(std::chrono::seconds(1));
                    promise.set_value_at_thread_exit(1);
                })
        .detach();

    for (;;)
    {
        auto status = future.wait_for(std::chrono::milliseconds(100));
        switch (status)
        {
        case std::future_status::ready:
            std::cout << future.get() << std::endl;
            return 0;
        case std::future_status::timeout:
            puts("timeout");
            break;
        case std::future_status::deferred:
            puts("deferred");
            break;
        }
    }
    return 0;
}
```
# std::packaged_task
std::packaged\_task 是 std::promise 的進異步封裝 

std::packaged\_task 創建了一個防函數內部創建了一個std::promis 並且packaged_task同樣不可拷貝

當 std::packaged\_task 調用後 會將函數返回值或異常設置到內部的 std::promis


```
#include <iostream>
#include <thread>
#include <future>
#include <functional>
int main(int argc, char *argv[])
{
    // 創建一個任務
    std::packaged_task<int(int, int)> task([](int a, int b)
                                           {
                                               std::this_thread::sleep_for(std::chrono::seconds(1));
                                               return a + b;
                                           });
    auto future = task.get_future();

    // 異步執行
    std::thread(std::bind(std::move(task), 1, 2)).detach();

    // 返回結果
    std::cout << future.get() << std::endl;
    return 0;
}
```
# std::async

std::async 將線程啓動和 std::packaged_task 封裝到了一起 

```
#include <iostream>
#include <thread>
#include <future>
#include <functional>
int main(int argc, char *argv[])
{
    std::cout << std::this_thread::get_id() << std::endl;
    auto future = std::async(std::launch::async, []
                         {
                             std::cout << std::this_thread::get_id() << std::endl;
                             std::this_thread::sleep_for(std::chrono::seconds(1));
                             return 123;
                         });
    std::cout << future.get() << std::endl;
    return 0;
}
```

注意 std::async 和自己啓動線程有點區別 std::async 返回的 future 如果沒有顯示調用 get 則其在析構時會 阻塞當前線程等待 async 執行完畢

std::async 第一個參數可以指定 如何執行任務

* std::launch::async 立刻創建線程並執行
* std::launch::deferred 在調用 future.get 或 future.wait 時才執行(在調用處同步執行 不會新啓線程)
* 如果不傳入啓動策略或者傳入 std::launch::async|std::launch::deferred 則系統會依據情況決定是否新啓線程(不建議這麼使用 會讓代碼執行不可控)

