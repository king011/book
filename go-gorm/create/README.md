# 創建記錄

```
user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}

result := db.Create(&user) // 提供數據指針來創建

user.ID             // 返回插入數據的主鍵
result.Error        // 返回 error
result.RowsAffected // 返回插入記錄的條數
```

# 用指定字段創建記錄
創建記錄 並更新給出的字段

```
db.Select("Name", "Age", "CreatedAt").Create(&user)
// INSERT INTO `users` (`name`,`age`,`created_at`) VALUES ("jinzhu", 18, "2020-07-04 11:05:21.775")
```

創建記錄並更新未給出的字段
```
db.Omit("Name", "Age", "CreatedAt").Create(&user)
// INSERT INTO `users` (`birthday`,`updated_at`) VALUES ("2020-01-01 00:00:00.000", "2020-07-04 11:05:21.775")
```

# 批量插入
要有效的插入大量記錄 將 slice 傳遞給 create, gorm 將生成一個單一的 SQL語句來插入所有記錄，並回填主鍵值，構造方法也會被調用

```
var users = []User{{Name: "jinzhu1"}, {Name: "jinzhu2"}, {Name: "jinzhu3"}}
db.Create(&users)

for _, user := range users {
  user.ID // 1,2,3
}
```

使用 CreateInBatches 創建時，還可以指定 創建的數量
```
var users = []User{name: "jinzhu_1"}, ...., {Name: "jinzhu_10000"}}

// 數量爲 100
db.CreateInBatches(users, 100)
```
> 使用 CreateBatchSize 選項初始化 gorm 時 ，所有的創建 & 關聯 INSERT 都將遵循該選項

```
db, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{
  CreateBatchSize: 1000,
})

db := db.Session(&gorm.Session{CreateBatchSize: 1000})

users = [5000]User{{Name: "jinzhu", Pets: []Pet{pet1, pet2, pet3}}...}

db.Create(&users)
// INSERT INTO users xxx (5 batches)
// INSERT INTO pets xxx (15 batches)
```

# 創建鉤子

gorm 允許用戶定義的鉤子有 BeforeSave, BeforeCreate, AfterSave, AfterCreate 創建記錄時將調用這些鉤子方法

```
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
  u.UUID = uuid.New()

    if u.Role == "admin" {
        return errors.New("invalid role")
    }
    return
}
```

如果向跳過 鉤子 方法 可以使用 SkipHooks 會話模式

```
DB.Session(&gorm.Session{SkipHooks: true}).Create(&user)

DB.Session(&gorm.Session{SkipHooks: true}).Create(&users)

DB.Session(&gorm.Session{SkipHooks: true}).CreateInBatches(users, 100)
```

# 根據 map 創建

gorm 支持 根據 map[string]interface{} 和 []map[string]interface{} 創建記錄

```
db.Model(&User{}).Create(map[string]interface{}{
  "Name": "jinzhu", "Age": 18,
})

// batch insert from `[]map[string]interface{}{}`
db.Model(&User{}).Create([]map[string]interface{}{
  {"Name": "jinzhu_1", "Age": 18},
  {"Name": "jinzhu_2", "Age": 20},
})
```

> 使用 map 創建記錄時 association 不會被調用 且主鍵也不會被自動填充

# 使用 SQL 表達式 Context Valuer 創建記錄

gorm 允許使用 SQL 表達式 插入數據 ，有兩種方法實現這個目標。根據 map[string]interface{} 或 自定義數據 創建
```
// 通過 map 創建記錄
db.Model(User{}).Create(map[string]interface{}{
  "Name": "jinzhu",
  "Location": clause.Expr{SQL: "ST_PointFromText(?)", Vars: []interface{}{"POINT(100 100)"}},
})
// INSERT INTO `users` (`name`,`location`) VALUES ("jinzhu",ST_PointFromText("POINT(100 100)"));

// 通過 自定義型別 創建記錄
type Location struct {
    X, Y int
}

// Scan 方法實現 sql.Scanner 接口
func (loc *Location) Scan(v interface{}) error {
  // Scan a value into struct from database driver
}

func (loc Location) GormDataType() string {
  return "geometry"
}

func (loc Location) GormValue(ctx context.Context, db *gorm.DB) clause.Expr {
  return clause.Expr{
    SQL:  "ST_PointFromText(?)",
    Vars: []interface{}{fmt.Sprintf("POINT(%d %d)", loc.X, loc.Y)},
  }
}

type User struct {
  Name     string
  Location Location
}

db.Create(&User{
  Name:     "jinzhu",
  Location: Location{X: 100, Y: 100},
})
// INSERT INTO `users` (`name`,`location`) VALUES ("jinzhu",ST_PointFromText("POINT(100 100)"))
```

# 高級選項

## 關聯創建

創建關聯數據時 如果關聯值是非零值，這些關聯會被 upset 且它們的 Hook 方法也會被調用

```
type CreditCard struct {
  gorm.Model
  Number   string
  UserID   uint
}

type User struct {
  gorm.Model
  Name       string
  CreditCard CreditCard
}

db.Create(&User{
  Name: "jinzhu",
  CreditCard: CreditCard{Number: "411111111111"}
})
// INSERT INTO `users` ...
// INSERT INTO `credit_cards` ...
```

可以通過 Select Omit 跳過關聯保存 例如:
```
db.Omit("CreditCard").Create(&user)

// 跳過所有關聯
db.Omit(clause.Associations).Create(&user)
```

## 默認值

可以通過 標籤 default 爲字段定義默認值
```
type User struct {
  ID   int64
  Name string `gorm:"default:galeone"`
  Age  int64  `gorm:"default:18"`
}
```

插入記錄時 默認值會被用於 填充爲 零值 的 字段

像 0 '' false 等零值不會將這些字段的默認值保存到 數據庫。需要使用 指針型別或 Scanner/Valuer 來避免此問題

```
type User struct {
  gorm.Model
  Name string
  Age  *int           `gorm:"default:18"`
  Active sql.NullBool `gorm:"default:true"`
}
```

若要數據庫有默認 虛擬/生成的值 必須爲字段設置 default 標籤。若要在遷移時 跳過默認值定義 可以使用 default:(-) ,例如:
```
type User struct {
  ID        string `gorm:"default:uuid_generate_v3()"` // 數據庫函數
  FirstName string
  LastName  string
  Age       uint8
  FullName  string `gorm:"->;type:GENERATED ALWAYS AS (concat(firstname,' ',lastname));default:(-);`
}
```

使用 虛擬/生成 的值時，可能需要禁用它的 創建 更新 權限

# Upsert 及衝突

gorm 爲不同數據庫 提供了 兼容的 Upset 支持

```
import "gorm.io/gorm/clause"

// 在衝突時，什麼都不做
db.Clauses(clause.OnConflict{DoNothing: true}).Create(&user)

// 在`id`衝突時，將列更新爲默認值
db.Clauses(clause.OnConflict{
  Columns:   []clause.Column{{Name: "id"}},
  DoUpdates: clause.Assignments(map[string]interface{}{"role": "user"}),
}).Create(&users)
// MERGE INTO "users" USING *** WHEN NOT MATCHED THEN INSERT *** WHEN MATCHED THEN UPDATE SET ***; SQL Server
// INSERT INTO `users` *** ON DUPLICATE KEY UPDATE ***; MySQL

// 使用SQL語句
db.Clauses(clause.OnConflict{
  Columns:   []clause.Column{{Name: "id"}},
  DoUpdates: clause.Assignments(map[string]interface{}{"count": gorm.Expr("GREATEST(count, VALUES(count))")}),
}).Create(&users)
// INSERT INTO `users` *** ON DUPLICATE KEY UPDATE `count`=GREATEST(count, VALUES(count));

// 在`id`衝突時，將列更新爲新值
db.Clauses(clause.OnConflict{
  Columns:   []clause.Column{{Name: "id"}},
  DoUpdates: clause.AssignmentColumns([]string{"name", "age"}),
}).Create(&users)
// MERGE INTO "users" USING *** WHEN NOT MATCHED THEN INSERT *** WHEN MATCHED THEN UPDATE SET "name"="excluded"."name"; SQL Server
// INSERT INTO "users" *** ON CONFLICT ("id") DO UPDATE SET "name"="excluded"."name", "age"="excluded"."age"; PostgreSQL
// INSERT INTO `users` *** ON DUPLICATE KEY UPDATE `name`=VALUES(name),`age=VALUES(age); MySQL

// 在衝突時，更新除主鍵外的所有列到新值。
db.Clauses(clause.OnConflict{
  UpdateAll: true,
}).Create(&users)
// INSERT INTO "users" *** ON CONFLICT ("id") DO UPDATE SET "name"="excluded"."name", "age"="excluded"."age", ...;
```