# command

deno 所有的命令都集中在 deno 程序中，你可以使用 --help 查看使用方法

```
deno help
deno -h
deno --help
```

```
deno help bundle
deno bundle -h
deno bundle --help
```

# run

run 用來執行腳本，可以使用從 url 本地檔案 或 '-' 標記從 stdin 讀取

```
deno run main.ts
deno run https://mydomain.com/main.ts
deno run -r https://mydomain.com/main.ts
cat main.ts | deno run -
```

* **-r** 參數可以重載代碼緩存

## Script arguments

所有檔案名後的選項都會作爲參數被傳遞給腳本，你可以使用 Deno.args 來獲取這些參數

```
deno run main.ts a b -c --quiet
```

```
// main.ts
console.log(Deno.args); // [ "a", "b", "-c", "--quiet" ]
```

所以傳遞給 deno 的參數要寫在檔案名稱之前

## Watch mode

run test bundle 等命令都支持一個 --watch 選項，用於監控本地源碼在其變化後自動執行對應命令

```
deno run --watch main.ts
deno test --watch
deno fmt --watch
```

## Integrity flags (lock files)

一些指令會將資源下載到緩存
* deno cache
* deno run
* deno test
* deno bundle
* deno doc
* deno compile

如果遠程資源發送發生了變化 deno 不會知道，你可以傳入 `--lock=lock.json --lock-write` 來告訴 deno 使用一個小型的 json 檔案來記錄遠程資源的 hash 值

```
--lock <FILE>    Check the specified lock file
--lock-write     Write lock file. Use with --lock.
```

## Cache and compilation flags

一些指令會將影響緩存
* deno cache
* deno run
* deno test
* deno bundle
* deno doc
* deno compile

```
--config <FILE>               Load configuration file
--import-map <FILE>           Load import map file
--no-remote                   Do not resolve remote modules
--reload=<CACHE_BLOCKLIST>    Reload source code cache (recompile TypeScript)
--unstable                    Enable unstable APIs
```

## Type checking flags

你可以使用 check 指令來檢查你的代碼

```
deno check main.ts
```

```
deno run --check main.ts
```

## [Permission flags](https://deno.land/manual@v1.28.1/basics/permissions#permissions-list)

deno 中訪問資源是受限的，如果要訪問特殊資源你需要明確的指定權限

* --allow-env=&lt;allow\-env&gt; Allow environment access for things like getting and setting of environment variables. Since Deno 1.9, you can specify an optional, comma-separated list of environment variables to provide an allow-list of allowed environment variables.
* --allow-sys=&lt;allow\-sys&gt; Allow access to APIs that provide information about user's operating system, eg. Deno.osRelease() and Deno.systemMemoryInfo().
* --allow-hrtime Allow high-resolution time measurement. High-resolution time can be used in timing attacks and fingerprinting.
* --allow-net=&lt;allow\-net&gt; Allow network access. You can specify an optional, comma-separated list of IP addresses or hostnames (optionally with ports) to provide an allow-list of allowed network addresses.
* --allow-ffi Allow loading of dynamic libraries. Be aware that dynamic libraries are not run in a sandbox and therefore do not have the same security restrictions as the Deno process. Therefore, use with caution. Please note that --allow-ffi is an unstable feature.
* --allow-read=&lt;allow\-read&gt; Allow file system read access. You can specify an optional, comma-separated list of directories or files to provide an allow-list of allowed file system access.
* --allow-run=&lt;allow\-run&gt; Allow running subprocesses. Since Deno 1.9, You can specify an optional, comma-separated list of subprocesses to provide an allow-list of allowed subprocesses. Be aware that subprocesses are not run in a sandbox and therefore do not have the same security restrictions as the Deno process. Therefore, use with caution.
* --allow-write=&lt;allow\-write&gt; Allow file system write access. You can specify an optional, comma-separated list of directories or files to provide an allow-list of allowed file system access.
* -A, --allow-all Allow all permissions. This enables all security sensitive functions. Use with caution.

# Configuration File

從 v1.18 開始 deno 會自動從當前目錄向上查找 deno.json/deno.jsonc 配置當阿，此外你也可以使用 --config 來明確指定配置檔案路徑

```
{
  "compilerOptions": {
    "allowJs": true,
    "lib": ["deno.window"],
    "strict": true
  },
  "importMap": "import_map.json",
  "lint": {
    "files": {
      "include": ["src/"],
      "exclude": ["src/testdata/"]
    },
    "rules": {
      "tags": ["recommended"],
      "include": ["ban-untagged-todo"],
      "exclude": ["no-unused-vars"]
    }
  },
  "fmt": {
    "files": {
      "include": ["src/"],
      "exclude": ["src/testdata/"]
    },
    "options": {
      "useTabs": true,
      "lineWidth": 80,
      "indentWidth": 4,
      "singleQuote": true,
      "proseWrap": "preserve"
    }
  },
  "test": {
    "files": {
      "include": ["src/"],
      "exclude": ["src/testdata/"]
    }
  }
}
```