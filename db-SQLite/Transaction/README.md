# [Transaction](https://www.sqlite.org/lang_transaction.html)

除非在 Transaction 內，否則不會發生讀寫。任何訪問數據庫的命令(除了一些 PRAGMA 語句)，如果 Transaction 尚未生效都將自動啓動一個 Transaction。自動啓動的 Transaction 在最後一個 SQL 語句完成時被提交。

可以使用 BEGIN 命令手動啓動 Transaction。手動啓動的 Transaction 會持續到下一個 COMMIT 或 ROLLBACK 命名被執行爲止。但是如果數據庫關閉，Transaction 也會被 ROLLBACK。此外如果發生了錯誤但指定了 CONFLICT 算法爲 ROLLBACK 同樣 Transaction 也會被 ROLLBACK。

使用 BEGIN 創建的 Transaction 不能嵌套。對於嵌套 Transaction 可以使用 SAVEPOINT。嘗試在 Transaction 中嵌套調用 BEGIN 命令將會發生錯誤，無論外部 Transaction 是由 BEGIN 還是 SAVEPOINT 啓動的。不帶 TO 子句的 COMMI/ROLLBACK 命令對 SAVEPOINT 啓動的 Transaction 作用與由 BEGIN 啓動的 Transaction 的作用相同。

# 讀寫 

SQLite 支持來自不同數據庫連接的多個同時讀取 Transaction，它們可以在不同的線程或進程中，但只支持同時一個寫入 Transaction

讀取 Transaction 只用於讀取，寫入 Transaction 則允許讀寫。讀取 Transaction 由 SELECT 語句啓動，寫入 Transaction 由 CREATE/DELETE/DROP/INSERT/UPDAT 等“寫入語句”啓動。如果在讀取 Transaction 處於獲得狀態時發生寫入語句，則如果可能讀取 Transaction 升級爲寫入 Transaction。如果其它一些連接已經修改了數據庫或者已經在修改數據庫中，那麼升級到寫入 Transaction 是不可能的，並且寫入語句將失敗並返回 SQLITE_BUSY

當讀取 Transaction 處於獲得狀態時，由單獨的數據庫連接實現的對數據庫的任何更改將不會被啓動的讀取 Transaction 的數據庫連接看到。如果數據庫連接 X 持有一個讀取 Transaction，那麼當 X 的 Transaction 仍然打開時，其它一些數據庫連接 Y 可能會更改數據庫內容，但是 X 將無法看到這些更改，直到 Transaction 結束。當其讀取 Transaction 處於獲得狀態時，X 將繼續查看 Y 實施更改之前的數據庫歷史快照。

# DEFERRED IMMEDIATE EXCLUSIVE

BEGIN 默認以 DEFERRED 模式啓動，你也可以顯示指定模式

```
BEGIN DEFERRED
```

* **DEFERRED** 表示直到第一次訪問數據庫時 Transaction 才真正開始，在內部 BEGIN DEFERRED 語句只是在數據庫連接上設置一個標誌，連接關閉通常在最後一個語句完成時發生自動提交。這會導致自動啓動的 Transaction 一直持續到顯示的 COMMIT/ROLLBACK 或直到錯誤或 ON CONFLICT ROLLBACK 子句引發回滾。如果 BEGIN DEFERRED 之後的第一條語句是 SELECT，那麼將啓動讀取 Transaction。如果可能後續的寫入語句會將 Transaction 升級爲寫入 Transaction，否則返回 SQLITE_BUSY 錯誤。如果 BEGIN DEFERRED 之後的第一條語句是寫入語句，那麼將啓動一個寫入 Transaction。
* **IMMEDIATE** 將導致連接立刻開始新的寫入，而不是等待寫入語句。如果另外一個寫入 Transaction 已經在另外一個連接上處於獲得狀態， BEGIN IMMEDIATE 可能會失敗並返回 SQLITE_BUSY
* **EXCLUSIVE** 與 IMMEDIATE 相似，立刻開始寫入 Transaction。EXCLUSIVE 和 IMMEDIATE 在 WAL 模式下是一樣的，但在其它日誌模式下 EXCLUSIVE 會阻止其它連接在 Transaction 進行時讀取數據庫

# 隱式與顯示 Transaction

隱式 Transaction (自動啓動的 Transaction) 在最後一個獲得語句完成時自動提交。一條語句在其最後一個遊標關閉時結束，這保證在準備好的語句被重置或完成時發生。處於 Transaction 控制的目的，某些語句可能會在重置或最終確定之前“完成”，但不能保證這一點。確保語句“完成”的唯一方法是對該語句調用 sqlite3\_reset 或 sqlite3\_finalize。用於增量 BLOB I/O 的打開的 sqlite3\_blob 也算作未完成的語句。sqlite3\_blob 在關閉時完成

顯示 COMMIT 命令會立即執行，即時有未決的 SELECT 語句頁數如此，但是如果有掛起的寫操作，COMMIT 命名將失敗，錯誤代碼爲 SQLITE_BUSY

# 錯誤
如果 Transaction 中發生某些類型的錯誤，Transaction 可能會自動回滾，也可能不會自動回滾:

* **SQLITE\_FULL** 數據庫或磁盤已滿
* **SQLITE\_IOERR** 磁盤 I/O 錯誤
* **SQLITE\_BUSY** 另一個進程正在使用數據庫
* **SQLITE\_NOMEM** 內存不足

對於所有這些錯誤，SQLite 會嘗試僅撤銷它正在處理的一條語句，並保持同一 Transaction 中先前的語句的更改不變，然後繼續該 Transaction。但是，根據正在評估的語句和錯誤發生的時間點，SQLite 可能需要回滾並取消整個 Transaction。應用程序可以通過使用 sqlite3\_get\_autocommit 函數來判斷 SQLite 執行的操作過程

建議應用程序通過顯示發出 ROLLBACK 命令來響應上面列出的錯誤，如果 Transaction 已經被錯誤響應自動回滾，那麼 ROLLBACK 命令將失敗並出現錯誤，但這不會造成任何危害

SQLite 的未來版本可能會擴展可能導致自動回滾的錯誤列表。SQLite 未來版本可能會更改錯誤響應。特別是，我們可能會選擇通過導致上述錯誤強制無條件回滾來簡化 SQLite 未來版本中的接口