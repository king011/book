# 前端代理

這個例子是一個前端代理，它以及 http2 的 **:authority** 來獲取 請求的 host 標頭將服務分發到 後端的 google 或 bing

```
admin:
  access_log_path: "/dev/stdout"
  address:
    socket_address: { address: 0.0.0.0, port_value: 8080 } # 管理節目
static_resources:
  listeners:
    - address:
        socket_address: { address: 0.0.0.0, port_value: 80 } # 監聽器工作在 80 端口
      filter_chains:
        - filters:
            - name: envoy.filters.network.http_connection_manager
              typed_config:
                "@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
                codec_type: AUTO
                stat_prefix: ingress_http # 統計時的前綴
                access_log:
                    - name: envoy.access_loggers.stdout
                      typed_config:
                          "@type": type.googleapis.com/envoy.extensions.access_loggers.stream.v3.StdoutAccessLog
                route_config:
                  name: search_route
                  virtual_hosts: # 路由配置
                    - name: service
                      domains: ["*"]
                      routes:
                        - match: # 匹配規則
                            prefix: "/"
                            headers:
                              - name: ":authority"
                                string_match:
                                  exact: "google.com"
                          route:
                            cluster: google # 轉發集羣
                            host_rewrite_literal: www.google.com # 重寫 host
                        - match: # 匹配規則
                            prefix: "/"
                            headers:
                              - name: ":authority"
                                string_match:
                                  exact: "bing.com"
                          route:
                            cluster: bing # 轉發集羣
                            host_rewrite_literal: www.bing.com # 重寫 host
                http_filters:
                  - name: envoy.filters.http.router
                    typed_config:
                      "@type": type.googleapis.com/envoy.extensions.filters.http.router.v3.Router
          # transport_socket: # https
          #   name: envoy.transport_sockets.tls
          #   typed_config:
          #     "@type": type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.DownstreamTlsContext
          #     common_tls_context:
          #       alpn_protocols: ["h2", "http/1.1"]
          #       tls_certificates:
          #         - certificate_chain:
          #             filename: "/etc/envoy/x509.crt"
          #           private_key:
          #             filename: "/etc/envoy/x509.key"
  clusters:
    - name: google
      connect_timeout: 1s
      type: LOGICAL_DNS
      dns_lookup_family: V4_ONLY
      lb_policy: ROUND_ROBIN
      load_assignment:
        cluster_name: google
        endpoints:
          - lb_endpoints:
              - endpoint:
                  address:
                    socket_address: { address: www.google.com, port_value: 443 }
      transport_socket: # 後端爲 https，如果爲 http 可以省略此項目
        name: envoy.transport_sockets.tls
        typed_config:
          "@type": type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.UpstreamTlsContext
    - name: bing
      connect_timeout: 1s
      type: LOGICAL_DNS
      dns_lookup_family: V4_ONLY
      lb_policy: ROUND_ROBIN
      load_assignment:
        cluster_name: bing
        endpoints:
          - lb_endpoints:
              - endpoint:
                  address:
                    socket_address: { address: www.bing.com, port_value: 443 }
      transport_socket: # 後端爲 https，如果爲 http 可以省略此項目
        name: envoy.transport_sockets.tls
        typed_config:
          "@type": type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.UpstreamTlsContext
```

```
curl -H "Host: google.com" 127.0.0.1 -v
```

```
curl -H "Host: bing.com" 127.0.0.1 -v
```

# grpc

grpc 實際是 http2 協議，envoy 可以直接支持，只需要將上游集羣設置爲 http2 即可

```
clusters:
	- name: XXX
		connect_timeout: 1s
		type: STRICT_DNS
		lb_policy: ROUND_ROBIN
		http2_protocol_options: {}
		load_assignment:
```
在集羣設置加上 **http2\_protocol\_options** envoy 就會使用 http2 連接上游，否則默認使用 http1.1連接上游

# websocket

在 **listeners\.filter\_chains\.filters\.typed\_config\.upgrade\_configs** 中配置 **upgrade_type: websocket** 以支持 websocket，但是如果後端集羣使用了http2 則 envoy v1.22.5 無法正常工作，所以建議將 websocket 單獨設置後端集羣並禁用上游 http2

```
- filters:
		- name: envoy.filters.network.http_connection_manager
			typed_config:
				"@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
				codec_type: AUTO
				stat_prefix: ingress_ws
				upgrade_configs:
					- upgrade_type: websocket
				route_config:
```