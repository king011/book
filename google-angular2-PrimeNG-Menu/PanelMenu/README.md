# [PanelMenu](https://primeng.org/panelmenu)

```
import {PanelMenuModule} from 'primeng/panelmenu';
import {MenuItem} from 'primeng/api';
```

PanelMenu 是 [TieredMenu](https://primeng.org/tieredmenu) 和 [Accordion](https://primeng.org/accordion) 的組合效果(以手風琴的風格顯示嵌套菜單)

![](assets/primeng.org_menubar.png)

```
<p-panelMenu [model]="items" [style]="{'width':'300px'}"></p-panelMenu>
```

```
export class PanelMenuDemo {
  
    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                icon: 'pi pi-pw pi-file',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'User', icon: 'pi pi-fw pi-user-plus'},
                            {label: 'Filter', icon: 'pi pi-fw pi-filter'}
                        ]
                    },
                    {label: 'Open', icon: 'pi pi-fw pi-external-link'},
                    {separator: true},
                    {label: 'Quit', icon: 'pi pi-fw pi-times'}
                ]
            },
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-trash'},
                    {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
                ]
            },
            {
                label: 'Help',
                icon: 'pi pi-fw pi-question',
                items: [
                    {
                        label: 'Contents',
                        icon: 'pi pi-pi pi-bars'
                    },
                    {
                        label: 'Search', 
                        icon: 'pi pi-pi pi-search', 
                        items: [
                            {
                                label: 'Text', 
                                items: [
                                    {
                                        label: 'Workspace'
                                    }
                                ]
                            },
                            {
                                label: 'User',
                                icon: 'pi pi-fw pi-file',
                            }
                    ]}
                ]
            },
            {
                label: 'Actions',
                icon: 'pi pi-fw pi-cog',
                items: [
                    {
                        label: 'Edit',
                        icon: 'pi pi-fw pi-pencil',
                        items: [
                            {label: 'Save', icon: 'pi pi-fw pi-save'},
                            {label: 'Update', icon: 'pi pi-fw pi-save'},
                        ]
                    },
                    {
                        label: 'Other',
                        icon: 'pi pi-fw pi-tags',
                        items: [
                            {label: 'Delete', icon: 'pi pi-fw pi-minus'}
                        ]
                    }
                ]
            }
        ];
    }
}
```

## Initial State

MenuItem 有一個 **expanded** 屬性來控制子菜單的可見性，你可以使用此屬性來控制菜單模型的初始狀態

## Animation Configuration

你可以使用 **transitionOptions** 屬性來設定過渡動畫，默認值是 **400ms cubic-bezier(0.86, 0, 0.07, 1)**，下面的例子將完全禁用動畫

```
 <p-panelMenu [transitionOptions]="'0ms'" [model]="items" [style]="{'width':'300px'}"></p-panelMenu>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| multiple | boolean | true | 運行同時激活多個選項卡 |
| transitionOptions | string | 400ms cubic-bezier(0.86, 0, 0.07, 1) | 過渡動畫 |
# Styling

| Name | Element |
| -------- | -------- |
| p-panelmenu     | Container element.     |
| p-panelmenu-header     | Accordion header of root submenu.     |
| p-panelmenu-content     | Accordion content of root submenu.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
| p-panelmenu-icon     | Arrow icon of an accordion header.     |
