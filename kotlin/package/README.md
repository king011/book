# 包

使用 package 定義包 使用 import 導入包

```
// 導入包中 的 Message
import org.example.Message

// 導入包中全部 導出符號
import org.example.*

// 導入符號 並定義別名
import org.test.Message as testMessage 
```