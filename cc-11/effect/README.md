# 右值參考 與 move 語義

c++03 的一個效率問題在於 臨時物件無法被拷貝 所以在需要傳輸 臨時物件時 只能重新構造一個副本 或者 動態申請傳遞指針

爲了解決此問題 c++11 提供了move 語義 `T&&` 以及 std::move 函數

`T&&` 定義一個 右值參考的 變數， 如果存在一個 左值參考的調用式和右值參考調用一致，此時編譯器將無法識別，此時需要顯示調用 std::move 取得右值

```
#include <iostream>

void is_r_value(int &i)
{
    puts("not right value");
}
void is_r_value(int &&i)
{
    puts("right value");
}
int main(int argc, char *argv[])
{
    int i = 0;
    is_r_value(1);            // yes
    is_r_value(i);            // no
    is_r_value(std::move(i)); // yes
    return 0;
}
```

爲此 我們的 class 變得更加複雜了因爲 你應該 定義 右值語義的 構造與賦值函數

```
#include <iostream>
#include <cstring>
class Animal
{
private:
    // 定義一個 字符串 類型 用於打印內存分配與釋放
    class string_t final
    {
    private:
        char *str = nullptr;

    public:
        string_t(const string_t &cat) = delete;
        string_t(const char *src = nullptr)
        {
            if (src)
            {
                puts("new");
                auto size = strlen(src);
                str = new char[size + 1];
                memcpy(str, src, size);
                str[size] = 0;
            }
        }
        ~string_t()
        {
            if (str)
            {
                puts("delete");
                delete str;
            }
        }
        inline char *c_str() const
        {
            return str;
        }
        void swap(string_t &other)
        {
            std::swap(str, other.str);
        }
    };

    string_t name_;

public:
    // 傳統的 構造 與 賦值 函數
    Animal(const char *name) : name_(name) {}
    Animal(const Animal &other) : name_(other.name_.c_str()) {}
    Animal &operator=(const Animal &other)
    {
        name_ = string_t(other.name_.c_str());
        return *this;
    }
    // 新的 move 構造 與 賦值 函數 (move 構造應該是 noexcept 的)
    Animal(Animal &&other) noexcept
    {
        name_.swap(other.name_);
    }
    Animal &operator=(Animal &&other)
    {
        name_.swap(other.name_);
        return *this;
    }

    virtual ~Animal() {}
    void speak() const
    {
        auto name = name_.c_str();
        if (name)
        {
            std::cout << name << " speak" << std::endl;
        }
    }
};
void speak(const Animal &&cat)
{
    cat.speak();
}
int main(int argc, char *argv[])
{
    Animal cat("cat"); // new
    cat.speak();
    puts("---------1");
    Animal cat1(std::move(cat)); // move to cat1
    cat1.speak();
    cat.speak(); // not print, name_ null
    puts("---------2");
    speak(std::move(cat1));
    puts("---------3");
    cat1.speak(); // 會打印 因爲 ::speak 中 右值被傳入 但未修改
    return 0;     // only delete cat1.name_
}
```

```
new
cat speak
---------1
cat speak
---------2
cat speak
---------3
cat speak
delete
```

右值形式 的 構造和賦值 和之前習慣性實現的 swap 函數功能類似 只是現在功能更加強大可用在更多地方比如完美轉發參數的工廠函數

# constexpr 泛化的廠商表示

c++11 前已經 具備了 constant expression。比如 3+4 會被編譯器識別出是常量，但是如果一個 函數 固定返回一個常量無法被編譯器識別，現在可以在函數之前加上 關鍵字 constexpr 告訴編譯器返回值是固定的常量

```
#include <iostream>
#include <cstring>
constexpr int GetFive() { return 5; }
constexpr double earth_gravitational_acceleration = 9.8;
constexpr double moon_gravitational_acceleration = earth_gravitational_acceleration / 6.0;
int main(int argc, char *argv[])
{
    int some_value[GetFive() + 7];
    return 0;
}
```

# 對 POD 定義的修正
c++03 中 要作為 POD 必須遵守幾條規則。符合規則的型別可以產生和c相容的物件記憶體佈局(object layout)，而且可以被靜態初始化。但c++03規格嚴格限制了何種型別與c相容或可以被靜態初始化的，儘管並不存在技術原因導致編譯器無法處理。如果建立一個c++03 POD型別，然後為其添加一個非虛成員函式，這個型別就不再是POD型別了，從而無法被靜態初始化，也不再與C相容，儘管其記憶體布局並沒有發生變化。

c++11通過把POD概念劃分成兩個概念：平凡的（trivial）和規格布局（standard-layout），放寬了關於POD的定義。

一個平凡的型別可以被靜態初始化，意味著使用memcpy來複製資料是合法的，而無須使用複製构造式。平凡的型別物件的生命周期開始於其儲存空間被分配時，而不是其构造式完成時。使用模版類 std::is\_trivial&lt;T&gt;::value 來判斷資料型別是否為平凡型別

一個平凡的的類別或結構符合以下定義：

1. 平凡的預設建構式。這可以使用預設建構式語法，例如SomeConstructor() = default;
2. 平凡的複製建構式和move构造式，可使用預設語法（default syntax）
3. 平凡的賦值運算子和move設定運算子，可使用預設語法（default syntax）
4. 平凡的解構式，不可以是虛函式（virtual）
5. 類沒有虛基礎類別和虛成員函式
6. 複製构造式和設定運算子還額外要求所有非靜態資料成員都是平凡的

一個符合規格布局的類封裝成員的方式與C相容。使用模版類 std::is\_standard\_layout&lt;T&gt;::value 來判斷型別是否是一個規格布局型別。一個標準布局（standard-layout）的類別或結構符合以下定義：

1. 所有non-static成員有相同的存取控制（public，private，protected）
2. 沒有虛擬函式
3. 沒有虛基類
4. 所有基礎類別符合標準布局
5. 所有非靜態的（non-static）資料成員屬於符合標準布局的類別
6. 類中第一個非靜態類別與基礎類別不是同一個類別。例如：struct A:B{ B b; int c;}不符合要求
7. 兩種情況必局其一：或者所有基礎類別都沒有non-static成員；或者最衍伸類別沒有non-static資料成員且至多一個帶有non-static成員的基礎類別。基本上，在該類別的繼承體系中只會有一個類別帶有non-static成員。

只有在既是平凡的並且又符合規格佈局才是 POD 可以使用 std::is\_pod&lt;T&gt;::value 來判斷

```
#include <iostream>
#include <type_traits>

class animal_t
{
public:
    animal_t(/* args */) = default;
    ~animal_t() = default;
};
int main(int argc, char *argv[])
{
    animal_t cat;
    std::cout << "is_trivial: " << std::is_trivial<decltype(cat)>() << "\n"
              << "is_standard_layout: " << std::is_standard_layout<decltype(cat)>::value << "\n"
              << "is_pod: " << std::is_pod<decltype(cat)>::value << "\n"
              << std::flush;
    return 0;
}
```

通過劃分，使得放棄一個特性而不失去另一個成為可能。一個具有複雜的複製和move构造式的類可能不是平凡的，但是它可能符合規格布局，從而能與 c 程式互動。類似地，一個同時具有 public 和 private 資料成員的類不符合規格布局，但它可以是平凡的，從而能夠使用 memcpy 來複製