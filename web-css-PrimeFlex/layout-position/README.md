# [position](https://www.primefaces.org/primeflex/position)

一些 position 系列的 class 定義了 css 的 position 屬性

| Class | Properties |
| -------- | -------- |
| static     | position: static;     |
| fixed     | position: fixed;     |
| relative     | position: relative;     |
| absolute     | position: absolute;     |
| sticky     | position: sticky;     |

## static

指定 static 的元素不特殊定位，不受 top/left/right/bottom 屬性的影響，大部分元素默認的值

![](assets/static.png)

```
<div class="card">
    <div class="relative card-container blue-container">
        <div class="static bg-blue-100 p-4 border-round" style="min-width: 300px; min-height: 150px;">
            <p class="font-bold text-gray-900">Static</p>
            <div class="absolute bottom-0 left-0 bg-blue-500 border-round p-4 font-bold text-white" style="min-width: 120px; min-height: 70px">
                Absolute
            </div>
        </div>
    </div>
</div>
```

## fixed

指定 fixed 的元素相對窗口定位，它將跟隨瀏覽器的滾動而保持始終在窗口同一位置

![](assets/fixed.png)

```
<div class="card">
    <div class="card-container yellow-container overflow-hidden" style="height: 250px">
        <div class="relative bg-yellow-500 border-round border-1 border-yellow-500" style="height: 200px">
            <div class="absolute top-0 left-0 px-4 py-3 w-full font-bold">
                Fixed
            </div>
            <div class="absolute overflow-auto surface-overlay mt-6 p-4 line-height-3" style="height: 150px">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
            </div>
        </div>
    </div>
</div>
```

## relative

relative 相對元素原始位置顯示，可以使用 top/left/right/bottom 屬性調整位置

![](assets/relative.png)

```
<div class="card">
    <div class="relative card-container green-container">
        <div class="relative bg-green-100 p-4 border-round font-bold text-gray-900" style="min-width: 300px; min-height: 160px;">Relative
            <div class="absolute bottom-0 left-0 bg-green-500 p-4 text-white font-bold border-round" style="min-width: 120px; min-height: 70px">
                Absolute
            </div>
        </div>
    </div>
</div>
```

## absolute

absolute 相對最近的定位元素定位(position 不爲 static 的元素)，如果沒有則相對 document 定位

![](assets/absolute.png)

```
<div class="card">
    <div class="card-container purple-container mb-4">
        <div class="relative bg-purple-100 border-round p-4 font-bold text-gray-900">
            <p class="mt-0">Relative</p>
            <div class="static bg-purple-200 border-round p-4 font-bold text-gray-900" style="min-width: 300px; min-height: 150px;">
                <p class="mt-0">Static</p>
                <div class="static bottom-0 left-0 bg-purple-500 border-round p-4 font-bold text-white" style="min-width: 120px; min-height: 70px">
                    Static
                </div>
            </div>
        </div>
    </div>

    <div class="card-container purple-container">
        <div class="relative bg-purple-100 border-round p-4 font-bold text-gray-900">
            <p class="mt-0">Relative</p>
            <div class="static bg-purple-200 border-round p-4 font-bold text-gray-900" style="min-width: 300px; min-height: 150px;">
                <p class="mt-0">Static</p>
                <div class="absolute bottom-0 left-0 bg-purple-500 border-round p-4 font-bold text-white" style="min-width: 120px; min-height: 70px">
                    Dynamic
                </div>
            </div>
        </div>
    </div>
</div>
```

## sticky

sticky 粘性定位，當 top/left/right/bottom 的實際值越過指定值時自動 由 relative 效果 變爲 fixed 的效果 

當同時指定 top/left/right/bottom 多個值時，轉變效果的優先級爲 top > bottom，left > right


![](assets/sticky.gif)

```
<div class="card">
    <div class="overflow-hidden card-container indigo-container">
        <div class="overflow-auto border-round border-1 border-indigo-500" style="height: 300px">
            <div>
                <div class="sticky top-0 font-bold text-white bg-indigo-500 p-4">
                    Sticky Title 1
                </div>
                <p class="surface-overlay p-4 m-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                </p>
            </div>
            <div>
                <div class="sticky top-0 font-bold text-white bg-indigo-500 p-4">
                    Sticky Title 2
                </div>
                <p class="surface-overlay p-4 m-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                </p>
            </div>
            <div>
                <div class="sticky top-0 font-bold text-white bg-indigo-500 p-4">
                    Sticky Title 3
                </div>
                <p class="surface-overlay p-4 m-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                </p>
            </div>
            <div>
                <div class="sticky top-0 font-bold text-white bg-indigo-500 p-4">
                    Sticky Title 4
                </div>
                <p class="surface-overlay p-4 m-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                </p>
            </div>
            <div>
                <div class="sticky top-0 font-bold text-white bg-indigo-500 p-4">
                    Sticky Title 5
                </div>
                <p class="surface-overlay p-4 m-0">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Vitae sapien pellentesque habitant morbi tristique senectus et netus. Vitae proin sagittis nisl rhoncus mattis.
                    Maecenas pharetra convallis posuere morbi leo urna molestie. At in tellus integer feugiat scelerisque.
                    Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Luctus accumsan tortor posuere ac ut.
                </p>
            </div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣 position 系列的 class 也支持響應式佈局，使用方式和 display 相同