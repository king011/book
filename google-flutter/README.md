# flutter
flutter 由 google 與 2017年5月 首次發佈的(2018年12月4日 v1.0.0) 一個 開源 行動應用軟體開發套件

使用 dart 作爲 開發 語言

* 官網 [http://flutter.io/](http://flutter.io/)
* 源碼 [https://github.com/flutter](https://github.com/flutter)
* 官方教學 [https://flutter.dev/docs](https://flutter.dev/docs)
* api [https://docs.flutter.io/](https://docs.flutter.io/)
* 中文社區 [https://flutterchina.club/](https://flutterchina.club/)