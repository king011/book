# [evdns\_base](https://libevent.org/doc/dns_8h.html)

```
#include <event2/dns.h>
```

evdns\_base 提供了異步解析 dns 等能力

# evdns\_base\_new

調用 [evdns\_base\_new(eb, flags)](https://libevent.org/doc/dns_8h.html#afdcc359c412591d174f4d965afd18513) 來初始化 異步 dns 庫 evdns\_base，如果成功返回對象指針 否則返回 NULL

* **eb** 是一個 event\_base 指針
* **flags** 可以是 EVDNS\_BASE\_INITIALIZE\_NAMESERVERS | EVDNS\_BASE\_DISABLE\_WHEN\_INACTIVE | EVDNS\_BASE\_NAMESERVERS\_NO\_DEFAULT

1.  **EVDNS\_BASE\_INITIALIZE\_NAMESERVERS** 從 resolv.conf 中加載 nameservers
2.  **EVDNS\_BASE\_DISABLE\_WHEN\_INACTIVE** 當沒有活躍的 dns 請求時，不要阻止 event\_base 退出
3.   **EVDNS\_BASE\_NAMESERVERS\_NO\_DEFAULT** 如果設置了 EVDNS\_BASE\_INITIALIZE\_NAMESERVERS，但 resolv.conf 中沒有 nameserver 時不要添加默認的 nameserver

調用 [evdns\_base\_free(esb, fail\_requests)](https://libevent.org/doc/dns_8h.html#ab221bbadb18451c9007e8dc98bc97786) 來終止所有活動的dns解析

* **esb** 是一個 evdns\_base 指針
* **fail\_requests** 如果非 0 活動中的請求會返回一個空結果錯誤碼爲 DNS\_ERR\_SHUTDOWN，否則活動請求會被丟棄(不進行回調)

# evdns\_base\_resolve\_ipv4

你可以使用 [evdns\_base\_resolve\_ipv4(esb, name, flags, callback, ptr)](https://libevent.org/doc/dns_8h.html#a4f8b11705fa285dffa661c7f55f86693) 或 [evdns\_base\_resolve\_ipv6(esb, name, flags, callback, ptr)](https://libevent.org/doc/dns_8h.html#a646ca1414b09b9fec7763bf8aca05e74) 來解析 A/AAAA 記錄

* **esb** 是一個 evdns\_base 指針
* **name** 這是要解析的 hostname
* **flags**  0 或者 DNS\_QUERY\_\* 的標記
	* **DNS\_QUERY\_IGNTC** 回來響應中的 transcation 不要回退到 tcp 連接
	* **DNS\_QUERY\_NO\_SEARCH** 禁用搜索查詢
	* **DNS\_QUERY\_USEVC** 使用 tcp 而非而 udp 查詢
* **callback** 異步完成的回調函數
* **ptr** 傳給回調函數的自定義參數

你也可以使用 [evdns\_base\_resolve\_reverse(esb, in\_addr, flags, callback, ptr)](https://libevent.org/doc/dns_8h.html#a1f9cbcd4c017180e7d302056b3d5bbd4) 或 [evdns\_base\_resolve\_reverse\_ipv6(esb, in6\_addr, flags, callback, ptr)](https://libevent.org/doc/dns_8h.html#a55843d1e90512bfbe8499d52642240cd) 來查詢 ipv4/ipv6 的反向 PTR 記錄 

如果函數調用成功會返回 evdns\_request 指針，否則返回 NULL，你可以調用 [evdns\_cancel\_request(esb, req)](https://libevent.org/doc/dns_8h.html#a062bdd01f7243b3c8509d93c45a802ef) 來取消未完成的 dns 查詢

當異步解析結束後會通過 [evdns\_callback\_type](https://libevent.org/doc/dns_8h.html#a43018959c4be1a9b16e4143c5e3ff556) 回調函數來通知 dns 的解析結果

```
typedef void(* evdns_callback_type) (int err, char type, int count, int ttl, void *addresses, void *arg)
```

* **err** 這是執行結果的錯誤碼，libevent 定義了 DNS\_ERR\_\* 宏，同時 [evdns\_err\_to\_string(err)](https://libevent.org/doc/dns_8h.html#a24bb4cf48915f73e4e9c6d4b6216466b) 函數可以返回對於錯誤碼的字符串描述文本
* **type** DNS\_IPv4\_A 或 DNS\_PTR 或 DNS\_IPv6\_AAAA
* **count** 解析到的地址數量
* **ttl** 解析結果可以緩存的秒數
* **addresses** 地址數組依據 type 不同而各異
	* **DNS\_IPv4\_A** 表示 ipv4 的 4byte 數組
	* **DNS\_IPv6\_AAAA** 表示 ipv4 的 16byte 數組
	* **DNS\_PTR** 以 NULL 結尾的字符串(char\*\*)

# nameservers

libevent 也提供了很多 api 用於管理 evdns\_base 使用的 nameservers

# server

libevent 也提供了很多 api 用於實現 dns 服務器

# Example
```
#include <event2/event.h>
#include <event2/dns.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

void resolve_handler(int err, char type, int count, int ttl, void *addresses, void *arg)
{
    if (err)
    {
        printf("resolve error: %s => %s(%d)\n", arg, evdns_err_to_string(err), err);
        return;
    }
    char *s = NULL;
    switch (type)
    {
    case DNS_IPv4_A:
        s = "ipv4";
        break;
    case DNS_IPv6_AAAA:
        s = "ipv6";
        break;
    case DNS_PTR:
        s = "ptr";
        break;
    default:
        s = "unknow";
        break;
    }
    printf("resolve %s success, count=%d, ttl=%d type=%s\n", arg, count, ttl, s);

    switch (type)
    {
    case DNS_IPv4_A:
    {
        char *p = addresses;

        for (int i = 0; i < count; i++)
        {
            char ip[16];
            evutil_inet_ntop(AF_INET, p, ip, 16);
            p += 4;
            printf(" %2d. %s\n", i + 1, ip);
        }
        puts("");
    }
    break;
    case DNS_IPv6_AAAA:
    {
        char *p = addresses;

        for (int i = 0; i < count; i++)
        {
            char ip[40] = {0};
            evutil_inet_ntop(AF_INET6, p, ip, 40);
            p += 16;
            printf(" %2d. %s\n", i + 1, ip);
        }
        puts("");
    }
    break;
    case DNS_PTR:
        printf("  -  %s\n\n", *(char **)addresses);
        break;
    }
}
int main(int argc, char *argv[])
{
    if (argc < 1)
    {
        return 0;
    }
    int err = -1;
    struct event_base *eb = event_base_new();
    if (!eb)
    {
        puts("event_base_new error");
        return err;
    }
    struct evdns_base *esb = evdns_base_new(eb, EVDNS_BASE_INITIALIZE_NAMESERVERS | EVDNS_BASE_DISABLE_WHEN_INACTIVE);
    if (!esb)
    {
        puts("evdns_base_new error");
        goto END;
    }
    for (size_t i = 1; i < argc; i++)
    {
        int len = strlen(argv[i]);
        if (len > 3 && !memcmp(argv[i], "ip:", 3))
        {
            struct in_addr *addr = malloc(sizeof(struct in_addr));
            if (!evutil_inet_pton(AF_INET, argv[i] + 3, addr))
            {
                printf("not a ipv4: %s\n", argv[i] + 3);
                goto END;
            }
            if (!evdns_base_resolve_reverse(esb, addr, 0, resolve_handler, argv[i] + 3))
            {
                printf("evdns_base_resolve_reverse %s error\n", argv[i] + 3);
                goto END;
            }
        }
        else if (len > 5 && !memcmp(argv[i], "ipv6:", 5))
        {
            struct in6_addr *addr = malloc(sizeof(struct in6_addr));
            if (!evutil_inet_pton(AF_INET6, argv[i] + 5, addr))
            {
                printf("not a ipv6: %s\n", argv[i] + 5);
                goto END;
            }
            if (!evdns_base_resolve_reverse_ipv6(esb, addr, 0, resolve_handler, argv[i] + 5))
            {
                printf("evdns_base_resolve_reverse %s error\n", argv[i] + 5);
                goto END;
            }
        }
        else if (len > 3 && !memcmp(argv[i], "v6:", 3))
        {
            if (!evdns_base_resolve_ipv6(esb, argv[i] + 3, 0, resolve_handler, argv[i] + 3))
            {
                printf("evdns_base_resolve_ipv6 %s error\n", argv[i] + 3);
                goto END;
            }
        }
        else
        {
            if (!evdns_base_resolve_ipv4(esb, argv[i], 0, resolve_handler, argv[i]))
            {
                printf("evdns_base_resolve_ipv4 %s error\n", argv[i]);
                goto END;
            }
        }
    }
    err = event_base_dispatch(eb);
    if (err < 0)
    {
        puts("event_base_dispatch error");
        goto END;
    }
    err = 0;
END:
    if (esb)
    {
        evdns_base_free(esb, 0);
    }
    event_base_free(eb);
    return err;
}
```
