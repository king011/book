# [生命週期回調](http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html)

cocos 爲 組件腳本提供了 生命週期回調函數

```
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        console.log("0 onLoad")
    }
    start() {
        console.log("1 start")
    }
    update(dt: number) {
        console.log("2 update before every frame")
    }
    lateUpdate() {
        console.log("3 lateUpdate after every frame")
    }
    onDestroy() {
        console.log("4 onDestroy")
    }
    onEnable() {
        console.log("onEnable")
    }
    onDisable() {
        console.log("onDisable")
    }
}

```
# onLoad 

* onLoad 會在腳本初始化化階段被調用 

* onLoad 會在節點首次激活時觸發 比如 場景被 載入

* 在 onLoad 階段 cocos 保證了 你可以獲得場景中所有節點 以及節點關聯的資源數據

* onLoad 總是會在 任何 start 方法之前被調用 這能用於 安排 腳本初始化 順序 通常會在 onLoad 階段去做一些 初始化 相關操作

# start

* start 會在組件第一次 激活前 也就是 第一次執行 update 之前 被回調

* start 通常 用於設置一些中間狀態數據 這些數據 可能會在 update 時 發送改變

# update

遊戲開發的一個關鍵點是在 每幀渲染前更新節點 這些操作通常放在 update(dt: number) 回調中

dt 是完成最後一幀所使用的 增量時間 (單位:秒)

# lateUpdate

如果需要在每幀渲染結束後執行一些 額外操作 可以 設置到 lateUpdate 中

# onEnable

當組件的 enabled 屬性從 false 變爲 true 或者組件掛接節點的 active屬性從 false變爲 true時 會回調 onEnable

然節點 第一次被創建 且 enabled 爲 true 則 onEnable 會在 onLoad 之後 start 之前被調用

> 節點的 active 爲 false 時 組件 enabled 即時變換了 也不會回調 onEnable
> 

# onDisable

當組件的 enabled 屬性 從 true 變爲 false 或者組件掛接節點的 active屬性從 true變爲 false時 會回調 onDisable

> 節點的 active 爲 false 時 組件 enabled 即時變換了 也不會回調 onEnable
> 

# onDestroy

當 組件所在節點 調用了 destory() 則會回調 onDestroy 並在幀結束時 統一回收 組件
 