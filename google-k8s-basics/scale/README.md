# [應用擴展](https://kubernetes.io/docs/tutorials/kubernetes-basics/scale/scale-intro/)

![](assets/module_05_scaling1.svg)

![](assets/module_05_scaling2.svg)


擴展 Deployment 將創建新的 Pods 並將資源調度分配到有可用資源的節點上，收縮會將 Pods 數量減少到所需的數量。另外 k8s 還支持自動縮放。將 Pods 數量收縮到 0 也是可以的，但這會終止 Deployment 上所有已經部署的 Pods。

運行應用程序的多個實例需要在它們之間分配流量。服務有一種負載均衡器類型，可以將網路流量均衡分配到外部訪問的 Pods 上。服務將會一直通過端點來監視 Pods 的運行，保證流量只分配到可用的 Pods 上。

# 縮放應用

1. 使用 `get deployments` 列出 deployments

	```
	kubectl get deployments
	```
	
	```
	NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
	kubernetes-bootcamp   1/1     1            1           3m45s
	```
	
	* NAME -> 集羣中 deployments 的名稱
	* READY -> 當前副本 和 需要的副本 比
	* UP-TO-DATE -> 顯示已更新以達到所需狀態的副本數
	* AVAILABLE -> 顯示你的用戶可以使用多少個應用程式副本
	* AGE -> 顯示程式已運行的時間

1.	要查看由 ReplicaSet 創建的 Deployment 則運行:
	
	```
	kubectl get rs
	```
	
	```
	NAME                             DESIRED   CURRENT   READY   AGE
	kubernetes-bootcamp-765bf4c7b4   1         1         1       9m22s
	```
	
	* Name -> [DEPLOYMENT-NAME]-[RANDOM-STRING]
	* DESIRED -> 顯示應用程式需要的副本數
	* CURRENT-> 顯示當前正在運行多少個副本

1.	使用 scale 命令 擴展副本到 4 個
	
	```
	kubectl scale deployments/kubernetes-bootcamp --replicas=4
	```
	
1.	使用下述指令 查看 Pod 變化:
	
	```
	kubectl get pods -o wide
	```
	
1.	使用下述指令 查詢 詳情：

	```
	kubectl describe deployments/kubernetes-bootcamp
	```

# 負載均衡

1. 檢查以下服務是否在負載平衡流量。要找出公開的 端口 可以使用 describe 查看服務

	```
	kubectl describe services/kubernetes-bootcamp
	```
	
1. 使用下述指令 將公開端口設置到 環境變量

	```
	export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
	echo NODE_PORT=$NODE_PORT
	```
	
1. 向服務發送請求

	```
	curl $(minikube ip):$NODE_PORT
	```