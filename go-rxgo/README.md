# rxgo

rxgo 是 ReactiveX 的 go 實現

```bash
go get -u github.com/reactivex/rxgo
```

* 官網 [http://reactivex.io/](http://reactivex.io/)
* 源碼 [https://github.com/ReactiveX/RxGo](https://github.com/ReactiveX/RxGo)