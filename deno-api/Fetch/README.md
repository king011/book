# [Fetch](https://deno.land/api@v1.28.1?s=fetch)

deno 提供了 Fetch API 來方便的發送 http 請求

**[注意](https://deno.land/manual@v1.28.2/runtime/web_platform_apis#spec-deviations)** 
* 目前 deno(1.28.2) 沒有 cookie jar 所以 fetch 不會主動處理 cookie
* 目前 deno(1.28.2) 不處理同源策略


```
/**
 * @throws Error
 */
function fetch(
  input: URL | Request | string,
  init?: RequestInit,
): Promise<Response>;
```

```
const resp = await fetch("http://127.0.0.1");
console.log(resp.status, resp.statusText);
const content = resp.headers.get("content-type") ?? "";
if (content.indexOf("application/json") >= 0) {
  console.log(await resp.json());
} else {
  console.log(await resp.text());
}
```