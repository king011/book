# 模型

模型是標準的 struct 由 go的基本類型 實現了 [Scanner](https://pkg.go.dev/database/sql/?tab=doc#Scanner) 和 [Valuer](https://pkg.go.dev/database/sql/driver#Valuer) 接口的自定義類型及指針或別名

```
type User struct {
  ID           uint
  Name         string
  Email        *string
  Age          uint8
  Birthday     *time.Time
  MemberNumber sql.NullString
  ActivatedAt  sql.NullTime
  CreatedAt    time.Time
  UpdatedAt    time.Time
}
```

# 約定

gorm 傾向使用約定而非配置 默認情況下 gorm 使用 **ID** 作爲主鍵，使用結構體名稱的 蛇形複數 作爲表明，字段名的 蛇形作爲列名，並使用 CreateAt UpdateAt 字段 追蹤創建 更新時間

遵循 gorm 約定可以 減少 配置和代碼量 如果因爲一些奇怪的狗屎原因無法遵循約定 gorm 允許使用 自定義配置它們

# 高級選項
## 字段級權限控制

可導出字段在 使用 gorm 進行 CRUD時擁有 全部 權限 此外 gorm 允許通過標籤控制字段級別的權限

```
type User struct {
  Name string `gorm:"<-:create"` // allow read and create
  Name string `gorm:"<-:update"` // allow read and update
  Name string `gorm:"<-"`        // allow read and write (create and update)
  Name string `gorm:"<-:false"`  // allow read, disable write permission
  Name string `gorm:"->"`        // readonly (disable write permission unless it configured )
  Name string `gorm:"->;<-:create"` // allow read and create
  Name string `gorm:"->:false;<-:create"` // createonly (disabled read from db)
  Name string `gorm:"-"`  // ignore this field when write and read with struct
}
```
> 使用 gorm Migrator 創建表時 不會創建忽略的字段

## 創建/更新 時間追蹤 (納秒 毫秒 秒 Time)

gorm 約定 使用 CreateAt UpdateAt 追蹤 創建/更新 時間。 如果定義了這種字段 gorm 在 **創建/更新** 時會自動填充 當前時間

要使用不同名稱的字段 可以配置 autoCreateTime autoUpdateTime 標籤

如果向保存 unix 時間戳 而非 Time 可以簡單的將 time.Time 修改爲 int 型別即可

```
type User struct {
  CreatedAt time.Time // 在創建時，如果該字段爲0值，則使用當前時間填充
  UpdatedAt int       // 在 創建/更新 時，如果該字段爲0值，則使用當前時間填充
  Updated   int64 `gorm:"autoUpdateTime:nano"` //使用時間戳 納秒 充更新時間
  Updated   int64 `gorm:"autoUpdateTime:milli"` // 使用時間戳 毫秒 充更新時間
  Created   int64 `gorm:"autoCreateTime"`      // 使用時間戳 秒 充更新時間
}
```

## 嵌入 結構體

gorm 允許 嵌入結構體

gorm.Model 是 gorm 定義的 一個 結構體 包含了 ID CreatedAt UpdatedAt DeletedAt 字段

```
// gorm.Model 的定义
type Model struct {
  ID        uint           `gorm:"primaryKey"`
  CreatedAt time.Time
  UpdatedAt time.Time
  DeletedAt gorm.DeletedAt `gorm:"index"`
}
```

對於 匿名字段 gorm 會將其字段包含在父結構中
```
type User struct {
  gorm.Model
  Name string
}
// 等效於
type User struct {
  ID        uint           `gorm:"primaryKey"`
  CreatedAt time.Time
  UpdatedAt time.Time
  DeletedAt gorm.DeletedAt `gorm:"index"`
  Name string
}
```

對於 非匿名字段 可以使用 embedded 嵌入 結構

```
type Author struct {
    Name  string
    Email string
}

type Blog struct {
  ID      int
  Author  Author `gorm:"embedded"`
  Upvotes int32
}
// 等效於
type Blog struct {
  ID    int64
    Name  string
    Email string
  Upvotes  int32
}
```

使用 embedded 時海可以配合使用 embeddedPrefix 爲 字段名 添加 前綴

```
type Blog struct {
  ID      int
  Author  Author `gorm:"embedded;embeddedPrefix:author_"`
  Upvotes int32
}
// 等效于
type Blog struct {
  ID          int64
    AuthorName  string
    AuthorEmail string
  Upvotes     int32
}
```

## 字段標籤

說明 model 時 tag 是可選的，gorm 支持以下 tag(tag名不區分大小寫 建議使用 camelCase 風格)


| 標籤名 | 說明 | 
| -------- | -------- |
| column     | 指定db列名     |
| type     | 列數據型別 推薦使用兼容性好的通用類型，例如：所有數據庫都支持 bool int uint float string time bytes 並且可以和其它標籤一起使用，例如：not null size autoIncrement ... 像 varbinary(8) 這樣指定數據庫型別也是支持的。在使用指定數據庫型別時，需要完整的數據庫型別，如：MEDIUMINT UNSIGNED not NULL AUTO_INSTREMENT     |
| size     | 指定列大小，例如：**size:256**     |
| primaryKey     | 指定爲主鍵     |
| unique     | 指定爲唯一     |
| default     | 指定默認值     |
| precision     | 指定列精度     |
| scale     | 指定列大小     |
| not null     | 指定列不能爲 NULL     |
| autoIncrement     | 指定列爲自增     |
| autoIncrementIncrement     | 自增步長     |
| embedded     | 嵌套字段     |
| embeddedPrefix     | 嵌入字段的列名前綴     |
| autoCreateTime     | 創建 時 追蹤當前時間     |
| autoUpdateTime     | 創建或更新 時 追蹤當前時間     |
| index     | 創建索引     |
| uniqueIndex     | 創建唯一索引     |
| check     | 創建檢查約束，例如 check:age > 13     |
| <-     | 設置寫入權限， <-:create 只創建； <-:update 只更新 ; <-:false 無寫入權限; <- 創建和更新權限     |
| ->     | 設置字段讀權限， ->:false 無讀權限     |
| -     | 忽略字段     |
| comment     | 遷移時爲字段添加註釋     |

## 關聯標籤

gorm 允許通過 標籤 爲關聯配置 外鍵 約束 many2many 表