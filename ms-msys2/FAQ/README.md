# 中文亂碼

windows 自帶的一些指令使用了本地編碼例如 gbk，而 msys2 使用 utf8 這會讓這些指令在 msys2 中顯示爲亂碼

解決方案是在創建一個使用 iconv 轉碼的腳本然後 **/etc/profile.d/alias.sh** 中創建別名

```
#info="/usr/bin/winexec.sh"
#!/usr/bin/bash
$@ |iconv -f gbk -t utf-8
```

```
#info="/etc/profile.d/alias.sh"

alias ping="/usr/bin/winexec.sh ping"
alias netstat="/usr/bin/winexec.sh netstat"
alias nslookup="/usr/bin/winexec.sh nslookup"
```

> 注意這裏利用了管道，它可以轉碼 stdout 但不能轉碼 stderror，並且即時出現錯誤 $? 也會是 0(因爲最後一個指令是 iconv)

