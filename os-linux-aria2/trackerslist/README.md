# trackerslist
trackerslist 是一個開源(GPL2)項目 基本每天更新 維護了最新的 bt track 服務器地址 

* trackers_best.txt 檔案 保存了 優質的地址
* trackers_all.txt 檔案 保存了所有協議的 地址
* 項目地址 [https://github.com/ngosang/trackerslist](https://github.com/ngosang/trackerslist)

# bt-tracker
track服務器 將地址 加入到 aria2.conf 中的 bt-tracker 配置項即可 多個使用 , 分隔

```
echo 'URL=https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all.txt
set -e;
echo curl $URL
value=$(curl  $URL)
i=0
echo -n bt-tracker=
for val in $value
do
    if [[ $i == 0 ]];then
        echo -n $val
        i=1
    else
        echo -n ,$val
    fi
done
echo
' | bash
```

```
bt-tracker=udp://tracker.coppersurfer.tk:6969/announce,udp://tracker.opentrackr.org:1337/announce,udp://tracker.leechers-paradise.org:6969/announce,udp://p4p.arenabg.com:1337/announce,udp://9.rarbg.to:2710/announce,udp://9.rarbg.me:2710/announce,udp://exodus.desync.com:6969/announce,udp://tracker.cyberia.is:6969/announce,udp://retracker.lanta-net.ru:2710/announce,udp://open.stealth.si:80/announce,udp://tracker.tiny-vps.com:6969/announce,udp://tracker.torrent.eu.org:451/announce,http://tracker4.itzmx.com:2710/announce,udp://tracker3.itzmx.com:6961/announce,http://tracker1.itzmx.com:8080/announce,udp://tracker.moeking.me:6969/announce,udp://bt2.archive.org:6969/announce,udp://bt1.archive.org:6969/announce,udp://ipv4.tracker.harry.lu:80/announce,udp://valakas.rollo.dnsabr.com:2710/announce
```