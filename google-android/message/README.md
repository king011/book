# 消息機制

android 佔用 java 線程 在此之上 實現了一套 消息機制 用於 線程間通信

* Looper 實現了消息隊列 和 隊列處理
* Handler 和 Looper 關聯 提供了 將 消息 加入到 Looper 消息隊列的 功能
* Message 是對 消息的封裝 存儲了 必要的 消息相關數據


```
package com.king011.example;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static public final String TAG="MainActivity";
    private Handler handler = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void onStop(View view){
        if(handler != null){
            handler.sendMessage(Message.obtain());
            handler = null;
        }

    }
    public void onStart(View view){
        new Thread(new Runnable() {
            private int i = 0;
            @Override
            public void run() {
                // 創建 消息隊列
                Looper.prepare();

                // 創建一個 管理 主線程的 Handler
                // 以便 發送 消息 到 主線程
                Handler mainHandler = new Handler(Looper.getMainLooper()){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        handler = (Handler) msg.obj;
                    }
                };

                // 創建 當前 線程 管理的 Handler
                // 以便 其它線程 發送 消息 到 當前線程
                Handler handler = new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        // 通知 looper 退出
                        Looper.myLooper().quit();
                    }
                };

                // 通知 主線程 可以 發送 消息
                Message msg = Message.obtain();
                msg.obj = handler;
                mainHandler.sendMessage(msg);

                // 循環 處理 消息隊列
                Log.i(TAG,"start loop");
                Looper.loop();
                Log.i(TAG,"exit loop");
            }
        }).start();
    }
}
```

# Looper
[android.os.Looper](https://developer.android.com/reference/android/os/Looper) 

* 實現了消息隊列 和 隊列處理
* Looper 只能通過 static prepare() 方法 創建 一個線程只能有一個 Looper
* static loop 方法 會獲取當前線程 管理的 Looper 並 運行消息處理 循環
* Looper.quit 通知 loop 退出 消息處理 循環
* static myLooper() 方法 返回 當前線程 關聯的 Looper/null

> prepare 會將 創建的 Looper 和 當前 線程 關聯 並且如果 當前線程 已經創建了 則拋出異常
> 
> android 已經爲 主線程 創建了 Looper 並啓動了 消息處理 循環

# Handler
[android.os.Handler](https://developer.android.com/reference/android/os/Handler)

Handler 提供了 多個 構造 方法
```
// 創建 Handler 和當前線程的 Looper 關聯
Handler();
// 創建 Handler 和當前線程的 Looper 關聯 並使用 回調處理 消息
Handler(Handler.Callback callback);
// 創建 Handler 和指定 Looper 關聯
Handler(Looper looper);
// 創建 Handler 和指定 Looper 關聯 並使用 回調處理 消息
Handler(Looper looper, Handler.Callback callback);
```

> 當 Looper 處理 Handler 傳來的消息時 會實現 查看 Handler 是否指定 Callback 有則 調用 Callback 否則 調用 Handler.handleMessage 處理
> 

Handler 提供了 多個 sendMessage 和變種 函數 用來 將 消息發送到管理的 Looper

# Message
[android.os.Message](https://developer.android.com/reference/android/os/Message)

Message 存儲了 消息 相關 信息

應該使用 static obtainXXX 來 創建 消息 Message 維護了一個 消息池 obtain 可以從全局的 消息池中 創建一個 消息  

Message 保存 幾個 常用的 public 變量

| 變量名 | 含義 |
| -------- | -------- | 
| int what     | 自定義的消息碼 通常用來 區分 消息     | 
| int arg1     | 存儲 消息 相關的 int 數據     | 
| int arg2     | 存儲 消息 相關的 int 數據     | 
| Object obj     | 存儲 消息 相關的 對象     | 

# HandlerThread
[android.os.HandlerThread](https://developer.android.com/reference/android/os/HandlerThread)

一個 帶 looper 的 線程 就是一個 很好的 工作隊列 HandlerThread 實現了這個 功能

```
package com.king011.example;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    static public final String TAG = "MainActivity";
    HandlerThread thread = null;
    Handler handler = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onStart(View view){
        if(thread != null) {
            return;
        }
        // 創建線程
        thread = new HandlerThread("work thread", Process.THREAD_PRIORITY_BACKGROUND);
        // 運行 同時 創建 looper 之後 運行 消息處理
        thread.start();
        Log.i(TAG,"start");

        // 創建 線程 looper 的 handler
        handler = new Handler(thread.getLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Log.i(TAG,"dael send "+Long.toString(msg.what));
            }
        };
    }
    public void onStop(View view){
        if(thread == null) {
            return;
        }
        // 停止 接收新 消息 請求
        // 處理玩完 已發送 消息隊列後 退出 線程
        thread.quitSafely();
        //thread.quit(); //退出 不用處理 待處理的消息
        thread = null;
        handler = null;
        Log.i(TAG,"quitSafe");

    }
    public void onSend(View view){
        if(handler == null) {
            return;
        }
        Log.i(TAG,"send");
        handler.sendEmptyMessage(100);
    }
}
```