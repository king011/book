# 手動安裝

直接下載最小程式運行即可

## ~/.config/code-server/config.yaml

**~/.config/code-server/config.yaml** 檔案是配置程式記錄了 連接密碼 服務器監聽地址

```
$ cat ~/.config/code-server/config.yaml
bind-addr: 127.0.0.1:8080
auth: password
password: 7e6695d9af07dbf60b722e94
cert: false
```

## ~/.local/share/code-server

**~/.local/share/code-server** 是用戶數據目錄 保存了用戶設定 和 安裝的插件

## systemd

請將 User 屬性改爲你自己的linux 用戶名

```
#info="code-server.service"
[Unit]
Description=Code Server Service
After=network-online.target
Wants=network-online.target

[Service]
User=king
Type=simple
ExecStart=/opt/code-server/bin/code-server
KillMode=control-group
Restart=on-failure
LimitNOFILE=1048576

[Install]
WantedBy=multi-user.target
```

# [Debian/Ubuntu](https://coder.com/docs/code-server/install#debian-ubuntu)

官方提供了 Debian Ubuntu 安裝包，推薦使用

```
# 請設置爲要安裝的版本號
VERSION=4.92.2

curl -fOL https://github.com/coder/code-server/releases/download/v$VERSION/code-server_${VERSION}_amd64.deb
sudo dpkg -i code-server_${VERSION}_amd64.deb
sudo systemctl enable --now code-server@$USER
# Now visit http://127.0.0.1:8080. Your password is in ~/.config/code-server/config.yaml
```

