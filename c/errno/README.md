# errno

```
#include <errno.h>
```

c語言中 全局變量 errno 用來存儲錯誤碼 當一些系統調用失敗時 會設置 errno

```
#include <string.h>
```

strerror 返回 errno 相關的 錯誤描述字符串

```
#include <stdio.h>
```

perror 用來打印 和額外自定義描述 和 errno 相關的 錯誤描述字符串 

```
#include <errno.h>
#include <string.h>
#include <stdio.h>
int main(int argc, char *argv[])
{
    const char *filename = "acb";
    FILE *file = fopen(filename, "r");
    if (file)
    {
        fclose(file);
    }
    else
    {
        int ec = errno;
        perror(filename);
        puts(strerror(ec));
    }
    return 0;
}
```

# 注意

1. 並非所有系統函數都會設置 errno 應該查看api說明
2. 一些函數在 成功時不會設置 errno 故可能需要在調用函數前 設置 errno 爲0 才能依據 errno 獲取準確的錯誤信息
3. 一些函數 會返回是否執行成功 應該首先檢查 此類函數的返回值 只在失敗時 errno 才是有意義的
4. 不要重新定義 errno 直接 `#include <errno.h>` 即可 自定義 errno 行爲是未定義的

# 多線程

早期 errno 被定義爲 `extern int errno` 這對多線程來說是錯誤的

目前 許多編譯器 在多線程 將 errno 設置爲線程局部變量 來實現多線程間的錯誤不會串改

1. 通常在線程下 要確定 \_\_ASSEMBLER\_\_ 沒被定義
2. 確定 \_LIBC 沒有被定義爲 \_LIBC\_REENTRANT

使用如下代碼 可以檢查 
```
int main(void)
{
#ifndef __ASSEMBLER__
    printf( "__ASSEMBLER__ is not defined！\n" );
#else
    printf( "__ASSEMBLER__ is defined！\n" );
#endif
#ifndef __LIBC
    printf( "__LIBC is not defined\n" );
#else
    printf( "__LIBC is defined！\n" );
#endif
#ifndef _LIBC_REENTRANT
    printf( "_LIBC_REENTRANT is not defined\n" );
#else
    printf( "_LIBC_REENTRANT is defined！\n" );
#endif
    return 0;
}
```