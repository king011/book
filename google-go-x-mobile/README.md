# mobile

```
golang.org/x/mobile
```

mobile 庫提供了 對移動平臺(Android 和 IOS)的支持 同時提供了構建用的工具

通常有兩種方式使用 go

1. 編寫完全由 go 實現的 程式
2. 通過 go 包生成綁定並從 Java(在 Android)上 和 Objective-C(在 IOS 上) 調用它們來編寫 SDK 應用程式

* 文檔 [https://github.com/golang/go/wiki/Mobile](https://github.com/golang/go/wiki/Mobile)