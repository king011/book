# 環境安裝

1. 安裝 go 1.16 或更高的版本

2. mobile 提供了一個 gomobile 工具用於完成代碼的構建和綁定

	```
	#info="安裝 gomobile"
	go install golang.org/x/mobile/cmd/gomobile@latest
	```
	
3. 安裝 gobind

	```sh
	gomobile init
	```
	
# Android 

如果要開發 android 需要安裝 android 開發環境 SDK+NDK

此外還需要安裝配置 環境變量 ANDROID\_HOME 指向 sdk 安裝路徑

在包中執行下述命令 創建 綁定的 java 包

```
gomobile bind -o XXX.aar -target=android
```

* -o XXX.aar 指定 輸出包
* -target 指定輸出目標平臺

此外在每次編譯輸出庫前 請執行 下述命令(爲 go.mod 寫入一些必要的依賴)
```
go get -d golang.org/x/mobile/cmd/gomobile
```