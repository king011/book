# C-Thread-Pool

C-Thread-Pool 是一個開源(MIT) 的輕量 純 c 實現的 POSIX 線程池，使用編譯都很容易，只需要將 thpool.c thpool.h 加入項目即可

* [源碼](https://github.com/Pithikos/C-Thread-Pool/tree/master)

```
#include <thpool.h>

```
# Example

```
#include <thpool.h>
#include <stdio.h>
#include <unistd.h>
void thread_work(void *arg)
{
    size_t i = (size_t)arg;
    printf("threads %zu begin\n", i);
    sleep(1);
    printf("threads %zu end\n", i);
}
int main(int argc, char *argv[])
{
    // 初始化線程池，工作線程數爲 4
    threadpool threads = thpool_init(4);
    if (!threads)
    {
        puts("thpool_init error");
        return 1;
    }

    for (size_t i = 0; i < 10; i++)
    {
        // 添加工作
        if (thpool_add_work(threads, thread_work, (void *)i))
        {
            printf("add work %zu error\n", i);
        }
    }

    // 返回當前正在工作的線程數量
    printf("threads: %d\n", thpool_num_threads_working(threads));

    // 等待所有工作完成
    thpool_wait(threads);
    // 銷毀線程池釋放所有資源
    thpool_destroy(threads);
    return 0;
}
```

注意當線程工作線程都處於工作狀態時，新的任務將會阻塞，只到有線程空閒下來
