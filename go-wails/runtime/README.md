# [runtime](https://wails.io/docs/reference/runtime/intro)

wails 提供量一個 runtime，它們同時在 go 和 javascript 中被提供並且儘量保持一致

* [Windows](go-wails/Windows)
* Menu
* [Dialog](go-wails/Dialog)
* Events
* Browser
* [Log](go-wails/Log)

Go Runtime 可以通過 import github.com/wailsapp/wails/v2/pkg/runtime 來獲取，它們的第一個參數都是一個 context.Context， 你需要從 OnStartup/OnDomReady 來獲取這個 Context
 
JS Runtime 可以通過 window.runtime 來訪問

## Hide

隱藏應用程序

```
Go: Hide(ctx context.Context)
JS: Hide()
```

在 MAC 上，這將以與標準 Mac 應用程序中的隱藏菜單相同的方式隱藏應用程序。這與隱藏窗口不同，但應用程序仍在前臺。對於 Windows 和 Linux 這目前與 WindowsHide 相同

## Show

顯示應用程序

```
Go: Show(ctx context.Context)
JS: Show()
```

在 Mac 上，這會將應用程序帶回前臺。對於 Windows 和 Linux 這目前與 WindowShow 相同

## Quit

退出應用程序

```
Go: Quit(ctx context.Context)
JS: Quit()
```

## Environment

返回當前環境信息

```
Go: Environment(ctx context.Context) EnvironmentInfo
JS: Environment(): Promise<EnvironmentInfo>
```

```
#info="go"
type EnvironmentInfo struct {
    BuildType string
    Platform  string
    Arch      string
}
```

```
#info="JS"
interface EnvironmentInfo {
  buildType: string;
  platform: string;
  arch: string;
}
```

# [Menu](https://wails.io/docs/reference/runtime/menu)

這些方法用於重新加載程序菜單，目前(v2.3.1) 僅支持從 golang 調用

## MenuSetApplicationMenu

將應用程序菜單設置爲給定的值

```
Go: MenuSetApplicationMenu(ctx context.Context, menu *menu.Menu)
```

## MenuUpdateApplicationMenu

更新應用程序菜單，獲取傳遞給 MenuSetApplicationMenu 的菜單的任何更改

```
Go: MenuUpdateApplicationMenu(ctx context.Context)
```

# [Events](https://wails.io/docs/reference/runtime/events)

wails 提供來一組事件系統，可以由 golang 或 javascript 進行收發

## EventsOn

爲事件註冊一個監聽器，當發出 eventName 事件時將觸發回調。這個函數將返回一個用於取消監聽的函數

```
Go: EventsOn(ctx context.Context, eventName string, callback func(optionalData ...interface{})) func()
JS: EventsOn(eventName string, callback function(optionalData?: any)): () => void
```

## EventsOnce
類似 EventsOn，但監聽器被回調一次後自動取消
```
Go: EventsOnce(ctx context.Context, eventName string, callback func(optionalData ...interface{})) func()
JS: EventsOnce(eventName string, callback function(optionalData?: any)): () => void
```

## EventsOnMultiple
類似 EventsOn，但監聽器被回調 counter 次後自動取消
```
Go: EventsOnMultiple(ctx context.Context, eventName string, callback func(optionalData ...interface{}), counter int) func()
JS: EventsOnMultiple(eventName string, callback function(optionalData?: any), counter int): () => void
```
## EventsOff
註銷給定名稱事件的監聽器

```
Go: EventsOff(ctx context.Context, eventName string, additionalEventNames ...string)
JS: EventsOff(eventName string, ...additionalEventNames)
```

## EventsEmit

發出給定的事件，這將觸發事件監聽器被回調

```
Go: EventsEmit(ctx context.Context, eventName string, optionalData ...interface{})
JS: EventsEmit(eventName: string, ...optionalData: any)
```

# [Browser](https://wails.io/docs/reference/runtime/browser)

用於打開系統瀏覽器

## BrowserOpenURL

在系統瀏覽器中打開 url

```
Go: BrowserOpenURL(ctx context.Context, url string)
JS: BrowserOpenURL(url string)
```