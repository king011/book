# 解析

Luxon 不是 NLP 工具，並不適合所有日期解析工作。但它確實可以做一些解析：

1. 直接支持多種知名的格式，包括大多數有效的 ISO 8601
2. ad-hoc 用於解析特定格式的臨時解析器

# Parsing technical formats

## ISO 8601

通過 fromISO 方法支持多種有效的 ISO 8601 格式

```
DateTime.fromISO("2016-05-25");
```

```
2016
2016-05
201605
2016-05-25
20160525
2016-05-25T09
2016-05-25T09:24
2016-05-25T09:24:15
2016-05-25T09:24:15.123
2016-05-25T0924
2016-05-25T092415
2016-05-25T092415.123
2016-05-25T09:24:15,123
2016-W21-3
2016W213
2016-W21-3T09:24:15.123
2016W213T09:24:15.123
2016-200
2016200
2016-200T09:24:15.123
09:24
09:24:15
09:24:15.123
09:24:15,123
```

* 此外，所有時間都支持偏移參數 如 "Z" 和 "+06:00"
* 缺少的低階值始終設置爲可能的最小值；既它總是解析爲完整的日期時間。例如 "2016-05-25" 解析爲當然午夜，"2016-05" 解析爲月初
* 如果未指定偏移量，則時間將被解析爲本地時間，但請參閱方法文檔和時區文檔

## HTTP and RFC2822

Luxon 提供對 RFC 2822 和 HTTP header 規範(RFC 850 and 1123) 的解析支持

```
DateTime.fromRFC2822("Tue, 01 Nov 2016 13:23:12 +0630");
DateTime.fromHTTP("Sunday, 06-Nov-94 08:49:37 GMT");
DateTime.fromHTTP("Sun, 06 Nov 1994 08:49:37 GMT");
```

## SQL

通過 fromSQL 接收 SQL 日期 時間 和日期時間

```
DateTime.fromSQL("2017-05-15");
DateTime.fromSQL("2017-05-15 09:24:15");
DateTime.fromSQL("09:24:15");
```

## Unix timestamps

可以通過 unix 創建
```
DateTime.fromMillis(1542674993410);
DateTime.fromSeconds(1542674993);
```

## fromJSDate

fromJSDate 可以將 js 的 Date 轉爲 Luxon 的 DateTime 對象

# Ad-hoc parsing

使用 fromFormat 從自定義的臨時解析器中解析

```
DateTime.fromFormat("May 25 1982", "LLLL dd yyyy");
```

## Intl

Luxon 支持解析國際化字符串

```
DateTime.fromFormat("mai 25 1982", "LLLL dd yyyy", { locale: "fr" });
```

## 限制

fromFormat 並不支持 toFormat 所支持的每個 token

<table>
<thead>
<tr>
<th>Standalone token</th>
<th>Format token</th>
<th>Description</th>
<th>Example</th>
</tr>
</thead>
<tbody><tr>
<td>S</td>
<td></td>
<td>millisecond, no padding</td>
<td>54</td>
</tr>
<tr>
<td>SSS</td>
<td></td>
<td>millisecond, padded to 3</td>
<td>054</td>
</tr>
<tr>
<td>u</td>
<td></td>
<td>fractional seconds, (5 is a half second, 54 is slightly more)</td>
<td>54</td>
</tr>
<tr>
<td>uu</td>
<td></td>
<td>fractional seconds, (one or two digits)</td>
<td>05</td>
</tr>
<tr>
<td>uuu</td>
<td></td>
<td>fractional seconds, (only one digit)</td>
<td>5</td>
</tr>
<tr>
<td>s</td>
<td></td>
<td>second, no padding</td>
<td>4</td>
</tr>
<tr>
<td>ss</td>
<td></td>
<td>second, padded to 2 padding</td>
<td>04</td>
</tr>
<tr>
<td>m</td>
<td></td>
<td>minute, no padding</td>
<td>7</td>
</tr>
<tr>
<td>mm</td>
<td></td>
<td>minute, padded to 2</td>
<td>07</td>
</tr>
<tr>
<td>h</td>
<td></td>
<td>hour in 12-hour time, no padding</td>
<td>1</td>
</tr>
<tr>
<td>hh</td>
<td></td>
<td>hour in 12-hour time, padded to 2</td>
<td>01</td>
</tr>
<tr>
<td>H</td>
<td></td>
<td>hour in 24-hour time, no padding</td>
<td>13</td>
</tr>
<tr>
<td>HH</td>
<td></td>
<td>hour in 24-hour time, padded to 2</td>
<td>13</td>
</tr>
<tr>
<td>Z</td>
<td></td>
<td>narrow offset</td>
<td>+5</td>
</tr>
<tr>
<td>ZZ</td>
<td></td>
<td>short offset</td>
<td>+05:00</td>
</tr>
<tr>
<td>ZZZ</td>
<td></td>
<td>techie offset</td>
<td>+0500</td>
</tr>
<tr>
<td>z</td>
<td></td>
<td>IANA zone</td>
<td>America/New_York</td>
</tr>
<tr>
<td>a</td>
<td></td>
<td>meridiem</td>
<td>AM</td>
</tr>
<tr>
<td>d</td>
<td></td>
<td>day of the month, no padding</td>
<td>6</td>
</tr>
<tr>
<td>dd</td>
<td></td>
<td>day of the month, padded to 2</td>
<td>06</td>
</tr>
<tr>
<td>E</td>
<td>c</td>
<td>day of the week, as number from 1-7 (Monday is 1, Sunday is 7)</td>
<td>3</td>
</tr>
<tr>
<td>EEE</td>
<td>ccc</td>
<td>day of the week, as an abbreviate localized string</td>
<td>Wed</td>
</tr>
<tr>
<td>EEEE</td>
<td>cccc</td>
<td>day of the week, as an unabbreviated localized string</td>
<td>Wednesday</td>
</tr>
<tr>
<td>M</td>
<td>L</td>
<td>month as an unpadded number</td>
<td>8</td>
</tr>
<tr>
<td>MM</td>
<td>LL</td>
<td>month as an padded number</td>
<td>08</td>
</tr>
<tr>
<td>MMM</td>
<td>LLL</td>
<td>month as an abbreviated localized string</td>
<td>Aug</td>
</tr>
<tr>
<td>MMMM</td>
<td>LLLL</td>
<td>month as an unabbreviated localized string</td>
<td>August</td>
</tr>
<tr>
<td>y</td>
<td></td>
<td>year, 1-6 digits, very literally</td>
<td>2014</td>
</tr>
<tr>
<td>yy</td>
<td></td>
<td>two-digit year, interpreted as &gt; 1960 (also accepts 4)</td>
<td>14</td>
</tr>
<tr>
<td>yyyy</td>
<td></td>
<td>four-digit year</td>
<td>2014</td>
</tr>
<tr>
<td>yyyyy</td>
<td></td>
<td>four- to six-digit years</td>
<td>10340</td>
</tr>
<tr>
<td>yyyyyy</td>
<td></td>
<td>six-digit years</td>
<td>010340</td>
</tr>
<tr>
<td>G</td>
<td></td>
<td>abbreviated localized era</td>
<td>AD</td>
</tr>
<tr>
<td>GG</td>
<td></td>
<td>unabbreviated localized era</td>
<td>Anno Domini</td>
</tr>
<tr>
<td>GGGGG</td>
<td></td>
<td>one-letter localized era</td>
<td>A</td>
</tr>
<tr>
<td>kk</td>
<td></td>
<td>ISO week year, unpadded</td>
<td>17</td>
</tr>
<tr>
<td>kkkk</td>
<td></td>
<td>ISO week year, padded to 4</td>
<td>2014</td>
</tr>
<tr>
<td>W</td>
<td></td>
<td>ISO week number, unpadded</td>
<td>32</td>
</tr>
<tr>
<td>WW</td>
<td></td>
<td>ISO week number, padded to 2</td>
<td>32</td>
</tr>
<tr>
<td>o</td>
<td></td>
<td>ordinal (day of year), unpadded</td>
<td>218</td>
</tr>
<tr>
<td>ooo</td>
<td></td>
<td>ordinal (day of year), padded to 3</td>
<td>218</td>
</tr>
<tr>
<td>q</td>
<td></td>
<td>quarter, no padding</td>
<td>3</td>
</tr>
<tr>
<td>D</td>
<td></td>
<td>localized numeric date</td>
<td>9/6/2014</td>
</tr>
<tr>
<td>DD</td>
<td></td>
<td>localized date with abbreviated month</td>
<td>Aug 6, 2014</td>
</tr>
<tr>
<td>DDD</td>
<td></td>
<td>localized date with full month</td>
<td>August 6, 2014</td>
</tr>
<tr>
<td>DDDD</td>
<td></td>
<td>localized date with full month and weekday</td>
<td>Wednesday, August 6, 2014</td>
</tr>
<tr>
<td>t</td>
<td></td>
<td>localized time</td>
<td>1:07 AM</td>
</tr>
<tr>
<td>tt</td>
<td></td>
<td>localized time with seconds</td>
<td>1:07:04 PM</td>
</tr>
<tr>
<td>T</td>
<td></td>
<td>localized 24-hour time</td>
<td>13:07</td>
</tr>
<tr>
<td>TT</td>
<td></td>
<td>localized 24-hour time with seconds</td>
<td>13:07:04</td>
</tr>
<tr>
<td>TTT</td>
<td></td>
<td>localized 24-hour time with seconds and abbreviated offset</td>
<td>13:07:04 EDT</td>
</tr>
<tr>
<td>f</td>
<td></td>
<td>short localized date and time</td>
<td>8/6/2014, 1:07 PM</td>
</tr>
<tr>
<td>ff</td>
<td></td>
<td>less short localized date and time</td>
<td>Aug 6, 2014, 1:07 PM</td>
</tr>
<tr>
<td>F</td>
<td></td>
<td>short localized date and time with seconds</td>
<td>8/6/2014, 1:07:04 PM</td>
</tr>
<tr>
<td>FF</td>
<td></td>
<td>less short localized date and time with seconds</td>
<td>Aug 6, 2014, 1:07:04 PM</td>
</tr>
<tr>
<td>'</td>
<td></td>
<td>literal start/end, characters between are not tokenized</td>
<td>'T'</td>
</tr>
</tbody></table>