# art-template

art-template 一個開源MIT快速的 js 模板引擎 支持 node 和 browser

* 官網 [https://aui.github.io/art-template/zh-cn/index.html](https://aui.github.io/art-template/zh-cn/index.html)
* 源碼 [https://github.com/aui/art-template](https://github.com/aui/art-template)
* 文檔 [https://aui.github.io/art-template/zh-cn/docs/](https://aui.github.io/art-template/zh-cn/docs/)