# [Events](https://github.com/jerryscript-project/iotjs/blob/master/docs/api/IoT.js-API-Events.md)

Events 提供了事件處理的支持，IoT.js 架構基於事件驅動

```
var EventEmitter = require('events');

var emitter = new EventEmitter();
```
# class EventEmitter

```
class EventEmitter {
	/**
	 * 在事件尾添加一個事件監聽器
	 * @param event 
	 * @param listener 
	 */
	addListener(event: string, listener: (...args: Array<any>) => void): EventEmitter
	/**
	 * 在事件尾添加一個事件監聽器
	 * @param event 
	 * @param listener 
	 */
	on(event: string, listener: (...args: Array<any>) => void): EventEmitter
	/**
	 * 在事件尾添加一個一次性事件監聽器
	 * @param event 
	 * @param listener 
	 */
	once(event: string, listener: (...args: Array<any>) => void): EventEmitter

	/**
	 * 發射一個事件，將會依次調用註冊的監聽器
	 * @param event 
	 * @param args 
	 * @returns 返回是否存在監聽器
	 */
	emit(event: string, ...args: Array<any>): boolean

	/**
	 * 刪除指定監聽器
	 * @param event 
	 * @param listener 
	 */
	removeListener(event: string, listener: (...args: Array<any>) => void): EventEmitter
	/**
	 * 刪除所有相關的監聽器
	 * @param event 
	 */
	removeAllListeners(event?: string)
}
```

