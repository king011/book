# WordPress

WordPress 是一個 以 PHP 和 MySQL 爲平臺的 開源(GPLv2) 部落格軟體和內容系統

* 官網 [https://wordpress.org/](https://wordpress.org/)
* 源碼 [https://core.trac.wordpress.org/browser](https://core.trac.wordpress.org/browser)
* docker [https://hub.docker.com/_/wordpress](https://hub.docker.com/_/wordpress)