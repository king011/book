# 圖標

在 flutter 中 可以像web開發一樣是 使用字體 圖標 flutter 默認自帶了 一組 material 圖標 使用 如下 配置 啓用

```
#info="pubspec.yaml"
flutter:

  uses-material-design: true
```

```
Center(
	child: Text(
		"\uE914 \uE000 \uE90D",
		style: TextStyle(
			fontFamily: "MaterialIcons",
			fontSize: 24.0,
			color: Colors.green,
		),
	),
),
```
**\uE914 \uE000 \uE90D** 是三個 圖標的 編碼

# Icon Icons

flutter 提供了 Icon 來專門處理 字體圖標 Icons 包含了 所有 material 圖標的 定義

```
Center(
	child: Row(
		children: <Widget>[
			Icon(Icons.accessible, color: Colors.green),
			Icon(Icons.error, color: Colors.green),
			Icon(Icons.fingerprint, color: Colors.green),
		],
	),
)
```

# 自定義圖標
將 字體圖標 打包成爲 ttf 格式 並在 flutter 中 作爲資源定義 即可 使用

[http://fluttericon.com/](http://fluttericon.com/)  提供了在線 打包 svg 到 flutter 資源的 功能

```
#info="pubspec.yaml"
flutter:
  fonts:
    - family: MyFlutterApp # 指定字體名
      fonts:
        - asset: assets/fonts/MyFlutterApp.ttf # 指定字體 檔案
```

```
import 'my_flutter_app_icons.dart';

Icon(MyFlutterApp.access_time, color: Colors.green),
Icon(MyFlutterApp.accessible, color: Colors.green),
```

