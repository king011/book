# 應用發佈

k8s 提供了多種發佈應用的方式 以應對不同情況

* 無狀態應用
* 有狀態應用
* StatefulSet

# [無狀態應用](https://kubernetes.io/docs/tutorials/stateless-application/)

無狀態應用 通常沒有需要持久化的數據 只是單純提供計算能力

一般將 無狀態應用 
1. 使用 Deployment 管理運行的容器
2. 創建一個 Service 將 容器服務 暴露給 外部訪問

```
apiVersion: v1
kind: Service
metadata:
  name: expose-example-service
  labels:
    app.kubernetes.io/name: expose-example
spec:
  ports:
  - port: 8080 # endpoint 端口
    protocol: TCP # UDP, TCP, or SCTP. Default is TCP
    nodePort:  # 指定 外部訪問端口 如果爲空 自動生成一個可用的 端口
  clusterIP:  # 指定 ip 如果爲空 自動生成一個可用的 ip
  type: NodePort
  selector:
    app.kubernetes.io/name: expose-example
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/name: expose-example
  name: expose-example-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app.kubernetes.io/name: expose-example
  template:
    metadata:
      labels:
        app.kubernetes.io/name: expose-example
    spec:
      containers:
      - image: gcr.io/google-samples/kubernetes-bootcamp:v1
        name: expose-example
        ports:
        - containerPort: 8080
```

```
curl http://<external-ip>:<port>
```

# [有狀態應用](https://kubernetes.io/docs/tutorials/stateful-application/)

有狀態應用只是相對於無狀態應用 增加了持久層 k8s 使用 PVC 來提供

```
apiVersion: v1
kind: Service
metadata:
  name: stateful-mariadb-service
  labels:
    app: stateful-mariadb-example
spec:
  ports:
  - port: 3306 # endpoint 端口
    protocol: TCP # UDP, TCP, or SCTP. Default is TCP
    nodePort:  # 指定 外部訪問端口 如果爲空 自動生成一個可用的 端口
  clusterIP:  # 指定 ip 如果爲空 自動生成一個可用的 ip
  type: NodePort
  selector:
    app: stateful-mariadb-example
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: stateful-mariadb-pvc
  labels:
    app: stateful-mariadb-example
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: stateful-mariadb-example
  name: stateful-mariadb-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: stateful-mariadb-example
  template:
    metadata:
      labels:
        app: stateful-mariadb-example
    spec:
      containers:
      - image: mariadb:10.5.9
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: '123'
        name: stateful-mariadb
        ports:
        - containerPort: 3306
        volumeMounts:
        - name: stateful-mariadb-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: stateful-mariadb-storage
        persistentVolumeClaim:
          claimName: stateful-mariadb-pvc
```

```
mysql -h <external-ip> -P <port> -uroot -p123
```

# [StatefulSet](https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/)

StatefulSet 是有狀態應用集羣

* 對於有 N 個副本的 StatefulSet，Pod 按照 0 ... N-1 的順序創建
* Pod 擁有一個唯一順序的索引和穩定的網路身份標識
* Pod 擁有穩定的 PVC

下述例子中的 nfs-storage-class 是已經創建好的 存儲類 用於 提供動態供應
```
apiVersion: v1
kind: Service
metadata:
  name: statefulset-mariadb-service
  labels:
    app: statefulset-mariadb-example
spec:
  ports:
    - port: 3306 # endpoint 端口
      protocol: TCP # UDP, TCP, or SCTP. Default is TCP
  clusterIP: None
  selector:
    app: statefulset-mariadb-example
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app: statefulset-mariadb-example
  name: statefulset-mariadb
spec:
  serviceName: "statefulset-mariadb-service" # StatefulSet 需要指定 此值
  replicas: 2
  selector:
    matchLabels:
      app: statefulset-mariadb-example
  template:
    metadata:
      labels:
        app: statefulset-mariadb-example
    spec:
      containers:
        - image: mariadb:10.5.9
          env:
            - name: MYSQL_ROOT_PASSWORD
              value: "123"
          name: statefulset-mariadb
          ports:
            - containerPort: 3306
          volumeMounts:
            - name: statefulset-mariadb-storage
              mountPath: /var/lib/mysql
  volumeClaimTemplates: # 使用動態供應
    - metadata:
        name: statefulset-mariadb-storage
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "nfs-storage-class"
        resources:
          requests:
            storage: 100Mi
```

statefulset-mariadb-service 通過 `clusterIP: None` 創建了一個無頭服務 爲集羣內部生成了 dns記錄 用戶 服務互相訪問 其名稱格式爲 `<StatefulSet name>-<index>.<Service name>`


busybox 鏡像 帶了一個 nslookup 工具 可以用於測試 內部 dns 是否正確
```
$ kubectl run -i --tty --image busybox:1.28 dns-test --restart=Never --rm
/ # nslookup statefulset-mariadb-0.statefulset-mariadb-service
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      statefulset-mariadb-0.statefulset-mariadb-service
Address 1: 192.168.219.69 statefulset-mariadb-0.statefulset-mariadb-service.default.svc.cluster.local
/ # nslookup statefulset-mariadb-1.statefulset-mariadb-service
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      statefulset-mariadb-1.statefulset-mariadb-service
Address 1: 192.168.219.72 statefulset-mariadb-1.statefulset-mariadb-service.default.svc.cluster.local
```