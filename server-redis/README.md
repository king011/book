# redis

redis 是一個 ANSI C 實現的開源(BSD) 分散式 高效快速的 鍵值數據庫

* 官網 [https://redis.io/](https://redis.io/)
* 源碼 [https://github.com/redis/redis](https://github.com/redis/redis)
* docker [https://hub.docker.com/_/redis](https://hub.docker.com/_/redis)