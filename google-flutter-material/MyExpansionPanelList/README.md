# ExpansionPanelList

[ExpansionPanelList](https://docs.flutter.io/flutter/material/ExpansionPanelList-class.html) 是面板列表 可以容納多個 面板

ExpansionPanelList 必須被包裹在一個 無線空間中 可以簡單的 放到 SingleChildScrollView 中即可

ExpansionPanelList 的 children 是 List&lt;[ExpansionPanel](https://docs.flutter.io/flutter/material/ExpansionPanel-class.html)&gt; 定義每個 面板的 信息

![](assets/ExpansionPanelList.gif)

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: SingleChildScrollView(
        child: MyExpansionPanelList(),
      ),
    );
  }
}

/// 派生 一個 StatefulWidget 來 管理 面板展開
class MyExpansionPanelList extends StatefulWidget {
  MyExpansionPanelList({Key key}) : super(key: key);

  @override
  _MyExpansionPanelListState createState() =>
      _MyExpansionPanelListState();
}

class _MyExpansionPanelListState extends State<MyExpansionPanelList> {
  /// 定義 當前展開的 面板
  final List<Panel> _panels = panels;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return ExpansionPanelList(
      /// 每當 面板 展開 關閉時 會回調此 函數
      expansionCallback: (panelIndex, isExpanded) {
        final expanded = !isExpanded;
        if (expanded != _panels[panelIndex].expanded) {
          setState(() {
            _panels[panelIndex].expanded = expanded;
          });
        }
      },

      /// 子面板 數組
      children: _panels.map((Panel paenl) {
        return ExpansionPanel(
          // 面板 展開狀態
          isExpanded: paenl.expanded,
          // 創建 面板 標題
          headerBuilder: (context, isExpanded) => ListTile(
                title: Text('${paenl.title}'),
              ),
          // 創建 面板 內容
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(paenl.icon, size: 128.0, color: textStyle.color),
                Text(paenl.title, style: textStyle),
              ],
            ),
          ),
        );
      }).toList(),
    );
  }
}

/// 定義面板 數據
class Panel {
  Panel({this.title, this.icon, this.expanded = false});
  String title;
  IconData icon;
  bool expanded;
}

List<Panel> panels = <Panel>[
  Panel(title: 'CAR', icon: Icons.directions_car, expanded: true),
  Panel(title: 'BICYCLE', icon: Icons.directions_bike),
  Panel(title: 'BOAT', icon: Icons.directions_boat),
  Panel(title: 'BUS', icon: Icons.directions_bus),
  Panel(title: 'TRAIN', icon: Icons.directions_railway),
  Panel(title: 'WALK', icon: Icons.directions_walk),
];
```