# [部署應用](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/)

一旦運行了 k8s 集羣，就可以在騎上部署容器化應用程序。爲此，需要創建 **Deployment** 配置。Deployment 指揮 k8s 如何創建和更新應用實例。創建 Deployment 後 k8s master 將應用程序實例調度到集羣中的各個節點上。

創建實例後，Deployment 控制器會持續監視這些實例。如果託管實例的節點關閉或被刪除，則 Deployment 控制器會將該實例替換爲集羣中另一個節點上的實例。這提供了一種自我修復機制來解決機器故障維護問題。

# 部署第一個 k8s 應用

![](assets/module_02_first_app.svg)

1. 使用 `kubectl create deployment` 來創建一個 deployment 以部署應用

	```
	kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
	```
	
	此命令部署了一個名稱爲 kubernetes-bootcamp 的 deployment k8s 執行了下述操作
	
	* 搜索可以在其中運行應用實例的適合 Node
	* 安排應用在此 Node 上運行
	* 配置集羣以在需要時在新節點上重新安排實例

1. 使用 `kubectl get deployments` 來列出部署的實例

	```
	kubectl get deployments
	```
1. k8s 內部運行的 Pod 在私有的隔離網路上運行。默認情況下它同一 k8s 集羣中的其它 Pod 和 服務中可見，但在網路外部不可見。

	kubectl 可以創建一個代理 該代理會將通信轉發到集羣範圍的專用網路中。
	
	```
	kubectl proxy
	```
	
	`kubectl proxy` 會監聽 `127.0.0.1:8001` 將其轉發到 集羣
	
1. 此時你可以同個 `localhost:8001` 來訪問部署的應用

	```
	curl http://localhost:8001/version
	```
	
1. 如果 無法訪問可能是需要爲代理指定 Pod 名稱 需要將 名稱設置到 POD_NAME 環境變量

	```
	export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
	echo Name of the Pod: $POD_NAME
	```
	