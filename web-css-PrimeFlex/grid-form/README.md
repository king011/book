# [FormLayout](https://www.primefaces.org/primeflex/formlayout)

可以使用 Grid System 來對 form 表單進行微調，通常你需要爲表單項目指定 class field 來使它展現 PrimeFlex 定義的效果

## Vertical

這是一個垂直的表單

![](assets/v0.png)

```
<div class="card">
    <h5>Vertical</h5>
    <div class="field">
        <label for="firstname1">Firstname</label>
        <input id="firstname1" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
    </div>
    <div class="field">
        <label for="lastname1">Lastname</label>
        <input id="lastname1" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
    </div>
</div>
```

你可以使用 grid 來將表單項 放到同一行中

![](assets/v1.png)

```

<div class="card">
    <h5>Vertical and Grid</h5>
    <div class="formgrid grid">
        <div class="field col">
            <label for="firstname2">Firstname</label>
            <input id="firstname2" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
        <div class="field col">
            <label for="lastname2">Lastname</label>
            <input id="lastname2" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
    </div>
</div>
```

## Horizontal

你可以使用 grid 來將 標籤 和 input 設置到同行中，並且可以指定標籤寬度

![](assets/h0.png)

```
<div class="field grid">
    <label for="firstname3" class="col-fixed" style="width:100px">Firstname</label>
    <div class="col">
        <input id="firstname3" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary">
    </div>
</div>
<div class="field grid">
    <label for="lastname3" class="col-fixed" style="width:100px">Lastname</label>
    <div class="col">
        <input id="lastname3" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary">
    </div>
</div>
```

你也可以指定 標籤 和 input 佔用的寬度列數

![](assets/h1.png)

```
<div class="card">
    <h5>Horizontal</h5>
    <div class="field grid">
        <label for="firstname4" class="col-12 mb-2 md:col-2 md:mb-0">Firstname</label>
        <div class="col-12 md:col-10">
            <input id="firstname4" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
    </div>
    <div class="field grid">
        <label for="lastname4" class="col-12 mb-2 md:col-2 md:mb-0">Lastname</label>
        <div class="col-12 md:col-10">
            <input id="lastname4" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
    </div>
</div>
```

## Inline
 這是一個 inline 的例子，通過指定 class formgroup-inline 來獲取效果
 
 ![](assets/inline.png)
 
 ```
<div class="formgroup-inline">
    <div class="field">
        <label for="firstname5" class="p-sr-only">Firstname</label>
        <input id="firstname5" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary" placeholder="Firstname">
    </div>
    <div class="field">
        <label for="lastname5" class="p-sr-only">Lastname</label>
        <input id="lastname5" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary" placeholder="Lastname">
    </div>
    <button type="button" class="text-white bg-primary-500 border-primary-500 px-3 py-2 text-base border-1 border-solid border-round cursor-pointer transition-all transition-duration-200 hover:bg-primary-600 hover:border-primary-600 active:bg-primary-700 active:border-primary-700">Submit</button>
</div>
```

## Checkbox

這是一個複選框的例子，通過使用 class field-checkbox 來獲取效果

![](assets/checkbox.png)

```
<div class="card">
    <h5>Vertical Checkbox</h5>
    <div class="field-checkbox">
        <input type="checkbox" id="city1">
        <label for="city1">Chicago</label>
    </div>
    <div class="field-checkbox">
        <input type="checkbox" id="city2">
        <label for="city2">Los Angeles</label>
    </div>

    <h5>Horizontal Checkbox</h5>
    <div class="formgroup-inline">
        <div class="field-checkbox">
            <input type="checkbox" id="city3">
            <label for="city3">Chicago</label>
        </div>
        <div class="field-checkbox">
            <input type="checkbox" id="city4">
            <label for="city4">Los Angeles</label>
        </div>
    </div>
</div>
```

## Radiobutton
這是一個 radiobutton 的例子，通過使用 class field-radiobutton 來獲取效果，通過設置相同的 form name 屬性來將 radiobutton 設置爲同一組

![](assets/radiobutton.png)

```
<div class="card">
    <h5>Vertical RadioButton</h5>
    <div class="field-radiobutton">
        <input type="radio" name="vcity" id="city5">
        <label for="city5">Chicago</label>
    </div>
    <div class="field-radiobutton">
        <input type="radio" name="vcity" id="city6">
        <label for="city6">Los Angeles</label>
    </div>

    <h5>Horizontal RadioButton</h5>
    <div class="formgroup-inline">
        <div class="field-radiobutton">
            <input type="radio" name="hcity" id="city7">
            <label for="city7">Chicago</label>
        </div>
        <div class="field-radiobutton">
            <input type="radio" name="hcity" id="city8">
            <label for="city8">Los Angeles</label>
        </div>
    </div>
</div>
```

## Help Text

你可以在 class field 的子項中使用 small 標籤來爲 field 顯示一些幫助信息

![](assets/help-text.png)

```
<div class="card">
    <h5>Help Text</h5>
    <div class="field">
        <label for="username">Username</label>
        <input id="username" type="username" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full" aria-describedby="username-help">
        <small id="username-help">Enter your username to reset your password.</small>
    </div>
</div>
```

## Advanced

這是一個綜合使用各種 class 的例子

![](assets/advanced.png)

```
<div class="card">
    <h5>Advanced</h5>
    <div class="formgrid grid">
        <div class="field col-12 md:col-6">
            <label for="firstname6">Firstname</label>
            <input id="firstname6" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
        <div class="field col-12 md:col-6">
            <label for="lastname6">Lastname</label>
            <input id="lastname6" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
        <div class="field col-12">
            <label for="address">Address</label>
            <textarea id="address" type="text" rows="4" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full"></textarea>
        </div>
        <div class="field col-12 md:col-6">
            <label for="city">City</label>
            <input id="city" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
        <div class="field col-12 md:col-3">
            <label for="state">State</label>
            <select id="state" class="w-full text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round outline-none focus:border-primary" style="appearance: auto">
                <option>Arizona</option>
                <option>California</option>
                <option>Florida</option>
                <option>Ohio</option>
                <option>Washington</option>
            </select>
        </div>
        <div class="field col-12 md:col-3">
            <label for="zip">Zip</label>
            <input id="zip" type="text" class="text-base text-color surface-overlay p-2 border-1 border-solid surface-border border-round appearance-none outline-none focus:border-primary w-full">
        </div>
    </div>
</div>
```