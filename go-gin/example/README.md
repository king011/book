# example

```
package main

import (
	"errors"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
)

const addr = `localhost:8080`

func main() {
	l, e := net.Listen(`tcp`, addr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println(`work at`, addr)
	// 創建 默認設置的 gin 引擎
	router := gin.Default()
	// 使用中間件
	router.Use(tarceHandler)
	// 使用 gzip 中間件 壓縮
	// router.Use(
	//  gzip.Gzip(gzip.DefaultCompression,
	//      // 壓縮時 排除 這些 後綴名的 檔案
	//      gzip.WithExcludedExtensions([]string{`.go`, `.pdf`, `.mp4`, `.mp3`, `.png`, `.gif`, `.jpeg`, `.jpg`}),
	//  ),
	// )
	gz := gzip.Gzip(gzip.DefaultCompression)

	// 註冊 路由
	router.GET(`/text`, text)
	router.GET(`/json`, json)
	router.GET(`/json/pure`, pureJSON)
	router.GET(`/xml`, xml)
	router.GET(`/err`, json)
	// 模糊匹配 必須在 * 後跟上一個參數名 會將模糊匹配內容 bind到此參數
	router.GET(`/angular/*path`, angular)
	// 綁定參數
	router.GET(`/book/:id/:name`, book)
	// 返回 檔案
	router.GET(`/main.go`, gz, source)

	// 分塊傳輸
	router.GET(`/chunked`, chunked)

	// 分組路由
	v1 := router.Group(`/v1`)
	{
		v1.GET("/version", func(c *gin.Context) {
			c.String(http.StatusOK, "v1 version")
		})
		v1.GET("/id", func(c *gin.Context) {
			c.String(http.StatusOK, "v1 id =1")
		})
	}
	v2 := router.Group(`/v2`)
	{
		v2.GET("/version", func(c *gin.Context) {
			c.String(http.StatusOK, "v2 version")
		})
		v2.GET("/id", func(c *gin.Context) {
			c.String(http.StatusOK, "v2 id =2")
		})
	}
	// 運行 服務器
	router.RunListener(l)
}
func text(c *gin.Context) {
	c.String(http.StatusOK, `%s is an idea`, `cerberus`)
}
func json(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		`name`: `<a>測試</a>cerberus`,
		`lv`:   1,
	})
	// {"lv":1,"name":"\u003ca\u003e測試\u003c/a\u003ecerberus"}
}
func pureJSON(c *gin.Context) {
	// pureJSON 不會轉義 html 標籤
	c.PureJSON(http.StatusOK, gin.H{
		`name`: `<a>測試</a>cerberus`,
		`lv`:   1,
	})
	// {"lv":1,"name":"<a>測試</a>cerberus"}
}
func xml(c *gin.Context) {
	c.XML(http.StatusOK, gin.H{
		`name`: `cerberus`,
		`lv`:   1,
	})
}

func source(c *gin.Context) {
	//http.ServeFile(c.Writer, c.Request, `main.go`)
	c.File(`main.go`)
}
func chunked(c *gin.Context) {
	c.Header(`Content-Type`, `application/json`)
	strs := []string{
		`{"name":`,
		`"cerberus`,
		`"`,
		`,"lv":1}`,
	}
	for _, str := range strs {
		// 傳輸塊數據
		c.Writer.Write([]byte(str))
		c.Writer.Flush()
		time.Sleep(time.Second)
	}
}
func angular(c *gin.Context) {
	c.String(http.StatusOK, c.Param("path"))
}
func book(c *gin.Context) {
	log.Println("book")
	c.String(http.StatusOK, `%s %s`, c.Param("id"), c.Param("name"))
}
func tarceHandler(c *gin.Context) {
	log.Println(`start`, c.FullPath())
	if c.FullPath() == "/err" {
		// 需要調用 Abourt 阻止 下個 handler 被調用
		c.AbortWithError(http.StatusForbidden, errors.New("test error"))
		return
	}
	// 掛起中間件 next 之前的 代碼 在 handler 之前執行
	c.Next()

	// next 之後的 代碼 在 handler 之後執行
	log.Println(`stop`, c.FullPath())
}
```