# [io](https://pkg.go.dev/io)

io 庫 提供了 基礎的 io 接口 和 工具 相比其它語言 golang io 提供了 更簡介 但 方便的 io 操作

# 錯誤
```go
// EOF 在 Read 時 用於返回 數據沒有 更多可讀內容 正常結束
var EOF = errors.New("EOF")

```

> 通常在 除了 CopyN 之外 遇到 EOF 返回的讀取到的 數據長度 都是0
> 

# Copy
```go
// Copy 將 數據從 Reader 拷貝到 Writer 中 直到 遇到 任何錯誤
//
// Reader 返回 EOF 時 Copy 會返回 nil 並 結束 copy 操作
func Copy(dst Writer, src Reader) (written int64, err error){ ... }

// CopyBuffer 同 Copy 只是 使用 buf 作爲臨時 緩衝區
//
// Copy 是 CopyBuffer(dst,src,nil) 的語法糖
// 
// buf 如果不爲 nil 則 len(buf) 不能爲0
func CopyBuffer(dst Writer, src Reader, buf []byte) (written int64, err error){ ... }

// CopyN 從 Reader 中 copy 最多 n 字節數據到 Writer
//
// 如果 written < n 並且 沒有 錯誤 則 返回EOF
func CopyN(dst Writer, src Reader, n int64) (written int64, err error){ ... }
```

> CopyN 在 Reader 沒有足夠數據時 會 返回 EOF 而非 nil
> 

在 WriteTo/ReaderFrom 中使用 Copy/CopyBuffer 需要格外小心  
不要將 原始 src dst 傳給 Copy/CopyBuffer  
因爲 如果原始 src dst 實現了 WriteTo/ReaderFrom 則 Copy/CopyBuffer 會直接調用 WriteTo/ReaderFrom 這將造成 死循環的相互調用  
(比如 將 兩個 net.TCPConn 使用 io.Copy 則可能會 ReaderFrom 和 io.Copy 的互相死循環調用)

# Write Read
```go
// 如果 w 實現了 WriteString 則調用 否則 調用 w.Write([]byte(s))
func WriteString(w Writer, s string) (n int, err error) { ... }

// ReadAtLeast 讀取 [min,len(buf)] 字節數據 到 buf 
//
// 如果 複製的數據數 n < min 則返回 錯誤
//
// 如果 Reader 返回 EOF 則 n == 0 ReadAtLeast 返回 EOF 否則 返回 ErrUnexpectedEOF
//
// 如果 len(buf) < min 返回 ErrShortBuffer 錯誤
func ReadAtLeast(r Reader, buf []byte, min int) (n int, err error) { ... }

// ReadFull 是 ReadAtLeast(r,buf,len(buf)) 的 語法糖
func ReadFull(r Reader, buf []byte) (n int, err error) { ... }
```

# LimitedReader

LimitedReader 包裝了一個 Reader 限制了 最多從 Reader 中 讀取的 數據量

一旦 達到 限制 在 Read 將 返回 EOF 如果原 Read 返回了 錯誤 也將 直接返回

輔助函數 LimitReader 將創建一個 \*LimitedReader

```go
type LimitedReader struct {
    R Reader // underlying reader
    N int64  // max bytes remaining
}

func LimitReader(r Reader, n int64) Reader
```

```go
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	r := strings.NewReader("cerberus is an idea")
	lr := io.LimitReader(r, 8)
	b, e := ioutil.ReadAll(lr)
	if e != nil {
		log.Fatalln(e)
	}
	// ture
	fmt.Println(string(b) == "cerberus")
	_, e = lr.Read(nil)
	// true
	fmt.Println(io.EOF == e)

	b, e = ioutil.ReadAll(r)
	if e != nil {
		log.Fatalln(e)
	}
	// true
	fmt.Println(string(b) == " is an idea")
}
```

# MultiWriter

MultiWriter 組合 多個 Writer 將 數據依次 Write 到 每個 Writer

當 任何一個 Writer 返回錯誤 則 返回錯誤哦 不會繼續 向 後續 Writer 進行 Write

```go
#info=false
func MultiWriter(writers ...Writer) Writer
```

```go
package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"strings"
)

func main() {
	r := strings.NewReader("cerberus is an idea")

	var buf1, buf2 bytes.Buffer
	w := io.MultiWriter(&buf1, &buf2)

	if _, e := io.Copy(w, r); e != nil {
		log.Fatalln(e)
	}

	fmt.Println(buf1.String())
	fmt.Println(buf2.String())
}
```

# Pipe
Pipe 創建一個 goroutine safe 的 無緩存 內存 管道 Writer 的值將被 Reader 讀出

Writer 將被阻塞 直到 數據被 Reader 讀出 或 Writer/Reader 被關閉\[返回 ErrClosedPipe 錯誤\]

Reader 將被阻塞 直到 讀取到數據, 或 Writer 被關閉 返回 EOF, Reader 被關閉 返回 ErrClosedPipe

```go
package main

import (
	"fmt"
	"io"
	"time"
)

func main() {
	r, w := io.Pipe()
	time.AfterFunc(time.Second, func() {
		defer w.Close()

		_, e := w.Write([]byte("123"))
		if e != nil {
			fmt.Println(e)
			return
		}
	})

	b := make([]byte, 10)
	for {
		n, e := r.Read(b)
		if e != nil {
			if e == io.EOF {
				break
			}
			fmt.Println("r", e)
			continue
		}
		fmt.Println("read", string(b[:n]))
	}
}
```
