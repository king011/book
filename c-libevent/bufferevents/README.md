# [bufferevents](https://libevent.org/doc/bufferevent_8h.html)

```
#include <event2/bufferevent.h>
```

大多時候，應用程序除了響應事件外還想執行一定量的數據緩衝。例如，當我們想寫入數據時，通常的運行模式如下：
1. 決定我們要將一些數據寫入 socket，將該數據放入緩衝區
2. 等待 socket 變得可寫
3. 儘可能多的寫入數據
4. 記住我們寫入了多少，如果我們還有更多數據要寫，等待連接再次變得可寫

這種緩衝 IO 模式很常見，所以 libevent 爲其提供了一種通用的機制。bufferevents 由底層(例如 socket) 讀取緩衝區和寫入緩衝區組成。與在底層傳輸準備好讀取或寫入時提供回調的常規事件不同，bufferevent 在讀取到或寫入了足夠的數據值調用用戶提供的回調

有多種 bufferevent 提供了共享的接口，目前包含如下多種 bufferevent:

* **socket-based bufferevents** 它使用 event\_\* 接口作爲後端，從底層 socket 發送 和 接收 數據
* **asynchronous-IO bufferevents** 使用 Windows IOCP 接口向 socket 發送和接收數據 (僅 Windows 且是實驗性質的)
* **filtering bufferevents** 在將傳入和傳出數據傳遞給 bufferevent 對象之前處理傳入傳出的 bufferevent。例如壓縮或轉換數據
* **paired bufferevents** 兩個 bufferevents 組件相互傳遞數據

bufferevent 有一個輸出緩衝區和輸入緩衝區都是 evbuffers，當你要發送數據時將數據添加到輸出緩衝區，當有數據可讀時從輸入緩衝區獲取數據

# 回調和水印

每個 bufferevent 都有兩個數據相關的回調：一個讀回調和一個寫回調。默認情況下，只要從底層傳輸中讀取任何數據，就會調用讀取回調；而只要輸出緩衝區中的足夠數據被清空到底層傳輸，就會調用寫入回調。你可以通過調整 bufferevent 的讀寫水印來覆蓋這些函數的行爲

每個 bufferevent 都有四個水印：
* **Read low-water mark** 每當發生將 bufferevent 的輸入緩衝區留在此級別或更高級別的讀取時，將調用 bufferevent 的讀取回調。默認爲 0，因此每次讀取都會導致調用讀取回調
* **Read high-water mark** 如果 bufferevent 的輸入緩衝區達到此級別，bufferevent 將停止讀取，直到從輸入緩衝區中排除足夠的數據使緩衝區再次低於它。默認爲無限制，這樣我們就不會因爲輸入緩衝區的大小而停止讀取
* **Write low-water mark** 每當發生將我們帶到此級別或以下級別的寫入時，我們都會調用寫入回調。默認爲 0，因爲除非輸出緩衝區爲空，否則不會調用寫回調
* **Write high-water mark** 不直接由 bufferevent 使用，當 bufferevent 用作另一個 bufferevent 的底層傳輸時，此水印可能具有特殊含義。請參閱下面有關過濾緩衝區事件的說明

bufferevent 也有一個 error 或 event 回調，它被調用以告知用戶有非面向數據的事件，例如當連接關閉或發送錯誤時。它定義了以下事件標誌：
* **BEV\_EVENT\_READING** 對 bufferevent 的讀取操作期間發生了一個事件。具體事件由其它標誌確定
* **BEV\_EVENT\_WRITING** 對 bufferevent 的寫入操作期間發生了一個事件。具體事件由其它標誌確定
* **BEV\_EVENT\_ERROR** 緩衝區操作期間發生了錯誤。有關錯誤的更多信息可以調用 EVUTIL\_SOCKET\_ERROR() 獲取
* **BEV\_EVENT\_TIMEOUT** 緩衝區超時事件
* **BEV\_EVENT\_EOF** 在 bufferevent 上讀取到了 EOF 的結束指示
* **BEV\_EVENT\_CONNECTED** 在 bufferevent 上完成了連接請求

# 標誌

你可以在創建 bufferevent 時使用一個或多個標誌來改變其行爲。公認的標誌是：
* **BEV\_OPT\_CLOSE\_ON\_FREE** 當 bufferevent 被釋放時，關閉底層傳輸。這將關閉底層 socket，ssl，釋放底層 bufferevent 等
* **BEV\_OPT\_THREADSAFE** 自動爲 bufferevent 分配鎖，以便從多線程使用它時安全的
* **BEV\_OPT\_DEFER\_CALLBACKS** 延遲回調
* **BEV\_OPT\_UNLOCK\_CALLBACKS** 默認情況下當 bufferevent 設置了BEV\_OPT\_THREADSAFE 時，只要調用任何用戶提供的回調，bufferevent 的鎖就會被持有。設置此選項會使 Libevent 調用你的回調時釋放 bufferevent 的鎖

默認情況下，當相應條件發生時，會立刻執行回調。當依賴關係變得複製時，這種立刻調用可能會造成麻煩。例如，假設有一個回調在 evbuffer A 變空時將數據移入 evbuffer A，另外一個回調在 evbuffer A 變滿時處理數據。由於這些調用都發生在堆棧上，因此如果依賴性變得足夠複製，你可能會有堆棧溢出的風險

# 使用 socket-based 

最簡單的 bufferevents 時基於 socket 的。libevent 在底層會檢測 socket 何時準備好進行讀取和寫入操作，並使用底層網路調用(如 readv writev WSASend WSARecv) 來傳輸和接收數據

你可以使用 [bufferevent\_socket\_new(eb, fd, options)](https://libevent.org/doc/bufferevent_8h.html#a71181be5ab504e26f866dd3d91494854) 來創建一個使用 socket 作爲後端的 bufferevent，成功返回 bufferevent 指針，失敗返回 NULL

* **eb** 是一個 event\_base 指針
* **fd** 是檔案描述符不允許是管道。可以設置爲 -1 只是之後需要使用 bufferevent_setfd/bufferevent_socket_connect 來設置它
* **options** 是 0 或者多個 BEV\_OPT\_\* 標誌

你可以使用 [bufferevent\_socket\_connect(bev, addr, socklen)](https://libevent.org/doc/bufferevent_8h.html#aefc782ac77411bdd712be30d6268af2e) 來連接服務器，參數和標準的 connet 類似，成功返回 0 失敗返回 -1

也可以使用 [bufferevent\_socket\_connect\_hostname(bev, esb, family, hostname, port)](https://libevent.org/doc/bufferevent_8h.html#a5c3c313f2ff0d79a1889e508b37d9dbe) 來連接一個域名，但需要準備一個 evdns\_base，成功返回 0 失敗返回 -1。

你可以在回調函數裏面使用 `evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR())` 來獲取 socket 的相關錯誤消息

當使用 dns 時，在回調函數中如果發現連接出錯，你需要線使用 [bufferevent\_socket\_get\_dns\_error(bev)](https://libevent.org/doc/bufferevent_8h.html#aa533016e5ffc2ed558a5d85f9fbe2fd5) 來獲取是否是 dns 錯誤，使用 evutil\_gai\_strerror(err) 獲取錯誤代碼的文本字符串


# 釋放 bufferevent

你可以調用 [bufferevent\_free(bev)](https://libevent.org/doc/bufferevent_8h.html#a8baa699f526f237c0d33f618f073c1cc) 來釋放 bufferevent。 bufferevents 內部使用來引用計數，所以如果 bufferevent 有掛起的延遲回調，它不會被刪除直到回調完成

但是 bufferevent\_free 函數會儘快釋放 bufferevent。如果有呆處理的數據要寫入 bufferevent，它可能不會在 bufferevent 被釋放之前被刷新。

如果設置了 **BEV\_OPT\_CLOSE\_ON\_FREE** 標誌，bufferevent 會在釋放時關閉與之關聯的 底層 socket 或 bufferevent

# 操作回調水印

```
typedef void (*bufferevent_data_cb)(struct bufferevent *bev, void *ctx);
typedef void (*bufferevent_event_cb)(struct bufferevent *bev,
    short events, void *ctx);

void bufferevent_setcb(struct bufferevent *bufev,
    bufferevent_data_cb readcb, bufferevent_data_cb writecb,
    bufferevent_event_cb eventcb, void *cbarg);

void bufferevent_getcb(struct bufferevent *bufev,
    bufferevent_data_cb *readcb_ptr,
    bufferevent_data_cb *writecb_ptr,
    bufferevent_event_cb *eventcb_ptr,
    void **cbarg_ptr);
```

你可以調用 [bufferevent\_setcb(bev, readcb, writecb, eventcb, ctx)](https://libevent.org/doc/bufferevent_8h.html#a031df52978c5237b70fb8ae7df572c97) 來更改回調函數。

* **bev** 一個 bufferevent 指針
* **readcb** 當有數據要讀取時的回調，如果爲 NULL 則禁用此回調
* **writecb** 當底層設備準備好寫入時回調，如果爲 NULL 則禁用此回調
* **eventcb** 當發生來讀寫之外的事件時回調，如果爲 NULL 則禁用此回調
* **ctx** 傳遞給回調函數的自定義參數

你可以通過傳遞 NULL 來禁用相應的回調。請注意 所有回調都共享同一個 ctx 自定義參數。你可以調用 [bufferevent\_getcb(bev, readcb_ptr, writecb_ptr, eventcb_ptr, ctx_prt)](https://libevent.org/doc/bufferevent_8h.html#ae58a293995d110362cc1fc695678a928) 來返回當前設置的回調信息，任何的 NULL 參數都將被忽略

你可以調用 [bufferevent\_enable(bev, event)](https://libevent.org/doc/bufferevent_8h.html#aa8a5dd2436494afd374213b99102265b) 和 [bufferevent\_disable(bev, event)](https://libevent.org/doc/bufferevent_8h.html#a4f3974def824e73a6861d94cff71e7c6)來啓用或禁用 EV\_READ | EV\_WRITE，成功返回 0 失敗返回 -1。

你不需要在輸出緩衝區爲空時禁用寫入: bufferevent 會自動停止寫入，並在有數據可寫入時再次重新啓動。同樣，當輸入緩衝區達到其高水位線時無需禁用讀取：bufferevent自動停止讀取，並在有空間可讀時再次重新啓動。默認情況下，新創建的 bufferevent 都是啓用寫入禁用讀取的，你可以調用 [bufferevent\_get\_enabled(bev)](https://libevent.org/doc/bufferevent_8h.html#ab78b8248210f0fadac8b5924dde5a3e7) 返回當前啓用的 EV\_READ | EV\_WRITE。

建議調用 [bufferevent\_setwatermark(bev, events, lowmark, highmark)](https://libevent.org/doc/bufferevent_8h.html#ae23c61cdaf3f74d0fea96d41aa6f7783) 函數爲 bufferevent 設置 讀取/寫入 水印。否則沒有即時處理的數據會持續堆積到內存總重耗盡內存，特別時網路程序，與你通信的另一端程序如果存在 bug ，沒有即時的處理數據會停止發送數據給你，沒有水印的 bufferevent 可能最終會耗盡計算機全部內存。設置 0 爲高水位線相當於無限制，這也是默認值


* **bev** 一個 bufferevent 指針
* **events** EV\_READ | EV\_WRITE
* **lowmark** 低水位線， 通常應該設置爲 0。這樣每當有數據可讀就會回調。寫入的數據全部發送才會回調
* **highmark** 高水位線。對於讀取可以看作是一個最大讀取緩存，到到達此值就會暫停接收網路數據。對於寫入可以看作是一個最大寫入緩存，到達此值寫入數據就會失敗要等待數據遞交給網卡

# 操作數據

bufferevent 有輸入輸出緩衝區，將你要發送的數據添加到輸出緩衝區，從輸入緩衝區讀取收到的數據即可：

* [bufferevent\_get\_output(bev)](https://libevent.org/doc/bufferevent_8h.html#ac6e45fa1fc577c4f0e6c4689e72c0a15) 函數返回輸出緩衝區 evbuffer 指針，你只能向它添加數據
* [bufferevent\_get\_input(bev)](https://libevent.org/doc/bufferevent_8h.html#a3ddbf50178b96b3f243ddc83d67e88b0) 函數返回輸入緩衝區 evbuffer 指針，你只能從它裏面移除數據

此外你也可以直接調用 [bufferevent\_write(bev, data, size)](https://libevent.org/doc/bufferevent_8h.html#a7873bee379202ca1913ea365b92d2ed1) 或 [bufferevent\_write\_buffer(bev, buf)](https://libevent.org/doc/bufferevent_8h.html#ac9b0f97c37f116dcb579e4491dadda1d) 發送數據，成功返回 0 失敗返回 -1

同樣你也可以直接調用 [bufferevent\_read(bev, data, size)](https://libevent.org/doc/bufferevent_8h.html#a9e36c54f6b0ea02183998d5a604a00ef) 接收數據，成功返回接收字節，沒有數據或失敗返回 0。調用 [bufferevent\_read\_buffer(bev, buf)](https://libevent.org/doc/bufferevent_8h.html#abcab28c476ef6bde1faf233bfd18285c) 將接收的數據添加到 buf 中，成功返回 0 失敗返回 -1

# 讀寫超時

你可以調用 [bufferevent\_set\_timeouts(bev, timeout_read, timeout_write)](https://libevent.org/doc/bufferevent_8h.html#aec0864607ef2bf9816cda06c1c4ab83f) 爲讀寫設置一個超時回調，如果傳入 NULL 則相當於禁用相應的超時。

注意超時是在想要讀取或寫入時才計算在內的。意思是如果在 bufferevent 上禁用讀取，或者輸入緩衝區已滿(到達高水位線)，則不會啓用讀取超時。同樣，如果禁用寫入或者沒有數據可寫，則不會啓用寫入超時

# 刷新

可以調用 [bufferevent\_flush(bev, iotype, mode)] 告訴 bufferevent 強制從底層傳輸讀取或寫入儘可能多的數據，忽略可能阻止它們被寫入的其它限制。它的詳細功能取決與 bufferevent 後端類型，以 socket 爲後端的 bufferevent 沒有效果。函數失敗返回 -1，沒有數據被刷新返回 0，有數據被刷新返回 1
* **bev** 一個 bufferevent 指針
* **iotype** EV\_READ | EV\_WRITE
* **mode** BEV\_NORMAL BEV\_FLUSH BEV\_FINISHED。 BEV\_FINISHED 表示告訴對方不要再發數據了另外兩個值含義由不同後端確定具體含義

# 後端獨有函數
還存在一些特殊函數，它們只在某些類型後端的 bufferevent 才被支持

[bufferevent\_priority\_set(bev, pri)](https://libevent.org/doc/bufferevent_8h.html#a2a0eaaf330e2fa46f84dd5ec3e1299c8) 調整事件的優先級，詳細見 [event\_priority\_set(ev, pri)](https://libevent.org/doc/event_8h.html#a11a800dce1fe61de5e2506aaf603c47f)。成功返回 0，失敗返回 -1。它只支持 socket 緩衝事件。調用 [bufferevent\_get\_priority(bev)](https://libevent.org/doc/bufferevent_8h.html#a0091b3c386580cf2980039ad9a629b70) 返回讀取優先級

[bufferevent\_setfd(bev, fd)](https://libevent.org/doc/bufferevent_8h.html#a66cdf9296b02676aacf98a6277a7e756) 設置事件關聯的檔案描述符。成功時返回 0 失敗時返回 -1。它只支持 socket 作爲後端的 buffevent。調用 [bufferevent\_getfd(bev)](https://libevent.org/doc/bufferevent_8h.html#a544e049e4a8cca27ab1a98b8339fe72a) 返回關聯的檔案描述符

[bufferevent\_get\_base(bev)](https://libevent.org/doc/bufferevent_8h.html#a16aa6baf1d489b14fd31cf5e18265186) 返回關聯的 event\_base 指針

[bufferevent\_get\_underlying(bev)](https://libevent.org/doc/bufferevent_8h.html#a3e0081196d2776b92994c6034eb00b2c) 返回另外 bufferevent 用作傳輸的 bufferevent，如果沒有另外 bufferevent 返回 NULL

# Example

```
#include <event2/event.h>
#include <event2/dns.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/util.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#define TEST_CONNECT_HOSTNAME 1
#ifdef TEST_CONNECT_HOSTNAME
#define TEST_CONNECT_ADDR "localhost"
#else
#define TEST_CONNECT_ADDR "127.0.0.1"
#endif
#define TEST_CONNECT_PORT 9001

#define CLIENT_MAX_MESSAGE (32 * 1024)
#define CLIENT_STEP_CONNECT 0
#define CLIENT_STEP_SEND_HELLO 1
#define CLIENT_STEP_SEND_DATA 2
#define CLIENT_STEP_SEND_COMPLETE 3
typedef struct
{
    struct event *timeout;
    struct bufferevent *bev;
#ifdef TEST_CONNECT_HOSTNAME
    struct evdns_base *esb;
#endif
    int state;
    int step;
    uint16_t header;
    char buffer[CLIENT_MAX_MESSAGE + 1];
    char *message;
    int count;
} client_t;
void client_free(client_t *client)
{
    if (client->bev)
    {
        bufferevent_free(client->bev);
    }
    if (client->timeout)
    {
        event_free(client->timeout);
    }
#ifdef TEST_CONNECT_HOSTNAME
    if (client->esb)
    {
        evdns_base_free(client->esb, 0);
    }
#endif
    free(client);
}
int client_send(client_t *client, const char *str)
{
    size_t len = strlen(str);
    if (len > CLIENT_MAX_MESSAGE)
    {
        puts("message too large");
        return -1;
    }
    uint16_t header = len;
    if (bufferevent_write(client->bev, &header, 2))
    {
        puts("write error");
        return -1;
    }
    if (bufferevent_write(client->bev, str, len))
    {
        puts("write error");
        return -1;
    }
    return 0;
}
int client_loop(client_t *client)
{
    switch (client->step)
    {
    case CLIENT_STEP_CONNECT:
        puts("send hello");
        if (client_send(client, "hello message"))
        {
            break;
        }
        client->step = CLIENT_STEP_SEND_HELLO;
        return 0;
    case CLIENT_STEP_SEND_HELLO:
        if (memcmp(client->message, "hello message", client->header))
        {
            printf("received unexpected hello: %s\n", client->message);
            break;
        }
        if (client_send(client, "data message"))
        {
            break;
        }
        client->count = 1;
        client->step = CLIENT_STEP_SEND_DATA;
        return 0;
    case CLIENT_STEP_SEND_DATA:
        printf("recv %d: %s\n", client->count, client->message);
        client->count++;
        if (client->count < 10) // 繼續模擬數據收發
        {
            if (client_send(client, "data message"))
            {
                break;
            }
            return 0;
        }

        // 工作完成 釋放資源
        client_free(client);
        return 0;
    default:
        printf("unexpected state %d\n", client->step);
        break;
    }
    return -1;
}
void event_read_cb(struct bufferevent *bev, void *ctx)
{
    puts("event_read_cb");
    client_t *client = ctx;
    if (client->message)
    {
        puts("received a message that was too late to process");
        client_free(client);
        return;
    }
    struct evbuffer *buf = bufferevent_get_input(bev);
    size_t recv = evbuffer_get_length(buf);
    // 收包頭
    while (1)
    {
        if (client->header) // 已收到完整包頭
        {
            break;
        }
        else if (recv < 2) // 沒有完整包頭，等待網路數據
        {
            return;
        }
        // 讀取包頭
        evbuffer_remove(buf, &client->header, 2);
        recv -= 2;
    }
    if (client->header > CLIENT_MAX_MESSAGE)
    {
        puts("message too large");
        client_free(client);
        return;
    }
    // 等待一個完整的消息
    if (recv < client->header)
    {
        return;
    }
    evbuffer_remove(buf, client->buffer, client->header);
    client->message = client->buffer;
    client->message[client->header] = 0;
    // 進入狀態機 處理消息
    if (client_loop(client))
    {
        client_free(client);
        return;
    }
    // 處理完成準備接收下一個消息
    client->header = 0;
    client->message = NULL;
}
void event_connect_timeout_cb(evutil_socket_t fd, short events, void *arg)
{
    puts("connect timeout");
    client_free(arg);
}
void event_connect_cb(struct bufferevent *bev, short what, client_t *client)
{
    // 關閉 timeout
    if (client->timeout)
    {
        event_free(client->timeout);
        client->timeout = NULL;
    }

    if (what & BEV_EVENT_CONNECTED)
    {
        puts("connect success");
        client->state = 1;
        // 連接成功 進入連接狀態機
        if (client_loop(client))
        {
            client_free(client);
        }
    }
    else if (what & BEV_EVENT_ERROR)
    {
#ifdef TEST_CONNECT_HOSTNAME
        int err = bufferevent_socket_get_dns_error(bev);
        if (err)
        {
            printf("DNS error: %s\n", evutil_gai_strerror(err));
        }
        else
        {
            printf("connect: %s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
        }
#else
        printf("connect: %s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
#endif
        client_free(client);
    }
}
void event_cb(struct bufferevent *bev, short what, void *ctx)
{
    puts("event_cb");
    client_t *client = ctx;
    if (!client->state)
    {
        event_connect_cb(bev, what, client);
        return;
    }
    if (what & BEV_EVENT_WRITING)
    {
        if (what & BEV_EVENT_EOF)
        {
            puts("write on eof");
        }
        else if (what & BEV_EVENT_TIMEOUT)
        {
            puts("write timeout");
        }
        else if (what & BEV_EVENT_ERROR)
        {
            printf("write error: %s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
        }
    }
    else if (what & BEV_EVENT_READING)
    {
        if (what & BEV_EVENT_EOF)
        {
            puts("read on eof");
        }
        else if (what & BEV_EVENT_TIMEOUT)
        {
            puts("read timeout");
        }
        else if (what & BEV_EVENT_ERROR)
        {
            printf("read error: %s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
        }
    }
    client_free(client);
}
int bufferevent_example(struct event_base *eb)
{
    int ret = -1;
    client_t *client = malloc(sizeof(client_t));
    if (!client)
    {
        puts("malloc client error");
        goto END;
    }
    memset(client, 0, sizeof(client_t));
    // 爲連接添加 超時
    client->timeout = event_new(eb, -1, EV_TIMEOUT, event_connect_timeout_cb, client);
    if (!client->timeout)
    {
        puts("event_new connect timeout error");
        goto END;
    }
    struct timeval tv = {
        .tv_sec = 1,
        .tv_usec = 0,
    };
    if (event_add(client->timeout, &tv))
    {
        puts("event_add connect timeout error");
        goto END;
    }

    // 創建 bufferevent
    client->bev = bufferevent_socket_new(eb, -1, BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    if (!client->bev)
    {
        puts("bufferevent_socket_new error");
        goto END;
    }
    // 設置回調
    bufferevent_setcb(client->bev, event_read_cb, NULL, event_cb, client);
    // 啓用讀寫
    if (bufferevent_enable(client->bev, EV_READ | EV_WRITE))
    {
        puts("bufferevent_enable error");
        goto END;
    }
#ifdef TEST_CONNECT_HOSTNAME
    client->esb = evdns_base_new(eb, EVDNS_BASE_INITIALIZE_NAMESERVERS | EVDNS_BASE_DISABLE_WHEN_INACTIVE);
    if (!client->esb)
    {
        puts("evdns_base_new error");
        goto END;
    }
    if (bufferevent_socket_connect_hostname(client->bev, client->esb, AF_UNSPEC, TEST_CONNECT_ADDR, TEST_CONNECT_PORT))
    {
        puts("bufferevent_socket_connect error");
        goto END;
    }
#else
    // 連接服務器
    struct in_addr in_addr;
    if (evutil_inet_pton(AF_INET, TEST_CONNECT_ADDR, &in_addr) != 1)
    {
        puts("evutil_inet_pton error");
        goto END;
    }
    struct sockaddr_in addr;
    addr.sin_addr = in_addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(TEST_CONNECT_PORT);
    if (bufferevent_socket_connect(client->bev, (const struct sockaddr *)&addr, sizeof(addr)))
    {
        puts("bufferevent_socket_connect error");
        goto END;
    }
#endif
    puts("connecting");

    ret = 0;
    if (!ret)
    {
        return 0;
    }
END:
    if (client)
    {
        client_free(client);
    }
    return ret;
}

int main(int argc, char *argv[])
{
    int ret = -1;
    struct event_base *eb = NULL;
    eb = event_base_new();
    if (!eb)
    {
        puts("event_base_new error");
        goto END;
    }

    if (bufferevent_example(eb))
    {
        goto END;
    }
    ret = event_base_dispatch(eb);
    if (ret < 0)
    {
        puts("event_base_dispatch error");
        goto END;
    }
    ret = 0;
END:
    if (eb)
    {
        event_base_free(eb);
    }
    puts("end");
    return ret;
}
```