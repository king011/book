# [標準庫](https://deno.land/manual@v1.28.1/basics/standard_library)

deno 核心團隊審覈並保證與 deno 一起工作的標準庫位於 [https://deno.land/std@0.165.0](https://deno.land/std@0.165.0)

# 版本控制

目前標準庫並未穩定,所以它和 deno 沒有包含在一起並且版本號也並不一致

通常推薦 import 指定明確的標準庫版本,以免標準庫升級引發不確定問題

```
// 導入最新版本,這是不推薦的寫法
import { copy } from "https://deno.land/std/fs/copy.ts";
```

```
// 導入明確的版本,此版本不會發生變化,這是推薦寫法
import { copy } from "https://deno.land/std@0.165.0/fs/copy.ts";
```

# --unstable

標準庫中一些內容使用了 deno 不穩定的 api(目前 0.165.0),對此如果遇到提示 xxx does not exist,則可以傳入 --unstable 運行

```
deno run --allow-read --allow-write --unstable main.ts
```

這以問題應該會在將來解決