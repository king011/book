# hysteria2

hysteria2 是2023年開始很流行的一個代理工具和協議。(發佈了新的 v2 版本與之前版本不兼容)

hysteria2 使用修改過的 quic 傳輸數據，它能夠提供更好的 udp 轉發效率，更適合用於遊戲。此外目前 v2 沒有實現 fake tcp，需要此功能的可以選擇其 [v1](https://v1.hysteria.network/zh/) 版本

* 官網 [https://v2.hysteria.network/](https://v2.hysteria.network/)
* 源碼 [https://github.com/apernet/hysteria](https://github.com/apernet/hysteria)