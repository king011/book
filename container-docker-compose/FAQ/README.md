# 常用環境變量

有一些環境變量大部分容器都支持，因爲它們是 linux 系統支持的，但容器鏡像可能對齊進行了刪減並不一定都能夠支持，你最好測試下，下面是常用的環境變量

```
version: '1'
services:
  main:
    environment:
      - TZ=Asia/Shanghai # 指定時區爲上海
      # 設置使用 utf-8 字符集
      - LANG=C.UTF-8 
      - LC_ALL=C.UTF-8
```