# Console

System.Console 提供了 控制台 操作api

* Console.BackgroundColor 指定了 控制台 文件 背景色
* Console.ForegroundColor 指定了 控制台 文件 前景色


1. Write WriteLine 提供了 輸出功能
2. c#支持以 {n} {n:型別} 佔位符的方式 格式化 替代c的printf
3. n 從0 開始 計數

型別字符串

* :C/:c	格式化貨幣 $
* :D/:d	十進制 數字
* :E/:e	指數計數法
* :F/:f	指定小數點填充
* :G/:g	
* :N/:n
* :X/:x	十六進制

```c#
static void Main(string[] args)
{
    
    Console.BackgroundColor = ConsoleColor.Blue;
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine("i work for {0}\n{0} it's an idea","cerberus");
}
```

# 枚舉

```c#
enum Job
{
	King,
	Kate = 8,
	IllusiveMan = 10,
}
/*指定 枚舉 存儲 型別 默認 System.Int32*/
enum Power:byte
{
	King,
	Kate = 8,
	IllusiveMan = 10,
}
 
class Program
{
	static void Main(string[] args)
	{
		Job job = Job.King;
		Power power = Power.IllusiveMan;
 
		Console.WriteLine("key = {0}    val = {1}", job, (int)job);
		Console.WriteLine("key = {0}    val = {1}", power, (byte)power);
	}
}
```

# 數組

* c# 數組 基本 同 c++ 只是 其二維數組 可以允許 每行的 長度不同
* 且 Length 屬性 記錄了 數組長度

```
static void Main(string[] args)
{
    Console.WriteLine("******   聲明數組    ******");
    //string[] names = new string[3];
    //string[] names = new string[3]{"illusive man","king","kate"};
    string[] names =  { "illusive man", "king", "kate" };
    
    Console.WriteLine("******   遍歷數組    ******");
    for (int i = 0; i < names.Length; i++)
    {
        Console.WriteLine("names[{0}] = {1}",i,names[i]);
    }
    foreach (var name in names)
    {
        Console.WriteLine(name);
    }
 
 
    Console.WriteLine("******   二維數組    ******");
    int[][] table = new int[3][];
    table[0] = new int[1];
    table[1] = new int[2];
    table[2] = new int[3];
    int v = 0;
    for (int i = 0; i < table.Length; i++)
    {
        for (int j = 0; j < table[i].Length; j++)
        {
            table[i][j] = v++;
        }
    }
    foreach (var row in table)
    {
        foreach (var col in row)
        {
            Console.Write("{0}  ", col);
        }
        Console.WriteLine();
    }
    for (int i = 0; i < table.Length; i++)
    {
        Console.Write("row = {0}    ",i);
        for (int j = 0; j < table[i].Length; j++)
        {
            Console.Write("{0}  ",table[i][j]);
        }
        Console.WriteLine();
    }
 
    Console.WriteLine("hellow word");
 
}
```
## System.Array

System.Array 提供了 同數組 通用的 操作 方法

* Clear()		將數組 設置為空值(0 null false "")
* CopyTo()
* Length
* Rank
* Reverse
* Sort		使用 IComparer 接口 排序

```
static void Main(string[] args)
{
    string[] names =  { "illusive man", "king", "kate" };
    string[] strs = new string[3];
    Console.WriteLine("******   CopyTo  ******");
    names.CopyTo(strs, 0);
    foreach (var str in strs)
    {
        Console.WriteLine(str);
    }
 
    Console.WriteLine("******   Clear  ******");
    System.Array.Clear(strs, 0, strs.Length);
    foreach (var str in strs)
    {
        Console.WriteLine(str);
    }
 
 
    Console.WriteLine("******   Reverse  ******");
    System.Array.Reverse(names);
    foreach (var str in names)
    {
        Console.WriteLine(str);
    }
 
    Console.WriteLine("******   Sort  ******");
    System.Array.Sort(names);
    foreach (var str in names)
    {
        Console.WriteLine(str);
    }
    Console.WriteLine("hellow word");
 
}
```

# 命名參數

c# 支持在傳遞 參數時 通過 參數名指定參數 而不是 通過 參數位置

如果 使用 命名參數 命名參數必須在 使用 位置 參數 之後

```
class Program
{
    static void Main(string[] args)
    {
        Show("illusive man",power:9);
    }
    static void Show(string name = "king",int lv=1,int power = 10)
    {
        Console.WriteLine("{0} {1} {2}",name,lv,power);
    }
   
}
```

# 參數修飾符

c# 使用 參數修飾符 限定 參數如何 傳遞給函數

* 無		值傳遞
* out		輸出參數 引用傳遞 且函數必須為其設置值
* ref		同 out 只是 函數可以 不必一定要為 參數 設置值
* params	只能在最後一個 參數上限定(且 參數型別必須為數組) 代表 不定長參數 類似golang的 ...

> ref 參數在傳遞給 函數前 必須 賦初始值 out 則不需要

# 流程控制

```
break
continue
goto

for(;;)
{

}

for(var ... in ...)
{

}

while(...)
{

}

do
{

}while(...);


if(...)
{

}
else if(...)
{

}
else
{

}

switch(...)
{
case ?:
break;
default:
break;
}
```

# var 

關鍵字 var 類似 c++11的 auto

#  checked/unchecked

* checked 後的一個 代碼段 如果 發生 溢出 將拋出OverflowException異常
* unchecked 可以將 一段代碼的 checked 檢查 取消

```c#
try
{
    checked
    {
        int l = Int32.MaxValue;
        int r = 100;
 
        int sum = l + r;
    }
}
catch (OverflowException e)
{
    Console.WriteLine(e);
}
```

# StringBuilder

c# 提供了 System.Text.StringBuilder 類 處理字符串 以避免 string 類 產生 大量無用臨時 string的 問題

```c#
var str = new StringBuilder("cerberus it's an idea\n");
str.AppendFormat("i work for {0}", "cerberus".ToUpper());
Console.WriteLine(str);
```

# 型別



| c# | System | CLS | 備註 |
| -------- | -------- | -------- | -------- |
| bool     | System.Boolean     | y     |      |
| sbyte     | System.SByte     | n     | 帶符號8位數據     |
| byte     | System.Byte     | y     |      |
| short     | System.Int16     | y     |      |
| ushort     | System.UInt16     | n     |      |
| int     | System.Int32     | y     |      |
| uint     | System.UInt32     | n     |      |
| long     | System.Int64     | y     |      |
| ulong     | System.UInt64     | n     |      |
| char     | System.Char     | y     |      |
| float     | System.Single     | y     |      |
| double     | System.Double     | y     |      |
| decimal     | System.Decimal     | y     |      |
| string     | System.String     | y     |      |
| object     | System.Object     | y     |      |

c# 的 char string 使用 unicode 編碼(UTF-16)

```c#
// c# 在定義字符串前 加上@ 字符串將不會被轉義
Console.WriteLine(@"\n");	//輸出 \n 而非 換行
```

1. 雖然 string 也是引用 然 C#可以重載 運算符 
2. 故 在 == != 時是調用的 Equals 比較 而不是 直接比較的 引用地址 
3. 然 string 和java中類似(java中必須使用 Equals 比較字符串是否相等) 字符串是不可變的

# 結構

基本 同 c++ 只是 一個 結構的 實例， 其所有 屬性 都 必須 被 設置後， 才能 進行 其他 操作

使用 new 創建 將為所有屬性 設置 默認值

```
struct Point
{
    public int X,Y;
    public void Show()
    {
        Console.WriteLine("x = {0}  y = {1}",X,Y);
    }
}
 
class Program
{
    static void Main(string[] args)
    {
        Point p0;
        p0.X = 0;
        p0.Y = 2;
        p0.Show();
 
        Point p1 = new Point();
        p1.Show();
 
    }
}
```

> c# 對結構 默認使用 值傳遞 對類使用 引用傳遞