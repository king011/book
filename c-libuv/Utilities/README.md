# [Utilities](http://docs.libuv.org/en/v1.x/guide/utilities.html)

libuv 已經提供了一些有用的 api 來支持 libuv 模式

# Timers 

定時器被設計爲經過一段後進行回調，定時器也可以設定爲 定期調用 而非 只調用一次

下面例子啓動一個定期器在 5s 後回調第一次，之後每個 2s 回調一次
```
uv_timer_t timer_req;

uv_timer_init(loop, &timer_req);
uv_timer_start(&timer_req, callback, 5000, 2000);
```

> 最后一个参数传入0 则只会回调一次

可以随时停止定时器

```
uv_timer_stop(&timer_req);
```

也可以随时修改定时器 间隔 时间

* 無法修改初次回調時間
* 將間隔時間修改爲 0 將 停止回調

```
uv_timer_set_repeat(uv_timer_t *timer, int64_t repeat);
```


uv\_timer\_again 僅用在重複計時器上，如果計時器尚未調用過 uv\_timer\_start 返回 UV\_EINVAL 錯誤，會執行 uv\_timer\_again 下述操作

1. 停止定時器
2. 將 定時器的 第一次回調時間 和間隔時間修改爲 uv\_timer\_start 傳入的間隔時間
3. 重新啓動定時器

```
int uv_timer_again(uv_timer_t *)
```

# Event loop reference count

只要有活動句柄，事件循環就會運行。該系統的工作原理是讓每個句柄在啓動(xxx\_start)時增加事件循環的引用計數，並在停止時減少引用計數。libuv 也提供了以下方法手動更改句柄的引用計數：

```
void uv_ref(uv_handle_t*);
void uv_unref(uv_handle_t*);
```

這將允許循環在即時觀察者處於活動時退出，或使用自定義對象來保持循環活動

比如你有個每 2s 執行一次的 垃圾收集器，你可以 uv_unref 她，這樣當沒有其它活動事件時循環會退出

```
uv_loop_t *loop;
uv_timer_t gc_req;
uv_timer_t fake_job_req;

int main() {
    loop = uv_default_loop();

    uv_timer_init(loop, &gc_req);
    uv_unref((uv_handle_t*) &gc_req);

    uv_timer_start(&gc_req, gc, 0, 2000);

    // could actually be a TCP download or something
    uv_timer_init(loop, &fake_job_req);
    uv_timer_start(&fake_job_req, fake_job, 9000, 0);
    return uv_run(loop, UV_RUN_DEFAULT);
}
```

# Idler pattern

uv\_idle\_t 空間句柄，會在每個事件循環都被調用一次空閒句柄的回調。可用於執行一些非常低優先級的活動。例如，可以在空閒期間將每日應用程序性能摘要發送給開發人員分析，或更新 ui 避免節目假死。

```
#include <iostream>
#include <uv.h>

int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();

    uv_idle_t idler;

    uv_idle_init(loop, &idler);
    uv_idle_start(&idler, [](uv_idle_t *handle)
                  {
                      fprintf(stderr, "Computing PI...\n");
                      uv_idle_stop(handle);
                  });

    return uv_run(loop, UV_RUN_DEFAULT);
}
```

# Passing data to worker thread

使用 uv\_queue\_work 時，通常需要將複雜數據傳遞給工作線程。解決方案時使用結構體並設置 uv\_work\_t.data 指向它。一個技巧是將 uv\_work\_t 作爲結構體的第一個成員

```
struct ftp_baton {
    uv_work_t req;
    char *host;
    int port;
    char *username;
    char *password;
}
```

```
ftp_baton *baton = (ftp_baton*) malloc(sizeof(ftp_baton));
baton->req.data = (void*) baton;
baton->host = strdup("my.webhost.com");
baton->port = 21;
// ...

uv_queue_work(loop, &baton->req, ftp_session, ftp_cleanup);
```
```
void ftp_session(uv_work_t *req) {
    ftp_baton *baton = (ftp_baton*) req->data;

    fprintf(stderr, "Connecting to %s\n", baton->host);
}

void ftp_cleanup(uv_work_t *req) {
    ftp_baton *baton = (ftp_baton*) req->data;

    free(baton->host);
    // ...
    free(baton);
}
```