# [Routing](https://www.rabbitmq.com/tutorials/tutorial-four-go.html)

RabbitMQ 支持接收數據的子集

# Routing Key

Routing Key 在不同的交互機中 有不同的含義 對於 fanout 交換機 會忽略掉 routing key

![](assets/direct-exchange.webp)

# direct

對於 direct 交換機 會將 消息推送到 routing key 完全匹配的 隊列中

```
#info="producer"

package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `direct`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 交換機 只有在交換機不存在時 才會創建
	e = channel.ExchangeDeclare(
		ExchangeName, // 交換機名稱
		"direct",     //使用 直接 交換機
		true,         // 是否持久化
		false,        // 是否在沒人使用時 自動刪除
		false,        // 是否爲 internal 如果爲 true 則爲 internal 不會接受消息發佈
		false,        // no-wait
		nil,          // arguments
	)
	if e != nil {
		log.Println(e)
		return
	}

	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {
		var routingKey string
		switch i % 3 {
		case 0:
			routingKey = "orange"
		case 1:
			routingKey = "blank"
		default:
			routingKey = "green"
		}

		// 發佈消息
		e = channel.Publish(
			ExchangeName,
			routingKey, // 指定 routing key
			false,
			false,
			amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         []byte(strconv.Itoa(i)), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, i, routingKey)
		} else {
			log.Println(e)
			break
		}
		time.Sleep(time.Millisecond * 1000)
	}
}
```

```
#info="consumer"

package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `direct`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	go consumer(c, "one", "orange")
	consumer(c, "two", "blank", "green")
}
func consumer(c *amqp.Connection, tag string, routingKeys ...string) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 交換機 只有在交換機不存在時 才會創建
	e = channel.ExchangeDeclare(
		ExchangeName, // 交換機名稱
		"direct",     //使用 直接 交換機
		true,         // 是否持久化
		false,        // 是否在沒人使用時 自動刪除
		false,        // 是否爲 internal 如果爲 true 則爲 internal 不會接受消息發佈
		false,        // no-wait
		nil,          // arguments
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		``,    // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		true,  // 是否持久化
		true,  // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 綁定 隊列到交換機
	for _, routingKey := range routingKeys {
		e = channel.QueueBind(
			q.Name,       // 隊列名
			routingKey,   // 路由鍵
			ExchangeName, // 交換機
			false,        // no wait
			nil,          // args
		)
		if e != nil {
			log.Println(e)
			return
		}
	}
	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		true, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	for msg := range ch {
		v, e0 := strconv.Atoi(string(msg.Body))
		fmt.Println(tag, `recv <-`, msg.RoutingKey, v, e0)
	}
}
```