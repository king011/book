# 組件交互

angular 提供了多種方法 實現 組件 間的 交互

## @Input

@Input 裝飾器 可以定義一個 輸入屬性 父組件 可以 使用 模板語法 對齊進行 綁定

```ts
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  // 定義一個 name 輸入 屬性
  @Input()
  name: string

  // 定義一個 lv 輸入 屬性
  // 裝飾器 可以和 set 一切使用
  private lv: number
  @Input("lv")
  set _lv(v: number) {
    this.lv = v
  }

  constructor() { }

  ngOnInit() {
  }
}
```

## @Output

@Output 裝飾器 可以定義一個 輸出 事件 供 父組件 監聽

**父模板**
```html
<p>
  <app-box [(val)]="base"></app-box>
  home val = {{base}}
</p>
```

> \[\(val\)\]=&quot;base&quot;&nbsp;其實是&nbsp;\[val\]=&quot;base&quot;&nbsp;\(valChange\)=&quot;base=$event&quot;&nbsp;的語法糖

**子組件**
```ts
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
 
@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {
  @Input() val: number;
  @Output() valChange = new EventEmitter<number>();
 
  constructor() { }
 
  ngOnInit() {
  }
  add(v: number) {
    this.val += v;
    this.valChange.emit(this.val);
  }
}
```

## \#var

父組件 可以 在模板中 使用 \# 創建一個 組件的 引用 之後在 模板中 通過 引用 訪問 子組件屬性 方法 

```html
<p>
  <app-beauty #beauty></app-beauty>
  <button (click)="beauty.name='kate'">set</button>
  <button (click)="beauty.reset()">reset</button>
 
  <app-beauty #beauty1></app-beauty>
  <button (click)="beauty1.name='kate'">set</button>
  <button (click)="beauty1.reset()">reset</button>
</p>
```

## @ViewChild

@ViewChild 裝飾器 可以建立一個 子組件的 引用

```ts
import { Component, OnInit, ViewChild } from '@angular/core';
import { BeautyComponent } from './beauty/beauty.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild(BeautyComponent)
  private beauty: BeautyComponent;
 
  constructor() {
  }
  ngOnInit() {
    this.beauty.name = "kate"
  }
  reset(){
    this.beauty.reset()
  }
 
}
```

> 如果模板中 有多個同樣的 子組件 所有引用都會指向 第一個 子組件(既無法爲多個同樣的 子組件 建立引用)

## 通過 服務

在 父組件的 providers 中 指定 一個 共享的 服務  
父子 組件 通過 此 服務 來通信  
\(這種方式 也可以在 非 父子組件 間 通信\)

 **service**
```ts
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MessageService {
	private parent = new Subject<string>();
	private child = new Subject<string>();

	parentObservable = this.parent.asObservable();
	childObservable = this.child.asObservable();

	sendToParent(msg: string) {
		this.parent.next(msg);
	}
	sendToChild(msg: string) {
		this.child.next(msg);
	}
}
```
	
**父組件**
```ts
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { BeautyComponent } from './beauty/beauty.component';
import { MessageService } from './message.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MessageService]
})
export class HomeComponent implements OnInit, OnDestroy {
  val:string
  constructor(private srv: MessageService) {
  }
  private subscribable: Subscription
  ngOnInit() {
    // 訂閱 發送到 父節點的 消息
    this.subscribable = this.srv.parentObservable.subscribe(
      msg => {
        console.log("home get ", msg)
      }
    );
  }
  ngOnDestroy() {
    // 反訂閱 
    if (this.subscribable) {
      this.subscribable.unsubscribe()
    }
  }
  send(val:string){
    // 向 子節點 發送消息
    this.srv.sendToChild(val);
  }
}
```
**子組件**
```ts
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService } from '../message.service';
@Component({
  selector: 'app-beauty',
  templateUrl: './beauty.component.html',
  styleUrls: ['./beauty.component.css']
})
export class BeautyComponent implements OnInit, OnDestroy {
  val:string
  constructor(private srv: MessageService) {
  }
  private subscribable: Subscription
  ngOnInit() {
    // 訂閱 發送到 子節點的 消息
    this.subscribable = this.srv.childObservable.subscribe(
      msg => {
        console.log("child get ", msg)
      }
    );
  }
  ngOnDestroy() {
    // 反訂閱 
    if (this.subscribable) {
      this.subscribable.unsubscribe()
    }
  }
  send(val:string){
    // 向 父節點 發送消息
    this.srv.sendToParent(val);
  }
 
}
```

# 組件樣式

* angular 使用 標準的 css 來設置 樣式

* angular 爲css提供了作用域  組件的 樣式 只會影響組件自身

* angular 支持了 sass/scss less stylus 等css預處理器 只要 在 new 時 指定 即可

	```sh
	ng new sassy-project --style=sass
	```
	
## :host

* :host 僞選擇器 用來選擇 組件的 宿主元素
* 以宿主作爲 目標選擇 需要 使用 **\(\)** 的形式書寫

```css
:host{
    background: red;
    display: block;
}
:host(:hover) {
    background: yellow;
    display: block;
}
```

## :host-context
* :host-context 類似 :host 但她向祖先元素 查詢 樣式

```css
/* 當祖先元素中有定義 .theme-light 時 才把組件類的 h2 設置 background-color*/
:host-context(.theme-light) h2 {
  background-color: #eef;
}
```
