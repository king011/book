# awk

awk 是大多數 unix/linux 自帶的文字處理工具

awk 名稱是 Alfred **A**ho, Peter **W**einberger 和 Brian **K**ernighan 三位作者姓氏的簡寫。

# 結構

awk 將檔案作爲**記錄序列**處理。在一般情況下檔案內容的每行都是一個**記錄**。每個記錄都會被分割爲一系列的**域**

awk 程式可以分爲如下三個可選的塊

| 塊 | 含義 |
| -------- | -------- |
| BEGIN { statement }     | BEGIN 標記的塊會在所有輸入前被執行，通常用來進行一些初始化操作     |
| { statement }     | 沒有標準的塊將在 BEGIN 執行之後，爲每個輸入的記錄執行     |
| END { statement }     | END 標準的塊會在處理完所有輸入後執行，通常進行結束的收尾工作     |

每個塊都是可選的並且支持重複定義塊

```
#!/usr/bin/awk -f

BEGIN { # 定義 BEGIN 塊
    FS = ":" # 設置使用分號分隔域
    total = 0 # 自定義變量
    print("--- begin scan ---")
}

{   # 主要處理塊
    if($NF !~ "/nologin$"){ # 查找所有非 nologin 用戶
            print $0
            total++
    }
}

END { # 定義結束塊
    print("--- end scan ---")
    printf("totals: %d\n",total)
}
```

* $N 用於獲取第幾個域，$0則代表當前記錄
* print 輸出一行內容
* printf 類似 c 的 printf




| 內置變數 | 含義 |
| -------- | -------- |
| NR     | 已經輸入的記錄條數     |
| NF     | 當前記錄中的域數量，最會一個域可以使用 $NF 表示      |
| FILENAME     | 輸入檔案名     |
| FS     | 域分隔符號默認爲空格或製表符     |
| RS     | 記錄分隔符，默認爲換行符     |
| OFS     | 輸出分隔符，print 命令參數的符號 默認爲空格     |
| ORS     | 輸出記錄分隔符號，即每個 print 輸出間的符號，默認爲換行     |
| OFMT     | 輸出數字格式默認爲 %.6g     |



# 比較



| 符號 | 含義 |
| -------- | -------- |
| ==     | 相等     |
| !=     | 不等     |
| >     | 大於     |
| >=     | 大於等於     |
| <     | 小於     |
| <=     | 小於等於     |
| &&     | 條件 AND     |
| ||     | 條件 OR     |
| ~     | 正則匹配     |
| !~     | 正則不匹配     |


# 函數

```
#!/usr/bin/awk -f

# 定義函數
function printn(number) {
    for(i=0;i<3;i++){
        print i
    }
}
BEGIN { 
    # 調用函數
   printn(3)
}
```