# assert

assert包 提供了 一些簡便方法 使用 單元測試 不用 一直使用 冗餘的 if e 形式

```
package main_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type Object struct {
	Value string
	Level int
}

func TestSomething(t *testing.T) {
	var object *Object
	// assert equality
	assert.Equal(t, 123, 123, "they should be equal")

	// assert inequality
	assert.NotEqual(t, 123, 456, "they should not be equal")

	// assert for nil (good for errors)
	assert.Nil(t, object)

	// assert for not nil (good when you expect something)
	object = &Object{
		Value: "Something",
	}
	if assert.NotNil(t, object) {

		// now we know that object isn't nil, we are safe to make
		// further assertions without causing any errors
		assert.Equal(t, "Something", object.Value)

	}

}
```