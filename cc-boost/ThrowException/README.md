# ThrowException

```
#include <boost/throw_exception.hpp>
```

* Author(s) -> Emil Dotchevski and Peter Dimov
* First Release -> 1.56.0
* C++ Standard Minimum Level ->03
* Categories -> Miscellaneous

throw_exception.hpp 中提供了 一個函數 boost::throw_exception 以及配套的宏 BOOST_THROW_EXCEPTION

boost::throw_exception 是 throw 的替代品 其要求傳入 std::exception ，throw_exception 會生成一個 類型爲 boost::exception 和傳入基礎類型的異常 同時啓動 對 boost::exception_ptr 的支持

BOOST_THROW_EXCEPTION(x) 被定義爲 ::boost::throw_exception(x, BOOST_CURRENT_LOCATION) 爲拋的 boost::exception 寫入了 源碼所在位置信息

```
#include <iostream>
#include <boost/exception/all.hpp>
#include <boost/throw_exception.hpp>

int main(int argc, char *argv[])
{
    try
    {
        BOOST_THROW_EXCEPTION(std::runtime_error("Unspecified runtime error"));
        return 0;
    }
    catch (const std::exception &e)
    {
        std::cerr << boost::diagnostic_information(e) << std::flush;
    }
    return -1;
}
```

```
$ ./bin/console 
/home/dev/project/c++/console/src/main.cc(9): Throw in function int main(int, char**)
Dynamic exception type: boost::wrapexcept<std::runtime_error>
std::exception::what: Unspecified runtime error
```