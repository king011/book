# [InputText](https://primeng.org/inputtext)

```
import {InputTextModule} from 'primeng/inputtext';
```

InputText 呈現一個文本字段以輸入數據

![](assets/primeng.org_inputtext.png)

```
<input type="text" pInputText />
```

## Model Binding

InputText 支持 ngModel 同樣也支持響應式表單

```
<input type="text" pInputText [(ngModel)]="property"/>
```

```
<input type="text" pInputText formControlName="property"/>
```

## Float Label
通過將輸入和標籤包裝在容器中和指定 class  **p-float-label** 包裝浮動標籤

```
<span class="p-float-label">
    <input id="float-input" type="text" pInputText> 
    <label for="float-input">Username</label>
</span>
```

## Icons

你可以爲輸入框指定一個內嵌的圖標，圖標的未知取決與你指定的 class
* **p-input-icon-left**
* **p-input-icon-right**

```
<span class="p-input-icon-left">
    <i class="pi pi-search"></i>
    <input type="text" pInputText [(ngModel)]="value1" placeholder="Search">         
</span>

<span class="p-input-icon-right">
    <i class="pi pi-spin pi-spinner"></i>
    <input type="text" pInputText [(ngModel)]="value2" >        
</span> 
```
## Sizes

除了常規大小，PrimeNG還提供了兩種 不同大小的輸入框，爲其指定 class 即可

* **p-inputtext-sm**
* **p-inputtext-lg**

```
<input type="text" pInputText class="p-inputtext-sm" placeholder="Small">
<input type="text" pInputText placeholder="Normal">
<input type="text" pInputText class="p-inputtext-lg" placeholder="Large">
```

## Outlined vs Filled

默認的輸入框風格是 **outlined**，你可以指定 class **p-input-filled** 來使用 filled 風格的輸入框

```
<span class="p-input-filled">
    <input type="text" pInputText placeholder="Filled">
</span>
<input type="text" pInputText placeholder="Outlined">
```

## Addons

Text, icon, buttons 和 其它內容可以使用 class **p-inputgroup** 來分組包裝，多個插件也可以在同一組中使用

```
<div class="p-inputgroup">
    <span class="p-inputgroup-addon"><i class="pi pi-user"></i></span>
    <input type="text" pInputText placeholder="Username">         
</div>

<div class="p-inputgroup">
    <span class="p-inputgroup-addon"><i class="pi pi-tags"></i></span>  
    <span class="p-inputgroup-addon"><i class="pi pi-shopping-cart"></i></span>   
    <input type="text" pInputText placeholder="Price"> 
    <span class="p-inputgroup-addon">$</span>  
    <span class="p-inputgroup-addon">.00</span>      
</div>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| disabled     | boolean     | false     | 是否禁用組件     |

# Styling

| Name | Element |
| -------- | -------- |
| p-inputtext     | Input element     |

