# [Log](https://wails.io/docs/reference/runtime/log)

wails 提供來一組日誌函數功 golang 或 javascript 調用，它們被分爲如下等級

* Trace
* Debug
* Info
* Warning
* Error
* Fatal

# Print

輸入日誌

```
Go: LogPrint(ctx context.Context, message string)
JS: LogPrint(message: string)
```

```
Go: LogPrintf(ctx context.Context, format string, args ...interface{})
```

# Trace

輸出 Trace 等級的日誌

```
Go: LogTrace(ctx context.Context, message string)
JS: LogTrace(message: string)
```

```
Go: LogTracef(ctx context.Context, format string, args ...interface{})
```

# Debug

輸出 Debug 等級的日誌

```
Go: LogDebug(ctx context.Context, message string)
JS: LogDebug(message: string)
```

```
Go: LogDebugf(ctx context.Context, format string, args ...interface{})
```

# Info

輸出 Info 等級的日誌

```
Go: LogInfo(ctx context.Context, message string)
JS: LogInfo(message: string)
```

```
Go: LogInfof(ctx context.Context, format string, args ...interface{})
```

# Warning

輸出 Warning 等級的日誌

```
Go: LogWarning(ctx context.Context, message string)
JS: LogWarning(message: string)
```

```
Go: LogWarningf(ctx context.Context, format string, args ...interface{})
```

# Error

輸出 Error 等級的日誌

```
Go: LogError(ctx context.Context, message string)
JS: LogError(message: string)
```

```
Go: LogErrorf(ctx context.Context, format string, args ...interface{})
```

# Fatal

輸出 Fatal 等級的日誌

```
Go: LogFatal(ctx context.Context, message string)
JS: LogFatal(message: string)
```

```
Go: LogFatalf(ctx context.Context, format string, args ...interface{})
```

# LogSetLogLevel

設置日誌級別，在 javascript 中使用對應的 number 值設置

| Level | Value |
| -------- | -------- |
| Trace     | 1     |
| Debug     | 2     |
| Info     | 3     |
| Warning     | 4     |
| Error     | 5     |

```
Go: LogSetLogLevel(ctx context.Context, level logger.LogLevel)
JS: LogSetLogLevel(level: number)
```

# Logger

可以使用自定義的 Logger，只需要它實現 github.com/wailsapp/wails/v2/pkg/logger 中的  Logger  接口即可

```
type Logger interface {
    Print(message string)
    Trace(message string)
    Debug(message string)
    Info(message string)
    Warning(message string)
    Error(message string)
    Fatal(message string)
}
```