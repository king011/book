# [轉發tcp](https://www.envoyproxy.io/docs/envoy/latest/api-v3/extensions/filters/network/tcp_proxy/v3/tcp_proxy.proto)

```yaml
#info="cds.yaml"
resources:
    - '@type': type.googleapis.com/envoy.config.cluster.v3.Cluster
      connect_timeout: 5s
      load_assignment:
        cluster_name: tcp_cluster
        endpoints:
            - lb_endpoints:
                - endpoint:
                    address:
                        socket_address:
                            address: 172.30.0.10
                            ipv4_compat: true
                            port_value: 80
      name: tcp_cluster
      type: STATIC
```

```yaml
#info="cds.yaml"
resources:
    - '@type': type.googleapis.com/envoy.config.listener.v3.Listener
      address:
        socket_address:
            address: '::'
            ipv4_compat: true
            port_value: 9080
      default_filter_chain:
        filters:
            - name: envoy.filters.network.tcp
              typed_config:
                '@type': type.googleapis.com/envoy.extensions.filters.network.tcp_proxy.v3.TcpProxy
                access_log:
                    name: envoy.access_loggers.stdout
                    typed_config:
                        '@type': type.googleapis.com/envoy.extensions.access_loggers.stream.v3.StdoutAccessLog
                cluster: tcp_cluster
                stat_prefix: tcp_ingress
      name: tcp_listener
    - '@type': type.googleapis.com/envoy.config.listener.v3.Listener
      address:
        socket_address:
            address: '::'
            ipv4_compat: true
            port_value: 9443
      default_filter_chain:
        filters:
            - name: envoy.filters.network.tcp
              typed_config:
                '@type': type.googleapis.com/envoy.extensions.filters.network.tcp_proxy.v3.TcpProxy
                access_log:
                    name: envoy.access_loggers.stdout
                    typed_config:
                        '@type': type.googleapis.com/envoy.extensions.access_loggers.stream.v3.StdoutAccessLog
                cluster: tcp_cluster
                stat_prefix: tls_ingress
        transport_socket:
            name: envoy.transport_sockets.tls
            typed_config:
                '@type': type.googleapis.com/envoy.extensions.transport_sockets.tls.v3.DownstreamTlsContext
                common_tls_context:
                    tls_certificate_sds_secret_configs:
                        name: xsd
                        sds_config:
                            path_config_source:
                                path: /etc/envoy/sds.yaml
                            resource_api_version: V3
      listener_filters:
        - name: envoy.filters.listener.tls_inspector
          typed_config:
            '@type': type.googleapis.com/envoy.extensions.filters.listener.tls_inspector.v3.TlsInspector
      name: tls_listener
```