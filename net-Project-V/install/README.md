# linux
官方 提供了 linux下的 自動 安裝 腳本 go.sh

```
#info=false
wget https://install.direct/go.sh

chmod a+x go.sh
```


```
# 安裝 Project V
go.sh
go.sh -p socks5://127.0.0.1:1080
go.sh -p http://127.0.0.1:3128

# 刪除
go.sh --remov

# 檢查新版本 並更新
go.sh -c
```

go.sh 會 自動 安裝 並且 加入 systemd 服務 **v2ray.service** 並且 enabled 之後使用 systemd 進行管理

> Project V 會被安裝到 **/usr/bin/v2ray** 檔案夾中
>
> go.sh 需要 以 root 權限執行

# 命令

Project V 提供了 兩個 命令行 工具

* v2ctl
* v2ray

## v2ctl

v2ctl 支持如下 子命令

* verify 驗證檔案是否由 Project V 官方簽名
* config 從標準輸入讀取 json 配置 然後 從標準輸出 打印 Protobuf 配置
* uuid 輸出一個 隨機的 UUID

## v2ray

v2ray 是 project 的 核心 工具 提供了如下 參數

* -version 顯示 版本
* -test 測試配置檔案是否 有效
* -config 指定 配置檔案位置

如果 沒有指定 -config 則 從下列路徑 中 查找 config.json 檔案 作爲 配置
* 工作目錄
* 環境變量 v2ray.location.asset 指定的路徑
