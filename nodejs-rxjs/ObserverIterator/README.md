# Observer Pattern

```
function clickHandler(event) {
	console.log('user click!');
}

document.body.addEventListener('click', clickHandler)
```

觀察模式 允許註冊 監聽者 當事件發生時 觀察者被自動調用

# Iterator Pattern
```
function* getNumbers(max: number) {
    for (let i = 0; i < max; i++) {
        yield i
    }
}

const iterator = getNumbers(10)
while (true) {
    const result = iterator.next()
    if (result.done) {
        break
    }
    console.log(result.value)
}
```

迭代器模式 產生一個序列 並允許 漸進式的處理 序列中的數據

js 的 Iterator 只由一個 next 方法 next 只會返回兩種結果

1. 在最後一個元素前 {done:false,value:elem}
2. 在最後一個元素後 {done:true,value:undefined}

# Observable

Observable 類似 Iterator 可以漸進式取得資料 但 Observable 是生產者 推送資料

