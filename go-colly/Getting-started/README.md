# 入門

```
package main

import (
	"fmt"
	"log"

	"github.com/gocolly/colly/v2"
)

func main() {
	// 創建一個數據收集器
	c := colly.NewCollector(
		/* 設置可選的 參數*/

		// 假裝火狐
		colly.UserAgent(`Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0`),
		// 設置遞歸深度，默認爲0(不限制)
		colly.MaxDepth(1), // 1 相當於不遞歸

		/* ... */
	)

	/* 設置各種回調函數 */

	c.OnRequest(func(r *colly.Request) {
		// 在發送請求之前回調 可用於爲特定請求設置特有參數
		fmt.Println(`準備訪問:`, r.URL)
	})

	c.OnError(func(_ *colly.Response, err error) {
		// 如果在請求期間發送錯誤則回調此函數
		log.Println(`發送錯誤:`, err)
	})

	c.OnResponseHeaders(func(r *colly.Response) {
		// 在收到響應頭後回調
		fmt.Println(`響應頭:`, r.Headers)
	})

	c.OnResponse(func(r *colly.Response) {
		// 在收到完整響應後回調
		fmt.Println("響應:", r.Request.URL)
	})

	// OnHTML 如果收到內容是 html 則在 OnResponse 之後調用
	c.OnHTML(`a[href]`, func(e *colly.HTMLElement) {
		// 遞歸 訪問 url
		e.Request.Visit(e.Attr(`href`))
	})

	// OnHTML 選擇器語法 類似與 jquery
	c.OnHTML("tr td:nth-of-type(1)", func(e *colly.HTMLElement) {
		fmt.Println("First column of a table row:", e.Text)
	})

	c.OnXML("//h1", func(e *colly.XMLElement) {
		// 如果收到內容是 html 或 xml 在 OnHTML 之後調用
		fmt.Println(e.Text)
	})

	c.OnScraped(func(r *colly.Response) {
		// 在 OnXML 之後 回調 代表對當前 url 處理結束
		fmt.Println("完成:", r.Request.URL)
	})

	// 訪問網址
	e := c.Visit(`http://go-colly.org/`)
	if e != nil {
		log.Fatalln(e)
	}
}
```