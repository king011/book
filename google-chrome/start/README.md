# 常用參數
## 顯示版本  
```sh
google-chrome --version
```
	
## 使用代理訪問網路  
```sh
# 使用 http 代理
google-chrome --proxy-server="foopy:99"
# 使用 SOCKS v5 代理google-chrome --proxy-server="socks://foopy:1080"
google-chrome --proxy-server="socks5://foopy:1080"
# 使用 SOCKS v4 代理
google-chrome --proxy-server="socks4://foopy:1080"
# 對 http https 請求使用不同的代理
google-chrome --proxy-server="https=proxy1:80;http=socks4://baz:1080"
```
