# 編譯

iotjs 支持多個平臺

* Linux
* NuttX
* Tizen
* TizenRT

並對各自平臺可以指定不同的優化，不過所有第一步首先需要獲取源碼

```
git clone https://github.com/jerryscript-project/iotjs.git
cd iotjs
```

# Build for Linux

1. 獲取源碼
2. 一次構建所有
3. 執行 Iot.js
4. 清理構建目錄

假設使用 ubuntu 作爲構建系統 需要安裝一些構建工具

```
sudo apt-get install gyp cmake build-essential valgrind
```

此外需要一個目標平臺的 gcc 編譯器需要 4.8 或更高版本才可編譯

## 編譯

IoT.js 提供了 **tools/build.py** 腳本用於自動化構建項目 執行即可

```
tools/build.py
```

build.py 腳本可以接受一些 構建的基本選項
```
buildtype=debug|release (debug is default)
builddir=build (build is default)
clean
buildlib (default is False)
profile=path-to-profile (default: profiles/default.profile)
target-arch=x86|x86_64|x64|i686|arm (depends on your host platform)
target-os=linux|nuttx|darwin|osx (linux is default)
target-board
cmake-param
compile-flag
link_flag
external-include-dir
external-lib
jerry-cmake-param
jerry-compile-flag
jerry-link-flag
jerry-lto
jerry-heap-section
jerry-heaplimit (default is 81, may change)
jerry-memstat (default is False)
no-init-submodule (default is init)
no-check-tidy (default is check)
no-parallel-build
no-snapshot
nuttx-home= (no default value)
run-test (default is False)
```

所有選項需要以 **--選項名** 的形式使用，並可以名稱大概了解其含義，下面是一些可能需要解釋的選項

* builddir 編譯中間檔案和輸出檔案在此生成
* buildlib 如果爲 True 則將 iotjs 生成到庫中
* jerry-heaplimit JerryScript 默認對大小是 256 kb，此選項用於更改嵌入式系統的大小，目前爲 Nuttx 當前默認爲 81kb。對於 linux 還沒有效果。在構建 nuttx 時如果看到錯誤區域 sram 溢出 xxx 字節，則可能需要減少該值
* jerry-memstat 打開標誌，以便 jerry 在解析和執行時轉存字節碼和文字以及內存使用清空
* no-check-tidy 沒有檢查代碼是整潔的
* nuttx-home nuttx 平臺特定的，用於說明 nuttx 配置和頭檔案的位置
* run-test 構建後運行 test 檔案夾中所有測試

更多細節見 [Build Script](https://github.com/jerryscript-project/iotjs/blob/master/docs/build/Build-Script.md) 頁面

## 包含擴展模塊

有個兩種方式包含擴展模塊

1. 指定 ENABLE\_MODULE\_\[NAME\]=ON 參數，\[NAME\] 是模塊的大寫名稱

	```
	./tools/build.py --cmake-param=-DENABLE_MODULE_DGRAM=ON
	```

2. 使用配置檔案

	```
	#info="my-profile"
	ENABLE_MODULE_IOTJS_CORE_MODULES
	ENABLE_MODULE_IOTJS_BASIC_MODULES
	ENABLE_MODULE_DGRAM
	```

	```
	./tools/build.py --profile=./my-profile
	```
	
	## 選項示例
	
	```
	./tools/build.py --buildtype=release
	```
	
	```
	#info="交叉編譯到 arm"
	./tools/build.py \
		--buildtype=release \
		--target-os=linux \
		--target-arch=arm \
		--target-board=rpi2 \
		--cmake-param=-DCMAKE_C_COMPILER=arm-linux-gnueabihf-gcc \
		--jerry-cmake-param=-DCMAKE_C_COMPILER=arm-linux-gnueabihf-gcc \
		--no-snapshot
		--clean
	```
	
	--clean 參數會在構建前 清理所有已經構建的目錄