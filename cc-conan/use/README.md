# 使用包

通常在根目錄下創建一個 **conanfile.txt** 檔案 在裏面定義包依賴

```
#info="conanfile.txt"
 [requires]
 poco/1.9.4

 [generators]
 cmake

 [imports]
include, * -> third_party/include
```

1. requires -> 指定了依賴的包
2. generators -> 指定了構建系統，[點此](https://docs.conan.io/en/latest/reference/generators.html#generators-reference) 查看支持的 構建系統
3. imports -> 用於將包中的檔案 拷貝到當前項目

執行 conan install 即可安裝依賴 並且生成構建腳本
```
conan install . -if=build
```

* . 參數 用於指定 conanfile.txt 檔案所在目錄
* -if 參數 用於指定輸出腳本路徑，默認爲當前工作路徑

## requires

requires 段用於指定項目依賴

```
[requires]
mypackage/1.0.0@company/stable
```

* mypackage -> 包名稱
* 1.0.0 -> 包的版本 可以是任意字符串
* company -> 包所有者的名稱，類似名字空間以允許不同用戶創建同名的包
* stable -> 頻道，類似名字空間以允許 用戶爲同個包創建不同的包變體 比如 stable develop testing

requires 允許指定多個需求並覆蓋可傳遞的需求，比如 poco 依賴 openssl/1.0.2t zlib/1.2.11 我們可以指定一個 openssl 版本覆蓋 poco 包的默認設定

```
[requires]
poco/1.9.4
openssl/1.0.2u
```

```
[requires]
poco/1.9.4
openssl/1.0.2u
zlib/1.2.11@otheruser/alpha
```

## [generators](https://docs.conan.io/en/latest/reference/generators.html#generators-reference)

generators 用於指定爲項目生成 何種構建系統的腳本 

```
 [generators]
 cmake
```

## options

options 用於指定一些額外設置 例如爲一個默認 靜態鏈接的庫 指定使用 動態連接

```
poco:shared=True # PACKAGE:OPTION=VALUE
openssl:shared=True
```

## imports

imports 用於將包中的一些內容 拷貝到項目 比如 拷貝動態庫以便動態連接的程式可以正常加載依賴 拷貝 頭文件 以便ide 可以正確識別聲明

```
[imports]
include, * -> ./third_party/include
bin, *.dll -> ./bin
lib, *.dylib* -> ./bin
```

# profiles

**~/.conan/profiles/default** 定義了一些默認設置 

可以爲項目創建一個檔案 使用 -pr 參數 替換掉 默認設置檔案

創建一個 linux-amd64 檔案用於編譯 linux 程式
```
#info="linux-amd64"
[settings]
os=Linux
os_build=Linux
arch=x86_64
arch_build=x86_64
compiler=gcc
compiler.version=9
compiler.libcxx=libstdc++
build_type=Release
[options]
[build_requires]
[env]
CC=gcc
CXX=g++
```
創建一個 windows-amd64 檔案用於編譯 windows 程式
```
#info="windows-amd64"
[settings]
os=Windows
os_build=Linux
arch=x86_64
arch_build=x86_64
compiler=gcc
compiler.version=9
compiler.libcxx=libstdc++
build_type=Release
[options]
[build_requires]
[env]
CC=x86_64-w64-mingw32-gcc-posix
CXX=x86_64-w64-mingw32-g++-posix
```

```
conan install . -if=build/windows-amd64 -pr=windows-amd64
conan install . -if=build/linux-amd64 -pr=linux-amd64
```
