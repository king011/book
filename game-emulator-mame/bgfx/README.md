# [bgfx](https://docs.mamedev.org/advanced/bgfx.html)

mame 從 0.172 開始發行版本就包含了 bgfx,因此無需下載任何其它檔案，只需要在 mame.ini 中啓用即可

```
video bgfx
```

# 配置向

* **bgfx\_path** 這是存儲 BGFX 著色器檔案的位置。默認情況下是 MAME 安裝檔案夾中的 bgfx 檔案夾
* **bgfx\_backend** 選擇 bgfx 要使用的渲染後端，默認爲 **auto**,它將自動選擇合適的後端
	* d3d9
	* d3d11
	* d3d12
	* opengl
	* gles (opengl es)
	* metal (Apple Metal Graphics API)
	* vulkan 
* **bgfx\_debug** 啓用 bgfx 調試功能，大多數用戶不需要它
* **bgfx\_screen\_chains** 決定如何在每個懸索橋的基礎上處理 bgfx 選擇
	* default 默認的雙線性過濾輸出
	* unfiltered 最近鄰近採樣輸出
	* hlsl 通過著色器顯示模擬
	* crt-geom 輕量級 CRT 模擬
	* crt-geom-deluxe 更詳細的 CRT 模擬
	* lcd-grid LCD矩陣模擬
* **bgfx\_shadow\_mask** 指定了陰影遮罩效果的 PNG 檔案，默認是 slot-mask.png