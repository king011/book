# [evbuffer](https://libevent.org/doc/buffer_8h.html)

```
#include <event2/buffer.h>
```

evbuffer 實現了一個 bytes 隊列(從尾部添加從頭部移除)，針對將數據添加到末尾和從前端移除數據進行了優化，它通常作爲網路 IO 的緩衝部分。但是它不提供調度 IO 或在 IO 就緒時就緒觸發(這是 bufferevents 要做的)

# new free

使用 [evbuffer\_new()](https://libevent.org/doc/buffer_8h.html#a7a853e8ebc063d32cd11f1554c77615d) 創建一個 evbuffer 指針，如果發送錯誤會返回 NULL

當不在需要使用時，調用 [evbuffer\_free(buf)](https://libevent.org/doc/buffer_8h.html#ab255015b91f7b43ba09d8f9da54ed865) 釋放 evbuffer

# 多線程

默認情況下 evbuffer 是非線程安全的。你可以調用 [evbuffer\_enable\_locking(buf, lock)](https://libevent.org/doc/buffer_8h.html#af68e729ac81d8a1c02b716653af45ded) 來啓用鎖，如果 lock 傳入 NULL 則它會自動創建鎖資源。函數超過返回 0 否則返回 -1

你可以使用 [evbuffer\_lock(buf)](https://libevent.org/doc/buffer_8h.html#afe1a938f5728ca0770419119cd90359d) [evbuffer\_unlock(buf)](https://libevent.org/doc/buffer_8h.html#a25e8be33a5d5cb2684594b9b3cb6c7c9) 來鎖定或解鎖 evbuffer，如果沒有啓用鎖這兩個函數什麼都不會做。

> 當啓用了鎖時單個 evbuffer 操作將是原子的，你不需要手動對它進行 lock/unlock，只有在多個連續的 evbuffer 操作都需要線程獨佔時另外進行加鎖處理

# 已存儲數據長度

你可以調用 [evbuffer\_get\_length(buf)](https://libevent.org/doc/buffer_8h.html#a808db707e84e20229b2aafdcaba6c708) 返回存儲在 evbuffer 中數據的長度

因爲 evbuffer 中的數據可以存儲在不連續的內存中，你可以調用 [evbuffer\_get\_contiguous\_space(buf)](https://libevent.org/doc/buffer_8h.html#a3f0b89841da394aebf6dfd620dd787d1) 來獲取第一個存儲鏈中存儲的數據長度，如果返回 0 表示 evbuffer 中沒有數據(不會存在多餘的空鏈)

# 添加數據

你可以使用 [evbuffer\_add(buf, data, len)](https://libevent.org/doc/buffer_8h.html#a8abedc30187fe00d86b7b1cc4d3ce643) 向 evbuffer 添加指定長度的 bytes 數據，如果成功返回0，失敗返回 -1

[evbuffer\_add\_printf(buf, fmt, ...)](https://libevent.org/doc/buffer_8h.html#abd34b7bd9e698e15f868bf05fa6abc59)  和 [evbuffer\_add\_vprintf(buf, fmt, ap)](https://libevent.org/doc/buffer_8h.html#abb5d7931c7be6b2bde597cbb9b6dc72d) 類似 printf/vprintf 將數據格式化後再添加，如果成功寫入字節長度，失敗返回 -1

[evbuffer\_expand(buf, len)](https://libevent.org/doc/buffer_8h.html#a8d4919171c71fc41515c0373e8264527) 將 evbuffer 的可以空閒空間擴展到至少 len 指定的長度以便後寫寫入 len 長度的數據時無需進行內存分配，通常對於如果已知要寫入的數據長度時可以提前調用此函數用於預先分配好內存以提高數據添加效率

# 在 evbuffer 間移動數據

可以調用 [evbuffer\_add\_buffer(dst, src)](https://libevent.org/doc/buffer_8h.html#af95837e510c03d5e5fcf9c05db019f14) 將 src 的數據全部移動到 dst 中，如果成功返回 0，失敗返回 -1

也可以調用 [evbuffer\_remove\_buffer(src, dst, len)](https://libevent.org/doc/buffer_8h.html#a863342bdbdb4f91d0415ae29ef25494b) 將 src 中的數據移動到 dst 中，移動 min(len,  evbuffer\_get\_length(src)) 字節，如果成功返回移動的字節數，失敗返回 -1

> 這兩個函數的 src dst 位置是相反的，顯然是 api 設計不一致使用時要注意

# 將數據移動到前端

默認數據都是添加到末尾，但 [evbuffer\_prepend(buf, data, len)](https://libevent.org/doc/buffer_8h.html#a1112b433d2554b8b80b814feb37fdc95) 將數據添加到 buf 前端，成功返回 0，失敗返回 -1

類似的 [evbuffer\_prepend\_buffer(dst, src)](https://libevent.org/doc/buffer_8h.html#a1d26aa5a48b549d702bc5175b0444d33) 將 src 的數據全部移動到 dst 的前端

通常都不會用到這兩個函數，如果要使用請謹慎確定知道自己在做什麼

# 線性化緩衝區

evbuffer 內部可能由多個不連續的內存鏈組成，調用 [evbuffer\_pullup(buf, size)](https://libevent.org/doc/buffer_8h.html#acd73ca99c3a793ea26c43307b9723bd6) 將內部前 size 字節的數據線性化到連續的內存，如果成功返回內存地址，否則返回NULL
* 如果 size 爲負數則會線性化整個內部緩衝區
* 如果 size 大於緩衝區內數據大小會字節返回 NULL
* 如果緩衝區爲空也會返回 NULL
* 這個操作可能很慢，因爲可能需要重新複製整個緩衝區

通常沒有必要進行線性化，但你可以使用  evbuffer\_pullup(buf, 1) 和 evbuffer\_get\_contiguous\_space(buf) 來獲取內部鏈最前端內存的地址和大小(前提是 evbuffer 不爲空)

# 刪除數據

[evbuffer\_drain(buf, len)](https://libevent.org/doc/buffer_8h.html#a08599d8165ea785b9b6a4ddf44ec24f7) 從 buf 前端刪除 min(len, evbuffer\_get\_length) 長度的數據，如果成功返回 0，失敗返回 -1

[evbuffer\_remove(buf, dst, len)](https://libevent.org/doc/buffer_8h.html#aa53da314581de5e13bb0d92aa05e2301) 與 drain 類似，但將刪除的數據 copy 到 dst 中，如果成功返回移除的數據長度，失敗返回 -1

# 讀取行

很多時候需要按照行讀取數據，例如 http 1.x，ini 檔案，爲此 可以調用 [evbuffer\_readln(buf, outlen, style)](https://libevent.org/doc/buffer_8h.html#a8bcc94903283574331994c9364be3d7b) 如果成功會返回 malloc 申請的的字符串存儲了不包含換行和c字符串的0，沒有整行數據返回 NULL，outlen 指針如果被設置則會返回 返回字符串的長度

style 指定了 readln 支持的 換行格式

* **EVBUFFER\_EOL\_LF** 以 "\n" 爲換行 ASCII 值是 0x0A (\*inux 文本常用)
* **EVBUFFER\_EOL\_CRLF\_STRICT** 以 "\r\n" 爲換行 ASCII 值是 0x0D 0x0A (windows 文本常用)
* **EVBUFFER\_EOL\_CRLF** 以 "\n" 或者 "\r\n" 爲換行 (雖然 http 標準明確規定了使用 "\r\n" 作爲終止符，但一些客戶端可能沒按規定只發送了 "\n")
* **EVBUFFER\_EOL\_ANY** 行末是任意數據的 "\r" 和 "\n" 的組合
* **EVBUFFER\_EOL\_NUL** 行尾是一個值爲二進制 0 的但字節

```
#include <event2/buffer.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int readln(struct evbuffer *buf)
{
    char *s;
    size_t len;
    size_t line = 1;
    while (1)
    {
        s = evbuffer_readln(buf, &len, EVBUFFER_EOL_CRLF);
        if (!s)
        {
            break;
        }
        printf("%zu. \"", line++);
        for (size_t i = 0; i < len; i++)
        {
            putc(s[i], stdout);
        }
        puts("\"");

        free(s);
    }
}
int main(int argc, char *argv[])
{
    int ret = -1;
    struct evbuffer *buf = NULL;

    buf = evbuffer_new();
    if (!buf)
    {
        puts("evbuffer_new error");
        goto END;
    }
    const char *text = "cerberus is an idea\nkate is so beauty\r\n";
    if (evbuffer_add(buf, text, strlen(text)))
    {
        puts("evbuffer_add error");
        goto END;
    }

    ret = readln(buf);
END:
    if (buf)
    {
        evbuffer_free(buf);
    }
    return ret;
}
```

> 如果調用了 event\_set\_mem\_functions 替換了 malloc 則需要調用相應的替代 free 函數而非 原本的 free 來釋放 readln 返回的內存，libevent 真的應該提供一個 api 來調用內部的 malloc 和 free 但目前沒有只有使用者自己小心

# 搜索數據

evbuffer\_ptr 結構指向 evbuffer 中的一個位置，並包含可用於迭代 evbuffer 的數據
```
struct evbuffer_ptr {
        ev_ssize_t pos;
        struct {
                /* internal fields */
        } _internal;
};
```

pos 是唯一公開的屬性，它是相對 evbuffer 開始位置的偏移

你可以使用 [evbuffer\_search(buf, s, len, start)](https://libevent.org/doc/buffer_8h.html#a06e9211319a369ffb072d4f4a83b48e7) 在 buf 中查找長度爲 len 的字符串 s， 如果沒有找到返回的 evbuffer\_ptr.pos 爲 -1 否則 evbuffer\_ptr.pos 是找到的位置偏移。start 不爲 NULL 則跳過它指定的偏移後進行查找

[evbuffer\_search\_range(buf, s, len, start, end)](https://libevent.org/doc/buffer_8h.html#a3b1e093af8bc1a86963729ec1290de8f) 與 evbuffer\_search 類似但是它允許指定一個可選的 end 來指定查找的結束範圍

[evbuffer\_search\_eol(buf, start, outlen, style)](https://libevent.org/doc/buffer_8h.html#a28e2fa6852a5a49285933b1c95c7465f) 用於查找換行符，如果沒有找到返回的 evbuffer\_ptr.pos 爲 -1 否則 evbuffer\_ptr.pos 是找到的位置偏移。start 不爲 NULL 則跳過它指定的偏移後進行查找。outlen不會 NULL 則會設置爲換行符佔用的字節數， style 是要匹配的換行風格

evbuffer\_ptr 是和 evbuffer 關聯的你不能字節手動設置 pos 屬性爲此你可以調用提供的 [evbuffer\_ptr\_set(buf, pos, position, how)](https://libevent.org/doc/buffer_8h.html#a22137a538067a8450d52af0e6601ca49) 函數來設置，成功返回 0，失敗返回 -1

* **buf** 是一個 evbuffer 指針
* **pos** 是要設置的 evbuffer\_ptr 指針
* **position** 是要設置的值，一個 size\_t
* **how** 只是要如何設置值
	1. **EVBUFFER\_PTR\_SET** 將 pos 設置爲 position
	2. **EVBUFFER\_PTR\_ADD** 將 pos 當前偏移增加 position


> 任何修改了 evbuffer 內部佈局的操作都可能會導致 evbuffer\_ptr 失效，對失效的 evbuffer\_ptr 進行操作行爲是未定義的

# 複製數據

有時你想將數據複製出來，但不移除，你可以調用 [evbuffer\_copyout(buf, dst, len)](https://libevent.org/doc/buffer_8h.html#afdc539f4c3dbae7c671a4a857833a3d5) 它從 buf 中複製 min(len, evbuffer\_get\_length) 數據到 dst 中，如果成功返回複製的字節數，失敗返回 -1

[evbuffer\_copyout\_from(buf, pos, dst, len)](https://libevent.org/doc/buffer_8h.html#a3cf708e4e9384d70a5d2fd7158fd94cb) 與 evbuffer\_copyout 類似，但是它接收一個可選的 pos 用於指示要從哪個位置開發複製

# 檢視數據

除了將數據 copy 出來，你也可以直接檢視 evbuffer 內部的數據，這要用到 struct evbuffer\_iovec(內部的內存鏈)

```
struct evbuffer_iovec {
        void *iov_base;
        size_t iov_len;
};
```

你可以調用 [evbuffer\_peek(buf, len, pos, vec, n)](https://libevent.org/doc/buffer_8h.html#aad7c208f2afca3e7e46a1a95fb79eec1) 來獲取內部鏈的情況，這個函數參數比較複雜，不同的入參情況功能有所區別，最好按照功能去理解

* **buf** 是要檢視的 evbuffer 緩衝區
* **len** 是想要檢視的字節數量，如果是負數，則想要檢視儘可能多的鏈
* **pos** 是可選的偏移
* **vec** 是一個 evbuffer\_iovec 數組指針，用於輸出鏈信息
* **n** 是 vec 的容量，如果爲0則只計算需要的容量，不寫入 vec
* **return** 返回值是所需要的 vec 容量數，它可能小於 n,或者大於 n 我們需要更大的 vec 來存儲輸出數據

通常我們可以先以 vec 和 n 爲 0 調用來獲取需要的 vec 數量

```
int n = evbuffer_peek(buf, -1, NULL, NULL, 0)
```

之後在準備足夠的 vec 容量後，在此調用 evbuffer\_peek

```
if (n)
{
		struct evbuffer_iovec *vec = malloc(sizeof(struct evbuffer_iovec) * n);
		evbuffer_peek(buf, -1, NULL, vec, n);
		printf("%d\n", n);
		for (int i = 0; i < n; i++)
		{
				printf("%p %zu\n", (vec + i)->iov_base, (vec + i)->iov_len);
		}
}
```

> 如果 vec 的容量不夠，則返回值依然是需要的容量，而只會爲前 n 給 vec 填充數據(理所當然否則就內存越界了)

* 修改 evbuffer\_iovec 指向的數據會導致未定義行爲(它們是只讀的)
* 任何修改 evbuffer 的函數都會導致 evbuffer\_peek 產生的指針變得無效

# 直接添加 evbuffer\_iovec

你可以將 evbuffer\_iovec 直接添加到 evbuffer，首先你需要調用 [evbuffer\_reserve\_space(buf, size, vec, n)]() 來申請 evbuffer\_iovec

* **buf** 是要寫入的 evbuffer 指針
* **size** 是需要提供的字節容量，實際返回的容量可能大於這個值
* **vec** 用於接收 evbuffer\_iovec 的數組指針
* **n** vec 數組的容量，至少爲 1

成功返回提供的 vec 數量，失敗返回 -1

這個函數會優先找到內部空閒的鏈返回給 vec，此外 n 最好設置爲 2，否則太小的鏈將無法使用，這會導致內部不得不線性化一些鏈可能影響效率。此外目前版本(2023-05-31 libevent-2.1.12-stable) n 大於2 也只會返回兩個鏈，但後續版本可能會修改

在寫入數據後可以使用 [evbuffer\_commit\_space(buf, vec, n)](https://libevent.org/doc/buffer_8h.html#ab9cd3918d883d90acda2d9a9c530bf84) 來提交鏈，成功返回0，失敗返回 -1

```
#include <event2/buffer.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void println_evbuffer(struct evbuffer *buf)
{
    struct evbuffer_ptr start;
    evbuffer_ptr_set(buf, &start, 0, EVBUFFER_PTR_SET);
    struct evbuffer_iovec vec;
    int n;
    while (1)
    {
        n = evbuffer_peek(buf, -1, &start, &vec, 1);
        if (!n)
        {
            break;
        }
        for (size_t i = 0; i < vec.iov_len; i++)
        {
            putc(((char *)(vec.iov_base))[i], stdout);
        }
        evbuffer_ptr_set(buf, &start, vec.iov_len, EVBUFFER_PTR_ADD);
    }
    puts("");
}
int example_space(struct evbuffer *buf)
{
    struct evbuffer_iovec vec[2];
    // 保證創建至少 2 個 vec 以便測試
    for (size_t i = 2;; i++)
    {
        // 在 commit 前多次調用 reserve_space 是等效的
        if (evbuffer_reserve_space(buf, i, &vec, 2) == 2)
        {
            break;
        }
    }

    for (size_t i = 0; i < 2; i++)
    {
        size_t n = vec[i].iov_len > 10 ? 10 : vec[i].iov_len;
        memset(vec[i].iov_base, 'A' + i, n);
        // 可以修改提交的容量
        vec[i].iov_len = n;
    }

    // 提交
    if (evbuffer_commit_space(buf, &vec, 2))
    {
        puts("evbuffer_commit_space error");
        return -1;
    }

    // 打印
    println_evbuffer(buf);
    return 0;
}
int main(int argc, char *argv[])
{
    int ret = -1;
    struct evbuffer *buf = NULL;

    buf = evbuffer_new();
    if (!buf)
    {
        puts("evbuffer_new error");
        goto END;
    }
    if (evbuffer_add(buf, "example: ", 9))
    {
        puts("evbuffer_add error");
        goto END;
    }
    ret = example_space(buf);
END:
    if (buf)
    {
        evbuffer_free(buf);
    }
    return ret;
}
```

> 最好不要使用這兩個函數除非你明確知道自己在做什麼，通常沒有必要調用它們並且很任意出錯


# 網路 IO

evbuffer 最常見的使用場景是網路 IO

[evbuffer\_read(buf, fd, howmuch)](https://libevent.org/doc/buffer_8h.html#a2664ab1d68d21acf3185a01c85cf29af) 從 fd 讀取最大 howmuch 字節的數據到緩衝區尾。它成功時返回讀取到的字節數，遇到 EOF 返回0 ，出錯時返回 -1。請注意在 windows 上錯誤可能表示非阻塞操作不成功，你需要檢查 EAGAIN (或 windows 上的 WSAEWOULDBLOCK) 錯誤代碼。如果 howmuch 是負數或超出每次讀取的最大字節數，則會儘可能多的讀取數據

[evbuffer\_write\_atmost(buf, fd, howmuch)](https://libevent.org/doc/buffer_8h.html#a69f9cbac8f6dbd2d8f3c2706fbbdc7cd) 嘗試將緩衝區前端最多 howmuch字節寫入 fd。成功返回寫入字節數，失敗返回 -1。與 evbuffer\_read 類似，你需要檢查錯誤碼以確定是真實錯誤或只是表明非阻塞IO無法立刻完成。如果 homuch 是負數，會嘗試儘可能多的寫入數據

[evbuffer\_write(buf, fd)](https://libevent.org/doc/buffer_8h.html#a7d9605737ec9ba876f0ba49e937b9bd7) 是 evbuffer\_write\_atmost(buf, fd, -1) 的語法糖

在 linux 上這些函數對檔案描述符都有效，但 widnwos 只對 socket 有效

通常應該使用 bufferevents 來操作 socket 你不需要字節使用這三個函數

# 回調

evbuffer 提供了幾個回調函數以通知用戶何時向 evbuffer 添加或刪除了數據

```
struct evbuffer_cb_info {
        // 緩衝區大小改變前有多少字節
        size_t orig_size;
        // 多少字節被添加到緩衝區
        size_t n_added;
        // 多少字節被從緩衝區中刪除
        size_t n_deleted;
};

typedef void (*evbuffer_cb_func)(struct evbuffer *buffer,
    const struct evbuffer_cb_info *info, void *arg);
```

每當將數據添加到 evbuffer 或從中刪除數據時，都會調用回調 evbuffer\_cb\_func。
它接收 evbuffer 指針， evbuffer\_cb\_info 指針以及用戶自定義參數 arg


你可以使用 [evbuffer\_add\_cb(buf, cb, arg)](https://libevent.org/doc/buffer_8h.html#a3885fb5ca54eff0778916183f58a9c25) 來添加一個數據讀寫回調，成功返回 evbuffer\_cb\_entry 指針，失敗返回 NULL。可以多次調用設置多個互相獨立的回調，cb 傳入 NULL 則會清空所有回調

調用 [evbuffer\_remove\_cb\_entry(buf, ent)](https://libevent.org/doc/buffer_8h.html#ade150950a995af06cd3c6321f71eb3c1) 來刪除設置的回調，成功返回 0，失敗返回 -1

也可以使用 [evbuffer\_remove\_cb(buf, cb, arg)](https://libevent.org/doc/buffer_8h.html#abc4912118c60071766c6a9d343186137) 刪除所有使用指定回調函數和參數的回調，成功返回 0，失敗返回 -1

[evbuffer\_cb\_set\_flags(buf, ent, flags)](https://libevent.org/doc/buffer_8h.html#af5fa49788238edd197d3f242878576f3) [evbuffer\_cb\_clear\_flags(buf, ent, flags)](https://libevent.org/doc/buffer_8h.html#ae384dcadf478c364d005f57be899b21b) 用來爲 回調 設置/取消 標記，目前可用的標記只有 **EVBUFFER\_CB\_ENABLED**，這也是默認的，如果被 clear 則不會進行回調

如果有多個 evbuffer 並且其回調可能對導致彼此添加和刪除這可能會破壞堆棧，對此你可以調用 [evbuffer\_defer\_callbacks(buf, eb)](https://libevent.org/doc/buffer_8h.html#a97293fcb83dd9d073af4cf370b9d094d) 延遲回調到 事件循環

# 無拷貝 IO
真正快速的 IO 通常需要避免拷貝， evbuffer 提供了類似機制

```
typedef void (*evbuffer_ref_cleanup_cb)(const void *data,
    size_t datalen, void *extra);

int evbuffer_add_reference(struct evbuffer *outbuf,
    const void *data, size_t datlen,
    evbuffer_ref_cleanup_cb cleanupfn, void *extra);
```

[evbuffer_add_reference(buf, data, len, cleanupfn, extra)](https://libevent.org/doc/buffer_8h.html#a35e99b5370b59b0c0b391ada3a59befe) 將長度爲 len 的數據 data 添加到 buf 爲並保存對它的引用，當這部分數據不再需要時調用 清理函數 cleanupfn(data, len, extra) 對數據進行清理(這也是 evbuffer\_peek 返回的內存鏈指針只讀的原因，它可能來自一個只讀的引用)。成功返回 0，失敗返回 -1

# 添加 file
一些操作系統提供了將檔案寫入網路而無需將數據拷貝到用戶空間的方法。你可以簡單的調用 [evbuffer\_add\_file(buf, fd, offset, len)](https://libevent.org/doc/buffer_8h.html#a601996b1fc1f5c165dc62b89acbd069e) 來將數據添加到 evbuffer，成功返回 0 失敗返回 -1

1. 如果系統支持 splice 或 sendfile 會優先使用
2. 不支持會使用 nmap  或在 widnwos 上使用 CreateFileMapping 創建檔案映射，這種情況操作系統可能能夠識別是否需要把數據拷貝到用戶空間

要更詳細的控制檔案你可以使用 [evbuffer\_file\_segment\_new(fd, offset, len, flags)](https://libevent.org/doc/buffer_8h.html#aa52264c1529ee486400bbad0bf9f4271) 來創建 evbuffer\_file\_segment 指針，flags 指示了工作細節

* **EVBUF\_FS\_CLOSE\_ON\_FREE** 如果設置在調用 evbuffer\_file\_segment\_free 是自動釋放底層檔案
* **EVBUF\_FS\_DISABLE\_MMAP** 禁用內存映射，即使它是合適的
* **EVBUF\_FS\_DISABLE\_SENDFILE** 禁用 sendfile/splice，即使它是合適的
* **EVBUF\_FS\_DISABLE\_LOCKING** 不會爲檔案段分配任何鎖

之後你可以調用 [evbuffer\_add\_file\_segment(buf, seg, offset, len)](https://libevent.org/doc/buffer_8h.html#a300f9e6d1db3d6f834284c47fc77e1bc) 將檔案段添加到緩衝區，注意這裏的 offset 是相對 seg 的偏移而非相對原始檔案，成功返回 0，失敗返回 -1

當不在需要 seg 時需要調用 [evbuffer\_file\_segment\_free(seg)](https://libevent.org/doc/buffer_8h.html#a0b82a4d7dd6853882ed5a87559edbad1) 來釋放資源

通常你不需要使用 **evbuffer\_file\_segment** libevent 默認會選擇最合適的實現，只在不得已時自動過渡到更慢的實現(例如平臺不支持，或你將數據讀取到內存而非發送給 socket)

你可以使用 [evbuffer\_file\_segment\_add\_cleanup\_cb(seg, seg, arg)](https://libevent.org/doc/buffer_8h.html#ab0767fa82fecc6255129928b02ecc1d6) 來設置一個回調，當 seg 引用計數爲0 後被真實釋放時回調

```
typedef void (*evbuffer_file_segment_cleanup_cb)(
    struct evbuffer_file_segment const *seg, int flags, void *arg);

void evbuffer_file_segment_add_cleanup_cb(struct evbuffer_file_segment *seg,
        evbuffer_file_segment_cleanup_cb cb, void *arg);
```

```
#include <event2/buffer.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

void read_evbuffer(struct evbuffer *buf)
{
    char s[11];
    int n;
    while (1)
    {
        n = evbuffer_remove(buf, s, 10);
        if (n < 1)
        {
            break;
        }
        s[n] = 0;
        printf("%s", s);
    }
    puts("");
    printf("%zu\n", evbuffer_get_length(buf));
}
void cleanup_cb(struct evbuffer_file_segment const *seg, int flags, void *arg)
{
    puts("cleanup_cb");
}
int example_segment(struct evbuffer *buf, int fd)
{
    struct evbuffer_file_segment *seg = evbuffer_file_segment_new(fd, 0, 10, 0);
    if (!seg)
    {
        puts("evbuffer_file_segment_new error");
    }
    evbuffer_file_segment_add_cleanup_cb(seg, cleanup_cb, 0);
    if (evbuffer_add_file_segment(buf, seg, 0, 10))
    {
        puts("evbuffer_add_file_segment error");
        evbuffer_file_segment_free(seg);
        return -1;
    }
    if (evbuffer_add_file_segment(buf, seg, 0, 10))
    {
        evbuffer_file_segment_free(seg);
        puts("evbuffer_add_file_segment error");
        return -1;
    }
    // seg 採用了引用計算，不在使用了就可以 free 不會影響已經添加的 buf
    evbuffer_file_segment_free(seg);

    read_evbuffer(buf);
    return 0;
}
int main(int argc, char *argv[])
{
    int ret = -1;
    struct evbuffer *buf = NULL;
    int fd = open("main.js", O_RDONLY, 0666);
    if (fd == -1)
    {
        puts(strerror(errno));
        goto END;
    }
    buf = evbuffer_new();
    if (!buf)
    {
        puts("evbuffer_new error");
        goto END;
    }
    if (evbuffer_add(buf, "example: ", 9))
    {
        puts("evbuffer_add error");
        goto END;
    }
    ret = example_segment(buf, fd);
END:
    if (fd != -1)
    {
        close(fd);
    }
    if (buf)
    {
        evbuffer_free(buf);
    }
    return ret;
}
```


# 引用 evbuffer

你可以調用 [evbuffer\_add\_buffer\_reference(dst, src)](https://libevent.org/doc/buffer_8h.html#a35e99b5370b59b0c0b391ada3a59befe) 將一個 evbuffer 添加到另外一個 evbuffer 未，這不會發生真實的數據拷貝但行爲就像真的拷貝了一樣，它會將 src 內部的鏈引用計數加一 之後添加到 dst 鏈的末尾，所以行爲看起來就像真的將 src 的數據添加到了 dst 一樣，如果函數成功返回 0，失敗返回 -1

注意它只是將目前 src 鏈的情況添加，所以後續 src 發生了讀寫都不會影響到 dst，並且因爲底層內存鏈使用了引用計算，故可以安全的刪除 src 也不會影響到 dst，但是要注意，src 和 dst 不能嵌入引用


# add-only remove-only

你可以調用 [evbuffer\_freeze(buf, at\_front)](https://libevent.org/doc/buffer_8h.html#a2b32b99d60603aac9bbadb9e463a6bc1) 來凍結緩衝區的前端(at\_front 爲真)或後端(at\_front 爲假)，如果前端被凍結則無法從緩衝區中移除數據，如果後端被凍結則無法將數據添加到緩衝區。函數成功返回 0，失敗返回 -1

你可以調用 [evbuffer\_unfreeze(buf, at\_front)](https://libevent.org/doc/buffer_8h.html#af36bd50996e2db4445282e6a194328a2) 來解凍。函數成功返回 0，失敗返回 -1

