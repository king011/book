# [準備工作](https://pay.weixin.qq.com/index.php/apply/applyment_home/guide_normal)

首先需要到騰訊官網註冊一個商戶，並依據指引完善質料並開通需要的服務


# [APIv3](https://pay.weixin.qq.com/doc/v3/merchant/4012081606)

目前(2024-10-15)騰訊推出了最新的 APIv3 接口。以 REST 風格提供了 HTTPS 接口，以 JSON 作爲數據交互格式，以 SHA256-RSA 作爲數字簽名算法，以 AES-256-GCM 加密關鍵數據

你可以直接使用喜歡的語言發送 HTTP 來請求這些接口，此外騰訊也提供了一些語言的客戶端庫，後續文字將以官方 [go](https://github.com/wechatpay-apiv3/wechatpay-go) 客戶端爲例，提供使用說明

```
go get -u github.com/wechatpay-apiv3/wechatpay-go
```

# 客戶端模式

目前(2024-10-15) api 分爲了證書模式和公鑰模式, 下面是使用公鑰模式初始化客戶端

```
package main

import (
	"context"
	"fmt"
	"log"

	"github.com/wechatpay-apiv3/wechatpay-go/core"
	"github.com/wechatpay-apiv3/wechatpay-go/core/option"
	"github.com/wechatpay-apiv3/wechatpay-go/utils"
)

const (
	// 商戶號
	MchID = `17XXXXXX12`
	// 商戶證書序列號
	CertificateSerialNo = `3FXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXE8`
	// 商戶私鑰路徑
	PrivateKey = `apiclient_key.pem`
	// 微信支付公鑰 id
	PublicKeyID = `PUB_KEY_ID_0117XXXXXXXXXXXXXXXXXXXXXXXXXX0863`
	// 微信支付公鑰路徑
	PublicKey = `pub_key.txt`
)

func main() {
	// 從本地檔案加載商戶私鑰, 私鑰會用來生成請求簽名
	privateKey, e := utils.LoadPrivateKeyWithPath(PrivateKey)
	if e != nil {
		log.Fatal("load merchant private key fail:", e)
	}
	// 從本地檔案加載微信支付公鑰, 公鑰會用來驗證響應簽名
	publicKey, e := utils.LoadPublicKeyWithPath(PublicKey)
	if e != nil {
		log.Fatal("load merchant public key fail:", e)
	}

	// 創建客戶端
	client, e := core.NewClient(context.Background(),
		// 使用公鑰模式驗證簽名
		option.WithWechatPayPublicKeyAuthCipher(MchID,
			CertificateSerialNo,
			privateKey,
			PublicKeyID,
			publicKey,
		),
	)
	if e != nil {
		log.Fatal("new wechat pay client fial", e)
	}

	fmt.Println(client)
}
```

Client 提供了各種與 http action 同名的函數用於發送指定方法的 http 請求，使用 Client 的好處是它會在發送時自動計算簽名，並在接收到響應時自動驗證簽名