# 本地調試
得益與 v8 nodejs 提供了 單步調試等 功能

只需要 執行 node debug xxx.js 即可

| 命令 | 描述 | 
| -------- | -------- | 
| run     | 執行腳本,在第一行暫停     | 
| restart     | 重新執行腳本     | 
| cont,c     | 執行直到遇到斷點     | 
| next,n     | 單步執行     | 
| step,s     | 單步進入     | 
| out,o     | 從函數中步出     | 
| setBreakpoing(),sb()     | 在當前行設置斷點     | 
| setBreakpoing('f()'),sb(...)     | 在函數 f 第一行設置 斷點     | 
| setBreakpoing('main.js',20),sb(...)     | 在main.js 第20行設置斷點     | 
| clearBreakpoint(),cb()     | 清除斷點     | 
| backtrace,bt     | 顯示調用棧     | 
| list(5)     | 顯示 當前執行的 前後 5 行 代碼     | 
| watch(expr)     | 增加監視     | 
| unwatch(expr)     | 刪除監視     | 
| watchers     | 顯示 所有 監視 表達式 值     | 
| repl     | 在當前 上下文 打開 即時模式     | 
| kill     | 終止腳本執行     | 
| scripts     | 顯示 已加載 腳本     | 
| version     | 顯示 v8 版本     | 

# 遠程調試
```sh
# 打開遠程調試 在 指定端口 默認 5858
node --debug[=port] xxx.js

# 打開遠程調試 在 指定端口 默認 5858
# 但會暫停 腳本 等待客戶連接
node --debug-brk[=port] xxx.js

# 連接 調試 服務器
node debug address:port 
```

> 本地調試 只是將 遠程調試的 兩個 命令 整合到了一個 快捷命令

# node-inspector
node-inspector 提供了 類似 chrome 的調試功能
```sh
# 安裝 node-inspector
npm install -g node-inspector

# 運行 node-inspector
node-inspector
```
