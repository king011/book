# 備份 還原

對於 備份 還原 必須是相同版本的 gitlab

* gitlab\_rails\[&#39;backup\_path&#39;\] 指定 備份位置
* gitlab\_rails\[&#39;backup\_keep\_time&#39;\] 指定 保存備份時間 以秒計算

# 備份

```
# 創建備份
gitlab-rake gitlab:backup:create

# docker 創建備份
docker exec -t gitlab gitlab-rake gitlab:backup:create
```

# 還原

```
# 從 xxx_gitlab_backup.tar 檔案 還原
# 不用帶 _gitlab_backup.tar 後綴
gitlab-rake gitlab:backup:restore BACKUP=xxx
```