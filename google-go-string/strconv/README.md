# [strconv](https://pkg.go.dev/strconv)

```
import "strconv"
```

strconv 套件實現了基本資料類型的字串表示形式的轉換

## const IntSize

```
const IntSize = intSize
```

IntSize 返回了 int/uint 的 bit 長度，通常可以用來判斷當前程序的目標平臺是 32bit 還是 64bit


## [func FormatBool ](https://pkg.go.dev/strconv#FormatBool)

```
func FormatBool(b bool) string
```

FormatBool 根據 b 的值傳回「true」或「false」

## [func AppendBool](https://pkg.go.dev/strconv#AppendBool)

```
func AppendBool(dst []byte, b bool) []byte
```

AppendBool 根據 b 的值將「true」或「false」附加到 dst 並傳回擴充緩衝區

## [func ParseBool](https://pkg.go.dev/strconv#ParseBool)

```
func ParseBool(str string) (bool, error)
```

傳回字串表示的布林值。它接受 1、t、T、TRUE、true、True、0、f、F、FALSE、false、False。任何其他值都會傳回錯誤


## [func FormatFloat](https://pkg.go.dev/strconv#FormatFloat)

```
func FormatFloat(f float64, fmt byte, prec, bitSize int) string
```

FormatFloat 根據格式 fmt 和精確度 prec 將浮點數 f 轉換為字串。它假設原始結果是從 bitSize 位的浮點值（對於 float32 為 32，對於 float64 為 64）獲得的結果進行四捨五入

fmt 的有效值是
* 'b' (-ddddp±ddd, a binary exponent),
*  'e' (-d.dddde±dd, a decimal exponent),
* 'E' (-d.ddddE±dd, a decimal exponent),
* 'f' (-ddd.dddd, no exponent),
* 'g' ('e' for large exponents, 'f' otherwise),
* 'G' ('E' for large exponents, 'f' otherwise),
* 'x' (-0xd.ddddp±ddd, a hexadecimal fraction and binary exponent), or
* 'X' (-0Xd.ddddP±ddd, a hexadecimal fraction and binary exponent).

precision prec 控制「e」、「E」、「f」、「g」、「G」、「x」和「X」格式列印的位數（不包括指數）。對於“e”、“E”、“f”、“x”和“X”，它是小數點後的位數。對於“g”和“G”，它是有效數字的最大數量（刪除尾隨零）。特殊精確度 -1 使用所需的最小位數，以便 ParseFloat 準確地傳回 f

## [func AppendFloat](https://pkg.go.dev/strconv#AppendFloat)

```
func AppendFloat(dst []byte, f float64, fmt byte, prec, bitSize int) []byte
```

將由 FormatFloat 產生的浮點數 f 的字串形式附加到 dst 並傳回擴展緩衝區

## [func ParseFloat](https://pkg.go.dev/strconv#ParseFloat)

```
func ParseFloat(s string, bitSize int) (float64, error)
```

ParseFloat 將字串 s 轉換為浮點數，其精確度由 bitSize 指定：對於 float32，為 32；對於 float64，為 64。當bitSize=32時，結果仍然是float64類型，但它可以轉換為float32而不改變其值。

ParseFloat 接受 Go 浮點文字語法定義的十進位和十六進位浮點數。如果 s 格式正確且接近有效浮點數，ParseFloat 將傳回使用 IEEE754 無偏舍入進行舍入的最接近的浮點數。 （僅當十六進位表示中的位數多於尾數所能容納的位數時，解析十六進位浮點值才會進行捨入。）

ParseFloat 傳回的錯誤具有具體類型 *NumError 並包含 err.Num = s。

如果 s 的語法格式不正確，ParseFloat 將傳回 err.Err = ErrSyntax。

如果 s 在語法上格式良好，但與給定大小的最大浮點數的距離超過 1/2 ULP，則 ParseFloat 傳回 f = ±Inf, err.Err = ErrRange。

ParseFloat 將字串「NaN」以及（可能有符號的）字串「Inf」和「Infinity」識別為其各自的特殊浮點值。匹配時忽略大小寫。

## [func FormatUint](https://pkg.go.dev/strconv#FormatUint)

```
func FormatUint(i uint64, base int) string
```

FormatUint 傳回 i 在給定基數中的字串表示形式，2 <= base <= 36

結果使用小寫字母“a”到“z”來表示 >= 10 的數字值

## [func AppendUint](https://pkg.go.dev/strconv#AppendUint)

```
func AppendUint(dst []byte, i uint64, base int) []byte
```

AppendUint 將由 FormatUint 產生的無符號整數 i 的字串形式附加到 dst 並傳回擴展緩衝區

## [func ParseUint](https://pkg.go.dev/strconv#ParseUint)

```
func ParseUint(s string, base int, bitSize int) (uint64, error)
```

ParseUint 與 ParseInt 類似，但適用於無符號數

不允許使用符號前綴

## [func FormatInt](https://pkg.go.dev/strconv#FormatInt)

```
func FormatInt(i int64, base int) string
```

FormatInt 傳回 i 在給定基數中的字串表示形式，2 <= base <= 36

結果使用小寫字母“a”到“z”來表示 >= 10 的數字值


## [func AppendInt](https://pkg.go.dev/strconv#AppendInt)

```
https://pkg.go.dev/strconv#AppendInt
```

AppendInt 將由 FormatInt 產生的整數 i 的字串形式追加到 dst 並傳回擴充緩衝區

## [func ParseInt](https://pkg.go.dev/strconv#ParseInt)

```
func ParseInt(s string, base int, bitSize int) (i int64, err error)
```

ParseInt 以給定基數（0、2 到 36）和位元大小（0 到 64）解釋字串 s 並傳回對應的值 i。

該字串可以以一導符號開頭：“+”或“-”。

如果基底參數為 0，則真正的基數由符號後面的字串前綴（如果存在）隱含：2 表示“0b”，8 表示“0”或“0o”，16 表示“0x”，否則為 10。此外，僅對於參數基數 0，允許使用下劃線字符，如 Go 語法對整數文字的定義。

bitSize 參數指定結果必須適合的整數類型。位元大小 0、8、16、32 和 64 對應於 int、int8、int16、int32 和 int64。如果 bitSize 低於 0 或高於 64，則傳回錯誤。

ParseInt 傳回的錯誤具有具體類型 \*NumError 並包含 err.Num = s。如果 s 為空或包含無效數字，則 err.Err = ErrSyntax，傳回值為 0；如果 s 對應的值不能用給定大小的有符號整數表示，則 err.Err = ErrRange 且傳回值是對應 bitSize 和符號的最大幅度整數。


## [func IsGraphic](https://pkg.go.dev/strconv#IsGraphic)

```
func IsGraphic(r rune) bool
```

IsGraphic 報告符文是否被 Unicode 定義為圖形。此類字元包括 L、M、N、P、S 和 Z 類別的字母、標記、數字、標點符號、符號和空格

## [func IsPrint](https://pkg.go.dev/strconv#IsPrint)

```
func IsPrint(r rune) bool
```

IsPrint 報告 rune 是否被 Go 定義為可列印，其定義與 unicode 相同

## [func Quote](https://pkg.go.dev/strconv#Quote)

```
https://pkg.go.dev/strconv#Quote
```

Quote 傳回表示 s 的雙引號 Go 字串文字。傳回的字串使用 Go 轉義序列（\t、\n、\xFF、\u0100）作為 IsPrint 定義的控製字元和不可列印字元

## [func AppendQuote](https://pkg.go.dev/strconv#AppendQuote)

```
func AppendQuote(dst []byte, s string) []byte
```

AppendQuote 將由 Quote 產生的表示 s 的雙引號 Go 字串文字附加到 dst 並傳回擴充緩衝區

## [func QuoteToASCII](https://pkg.go.dev/strconv#QuoteToASCII)

```
func QuoteToASCII(s string) string
```

QuoteToASCII 傳回表示 s 的雙引號 Go 字串文字。傳回的字串對 IsPrint 定義的非 ASCII 字元和不可列印字元使用 Go 轉義序列（\t、\n、\xFF、\u0100）。

## [func AppendQuoteToASCII](https://pkg.go.dev/strconv#AppendQuoteToASCII)

```
func AppendQuoteToASCII(dst []byte, s string) []byte
```

AppendQuoteToASCII 將由 QuoteToASCII 產生的表示 s 的雙引號 Go 字串文字附加到 dst 並傳回擴展緩衝區

## [func QuoteToGraphic](https://pkg.go.dev/strconv#QuoteToGraphic)

```
func QuoteToGraphic(s string) string
```

QuoteToGraphic 傳回表示 s 的雙引號 Go 字串文字。傳回的字串保留 IsGraphic 定義的 Unicode 圖形字元不變，並對非圖形字元使用 Go 轉義序列（\t、\n、\xFF、\u0100）

## [func AppendQuoteToGraphic](https://pkg.go.dev/strconv#AppendQuoteToGraphic)

```
func AppendQuoteToGraphic(dst []byte, s string) []byte
```

AppendQuoteToGraphic 將由 QuoteToGraphic 產生的表示 s 的雙引號 Go 字串文字附加到 dst 並傳回擴展緩衝區

## [func QuoteRune](https://pkg.go.dev/strconv#QuoteRune)

```
func QuoteRune(r rune) string
```

QuoteRune 傳回表示符文的單引號 Go 字元文字。傳回的字串使用 Go 轉義序列（\t、\n、\xFF、\u0100）作為 IsPrint 定義的控製字元和不可列印字元。如果 r 不是有效的 Unicode 碼點，它將被解釋為 Unicode 替換字元 U+FFFD。


## [func AppendQuoteRune](https://pkg.go.dev/strconv#AppendQuoteRune)

```
func AppendQuoteRune(dst []byte, r rune) []byte
```

AppendQuoteRune 將由 QuoteRune 產生的表示符文的單引號 Go 字元文字附加到 dst 並返回擴展緩衝區

## [func QuoteRuneToASCII](https://pkg.go.dev/strconv#QuoteRuneToASCII)

```
func QuoteRuneToASCII(r rune) string
```

QuoteRuneToASCII 傳回表示符文的單引號 Go 字元文字。傳回的字串對 IsPrint 定義的非 ASCII 字元和不可列印字元使用 Go 轉義序列（\t、\n、\xFF、\u0100）。如果 r 不是有效的 Unicode 碼點，它將被解釋為 Unicode 替換字元 U+FFFD

## [func AppendQuoteRuneToASCII](https://pkg.go.dev/strconv#AppendQuoteRuneToASCII)

```
func AppendQuoteRuneToASCII(dst []byte, r rune) []byte
```

AppendQuoteRuneToASCII 將由 QuoteRuneToASCII 產生的表示符文的單引號 Go 字元文字附加到 dst 並返回擴展緩衝區

## [func QuoteRuneToGraphic](https://pkg.go.dev/strconv#QuoteRuneToGraphic)

```
func QuoteRuneToGraphic(r rune) string
```

QuoteRuneToGraphic 傳回表示符文的單引號 Go 字元文字。如果符文不是 IsGraphic 定義的 Unicode 圖形字符，則傳回的字串將使用 Go 轉義序列（\t、\n、\xFF、\u0100）。如果 r 不是有效的 Unicode 碼點，它將被解釋為 Unicode 替換字元 U+FFFD。

## [func AppendQuoteRuneToGraphic](https://pkg.go.dev/strconv#AppendQuoteRuneToGraphic)

```
func AppendQuoteRuneToGraphic(dst []byte, r rune) []byte
```

AppendQuoteRuneToGraphic 將由 QuoteRuneToGraphic 產生的表示符文的單引號 Go 字元文字附加到 dst 並返回擴展緩衝區


Unquote 將 s 解釋為單引號、雙引號或反引號 Go 字串文字，傳回 s 引用的字串值。 （如果 s 是單引號的，那麼它將是 Go 字元文字；取消引號會傳回對應的單字字串。）

## [func CanBackquote](https://pkg.go.dev/strconv#CanBackquote)

```
func CanBackquote(s string) bool
```

CanBackquote 報告字串 s 是否可以不變地表示為不帶製表符以外的控製字元的單行反引號字串

## [func QuotedPrefix](https://pkg.go.dev/strconv#QuotedPrefix)

```
func QuotedPrefix(s string) (string, error)
```

QuotedPrefix 傳回 s 前綴處的引號的字串（如 Unquote 所理解的）。如果 s 不是以有效的帶引號的字串開頭，QuotedPrefix 將傳回錯誤。

## [func Unquote](https://pkg.go.dev/strconv#Unquote)

```
func Unquote(s string) (string, error)
```

Unquote 將 s 解釋為單引號、雙引號或反引號 Go 字串文字，傳回 s 引用的字串值。 （如果 s 是單引號的，那麼它將是 Go 字元文字；取消引號會傳回對應的單字字串。）

## [func UnquoteChar](https://pkg.go.dev/strconv#UnquoteChar)

```
func UnquoteChar(s string, quote byte) (value rune, multibyte bool, tail string, err error)
```

UnquoteChar 解碼轉義字串或字串 s 表示的字元文字中的第一個字元或位元組。它傳回四個值：

1. **value** 解碼後的 Unicode 碼點或位元組值
2. **multibyte** 一個布林值，指示解碼的字元是否需要多位元組 UTF-8 表示形式
3. **tail** 字元後字串的剩餘部分
4. **err** 如果字元在語法上有效，則錯誤將為 nil

第二個參數 quote 指定正在解析的文字類型，從而指定允許使用哪個轉義引號字元。如果設定為單引號，則允許序列 \' 並不允許未轉義的 '。如果設定為雙引號，則允許 \" 且不允許未轉義的 "。如果設定為零，則不允許任何轉義，並允許兩個引號字元顯示為未轉義