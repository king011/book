# 開機啓動
wsl 必須運行到登入用戶下，故無法以服務啓動，但可以寫一些腳本讓用戶登入後自動啓動

1. 首先在 wsl 的 linux 系統創建一個可執行腳本 **/usr/bin/wsl\_linux.sh**

	```
	#!/bin/bash
	while [ true ]
	do
					cat /etc/issue
					uname -a
					echo
					date
					echo
					sleep 3600
	done
	```
	
	這個腳本將一直執行，什麼都不做所以不會消耗什麼資源，它的目的是爲了讓我們用來監控，子系統是否正常運行
	
2. 在 widnows 系統下創建一個腳本，用於啓動子系統，並監控在子系統退出後重啓子系統 **C:\Windows\System32\wsl\_ubuntu\_22.04.bat**

	```
	@echo off

	:MAIN
	@REM 啓動子系統
	wsl -d ubuntu-22.04 wsl_linux.sh

	@REM 等待 一分鐘 再重啓
	@timeout /t 60
	goto MAIN
	```

3. 將腳本加入註冊表啓動項目

	```
	HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
	```
	
	![](assets/regedit.png)

# ip 變化
wsl 運行的子系統可能會在重啓後改變 ip 這個主機訪問子系統造成了麻煩，一個解決方案是將 widnwos 的 **c:\Windows\System32\drivers\etc\hosts** 的修改權限設置給執行 wsl 的用戶，然後在 wsl 的子系統啓動時獲取自己ip並寫入 hosts 檔案


右擊->屬性->安全->編輯

![](assets/hosts0.png)
![](assets/hosts1.png)


```
#!/bin/bash
set -e
# 寫入 hosts 中的機器名稱
domain=ubuntu22.wsl

# 宿主機 hosts 檔案路徑
hosts_path="/mnt/c/Windows/System32/drivers/etc/hosts"

# 獲取 wsl ip
ip=$(ifconfig eth0 | grep -w inet | awk '{print $2}')

# 判斷 domain 是否已經存在
if grep -wq "$domain" "$hosts_path"; then
    # 重寫 hosts 檔案以便刪除 過期的靜態 ip
    hosts=$(grep -vw "$domain" "$hosts_path")
    echo "$hosts" > $hosts_path
fi
# 寫入靜態名稱
echo "$ip $domain" >> $hosts_path

### 同樣我們可以爲 wsl 子系統寫入一個固定的靜態名稱 用於訪問宿主機 ###
# 寫入 hosts 中的機器名稱
domain=host.wsl
hosts_path=/etc/hosts
ip=$(cat /etc/resolv.conf | grep "nameserver" | awk '{print $2}')
if grep -wq "$domain" "$hosts_path"; then
    hosts=$(grep -vw "$domain" "$hosts_path")
    echo "$hosts" > $hosts_path
fi
echo "$ip $domain" >> $hosts_path
```

結合 開機啓動 我們可以把這段代碼添加到 **wsl\_linux.sh** 的結尾，這樣每當宿主器重啓或者我們調用這個腳本啓動 wsl 時就會自動更新 hosts 檔案

```
#!/bin/bash
set -e
# 寫入 hosts 中的機器名稱
domain=ubuntu22.wsl

# 宿主機 hosts 檔案路徑
hosts_path="/mnt/c/Windows/System32/drivers/etc/hosts"

# 獲取 wsl ip
ip=$(ifconfig eth0 | grep -w inet | awk '{print $2}')
wsl_ip=$ip

# 判斷 domain 是否已經存在
if grep -wq "$domain" "$hosts_path"; then
    # 重寫 hosts 檔案以便刪除 過期的靜態 ip
    hosts=$(grep -vw "$domain" "$hosts_path")
    echo "$hosts" > $hosts_path
fi
# 寫入靜態名稱
echo "$ip $domain" >> $hosts_path

### 同樣我們可以爲 wsl 子系統寫入一個固定的靜態名稱 用於訪問宿主機 ###
# 寫入 hosts 中的機器名稱
domain=host.wsl
hosts_path=/etc/hosts
ip=$(cat /etc/resolv.conf | grep "nameserver" | awk '{print $2}')
if grep -wq "$domain" "$hosts_path"; then
    hosts=$(grep -vw "$domain" "$hosts_path")
    echo "$hosts" > $hosts_path
fi
echo "$ip $domain" >> $hosts_path

set +e
while [ true ]
do
    cat /etc/issue
    uname -a
    echo " - $(date)"
    echo " - $wsl_ip"
    echo
    sleep 3600
done
```
	
# 靜態 ip 與開機啓動

上面通過修改 hosts 的方式是 宿主機與 wsl 互訪的最簡單方案，但存在一些問題。如果宿主機中部署了 nginx，並且 nginx 和 wsl 都開機自動啓動，nginx 無法將流量路由到 wsl，因爲 nginx 會比 wsl 先啓動(wsl 和原生程式比起來啓動超慢)，這時 hosts 還沒有被修改故 nginx 無法找到 domain 解析到的 wsl 的ip 或者將它解析到上次 wsl 記錄的已經過期的 ip。

更好的解決方案是 爲 wsl 系統 和 宿主機 在統一網段內都指定一個固定的 ip ，通過下述兩條命令即可完成

```
netsh interface ip add address "vEthernet (WSL)" 192.168.50.1 255.255.255.0

wsl -d Ubuntu -u root ip addr add 192.168.50.10/24 broadcast 192.168.50.255 dev eth0 label eth0:1
```

1. 第一條命令(後面簡稱**命令一**)將爲 windows 中與 wsl 系統通信的虛擬網卡添加了一個 192.168.50.1/24 的 ip
2. 第二條命令(後續簡稱**命令二**)將爲 Ubuntu 這個子系統創建添加一個 192.168.50.10/24 的 ip

如此宿主機 和 wsl 將處於 192.168.50.0/24 這個共同的網段，並且可以使用對方的固定 ip 進行互訪。但是 windows 糟糕的設計導致要在宿主機啓動時自動執行這兩條命令變得困難，下面是面臨的問題簡述：

1. **命令一** 需要管理員用戶執行，因爲它修改了系統設定。**命令二** 需要使用桌面用戶執行，因爲 wsl 子系統是和桌面用戶綁定的
2. widnows 沒有類似 sudo/su 之類的指令取切換命令執行者，唯一的 runas 很不靈活且不允許自動輸入密碼(必須顯示輸入，一些過時的自動輸入方案被認爲是安全bug在新系統中也被修復)
3. **命令一** 不能在開機時就執行，因爲它還必須等待 wsl 的執行一些系統初始化

所以要在 windows 中開機自動執行上述命令需要按照下述步驟
1. 首先使用桌面用戶去啓動任意的 wsl 命令，好讓 windows 去初始化 wsl ，主要是爲 "vEthernet (WSL)" 虛擬網卡設定一個隨機的 ip 網段
2. 等待步驟 1 完成後，使用有管理員權限的用戶去執行**命令一**，爲 "vEthernet (WSL)" 指定固定 ip
3. 使用桌面用戶去執行 **命令二**啓動要開機執行的 wsl 子系統，並且爲了讓 wsl 子系統不被 windows 關閉執行玩命令二後要保持進程在子系統中持續允許不要退出

因爲無法切換執行命令的用戶所以要完成上述步驟必須創建兩個進程
* **進程一**以管理員用戶開機執行，這可以使用 [WinSW](https://book.king011.com/view/zh-Hant/view/ms-os-tools/WinSW) 工具
* **進程二** 以桌面用戶開機執行，並且去執行啓動 wsl ，通知進程一修改windows 網卡，最終啓動並監視子系統運行

	要讓程式二一桌面用戶開機執行，可以修改註冊表 **HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run** 添加一個啓動項目

上述兩個進程的功能本喵使用 [deno](https://deno.land/) 腳本實現

進程一 會在 127.0.0.1:50000 上啓動一個 http 服務器等待進程二發出請求後，就修改 widnwos 網卡

```
type HTTPHandler = (req: Request) => Response | Promise<Response>;
/**
 * 實現一個簡單路由
 */
class Router {
    private readonly handler_ = new Array<{
        prefix: string;
        hander: HTTPHandler;
    }>();
    /**
     * 註冊路由
     */
    handle(
        prefix: string,
        handler: HTTPHandler,
    ) {
        this.handler_.push({
            prefix: prefix,
            hander: handler,
        });
        return this;
    }
    /**
     * 運行服務
     */
    async serve(l: Deno.Listener) {
        for await (const c of l) {
            this._serveHttp(c);
        }
    }
    private async _serveHttp(c: Deno.Conn) {
        for await (const evt of Deno.serveHttp(c)) {
            this._handler(evt);
        }
    }
    private async _handler(evt: Deno.RequestEvent) {
        const req = evt.request;
        const url = req.url;
        const host = new URL(req.url).host;
        const path = url.substring(url.indexOf(host) + host.length);

        for (const h of this.handler_) {
            if (path.startsWith(h.prefix)) {
                try {
                    evt.respondWith(await h.hander(req));
                } catch (e) {
                    evt.respondWith(
                        new Response(`${e}`, {
                            status: 500,
                            statusText: "Internal Server Error",
                        }),
                    );
                }
                return;
            }
        }
        evt.respondWith(
            new Response(null, {
                status: 404,
                statusText: "Not Found",
            }),
        );
    }
}
const l = Deno.listen({ port: 58964, hostname: '127.0.0.1' });
console.log("listen on", l.addr);
const s = new Router();
s.handle(
    "/vEthernet",
    async (req) => {
        const obj = await req.json()
        const ip = obj.ip
        const mask = obj.mask

        const pd = Deno.run({
            cmd: [
                "netsh",
                "interface",
                "ip",
                "delete",
                "address",
                "vEthernet (WSL)",
                ip,
            ]
        })
        try {
            await pd.status()
        } catch (_) {
            //
        } finally {
            pd.close()
        }
        const p = Deno.run({
            cmd: [
                "netsh",
                "interface",
                "ip",
                "add",
                "address",
                "vEthernet (WSL)",
                `${ip}/${mask}`,
            ],
            stdout: "piped",
            stderr: "piped",
        })
        try {
            const [status, stdout, stderr] = await Promise.all([
                p.status(),
                p.output(),
                p.stderrOutput()
            ]);
            return new Response(
                JSON.stringify({
                    code: status.code,
                    stdout: new TextDecoder().decode(stdout),
                    stderr: new TextDecoder().decode(stderr),
                }),
                {
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                },
            )
        } finally {
            p.close()
        }
    }
);
s.serve(l);
```

進程二首先通知 windows 初始化 wsl，然後使用 http 通知進程一修改修改 widnwos 網卡，之後啓動要執行的 wsl 並設定網卡，最後在 wsl 中執行一個 sleep 腳本，以保持子系統不被 windows 關閉

```
interface WSLOptions {
    /**
     * 宿主機 service 監聽端口
     */
    service: number
    /**
    * 宿主機 ip
    */
    master: string
    /**
    * 子網掩碼 位數
    */
    mask: number
    /**
     * 廣播地址
     */
    broadcast: string

    /**
     * 子系統在 wsl 中註冊的名稱
     */
    distro: string

    /**
     * 子系統 ip
     */
    ip: string

    /**
     * 指令出錯後等待多久重試
     */
    sleep: number
}
function sleep(ms: number): Promise<void> {
    if (ms <= 0) {
        return Promise.resolve()
    }
    return new Promise((resolve) => setTimeout(resolve, ms))
}
class Used {
    readonly startAt = Date.now()
    get used(): string {
        const used = Date.now() - this.startAt
        return `${used}ms`
    }
}
class WSL {
    constructor(private readonly opts: WSLOptions) { }
    async serve() {
        const opts = this.opts
        while (true) {
            const used = new Used()
            // 運行子系統並 設置 ip
            console.log(` * ${opts.distro} address: ${opts.ip}/${opts.mask}`)
            await this._mustRun(opts)
            console.log(` ! ${opts.distro} address: ${opts.ip}/${opts.mask}, used: ${used.used}` + "\n")

            // 設置 宿主機 ip
            console.log(` * ip ${opts.master}/${opts.mask}`)
            await this._mustIP(opts)
            console.log(` ! ip ${opts.master}/${opts.mask}, used: ${used.used}` + "\n")

            console.log(` - all success, used: ${used.used}` + "\n")

            // 保存子系統運行
            await this._serve(opts)

            // 子系統被停止，等待一段時間後重啓子系統
            console.log(`Subsystem exit, Restart the subsystem after ${opts.sleep / 1000} seconds`)
            await sleep(opts.sleep)
        }
    }
    private async _mustRun(opts: WSLOptions) {
        while (true) {
            try {
                if (await this._run(opts)) {
                    break
                }
            } catch (e) {
                console.log(e)
            }
            await sleep(opts.sleep)
        }
    }
    private async _run(opts: WSLOptions) {
        const p = Deno.run({
            cmd: [
                'wsl', '-d', opts.distro, '-u', 'root', 'bash', '-c',
                `set -e
if ! ifconfig | grep -wq 'eth0:1'; then
    ip addr add ${opts.ip}/${opts.mask} broadcast ${opts.broadcast} dev eth0 label eth0:1
fi`
            ]
        })
        const s = await p.status()
        return s.code == 0
    }
    private async _mustIP(opts: WSLOptions) {
        while (true) {
            try {
                if (await this._ip(opts)) {
                    break
                }
            } catch (e) {
                console.log(e)
            }
            await sleep(opts.sleep)
        }
    }
    private async _ip(opts: WSLOptions) {
        const resp = await fetch(`http://127.0.0.1:${opts.service}/vEthernet`, {
            method: "POST",
            body: JSON.stringify({
                ip: opts.master,
                mask: opts.mask,
            }),
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            }
        })
        if (resp.status != 200) {
            console.log(`${resp.status} ${resp.statusText}`)
            return false
        }
        const obj = await resp.json()
        if (obj.code != 0) {
            console.log(obj.code)
            if (obj.stdout != "") {
                console.log("stdout:")
                console.log(obj.stdout)
            }
            if (obj.stderr != "") {
                console.log("stderr:")
                console.log(obj.stderr)
            }
            return false
        }
        return true
    }
    private async _serve(opts: WSLOptions) {
        const p = Deno.run({
            cmd: [
                'wsl', '-d', opts.distro, '-u', 'root', 'bash', '-c',
                `while [ true ]
do
    echo $HOSTNAME
    cat /etc/issue
    uname -a
    echo " - $(date)"
    echo
    sleep 3600
done`
            ]
        })
        const s = await p.status()
        return s.code == 0
    }
}

new WSL({
    service: 58964,
    master: '192.168.50.1',
    mask: 24,
    broadcast: '192.168.50.255',
    distro: 'Ubuntu',
    ip: '192.168.50.10',
    sleep: 1000 * 30,
}).serve()
```

160行 new WSL 定義了子系統的各種信息

# [內存佔用](https://learn.microsoft.com/zh-cn/windows/wsl/wsl-config)

vmmem 會佔用大量內存(最多到宿主機的 80%)

1. Windows+R 輸入 %UserProfile% 打開檔案夾
2. 創建一個 **.wslconfig** 配置檔

	```
	[wsl2]
	memory=4GB
	swap=0
	localhostForwarding=false
	```