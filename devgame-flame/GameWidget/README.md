# [GameWidget](https://docs.flame-engine.org/1.16.0/flame/game_widget.html)

```
class GameWidget<T extends Game>
extends StatefulWidget
```

GameWidget 是一個 flutter widget，用於將 Game 嵌入到 flutter 樹。

GameWidget 提供了足夠豐富的功能，通常需要使用 GameWidget 作爲 Flame Game 的根節點運行

```
void main() {
  runApp(
    GameWidget(game: MyGame()),
  );
}
```

同時，GameWidget 是一個常規的 Flutter widget，可以任意深度地插入到 widget 樹中，包括在單一應用程式中擁有多個 GameWidget 的可能性。

該小部件的佈局行為是它將擴展以填充所有可用空間。 因此，當用作根小部件時，它將使應用程式全螢幕顯示。 在任何其他佈局小部件內，它將佔用盡可能多的空間。

除了託管 Game 實例之外，GameWidget 還提供一些結構支持，具有以下功能：

* **loadingBuilder** 遊戲加載時顯示的 widget，通常是顯示一個遊戲加載動畫
* **errorBuilder** 當遊戲拋出異常時顯示的視圖，開發時很有用，發佈的遊戲應該捕獲並處理所有異常
* **backgroundBuilder** 在遊戲後面繪製一些裝飾
* **overlayBuilderMap** 在遊戲頂部繪製 widget

應該注意的是，GameWidget 不會裁剪其畫布的內容，這意味著遊戲可能會在其邊界之外進行繪製（並非總是如此，具體取決於使用的​​相機）。 如果這不是你想要的，那麼可以考慮將 widget 包裝在 Flutter 的 ClipRect 中。

## [Constructors](https://docs.flame-engine.org/1.16.0/flame/game_widget.html#constructors)

```
GameWidget({
	required this.game, 
	this.textDirection, 
	this.loadingBuilder, this.errorBuilder, this.backgroundBuilder, this.overlayBuilderMap, 
	this.initialActiveOverlays, 
	this.focusNode, this.autofocus = true, 
	this.mouseCursor, this.addRepaintBoundary = true, 
	super.key})
```

> 上述構造函數渲染提供的遊戲實例

```
GameWidget.controlled({
	required this.gameFactory, 
	this.textDirection, 
	this.loadingBuilder, this.errorBuilder, this.backgroundBuilder, this.overlayBuilderMap, 
	this.initialActiveOverlays, 
	this.focusNode, this.autofocus = true, 
	this.mouseCursor, this.addRepaintBoundary = true, 
	super.key})
```

> 上述構造函數將使用提供的 gameFactory 創建並擁有一個 Game 實例

當您想要將 GameWidget 放入另一個小工具中，但又想避免自行儲存遊戲實例時，此建構函式會很有用。 例如：

```
class MyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: GameWidget.controlled(
        gameFactory: MyGame.new,
      ),
    );
  }
}
```
## [Properties](https://docs.flame-engine.org/1.16.0/flame/game_widget.html#properties)

* **game : T?** Game 的實例實現了遊戲的主邏輯
* **gameFactory : GameFactory&lt;T&gt;?** 創建 Game 的工廠函數，使用 **GameWidget.controlled** 構造時需要指定
* **textDirection : TextDirection?** 遊戲中文本方向
* **loadingBuilder : GameLoadingWidgetBuilder?** 提供一個在遊戲加載時顯示的 widget，默認是 null
* **errorBuilder : GameErrorWidgetBuilder?** 提供一個顯示遊戲在加載時捕獲的錯誤的 widget
* **backgroundBuilder : WidgetBuilder?** 用於繪製 Game.backgroundColor 之外的遊戲背景
* **overlayBuilderMap : Map&lt;String,&nbsp;OverlayWidgetBuilder&lt;T&gt;&gt;?** 可以在遊戲表面顯示的小部件，可以使用 Game.overlays 動態創建

		```
		void main() {
			runApp(
				GameWidget(
					game: MyGame(),
					overlayBuilderMap: {
						'PauseMenu': (context, game) {
							return Container(
								color: const Color(0xFF000000),
								child: Text('A pause menu'),
							);
						},
					},
				),
			);
		}
		```
		
* **initialActiveOverlays&nbsp;:&nbsp;List&lt;String&gt;?**  遊戲開始時（但加載後）將顯示的覆蓋列表
* **focusNode : FocusNode?** 控制遊戲焦點以接收事件輸入。 如果省略，則默認為內部控制的焦點節點
* **autofocus : bool** 一旦遊戲安裝完畢，focusNode 是否請求焦點。 默認為 true
* **mouseCursor : MouseCursor?** 鼠標光標懸停在遊戲畫布上時的形狀。 該屬性可以通過 Game.mouse Cursor 動態更改
* **addRepaintBoundary : bool** 遊戲是否應採用 RepaintBoundary 的行為，默認為 true