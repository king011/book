# tlhelp32

[tlhelp32](https://docs.microsoft.com/zh-tw/windows/desktop/api/tlhelp32/nf-tlhelp32-createtoolhelp32snapshot) 提供了 系統 進程 模塊的 快照 供 用戶 查詢

| 條目 | 值 | 
| -------- | -------- | 
| Minimum supported client     | Windows XP \[desktop apps only\]     | 
| Minimum supported server     | Windows Server 2003 \[desktop apps only\]     | 
| Target Platform     | Windows     | 
| Header     | tlhelp32.h     | 
| Library     | Kernel32.lib     | 
| DLL     | Kernel32.dll     | 

# CreateToolhelp32Snapshot 創建快照

CreateToolhelp32Snapshot 用於 創建一個 快照  在不需要時 需要使用 CloseHandle 釋放快照句柄

```
HANDLE CreateToolhelp32Snapshot(
  DWORD dwFlags,
  DWORD th32ProcessID
);
```

dwFlags 指定了要 取得 的快照類型 可以將下列值 用 **|** 組合

th32ProcessID 指定了 要獲取哪個進程的 快照 如果爲 0 獲取整個系統的 快照 信息

```
#include <iostream>
#include <iomanip>
#include <windows.h>
#include <tlhelp32.h>

void display(HANDLE handle);
void display_module(DWORD pid);
std::string to_utf8(const std::wstring &src)
{
    int total = WideCharToMultiByte(CP_UTF8, 0, src.data(), src.size() * 2, 0, 0, 0, 0);
    char *str = new char[total];
    WideCharToMultiByte(CP_ACP, 0, src.data(), src.size() * 2, str, total, 0, 0);
    std::string result(str);
    delete[] str;
    return result;
}

int main()
{
    HANDLE handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (INVALID_HANDLE_VALUE == handle)
    {
        puts("CreateToolhelp32Snapshot for process err");
        return 1;
    }
    display(handle);
    CloseHandle(handle);
    return 0;
}
void display(HANDLE handle)
{
    PROCESSENTRY32 info;
    info.dwSize = sizeof(PROCESSENTRY32);
    WINBOOL ok = Process32First(handle, &info);
    while (ok)
    {
        if (!wcscmp(L"console.exe", info.szExeFile))
        {
            std::cout << "pid = " << info.th32ProcessID << "\n";
            std::cout << "parent pid = " << info.th32ParentProcessID << "\n";
            std::cout << "線程數 = " << info.cntThreads << "\n";
            std::cout << "線程優先級 = " << info.pcPriClassBase << "\n";
            std::cout << "進程名 = " << to_utf8(info.szExeFile) << "\n\n";
            // PROCESSENTRY32 其它屬性 都已經 被棄用

            display_module(info.th32ProcessID);
        }
        ok = Process32Next(handle, &info);
    }
}
void display_module(DWORD pid)
{
    HANDLE handle = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
    if (INVALID_HANDLE_VALUE == handle)
    {
        throw("CreateToolhelp32Snapshot for process err");
    }
    MODULEENTRY32 info;
    info.dwSize = sizeof(MODULEENTRY32);
    WINBOOL ok = Module32First(handle, &info);
    puts("*** modules ***");
    while (ok)
    {
        std::cout << "pid = " << info.th32ProcessID << "\n";
        std::cout << "模塊基址 = " << HANDLE(info.modBaseAddr) << "\n";
        std::cout << "模塊大小 = " << info.modBaseSize << "\n";
        std::cout << "模塊句柄 = " << info.hModule << "\n";
        std::cout << "模塊名 = " << to_utf8(info.szModule) << "\n";
        std::cout << "模塊全路徑 = " << to_utf8(info.szExePath) << "\n\n"; // 進程第一個 模塊全路徑 就是 執行檔的全路徑

        ok = Module32Next(handle, &info);
    }
    CloseHandle(handle);
}
```