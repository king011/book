# 場景

場景包含了遊戲環境 角色 和 UI 元素，可以將每個場景看作一個獨立的關卡，在遊戲設計時可以將遊戲劃分爲多個場景來分別實現

場景用來容納遊戲物體以構成遊戲元素，遊戲物體是組件的容器，組件用於完成具體功能 每個遊戲物體可以包含多個組件

# 組件

一個遊戲物體可以包含多個組件 選中物體後可以在 檢視窗口 修改組件屬性 或 添加刪除組件

![](assets/GameObject.png)

如上圖所示一個最簡單的 空白物體(GameObject)包含一個 變換組件(Transform) 可以在檢視窗口 修改其 標籤(Tag) 層級(Layer) 以及變換組件的屬性

## 變換組件

所有遊戲物體都必須包含且只有一個變換組件，變換組件確定了物體在遊戲世界中的 位置 旋轉角度 縮放比例

| 屬性 | 功能 |
| -------- | -------- |
| Position     | 位置     |
| Rotation     | 旋轉     |
| Scale     | 縮放     |

在 3D 空間中 變換組件具有 X軸 Y軸 Z軸，而 2D 空間中只有 X軸 Y軸

默認情況下 unity 始終使用 紅色(X) 綠色(Y) 藍色(Z) 表示軸

### 非等比縮放

非等比縮放在某下情況下會有用，但可能會出現bug。

例如 碰撞體 角色控制器 等組件，具有一個球體或膠囊體外殼，這些外殼的大小是通過一個半徑參數指定的，燈光 音源 也有類情況。在物體或父物體被拉伸或壓扁時，這些組件的球體範圍並不會跟着被壓扁，它們實際上仍然是球體或膠囊體。這可能會導致穿透模型被意外阻擋等情況發生。

此外父物體的變換會影響到子物體，當父物體沿着 X軸 和 Z軸 的縮放不一致時，子物體同樣會被拉伸。這時如果旋轉父物體，那麼子物體的縮放與旋轉將會難以計算。由於性能原因 此時 unity 不會立刻更新子物體信息，將會導致子物體信息沒有即時更新，而稍後又突然變化。

### 縮放和物體大小

變換組件的縮放參數決定了場景中模型的大小相對原始模型的大小的倍數。

默認情況下引擎規定所有場景中的單位都對應國際標準單位，比如空間中的 1個單位 代表 1米

1. 理論上爲了減少潛在問題，不應該通過變換組件的縮放來調整模型的大小，最佳方案是在製作模型 導出模型時，就旋轉符合實際比例的大小。
2. 此一等方案是在導入模型到 unity 時，可以在打入設置中改變模型的比例。在改變模型比例時，引擎會根據模型大小做特定優化。且在創建一個改變比例的模型到場景中時，由於引擎會自動調整模型，所以會帶來性能影響。


## 測試組件參數

你可以在運行模式後繼續修改組件的參數，以測試等待一個合適的參數值，不過當退出播放模式後組件值會被改爲原值(這是擔心在運行模式中你會改亂了參數)，故在運行模式調整到合適參數後需要記錄下來再停止播放後重新設定找到的值

# 腳本組件

unity 提供了將腳本作爲組件的功能，每一個新的腳本都定義了一種新的組件，腳本中的公共成員會顯示在檢視窗口中，就像內置組件一樣可以方便的修改

在 unity 2017 後的版本中，官方推薦的腳本語言爲 c#

要創建一個腳本組件 直接創建一個 c# 檔案 即可(檔案名需要和腳本中的類名一致 包括大小寫) 在工程窗口雙擊檔案即可打開編輯(在 unity 菜單 Edit -> Preferences -> External Tools 中可修改默認的腳本編輯器)

```c#
#info="Cat.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
```

unity 規定 腳本必須包含一個派生自 MonoBehaviour 的並與檔案同名的類，只有符合規範的腳本才會被視爲組件可以掛接給遊戲物體

默認創建的代碼會層級一個 Start 與 Update 函數

* Start 在物體剛剛被創建時被調用一次(在第一次 Update 之前)，通常用來執行初始化工作
* Update 在每一幀都會被調用，用來處理 用戶輸入 角色行動 等功能，基本上大部分遊戲邏輯都離不開 Update

> 通常不要使用構造函數做初始化，應該使用 Start 回調因爲構造函數的調用時機不可控

## 腳本控制遊戲物體

將腳本掛接給物體即可用於控制遊戲物體

unity 提供了 Debug.Log 函數可用於將調試信息輸出到 unity 控制檯

另外腳本的公共變量可以在檢視窗口中直接修改

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    public new string name = "cat";
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"I'm coming, my name is {name}");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
```

## 訪問組件

unity 提供了 GetComponent 函數用於訪問當前物體的組件，如果組件存在會返回組件的實例便可訪問組件提供的 方法 屬性

```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    public new string name = "cat";
    private Rigidbody rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        // 改變質量爲 10 kg
        rigidbody.mass = 10f;

        // 施加一個向左的力，大小爲 1000 牛頓
        rigidbody.AddForce(Vector3.left * 1000f);
    }

    // Update is called once per frame
    void Update()
    {
        // 自定義組件也可以獲取
        var cat = GetComponent<Cat>();
        Debug.Log(cat.name);
    }
}
```

## 訪問其它遊戲物體

通常遊戲物體之間需要互動 比如 敵人ai通常需要獲取玩家 來計算ai邏輯 unity 提供了多種方法可以獲取到其它物體

1. 最簡單的方法是 在組件腳本本創建一個 GameObject 屬性然後再 unity 可視化環境中將 要獲取的物體拖拽到 此屬性即可

	```c#
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Cat : MonoBehaviour
	{
			public new string name = "cat";
			public GameObject target;
			// Start is called before the first frame update
			void Start()
			{
			}
			// Update is called once per frame
			void Update()
			{
					if (target == null)
					{
							return;
					}
					var cat = target.GetComponent<Cat>();
					Debug.Log(cat?.name ?? "no target");
			}
	}
	```
	
	也可以將 型別由 GameObject 改變爲 組件 但此時需要被拖拽的遊戲物體具有此型別的組件，此外屬性也可以是一個 數組
	
	設置屬性是最簡單遊戲的方案 但如果被查找物體是動態創建的 或者被隨着遊戲進行而變換則需要通過 unity 提供的 查找函數來查找物體
	
1. 使用變換組件(所有遊戲物體都綁定唯一一個變換組件) 來獲取子組件 進而獲取 子物體(父子關係記錄在變換組件 而非 GameObject 中)

	```
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Parent : MonoBehaviour
	{
			// Start is called before the first frame update
			void Start()
			{
					for (int i = 0; i < transform.childCount; i++)
					{
							var child = transform.GetChild(i);
							var name = child.gameObject.name;
							Debug.Log($"{i + 1} {name}");
					}
			}
			// Update is called once per frame
			void Update()
			{
			}
	}
	```
	
	此外 Transform 提供了 Find("name") FindChild("name") 來查找子物體，不過效率並不好請儘量只在 Start 中使用
	
3. 如果要查找的物體不存在父子關係 可以通過 GameObject 提供的靜態方法 使用 名稱或標籤查找

	* GameObject.Find("name")
	* GameObject.FindWithTag("tag")
	* GameObject.FindGameObjectWithTag("tag")
	* GameObject.FindGameObjectsWithTag("tag")

## 事件函數

unity 中的腳本組織不想傳統的遊戲邏輯由一個主循環持續處理(主循環已經由框架實現了)。相對的 unity 會在特定的事件發生時，調用腳本中特定的函數，然後執行邏輯的任務就交給了腳本函數，這些特定的函數被稱爲事件函數。

### 更新事件

遊戲類似動畫是按幀運行的，只是對遊戲而言未來的幀還沒畫出來需要在遊戲運行時進行計算。遊戲中的一個基本概念是：在每一幀剛開始(渲染之前)，對物體的 位置 狀態 進行計算，然後渲染出來。 **Update** 函數就是來完成此功能的，它會在每一幀剛開始時被調用

```c#
void Update()
{
		float distance = speed * Time.deltaTime * Input.GetAxis("Horizontal");
		transform.Translate(Vector3.right * distance);
}
```

物理引擎也會按照物理幀更新，機制類似 Update 但調用時機不同。在每次物理更新時會調用 **FixedUpdate**，儘可能在 FixedUpdate 中執行物理相關的操作，在 Update 中執行其它操作

```c#
void FixedUpdate()
{
		Vector3 force = transform.forward * driveForce * Input.GetAxis("Vertical");
		Rigidbody.AddForce(force);
}
```

有時還需要在所有的 Update 函數執行完畢後再進行一些操作。例如攝像機需要跟隨物體的位置，那麼就需要在物體移動之後再更新攝像機的位置；還有當物體同時受到腳本和動畫的影響時，我們需要在動畫執行完畢後，再獲得物體的位置，這是就要用到 **LateUpdate** 函數

```
void LateUpdate()
{
		Camera.main.transform.LookAt(target.transform);
}
```

#### 時間和幀率

Update 用來執行遊戲邏輯例如角色勻速移動，但每幀的間隔時間時不確定的，此時只讀的 靜態屬性 **Time.deltaTime** 記錄了量幀間隔時間 

```
void Update()
{
		transform.Translate(0, 0, distancePerSecond * Time.deltaTime);
}
```

#### 物理更新間隔

與 Update 不同，物理系統必須以固定的時間間隔工作，這樣才能保證物理模擬的準確性。只讀的靜態屬性 **Time.fixedDeltaTime** 記錄了系統使用的間隔時間

默認值爲 **0.02**，可以在工程選項中的 TimeManager 中修改此值。較小的更新間隔會帶來更高的頻率 更準確 的運算結果 當然這會極大的增加硬件負擔

### 初始化事件

* Awake 會在腳本實例化時被調用一次，通常在裏面初始化一些需要在 Start 中就可以使用 數據
* Start 會在第一幀被渲染之前調用一次，通常用於執行物體的初始化

Start 調用時 所有腳本的 Awake 都已經被調用完成了

#### 創建和銷毀物體

大部分遊戲都會在運行中創建和銷毀物體，Unity 提供了 **Instantiate** 函數來創建一個新物體，需要提供一個 預製 或者 已經存在的物體 作爲模板

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parent : MonoBehaviour
{
    public GameObject enemy;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 2; i++)
        {
            Instantiate(enemy);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
```

**Destroy** 函數用來銷毀 物體 或 組件，下述代碼在調用 Destroy 後延遲 5秒才執行銷毀動作

```c#
void Start()
{
		for (int i = 0; i < 2; i++)
		{
				Destroy(Instantiate(enemy), 5f);
		}
}
```


## 物體與組件無效化

* 在檢視查窗口中 物體 和 大部分組件 都有一個小勾 取消這個勾 可以使 物體或組件無效化 (GameObject.active 屬性 記錄了這個勾是否選中)
* 物體無效化後其組件和子物體都會無效化
* 不能通過 active 判斷物體是否有效 需要使用 activeInHierarchy 屬性 因爲 active 沒有考慮父物體狀態 activeInHierarchy 則考慮了

## 腳本組件的生命週期

腳本不一定要 派生自 MonoBehaviour，但只有派生自 MonoBehaviour 的 class 才是組件，MonoBehaviour 事件的觸發具有明確的先後順序

![](assets/0.png)

## 常用事件

| 事件 | 說明 |
| -------- | -------- |
| Update     | 每幀被調用，按幀執行的邏輯都放在這裏     |
| LateUpdate     | 每幀遊戲邏輯的最後，渲染之前被調用     |
| FixedUpdate     | 固定更新，專門用於物理系統，因爲物理更新的頻率必須保證穩定性     |
| Awake     | 當腳本實例被創建時調用一次     |
| Start     | 在 Update 函數第一次被調用前調用一次     |
| Reset     | 重置爲默認值時調用(在 unity 可視化開發環境中才有這種情況)     |
| OnMouseEnter     | 當鼠標光標 進入 GUI 元素 或 碰撞體 中時調用     |
| OnMouseOver     | 當鼠標光標 懸浮在 GUI 元素 或 碰撞體 上時調用     |
| OnMouseExit     | 當鼠標光標 移除 GUI 元素 或 碰撞體 上時調用     |
| OnMouseDown     | 當鼠標光標在 GUI 元素 或 碰撞體 上 單擊時 調用     |
| OnMouseUp     | 當用戶釋放鼠標按鈕是調用     |
| OnMouseUpAsButton     | 只有當鼠標光標在同一個 GUI 元素 或 碰撞體 時按下，再釋放時調用     |
| OnMouseDrag     | 當用戶用鼠標拖拽 GUI 元素 或 碰撞體 時調用     |
| OnTriggerEnter     | 當碰撞體進入觸發器時調用     |
| OnTriggerExit     | 當碰撞體離開觸發器時調用     |
| OnTriggerStay     | 當碰撞體接觸觸發器時，在每一幀被調用     |
| OnCollisionEnter     | 當此 Collider/Rigidbody 觸發另外一個 Rigidbody/Collider 時被調用       |
| OnCollisionExit     | 當此 Collider/Rigidbody 停止觸發另外一個 Rigidbody/Collider 時被調用     |
| OnCollisionStay     | 當此 Collider/Rigidbody 觸發另外一個 Rigidbody/Collider 時，會在每一幀被調用     |
| OnControllerColliderHit     | 在移動時，當 Controller 碰撞到 Collider時被調用      |
| OnJointBreak     | 當附在同一對象上的關節被斷開時被調用     |
| OnParticleCollision     | 當粒子碰到 Collider 時被調用     |
| OnBecameVisible     | 當 Renderer 在任何攝像機上可見時調用     |
| OnBecameInvisible     | 當 Renderer 在任何攝像機上都不可見時調用     |
| OnEnable     | 當對象變爲可用或激活狀態時被調用     |
| OnDisable     | 當對象變爲不可用或非激活狀態時被調用     |
| OnDestroy     | 當 MonoBehaviour 被將銷毀時被調用     |
| OnPreCull     | 在攝像機消影場景之前被調用     |
| OnPreRender     | 在攝像機渲染場景之前被調用     |
| OnPostRender     | 在攝像機完成渲染之後被調用     |
| OnRenderObject     | 在攝像機場景渲染完成後被調用 和 OnPostRender 的區別在於 OnRenderObject 會在所有掛接此腳本的對象渲染完成時調用 無論對象是否在攝像機上     |
| OnWillRenderObject     | 如果對象可見，每個攝像機都會調用它     |
| OnGUI     | 渲染和處理 GUI 事件時調用，每幀調用一次     |
| OnRenderImage     | 當完成所有渲染圖片後被調用，用來渲染圖片後期效果     |
| OnDrawGizmosSelected     | 如果像物體在可視化界面被選中時繪製輔助線框，提供此函數     |
| OnDrawGizmos     | 如果像物體在可視化界面繪製可被點選的輔助線框，提供此函數     |
| OnApplicationPause     | 當玩家暫停/取消暫停時發送給所有遊戲物體     |
| OnApplicationFocus     | 當玩家獲得或失去焦點時發送給所有遊戲物體     |
| OnApplicationQuit     | 在應用退出前發送給所有遊戲物體     |
| OnPlayerConnected     | 當一個新玩家成功連接時在服務器上調用     |
| OnPlayerDisconnected     | 當一個玩家從服務器上斷開時，在服務器端調用     |
| OnServerInitialized     | 當 Network.InitializeServer 被調用並完成時，在服務器上調用     |
| OnConnectedToServer     | 當成功連接到服務器時在客戶端調用     |
| OnDisconnectedFromServer     | 當失去連接或從服務器端斷開時在客戶端調用     |
| OnFailedToConnect     | 當一個連接因爲某些原因失敗時在客戶端調用     |
| OnFailedToConnectToMasterServer     | 當連接主服務器失敗時在客戶端或服務器端調用     |
| OnMasterServerEvent     | 當報告事件來自主服務器時在客戶端或服務器端調用     |
| OnNetworkInstantiate     | 當一個物體使用 Network.Instantiate 進行網路初始化時調用     |
| OnSerializeNetworkView     | 當一個網路視圖腳本中，用於同步自定義變量     |

# 標籤

標籤(Tag) 時一個遊戲物體上的記號，通常時一個簡單的單詞

可以使用 GameObject.FindWithTag 來查找標籤物體。物體的默認標籤時 Untagged( 未指定標籤)

標籤只能創建和刪除無法修改，unity 提供了幾個默認的標籤

1. Untagged 沒有標籤
2. Respawn 出生
3. Finish 完成
4. EditorOnly 編輯器專用
5. MainCamera 主攝像機
6. Player 玩家
7. GameController 遊戲控制器

# 靜態物體

如果引擎事先知道某一物體在遊戲是靜態的，那麼引擎就可以假定它不會受到其它物體或者事件的影響，從而預先計算好物體的信息從而優化性能。

在檢視窗口中有個 Static 選項勾選後可以用來指定物體的靜態信息，並且 Static 本身是個多選下拉菜單組可以分別設置物體在不同子系統下的靜態性

* Nothing 禁用靜態特性
* Everything 在所有子系統中啓用靜態性
* Contribute GI 如果啓用此屬性，在全局光照計算中包含目標網格渲染器
* Occluder Static 在遮擋系統中，將物體標記爲靜態遮擋物
* Occludee Static 在遮擋系統中，將物體標記爲靜態被擋物
* Batching Static 將多個物體合併爲一個整體進行渲染
* Navigation Static 在尋路系統中將此物體作爲靜態的障礙物
* Off Mesh Link Generation 尋路系統中的網格鏈接
* Refection Probe Static 反射探針優化

# 圖層

圖層(Layer) 最有用 最常用的地方是用來讓攝像機僅渲染場景中的一部分物體 還可以讓燈光只照亮一部分物體。此外還能用來在進行 碰撞檢測 射線檢測 時，只讓某些物體發生碰撞，讓另外一些物體不發生碰撞

unity 支持最多32層，請勿使用第32層因爲 unity 默認使用最後一層進行渲染如果使用了可能會發生衝突

## 僅渲染部分圖層

在攝像機組件的 **Culling Mash**(剔除遮照) 下拉菜單中 可以選擇攝像機要渲染的圖層

## 選擇性的射線檢測

使用 圖層 和 遮罩 可以讓射線只檢測某些碰撞體，Physics.Raycast 可以發射一條射線，其中有個可選參數 **int layerMask** 用於指定射線要和哪些層級檢測碰撞，int 有 32 個字節 每個字節分別代表了某一層 爲 1 則會檢測碰撞，默認 射線會和除了 **Ignore Raycast** 外的所有層檢測

```c#
// 設置第 8 bit 爲1 所以只和 第8層檢測
int layerMask = 1 << 8;
if (Physics.Raycast(transform.position, Vector3.forward, Mathf.Infinity, layerMask))
{
		Debug.Log("hit");
}
```

```c#
void Update()
{
		// 設置第 8 bit 爲1 
		int layerMask = 1 << 8;
		// 取反所以和 第8層外的所有層檢測
		layerMask = ~layerMask;

		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),
				out hit,
				Mathf.Infinity, layerMask))
		{
				// DrawRay 繪製輔助線框
				Debug.DrawRay(transform.position,
						transform.TransformDirection(Vector3.forward) * hit.distance,
						Color.yellow);
				Debug.Log("hit");
		}
		else
		{
				// DrawRay 繪製輔助線框
				Debug.DrawRay(transform.position,
						transform.TransformDirection(Vector3.forward) * 1000,
						Color.white);
				Debug.Log("not hit");
		}
}
```

# 預置體

預置體是預先配置好的遊戲物體，它已經包含了一些組件和組件屬性，unity 可以方便的爲預置體創建新的實例物體，這些物體和預置體有一樣的初始組件與屬性，並且可單獨的修改這些組件或屬性，unity 允許將這些修改回設到預置體上，也可以通過修改預置體來修改所有由預置體創建的實例。

將遊戲物體拖拽到工程窗口既可將一個遊戲物體創建爲預置體

所有與預置體關聯的遊戲物體會在檢視窗口上出現三個按鈕

* Select 選中與物體關聯的預置體在工程窗口中會高亮顯示
* Revert 將物體上的修改設置回預置體(但變換組件的位置信息不會被寫回)
* Apply 將物體回滾到和預置體一樣

## 創建一堵牆

下面例子將使用預製體和腳本創建一堵牆

1. 創建一個 Cube 並保存了預製體 並掛接上 剛體組件

2. 創建一個空物體並掛載如下腳本

	```cs
	#info="Wall.cs"
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Wall : MonoBehaviour
	{
			public Transform brick;
			// Start is called before the first frame update
			void Start()
			{
					for (int x = 0; x < 5; x++)
					{
							for (int y = 0; y < 5; y++)
							{
									var current = Instantiate(brick, new Vector3(x, y, 0), Quaternion.identity);
									current.parent = transform;
							}
					}
			}
			// Update is called once per frame
			void Update()
			{
			}
	}
	```

將預製體拖動到 brick 屬性即可

# 輸入

輸入是遊戲的互動的基礎 unity 支持 鍵盤 手柄 搖桿等傳統設備，並且unity 爲這些傳統設備設計了一個虛擬控制軸(Virtual axes)。

## 虛擬控制軸

虛擬控制軸將不同的輸入設備都歸納到一個同一的虛擬控制系統中，例如
* 將鍵盤的 W S 和 手柄/搖桿 的 上下運動 默認都統一映射到 豎直(Vertical) 輸入上
* 將鼠標左鍵 和 鍵盤的Ctrl 都默認映射到 fire1 這個虛擬按軸上 

使用 輸入管理器(Input Manager) 可以 查看 修改 刪除 虛擬軸

1. 腳本可以直接通過虛擬軸的名稱讀取那個軸的輸入狀態

2. 創建 unity 工程時，默認創建了以下虛擬軸

   * 橫向輸入(Horizontal) 縱向輸入(Vertical) 被映射在鍵盤的 W A S D 以及方向鍵上
   * Fire1 Fire2 Fire3 映射到了鼠標 左 中 右 鍵 以及鍵盤的 Ctrl Alt Shift
   * 鼠標移動可以模擬搖桿輸入(和鼠標在屏幕上的位置無關) 且被映射在專門的鼠標偏移軸上
   * 其它常用的還有 跳躍(Jump) 確認(Submit) 取消(Cancel) ...

才 Edit -> Project Settings -> Input Managet 中可以編輯虛擬軸

![](assets/input.png)

* Size 是虛擬軸的 數量 要增加虛擬軸就增大 Size 值即可

* 虛擬軸具有 Positive(正) Negative(負) 兩個方向。某些相反的動作可以只用一個軸表示，例如 如果搖桿向上爲正，那麼向下就是同一個軸的負方向

* 每個虛擬軸有如下屬性

	| 屬性 | 功能 |
	| -------- | -------- |
	| Name     | 軸名字，腳本中使用此名字訪問這個軸     |
	| Descriptive Name     | 描述信息 在某些窗口中顯示出來以方便查看(正方向)     |
	| Descriptive Negative Name     | 描述信息 在某些窗口中顯示出來以方便查看(負方向)     |
	| Negative Button     | 該軸負方向，用於綁定某個按鍵     |
	| Positive Button     | 該軸正方向，用於綁定某個按鍵     |
	| Alt Negative Button     | 該軸負方向，用於綁定某個備用按鍵     |
	| Alt Positive Button     | 該軸正方向，用於綁定某個備用按鍵     |
	| Gravity     | 軸回中的力度     |
	| Dead     | 軸的死去     |
	| Sensitivity     | 敏感度     |
	| Snap     | 保持式按鍵。比如按下方向鍵，則一直保持下的狀態，直到再次按上方向鍵     |
	| Invert     | 如果勾選，則交互正負方向     |
	| Type     | 控制該虛擬軸的類型，比如 手柄 鍵盤 是不同的類型     |
	| Axis     | 很多手柄的輸入不是按鈕式的，這時就不能配置到 Button 裏面 而是要配置到這裏     |
	| Joy Num     | 當有多個控制器時，要填寫控制器的編號     |

早期遊戲輸入都是離線的 可以直接用 1 0 -1 表示，現代遊戲則存在中間態 比如 0 0.35 0.5 0.7 1這種帶多級梯度的，比如輕推搖桿表示走路，推到底就是跑步

雖然鍵盤沒有多級功能，但 unity 依然會模擬這個功能(將值很快的從 0 逐漸增加到 1)，敏感度(Sensitivity) 確定了增加速度

此外 unity 還設計了死區，一些手柄如果放着不動，輸出值可能會在 -0.05 -0.08 之間浮點，死區可以告訴 unity 忽略這個誤差，在該範圍內的抖動都忽略爲 0，這樣可以過濾掉輸入設備的誤差

### 在腳本中處理輸入

```
float value = Input.GetAxis("Horizontal");
```

value 的值範圍爲 \[-1,1\]，默認位置爲 0， 這個讀取虛擬軸的方法與具體控制設備是 鍵盤還是 手柄無關。

> 如果用鼠標控制虛擬軸，就可能由於移動過快導致值 超出 -1~1 的範圍

可以創建多個相同名字的虛擬軸，獲取到的值會是變化最大的結果，這是爲了遊戲可以同時用多種設備操作。例如 pc 遊戲可以用 鼠標 和鍵盤 操作，手機遊戲可以用 重力感應 手柄 操作

## 移動設備輸入

移動設備 通常使用手指觸摸操作功能，通常可以處理最多5根手指同時觸摸屏幕的情況

**Input.touches** 屬性記錄的 手指觸摸狀態數組(Touch\[\])，Touch 包含如下屬性

| 屬性 | 功能 |
| -------- | -------- |
| fingerId     | 觸摸的序號     |
| position     | 觸摸在屏幕上的位置     |
| deltaPosition     | 當前觸摸和前一個觸摸位置的差距     |
| deltaTime     | 最近兩次改變觸摸位置之間的時間間隔     |
| tapCount     | iPhone/iPad 設備會記錄用戶短時間內單擊屏幕次數，它表示用戶多次單擊操作且沒有將手拿開的次數。android 沒有這個功能，該值始終爲 1     |
| phase     | 觸摸階段     |

phase 可以是如下取值

* Begin 手指剛接觸到屏幕
* Moved 手指在屏幕上滑動
* Stationary 手指接觸到屏幕但還未滑動
* Ended 手指離開了屏幕。這個狀態代表着一次觸摸操作的結束
* Canceled 系統取消了這次觸屏操作。例如用戶拿起手機進行通話，或者觸摸點超過5個，這時操作會被取消。這個狀態也代表這次觸摸操作結束

下面例子 在手指開始觸摸時 創建一條射線 如果射線射到物體則在此處創建一個 預製體 

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Touch : MonoBehaviour
{
    public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        foreach (var touch in Input.touches)
        {
            // 如果某個手指剛開始觸摸
            if (touch.phase == TouchPhase.Began)
            {
                // 利用 攝像機 和 屏幕上的點，可以確定出一條從手指到場景內的射線
                var ray = Camera.main.ScreenPointToRay(touch.position);
                // 利用物理引擎發射這條射線
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Instantiate(prefab, hit.point, Quaternion.identity);
                }
            }
        }
    }
}
```

上面代碼無法在pc上工作因爲沒有手指 不過可以使用 鼠標來模擬，使用 Input.mousePosition 獲取鼠標光標位置

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Touch : MonoBehaviour
{
    public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // 利用 攝像機 和 屏幕上的點，可以確定出一條從手指到場景內的射線
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // 利用物理引擎發射這條射線
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Instantiate(prefab, hit.point, Quaternion.identity);
            }
        }
    }
}
```

### 加速度

當移動設備移動時， **Input.acceleration** 記錄了設備向各方向移動的加速度，着同樣是一個 Vector3 某個方向值 1.0 代表具有 這個方向 1.0g 的加速度，負值代表相反方向

在正常豎直手持手機時， X軸的正反向朝右，Y軸的正方向朝上，Z軸的正方向從手機指向用戶

下述例子安加速度 控制物體移動

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelerationControl : MonoBehaviour
{
    float speed = 10.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = Vector3.zero;
        dir.x = -Input.acceleration.y;
        dir.z = -Input.acceleration.x;

        // 將 dir 向量的範圍限制在單位球體內
        if (dir.sqrMagnitude > 1)
        {
            dir.Normalize();
        }
        // 計算幀
        dir *= Time.deltaTime;
        // 移動物體
        transform.Translate(dir * speed);
    }
}
```

## VR 輸入

unity 支持多種 VR 設備的專用輸入設備。不同 VR 設備具有不同的開發插件

# 方向與旋轉

在 3D 空間通常有兩種方式表示旋轉

* 歐拉角
* 四元數

着兩種方式各有利弊，unity 在引擎內部使用四元數表示旋轉，但在檢視窗口以歐拉角來表示物體旋轉


## 四元數

四元數可以用來表示物體的旋轉和朝向。通常包含四個數字 x y z w，但是這四個數字不代表直觀上的旋轉角度而是有獨特的數學定義，故不應該直接單獨修改着四個值。四元數的原點記作 **Identity**，此外有如下特定 

* 不存在萬向節鎖
* 一個四元數無法表示超過180度的旋轉
* 四元數的數值無法直觀理解

在腳本中使用四元數時應當使用 Quaternion 類以及其提供的方法來創建修改四元數

1. 創建四元數
	* Quaternion.LookRotation
	* Quaternion.AngleAxis
	* Quaternion.FromToRotation
1. 其它方法
* Quaternion.Slerp
* Quaternion.Inverse
* Quaternion.RotateTowards
* Transform.Rotate

```
#info="物體繞Y軸旋轉30度"
transform.Rotate(0,30,0);
```
```c#
float x;
// Update is called once per frame
void Update()
{
		x += Time.deltaTime * 10;
		// 避免直接修改 四元數的值 直接通過連續指定最終角度來讓物體旋轉
		transform.rotation = Quaternion.Euler(x, 0, 0);
}
```

# 燈光

燈光奠定了場景的基調。在遊戲中 模型 和 貼圖 定義了場景的骨架和外表，燈光則定義了場景的色調和情感。

在 層級窗口中 添加 Light -> Directional Light 即可添加一個方向光源，燈光有很多種，方向光源最適合作爲室內場景的整體光源。燈光同樣是一個組件也可以直接在遊戲物體中添加光照組件其位於 Rendering 分類中

![](assets/light.png)

unity 支持不同的渲染路徑。不同的渲染路徑影響的主要是光照和陰影，旋轉哪種渲染路徑主要取決與所要做的遊戲本身需求

## 點光源

點光源(Point Light) 是指空間中的一個點向每個方向都發射同樣強度的光。光線強度會隨着距離增加而減弱，到某個距離就會減小爲0。與真實世界的光照規律相符合

點光源非常適合用來模擬場景中的燈泡或蠟燭等光源，而且還能用於模擬槍械發射時照亮的效果。一般開槍時槍口的閃光是用粒子效果實現的，但槍口的火焰會瞬間照亮周圍的環境，這時可以用一個短時間出現的點光源來模擬這個效果，以使開槍效果更爲逼真

![](assets/Point.png)

## 探照燈

探照燈(Spot Light) 類似 點光源，但其發射角度被限制在一個固定的角度內，最終形成了一個錐形的照射區域

探照燈發射的光線會在錐形邊緣處截止。擴大發射角度可以增大錐形的範圍，但會影響光線的匯聚效果，這和現實中的探照燈或手電筒的光線特徵是一致的

探照燈通常用來表示一些特定的人造光源，例如 手電筒 汽車大燈 直升機上的探照燈 等

![](assets/Spot.png)

## 方向光源

方向光源(Directional Light) 在默認創建中就有一個。絕大多數場景都需要陽光提供基本的照明，就算是夜晚也需要一個類似月亮的照明效果。和現實中的太陽光非常相似，方向光並沒有發射源的位置，也就是說場景中，方向光所處的位置並不會對效果產生任何影響。

在 Unity 5.0 後，場景默認會創建一個方向光並關聯天空盒，相關設置在全局燈光窗口中。天空盒的顏色以及默認的太陽貼圖的位置都會和方向光綁定，以實現非常逼真的場景。

通過傾斜方向光，可以讓方向光接近平行與地面，營造出一種日出或日落的效果。如果讓方向光向斜上方照射，不僅環境會暗下來，天空盒也會暗下來，就和晚上一樣。而當方向光向下照射時，天空盒也會變得明亮，就像回到了白天。通過修改天空盒的設置，或者方向光的顏色，可以給整體環境籠罩上不同的色彩

## 區域光源

區域光源(Area Light) 在空間中是一個矩形。光線從矩形的表面均勻的向四周發射，但光線只會來自矩形的一面，而不會出現在另一面。區域光源不提供設置光照範圍的選項，但因爲光線強度是受距離約束的最終光照範圍還是會被強度所控制。由於區域光計算量較大，引擎不允許實時計算區域光照，只允許將區域光烘培到光照貼圖中

和點光源不同，區域光會從一個面發射光線到物體上，也就是說照射到物體的光線同時來自許許多多不同的點 不同的方向，所以得到的光照效果會非常柔和。使用區域光源可以營造出一條非常漂亮的充滿燈光的街道，或是以柔和的光線照亮的遊戲世界。使用一個較小的區域光，可以用來照亮一個較小的區域(例如一個房間)，但得到的效果比使用點光源得到的效果更接近真實世界的效果

![](assets/Area.png)

## 發光材質

與區域光類似，發光材質也可以從物體表面發射出光線。它們會發射出散射式的光線到場景中，引起場景中其它物體的顏色和亮度的變化。發光材質支持實時計算

在默認的着色器中，有一項 Emission 可以用來設置發光材質。此項默認未勾選，也就是材質不會發光。勾選後，就可以指定發光的貼圖 光的顏色 發光強度等

> 這種方式發射的光線只會影響場景中的靜態物體

## 環境光

環境光是一種特殊的光源，它會對整個場景提供照明，但這個光照不來自與任何一個具體的光源。它爲整個場景增加基礎的亮度，影響整體的效果。在很多情況下環境光都是必要的，一個典型的例子是明亮的卡通風格場景，這種場景要避免濃厚的陰影，甚至很多影子也是手繪到場景中，所以用環境光來替代普通的燈光會更合適。當需要整體提高場景的亮度(包括陰影處的亮度)時，也可以用環境光來實現


## 燈光設置

 燈光決定來物體的着色效果 以及物體的陰影效果

| 屬性 | 功能 |
| -------- | -------- |
| Type     | 燈光類型， 方向光 點光源 探照燈 區域光 等     |
| Range     | 指定光線照射的最遠距離。只有部分光源有此屬性     |
| Spot Angle     | 探照燈光源的照射角度     |
| Color     | 指定光的基本顏色     |
| Mode     | 燈光的渲染模式。有 RealTime(實時光照) Mixed(混合光照) Baked(預先烘培)     |
| Intensity     | 光照強度     |
| Indirect Multiplier     | 反射係數。反射就是從物體表面反射的光線，反射係數會影響反射光衰減的比例，一般整個值小於1，代表隨着反射次數增加，光線強度越來越低。但也可以大於1，讓每次反射光線都會便強，這種方法用於一些特殊情況，例如需要看清陰影處的細節的時候。也可以將此值設置爲0，既只有直射光沒有反射光，用來表現一些非常特殊的環境(例如恐怖的氣氛)     |
| Shadow Type     | 設置光線產生的陰影 硬的陰影(Hard Shadows) 軟的陰影(Soft Shadows) 沒有陰影(No Shadows)     |
| Baked Shadow Angle     | 當對方向光選擇產生軟的陰影時，此項用來柔化陰影的邊界以獲得更自然的效果     |
| Baked Shadow Radius     | 當對點光源或探照燈選擇產生軟的陰影時，此項用來柔化陰影的邊界以獲得更自然的效果     |
| Realtime Shadow     | 當選擇產生硬的陰影或軟的陰影時，此項用來控制實時陰影的效果     |
| Strength     | 用滑動條控制陰影的黑暗程度，取值範圍爲\[0,1\] 默認爲 1     |
| Resolution     | 控制陰影的解析度，較高的解析度讓陰影邊緣更準確，但是需要消耗更多的 GPU 和內存資源     |
| Bias     | 用滑動條來調整陰影離開物體的偏移量，取值範圍爲\[0\,2\] 默認 0.5 。這個選項通常用來避免錯誤的自陰影問題     |
| Normal Bias     | 用滑動條來讓產生陰影的表面沿法線方向收縮。取值範圍爲\[0,\3\] 這個選項通常用來避免錯誤的自陰影問題     |
| Near Plane     | 這個選項用來調整最近產生的陰影的平面，取值範圍爲\[0.1,10\] 默認0.2 。它的值和燈光的距離相關，是一個比例     |
| Cookie     | 指定一張貼圖，來模擬燈光投射過網格狀物體的效果(例如燈光投射過格子狀的窗戶以後，呈現處窗格的陰影)     |
| Draw Halo     | 燈光光暈，由於燈光附近的 灰塵 氣體的影響而讓光源附近出現一個團狀區域。此外unity還專門提供來光暈組件，可以和燈光的光暈同時使用     |
| Flare     | 和燈光的光暈不同，鏡頭光暈是模擬攝像機鏡頭內光線反射而出現的一種效果。在這個選項中可以指定鏡頭光暈的貼圖     |
| Render Mode     | 設置燈光渲染優先級，這會影響到燈光的真實度和性能     |
| Auto     | 運行時自動確定，和品質設置(Quality Settings) 有關     |
| Important     | 總是以像素級精度渲染，由於性能消耗更大，適用於屏幕中特別顯眼的地方     |
| Not Important     | 總是以較快的方式渲染     |
| Culling Mask     | 剔除遮罩。用來指定只有某些層會被這個燈光所影響     |

如果使用帶有透明通道的材質作爲燈光的 Cookie，那麼該 Cookie 的透明度會影響光線的亮度。可以讓亮度持續變化，這可以很好的增加場景的複雜度與氣氛

絕大部分內置的着色器都可以與每種光源協同工作。但是頂點光照着色器無法實現 Cookie 與 陰影。

* 帶有 Cookie 的探照燈，特別適合用來表現一束探照燈穿過窗戶照進房間的效果
* 低強度的點光源適合用來表現場景的深度，層次感
* 使用頂點光照着色器可以大幅提升性能。這種着色器只爲每個頂點計算光照，可以在低端設備上實現非常好的性能

# 攝像機

3D 遊戲中引入了攝像機的概念，作爲場景空間與最終屏幕展示之間的媒介。遊戲運行時至少需要一臺攝像機，也可以有多臺攝像機。多臺攝像機可以用來同時顯示場景中兩個不同的部分(比如雙人分屏遊戲)。

在 unity 中攝像機作爲一種組件被掛接到遊戲物體上，所以可以用動畫或物理系統來控制攝像機。unity 對於攝像機的數量不做限制，且可以爲這些攝像機設定不同的優先級，讓攝像機拍攝場景中不同的位置。甚至還可以用來實現某些極爲特殊的功能，例如 透視 小地圖 無人機 雙人同屏 等

![](assets/Camera.png)

| 屬性 | 功能 |
| -------- | -------- |
| Clear Flags     | 清除標記。用來指定屏幕中未繪製部分如何處理。當使用多個攝像機時這個選項非常必要     |
| Background     | 場景中沒有物體，也沒有天空盒的區域，會顯示這個選項指定的背景顏色     |
| Culling Mask     | 剔除遮罩。可以用來指定某些層不被渲染     |
| Projection     | 切換透視攝像機或正交攝像機。<br>Perspective 默認的透視攝像機模式。<br>Orthographic 正交攝像機模式。注意 延遲渲染(Deferred rendering) 在正交模式下不可用，此模式總是使用前向渲染(Forward rendering)。       |
| Size     | 當使用正交模式時，指定攝像機拍攝的範圍     |
| Field of View     | 當使用透視攝像機時，指定視野的角度     |
| Clipping Planes     | 間切面。指定攝像機渲染的距離範圍，過近或過遠的物體都不會被渲染<br> Near 靠近攝像機的那個剪切面的距離<br>Far 遠離攝像機的那個剪切面的距離     |
| Viewport Rect     | 視圖矩形。四個值用來確定攝像機拍攝到的畫面顯示到屏幕的哪個位置，以及顯示的大小。這四個值是標準化的值，取值範圍 \[0,1\]，按比例計算<br>X 攝像機畫面輸出到屏幕上的起始點的 X軸座標<br>Y 攝像機畫面輸出到屏幕上的起始點的 Y軸座標<br>W 攝像機畫面輸出到屏幕上的寬度<br> H 攝像機畫面輸出到屏幕上的高度     |
| Depth     | 深度，決定了該攝像機在繪製順序中的序號。較大深度的攝像機畫面會後繪製，所以會覆蓋較低深度的畫面     |
| Rendering Path     | 渲染路徑。unity 提供了多種渲染路徑，將影響 光照 陰影 等渲染效果<br> Use Player Settings 使用播放器的設置。渲染路徑以播放器中的設置爲准<br>Vertex Lit 頂點光照方式<br>Forward 前向渲染方式。每個物體的每個材質受到的光照影響，都會被計算一遍<br>Deferred Lighting 延遲渲染。先不考慮光照的前提下渲染每一物體，然後再統一計算每個像素的光照     |
| Target Texture     | 目標貼圖。攝像機默認渲染到屏幕上，但是設置此選項以後，就會渲染到一張貼圖上去。這在製作小地圖等特殊效果時非常有用     |
| HDR     | 讓這個攝像機 開啓 或 關閉 高範圍動態成像     |
| Target Display     | 顯示目標。指定該攝像機渲染到哪個外部設備上     |

攝像機是將遊戲畫面呈現給玩家的基礎。攝像機可以被設置 被調整 也可以用腳本來控制，甚至可以作爲子物體被掛接到其它父物體下面。對於桌面類遊戲可能只需要一個靜態的全景攝像機就夠了。而對於主視角遊戲來說，最簡單的方法是將攝像機掛接到遊戲人物身上，高度設置爲眼睛的高度。對於賽車遊戲來說，可能希望攝像機保持在車輛後面的某個位置

可以創建多個攝像機，並將它們設置爲不同的深度(Depth)。攝像機畫面會從較低深度開始，逐一繪製。

可以通過設置視圖矩形來設定攝像機畫面顯示到屏幕上的位置和大小。這樣就可以創建多個 畫中畫 效果，如 小地圖 無人機拍攝的畫面 後視鏡 等

## 清除標記

每個攝像機保存着各自的顏色和深度信息。沒有物體可渲染的部分就是空白區域，空白區域會默認渲染爲天空盒。當使用多個攝像機時，每個攝像機都保存着自己的顏色與深度信息，這些信息可以重疊。爲每一個攝像機設置不同的清除標記，可以達到同時顯示兩層畫面的效果

* Skybox
* Solid Color
* Depth only
* Don't Clear

### 天空盒

天空盒(Skybox) 是攝像機默認的清除標記，空白區域會顯示爲天空盒。這個天空盒默認以光照窗口中指定的天空盒爲准

可以爲每個攝像機添加不同的天空盒，可以嘗試在攝像機上添加專門的天空盒組件(Skybox Component)

### 純色

純色(Solid Color)，空白區域以指定的純色顯示

### 僅深度

僅深度(Depth only)，這一選項可用於混合兩個攝像機所看到的畫面。由於攝像機的深度爲多個攝像機區分了先後繪製順序，因此，後一個攝像機在攝製時，就可以保留前一個攝像機的畫面，但卻清除之前所有的深度信息。這樣一來，後一個攝像機所拍攝的畫面就會疊加到之前的畫面上，但是絕對不會被遮擋(因爲之前的深度信息已經被清除了)

這個功能很常被使用，比如用它製作主視角射擊遊戲中主角手持的槍。FPS的主角持槍靠近牆的時候，槍的模型很容易穿進牆面。而這時如果把槍放在遠離場景的位置，然後用另外一個攝像機單獨渲染槍支，再把槍支疊加到遊戲畫面中，這時槍支就絕對不會被任何物體擋住了

### 不清除

不清除(Don't Clear)，即不清除之前渲染的畫面，也不清除深度信息。結果是每個攝像機的畫面都被直接混合起來。這會造成一種比較混亂的顯示效果，這種模式在遊戲中很少使用，只有在自定義着色器的情況下才會用到

## 剪切面

攝像機所拍攝的範圍，實際上是一個金字塔的形狀，被稱爲是相機視錐(Camera Frustum)，由於我們不需要渲染特別遠處的物體，所以實際上所需拍攝的物體可以被限制在一個很有限的範圍內。這個範圍可以用兩個平面來表示，離攝像機較近的平面叫作 近剪切面(Near Clipping Plane)，較遠的叫作 遠剪切面(Far Clipping Plane)。這兩個平面截取了相機視錐的一部分，我們只需要拍攝和渲染中間的這一部分物體即可。將遠剪切面移到更遠，就可以看到更多遠處的細節，拉近遠剪切面就可以減少渲染工作提高遊戲性能。

![](assets/CameraFrustum.png)

完全位於相機視錐之外的物體不需要被渲染，這一特性被稱爲 視錐剪裁(Frustum Culling)，視錐剪裁是 3D 引擎最基本的功能之一，不需要關閉也不需要配置，但需要注意不要和其它功能搞混

更近一步，爲了深度優化遊戲，可能希望不同層級的物體具有不同的剪裁距離，例如很小的物體只有在很近處才能被看到，大型的建築在很遠的地方就會被看到。unity 提供了相關的功能，但只能在腳本中進行相關操作，方法名爲 Camera.layerCullDistance

## 視圖矩形

標準化視圖矩形(Normalized Viewport Rectangle) 用來指定攝像機所拍攝的內容固定顯示在屏幕的某一個矩形範圍內。例如 可以將一個小地圖視圖放在屏幕右下角的位置，或者將一個無人機視圖放在屏幕左上角的位置

使用視圖矩形很容易實現多人分屏遊戲，創建多個攝像機並分別設置 Viewport Rect 即可

## 渲染貼圖

渲染貼圖(Render Texture) 選項可以將攝像機所拍攝到的畫面渲染到一張貼圖上，這種貼圖可以被應用在另以物體上。這個功能讓我們很容易做出遊戲中的鏡子 顯示屏 小地圖 監視器 等內容

## Cinemachine 插件

unity官方提供了一個專業的插件 [Cinemachine](https://unity.com/cn/unity/features/editor/art-and-design/cinemachine) 來實現各種遊戲需要的攝像機，建議使用

