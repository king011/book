# 配置

```
# 檔案保存路徑 默認當前工作路徑 不支持~寫法
dir=/home/king/下載/aria2

# 啓用磁盤緩存 0 禁用 默認 16M
#disk-cache=32M


# 檔案分配方式 能有效降低 磁盤碎片 默認 prealloc
# 預分配所需要時間 none < falloc ? trunc < prealloc
# falloc trunc 需要檔案系統支持
# * NTFS 建議 falloc
# * EXT3/4 建議 trunc
# * MAC 需要註釋此項
file-allocation=trunc

# 啓用斷點恢復
continue=true

## 下下載連接相關 ##

# 最大同時下載任務數 運行時可修改 默认 5
#max-concurrent-downloads=5
# 統一服務器連接數量 添加時可指定 默認 1
max-connection-per-server=5
#最小檔案分片大小 添加時可指定 取值 [1M,1024M] 默认 20M
# 如果 min-split-size=10M 且檔案大小爲 20M 則使用 兩個來源下載 ，然 檔案大小爲15M 則使用一個來源下載
min-split-size=10M
# 單個任務最大線程數 添加時可指定 默认 5
#split=5
# 整體下載速度限制 運行時可修改 默认 0
#max-overall-download-limit=0
# 單個任務下載速度限制 默認 0
#max-download-limit=0
# 整體上傳速度限制 運行時可修改 默認 0
#max-overall-upload-limit=0
# 單個任務上傳限制  默认 0
#max-upload-limit=0
# 禁用 ipv6 默認 false
#disable-ipv6=true
# 連接超時時間 默認 60
#timeout=60
# 最大重試次數 默认 5 若爲 0 則不限制
#max-tries=5
# 設置重試等待秒數 默認 0
#retry-wait=0

## 進度相關 ##

# 從會話檔案 讀取下載任務 不支持~寫法
input-file=/home/king/.aria2/aria2.session
# 會話狀態保存檔案 不支持~寫法
save-session=/home/king/.aria2/aria2.session
# 定時保存會話到檔案 0 退出時保存 默认:0
save-session-interval=60

## RPC相關 ##

# 啓用RPC 默認 false
enable-rpc=true
# 允許所有來源 默認 false
rpc-allow-origin-all=true
# 允許非外部訪問 默認 false
rpc-listen-all=true
# 事件輪詢方式 [epoll, kqueue, port, poll, select]
event-poll=epoll
# RPC 監聽端口 默认 6800
#rpc-listen-port=6800
# 设置的RPC授权令牌, v1.18.4新增功能, 取代 --rpc-user 和 --rpc-passwd 选项
#rpc-secret=<TOKEN>

# 是否使用 https/wss
#rpc-secure=true
# 證書
#rpc-certificate=/path/to/certificate.pem
# 證書密鑰
#rpc-private-key=/path/to/certificate.key

## BT/PT 下載相關 ##

# 當下載是一個種子(以.torrent結尾)時 自動開啓BT任務 莫熱 true
#follow-torrent=true
# BT 監聽端口 默認 6881-6999
# listen-port=51413
# 當個種子最大連接數 默認 55
#bt-max-peers=55
# 打開 DHT功能  PT需要禁用 默认 true
# enable-dht=false
# 打開 ipv6 DHT功能  PT需要禁用 默认 true
#enable-dht6=false
# DHT 監聽端口 默認 6881-6999
#dht-listen-port=6881-6999
# 本地節點查找 PT 需要禁用 默認 false
#bt-enable-lpd=false
# 種子交換 PT 需要禁用 默認 true
# enable-peer-exchange=false
# 每個種子限速 對少種的 PT 很有用  默認 50K
#bt-request-peer-speed-limit=50K
# 客戶端僞裝 PT 需要
peer-id-prefix=-TR2770-
user-agent=Transmission/2.77
peer-agent=Transmission/2.77
# 當種子分享 比例 達到此時 自動停止做種 0 爲 一直做種 默認 1.0
# seed-ratio=0
# 強制保存會話 即時任務已經完成 默認 false 
#force-save=false
# BT效驗相關 默認 true
#bt-hash-check-seed=true
# 繼續之前的 BT 任務時 無需要在此效驗 默認 false
bt-seed-unverified=true
# 保存磁力鏈接元數據種子(.torrent) 默認 false
bt-save-metadata=true
```