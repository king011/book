# 異步編碼

對於 返回值 是  Future or Stream 的 函數 dart 提供了 async await 來異步 執行

* async 用來 聲明一個 異步 函數
* await 只能在async函數中 使用 其會 異步調用一個 返回值爲 Future的函數

```dart
import 'dart:math' deferred as math;

// 自定義 實現 異步 函數
Future<int> min(int l, int r) async {
  var v = math.min(l, r);
  // 返回值 不需要 調用 Future api dart 會自行處理
  return v;
}

// 聲明 main 爲 異步 函數
main() async {
  // 異步 調用 並讓出 執行 cpu
  //
  // 等待 異步 完成
  await math.loadLibrary();

  // 異步 完成 重新 得到 cpu

  // 調用 自己實現的 異步 函數 並接收 返回值
  var v = await min(11, 2);
  print(v);
}
```

# Stream

對於 Stream 需要使用 await for 來 從 Stream中 異步 獲取數據 或等待 Stream 關閉

```dart
Future main() async {
  // ...
  await for (var request in requestServer) {
    handleRequest(request);
  }
  // ...
}
```