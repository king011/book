# DiDB

一個 golang 實現的 開源(Apache 2.0) NewSQL 數據庫 支持混合事務和分析處理 工作負載 與 MySQL 部分相容 並且提供了 水平伸性 強一致性和高可用性

* 官網 [https://pingcap.com/](https://pingcap.com/)
* 源碼 [https://github.com/pingcap/tidb](https://github.com/pingcap/tidb)
