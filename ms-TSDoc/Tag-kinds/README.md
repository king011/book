# Tag kinds

TSDoc 將 Tag 分爲了三種 Block tags, modifier tags, inline tags

Tag 名稱都以 **@** 開頭後跟 **camelCase** 形式的 ASCII 字母

## Block tags

塊標籤 始終顯示爲一行的第一個元素。在規範化形式中，塊標籤應該是其中的唯一元素，除了某些爲第一行文本分配的特殊函數的標記。例如 @example 和 @throws 標籤將它們的第一行解釋爲標題

```
/**
 * This is the special summary section.
 *
 * @remarks
 * This is a standalone block.
 *
 * @example Logging a warning
 * ```ts
 * logger.warn('Something happened');
 * ```
 *
 * @example Logging an error
 * ```ts
 * logger.error('Something happened');
 * ```
 */
```

## Modifier tags

修飾符標籤 表示 API 的特殊性質。修飾符期望標記內容爲空。如果在修飾符標籤之後找到標記內容，解析器可能會選擇丟棄它，或者(在高兼容模式下)將其與前一個標籤關聯

```
/**
 * This is the special summary section.
 *
 * @remarks
 * This is a standalone block.
 *
 * @public @sealed
 */
```

例子中的 @public @sealed 都是修飾符標籤

## Inline tags
內聯標籤和 Markdown 表達式一起顯示爲內容元素。內聯標籤總是被 **{** 和 **}** 擴起來。下面是 **@link** 和 **@inheritDoc** 標籤的示例

```
class Book {
  /**
   * Writes the book information into a JSON file.
   *
   * @remarks
   * This method saves the book information to a JSON file conforming to the standardized
   * {@link http://example.com/ | Example Book Interchange Format}.
   */
  public writeFile(options?: IWriteFileOptions): void {
    . . .
  }

  /**
   * {@inheritDoc Book.writeFile}
   * @deprecated Use {@link Book.writeFile} instead.
   */
  public save(): void {
    . . .
  }
}
```
