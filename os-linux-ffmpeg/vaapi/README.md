# [vaapi](https://trac.ffmpeg.org/wiki/Hardware/VAAPI)

[codecs](http://www.ffmpeg.org/ffmpeg-codecs.html#VAAPI-encoders)

```
ffmpeg -vaapi_device /dev/dri/renderD128 -i input.mp4 -vf 'format=nv12,hwupload' -c:v h264_vaapi -qp 18 output.mp4
```