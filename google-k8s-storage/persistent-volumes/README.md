# [持久卷](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

將存儲如何供應的細節從如何被使用中抽象出來，k8s 爲此引入了兩個 API 資源 **PersistentVolume** 和 **PersistentVolumeClaim**

持久卷(PersistentVolume, PV) 是集羣中的一塊存儲，可以由管理員事先供應，或使用 存儲類(Storage Class) 來動態供應。PV 是集羣資源，就像節點也是集羣資源一樣。PV 和 普通的卷一樣，也是使用插件實現的，只是 PV 獨立於任何使用它的 Pod 的生命週期。

持久卷申領(PersistentVolumeClaim, PVC) 表達的是用戶對存儲的請求。PVC 會消耗掉 PV 資源。

儘管 PVC 允許用戶消耗 PV，常見的情況是針對不同的問題，用戶需要具有不同屬性(如 性能)的 PV 。集羣管理員需要能夠提供不同性質的 PV 並且這些 PV 之間的差別不僅限於卷大小和訪問模式。同時又不能將卷的實現細節保留給用戶。k8s 提供了 存儲類(Storage Class) 資源

# 卷和申領的生命週期

* PV 是集羣中的資源
* PVC 是對這些資源的請求

## 供應

PV 的供應有兩種方式
1. 靜態供應
2. 動態供應

### 靜態供應

集羣管理員創建若干 PV。這些卷對象帶有真實存儲的細節信息，並且對集羣用戶可見。PV 對象存在於 k8s API 中，可供用戶消費。

### 動態供應

如果管理員所創建的所有靜態 PV 都無法與用戶的 PVC 匹配，集羣可以嘗試爲該 PVC 動態供應一個存儲卷。這一供應操作是基於 StorageClass 來實現的：PVC 必須安裝某個存儲類，同時集羣管理員必須已經創建並配置了該類，這樣動態供應卷的動作才會發送。如果 PVC 指定的存儲類爲 "",則相當於爲自身禁止使用動態供應的卷。

## 綁定

用戶創建一個帶有特定存儲容器和特定訪問模式需求的 PVC 對象；在動態供應場景下，這個 PVC 對可能已經創建完畢。主控節點中的控制迴路檢測新的 PVC 對象，尋找與之匹配的 PV，並將二者綁定到一起。

## 使用

Pod 將 PVC 當作存儲卷來使用。集羣會檢視 PVC，找到所綁定的卷，並爲 Pod 掛載該卷。對於支持多種訪問模式的卷，用戶要在 Pod 中以卷的形式使用申領時指定期望的訪問模式。

## 保護使用中的存儲對象
保護使用中的存儲對象(Storage Object in Use Protection) 這一功能特性的目的是確保仍被 Pod 使用的 PVC 以及所綁定的 PV 在系統中不會被刪除。

如果管理員刪除某 Pod 使用的 PVC，該 PVC不會被立刻移除。PVC 的移除會被推遲，直到其不再被任何 Pod 使用。同樣如果管理員刪除已綁定到某 PVC 的 PV，該 PV 也不會被立刻移除。該 PV 對象的移除也會推遲到 該 PV 不再綁定到 PVC 時。

你可以查詢 PVC 的狀態爲 Terminating 且其 Finalizers 列表中包含 kubernetes.io/pvc-protection 時 表示此 PVC 處於被保護狀態

```
kubectl describe pvc hostpath
```

```
Name:          hostpath
Namespace:     default
StorageClass:  example-hostpath
Status:        Terminating
Volume:
Labels:        <none>
Annotations:   volume.beta.kubernetes.io/storage-class=example-hostpath
               volume.beta.kubernetes.io/storage-provisioner=example.com/hostpath
Finalizers:    [kubernetes.io/pvc-protection]
...
```

也可以查看 PV 的 狀態爲 Terminating 且 Finalizers 列表中包含 kubernetes.io/pvc-protection 時 表示此 PV 處於被保護狀態

```
kubectl describe pv task-pv-volume
```

```
Name:            task-pv-volume
Labels:          type=local
Annotations:     <none>
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:    standard
Status:          Terminating
Claim:
Reclaim Policy:  Delete
Access Modes:    RWO
Capacity:        1Gi
Message:
Source:
    Type:          HostPath (bare host directory volume)
    Path:          /tmp/data
    HostPathType:
Events:            <none>
```

## 回收

當用戶不再使用存儲卷時，它們可以從 API 中 將 PVC 刪除，從而允許該資源被回收再利用。PV 的回收策略告訴集羣，當其被從PVC中釋放時 如何處理該數據卷。

## 保留(Retain)

Retain 使得用戶可以手動回收資源。當 PVC 被刪除時，PV 仍然存在，對應的數據卷被視爲 已釋放(released) 。 由於卷上仍然存在着前一申領人的數據，該卷還不能用於其它申領。管理員可以通過下述步驟手動回收該卷

1. 刪除 PV 對象。與之相關的 處於外部基礎設施中的存儲資產(例如 AWS EBC, GCE PD, Zaure Disk, Cinder ...)在 PV 刪除後仍然存在。
2. 根據情況，手動清除所關聯的存儲資產上的數據。
3. 手動刪除所有關聯的存儲資產；如果你希望重用該存儲資產，可以基於存儲資產的定義創建新的 PV 對象。

### 刪除(Delete)

對於資產 Delete 回收策略的卷插件，刪除動作會將 PV 從 k8s 中移除，同時也會從外部基礎設施(如 AWS EBS,AWS EBC, GCE PD, Zaure Disk, Cinder ...) 中移除所關聯的存儲資產。動態供應的卷會繼承其 StorageClass 中設置的回收策略，該策略默認爲 Delete。 管理員需要根據 用戶的期望來配置 StorageClass，否則 PV 被創建之後必須要被 編輯或者修補

### 回收(Recycle)

Recycle 已經被棄用。應該使用新的方案 動態供應。

## 預留 PersistentVolume

提供在 PVC 中 指定 PV，你可以聲明該 特定 PV 和 PVC 間的綁定關係。如果該 PV 存在且未被通過其 claimRef 字段預留給 PVC，則該 PV 會和 該 PVC 綁定到一起。

綁定操作不會考慮某些卷匹配條件是否滿足，包括節點親和性等。控制面板仍然會檢查 存儲類 訪問模式 請求的尺寸 

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: foo-pvc
  namespace: foo
spec:
  storageClassName: "" # 此處必須顯示設置空字符串，否則會別設置爲默認的 StorageClass
  volumeName: foo-pv
  ...
```

此方法無法對 PV 的綁定特權做出任何形式的保證。如果有其它 PVC 可以使用你所指定的 PV，則你應該首先預留 該存儲卷。你可以將 PV 的 claimRef 終端 設置爲相關的 PVC 以確保其它 PVC 不會綁定到該 PV

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: foo-pv
spec:
  storageClassName: ""
  claimRef:
    name: foo-pvc
    namespace: foo
  ...
```

# 擴充 PVC

當前 k8s v1.11 默認開啓 pvc 擴充。可以擴充以下類型的卷

* gcePersistentDisk
* awsElasticBlockStore
* Cinder
* glusterfs
* rbd
* Azure File
* Azure Disk
* Portworx
* FlexVolumes CSI

只有當 PVC 的存儲類中 將 allowVolumeExpansion 設置爲 true 時 才可以擴展 PVC

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: gluster-vol-default
provisioner: kubernetes.io/glusterfs
parameters:
  resturl: "http://192.168.10.100:8080"
  restuser: ""
  secretNamespace: ""
  secretName: ""
allowVolumeExpansion: true
```

如果要爲某 PVC 請求較大的存儲類 可以編輯 PVC 對象，設置一個更大的尺寸值。這一操作會觸發下層 PV 提供存儲的卷擴充。k8s 不會創建新都 PV 來滿足此申領請求，而是將現有卷調整大小。

# 持久卷

每個 PV 對象都包含 spec 和 status 部分，分別對應卷的規約和狀態

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0003
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: slow
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /tmp
    server: 172.17.0.2
```

## 容量

一般而言，每個 PV 都有確定的存儲容量。使用 capacity 屬性來設置。

目前，存儲大小是可以設置和請求的唯一資源。未來可能會包含 IOPS 吞吐量等屬性

## 卷模式

針對 PV 持久卷，k8s 支持兩種 卷模式(volumeModes): Filesystem(檔案系統) 和 Block(塊)。volumeModes 是一個可選的 API 參數，默認爲 Filesystem。

volumeMode 屬性設置爲 Filesystem 的卷會被 Pod 掛載(Mount) 到某個目錄。如果該卷的存儲來自某塊設備而該設備目前爲空，k8s 會在第一次掛載卷之前 在設備上創建檔案系統

volumeMode 屬性設置爲 Block 的卷 作爲原始設備來使用。這類卷以塊設備的方式交給 Pod 使用，其上沒有任何檔案系統。這種模式對於爲 Pod 提供一種使用最快可能方式來訪問卷而言很有幫助，Pod 和 卷 之間不在檔案系統。另外 Pod 中運行的應用必須知道如何處理原始塊設備。

## 訪問模式

PV 可以用資源提供者所支持的任何方式掛載到宿主系統上。

訪問模式有
* ReadWriteOnce 卷可以被一個節點以讀寫方式掛載
* ReadOnlyMany 卷可以被多個節點以只讀方式掛載
* ReadWriteMany 卷可以被多個節點以讀寫方式掛載

在目錄行中 訪問模式 也使用以下 縮寫形式

* RWO - ReadWriteOnce
* ROX - ReadOnlyMany
* RWX - ReadWriteMany

## 類

每個 PV 可以屬於某個類 通過將其 storageClassName 屬性設置爲某個 StorageClass 的名稱來指定。特定的 PV 卷只能綁定到請求該 存儲類的 PVC。未設置 storageClassName 的 PV 卷沒有類設定，只能綁定到那些沒有指定特定 存儲類的 PVC

早前 k8s 使用註解 volume.beta.kubernetes.io/storage-class 而非 storageClassName 屬性。目前這一註解仍人有效 但將來可能會徹底廢棄

## 回收策略

目前的回收策略有 
* Retain 手動回收
* Recycle 基本擦除 (rm -rf /thevolume/\*)
* Delete 

目前 僅 NFS 和 HostPath 支持 回收

## 掛載選項

k8s 使用 mountOptions 指定 附加的掛載選項 ，k8s不會檢查選項是否合法，非法的選項只會導致掛載失敗

早前 k8s 使用註解 volume.beta.kubernetes.io/mount-options 而非 mountOptions 指定 附加選項，目前仍然有效，但將來可能徹底廢棄

## 節點親和性

每個 PV 可以設置 節點親和性 來定義一些約束，從而限制從哪些節點上可以訪問此卷。使用這些卷的 Pod 只會被調度到節點親和性規則所選擇的節點上執行

> 對大多類型卷而言 不需要設置 節點親和性。 AWS EBS、 GCE PD 和 Azure Disk 類型的卷都能自動設置相關字段。你需要爲 local 卷顯示設置此屬性

## 階段

每個卷會處於以下節點(Phase)之一

* Available -> 可用 卷是一個空閒的資源 尚未綁定到任何申領
* Bound -> 已綁定 該卷已經綁定到某個申領
* Released -> 已釋放 所綁定的申領已被刪除，但是資源尚未被集羣回收
* Failed -> 失敗 卷的自動回收操作失敗

# PersistentVolumeClaims

每個 PVC 對象都有 spec 和 status 部分，分別對於申領的規約和狀態

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
```

## 訪問模式 

accessModes 屬性 指定 訪問模式

## 卷模式

volumeMode 屬性 指定 卷模式

## 資源

resources 屬性 指定 要消耗的 PV 資源

## 選擇算號

selector 指定 選擇算符

* matchLabels 卷必須包含有此值的標籤
* matchExpressions 通過 手設定鍵(key) 值列表和操作符(operator) 來構造需求 合法的操作符有 In, NotIn, Exists, and DoesNotExist

matchLabels 和 matchExpressions 的所有需求都會使用 and 組合到一起，必須全部滿足才視爲匹配

## 類

使用 storageClassName 設置 申領的 存儲類

# 使用申領作爲卷

Pod 將 PVC 作爲卷來使用，並藉此訪問存儲資源

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```