# angular

wails 沒有提高 angular 模板(可能是 agular 相對其它前端太過複雜和更新頻繁)，但是它是支持的你只需要自己創建一個 angular 項目到 frontend 檔案夾，然後修改 **wails.json** 設定使用 angular 命令即可

```
  "frontend:build": "npx ng build",
  "frontend:install": "npm install",
  "frontend:dev:watcher": "npx ng serve",
  "frontend:dev:serverUrl": "http://localhost:4200",
```

大部分設定都是見名知意的，serverUrl 用於指定 wails 從哪個 url 檢測前端是否已經運行，這在 `wails dev` 指令執行時非常重要
