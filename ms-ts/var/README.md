# var let const
ts 使用 和 js 相同的 變量 聲明 方式

* var 反人類的 使用 函數 作爲 作用域
* let 意識到了 var 的 反人類 於是 修正爲 使用 塊 作爲 作用域
* const 類似 let 不過 其儲存的值 只可在定義時指定

# 解構數組
```ts
let input = [1, 2];
// 相當於 let first = input[0], second = input[1];
let [first, second] = input;
// swap
// _a = [second, first], first = _a[0], second = _a[1];
[first, second] = [second, first];
```

## 解構時 = 兩邊 數量 可以不等 
```ts
let [first, ...rest] = [1, 2, 3, 4];
console.log(first,rest) // 1 [ 2, 3, 4 ]
let [, second, , fourth] = [1, 2, 3, 4];
console.log(second,fourth) // 2 4
```
**es5**
```js
"use strict";
var _a = [1, 2, 3, 4], first = _a[0], rest = _a.slice(1);
console.log(first, rest);
var _b = [1, 2, 3, 4], second = _b[1], fourth = _b[3];
console.log(second, fourth);
```

## 解構也可用於函數 
```ts
function f([first, second]: [number, number]) {
    console.log(first);
    console.log(second);
}
f([1, 2]);
```
**es5**
```js
"use strict";
var input = [1, 2];
function f(_a) {
    var first = _a[0], second = _a[1];
    console.log(first);
    console.log(second);
}
f(input);
```
# 解構對象
```ts
let o = {
    a: "foo",
    b: 12,
    c: "bar"
};
let { a, b } = o;
```
**es5**
```js
"use strict";
var o = {
    a: "foo",
    b: 12,
    c: "bar"
};
var a = o.a, b = o.b;
```

```ts
let o = {
    a: "foo",
    b: 12,
    c: "bar"
};
let { a, ...passthrough } = o;
let total = passthrough.b + passthrough.c.length;
```
**es5**
```js
"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
var o = {
    a: "foo",
    b: 12,
    c: "bar"
};
var a = o.a, passthrough = __rest(o, ["a"]);
var total = passthrough.b + passthrough.c.length;
```
## 屬性 重命名
```ts
let o = {
    a: "foo",
    b: 12,
    c: "bar",
};
let { a: newName1, b: newName2 } = o;
```
**es5**
```js
"use strict";
var o = {
    a: "foo",
    b: 12,
    c: "bar",
};
var newName1 = o.a, newName2 = o.b;
```
## 函數
```ts
let o = {
    a: "foo",
    b: 12,
    c: "bar",
};
type C = { a: string, b?: number }
function f({ a, b }: C): void {
    console.log(a)
    console.log(b)
}
f(o)
```
**es5**
```js
"use strict";
var o = {
    a: "foo",
    b: 12,
    c: "bar",
};
function f(_a) {
    var a = _a.a, b = _a.b;
    console.log(a);
    console.log(b);
}
f(o);
```

# 展開
展開 執行 和 解構 相反的 操作
```ts
let first = [1, 2];
let second = [3, 4];
let bothPlus = [0, ...first, ...second, 5];
```
```js
"use strict";
var first = [1, 2];
var second = [3, 4];
var bothPlus = [0].concat(first, second, [5]);
```