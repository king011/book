# [背景音樂](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/bgm.html)

使用 Bgm 類，您可以管理與應用程式（或遊戲）生命週期狀態變更相關的背景音樂曲目的循環

當應用程式終止或發送到背景時，Bgm 將自動暫停目前播放的音樂曲目。 同樣，當應用程式恢復時，Bgm將恢復背景音樂。 也支援手動暫停和恢復曲目

為了使此類正常運行，必須透過呼叫以下命令來註冊觀察者：

```
FlameAudio.bgm.initialize();
```

**重要提示**：必須在 WidgetsBinding 類別的實例已存在的時間點呼叫初始化函數。 最佳實踐是將此呼叫放在遊戲的 onLoad 方法中

如果您聽完背景音樂但仍想保持應用程式/遊戲運行，請使用 dispose 函數刪除觀察者

```
FlameAudio.bgm.dispose();
```

若要播放循環背景音樂曲目，請執行：

```
import 'package:flame_audio/flame_audio.dart';

FlameAudio.bgm.play('adventure-track.mp3');
```

您必須具有適當的資料夾結構並將文件新增至 pubspec.yaml 檔案中，如 [Flame Audio 文檔](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/audio.html)中所述

# [Caching music files](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/bgm.html#caching-music-files)

預設情況下，Bgm 類別將使用 FlameAudio 的靜態實例來儲存快取的音樂檔案

因此，為了預先載入音樂，您可以使用 [Flame Audio 文檔](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/audio.html)中的相同建議

如果您願意，您可以選擇使用不同的支援 AudioCache 來建立自己的 Bgm 實例

# [Methods](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/bgm.html#methods)

## [Play](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/bgm.html#play)

play 函數接受一個字串，該字串應該是指向要播放的音樂檔案位置的路徑（遵循 Flame Audio 資料夾結構要求）

您可以傳遞一個額外的可選雙參數，即音量（預設為 1.0）

Examples:

```
FlameAudio.bgm.play('music/boss-fight/level-382.mp3');
```

```
FlameAudio.bgm.play('music/world-map.mp3', volume: .25);
```

## [Stop](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/bgm.html#stop)

要停止目前播放的背景音樂曲目，只需呼叫 stop 即可

```
FlameAudio.bgm.stop();
```

## [Pause and Resume](https://docs.flame-engine.org/1.16.0/bridge_packages/flame_audio/bgm.html#pause-and-resume)

若要手動暫停和恢復背景音樂，您可以使用 pause 和 resume 函數

FlameAudio.bgm 會自動處理暫停並恢復目前播放的背景音樂曲目。 當焦點返回應用程式/遊戲時，手動暫停可防止應用程式/遊戲自動恢復

```
FlameAudio.bgm.pause();
```

```
FlameAudio.bgm.resume();
```