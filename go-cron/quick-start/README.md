# 快速上手
```
package main

import (
	"fmt"
	"log"
	"time"

	"github.com/robfig/cron/v3"
)

func main() {
	c := cron.New()

	_, e := c.AddFunc("@every 1s", func() {
		fmt.Println("tick every 1 second")
	})
	if e != nil {
		log.Fatalln(e)
	}
	c.Start()

	time.Sleep(time.Second * 5)
	<-c.Stop().Done()
}
```

1. AddFunc/AddJob 增加一個定時任務，第一個參數是時間規則，第二個參數是無參回調 或 interface
2. 回調會新啓動一個 goroutine 執行，故多個回調需要自己保證鎖安全，即時同個定時任務可能會同時觸發多次回調(回調很耗時，但時間規則間隔很短，前個回調還未結束到時間了 cron 直接新啓goroutine 執行新回調)
3. Start 會在新的 goroutine 中執行定時器
4. Stop 會停止定時器，Done 返回的 chan 將在所有工作的回調函數結束後返回

```go
type Job interface {
  Run()
}
```
# @every

@every 指定一個字符串送入到 [time.ParseDuration](https://pkg.go.dev/time?utm_source=gopls#ParseDuration) 解析，然後每隔這麼長時間執行一次回調函數

* @every 1s 每隔1秒
* @every 1h 每隔1小時
* @every 1m2s 每隔1分鐘2秒


# crontab

crob 也支持類似 linux crontab 的時間指定模式,從左到右分別爲

| 含義 | 取值 | 支持字符 |
| -------- | -------- | -------- |
| 分鐘     | [0-59]     | * / , -     |
| 小時     | [0-23]     | * / , -     |
| 號數     | [1-31]     | * / , - ?     |
| 月份     | [1-12] 或者月份英文前三個字符不區分大小寫     | * / , -     |
| 星期     | [0-6] 或者星期英文前三個字符不區分大小寫     | * / , - ?     |

> 0 表示星期天

| 字符 | 含義 | 例子 |
| -------- | -------- |
| *     | 匹配任意值     |
| /     | 指定範圍步長     | 小時欄值爲 2-40/3 表示2分到40分，每隔3分鐘觸發一次 |
| ,     | 欄目多個值分隔符號     |
| -     | 指定全閉範圍區間     |
| ?     | 只能在號數和星期欄目使用，和 * 同義     |

## 語法糖

cron 定義了 幾個語法糖來作爲預定義的時間規則

| 語法糖 | 對應規則 | 含義 |
| -------- | -------- | -------- |
| @yearly 或者 @annually     | 0 0 1 1 *     | 每年開始執行一次     |
| @monthly     | 0 0 1 * *     | 每月開始執行一次     |
| @weekly     | 0 0 * * 0     | 每週開始(星期天)執行一次     |
| @daily 或者 @midnight     | 0 0 * * *     | 每天開始執行一次     |
| @hourly     | 0 * * * *     | 每小時開始執行一次     |

# 時區

可以在 cron.New 時指定 默認時區，此外在添加任務時也可以在時間規則最前面加上一個 CRON_TZ= 用於指定時區

```
func main() {
  nyc, _ := time.LoadLocation("America/New_York")
  c := cron.New(cron.WithLocation(nyc))
  c.AddFunc("0 6 * * ?", func() {
    fmt.Println("Every 6 o'clock at New York")
  })

  c.AddFunc("CRON_TZ=Asia/Tokyo 0 6 * * ?", func() {
    fmt.Println("Every 6 o'clock at Tokyo")
  })

  c.Start()

  for {
    time.Sleep(time.Second)
  }
}
```