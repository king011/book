# [Bracket Pair Colorizer](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer)

此插件允許使用顏色來匹配括號。用戶可以定義要匹配的字符以及要使用的顏色

```
ext install CoenraadS.bracket-pair-colorizer
```

2021年 vscode 以及內置來此功能

* 源碼 [https://github.com/CoenraadS/BracketPair](https://github.com/CoenraadS/BracketPair)
* 協議 [MIT](https://github.com/CoenraadS/BracketPair/blob/develop/LICENSE.md)

# 設定

```
#info="settings.json"
{
    "editor.bracketPairColorization.enabled": true,
    "editor.guides.bracketPairs":"active"
}
```

| 屬性 | 取值 | 描述 |
| -------- | -------- | -------- |
| editor.bracketPairColorization.enabled     | true 或 flase     | 當爲 true 時，爲匹配的括號着色     |
| editor.guides.bracketPairs     | true 或 false 或 "active"     | 爲 true 或 "active" 時，爲匹配的括號畫出輔助線     |


