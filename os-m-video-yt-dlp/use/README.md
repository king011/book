# 查詢視頻可用格式

通常可以使用 **-F** 或 **--list-formats** 指令來查詢下視頻所有可用格式

```
yt-dlp -F "$URL"
```

如果存在播放列表 youtube-dl 會查詢所有列表內容，可以使用 **--no-playlist** 參數設置不需要列表內容

```
yt-dlp -F --no-playlist  "$URL"
```

# 下載視頻

```
yt-dlp  "$URL"
```

通常 youtube-dl 會自動下載質量最高格式的視頻，但你可能需要特定格式可以使用 **-f** 參數指定， -f 參數支持多種指定方式，最簡單的用法是傳入 檔案後綴名(也可以傳入 -F 查詢出來的 format code 數值來指定具體哪個源)

```
yt-dlp --no-playlist -f mp4  "$URL"
```

```
# 下載質量最好的視頻
yt-dlp "$URL"
yt-dlp  -f  'bv+ba' "$URL"

# 下載高度小於 1300 的中質量最好的視頻(通常會匹配到 1080p 的視頻，這比指定1080p好因爲有些不規則視頻尺寸會剛好超過1080一點)
yt-dlp  -f 'bv[height<1300]+ba' "$URL"


```

# youtube 

通常 youtube 對質量的視頻使用了 視頻 和 音頻 分開存儲的方式

```
yt-dlp --no-playlist -f mp4  "$URL"

[youtube] NmC18U3U4yc: Downloading webpage
[youtube] NmC18U3U4yc: Downloading android player API JSON
[info] Available formats for NmC18U3U4yc:
ID  EXT   RESOLUTION FPS │   FILESIZE   TBR PROTO │ VCODEC          VBR ACODEC      ABR     ASR MORE INFO
─────────────────────────────────────────────────────────────────────────────────────────────────────────────────
sb2 mhtml 48x27          │                  mhtml │ images                                      storyboard
sb1 mhtml 83x45          │                  mhtml │ images                                      storyboard
sb0 mhtml 166x90         │                  mhtml │ images                                      storyboard
139 m4a   audio only     │   15.52MiB   48k https │ audio only          mp4a.40.5   48k 22050Hz low, m4a_dash
249 webm  audio only     │   15.67MiB   49k https │ audio only          opus        49k 48000Hz low, webm_dash
250 webm  audio only     │   20.57MiB   64k https │ audio only          opus        64k 48000Hz low, webm_dash
140 m4a   audio only     │   41.20MiB  129k https │ audio only          mp4a.40.2  129k 44100Hz medium, m4a_dash
251 webm  audio only     │   40.64MiB  127k https │ audio only          opus       127k 48000Hz medium, webm_dash
17  3gp   176x144      6 │   23.98MiB   75k https │ mp4v.20.3       75k mp4a.40.2    0k 22050Hz 144p
394 mp4   256x138     25 │   25.10MiB   78k https │ av01.0.00M.08   78k video only              144p, mp4_dash
160 mp4   256x138     25 │   12.70MiB   39k https │ avc1.4d400c     39k video only              144p, mp4_dash
278 webm  256x138     25 │   22.24MiB   69k https │ vp9             69k video only              144p, webm_dash
395 mp4   426x230     25 │   52.75MiB  165k https │ av01.0.00M.08  165k video only              240p, mp4_dash
133 mp4   426x230     25 │   26.47MiB   83k https │ avc1.4d4015     83k video only              240p, mp4_dash
242 webm  426x230     25 │   31.31MiB   98k https │ vp9             98k video only              240p, webm_dash
396 mp4   640x346     25 │   82.57MiB  259k https │ av01.0.01M.08  259k video only              360p, mp4_dash
134 mp4   640x346     25 │   48.62MiB  152k https │ avc1.4d401e    152k video only              360p, mp4_dash
18  mp4   640x346     25 │  138.95MiB  436k https │ avc1.42001E    436k mp4a.40.2    0k 44100Hz 360p
243 webm  640x346     25 │   61.51MiB  193k https │ vp9            193k video only              360p, webm_dash
397 mp4   854x462     25 │  136.22MiB  428k https │ av01.0.04M.08  428k video only              480p, mp4_dash
135 mp4   854x462     25 │   87.12MiB  273k https │ avc1.4d401e    273k video only              480p, mp4_dash
244 webm  854x462     25 │  101.07MiB  317k https │ vp9            317k video only              480p, webm_dash
22  mp4   1280x694    25 │ ~197.52MiB  606k https │ avc1.64001F    606k mp4a.40.2    0k 44100Hz 720p
398 mp4   1280x694    25 │  259.04MiB  814k https │ av01.0.05M.08  814k video only              720p, mp4_dash
136 mp4   1280x694    25 │  151.84MiB  477k https │ avc1.4d401f    477k video only              720p, mp4_dash
247 webm  1280x694    25 │  184.66MiB  580k https │ vp9            580k video only              720p, webm_dash
399 mp4   1920x1040   25 │  408.48MiB 1283k https │ av01.0.08M.08 1283k video only              1080p, mp4_dash
137 mp4   1920x1040   25 │  410.61MiB 1290k https │ avc1.640028   1290k video only              1080p, mp4_dash
248 webm  1920x1040   25 │  316.90MiB  995k https │ vp9            995k video only              1080p, webm_dash
```

對於這種情況可以分別下載 視頻 和 音頻後 使用 ffmpeg 將其合成到一起

```
ffmpeg -i input.mp4 -i input.m4a -c:v copy -c:a copy output.mp4
```

# flat-playlist

flat-playlist 可以用於獲取視頻相關信息

```
yt-dlp --flat-playlist --print "list=%(playlist_index)s title=%(title)s url=%(url)s" "$URL"
```

print 指定了輸出內容的 [輸出模板](https://github.com/yt-dlp/yt-dlp#output-template)，可以使用 **%(varname)s** 獲取相關變量