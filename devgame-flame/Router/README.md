# [RouterComponent](https://docs.flame-engine.org/1.16.0/flame/router.html)

RouterComponent 的工作是管理遊戲中多個畫面的導航。 它在本質上與 Flutter 的 Navigator 類別類似，只是它使用 Flame 元件而不是 Flutter 小部件。

典型的遊戲通常由多個頁面組成：啟動畫面、開始功能表頁面、設定頁面、製作人員名單、主遊戲頁面、幾個彈出視窗等。路由器將組織所有這些目的地，並允許您在之間切換他們。

在內部，RouterComponent 包含一個路由堆疊。 當您請求它顯示路由時，它將放置在堆疊中所有其他頁面的頂部。 稍後您可以 pop() 從堆疊中刪除最頂層的頁面。 路由器的頁面透過其唯一的名稱進行尋址。

路由器中的每個頁面可以是透明的或不透明的。 如果頁面不透明，則堆疊中位於其下方的頁面不會呈現，也不會接收指標事件（例如點擊或拖曳）。 相反，如果一個頁面是透明的，那麼它下面的頁面將正常渲染並接收事件。 此類透明頁面對於實作模式對話框、清單或對話 UI 等非常有用。

```
class MyGame extends FlameGame {
  late final RouterComponent router;

  @override
  void onLoad() {
    add(
      router = RouterComponent(
        routes: {
          'home': Route(HomePage.new),
          'level-selector': Route(LevelSelectorPage.new),
          'settings': Route(SettingsPage.new, transparent: true),
          'pause': PauseRoute(),
          'confirm-dialog': OverlayRoute.existing(),
        },
        initialRoute: 'home',
      ),
    );
  }
}

class PauseRoute extends Route { ... }
```

# [Route](https://docs.flame-engine.org/1.16.0/flame/router.html)

路由元件保存有關特定頁面內容的資訊。 路由作為子級掛載到 RouterComponent。

路由的主要屬性是它的建構器 - 使用其頁面內容建立元件的函數。

此外，路由可以是透明的，也可以是不透明的（預設）。 不透明會阻止其下方的路由呈現或接收指標事件，而透明路由則不會。 根據經驗，如果路由是全屏，則聲明路由不透明；如果路由只覆蓋屏幕的一部分，則聲明路由透明。

預設情況下，路由從堆疊中彈出後會保持頁面元件的狀態，並且僅在第一次啟動路由時呼叫建構器函數。 將 keepState 設為 false 會在從路由堆疊中彈出路由後刪除頁面元件，並且每次啟動路由時都會呼叫建構器函數。

可以使用pushReplacementNamed或pushReplacement取代目前路由。 每個方法只是在目前路由上執行 pop，然後執行 pushNamed 或 pushRoute。

# [OverlayRoute](https://docs.flame-engine.org/1.16.0/flame/router.html#overlayroute)

OverlayRoute 是一種特殊的路由，允許透過路由器添加遊戲疊加層。 這些路由預設是透明的

OverlayRoute 有兩個建構子。 第一個建構函式需要一個建構器函數來描述如何建構覆蓋層的小部件。 當 GameWidget 中已指定建構器函數時，可以使用第二個建構子：

```
final router = RouterComponent(
  routes: {
    'ok-dialog': OverlayRoute(
      (context, game) {
        return Center(
          child: DecoratedContainer(...),
        );
      },
    ),  // OverlayRoute
    'confirm-dialog': OverlayRoute.existing(),
  },
);
```

在 GameWidget 中定義的 overlays 不需要事先在路由映射中宣告： RouterComponent.pushOverlay() 方法可以為您做到這一點。 


可以使用 pushOverlay/pop 管理 overlays，但不要和 overlays.add/remove 混用。add/remove 不會進入路由棧，混用可能產生 bug

# [ValueRoute](https://docs.flame-engine.org/1.16.0/flame/router.html#valueroute)

ValueRoute 是一個能返回值的路由。例如，此類路由可用於要求使用者提供某些回饋的對話方塊

為了使用 ValueRoutes，需要兩個步驟：

1. 建立從 ValueRoute&lt;T&gt; 類別派生的路由，其中 T 是路由將傳回的值的類型。 在該類別中重寫 build() 方法來建構將要顯示的元件。 元件應該使用completeWith(value)方法來彈出路由並傳回指定的值

	```
	class YesNoDialog extends ValueRoute<bool> {
		YesNoDialog(this.text) : super(value: false);
		final String text;
		@override
		Component build() {
			return PositionComponent(
				children: [
					RectangleComponent(),
					TextComponent(text: text),
					Button(
						text: 'Yes',
						action: () => completeWith(true),
					),
					Button(
						text: 'No',
						action: () => completeWith(false),
					),
				],
			);
		}
	}
	```
	
2. 使用 Router.pushAndWait() 顯示路由，該路由傳回一個 future，該 future 使用從路由傳回的值進行完成

	```
	Future<void> foo() async {
		final result = await game.router.pushAndWait(YesNoDialog('Are you sure?'));
		if (result) {
			// ... the user is sure
		} else {
			// ... the user was not so sure
		}
	}
	```