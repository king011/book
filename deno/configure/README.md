# [configure](https://deno.land/manual@v1.28.1/getting_started/configuration_file)

```
#info="deno.jsonc"
{
  "compilerOptions": {
    "checkJs": true // 檢查 js 傳入的數據類型
  },
  "importMap": "import_map.json", // 使用 import_map 管理依賴似乎是目前唯一正確的選擇
  "tasks": {
    "run": "deno run --lock-write src/main.ts",
    "test": "deno test --lock-write --fail-fast",
    "bench": "deno bench --lock-write src/main.ts",
    "cache": "deno cache --lock-write src/main.ts",
    "reload": "deno cache --reload --lock-write src/main.ts",
    "bundle": "deno bundle --lock-write src/main.ts -- ./bundle.js",
    "compile": "deno compile --lock-write --output ./bundle src/main.ts" // 不建議使用,這會打包一個完整的v8引擎,幹嘛不用 golang 更快更方便
  }
}
```

```
#info="import_map.json"
{
  "imports": {
    "std/": "https://deno.land/std@0.165.0/",
    "easyts/":"https://raw.githubusercontent.com/powerpuffpenguin/easyts/0.0.16/deno/"
  }
}
```