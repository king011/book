# table

table 是 lua 中唯一的一種數據結構，其它結構都只能以 table 來模擬

table 是一個 key-value 鍵值對，你可以使用任意型別數據轉爲 key 和 value

```
-- 創建 table
local t={}
-- 設置 key
t['name']='lua'
-- 刪除 key
t['name']=nil
-- 刪除 table
t=nil
```

# 數組
當 table 的 key 是連續的 正整數時 table 就是一個數組，lua 還爲此提供了一些輔助函數

```
local function printArray(arrs)
    -- 遍歷數組
    print('list:')
    for _, v in ipairs(arrs) do
        print('\t'..v)
    end    
end

-- 初始化數組
local arrs={1,3,2}
-- 排序數組
-- sort(list: Array, comp: fcuntion(a,b)=>boolean)
table.sort(arrs,function (a, b)
    return a>b
end)
printArray(arrs)

-- 在末尾添加元素
table.insert(arrs,5) -- 3 2 1 5
-- 在指定位置添加元素
table.insert(arrs,5,6) -- 3 2 1 5 nil 6
table.insert(arrs,4,4) -- 3 2 1 4 5 6
printArray(arrs)

-- 刪除指定位置的元素
table.remove(arrs,3) -- 3 2 4 5 6
-- 刪除最後一個元素
table.remove(arrs) -- 3 2 4 5
printArray(arrs)
```

# 字符串連接緩存

lua 的字符串都是 const 的，當有大量字符串需要連接時可以使用 table 緩存後再一次連接以降低消耗

```
-- 定義一個緩存數組
local strs={}
for i = 1, 10 do
    table.insert(strs,i) -- 將要連接的內容緩存進數組
end
-- join
-- function table.concat(list, sep?:string, first?:integer, last?:integer):string
print(table.concat(strs,' ',5,6))
```