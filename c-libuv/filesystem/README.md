# [檔案系統](http://docs.libuv.org/en/v1.x/guide/filesystem.html)

libuv 使用 uv\_fs\_\* 函數 和 uv_fs_t 實現簡單的檔案系統 讀/寫

> filesystem 與套接字不同，套接字使用操作系統提供的非阻塞操作。檔案系統在內部使用阻塞函數，但在線程池中調用這些函數並在需要應用程式交互時通知在事件循環中註冊的觀察者

* 所有 filesystem functions 都提供了 同步 和 異步 兩種方式

如果回調傳輸爲 NULL 則以同步方式回調，函數返回值是一個 libuv 錯誤碼。此返回值通常只對同步調用有用。異步形式在傳入回調且返回值爲 0 時被調用。

# 讀寫檔案
* 使用檔案描述符：

	```
	int uv_fs_open(uv_loop_t* loop, uv_fs_t* req, const char* path, int flags, int mode, uv_fs_cb cb)
	```
	
	flags 和 mode 是標準的 unix 標誌。libuv 負責轉換爲適當的 windows 標誌。
	
* 關閉檔案描述符：

	```
	int uv_fs_close(uv_loop_t* loop, uv_fs_t* req, uv_file file, uv_fs_cb cb)
	```
	
* 回調簽名

	```
	void callback(uv_fs_t* req);
	```
	
下面是一個來實現一個簡單的 cat：
	
```
#include <iostream>
#include <uv.h>
#define BUFFER_SIZE (1024 * 32)
// 自定義結構存儲 讀寫 額外信息
struct request_t
{
    uv_fs_t req;
    uv_buf_t buf;
    int cap;
    uv_file fd;
};
void post_read(uv_loop_t *loop, request_t *req);
void on_write(uv_fs_t *req)
{
    if (req->result < 0)
    {
        fprintf(stderr, "Write error: %s\n", uv_strerror((int)req->result));
    }
    else
    {
        // 寫入成功 繼續讀檔案
        post_read(req->loop, (request_t *)req->data);
    }
}
void on_read(uv_fs_t *req)
{
    if (req->result < 0)
    {
        fprintf(stderr, "Read error: %s\n", uv_strerror(req->result));
    }
    else if (req->result == 0)
    {
        // 檔案讀取結束 關閉檔案
        uv_fs_t close_req;
        // 回調爲 NULL 同步調用
        uv_fs_close(req->loop, &close_req, ((request_t *)req->data)->fd, NULL);
        uv_fs_req_cleanup(&close_req);

        // 清理 讀寫時 uv_fs_t 內部申請資源
        uv_fs_req_cleanup(req);
        free(req->data);
    }
    else
    {
        // 讀取成功
        request_t *read_req = (request_t *)(req->data);
        read_req->buf.len = req->result;

        // 異步寫入
        uv_fs_write(req->loop, &read_req->req, 1, &read_req->buf, 1, -1, on_write);
    }
}
void post_read(uv_loop_t *loop, request_t *req)
{
    uv_fs_read(loop, &req->req, req->fd, &req->buf, 1, -1, on_read);
}
void on_open(uv_fs_t *req)
{
    if (req->result >= 0)
    {
        // 創建讀寫資源
        request_t *read_write_req = (request_t *)malloc(sizeof(request_t));
        read_write_req->req.data = read_write_req;
        read_write_req->fd = req->result;
        char *buffer = (char *)malloc(BUFFER_SIZE);
        read_write_req->buf = uv_buf_init(buffer, BUFFER_SIZE);
        read_write_req->cap = BUFFER_SIZE;

        // 異步讀取
        post_read(req->loop, read_write_req);
    }
    else
    {
        fprintf(stderr, "error opening file: %s\n", uv_strerror((int)req->result));
    }
}
int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();

    // 異步打開檔案
    uv_fs_t open_req;
    uv_fs_open(loop, &open_req, argv[1], O_RDONLY, 0, on_open);

    int result = uv_run(loop, UV_RUN_DEFAULT);
    uv_fs_req_cleanup(&open_req); // 清理 uv_fs_t 內部申請資源
    return result;
}
```

在請求結束後應該調用 uv_fs_req_cleanup 釋放掉 libuv 內部可能申請的資源

# 檔案操作

所有標誌檔案系統操作都支持異步模式，如 unlink rmdir stat，並具有直觀的參數順序。它們遵循與 讀/寫/打開 相同的調用模式，在 uv\_fs\_t.result 字段返回結果

```
int uv_fs_close(uv_loop_t* loop, uv_fs_t* req, uv_file file, uv_fs_cb cb);
int uv_fs_open(uv_loop_t* loop, uv_fs_t* req, const char* path, int flags, int mode, uv_fs_cb cb);
int uv_fs_read(uv_loop_t* loop, uv_fs_t* req, uv_file file, const uv_buf_t bufs[], unsigned int nbufs, int64_t offset, uv_fs_cb cb);
int uv_fs_unlink(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_write(uv_loop_t* loop, uv_fs_t* req, uv_file file, const uv_buf_t bufs[], unsigned int nbufs, int64_t offset, uv_fs_cb cb);
int uv_fs_copyfile(uv_loop_t* loop, uv_fs_t* req, const char* path, const char* new_path, int flags, uv_fs_cb cb);
int uv_fs_mkdir(uv_loop_t* loop, uv_fs_t* req, const char* path, int mode, uv_fs_cb cb);
int uv_fs_mkdtemp(uv_loop_t* loop, uv_fs_t* req, const char* tpl, uv_fs_cb cb);
int uv_fs_rmdir(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_scandir(uv_loop_t* loop, uv_fs_t* req, const char* path, int flags, uv_fs_cb cb);
int uv_fs_scandir_next(uv_fs_t* req, uv_dirent_t* ent);
int uv_fs_opendir(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_readdir(uv_loop_t* loop, uv_fs_t* req, uv_dir_t* dir, uv_fs_cb cb);
int uv_fs_closedir(uv_loop_t* loop, uv_fs_t* req, uv_dir_t* dir, uv_fs_cb cb);
int uv_fs_stat(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_fstat(uv_loop_t* loop, uv_fs_t* req, uv_file file, uv_fs_cb cb);
int uv_fs_rename(uv_loop_t* loop, uv_fs_t* req, const char* path, const char* new_path, uv_fs_cb cb);
int uv_fs_fsync(uv_loop_t* loop, uv_fs_t* req, uv_file file, uv_fs_cb cb);
int uv_fs_fdatasync(uv_loop_t* loop, uv_fs_t* req, uv_file file, uv_fs_cb cb);
int uv_fs_ftruncate(uv_loop_t* loop, uv_fs_t* req, uv_file file, int64_t offset, uv_fs_cb cb);
int uv_fs_sendfile(uv_loop_t* loop, uv_fs_t* req, uv_file out_fd, uv_file in_fd, int64_t in_offset, size_t length, uv_fs_cb cb);
int uv_fs_access(uv_loop_t* loop, uv_fs_t* req, const char* path, int mode, uv_fs_cb cb);
int uv_fs_chmod(uv_loop_t* loop, uv_fs_t* req, const char* path, int mode, uv_fs_cb cb);
int uv_fs_utime(uv_loop_t* loop, uv_fs_t* req, const char* path, double atime, double mtime, uv_fs_cb cb);
int uv_fs_futime(uv_loop_t* loop, uv_fs_t* req, uv_file file, double atime, double mtime, uv_fs_cb cb);
int uv_fs_lstat(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_link(uv_loop_t* loop, uv_fs_t* req, const char* path, const char* new_path, uv_fs_cb cb);
int uv_fs_symlink(uv_loop_t* loop, uv_fs_t* req, const char* path, const char* new_path, int flags, uv_fs_cb cb);
int uv_fs_readlink(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_realpath(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_fs_cb cb);
int uv_fs_fchmod(uv_loop_t* loop, uv_fs_t* req, uv_file file, int mode, uv_fs_cb cb);
int uv_fs_chown(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_uid_t uid, uv_gid_t gid, uv_fs_cb cb);
int uv_fs_fchown(uv_loop_t* loop, uv_fs_t* req, uv_file file, uv_uid_t uid, uv_gid_t gid, uv_fs_cb cb);
int uv_fs_lchown(uv_loop_t* loop, uv_fs_t* req, const char* path, uv_uid_t uid, uv_gid_t gid, uv_fs_cb cb);
```

# Buffers 和 Streams

libuv 中的基本 i/o 句柄是流(uv\_stream\_t)。 tcp套接字 udp套接字 和 檔案i/o 管道 IPC 都被視爲流子類。

流使用每個子類的自定義函數初始化，然後可使用下述函數操作：

```
int uv_read_start(uv_stream_t*, uv_alloc_cb alloc_cb, uv_read_cb read_cb);
int uv_read_stop(uv_stream_t*);
int uv_write(uv_write_t* req, uv_stream_t* handle,
             const uv_buf_t bufs[], unsigned int nbufs, uv_write_cb cb);
```

基與流的函數比檔案系統函數更易與使用，並且當 uv\_read\_start 被調用一次時，libuv 將自動從流中讀取，直到 uv_read_stop 被調用

數據的離散單元是緩衝區 uv\_buf\_t。這只是一個指向字節 uv\_buf\_t.base uv\_buf\_t.len 的指針集合。uv\_buf\_t 是輕量級的並按值傳遞，需要管理的實際字節，必須由調用者分配和釋放。