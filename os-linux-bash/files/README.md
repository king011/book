# 寫入檔案

bash 可以藉助重定向來寫入檔案

```
# 清空檔案 file
> file

# 將檔案內容替換爲 指定內容
echo "some string" > file

# 在檔案結尾添加內容
echo "foo bar baz" >> file

# -n 參數可以讓 echo 不要顯示換行
echo -n "foo bar baz" >> file
```

# 讀取檔案


```
# 讀取行第一行到變量
read -r line < file
line=$(head -1 file)

# 按行讀取檔案
i=0
while read -r line || [[ -n $line ]]; do
    echo "$i '$line'"
    i=$((i+1))
done < file

# 也可以在 read 後將值使用 IFS 分割給多個變量
while read -r field1 field2 field3 _; do
    # do something with $field1, $field2, and $field3
done < file


# 獲取檔案名
# "/" -> ""
filepath="/path/to/file.txt"
echo "${filepath##*/}"

# 獲取檔案夾名稱
# "/" -> ""
filepath="/path/to/file.txt"
echo "${filepath%/*}"

# 獲取擴展名，不包含 .
filepath="file.txt"
echo "${filepath##*.}"

# 獲取不包含擴展名的檔案名稱
filepath="file.txt"
echo "${filepath%.*}"


# 獲取檔案大小
size=$(wc -c < file)
```

> read 讀取檔案，如果檔案最後一行沒有新的換行，read 會因爲遇到 EOF 而直接結束導致最後一行被遺失，可以使用 -n 來判斷 line 是否存在內容

你也可以使用 IFS 和 cat 來將檔案映射到數組後進行讀取
```
ifs=$IFS
IFS=$'\n'
lines=(`cat a`)
IFS=$ifs
for line in "${lines[@]}"; do 
    echo "* '$line'"
done
```

# 查找檔案

linux 系統管理很多時候都是在處理檔案，find 指令提供了查找檔案的功能

是最簡單用法是直接傳入要查找的檔案夾路徑，find 會返回所有找到的內存數組

```
# 查找 /tmp/ 檔案夾下的所有內容
find /tmp/
```

也可以不傳入要查找的 path 則默認查找當前工作路徑下的內容

## -name -iname 查找名稱

-name 和 -iname 可以以通配符指示要查找的檔案名稱，區別在於 name 會區分大小寫 iname 不區分大小寫

```
# 查找 /tmp/ 檔案夾下的所有名稱以 .log 結束的內容
find /tmp/ -iname *.log
```

## -maxdepth 查找深度

find 默認會遞歸查找所有找到的檔案夾，指示 -maxdepth 可以限定查找深度

```
# 查找深度爲 1 既，只查找 /tmp/ 目錄下的內容，不進入任何子路徑
find /tmp/ -maxdepth 1 -iname *.log
```

**注意** maxdepth 參數需要寫在 name 和 iname 參數之前


## -type 查找類型

-type 標記可以限定 find 要查找的檔案內心

| 標記 | 含義 |
| -------- | -------- |
| b     | block (buffered) special     | 
| c     | character (unbuffered) special     | 
| d     | directory **檔案夾**     | 
| p     | named pipe (FIFO)     | 
| f     | regular file **常規檔案**     | 
| l     | symbolic link     | 
| s     | socket     |
| d     | door (Solaris)     |

多個類型使用 **,** 分隔

```
find -type f,d
```

## bash 遍歷

find 查找到的檔案名可能帶有空格，這在使用 for in 遍歷時會被誤認爲是兩個檔案，需要修改 IFS 進行遍歷

```
ifs=$IFS
IFS="
"
i=1
for file in `find . -maxdepth 1`;do
    echo "$i $file"
    i=$((i+1))
done
IFS=$ifs
```

在遍歷完成後需要將 IFS 還原否則可能會引起麻煩，這對某些時候會造成困擾(比如for在一個函數中 for 的實現內部可能會提前 return 函數，這很難保證 IFS 不被忘記還原)。爲此也可以先將 find 查找的結果保存到數組中，並且立刻還原 IFS 在此之後再進行遍歷

```
ifs=$IFS
IFS="
"
files=(`find . -maxdepth 1`)
IFS=$ifs

i=1
for file in "${files[@]}";do
    echo "$i $file"
    i=$((i+1))
done
```
