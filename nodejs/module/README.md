# 模塊
在 node 中 一個檔案 (可以是 js代碼 c/c++編寫的模塊) 就是 一個 模塊

使用 關鍵字 **require** 傳入模塊名 加載模塊  
require 返回 模塊對象

# 創建模塊
模塊中 會自動創建一個 exports 的 \{\}  
require 的返回值 其實 就是這個 exports 對象 

**animal.js**
```js
"use strict"
exports.Version = "0.0.1"
exports.New = function(name,lv){
	var _name = name;
	var _lv = lv;
 
	return {
		Speak:function(){
			console.log("i'm ",_name," lv =",_lv);
		},
		SetLv:function(lv){
			_lv = lv;
		},
	};
};
```
main.js
```js
"use strict"
 
//加載模塊
var animal = require('./animal');
 
console.log("version :",animal.Version);
 
var cat = animal.New("cat",10);
var dog = animal.New("dog",9);
cat.Speak();
cat.SetLv(6);
cat.Speak();
dog.Speak();
```
## module.exports
其實 模塊被導出的 是 module.exports = \{\} 只不過  
exports = module.exports

故可以直接 寫 module.exports = {} 創建 模塊
```js
"use strict"
 
module.exports = {
	Version:"0.0.1",
	New:function(name,lv){
		var _name = name;
		var _lv = lv;
 
		return {
			Speak:function(){
				console.log("i'm ",_name," lv =",_lv);
			},
			SetLv:function(lv){
				_lv = lv;
			},
		};
	},
};
```
> module.exports 不一定要是 一個 {}  
> 從技術上講 module.exports 可以是 任何 js 型別 比如一個 函數  
> require 只是簡單的將其 返回 並且避免重複加載  
> 
> 不過 一般來說 module.exports 依然只是一個 {} 

# 包
node 的 包 只是 將 模塊 進行了進一步的整合

要創建一個 包 直接創建一個 檔案夾 在裏面 創建一個 index\.js/index\.node 模塊即可

## package.json
在 包的 檔案夾下 創建 一個 package.json 的配置 可以對包進行 更詳細的 描述  
npm init 可以創建一個 package.json 檔案

* name	包名
* main	包入口檔案
* ...

```json
{
  "name": "animal",
  "version": "1.0.0",
  "description": "",
  "main": "./lib/animal.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "king",
  "license": "GPL-3.0"
}
```