# typedoc

typedoc 是一個開源的(Apache-2.0)第三方工具，用於爲 typescript 生成 html 文檔 或 json 模型

* 官網 [https://typedoc.org/](https://typedoc.org/)
* 源碼 [https://github.com/TypeStrong/typedoc](https://github.com/TypeStrong/typedoc)

```
# Install
npm install typedoc

# Execute typedoc on your project
npx typedoc src/index.ts
```