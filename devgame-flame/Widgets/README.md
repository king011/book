# [Widgets](https://docs.flame-engine.org/1.16.0/flame/other/widgets.html)

使用 Flutter 開發遊戲時的一個優勢是能夠使用 Flutter 的廣泛工具集來建立 UI，Flame 試圖透過引入專為遊戲而製作的小部件來擴展這一點。

在這裡您可以找到 Flame 提供的所有可用小工具。

您也可以在[此處](https://github.com/flame-engine/flame/tree/main/examples/lib/stories/widgets)查看 [Dashbook](https://github.com/bluefireteam/dashbook) 沙箱內展示的所有小部件

# [NineTileBoxWidget](https://docs.flame-engine.org/1.16.0/flame/other/widgets.html#ninetileboxwidget)

九塊方塊(Nine Tile Box )是使用網格精靈繪製的矩形

網格精靈是一個 3x3 的網格，有 9 個區塊，分別代表 4 個角落、4 個邊和中間

角部以相同的尺寸繪製，邊緣朝側面拉伸，中間雙向擴展

NineTileBoxWidget 使用該標準實作了一個容器。 此模式也作為 NineTileBoxComponent 中的元件實現，以便您可以將此功能直接添加到 FlameGame 中。 要了解更多信息，請查看[此處](https://docs.flame-engine.org/1.16.0/flame/components.html#ninetileboxcomponent)的組件文件

在這裡您可以找到如何使用它的範例（不使用 NineTileBoxComponent）：

```
import 'package:flame/widgets';

NineTileBoxWidget(
    image: image, // dart:ui image instance
    tileSize: 16, // The width/height of the tile on your grid image
    destTileSize: 50, // The dimensions to be used when drawing the tile on the canvas
    child: SomeWidget(), // Any Flutter widget
)
```

# [SpriteButton](https://docs.flame-engine.org/1.16.0/flame/other/widgets.html#spritebutton)

SpriteButton 是一個簡單的 widget，它創建基於 Flame 精靈的按鈕。 當嘗試創建非預設外觀的按鈕時，這非常有用。 例如，當您透過在圖形編輯器中繪製按鈕而不是直接在 Flutter 中繪製按鈕來更輕鬆地實現您想要的外觀

如何使用它：

```
SpriteButton(
    onPressed: () {
      print('Pressed');
    },
    label: const Text('Sprite Button', style: const TextStyle(color: const Color(0xFF5D275D))),
    sprite: _spriteButton,
    pressedSprite: _pressedSprite,
    height: _height,
    width: _width,
)
```

# [SpriteWidget](https://docs.flame-engine.org/1.16.0/flame/other/widgets.html#spritewidget)

SpriteWidget 是一個用於在 widget 樹中顯示 Sprite 的小部件

如何使用它：

```
SpriteWidget(
    sprite: yourSprite,
    anchor: Anchor.center,
)
```

# [SpriteAnimationWidget](https://docs.flame-engine.org/1.16.0/flame/other/widgets.html#spriteanimationwidget)

SpriteAnimationWidget 是一個用於在 widget 樹中顯示 SpriteAnimations 的小部件

如何使用它：

```
SpriteAnimationWidget(
    animation: _animation,
    animationTicker: _animationTicker,
    playing: true,
    anchor: Anchor.center,
)
```