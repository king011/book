# [動態卷供應](https://kubernetes.io/docs/concepts/storage/dynamic-provisioning/)

動態卷供應允許安需創建存儲卷。

# 啓用動態卷供應

要啓動動態供應，需要預先創建一個或多個 StorageClass 。StorageClass 定義類當動態供應被調用時，哪一個驅動將被使用和哪些參數將被傳遞給驅動。以下清單創建類一個 StorageClass 存儲類 slow，它提供類似標準磁盤的永久磁盤

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: slow
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
```

以下清單創建類一個 fast 存儲類，它提供 SSD 的永久磁盤

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: fast
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-ssd
```

# 使用動態卷供應

通過在 PVC 中 包含 存儲類來請求動態供應的存儲

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: claim1
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: fast
  resources:
    requests:
      storage: 30Gi
```

# 設置默認值的行爲

可以在集羣上啓用動態供應，以便在未定義存儲類的情況下動態設置所有聲明。可以通過以下方式啓用此行爲

* 標記一個 StorageClass 爲 默認
* 確認  DefaultStorageClass 准如控制器  在 API 服務端被啓用

可以提供向其添加 storageclass.kubernetes.io/is-default-class 註解來將特定 StorageClass 標記爲默認。當集羣中存在默認的 StorageClass 並且用戶創建類一個未指定 storageClassName 的 PVC 時，DefaultStorageClass 准如控制器會自動向其中添加指向默認存儲類的 storageClassName 字段

> 集羣上最多只能有一個 默認存儲類，否則無法創建沒有明確指定 storageClassName 的 PVC