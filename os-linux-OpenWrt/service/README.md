# 服務

OpenWrt 使用自己的 服務腳本  所有服務腳本需要放到 **/etc/init.d/** 下面

並且 OpenWrt 提供了 **/etc/rc.common** 來簡化服務腳本的編寫

```
#!/bin/sh /etc/rc.common
# Copyright (C)2006 OpenWrt.org
START=50
start() {
    [ -d /www ] && httpd -p 80 -h /www-r OpenWrt
}
stop() {
    killall httpd
}
```

* 第一行 使用 rc.common 分析 參數
* START 定義 服務開機時的啓動順序 越大 越後啓動
* start() 定義 啓動 命令
* stop() 定義 關閉 命令


# rc.common

rc.common 提供了多個 參數 映射到服務腳本

* start  Start the service
* stop   Stop the service
* restart Restart the service 
* reload Reload configuration files (or restart if that fails) 
*  enable Enable service autostart 
*  disable Disable service autostart       

enable/disable 通過向 /etc/rc.d 下創建/刪除 連接 來完成
