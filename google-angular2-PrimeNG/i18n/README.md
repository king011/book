# [i18n](https://primeng.org/i18n)

PrimeNG 提供了一個 PrimeNGConfig 服務，使用它的 setTranslation 可以修改組件默認顯示的文本

```
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {

    constructor(private config: PrimeNGConfig) {}

    ngOnInit() {
        this.config.setTranslation({
            accept: 'Accept',
            reject: 'Cancel',
            //translations
        });
    }
}
```

# ngx-translate

PrimeNG 可以很好的和 ngx-translate 一起工作

```
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {

    constructor(private config: PrimeNGConfig, private translateService: TranslateService) {}

    ngOnInit() {
        this.translateService.setDefaultLang('en');
    }

    translate(lang: string) {
        this.translateService.use(lang);
        this.translateService.get('primeng').subscribe(res => this.config.setTranslation(res));
    }
}
```

```
#info="en.json"
{
    "primeng": {
        "startsWith": "Starts with",
        "contains": "Contains",
        "notContains": "Not contains",
        "endsWith": "Ends with",
        "equals": "Equals",
        "notEquals": "Not equals",
        "noFilter": "No Filter",
        "lt": "Less than",
        "lte": "Less than or equal to",
        "gt": "Greater than",
        "gte": "Greater than or equal to",
        "is": "Is",
        "isNot": "Is not",
        "before": "Before",
        "after": "After",
        "dateIs": "Date is",
        "dateIsNot": "Date is not",
        "dateBefore": "Date is before",
        "dateAfter": "Date is after",
        "clear": "Clear",
        "apply": "Apply",
        "matchAll": "Match All",
        "matchAny": "Match Any",
        "addRule": "Add Rule",
        "removeRule": "Remove Rule",
        "accept": "Yes",
        "reject": "No",
        "choose": "Choose",
        "upload": "Upload",
        "cancel": "Cancel",
        "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        "dayNamesMin": ["Su","Mo","Tu","We","Th","Fr","Sa"],
        "monthNames": ["January","February","March","April","May","June","July","August","September","October","November","December"],
        "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        "dateFormat": "mm/dd/yy",
        "firstDayOfWeek": 0,
        "today": "Today",
        "weekHeader": "Wk",
        "weak": "Weak",
        "medium": "Medium",
        "strong": "Strong",
        "passwordPrompt": "Enter a password",
        "emptyMessage": "No results found",
        "emptyFilterMessage": "No results found"
    }
}
```