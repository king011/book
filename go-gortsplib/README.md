# gortsplib

gortsplib 是使用 golang 實現的一個開源(MIT) rtsp 服務器/客戶端 庫

* 源碼 [https://github.com/aler9/gortsplib](https://github.com/aler9/gortsplib)

```
package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/aler9/gortsplib/v2/pkg/format"
	"github.com/aler9/gortsplib/v2/pkg/media"
	"github.com/aler9/gortsplib/v2/pkg/url"
	"github.com/pion/rtp"

	"github.com/aler9/gortsplib/v2"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var (
		help bool
		uri  string
	)
	flag.StringVar(&uri, `uri`, ``, `rstp server uri`)
	flag.BoolVar(&help, `help`, false, `display help`)
	flag.Parse()
	if help {
		flag.PrintDefaults()
		return
	} else {
		// 這裏不是 net/url，而是 gortsplib 提供的 url
		u, e := url.Parse(uri)
		if e != nil {
			log.Fatalln(e)
		}
		runClient(u)
	}
}
func runClient(u *url.URL) {
	// 創建客戶端
	var c gortsplib.Client
	// 初始化與服務器的連接
	e := c.Start(u.Scheme, u.Host)
	if e != nil {
		log.Fatalln(e)
	}
	defer c.Close()

	// 查詢媒體
	medias, baseURL, _, e := c.Describe(u)
	if e != nil {
		log.Fatalln(e)
	}
	for _, media := range medias {
		fmt.Println(media)
	}

	// rtsp 需要在播放前爲媒體調用 setup
	// setup all medias
	e = c.SetupAll(medias, baseURL)
	if e != nil {
		log.Fatalln(e)
	}
	f, e := os.Create("a.mp3")
	if e != nil {
		log.Fatalln(e)
	}
	defer f.Close()

	// 當收到 rtp 包時回調，不用擔心 tcp/udp 庫會負責處理並爲 udp 重新排序後最終回調到這個供上層處理
	c.OnPacketRTPAny(func(medi *media.Media, forma format.Format, pkt *rtp.Packet) {
		log.Printf("RTP packet from media %v %vbytes\n", medi, len(pkt.Payload))
	})

	// 開始播放
	_, e = c.Play(nil)
	if e != nil {
		log.Fatalln(e)
	}

	// 等待流播放 直到出錯
	log.Println(c.Wait())
}
```