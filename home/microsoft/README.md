# 微軟
由 microsoft 提供/主導 或依賴微軟 的 服務

[cmd](https://learn.microsoft.com/zh-tw/windows-server/administration/windows-commands/windows-commands)

# 操縱系統
* [wsl](ms-wsl/0)
* [windows](ms-windows/0)
* [常用工具](ms-os-tools/0)
* [windows api](ms-os-winapi/0)
# 程式開發

* [msys2](ms-msys2/0) 開發環境
* [Winsock](ms-Winsock/0) 網路編程

# 檔案編輯
* [vscode](ms-vscode/0)
    * [vscode 插件開發](ms-vscode-plugins/0)
    * [vscode 插件](ms-vscode-plugins-use/0)
# TypeScript
* [TypeScript](ms-ts/0)
* [TSDoc](ms-TSDoc/0)
* [typedoc](ms-typedoc/0)
# C＃
* [c #](ms-csharp/0)