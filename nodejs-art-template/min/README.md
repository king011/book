# 壓縮頁面

art-template 內建的壓縮器可以壓縮 html js css ，它在編輯階段完成，因此完全不會影響渲染速度，並可加快網路傳輸。

```
template.defaults.minimize = true;
```

# 默認配置
```
template.defaults.htmlMinifierOptions = {
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true,
    // 运行时自动合并：rules.map(rule => rule.test)
    ignoreCustomFragments: []
};
```