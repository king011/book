# Angular
Angular 是google在AngularJS之上 於2016年 重新開發設計的 前端框架  
主要使用 TypeScript 開發

* 官網 [https://angular.io/](https://angular.io/)
* 文檔 [https://angular.io/guide/quickstart](https://angular.io/guide/quickstart)
* 中文文檔 [https://angular.cn/docs](https://angular.cn/docs)
* github [https://github.com/angular/angular](https://github.com/angular/angular)

# 安裝
1. 安裝好 node 和 npm
2. 安裝 Angular 工具 `npm install -g @angular/cli`

# 常用命令
```sh
# 創建新項目
ng new --strict XXX

# 更新 項目 到最新版本的 Angular
ng update
# 更新 cli 工具到最新 版本
ng update @angular/cli

# 運行項目 並關閉 域名檢測
ng serve --disable-host-check
```

## --strict

--strict 是 v10 新增的參數 會爲項目配置 嚴格選項

1. typescript 中啓用嚴格模式
2. 將模板型別檢查設置爲 Strict
3. 將默認包預算減少約75%
4. 配置 linting 規則 防止聲明 any
5. 將應用配置爲 side-effect-free 以實現更高級的 tree-shaking 優化