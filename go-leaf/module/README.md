# 模塊

leaf 由多個模塊組成

* 每個模塊運行在一個單獨的 goroutine 中
* 模塊間通過 chanrpc 進行通信

```
leaf.Run(
	game.Module,
	gate.Module,
	login.Module,
)
```

將所有模塊傳遞給 leaf.Run 即可啓動遊戲服務器

模塊需要實現 如下接口
```
type Module interface {
	OnInit()
	OnDestroy()
	Run(closeSig chan bool)
}
```

1. leaf 會在初始 goroutine中 按註冊順序執行各模塊的 OnInit
2. 等待所有模塊OnInit執行完畢 leaf會爲每個模塊啓動一個單獨的 goroutine 並行 運行各模塊的 Run
3. 當結束遊戲時(CTRL+C) leaf 會在同個goroutine中按照註冊模塊相反順序 執行 OnDestroy

# package message

通常會爲 leaf 定義一個 message 包 來管理遊戲通信用到的網路消息 同時爲全遊戲指定一個 network.Processor 來處理網路消息


```
#info="github.com/name5566/leaf/network/processor.go"
package network

type Processor interface {
	// must goroutine safe
	Route(msg interface{}, userData interface{}) error
	// must goroutine safe
	Unmarshal(data []byte) (interface{}, error)
	// must goroutine safe
	Marshal(msg interface{}) ([][]byte, error)
}
```

```
#info="server/message"
package message

import "github.com/name5566/leaf/network/json"

// Processor 使用 leaf 提供的 json 處理器來處理網路消息
var Processor = json.NewProcessor()

func init() {
	// 註冊消息 名稱爲 型別名 Hello
	Processor.Register(&Hello{})
}

// A Hello 定義一個 網路消息
type Hello struct {
	Name string
}
```

# game 

通常需要爲遊戲服務器 定義多個 模塊來處理 網路請求 此處定義了一個 game 模塊來充當此角色

```
package game

import (
	"fmt"
	"reflect"
	"server/base"
	"server/message"

	"github.com/name5566/leaf/gate"
	"github.com/name5566/leaf/module"
)

var (
	// 創建遊戲 骨架
	skeleton = base.NewSkeleton()
	// ChanRPC 導出 ChanRPC 以供模塊間通信
	ChanRPC = skeleton.ChanRPCServer
)

// A Module 定義一個 遊戲模塊
type Module struct {
	// 嵌入 leaf 骨架 已經實現了 RUn
	*module.Skeleton
}

// OnInit 初始化 模塊
func (m *Module) OnInit() {
	m.Skeleton = skeleton

	// 爲模塊註冊 消息處理器
	skeleton.RegisterChanRPC("NewAgent", m.newAgent)
	skeleton.RegisterChanRPC("CloseAgent", m.closeAgent)

	// 註冊 hellow 消息處理
	skeleton.RegisterChanRPC(reflect.TypeOf(&message.Hello{}), m.hello)
}

// OnDestroy 銷毀模塊
func (*Module) OnDestroy() {

}
func (*Module) newAgent(args []interface{}) {
	a := args[0].(gate.Agent)
	fmt.Println(`one in`, a.RemoteAddr())
}
func (*Module) closeAgent(args []interface{}) {
	a := args[0].(gate.Agent)
	fmt.Println(`one out`, a.RemoteAddr())
}
func (*Module) hello(args []interface{}) {
	// 收到的 Hello 消息
	m := args[0].(*message.Hello)
	// 消息發送者
	a := args[1].(gate.Agent)
	fmt.Println(a.RemoteAddr(), "hello", m.Name)
	// 回覆消息
	a.WriteMsg(&message.Hello{
		Name: "success",
	})
}
```

```
package base

import (
	"github.com/name5566/leaf/chanrpc"
	"github.com/name5566/leaf/module"
)

// NewSkeleton 創建一個輔助函數用於創建遊戲骨架
func NewSkeleton() *module.Skeleton {
	skeleton := &module.Skeleton{
		GoLen:              0,
		TimerDispatcherLen: 0,
		AsynCallLen:        0,
		ChanRPCServer:      chanrpc.NewServer(0),
	}
	skeleton.Init()
	return skeleton
}
```

# gate

通常 leaf 需要一個 gate 模塊 用於 處理 連接 以及設置消息的處理模塊

leaf 已經實現了一個 gate 模塊 用於處理 連接 直接 將其嵌入到自己的 gate 模塊即可

```
package gate

import (
	"math"
	"server/game"
	"server/message"

	"github.com/name5566/leaf/gate"
)

func init() {
	// 設置消息路由到的目標模塊
	message.Processor.SetRouter(&message.Hello{}, game.ChanRPC)
}

// A Module impl leaf module
type Module struct {
	*gate.Gate
}

// OnInit .
func (m *Module) OnInit() {
	m.Gate = &gate.Gate{
		MaxConnNum:      20000,
		PendingWriteNum: 100,
		MaxMsgLen:       math.MaxUint16, // 最大消息長度
		WSAddr:          "",
		HTTPTimeout:     0,
		CertFile:        "",
		KeyFile:         "",
		TCPAddr:         "127.0.0.1:9000",
		LenMsgLen:       2, // 消息長度使用 2 字節表示
		LittleEndian:    false,
		Processor:       message.Processor, // 指定消息處理器
		AgentChanRPC:    game.ChanRPC,      // 將 連接短信消息 路由給 game 模塊
	}
}

// OnDestroy .
func (m *Module) OnDestroy() {

}
```

# main

將 所有模塊 傳入 leaf.Run 即可 運行 leaf 服務器

```
package main

import (
	"server/game"
	"server/gate"

	"github.com/name5566/leaf"
)

func main() {
	leaf.Run(
		&game.Module{},
		&gate.Module{},
	)
}
```

# 消息包

leaf 消息使用 len+data 的形式 len 指示了data的長度

```
package main

import (
	"encoding/binary"
	"fmt"
	"log"
	"net"
)

func main() {
	c, e := net.Dial(`tcp`, "127.0.0.1:9000")
	if e != nil {
		log.Fatalln((e))
	}
	defer c.Close()
	b := make([]byte, 1024)

	// 發送一個 Hello 請求
	data := []byte(`{
	"Hello": {
		"Name": "leaf"
	}
}`)
	binary.BigEndian.PutUint16(b, uint16(len(data)+2))
	copy(b[2:], data)
	_, e = c.Write(b[:len(data)])
	if e != nil {
		log.Fatalln(e)
	}
	n, e := c.Read(b)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(string(b[:n]))
}
```