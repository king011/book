# IoT.js

IoT.js 是三星公司開源(Apache 2.0)的一個物聯網平臺，它使用 jerryscript 一個爲物聯網優化的 javascript 引擎(更少的內存佔用，但速度更慢)兼容 nodejs 模塊

* 官網 [https://iotjs.net/](https://iotjs.net/)
* 源碼 [https://github.com/jerryscript-project/iotjs](https://github.com/jerryscript-project/iotjs)