# stream

stream 庫 提供了 數據流操作相關實現

# Writable 

Writable 是一個 可寫流 定義了 可寫流的 基本接口 其接收一個 WritableOptions 進行構造

```
#info="WritableOptions"
interface WritableOptions {
		highWaterMark?: number;
		decodeStrings?: boolean;
		defaultEncoding?: string;
		objectMode?: boolean;
		emitClose?: boolean;
		write?(this: Writable, chunk: any, encoding: string, callback: (error?: Error | null) => void): void;
		writev?(this: Writable, chunks: Array<{ chunk: any, encoding: string }>, callback: (error?: Error | null) => void): void;
		destroy?(this: Writable, error: Error | null, callback: (error: Error | null) => void): void;
		final?(this: Writable, callback: (error?: Error | null) => void): void;
		autoDestroy?: boolean;
}
```

extends Writable 即可實現一個 自己的 可寫流
```
import { Writable } from 'stream'

// 要實現一個可寫流 只需要 從 Writable 派生即可
class StringBuffer extends Writable {
    private data_ = new Array<Buffer>()
    constructor() {
        super({
            highWaterMark: 16384, // write 返回 false 時的緩衝區大小 默認16k 對象默認16個
            decodeStrings: true,// 是否將 string 編碼爲 Buffer
            defaultEncoding: 'utf8',// 參數默認的字符編碼
            objectMode: false, // 是否可以 write string Buffer Uint8Array 之外的型別
            emitClose: true, // 流銷毀時 是否除非 close 事件
            autoDestroy: false, //流是否應該自動 調用 destroy() 
            // 必須實現 write 或 writev 來 將 數據寫到底層目標
            write(this: StringBuffer, chunk: any, encoding: string, callback: (error?: Error | null) => void): void {
                if (this.data_.length >= 2) {
                    callback(new Error('full')) // 異常使用 Error 通知 如果 autoDestroy 爲 true 會自動 destroy 流
                } else {
                    this.data_.push(chunk)
                    callback()
                }
            },
            writev(this: StringBuffer, chunks: Array<{ chunk: any, encoding: string }>, callback: (error?: Error | null) => void): void {
                if (this.data_.length + chunks.length >= 2) {
                    if (chunks.length > 1) {
                        callback(new Error('limit'))
                    } else {
                        callback(new Error('full'))
                    }
                } else {
                    for (let i = 0; i < chunks.length; i++) {
                        this.data_.push(chunks[i].chunk)
                    }
                    callback()
                }
            },
            destroy(this: StringBuffer, error: Error | null, callback: (error: Error | null) => void): void {
                console.log(`buffer destroy`)
                callback(null)
            },
            final(this: StringBuffer, callback: (error?: Error | null) => void): void {
                // 當結束寫入所有剩餘數據時調用
                console.log(`buffer final`)
                callback(null)
            }
        })
    }
    toString(): string {
        return this.data_.join()
    }
}
const buffer = new StringBuffer()

buffer.on('close', () => {
    // 關閉時回調
    console.log('close')
}).on('drain', () => {
    // 緩存全部寫入到 目標後回調
    console.log('drain')
}).on('error', (e) => {
    // 發生錯誤後 回調 會使用 write 等函數不拋出異常
    console.log('error', (e as Error).message)
}).on('finish', () => {
    // end調用後 
    // 將緩衝數據發送給 底層後 回調
    console.log('finish')
})

console.log('w1', buffer.write('1',
    () => {
        console.log(`w1 success`)
    }),
)
console.log('w2', buffer.write('2'))
try {
    console.log('w3', buffer.write('3'))
} catch (e) {
    console.log('write w3 error : ', (e as Error).message)
}
console.log('str', buffer.toString())


// 手動銷毀流
buffer.destroy()
console.log('w5', buffer.write('5'))

// 通知 所有數據已寫完
buffer.end()
console.log('w4', buffer.write('4'))
```

* 當 write 返回 false node 會把數據加入到緩存中 緩存大小只受限與內存 此時應該等待 drain 事件通知 代表所有緩存已經清空 之後再進行 write 操作 (否則很容忍受到惡意攻擊)
* destroy 調用 close 才會被通知
* end 調用 finish 才會被通知