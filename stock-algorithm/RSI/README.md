# RSI

Relative Strength Index 一種比較價格升降運動以表達價格強度的技術分析工具 簡寫 RSI

RSI 在 1978年6月由 Welles Wilder JR 提出

# 計算方法

1. 設每天向上變得爲U向下 變得爲D
2. 在價格上升的日子 U=今日收盤價-昨日收盤價;D=0
3. 在價格下跌的日子 U=0;D=昨日收盤價格-今日收盤價
4. U和D需要計算n日的 指數平均數

![](assets/rs.svg)

![](assets/rsi.svg)

```
package main

import (
	"fmt"
)

func main() {
	// 模擬數據
	values := []float32{
		43.34, 42.02, 42.19, 41.85, 43.29, 43.93, 44.86, 45.53, 46.02, 44.81, 48.15, 51, 50.95, 51.4, 51.04, 52.52, 51.5, 51.95, 51.2, 51.12, 52.28, 51.72, 50.85, 51.65, 51.06, 49.3, 49.58, 50.29, 50.34, 51.42, 52.93, 52.89, 53.35, 53.76, 52.86, 52.12, 50.89, 51.04, 50.14, 50.39, 50.01, 50.87, 52.52, 51.27, 51.29, 51.67, 52.08, 51.66, 51.73, 50.14, 50.22, 51.46, 51.84, 50.15, 52.2, 52.65, 49.45, 50.88, 50.32, 51.97, 50.55, 50.35, 51.1, 50.29, 49.58, 52.68, 50.81, 52.95, 52.06, 52.92, 53.04, 53, 54.45, 52.98, 52.25, 48.39, 55.86, 59.5, 61.24, 58.86, 55.65, 53.34, 50.84, 49.45, 47.91, 49.16, 52.26, 48.17, 44.1, 45, 45.47, 44.43, 45.67, 41.92, 42.06, 40.6, 42.35, 44.07, 46.32, 44.64, 42.95, 43.95, 45.12, 45.52, 48.56, 50.26, 47.21, 46.56, 43.1, 42.46, 42.24, 41.57, 44.01, 45.45, 40.52, 42.86, 45.42, 46.68, 47.41, 44.26, 41.25, 41.12, 37.49, 39.23, 38.51, 42.12, 44.08, 43.8, 46.02, 41.86, 40.66, 35.39, 38.06, 39.64, 36.43, 40.25, 41.19,
	}
	fmt.Println(`length`, len(values))
	// 計算10天的 rsi
	count := 10
	items := make([]float32, count)
	// 計算 U D
	udCount := EMAK(14, count)
	u := make([]float32, udCount)
	d := make([]float32, udCount)
	fmt.Println(`valuesCount`, udCount+1)

	for i := 0; i < udCount; i++ {
		if values[i] > values[i+1] {
			u[i] = values[i] - values[i+1]
		} else {
			d[i] = values[i+1] - values[i]
		}
	}
	// 計算 RSI
	for i := 0; i < count; i++ {
		emad := EMA(d[i:], 14)
		if emad == 0 {
			continue
		}
		emau := EMA(u[i:], 14)
		rs := emau / emad
		items[i] = 100 - 100/(1+rs)
	}

	// 打印 ema
	for i := 0; i < count; i++ {
		fmt.Println(i, items[i])
	}
}

// EMAK 返回 最少需要多少 數據源
func EMAK(days, count int) int {
	return int(3.45*float32(days+1)) + 1 + count - 1
}

// EMA 計算指數平均數
func EMA(values []float32, days int) (value float32) {
	if days < 2 {
		panic(`days < 2`)
	}
	// 保證至少追溯到 3.45*(N+1) 天
	k := int(3.45*float32(days+1)) + 1
	if len(values) < k {
		panic(fmt.Sprint(`length < k `, k))
	}
	// 計算 1-α
	subAlpha := float32(1) - float32(2)/float32(days+1)
	// 緩存 pow 結果
	var pow float32 = 1
	// 保存 分母
	var denominator float32 = 1.0
	// 保存 分子
	var numerator float32 = 0
	for i := 0; i < k; i++ {
		if i == 0 {
			// 分子 p1
			numerator = values[i]
			// 分母 1
			//denominator = 1
		} else {
			pow *= subAlpha
			// 分子 previous + pn*pow(subAlpha,n)
			numerator += values[i] * pow
			// 分母 previous + pow(subAlpha,n)
			denominator += pow
		}
	}
	return numerator / denominator
}
```