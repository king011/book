# 定義

* 中軌 = N時間段 簡單移動平均數
* 上軌 = 中軌 + K * N時間段的 標準差
* 下軌 = 中軌 - K * N時間段的 標準差

一般情況下 設定 N = 20 和 K = 2 ， 在日線裏 N = 20 其實是 月均線，依據 常態分佈規則 約有 95% 的數值會分佈在 距離平均值 正負2個標準差內

```
package main

import (
	"fmt"
	"math"
)

const (
	// N 布林帶 時間段
	N = 20
	// K 布林帶 K個標準差
	K = 2
)

func main() {
	// 模擬 股價
	vals := make([]float32, N)
	for i := 0; i < N; i++ {
		// now - i 的股價
		vals[i] = float32(i) + 100
	}

	// 中軌
	var middle float32
	for _, val := range vals {
		middle += val
	}
	middle /= N

	// 算標準差
	var diff, standard float32
	for _, val := range vals {
		diff = val - middle
		standard += diff * diff
	}
	standard = float32(math.Sqrt(float64(standard / N)))

	// 上下軌
	high := middle + K*standard
	low := middle - K*standard

	// %b
	pb := (vals[0] - low) / (high - low)
	// 帶寬指標
	bw := (high - low) / middle

	fmt.Println("上軌 :", high)
	fmt.Println("中軌 :", middle)
	fmt.Println("下軌 :", low)
	fmt.Println("PB :", pb)
	fmt.Println("BW :", bw)
}
```
# 移動平均數
移動平均數 就是 最近 N 時間段 的股價收盤 平均值 

# 標準差
1. 標準差 需要先 求出方差 ，方差開方便得到 標準差
2. 要得到方差，首先獲取每個樣本和移動平均數的差值，然後平方，在求這些平方的平均數

# %b 指標
Percent b 簡寫 PB 是以數字形式呈現收盤價在布林帶中的位置

* 當 %b &gt; 1 表示 收盤價位於 布林帶之上 
* 當 %b = 1 時 表示 收盤價位於 布林帶上軌
* 當 %b = 0.5 時 表示 收盤價位於 布林帶中間 
* 當 %b = 0 時 表示 收盤價位於 布林帶下軌
* 當 %b &lt; 0 表示 收盤價位於 布林帶之下


%b = (收盤價 - 布林帶下軌值) / (布林帶上軌值 - 布林帶下軌值)

# 帶寬指標

Bandwidth 簡寫 BW 反映了上下軌 與 股價平均成本的 比率 例如 0.3 代表了 上下軌幅度爲股價平均成本的30% 

BW 值越大 代表幅度相對平均成本比率越大，值越小 代表幅度相對平均成本比率越小

帶寬指標 = (布林帶上軌值 − 布林帶下軌值) / 布林帶中軌值