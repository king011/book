# webapi

piwigo 提供了一組 web api 供第三方應用調用，下文假設使用 json 作爲響應數據，並且已經設置了如下環境變量

* PiwigoURL: 你的 piwigo 網址
* PiwigoUser: 登入用戶名
* PiwigoPassword: 登入密碼
* PiwigoCookie: 包含 session 的 cookie 值以供需要登入的接口使用

此外也可以到此 [http://vegephoteque.fr/tools/ws.htm](http://vegephoteque.fr/tools/ws.htm) 查詢api使用說明 
# pwg.session.xxx

pwg.session.xxx 提供了 session 相關操作

## pwg.session.login

此接口用於登入，需要傳入用戶名和登入密碼:

```
curl -X POST \
	-d "username=$PiwigoUser" \
	-d "password=$PiwigoPassword" \
	"$PiwigoURL/ws.php?format=json&method=pwg.session.login" 
```

成功時返回
```
{"stat":"ok","result":true}
```
失敗返回
```
{"stat":"fail","err":999,"message":"Invalid username\/password"}
```

## pwg.session.getStatus

返回當前 session 信息。還提供了一個可用於管理方法的令牌

```
curl -X GET \
	-H "Cookie: $PiwigoCookie" \
	"$PiwigoURL/ws.php?format=json&method=pwg.session.getStatus" 
```

已經登入返回
```
{"stat":"ok","result":{"username":"king","status":"webmaster","theme":"modus","language":"zh_TW","pwg_token":"5792b4aa09cd9931d8a73688e920a909","charset":"utf-8","current_datetime":"2022-03-28 17:24:57","version":"12.2.0","available_sizes":["square","thumb","2small","xsmall","small","medium","large","xlarge","xxlarge"],"upload_file_types":"jpg,jpeg,png,gif,tiff,tif,mpg,zip,avi,mp3,ogg,pdf,ogv,mp4,m4v,webm,webmv,strm","upload_form_chunk_size":500}}
```
否則返回
```
{"stat":"ok","result":{"username":"guest","status":"guest","theme":"modus","language":"zh_TW","pwg_token":"b901366f308055c565b0333ca4a380da","charset":"utf-8","current_datetime":"2022-03-28 17:26:30","version":"12.2.0","available_sizes":["square","thumb","2small","xsmall","small","medium","large","xlarge","xxlarge"]}}
```

## pwg.session.logout

用於註銷當前 session

```
curl -X POST \
	-H "Content-Type: application/x-www-form-urlencoded" \
	-H "Cookie: $PiwigoCookie" \
	"$PiwigoURL/ws.php?format=json&method=pwg.session.logout" 
```

成功返回
```
{"stat":"ok","result":true}
```
似乎不會返回失敗

# pwd.categories.xxx

pwg.categories.xxx 提供了 相冊 相關操作

## pwg.categories.getList

```
curl -X GET \
	-H "Cookie: $PiwigoCookie" \
	"$PiwigoURL/ws.php?format=json&method=pwg.categories.getList" 
```