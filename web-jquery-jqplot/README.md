# jqplot

jqplot 是一個開源(MIT or GPL2) 的 jquery 圖表插件

支持 餅圖 折線 柱圖

* 官網 [http://www.jqplot.com/index.php](http://www.jqplot.com/index.php)
* document [http://www.jqplot.com/docs/files/usage-txt.html](http://www.jqplot.com/docs/files/usage-txt.html)
* example [http://www.jqplot.com/examples/](http://www.jqplot.com/examples/)
* 源碼 [https://github.com/jqPlot/jqPlot/](https://github.com/jqPlot/jqPlot/)

![](assets/1.png)

![](assets/2.png)

![](assets/3.png)