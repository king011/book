# 選項菜單

select 類似html select 但爲其提供了 material 風格 同時 支持 ngModel 和 formControlName 之類的表單 綁定功能

```
import {MatSelectModule} from '@angular/material/select';
```


使用 mat-select 和 mat-option 定義 選項 類似 html的 select 和 option

```html
<mat-form-field>
  <mat-select formControlName="interest">
    <mat-option *ngFor="let item of interestList" [value]="item.id">{{ item.name }}</mat-option>
  </mat-select>
</mat-form-field>
```

![](assets/01-mat-select-basic.gif)

# 設定 placeholder 屬性

placeholder 屬性 可以爲選項菜單 提供一個 label/placeholder

```html
<mat-form-field>
  <mat-select formControlName="interest" placeholder="你的休閒愛好?">
    <mat-option *ngFor="let item of interestList" [value]="item.id">{{ item.name }}</mat-option>
  </mat-select>
</mat-form-field>
```

![](assets/02-mat-select-placeholder.png)

# 設定 disabled

爲 mat-select 設定 disabled 屬性 可以 禁用 選項菜單

```html
<mat-form-field>
  <mat-select formControlName="interest" placeholder="你的休閒愛好?" disabled>
    <mat-option *ngFor="let item of interestList" [value]="item.id">{{ item.name }}</mat-option>
  </mat-select>
</mat-form-field>
```
![](assets/03-disabled-mat-select.png)

爲 mat-option 設定 disabled 屬性 可以 某個選項

```html
<mat-form-field>
  <mat-select formControlName="interest" placeholder="你的休閒愛好?">
    <mat-option *ngFor="let item of interestList; let index = index" [value]="item.id" [disabled]="index === 1">{{ item.name }}</mat-option>
  </mat-select>
</mat-form-field>
```

![](assets/04-disabled-mat-option.png)

# 還原 mat-select 選取狀態

目前 選擇 mat-option 後 無法取消目前的 選取狀態 這會使得選項非必填無法 取消 此時可以加入一個沒有設定的 value(\[value\]=&quot;undefined&quot;) 的 mat-option 當選擇這個項時 就會自動跳回沒有選擇項的模式

```html
<mat-form-field>
  <mat-select formControlName="interest" placeholder="你的休閒愛好?">
    <mat-option [value]="undefined">無</mat-option>
    <mat-option *ngFor="let item of interestList; let index = index" [value]="item.id" [disabled]="index === 1">{{ item.name }}</mat-option>
  </mat-select>
</mat-form-field>
```

![](assets/05-reset-select.gif)

# mat-optgroup 分組

可以使用 mat-optgroup 將 選項 分到不同的 組別中去 並且 mat-optgroup 也支持 **disabled** 屬性來禁用整個組

```html
<mat-select formControlName="interest" placeholder="你的休閒愛好?">
  <mat-option [value]="undefined">無</mat-option>
  <mat-optgroup *ngFor="let parent of nestInterestList; let index = index" [label]="parent.name" [disabled]="index===1">
    <mat-option *ngFor="let child of parent.subItems" [value]="child.id">{{ child.name }}</mat-option>
  </mat-optgroup>
</mat-select>
```

![](assets/06-mat-optgroup.gif)

# 設定 multiple 多選

可以 設定 multiple 屬性 來多選 此時 value 將和一個 數組 綁定

```html
<mat-form-field>
  <mat-select formControlName="interest" placeholder="你的休閒愛好?" multiple>
    ...
  </mat-select>
</mat-form-field>
```

![](assets/07-mutiple-select.gif)

mutiple 屬性無法 動態 改變 所以 `[multiple]="isMultiple"` 是錯誤寫法 要動態切換 是否可 多選 可以 使用 ngIf/ngSwitch

```html
<mat-select *ngIf="isMultiple" multiple>
  ...
</mat-select>
<mat-select *ngIf="!isMultiple">
  ...
</mat-select>
```

# 自動選中後顯示的文本

可以通過 mat-select-trigger 自定義 有選項時 顯示的文本 這通常對 多選 很有用

```html
<mat-form-field>
  <mat-select formControlName="interest" #selectInterest placeholder="你的休閒愛好?" multiple>
    <mat-select-trigger>
      共選擇了 {{ selectInterest.selected.length }} 項興趣
    </mat-select-trigger>
    ...
  </mat-select>
</mat-form-field>
```

![](assets/08-mat-select-trigger.gif)

# 取消 ripple 特效

ripple 特效 會在 選擇項目時 產生一個 漣漪動畫效果 加入 disableRipple 屬性 即可取消此特效

```html
<mat-form-field>
  <mat-select ... disableRipple>
    ...
  </mat-select>
</mat-form-field>
```

# 自定 下來 panel css

mat-select 提供了 panelClass 屬性 來 設置 選項菜單的 css 

```html
<mat-form-field>
  <mat-select ... panelClass="blue-dropdown">
    ...
  </mat-select>
</mat-form-field>
```

```css
.blue-dropdown .mat-select-content {
  background: rgba(0, 0, 255, 0.5);
}
```

這些功能都被拉到共用的 Angular CDK 所以 panel 其實會被放到 component 之外 所以在 component.css 中 定義此 css 會無效 除非額外設定 ViewEncapsulation , 或者 直接在全域 style.css 中定義即可

![](assets/11-panelclass-select.gif)
