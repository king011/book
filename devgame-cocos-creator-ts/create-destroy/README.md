# [創建和銷毀節點](http://docs.cocos.com/creator/manual/zh/scripting/create-destroy.html)

除了 通過 creator 編輯 節點 cocos 提供了動態 編輯 節點的 能力

# 創建新節點

使用 new cc.Node() 即可 動態 創建 節點

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // 定義一個 creator 屬性 以便在 creator 中 指定 資源 
    @property(cc.SpriteFrame)
    readonly sprite: cc.SpriteFrame = null

    // LIFE-CYCLE CALLBACKS:

    // 在 start 中 創建新節點
    start() {
        // 創建一個 名稱爲 Sprite 的 節點
        const node = new cc.Node('Sprite')
        // 爲節點 掛接 cc.Sprite 組件
        const sprite = node.addComponent(cc.Sprite)
        // 設置 cc.Sprite 組件 使用的 資源
        sprite.spriteFrame = this.sprite
        // 間新 節點 加入到 節點樹
        node.parent = this.node
    }

}
```

# 克隆已有節點

使用 cc.instantiate 可以 克隆一個 已有節點

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // 定義一個 creator 屬性 以便在 creator 中 指定 源
    @property(cc.Node)
    readonly source: cc.Node = null

    // LIFE-CYCLE CALLBACKS:

    // 在 start 中 克隆 新節點
    start() {
        // 獲取 克隆 源
        let source: cc.Node = null
        if (this.source) {
            source = this.source
        } else {
            source = cc.find("Canvas/cocos")
        }
        // 克隆 節點
        const clone = cc.instantiate(source)

        // 將 克隆體 添加到 場景
        clone.parent = cc.director.getScene()
        clone.setPosition(0, 0)
        clone.setAnchorPoint(0, 0)
    }
}
```

# 創建 預製節點

創建 預製節點 的 方法 類似 克隆節點

```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // 定義一個 creator 屬性 以便在 creator 中 指定 源
    @property(cc.Prefab)
    readonly source: cc.Prefab = null

    // LIFE-CYCLE CALLBACKS:

    // 在 start 中 克隆 新節點
    start() {
        // 創建 預製節點
        const clone = cc.instantiate(this.source)

        // 將 預製節點 添加到 場景
        clone.parent = cc.director.getScene()
        clone.setPosition(0, 0)
        clone.setAnchorPoint(0, 0)
    }
}
```

# 銷毀節點

* 調用 destroy 即可 銷毀節點 

* 節點不會被立刻移除 而是在當前邏輯更新結束後 統一移除 當一個 節點 被銷毀後 處於 無效狀態 cc.isValid 可以返回 節點是否有效


```ts
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Node)
    readonly target: cc.Node = null

    // LIFE-CYCLE CALLBACKS:

    start() {
        if (this.target) {
            setTimeout(() => {
                this.target.destroy()
            }, 5000)
        } else {
            console.warn("target null")
        }
    }
    update(dt: number) {
        if (this.target) {
            if (cc.isValid(this.target)) {
                this.target.angle -= dt * (360 / 5)
            }
        }
    }
}
```

## destroy 和 removeFromParent 區別

調用 removeFromParent 後 只是從 父節點中 移除 但 可能在其它地方被引用 故可能不會釋放內存

調用destroy 會直接 銷毀節點 並從 父節點中 移除

