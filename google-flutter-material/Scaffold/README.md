# Scaffold

[Scaffold](https://docs.flutter.io/flutter/material/Scaffold-class.html) 是一個容器

爲 material 程式 提供了 容納 導航 body 抽屜 等效果的標準方案

* appBar 屬性 接收一個 AppBar 爲 app 提供一個 工具欄
* body 屬性 用來容納 主要內容
* drawer 屬性 用來 提供 抽屜效果