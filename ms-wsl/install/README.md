# 安裝

1. 開啓 WSL 相關組件

	在 Power Shell 輸入如下指令：
	```
	dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
	dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
	```
	
	或者使用 添加刪除windows組件功能：
	
	![](assets/w.png)
	
	安裝完成後 可能需要重啓計算機
	
1. 在 windows 商店 安裝想玩的 linux 系統

1. 列出子系統

	```
	wsl -l -v
	```
	
	* -l 列出子系統
	* -v 打印詳細信息

1. 將子系統切換爲 wsl2

	```
	wsl --set-version XXX 2
	```
	
	此外可以使用下述指令 設置以後安裝 linux 時默認啓用 wsl2
	
	```
	wsl --set-default-version 2
	```