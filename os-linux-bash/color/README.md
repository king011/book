# 顏色與格式

在 ANSI/VT100 終端和終端仿真器中不止能夠顯示顏色還支持一些格式化的文本。這些特性由轉義序列定義，其形式爲：

```
<Esc>[FormatCodem
```

1. **&lt;Esc&gt;** 字符是固定的開始標記在 bash 中可以使用以下語句獲取

	* \e
	* \033
	* \x1B

2. **[** 也是固定語法
3. **FormatCode** 是一個編碼數字用來指定要使用的顏色或格式
4. **m** 固定語法代表定義結束

```
#!/usr/bin/env bash

# -e 參數 啓用 轉義序列
# -n 參數 不要輸出換行
echo -en "\e[31m"
echo my color is red

# 4 加上下劃線
echo -en "\e[4m"
echo i have underlined and red color

# 清除格式與顏色
echo -en "\e[0m"

echo default

# 也可以在輸出中定義轉義序列和輸出內容
# 多個 FormatCode使用分號隔開
echo -e "\e[32;4mi have underlined and green color\e[0m"
```

**注意** 一定要記得在結束後定義 **\e[0m** 將終端的輸出顏色與格式恢復到默認，不然此後你的終端可能會顯示的亂七八糟

# 設置格式

| FormatCode | 描述 | 例子 | 預覽 |
| -------- | -------- | -------- | -------- |
| 1     | 粗體/明亮(Bold/Bright)     | echo -e "Normal '\e[1mBold\e[0m'"     | ![](assets/1.png) |
| 2     | 黯淡(Dim)     | echo -e "Normal '\e[2mDim\e[0m'"     | ![](assets/2.png) |
| 4     | 下劃線(Underlined)     | echo -e "Normal '\e[4mUnderlined\e[0m'"     | ![](assets/4.png) |
| 5     | 閃爍(Blink)     | echo -e "Normal '\e[5mBlink\e[0m'"     | ![](assets/5.gif) |
| 7     | 反轉前景色和背景色     | echo -e "Normal '\e[7mInverted\e[0m'"     | ![](assets/7.png) |
| 8     | 隱藏(用於密碼)     | echo -e "Normal '\e[8mHide\e[0m'"     | ![](assets/8.png) |

不要依賴 **8(隱藏)** 一些終端可能並不支持
# 重置格式

| FormatCode | 描述 |
| -------- | -------- |
| 0     | 重置 所有屬性     |
| 21     | 重置 粗體/明亮     |
| 22     | 重置 黯淡     |
| 24     | 重置 下劃線     |
| 25     | 重置 閃爍     |
| 27     | 重置 反轉     |
| 28     | 重置 隱藏     |

除了 **0** 重置所有格式和顏色外，其它重置都是對應設置格式的 FormatCode + 20

# 8/16 色

## 前景色


| FormatCode | 描述 | 例子 | 預覽 |
| -------- | -------- | -------- | -------- |
| 39     | 默認前景色     | echo -e "Default \e[39mDefault"     | ![](assets/39.png)     |
| 30     | 黑色     | echo -e "Default \e[30mBlack"     | ![](assets/30.png)     |
| 31     | 紅色     | echo -e "Default \e[31mRed"     | ![](assets/31.png)     |
| 32     | 綠色     | echo -e "Default \e[32mGreen"     | ![](assets/32.png)     |
| 33     | 黃色     | echo -e "Default \e[33mYellow"     | ![](assets/33.png)     |
| 34     | 藍色     | echo -e "Default \e[34mBlue"     | ![](assets/34.png)     |
| 35     | 品紅     | echo -e "Default \e[35mMagenta"     | ![](assets/35.png)     |
| 36     | 青色     | echo -e "Default \e[36mCyan"     | ![](assets/36.png)     |
| 37     | 淺灰     | echo -e "Default \e[37mLight gray"     | ![](assets/37.png)     |
| 90     | 深灰     | echo -e "Default \e[90mDark gray"     | ![](assets/90.png)     |
| 91     | 淡紅     | echo -e "Default \e[91mLight red"     | ![](assets/91.png)      |
| 92     | 淺綠     | echo -e "Default \e[92mLight green"     | ![](assets/92.png)      |
| 93     | 淺黃     | echo -e "Default \e[93mLight yellow"     | ![](assets/93.png)      |
| 94     | 淺藍     | echo -e "Default \e[94mLight blue"     | ![](assets/94.png)      |
| 95     | 淺品紅     | echo -e "Default \e[95mLight magenta"     | ![](assets/95.png)      |
| 96     | 淺青綠     | echo -e "Default \e[96mLight cyan"     | ![](assets/96.png)      |
| 97     | 白色     | echo -e "Default \e[97mWhite"     | ![](assets/97.png)      |

## 背景色

| FormatCode | 描述 | 例子 | 預覽 |
| -------- | -------- | -------- | -------- |
| 49     | 默認背景色     | echo -e "Default \e[49mDefault"     | ![](assets/49.png)     |
| 40     | 黑色     | echo -e "Default \e[40mBlack"     | ![](assets/40.png)     |
| 41     | 紅色     | 	echo -e "Default \e[41mRed"     | ![](assets/41.png)     |
| 42     | 綠色     | echo -e "Default \e[42mGreen"     | ![](assets/42.png)     |
| 43     | 黃色     | echo -e "Default \e[43mYellow"     | ![](assets/43.png)     |
| 44     | 藍色     | echo -e "Default \e[44mBlue"     | ![](assets/44.png)     |
| 45     | 品紅     | echo -e "Default \e[45mMagenta"     | ![](assets/45.png)     |
| 46     | 青色     | echo -e "Default \e[46mCyan"     | ![](assets/46.png)     |
| 47     | 淺灰     | echo -e "Default \e[47mLight gray"     | ![](assets/047.png)     |
| 100     | 深灰     | echo -e "Default \e[100mDark gray"     |
| 101     | 淺紅     | echo -e "Default \e[101mLight red"     | ![](assets/101.png)     |
| 102     | 深綠     | echo -e "Default \e[102mLight green"     | ![](assets/102.png)     |
| 103     | 淺黃     | echo -e "Default \e[103mLight yellow"     | ![](assets/103.png)     |
| 104     | 淺藍     | 	echo -e "Default \e[104mLight blue"     | ![](assets/104.png)     |
| 105     | 淺品紅     | echo -e "Default \e[105mLight magenta"     | ![](assets/105.png)     |
| 106     | 淺青色     | echo -e "Default \e[106mLight cyan"     | ![](assets/106.png)     |
| 107     | 白色     | echo -e "Default \e[107mWhite"     |

![](assets/background.png)
