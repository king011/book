# 使用 usb

在linux下 需要將 用戶 加入vboxusers 組
```
sudo gpasswd -a XXX vboxusers
```

# 壓縮 vdi 鏡像大小

```
VBoxManage modifyhd XXX.vdi --compact
```

# 創建新的 uuid
```
VBoxManage  internalcommands sethduuid XXX.vdi
```

# xp 3d

Virtualbox 6.1 開始對於 VBoxVGA 不在提供 3d 支持

* VBoxVGA 適用與舊版 os(windows 7 之前的 windows 版本)
* VBoxSVGA 用於 windows 7 或更高版本的 windows 與 VBoxVGA 相比，提高了性能和 對 3d 的支持
* VMSVGA 用於linux

如果要使用 xp 支持 3d 只能安裝 5.x 的 Virtualbox(6.0 也支持但會在每次啓動時進行警告)
