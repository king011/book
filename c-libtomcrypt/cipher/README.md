# 對稱加密

# 加密模式（mode of operation）
分組加密演算法是按分組大小來進行加解密操作的，如DES演算法的分組是64位，而AES是128位，但實際明文的長度一般要遠大於分組大小，這樣的情況如何處理呢？

這正是即工作模式要解決的問題：明文資料流怎樣按分組大小切分，資料不對齊的情況怎麼處理等等。

早在1981年，DES演算法公佈之後，NIST在標準文獻FIPS 81中公佈了4種工作模式：

* 電子密碼本：Electronic Code Book Mode (ECB)
* 密碼分組連結：Cipher Block Chaining Mode (CBC)
* 密文反饋：Cipher Feedback Mode (CFB)
* 輸出反饋：Output Feedback Mode (OFB)

2001年又針對AES加入了新的工作模式：

* 計數器模式：Counter Mode (CTR)

## ECB 電子密碼本

ECB模式只是將明文按分組大小切分，然後用同樣的金鑰正常加密切分好的明文分組，所以它不需要 IV

![](assets/EBC.png)

ECB的理想應用場景是短資料（如加密金鑰）的加密。此模式的問題是無法隱藏原明文資料的模式，因為同樣的明文分組加密得到的密文也是一樣的

因爲存在明文攻擊的可能所以這種模式已經很少使用

```
// 因爲是分組計算，所以每次加密的數據必須是分組大小的整數倍，對於 AES 算法來說就是 16 字節
int encrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_ECB ecb;
    if (ret = ecb_start(cipher, key, key_len, 0, &ecb))
    {
        printf("ecb_start fail %d\n", ret);
        return ret;
    }
    if (ret = ecb_encrypt(input, output, input_len, &ecb))
    {
        printf("ecb_encrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
// 因爲是分組計算，所以每次解密的數據必須是分組大小的整數倍，對於 AES 算法來說就是 16 字節
int decrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_ECB ecb;
    if (ret = ecb_start(cipher, key, key_len, 0, &ecb))
    {
        printf("ecb_start fail %d\n", ret);
        return ret;
    }

    if (ret = ecb_decrypt(input, output, input_len, &ecb))
    {
        printf("ecb_decrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
```

## CBC 密碼分組連結模式

此模式是1976年由IBM所發明，引入了IV（初始化向量：Initialization Vector）的概念。

* IV長度和分組大小一致，通常情況下不用保密
* 不過在大多數情況下，針對同一金鑰不應多次使用同一組IV

CBC要求第一個分組的明文在加密運算前先與IV進行異或；從第二組開始，所有的明文先與前一分組加密後的密文進行異或

![](assets/CBC.png)

CBC模式相比ECB實現了更好的模式隱藏，但因為其將密文引入運算，加解密操作無法並行操作。同時引入的IV向量，還需要加、解密雙方共同知曉方可

```
#include <stdio.h>
#include <errno.h>
#include <libtomcrypt/tomcrypt.h>
#include <stdint.h>
#define CIPHER "aes"
#define KEY "12345678901"
#define IV "this is iv"

int hash_sum(const struct ltc_hash_descriptor *desc, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    hash_state md;
    if (ret = desc->init(&md))
    {
        printf("%s init fail\n", desc->name);
        return ret;
    }
    if (ret = desc->process(&md, input, input_len))
    {
        printf("%s process fail\n", desc->name);
        return ret;
    }
    if (ret = desc->done(&md, output))
    {
        printf("%s done fail\n", desc->name);
        return ret;
    }
    return ret;
}
void hex_println(const uint8_t *s, const size_t s_len)
{
    static uint8_t *hextable = "0123456789abcdef";
    uint8_t j = 0;
    uint8_t v;
    for (size_t i = 0; i < s_len; i++)
    {
        v = s[i];
        printf("%c", hextable[v >> 4]);
        printf("%c", hextable[v & 0x0f]);
    }
    printf("\n");
}
// 因爲是分組計算，所以每次加密的數據必須是分組大小的整數倍，對於 AES 算法來說就是 16 字節
int encrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_CBC cbc;
    if (ret = cbc_start(cipher, iv, key, key_len, 0, &cbc))
    {
        printf("cbc_start fail %d\n", ret);
        return ret;
    }
    if (ret = cbc_encrypt(input, output, input_len, &cbc))
    {
        printf("cbc_encrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
// 因爲是分組計算，所以每次解密的數據必須是分組大小的整數倍，對於 AES 算法來說就是 16 字節
int decrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_CBC cbc;
    if (ret = cbc_start(cipher, iv, key, key_len, 0, &cbc))
    {
        printf("cbc_start fail %d\n", ret);
        return ret;
    }

    if (ret = cbc_decrypt(input, output, input_len, &cbc))
    {
        printf("cbc_decrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
int main(int argc, const char **argv)
{
    // 註冊所有註冊的算法
    register_all_ciphers();
    int ret = 0;

    // 查找算法
    int cipher = find_cipher(CIPHER);
    if (cipher == -1)
    {
        printf("cipher not found: %s\n", CIPHER);
        return -1;
    }

    // 對於 aes 密鑰長度確定了算法
    // 16 AES-128，可以使用 md5 產生
    // 24 AES-192
    // 32 AES-256，可以使用 sha256 產生
    uint8_t key[32];
    if (ret = hash_sum(&sha256_desc, KEY, strlen(KEY), key))
    {
        return -1;
    }
    size_t key_len = sizeof(key) / sizeof(uint8_t);

    // iv 長度需要和加密塊大小一致 aes 是 16 這正好是 md5 的長度
    uint8_t iv[16];
    if (ret = hash_sum(&md5_desc, IV, strlen(IV), iv))
    {
        return -1;
    }

    // 創建緩衝區
    uint8_t *str = "123草7890123草";
    size_t length = strlen(str);
    uint8_t *dec = 0;
    uint8_t *enc = malloc(length);
    if (!enc)
    {
        ret = errno;
        puts(strerror(errno));
        goto END;
    }
    dec = malloc(length + 1);
    if (!dec)
    {
        ret = errno;
        puts(strerror(errno));
        goto END;
    }
    dec[length] = 0;
    printf("key=%s\niv=%s\nstr=%s\n", KEY, IV, str);

    // 執行加密
    if (ret = encrypt(cipher, key, key_len, iv, str, length, enc))
    {
        goto END;
    }

    // 打印加密結果
    hex_println(enc, length);

    // 執行解密
    if (ret = decrypt(cipher, key, key_len, iv, enc, length, dec))
    {
        goto END;
    }
    puts(dec);

END:
    if (enc)
    {
        free(enc);
    }
    if (dec)
    {
        free(dec);
    }
    return ret;
}
```

## CFB 密文反饋模式

與CBC模式類似，但不同的地方在於，CFB模式先生成密碼流字典，然後用密碼字典與明文進行異或操作並最終生成密文。後一分組的密碼字典的生成需要前一分組的密文參與運算。

* 這樣的好處是不需要對原文進行填充對齊，所以使用上比 CBC 容易
* 不需要對齊的特性，對流式數據加解密也比較友好

![](assets/CFB.png)

```
int encrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_CFB cfb;
    if (ret = cfb_start(cipher, iv, key, key_len, 0, &cfb))
    {
        printf("cfb_start fail %d\n", ret);
        return ret;
    }
    if (ret = cfb_encrypt(input, output, input_len, &cfb))
    {
        printf("cfb_encrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
int decrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_CFB cfb;
    if (ret = cfb_start(cipher, iv, key, key_len, 0, &cfb))
    {
        printf("cfb_start fail %d\n", ret);
        return ret;
    }

    if (ret = cfb_decrypt(input, output, input_len, &cfb))
    {
        printf("cfb_decrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
```

## OFB 輸出反饋模式

OFB模式與CFB模式不同的地方是：生成字典的時候會採用明文參與運算，CFB採用的是密文

* 通常採用 OFB 的地方都比較少，因爲它也 CFB 比起來沒有什麼有用的特性
* 並且它加密和解密是同樣的操作，所以有時還會讓人感到困惑，建議不要使用這個模式

![](assets/OFB.png)

```
int encrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_OFB ofb;
    if (ret = ofb_start(cipher, iv, key, key_len, 0, &ofb))
    {
        printf("ofb_start fail %d\n", ret);
        return ret;
    }
    if (ret = ofb_encrypt(input, output, input_len, &ofb))
    {
        printf("ofb_encrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
int decrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_OFB ofb;
    if (ret = ofb_start(cipher, iv, key, key_len, 0, &ofb))
    {
        printf("ofb_start fail %d\n", ret);
        return ret;
    }

    // ofb 模式加密解密過程是一樣的，所以這裏也可以調用 ofb_encrypt 進行解密
    if (ret = ofb_decrypt(input, output, input_len, &ofb))
    {
        printf("ofb_decrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
```

## CTR 計數器模式模式

CTR模式同樣會產生流密碼字典，但同是會引入一個計數，以保證任意長時間均不會產生重複輸出

![](assets/CTR.png)

CTR模式只需要實現加密演算法以生成字典，明文資料與之異或後得到密文，反之便是解密過程。CTR模式可以採用並行演算法處理以提升吞量，另外加密資料塊的訪問可以是隨機的，與前後上下文無關

```
int encrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_CTR ctr;
    // CTR_COUNTER_LITTLE_ENDIAN
    // CTR_COUNTER_BIG_ENDIAN
    // CTR_COUNTER_LITTLE_ENDIAN | LTC_CTR_RFC3686
    // CTR_COUNTER_BIG_ENDIAN | LTC_CTR_RFC3686
    if (ret = ctr_start(cipher, iv, key, key_len, 0, CTR_COUNTER_BIG_ENDIAN, &ctr))
    {
        printf("ctr_start fail %d\n", ret);
        return ret;
    }
    if (ret = ctr_encrypt(input, output, input_len, &ctr))
    {
        printf("ctr_encrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
int decrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    symmetric_CTR ctr;
    // CTR_COUNTER_LITTLE_ENDIAN
    // CTR_COUNTER_BIG_ENDIAN
    // CTR_COUNTER_LITTLE_ENDIAN | LTC_CTR_RFC3686
    // CTR_COUNTER_BIG_ENDIAN | LTC_CTR_RFC3686
    if (ret = ctr_start(cipher, iv, key, key_len, 0, CTR_COUNTER_BIG_ENDIAN, &ctr))
    {
        printf("ctr_start fail %d\n", ret);
        return ret;
    }
    if (ret = ctr_decrypt(input, output, input_len, &ctr))
    {
        printf("ctr_decrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
```

# 模式總結

* **ECB** Electronic CodeBook (ECB)，不變的 key，需要加密器和解密器，加解密的複雜度可能不同
	* 優點
		* 構造簡單、容易實做
	* 缺點
		* 長時間下，容易被偵測。影像資料的差異性不大，很容易被辨識到重複性，相較於文字很容易受前後文的影響
* **CBC** Cipher Block Chaining (CBC)，不變的 key，以及前一個密文會先針對明文加密
	* 優點
		* 相同明文，會因為前一個的密文不同造就出不同的密文，也就是加密器多一個新的狀態
	* 缺點
		* 一個密文 Ci 的錯誤，會導致兩個明文解析錯誤 (Pi & Pi+1)
		* 第一次加密很容易被抽換 bitwise，因為每次驅動的 Initial Vector 都相同
* **CFB** Cipher FeedBack (CFB)，類似 CBC，但前一個密文的結果只影響一部分的加密關係，然後將前一段密文狀態加密 key，再對明文加密
	* 優點
		* 支持即時 (real-time) 通訊
		* 支持自同步 (self-synchronization)，即使中斷連線、訊息錯誤，可以在數個週期後再次同步運作
		* 藉由自同步的概念，可以捨棄掉 Initial Vector
		* 後半部的明文，可以透過週期性的部分密文建立解密狀態，支持 random access
	* 缺點
		* error propagation 錯誤增長，當一個訊息錯誤時，需要好幾個週期後才能修正回來，這導致中間的解密訊息都不能用
		* 雜訊過多的情況下，不宜使用
* **OFB** Output FeedBack (OFB)，類似於 CFB，將前一段的加密 key 拿回來加密，不依賴接收的密文狀態
	* 優點
		* 支持即時 (real-time) 通訊
		* 只需要加密器，加密做兩次相當於解密
		* 相較於 CFB，沒有錯誤增長的情況
		* 依序使用的 key，可以事先算出來，然後依次使用
		* 雜訊下支持的能力好
	* 缺點
		* 必須一直保持同步
		* 訊息被修改時，不易被發現，只單純影響單一明文 (沒有錯誤增長)
		* 起始狀態的 Initial Vector，不能重複使用，否則很容易被攻擊者抓到
		* 假設沒有預先算 key，沒辦法解密出後半部的明文
* **CTR** Counter (CTR)，類似於 OFB，直接利用計數器作為加密 key
	* 優點
		* 加解密可以平行化處理，如果加解密速度耗時，可以選擇這一種
		* 支持 random access
	* 缺點
		* 必須一直保持同步
		* 訊息被修改時，不易被發現，只單純影響單一明文 (沒有錯誤增長)
		* 起始狀態的 Initial Vector，不能重複使用，否則很容易被攻擊者抓到

| 特點 | 加密模式 |
| -------- | -------- |
| 區塊加密     | 	ECB、CBC、CTR     |
| 串流加密     | CFB、OFB     |
| 傳遞誤差(資料不能有缺失)     | CBC、CFB     |
| 不傳遞誤差(資料允許缺失)     | ECB、OFB、CTR     |
| 可並行     | ECB、CTR     |
| 不可並行     | CBC、OFB、CFB     |


# CCM Counter with CBC-MAC

CCM模式，全稱是Counter with Cipher Block Chaining-Message Authentication Code，是CTR工作模式和CMAC認證演算法的組合體，可以同時資料加密和鑑別服務。

明文資料通過CTR模式加密成密文，然後在密文後面再附加上認證資料，所以最終的密文會比明文要長。具體的加密流程如下描述：先對明文資料認證併產生一個tag，在後續加密過程中使用此tag和IV生成校驗值U。然後用CTR模式來加密原輸入明文資料，在密文的後面附上校驗碼Ｕ

它主要用於無線網路安全標準。如 IEEE 802.11i(WPA2) 和 IEEE 802.16e(WiMax)

![](assets/CCM.png)

tomcrypt 也提供了 CCM 的支持，但本喵對 CCM 不太了解，且 golang 標準庫中未提供 CCM 實現本喵難以對照測驗對 CCM 的理解是否正確，同時 CCM 使用場景並不多故此處沒有給出 CCM 的 tomcrypt 實例代碼

# GCM：伽羅瓦計數器模式 Galois/Counter Mode

GCM模式是CTR和GHASH的組合，GHASH操作定義為密文結果與金鑰以及訊息長度在GF（2^128）域上相乘。GCM比CCM的優勢是在於更高並行度及更好的效能。

GCM中的G就是指GMAC，C就是指CTR。
GCM可以提供對消息的加密和完整性校驗，另外，它還可以提供附加消息的完整性校驗。在實際應用場景中，有些信息是我們不需要保密，但信息的接收者需要確認它的真實性的，例如源IP，源端口，目的IP，IV，等等。因此，我們可以將這一部分作為附加消息加入到MAC值的計算當中。下圖的Ek表示用對稱秘鑰k對輸入做AES運算。最後，密文接收者會收到密文、IV（計數器CTR的初始值）、MAC值。

![](assets/GCM.png)

由於GCM現在屬於最嚴謹的加密模式，TLS 1.2標準使用的就是AES-GCM演算法

GCM 通常用於網路數據傳輸，可以保證數據的加密和不能被篡改。同時需要將每幀要加密的數據 一次 傳給 加密函數，同時每次都使用不同 IV

```
int gcm_memory(      int           cipher,
               const unsigned char *key,    unsigned long keylen,
               const unsigned char *IV,     unsigned long IVlen,
               const unsigned char *adata,  unsigned long adatalen,
                     unsigned char *pt,     unsigned long ptlen,
                     unsigned char *ct,
                     unsigned char *tag,    unsigned long *taglen,
                               int direction);
```

* **cipher** 加密算法索引
* **key** 密鑰
* **keylen** 密鑰長度
* **IV** 初始化向量
* **IVlen** 向量長度。標準的算法長度是 12
* **adata** 可選的附加數據阿
* **adatalen** 附加數據長度
* **pt** 明文
* **ptlen** 明文長度
* **tag** 數據簽名
* **taglen** 簽名長度，目前固定爲16
* **direction** 爲 GCM\_ENCRYPT 則將明文加密並將簽名存儲到 tag，如果爲 GCM_DECRYPT 則先驗證簽名並將密文解密

```
// 注意 output 應該比 input 多 16 字節，因爲 GCM 增加了 16 字節的簽名
int encrypt(int cipher,
            const uint8_t *key, const size_t key_len,
            const uint8_t *iv,
            const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    char tag[16];
    unsigned long taglen;
    if (ret = gcm_memory(cipher,
                         key, key_len,
                         iv, 12,
                         0, 0,
                         input, input_len,
                         output,
                         output + input_len, &taglen,
                         GCM_ENCRYPT))
    {
        printf("gcm_encrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
int decrypt(int cipher, const uint8_t *key, const size_t key_len, const uint8_t *iv, const uint8_t *input, const size_t input_len, uint8_t *output)
{
    int ret = CRYPT_OK;
    unsigned long taglen = 16;
    if (ret = gcm_memory(cipher,
                         key, key_len,
                         iv, 12,
                         0, 0,
                         output, input_len - 16,
                         input,
                         input + input_len - 16, &taglen,
                         GCM_DECRYPT))
    {
        printf("gcm_decrypt fail %d\n", ret);
        return ret;
    }
    return ret;
}
```

* **adata** 是雙方都需要知道的數據用於簽名
* **GCM** 加密後長度會增加 16 字節的簽名在末尾
* **IV** 每次加密都應該使用不同的隨機IV(否則相同幀會產生相同的密文)，網路通信中還需要傳遞 IV供接收方解密，所以每幀數據會多出 16 + 12 字節