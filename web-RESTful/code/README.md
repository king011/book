# http狀態碼

api應該 正確的返回 http 狀態碼

* 2xx 成功
    * 200 Ok 通用狀態碼
    * 201 Created 資源新增成功
    * 202 Accepted 請求已接受 但尚在處理中
    * 204 No Content 請求成功 但未回傳任何內容
* 3xx 重定向
    * 301 Moved Permanently 資源已移到它處
    * 303 See Other 回傳的內容可在它處取得(例如用戶發送了一個POST請求後) 
    * 304 Not Modified 請求的資源並未修改
* 4xx 用戶端錯誤 (客戶端不應該 retry原始請求)
    * 400 Bad Request 通用狀態碼
    * 401 Unauthorized 用戶端未驗證
    * 403 Forbidden 用戶端被禁止此請求
    * 404 Not Found 資源不存在
    * 405 Method Not Allowed 不支持請求的HTTP方法
    * 406 Not Acceptable 不支援請求的內容類型 (Accept 表頭)
    * 415 Unsupported Media Type 不支援請求使用的內容類型 (Content-Type 表頭)
* 5xx 服務器錯誤 (客戶端可合理 retry)
    * 500 Internal Server Error 服務器未知錯誤
    * 501 Not Implemented 請求目前未支援 將來可能會支援
    * 502 Bad Gateway 上游服務器未回傳正確結果 一般 gateway 或 proxy server 才會回傳此狀態碼
    * 503 Service Unavailable 暫停服務(如果一切順利 不久就會恢復服務)
    * 504 Gateway Timeout 上游服務器超時 一般是gateway或proxy server才會回傳此狀態碼

