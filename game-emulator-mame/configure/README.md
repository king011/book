# [配置](https://docs.mamedev.org/initialsetup/configuringmame.html)
在當前工作路徑下創建 默認的 **mame.ini** 配置檔案

```
mame64 -createconfig
```

其中
* **rompath** 指定了到哪裏尋找 roms 檔案
* **samplepath** 指定了到哪裏尋找 roms 對應遊戲的截圖

```
$ cat mame.ini |egrep 'rompath|samplepath'
rompath                   roms
samplepath                samples
```

# 運行遊戲

如果已經知道遊戲的 rom 名稱直接將其名字傳遞給 mame 即可直接運行，例如運行 Capcom 的 wof

```
mame64 wof
```

如果不知道名字則可以執行運行 mame 進入遊戲列表選擇要運行的遊戲

```
mame64
```