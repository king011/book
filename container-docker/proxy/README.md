# [daemon 代理](https://docs.docker.com/config/daemon/proxy/#environment-variables)

pull push 等操作都是由 daemon 程序處理,所以需要爲 daemon 設置代理

```
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo vi /etc/systemd/system/docker.service.d/http-proxy.conf
```

```
#info="http-proxy.conf"
[Service]
Environment="HTTP_PROXY=http://127.0.0.1:8118"
Environment="HTTPS_PROXY=http://127.0.0.1:8118"
Environment="NO_PROXY=localhost,127.0.0.1,docker-registry.example.com,.corp"
```

```
sudo systemctl daemon-reload
sudo systemctl restart docker

# 查看環境變量
sudo systemctl show --property=Environment docker
```

