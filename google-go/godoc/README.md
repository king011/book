# godoc

godoc 是 go 提供了 api 文檔 工具 可以從源碼的註釋中 自動 創建一個 api 文檔 http 服務

```
godoc -http=localhost:6060
```

# 基本 語法
```
// Package doc 爲包添加註釋
//
// Title
//
// 首字母大寫 並且 不以 . 結尾 創建標題 否則爲 段落
//
//
// Title 2
//
// godoc 不允許使用換行 寫 <br> \n
// 都無效 會被轉義
//
// 只能 加上空換行 來創建一個新的 段落
package doc

// Version 爲 常量 添加註釋
const Version = "0.0.1"

// OS 爲 變量 添加註釋
var OS = "linux"

// Sum 爲函數 添加註釋 , 使用 BUG 標註 bug
//
// BUG(who): 不要傳入 nil
func Sum(n ...int) (sum int) {
	for _, v := range n {
		sum += v
	}
	return
}

// Add 使用 Deprecated 標註棄用 api
//
// Deprecated: should implement Sum instead (or additionally).
func Add(n ...int) int {
	return Sum(n...)
}
```

# 示例代碼

示例代碼 需要 創建到一個 **xxx_test.go** 檔案中

```
package doc_test

import (
	"fmt"
	"test/doc"
)

// 全局 示例代碼
func Example() {
	fmt.Println(doc.OS)
	fmt.Println(doc.Version)
}

// 函數 示例代碼 需要叫 ExampleXXX
func ExampleSum() {
	doc.Sum(1, 2, 3, 4, 5, 6)
}
```

