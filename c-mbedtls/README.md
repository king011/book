# mbedtls

mbedtls 是一個使用 c 語言實現的開源(Apache-2.0 or GPL-2.0-or-later) tls 層,代碼佔用空間小適合用於嵌入式設備

* 源碼 [https://github.com/Mbed-TLS/mbedtls](https://github.com/Mbed-TLS/mbedtls)