# 基礎圖層

linuxserver 規劃了各種基礎 images，主要應用程式 images 源自這些 images。這主要有兩個原因

1. 多個 images 之間共享依賴基礎，這減少了共享相同依賴關係的多個應用之間的變化的可能性
2. 充分利用 docker image layer 系統，減少主機上 images 佔用的空間，在本地運行的多個共享基礎鏡像的容器將重用該鏡像以及任何祖先

# /config volume

爲了減少 images 之間的差異，應用的配置和依賴目錄採用了通用的結構模式。這意味着每個鏡像都有自己內部的 **/config** 目錄，其中包含所有特定與應用的配置。除了少數 images 外，所有 linuxserver 的 images 都暴露了這個 volume

# 創建/運行 容器

要創建或運行 linuxserver 通常都是使用類似的命令模式

```
docker create \
    --name=<container_name> \
    -v <path_to_data>:/config \
    -e PUID=<uid> \
    -e PGID=<gid> \
    -e TZ=Asia/Shanghai \
    -p <host_port>:<app_port> \
    linuxserver/<image_name>
```
