# Any

```
#include <boost/any.hpp>
```

* Author(s) -> Kevlin Henney
* First Release -> 1.23.0
* C++ Standard Minimum Level -> 03
* Categories -> Data structures

c++ 是強類型，有時候弱類型可以提供一些方便，Any 庫爲c++提供了這一可能 

any 可以用於存儲任意帶 拷貝或移動的 值其定義如下

```
class any {
public:
  // construct/copy/destruct
  any() BOOST_NOEXCEPT;
  any(const any &) BOOST_NOEXCEPT;
  any(any &&) BOOST_NOEXCEPT;
  template<typename ValueType> any(const ValueType &);
  template<typename ValueType> any(ValueType &&);
  any & operator=(const any &);
  any & operator=(any &&) BOOST_NOEXCEPT;
  template<typename ValueType> any & operator=(const ValueType &);
  template<typename ValueType> any & operator=(ValueType &&) BOOST_NOEXCEPT;
  ~any() BOOST_NOEXCEPT;

  // modifiers
  any & swap(any &) BOOST_NOEXCEPT;
  void clear() BOOST_NOEXCEPT;

  // queries
  bool empty() BOOST_NOEXCEPT const;
  const std::type_info & type() BOOST_NOEXCEPT const;
};
```

存儲的值需要可以拷貝 或 移動 並且其移動語義 與 析構函數不能拋出異常


# any_cast

使用 any\_cast 可以獲取 any存儲的值 如果類型不匹配將拋出 bad\_any\_cast 異常

```
class bad_any_cast : public std::bad_cast {
public:
  virtual const char * what() BOOST_NOEXCEPT const;
};
```

```
#include <iostream>
#include <boost/any.hpp>

void print(boost::any &v)
{
    const auto &t = v.type();
    if (t == typeid(int))
    {
        std::cout << "int value: " << boost::any_cast<int>(v) << std::endl;
    }
    else if (t == typeid(const char *))
    {
        std::cout << "const char* value: " << boost::any_cast<const char *>(v) << std::endl;
    }
    else if (t == typeid(char *))
    {
        std::cout << "char* value: " << boost::any_cast<char *>(v) << std::endl;
    }
    else
    {
        std::cout << "unknow type" << std::endl;
    }
}
int main(int argc, char *argv[])
{
    boost::any x = 1;
    print(x);
    x = "abc";
    print(x);
    char *ok = (char *)("def");
    x = ok;
    print(x);
    return 0;
}
```