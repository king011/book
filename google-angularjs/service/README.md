# 服務

在 AngularJS  中 服務是一個 函數 或 對象  
在控制器中 使用 依賴注入 以供 控制器 使用

AngularJS 內置了 30多個 服務 以提供 常用的功能

```
var app = angular.module('app', []);
app.controller('ctrl', function($location) {
	alert($location.absUrl());
});
```

# 自定義服務

使用 service 方法 可以自定義 服務 以供其它 過濾器或 控制器使用

```
var app = angular.module('app', []);
// 自定義 服務
app.service("sv",function(){
	var name = "kate"
	this.get = function(){
		return name;
	}
	this.set = function(str){
		name = str;
	}
});
 
// 在控制器中 使用 自定義 服務
app.controller('ctrl', function($log,sv) {
	sv.set("kate beckinsale")
	$log.log(sv.get());
});
```

需要在 使用 服務的 控制器/過濾器 之前 定義服務

自定義服務的名稱 不能以 **$** 開始

# $log

$log 提供了 日誌服務

```
var app = angular.module('app', []);
app.controller('ctrl', function($log) {
	$log.log("this is a  log message");
	$log.debug("this is a  debug message");
	$log.info("this is a  info message");
	$log.warn("this is a  warn message");
	$log.error("this is a  error message");
});
```

# $location

$location 服務提供了 到瀏覽器 url地址的 綁定

```
var app = angular.module('app', []);
app.controller('ctrl', function($location) {
	// 返回 瀏覽器 地址
	$location.absUrl();
	// 返回 請求 協議
	$location.protocol();
	// 返回 主機名 (沒有端口號)
	$location.host();
	// 返回 端口號
	$location.port();
	// ...
});
```