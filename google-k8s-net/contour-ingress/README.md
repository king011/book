# [contour ingress](https://projectcontour.io/)

contour 是一個開源的 ingress 控制器，此外也可以作爲 http 網關

使用下述步驟進行安裝

1. 在 k8s 中 安裝  contour

	```
	kubectl apply -f https://projectcontour.io/quickstart/contour.yaml
	```
	
2. 查看 contour 的所有資源

	```
	kubectl get all -n projectcontour
	```
	
3. 查看 contour 是否就緒

	```
	kubectl get pods -n projectcontour -o wide
	```
	
	如果一切就緒
	* 有 2 個 contour pods 狀態爲 1/1     Running
	* 有 1 個 envoy pod 狀態爲 2/2     Running

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: http-ingress
spec:
    ingressClassName: contour
    rules:
        - http:
              paths:
                  - pathType: Prefix
                    path: /version
                    backend:
                        service:
                            name: apps-meta
                            port:
                                number: 80
```

# [HTTPProxy](https://projectcontour.io/docs/v1.22.1/config/fundamentals/)


HTTPProxy 是 contour 的自定義資源目的是擴展 Ingress 功能

* [api](https://projectcontour.io/docs/v1.22.1/config/api/)

```
# 爲 http 創建的 ingress
apiVersion: projectcontour.io/v1
kind: HTTPProxy
metadata:
  namespace: demo-ingress
  name: demo-http-ingress
spec:
  virtualhost:
    fqdn: "127.0.0.1"
  routes:
    # http routes
    - conditions:
      - prefix: /api/v1/
      enableWebsockets: true
      services:
        - name: demo-v1
          port: 9000
    - conditions:
      - prefix: /api/v2/
      enableWebsockets: true
      services:
        - name: demo-v2
          port: 9000
    # grpc routes
    - conditions:
      - prefix: /jsgenerate_server.api.V1/
      services:
        - name: demo-v1
          port: 9000
          protocol: h2c
    - conditions:
      - prefix: /jsgenerate_server.api.V2/
      services:
        - name: demo-v2
          port: 9000
          protocol: h2c
```

> HTTPProxy 必須指定 spec.virtualhost.fqdn 來限定主機名稱，並且不支持通配符

查看 httpproxy 資源是否正確
```
kubectl get httpproxy -n demo-ingress
```