# syncthing

syncthing 是使用 golang 實現的一個開源(MPL2) 跨平臺 檔案同步系統

* 官網 [https://syncthing.net/](https://syncthing.net/)
* 源碼 [https://github.com/syncthing/syncthing](https://github.com/syncthing/syncthing)
* docker [https://hub.docker.com/r/syncthing/syncthing](https://hub.docker.com/r/syncthing/syncthing)