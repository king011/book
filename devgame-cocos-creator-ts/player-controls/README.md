# 全局系統事件

全局系統事件 由 [cc.systemEvent](https://docs.cocos.com/creator/api/zh/classes/SystemEvent.html) 統一 派發 和 監聽

# 鍵盤事件
* cc.SystemEvent.EventType.KEY_DOWN
* cc.SystemEvent.EventType.KEY_UP
* [cc.Event.EventKeyboard](http://docs.cocos.com/creator/api/zh/classes/Event.EventKeyboard.html)

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // 監聽 鍵盤 按下/擡起
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)
        // once
    }
    onDestroy() {
        // 取消 事件 監聽
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)
    }
    private onKeyDown(evt: cc.Event.EventKeyboard) {
        switch (evt.keyCode) {
            case cc.macro.KEY.a:
                console.log('KEY_DOWN a')
                break
        }
    }
    private onKeyUp(evt: cc.Event.EventKeyboard) {
        switch (evt.keyCode) {
            case cc.macro.KEY.a:
                console.log('KEY_UP a')
                break
        }
    }
}
```

# 重力事件

* cc.SystemEvent.EventType.DEVICEMOTION
* [cc.Event.EventAcceleration](http://docs.cocos.com/creator/api/zh/classes/Event.EventAcceleration.html)

```
const { ccclass, property } = cc._decorator

@ccclass
export default class NewClass extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // 監聽 重力事件
        cc.systemEvent.setAccelerometerEnabled(true) // 啓用重力加速度
        cc.systemEvent.on(cc.SystemEvent.EventType.DEVICEMOTION, this.onDeviceMotionEvent, this)
        // once
    }
    onDestroy() {
        // 取消 事件 監聽
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onDeviceMotionEvent, this)
    }
    private onDeviceMotionEvent(evt: cc.Event.EventAcceleration) {
        const acc = (evt as any).acc
        cc.log(acc.x + "   " + acc.y)
    }
}
```