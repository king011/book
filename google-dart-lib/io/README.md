# dart:io

```dart
#info=false
import 'dart:io';
```

dart:io 提供了 各種 io 接口

# ServerSocket Socket

* ServerSocket 實現了 tcp 服務器
* Socket 實現了 tcp 客戶端

```dart
import 'dart:io';
import 'dart:convert';
import 'dart:async';

void main() async {
  try {
    // 監聽 端口
    final l = await ServerSocket.bind(InternetAddress.anyIPv4, 1102);
    print("tcp work at :1102");

    // 等待 連接
    l.listen((s) {
      // 處理 請求
      done(l, s);
    });

    // 模擬 客戶端
    runClient();
  } on SocketException catch (e) {
    print(e.toString());
  }
}

Future<void> _sleep(Duration duration) async {
  final controller = StreamController<int>();
  Timer(duration, () {
    controller.close();
  });
  await for (var _ in controller.stream) {}
}

Future<void> runClient() async {
  Socket s;

  try {
    s = await Socket.connect("localhost", 1102);
    s.listen((data) {
      print("client read ${utf8.decode(data)}");
    });
    for (var i = 0; i < 5; i++) {
      // 發送數據
      s.add(utf8.encode("data $i"));

      await _sleep(Duration(seconds: 1));
    }
  } on SocketException catch (e) {
    print(e.toString());
  }
  s?.close();
}

Future<void> done(ServerSocket l, Socket s) async {
  final addr = "${s.remoteAddress.address}:${s.remotePort}";
  print("one in $addr");
  try {
    // 讀取 數據
    // s.listen(onData)
    await for (var data in s) {
      final str = utf8.decode(data);
      print("from $addr : $str");

      // 關閉 服務器
      if (str == "exit") {
        l.close();
        break;
      }

      // 發送數據
      s.add(data);
    }
  } on SocketException catch (e) {
    print(e.toString());
  }
  await s.close();
  print("one out $addr");
}
```

# SecureServerSocket SecureSocket
SecureServerSocket SecureSocket 派生自 ServerSocket Socket 提供了對 tls 的支持

```
import 'dart:io';
import 'dart:convert';
import 'dart:async';

void main() async {
  try {
    /// 設置 tls 環境
    final context = SecurityContext()
      ..useCertificateChain("test.pem")
      ..usePrivateKey("test.key");
    // 監聽 端口
    final l =
        await SecureServerSocket.bind(InternetAddress.anyIPv4, 1102, context);
    print("tcp work at :1102");

    // 等待 連接
    l.listen((s) {
      // 處理 請求
      done(l, s);
    });

    // 模擬 客戶端
    runClient();
  } on SocketException catch (e) {
    print(e.toString());
  }
}

Future<void> _sleep(Duration duration) async {
  final controller = StreamController<int>();
  Timer(duration, () {
    controller.close();
  });
  await for (var _ in controller.stream) {}
}

Future<void> runClient() async {
  SecureSocket s;

  try {
    s = await SecureSocket.connect(
      "localhost",
      1102,
      onBadCertificate: (certificate) => true, // 忽略證書驗證
    );
    s.listen((data) {
      print("client read ${utf8.decode(data)}");
    });
    for (var i = 0; i < 5; i++) {
      // 發送數據
      s.add(utf8.encode("data $i"));

      await _sleep(Duration(seconds: 1));
    }
  } on SocketException catch (e) {
    print(e.toString());
  }
  s?.close();
}

Future<void> done(SecureServerSocket l, SecureSocket s) async {
  final addr = "${s.remoteAddress.address}:${s.remotePort}";
  print("one in $addr");
  try {
    // 讀取 數據
    // s.listen(onData)
    await for (var data in s) {
      final str = utf8.decode(data);
      print("from $addr : $str");

      // 關閉 服務器
      if (str == "exit") {
        l.close();
        break;
      }

      // 發送數據
      s.add(data);
    }
  } on SocketException catch (e) {
    print(e.toString());
  }
  await s.close();
  print("one out $addr");
}
```
# HttpServer HttpRequest WebSocket
* HttpServer HttpRequest 實現了 htp 服務器
* WebSocket 提供了 websocket 的 客戶端 服務器 實現

```
import 'dart:io';
import 'dart:convert';

void main() async {
  /// 創建 http 服務器
  /// bindSecure 創建 https 服務器
  var server = await HttpServer.bind(
    InternetAddress.loopbackIPv4,
    4040,
  );
  print('Listening on ${server.address}:${server.port}');

  /// 註冊路由
  final router = {
    '/': (HttpRequest request) async {
      final response = request.response;

      /// 返回 html
      response
        ..headers.contentType = ContentType.html
        ..write(
            '<html><header><title>測試</title></header><body>cerberus is an idea</body></html>');
      await response.close();
    },
    '/text': (HttpRequest request) async {
      final response = request.response;

      /// 返回 文本
      response
        ..headers.contentType = ContentType.text
        ..write('文本測試');
      await response.close();
    },
    '/xml': (HttpRequest request) async {
      final response = request.response;

      /// 返回 xml
      response
        ..headers.contentType =
            ContentType("application", "xml", charset: "utf-8")
        ..write('''<?xml version="1.0" encoding="UTF-8"?>
<urls>
  <url>google.com</url>
  <url>book.king011.com</url>
</urls>''');
      await response.close();
    },
    '/json': (HttpRequest request) async {
      final response = request.response;

      /// 返回 json
      response
        ..headers.contentType = ContentType.json
        ..write('''{
  "urls" : [
    "google.com",
    "book.king011.com"
  ]
}''');
      await response.close();
    },
    '/cerberus.png': (HttpRequest request) async {
      final response = request.response;
      try {
        final stream = File('cerberus.png').openRead();

        // /// 返回 二進制
        // response.headers.contentType = ContentType.binary;
        /// 返回 圖像
        response.headers.contentType = ContentType('image',
            'png'); // ContentType('image', 'jpeg'); // ContentType('image', 'gif');
        await response.addStream(stream);
      } catch (e) {
        response
          ..statusCode = HttpStatus.internalServerError
          ..write(e.toString());
      }
      await response.close();
    },
    '/ws': (HttpRequest request) async {
      final response = request.response;

      /// 升級到 websocket
      WebSocketTransformer.upgrade(request).then(
        (websocket) {
          websocket.listen(
            (data) {
              print("ws server get : ${utf8.decode(data)}");
              websocket.add(data);
            },
            onError: (e) {
              print('ws server error : $e');
            },
            onDone: () {
              websocket.close();
            },
          );
        },
        onError: (e) {
          response
            ..statusCode = HttpStatus.internalServerError
            ..write(e.toString());
          response.close();
        },
      );
    }
  };

  runWebsocketClient();

  /// 響應請求
  await for (HttpRequest request in server) {
    final f = router[request.uri.path];
    if (f == null) {
      final response = request.response;
      response
        ..statusCode = HttpStatus.notFound
        ..headers.contentType = ContentType.text
        ..write('Not found : ${request.uri.path}');
      await response.close();
    } else {
      await f(request);
    }
  }
}

runWebsocketClient() {
  WebSocket.connect("ws://127.0.0.1:4040/ws").then(
    (websocket) {
      websocket.add(utf8.encode("cerberus is an idae"));
      websocket.listen(
        (data) {
          print("ws client get : ${utf8.decode(data)}");
          websocket.close();
        },
        onError: (e) {
          print('ws client error : $e');
        },
        onDone: () {
          websocket.close();
        },
      );
    },
    onError: (e) {
      print("ws connect error : $e");
    },
  );
}
```