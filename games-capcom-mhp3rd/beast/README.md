# 土砂龍
![](assets/土砂龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎<br>×</center></div>
</td><td><div><center>×<br>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×<br>△</center></div>
</td><td><div><center>前爪</center></div>
</td><td><div><center>身体<br>前爪</center></div>
</td><td><div><center>前爪<br>尾巴</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 22 </td><td> 25 </td><td> 15 </td><td> 40 (0) </td><td> 0 (50) </td><td> 0 </td><td> 20 </td><td> 10<br>(0) </td><td> 100 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 35 </td><td> <font color="#ff0000">45 </font></td><td> 40 </td><td> 25 (0) </td><td> 0 (30) </td><td> 0 </td><td> 10 </td><td> 5<br>(0) </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 45 </td><td> 20 (0) </td><td> 0 (25) </td><td> 0 </td><td> 10 </td><td> 5<br>(0) </td><td> 0 </td><td> 100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 35 </td><td> 30 </td><td> 35 </td><td> 20 (0) </td><td> 0 (25) </td><td> 0 </td><td> 10 </td><td> 5<br>(0) </td><td> 0 </td><td> 左100/右100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 40 </td><td> 40 </td><td> <font color="#ff0000">50 </font></td><td> 25 (0) </td><td> 0 (30) </td><td> 0 </td><td> 20 </td><td> 10<br>(0) </td><td> 0 </td><td> 150
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 180 </td><td> 180 </td><td> 90 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 150 (750) </td><td> 120 (660) </td><td> 120 (660) </td><td> 100 (490) </td><td> 100 (550)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 10/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 40秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 200 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★4（容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="土砂龙的甲壳">土砂龙的甲壳</a> <font color="#008000"><b>46%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a> <font color="#008000"><b>29%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="土砂龙的头壳">土砂龙的头壳</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E7%88%AA" title="土砂龙的爪">土砂龙的爪</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="土砂龙的坚壳">土砂龙的坚壳</a> <font color="#008000"><b>44%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="土砂龙的坚甲">土砂龙的坚甲</a> <font color="#008000"><b>28%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="土砂龙的锐爪">土砂龙的锐爪</a> <font color="#008000"><b>18%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="土砂龙的头壳">土砂龙的头壳</a> <font color="#008000"><b>10%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="土砂龙的尾巴">土砂龙的尾巴</a> <font color="#008000"><b>70%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a> <font color="#008000"><b>30%</b></font> </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="土砂龙的尾巴">土砂龙的尾巴</a> <font color="#008000"><b>55%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="土砂龙的坚甲">土砂龙的坚甲</a> <font color="#008000"><b>32%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a> <font color="#008000"><b>10%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 1 </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="土砂龙的头壳">土砂龙的头壳</a> <font color="#008000"><b>80%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a> <font color="#008000"><b>20%</b></font> </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="土砂龙的头壳">土砂龙的头壳</a> <font color="#008000"><b>58%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="土砂龙的坚甲">土砂龙的坚甲</a> <font color="#008000"><b>34%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a> <font color="#008000"><b>5%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> 打击属性积蓄到一定量破头
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a> <font color="#008000"><b>5%</b></font><br><a href="/%E8%82%A5%E6%B2%83%E7%9A%84%E6%B3%A5" title="肥沃的泥">肥沃的泥</a> <font color="#008000"><b>20%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E8%82%A5%E6%B2%83%E7%9A%84%E6%B3%A5" title="肥沃的泥">肥沃的泥</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="土砂龙的坚甲">土砂龙的坚甲</a> <font color="#008000"><b>10%</b></font> </td><td> 将土砂龙身上的泥土打掉落，有一定的几率掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E7%88%AA" title="土砂龙的爪">土砂龙的爪</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="土砂龙的甲壳">土砂龙的甲壳</a>*1 <font color="#008000"><b>35%</b></font> </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="土砂龙的锐爪">土砂龙的锐爪</a>*1 <font color="#008000"><b>62%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="土砂龙的坚壳">土砂龙的坚壳</a>*1 <font color="#008000"><b>23%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E7%88%AA" title="土砂龙的爪">土砂龙的爪</a>*2 <font color="#008000"><b>15%</b></font> </td><td> 破爪战斗胜利后奖励获得
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="土砂龙的头壳">土砂龙的头壳</a>*1 <font color="#008000"><b>33%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="土砂龙的尾巴">土砂龙的尾巴</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E8%82%A5%E6%B2%83%E7%9A%84%E6%B3%A5" title="肥沃的泥">肥沃的泥</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="土砂龙的甲壳">土砂龙的甲壳</a>*1 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="土砂龙的头壳">土砂龙的头壳</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="土砂龙的坚甲">土砂龙的坚甲</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="土砂龙的尾巴">土砂龙的尾巴</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E8%82%A5%E6%B2%83%E7%9A%84%E6%B3%A5" title="肥沃的泥">肥沃的泥</a>*4 <font color="#008000"><b>10%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="土砂龙的坚壳">土砂龙的坚壳</a>*1 <font color="#008000"><b>5%</b></font><br><a href="/%E5%9C%9F%E7%A0%82%E9%BE%99%E7%9A%84%E8%83%8C%E7%94%B2" title="土砂龙的背甲">土砂龙的背甲</a>*1 <font color="#008000"><b>5%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>砂原からの救援依頼
</p>
</td><td>
<p>讨伐一头土砂龙和一头<a href="/%E8%A7%92%E9%BE%99" title="角龙">角龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D2%E6%98%9F" title="集会所下位2星">集会所下位2星</a>
</p>
</td><td>
<p>土砂竜ボルボロス！
</p>
</td><td>
<p>讨伐一头土砂龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D3%E6%98%9F" title="集会所下位3星">集会所下位3星</a>
</p>
</td><td>
<p>土砂竜ボルボロス！
</p>
</td><td>
<p>讨伐一头土砂龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>竜骨結晶を納品せよ！
</p>
</td><td>
<p>缴纳10个<a href="/index.php?title=%E9%BE%99%E9%AA%A8%E7%BB%93%E6%99%B6&amp;action=edit&amp;redlink=1" class="new" title="龙骨结晶（尚未撰写）">龙骨结晶</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>角竜と土砂竜の猛攻！
</p>
</td><td>
<p>讨伐一头土砂龙和一头<a href="/%E9%BB%91%E8%A7%92%E9%BE%99" title="黑角龙">黑角龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>砂原の採取ツアー
</p>
</td><td>
<p>缴纳<a href="/index.php?title=%E7%8C%AB%E5%AE%85%E5%88%B8&amp;action=edit&amp;redlink=1" class="new" title="猫宅券（尚未撰写）">猫宅券</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>土砂竜ボルボロス！
</p>
</td><td>
<p>讨伐一头土砂龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D6%E6%98%9F" title="集会所上位6星">集会所上位6星</a>
</p>
</td><td>
<p>泥沼の双生児
</p>
</td><td>
<p>讨伐两头土砂龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>落石注意！？
</p>
</td><td>
<p>讨伐一头<a href="/%E8%B5%A4%E7%94%B2%E5%85%BD" title="赤甲兽">赤甲兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>今そこにある恐怖
</p>
</td><td>
<p>讨伐一头土砂龙、一头<a href="/%E8%B5%A4%E7%94%B2%E5%85%BD" title="赤甲兽">赤甲兽</a>、<a href="/%E9%A3%8E%E7%89%99%E9%BE%99" title="风牙龙">风牙龙</a>连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%A5%AE%E6%96%99%E4%BB%BB%E5%8A%A1" title="饮料任务">饮料任务</a>
</p>
</td><td>
<p>泥沼での狩猟
</p>
</td><td>
<p>讨伐一头土砂龙
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 向后退几步后使用突进攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 地面头槌 </td><td> 脑袋锤向地面来攻击，同时前方溅出泥巴，碰到会陷入带泥状态并有一定几率陷入潮湿异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前咬 </td><td> 前进二三步使用啃咬攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转身体180°使用甩尾攻击，可以甩2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩泥巴 </td><td> 晃动身体向四周甩下泥巴，被打中会陷入带泥状态并有一定几率陷入潮湿异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滚泥 </td><td> 跑去泥滩打滚，使自己进入带泥状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼
</td></tr></tbody></table>
# 冰碎龍
![](assets/冰碎龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎<br>○</center></div>
</td><td><div><center>×<br>△</center></div>
</td><td><div><center>△<br>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>×<br>△</center></div>
</td><td><div><center>前爪</center></div>
</td><td><div><center>身体<br>前爪</center></div>
</td><td><div><center>尾巴</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 20 </td><td> 22 </td><td> 13 </td><td> 25 (35) </td><td> 0 (0) </td><td> 20 (15) </td><td> 0 </td><td> 10 (0) </td><td> 100 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 35 </td><td> <font color="#ff0000">45 </font></td><td> 35 </td><td> 15 (25) </td><td> 0 (0) </td><td> 15 (10) </td><td> 0 </td><td> 5 (0) </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 40 </td><td> 10 (20) </td><td> 5 (0) </td><td> 20 10) </td><td> 0 </td><td> 5 (0) </td><td> 0 </td><td> 100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 30 </td><td> 27 </td><td> 20 </td><td> 10 (20) </td><td> 5 (0) </td><td> 25 (5)</td><td> 0 </td><td> 5 (0) </td><td> 0 </td><td> 左100/右100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 36 </td><td> 36 </td><td> <font color="#ff0000">45 </font></td><td> 15 (25) </td><td> 0 (0) </td><td> 20 10) </td><td> 0 </td><td> 10 (0) </td><td> 0 </td><td> 150
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 200 </td><td> 200 </td><td> 120 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 150 (750) </td><td> 140 (760) </td><td> 140 (760) </td><td> 100 (520) </td><td> 100 (550)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 10/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 40秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 200 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> 下位没有此怪物 </td><td> <a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冰碎龙的坚壳">冰碎龙的坚壳</a> <font color="#008000"><b>44%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="冰碎龙的坚甲">冰碎龙的坚甲</a> <font color="#008000"><b>28%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冰碎龙的锐爪">冰碎龙的锐爪</a> <font color="#008000"><b>18%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="冰碎龙的头壳">冰碎龙的头壳</a> <font color="#008000"><b>10%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> 下位没有此怪物 </td><td> <a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="冰碎龙的尾巴">冰碎龙的尾巴</a> <font color="#008000"><b>70%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="冰碎龙的坚甲">冰碎龙的坚甲</a> <font color="#008000"><b>27%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 1 </td><td> 下位没有此怪物 </td><td> <a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="冰碎龙的头壳">冰碎龙的头壳</a> <font color="#008000"><b>80%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="冰碎龙的坚甲">冰碎龙的坚甲</a> <font color="#008000"><b>17%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a> <font color="#008000"><b>3%</b></font> </td><td> 打击属性积蓄到一定量破头
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> 下位没有此怪物 </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%86%BB%E7%BB%93%E7%9A%84%E7%B2%98%E6%B6%B2%E5%9D%97" title="冻结的粘液块">冻结的粘液块</a> <font color="#008000"><b>15%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="冰碎龙的坚甲">冰碎龙的坚甲</a> <font color="#008000"><b>10%</b></font> </td><td> 将冰碎龙身上的冰块打掉落，有一定的几率掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 下位无此怪物 </td><td> <a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E9%94%90%E7%88%AA" title="冰碎龙的锐爪">冰碎龙的锐爪</a> <font color="#008000"><b>65%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冰碎龙的坚壳">冰碎龙的坚壳</a> <font color="#008000"><b>35%</b></font> </td><td> 破爪战斗胜利后奖励获得
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> <a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="冰碎龙的头壳">冰碎龙的头壳</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E7%94%B2" title="冰碎龙的坚甲">冰碎龙的坚甲</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="冰碎龙的尾巴">冰碎龙的尾巴</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E5%86%BB%E7%BB%93%E7%9A%84%E7%B2%98%E6%B6%B2%E5%9D%97" title="冻结的粘液块">冻结的粘液块</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E5%86%B0%E7%A2%8E%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="冰碎龙的坚壳">冰碎龙的坚壳</a>*1 <font color="#008000"><b>5%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>雪原のスノーダンパー
</p>
</td><td>
<p>讨伐一头冰碎龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>凍土戦線・氷塊あり！
</p>
</td><td>
<p>讨伐一头<a href="/%E5%86%B0%E7%89%99%E9%BE%99" title="冰牙龙">冰牙龙</a>和一头冰碎龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>凍土に放り込まれたものたち
</p>
</td><td>
<p>两头<a href="/%E7%9C%A0%E7%8B%97%E9%BE%99" title="眠狗龙">眠狗龙</a>、一头冰碎龙、一头<a href="/%E5%86%BB%E6%88%88%E9%BE%99" title="冻戈龙">冻戈龙</a>连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>選ばれし者とは
</p>
</td><td>
<p>一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>、一头<a href="/%E5%86%BB%E6%88%88%E9%BE%99" title="冻戈龙">冻戈龙</a>、一头<a href="/%E9%BB%91%E8%BD%B0%E9%BE%99" title="黑轰龙">黑轰龙</a>、一头冰碎龙连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 向后退几步后使用突进攻击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车U </td><td> 沿U字形轨道进行的突进攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 地面头槌 </td><td> 脑袋锤向地面来攻击，同时前方散出雪块，碰到会陷入带雪状态并有一定几率陷入冰冻异常，可以槌2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前咬 </td><td> 前进二三步使用啃咬攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转身体180°使用甩尾攻击，可以甩2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩泥巴 </td><td> 晃动身体向四周甩下雪块，被打中会陷入带雪状态并有一定几率陷入冰冻异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 滚雪 </td><td> 跑去雪地打滚，使自己进入带雪状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人火怒时使用的威吓攻击，属于低吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身使用身体撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铲冰块 </td><td> 用头从雪地以左中右3方向铲出3块冰块
</td></tr></tbody></table>

# 爆鎚龍
![](assets/爆鎚龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○<br>◎</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>头(颚部破坏后)<br>腹部</center></div>
</td><td><div><center>头(颚部破坏后)<br>腹部</center></div>
</td><td><div><center>腹部</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 19 <font color="#ff0000">(55) </font></td><td> 20 <font color="#ff0000">(50) </font></td><td> 10 (40) </td><td> 0 </td><td> 40 (20) </td><td> 5 (15) </td><td> 20 (30) </td><td> 30 (35) </td><td> 100 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 25 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 20 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 22 </td><td> 24 </td><td> 10 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 0 </td><td> 30 </td><td> 5 </td><td> 15 </td><td> 30 </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 25 </td><td> 30 </td><td> 20 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 25 </td><td> 30 </td><td> 20 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 左260/右260
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 35 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 22 </td><td> 24 </td><td> 10 </td><td> 0 </td><td> 35 </td><td> 5 </td><td> 15 </td><td> 25 </td><td> 0 </td><td> 300
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 200 </td><td> 250 </td><td> 180 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 75 (450) </td><td> 100 (600) </td><td> 50 (450) </td><td> 180 (900) </td><td> 100 (650)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 10/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/15秒 </td><td> 10/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 30秒 </td><td> 15秒 </td><td> 30秒 </td><td> 15秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 300 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回20秒（疲劳30秒） → 2回10秒（疲劳20秒） → 3回8秒（疲劳10秒） → 4回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回10秒（疲劳20秒） → 2回5秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+5秒、发怒-5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★5（稍微容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 4 </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%B3%9E" title="爆锤龙的鳞">爆锤龙的鳞</a> <font color="#008000"><b>46%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="爆锤龙的甲壳">爆锤龙的甲壳</a> <font color="#008000"><b>34%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%A2%9A" title="爆锤龙的颚">爆锤龙的颚</a> <font color="#008000"><b>12%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a> <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="爆锤龙的上鳞">爆锤龙的上鳞</a> <font color="#008000"><b>42%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="爆锤龙的坚壳">爆锤龙的坚壳</a> <font color="#008000"><b>28%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a> <font color="#008000"><b>12%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%A2%9A" title="爆锤龙的颚">爆锤龙的颚</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>8%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%B3%9E" title="爆锤龙的鳞">爆锤龙的鳞</a> <font color="#008000"><b>58%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a> <font color="#008000"><b>32%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="爆锤龙的上鳞">爆锤龙的上鳞</a> <font color="#008000"><b>58%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a> <font color="#008000"><b>23%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>14%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a> <font color="#008000"><b>5%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>74%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a> <font color="#008000"><b>18%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a> <font color="#008000"><b>13%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a> <font color="#008000"><b>4%</b></font> </td><td> 将其身体部位打出硬直时，可能掉落
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体<br>挖掘 </td><td> 3 </td><td> <a href="/%E7%87%83%E7%9F%B3%E7%82%AD" title="燃石炭">燃石炭</a> <font color="#008000"><b>50%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a> <font color="#008000"><b>43%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>7%</b></font> </td><td> <a href="/%E5%BC%BA%E7%87%83%E7%9F%B3%E7%82%AD" title="强燃石炭">强燃石炭</a> <font color="#008000"><b>40%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a> <font color="#008000"><b>35%</b></font><br><a href="/%E7%87%83%E7%9F%B3%E7%82%AD" title="燃石炭">燃石炭</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a> <font color="#008000"><b>5%</b></font> </td><td> 将爆锤龙击倒之后，在其背部挖掘。（需携带采矿镐）
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头部（颚） </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%A2%9A" title="爆锤龙的颚">爆锤龙的颚</a>*1 <font color="#008000"><b>74%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="爆锤龙的甲壳">爆锤龙的甲壳</a>*1 <font color="#008000"><b>16%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%A2%9A" title="爆锤龙的颚">爆锤龙的颚</a>*1 <font color="#008000"><b>57%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="爆锤龙的坚壳">爆锤龙的坚壳</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="爆锤龙的甲壳">爆锤龙的甲壳</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a>*2 <font color="#008000"><b>5%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%B3%9E" title="爆锤龙的鳞">爆锤龙的鳞</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="爆锤龙的上鳞">爆锤龙的上鳞</a>*2 <font color="#008000"><b>58%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a>*1 <font color="#008000"><b>16%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%B3%9E" title="爆锤龙的鳞">爆锤龙的鳞</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*4 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="爆锤龙的甲壳">爆锤龙的甲壳</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%B3%9E" title="爆锤龙的鳞">爆锤龙的鳞</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%A2%9A" title="爆锤龙的颚">爆锤龙的颚</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a>*1 <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="爆锤龙的坚壳">爆锤龙的坚壳</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="爆锤龙的上鳞">爆锤龙的上鳞</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%A2%9A" title="爆锤龙的颚">爆锤龙的颚</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*4 <font color="#008000"><b>12%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="爆锤龙的耐热壳">爆锤龙的耐热壳</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>威胁！火山的铁锤！
</p>
</td><td>
<p>讨伐一头爆锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A15%E6%98%9F" title="村长任务5星">村长任务5星</a>
</p>
</td><td>
<p>请注意脚下
</p>
</td><td>
<p>讨伐15头<a href="/%E7%86%94%E5%B2%A9%E5%85%BD" title="熔岩兽">熔岩兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>火山骚动！
</p>
</td><td>
<p>讨伐2头爆锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>威胁！火山的铁锤！
</p>
</td><td>
<p>讨伐一头爆锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D4%E6%98%9F" title="集会所下位4星">集会所下位4星</a>
</p>
</td><td>
<p>挖掘吧！燃石炭！
</p>
</td><td>
<p>缴纳20个<a href="/%E7%87%83%E7%9F%B3%E7%82%AD" title="燃石炭">燃石炭</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>火山炎上！
</p>
</td><td>
<p>讨伐一头爆锤龙和<a href="/%E7%82%8E%E6%88%88%E9%BE%99" title="炎戈龙">炎戈龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>为了吾主前往火山吧！
</p>
</td><td>
<p>讨伐一头<a href="/%E6%AF%92%E7%8B%97%E9%BE%99" title="毒狗龙">毒狗龙</a>和爆锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>火山车撤
</p>
</td><td>
<p>讨伐一头爆锤龙和一头<a href="/%E9%92%A2%E9%94%A4%E9%BE%99" title="钢锤龙">钢锤龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>火山の採取ツアー
</p>
</td><td>
<p>缴纳<a href="/index.php?title=%E7%8C%AB%E5%AE%85%E5%88%B8&amp;action=edit&amp;redlink=1" class="new" title="猫宅券（尚未撰写）">猫宅券</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>脅威！火山の鉄槌！
</p>
</td><td>
<p>讨伐一头爆锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>決戦の火山
</p>
</td><td>
<p>讨伐一头<a href="/%E6%81%90%E6%9A%B4%E9%BE%99" title="恐暴龙">恐暴龙</a>和一头爆锤龙
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 对前侧方一定范围使用啃咬攻击，同时尾部也存在擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下颚震地 </td><td> 仰起上半身，用下颚猛地向地面敲击一下，附带地震效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 4连锤地 </td><td> 用下颚左右交替连续锤地四下
</td></tr>
<tr style="background:#f9f9f9;">
<td> 我是轮胎君 </td><td> 卷起身体呈轮胎状进行滚动攻击，分直线式，转弯式和追踪式
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身用身体撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转身体使用甩尾攻击，同时向前方甩出火药岩
</td></tr>
<tr style="background:#f9f9f9;">
<td> 炎热气体 </td><td> 全身释放出高温气体，范围内都会被打中并使人陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 催眠气体 </td><td> 全身释放出催眠气体，范围内都会被打中并使人陷入睡眠异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼（吼完的锤地无攻击判定）
</td></tr></tbody></table>

# 鋼鎚龍
![](assets/鋼鎚龍.jpeg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○<br>◎</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>头(颚部破坏后)<br>腹部</center></div>
</td><td><div><center>头(颚部破坏后)<br>腹部</center></div>
</td><td><div><center>腹部</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 19 <font color="#ff0000">(55) </font></td><td> 20 <font color="#ff0000">(50) </font></td><td> 10 (40) </td><td> 0 </td><td> 40 (20) </td><td> 5 (15) </td><td> 20 (30) </td><td> 30 (35) </td><td> 100 </td><td> 280
</td></tr>
<tr style="background:#f9f9f9;">
<td> 颈 </td><td> 25 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 20 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 22 </td><td> 24 </td><td> 10 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">45 </font></td><td> 0 </td><td> 30 </td><td> 5 </td><td> 15 </td><td> 30 </td><td> 0 </td><td> 500
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 25 </td><td> 30 </td><td> 15 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 左280/右280
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 35 </td><td> 30 </td><td> 25 </td><td> 0 </td><td> 25 </td><td> 0 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 22 </td><td> 24 </td><td> 10 </td><td> 0 </td><td> 35 </td><td> 5 </td><td> 15 </td><td> 25 </td><td> 0 </td><td> 300
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 250 </td><td> 250 </td><td> 180 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 75 (450) </td><td> 100 (650) </td><td> 50 (450) </td><td> 180 (900) </td><td> 75 (600)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 10/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/15秒 </td><td> 10/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 30秒 </td><td> 15秒 </td><td> 30秒 </td><td> 15秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 300 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回20秒（疲劳30秒） → 2回10秒（疲劳20秒） → 3回8秒（疲劳10秒） → 4回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回10秒（疲劳20秒） → 2回5秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+5秒、发怒-5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 4 </td><td> 下位无此怪物 </td><td> <a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="钢锤龙的上鳞">钢锤龙的上鳞</a> <font color="#008000"><b>42%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="钢锤龙的坚壳">钢锤龙的坚壳</a> <font color="#008000"><b>28%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="钢锤龙的耐热壳">钢锤龙的耐热壳</a> <font color="#008000"><b>12%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E5%88%9A%E9%A2%9A" title="钢锤龙的刚颚">钢锤龙的刚颚</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>8%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 1 </td><td> 下位无此怪物 </td><td> <a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="钢锤龙的上鳞">钢锤龙的上鳞</a> <font color="#008000"><b>58%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="钢锤龙的耐热壳">钢锤龙的耐热壳</a> <font color="#008000"><b>23%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>14%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a> <font color="#008000"><b>5%</b></font> </td><td> 斩属性武器断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> 下位无此怪物 </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a> <font color="#008000"><b>13%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a> <font color="#008000"><b>4%</b></font> </td><td> 将其身体部位打出硬直时，可能掉落
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体<br>挖掘 </td><td> 3 </td><td> 下位无此怪物 </td><td> <a href="/%E5%BC%BA%E7%87%83%E7%9F%B3%E7%82%AD" title="强燃石炭">强燃石炭</a> <font color="#008000"><b>40%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a> <font color="#008000"><b>35%</b></font><br><a href="/%E7%87%83%E7%9F%B3%E7%82%AD" title="燃石炭">燃石炭</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%BA%A2%E8%8E%B2%E7%9F%B3" title="红莲石">红莲石</a> <font color="#008000"><b>10%</b></font><br><a href="/%E7%8B%B1%E7%82%8E%E7%9F%B3" title="狱炎石">狱炎石</a> <font color="#008000"><b>5%</b></font> </td><td> 将爆锤龙击倒之后，在其背部挖掘。（需携带采矿镐）
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头部（颚） </td><td> 下位无此怪物 </td><td> <a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E5%88%9A%E9%A2%9A" title="钢锤龙的刚颚">钢锤龙的刚颚</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="钢锤龙的坚壳">钢锤龙的坚壳</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="钢锤龙的耐热壳">钢锤龙的耐热壳</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 下位无此怪物 </td><td> <a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="钢锤龙的上鳞">钢锤龙的上鳞</a>*2 <font color="#008000"><b>58%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="钢锤龙的耐热壳">钢锤龙的耐热壳</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*4 <font color="#008000"><b>8%</b></font> </td><td>
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> <a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="钢锤龙的坚壳">钢锤龙的坚壳</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E4%B8%8A%E9%B3%9E" title="钢锤龙的上鳞">钢锤龙的上鳞</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E5%88%9A%E9%A2%9A" title="钢锤龙的刚颚">钢锤龙的刚颚</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%92%A2%E9%94%A4%E9%BE%99%E7%9A%84%E8%80%90%E7%83%AD%E5%A3%B3" title="钢锤龙的耐热壳">钢锤龙的耐热壳</a>*2 <font color="#008000"><b>12%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E9%AA%A8%E9%AB%93" title="爆锤龙的骨髓">爆锤龙的骨髓</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E6%BA%B6%E5%B2%A9%E5%9D%97" title="溶岩块">溶岩块</a>*4 <font color="#008000"><b>10%</b></font><br><a href="/%E7%88%86%E9%94%A4%E9%BE%99%E7%9A%84%E7%BA%A2%E7%8E%89" title="爆锤龙的红玉">爆锤龙的红玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>火砕の轍
</p>
</td><td>
<p>讨伐一头<a href="/%E7%88%86%E9%94%A4%E9%BE%99" title="爆锤龙">爆锤龙</a>和一头钢锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>仄暗い火口の中から
</p>
</td><td>
<p>讨伐一头钢锤龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>クローベリング・タイム！
</p>
</td><td>
<p>讨伐一头钢锤龙、两头<a href="/%E6%AF%92%E7%8B%97%E9%BE%99" title="毒狗龙">毒狗龙</a>、一头<a href="/%E9%9B%84%E7%81%AB%E9%BE%99" title="雄火龙">雄火龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>火山の熱帯地より
</p>
</td><td>
<p>讨伐一头钢锤龙和一头<a href="/%E9%BB%91%E8%BD%B0%E9%BE%99" title="黑轰龙">黑轰龙</a>
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 对前侧方一定范围使用啃咬攻击，同时尾部也存在擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下颚震地 </td><td> 仰起上半身，用下颚猛地向地面敲击一下，附带地震效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 4连锤地 </td><td> 用下颚左右交替连续锤地四下
</td></tr>
<tr style="background:#f9f9f9;">
<td> 我是轮胎君 </td><td> 卷起身体呈轮胎状进行滚动攻击，分直线式，转弯式和追踪式
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身用身体撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转身体使用甩尾攻击，同时向前方甩出恶臭岩
</td></tr>
<tr style="background:#f9f9f9;">
<td> 恶臭气体 </td><td> 全身释放出恶臭气体，范围内都会被打中并使人陷入恶臭异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【小】 </td><td> 发现猎人或怒时使用的威吓攻击，属于低吼（吼完的锤地无攻击判定）
</td></tr></tbody></table>
# 尾槌龍

![](assets/尾槌龍.jpg)

## 基本抗性

◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>驼峰</center></div>
</td><td><div><center>驼峰</center></div>
</td><td><div><center>驼峰(驼峰破坏后)<br>尾巴</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 43 </td><td> 24 </td><td> 35 </td><td> 10 </td><td> 5 </td><td> 15 </td><td> 5 </td><td> 5 </td><td> 100 </td><td> 280
</td></tr>
<tr style="background:#f9f9f9;">
<td> 角 </td><td> 23 </td><td> 21 </td><td> 20 </td><td> 35 </td><td> 10 </td><td> 35 </td><td> 20 </td><td> 0 </td><td> 150 </td><td> 280
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> 25 </td><td> 35 </td><td> 25 </td><td> 15 </td><td> 5 </td><td> 10 </td><td> 15 </td><td> 10 </td><td> 0 </td><td> 400
</td></tr>
<tr style="background:#f9f9f9;">
<td> 驼峰 </td><td> <font color="#ff0000">60 (96) </font></td><td> <font color="#ff0000">60 (96) </font></td><td> 35 <font color="#ff0000">(60) </font></td><td> 20 (30) </td><td> 5 (10) </td><td> 0 (5) </td><td> 15 (20) </td><td> 10 (20) </td><td> 0 </td><td> 380
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 32 </td><td> 30 </td><td> 25 </td><td> 15 </td><td> 0 </td><td> 10 </td><td> 10 </td><td> 10 </td><td> 0 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 后脚 </td><td> 44 </td><td> 44 </td><td> 35 </td><td> 10 </td><td> 0 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 左200/右200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 32 </td><td> 40 </td><td> <font color="#ff0000">50 </font></td><td> 10 </td><td> 5 </td><td> 5 </td><td> 5 </td><td> 10 </td><td> 0 </td><td> 250
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾尖 </td><td> 24 </td><td> 43 </td><td> 40 </td><td> 20 </td><td> 5 </td><td> 5 </td><td> 15 </td><td> 20 </td><td> 0 </td><td> 170
</td></tr></tbody></table>

## 屬性攻擊

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 120 </td><td> 180 </td><td> 100 </td><td> 150 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 120 (600) </td><td> 90 (540) </td><td> 150 (700) </td><td> 150 (750) </td><td> 100 (700)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/15秒 </td><td> 10/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 120秒 </td><td> 10秒 </td><td> 60秒 </td><td> 15秒 </td><td> 75秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 410 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>

## 道具效果

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳16秒） → 2回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回12秒（疲劳15秒） → 2回7秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回5秒 （疲劳+5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★6（稍微难逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>

## 剝取 掉落

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 4 </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a> <font color="#008000"><b>46%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%8B%94%E7%94%B2" title="尾槌龙的苔甲">尾槌龙的苔甲</a> <font color="#008000"><b>40%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a> <font color="#008000"><b>4%</b></font> </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="尾槌龙的坚壳">尾槌龙的坚壳</a> <font color="#008000"><b>42%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E8%8B%94%E7%94%B2" title="尾槌龙的坚苔甲">尾槌龙的坚苔甲</a> <font color="#008000"><b>34%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a> <font color="#008000"><b>6%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a> <font color="#008000"><b>5%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E4%BB%99%E9%AA%A8" title="尾槌龙的仙骨">尾槌龙的仙骨</a> <font color="#008000"><b>3%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴(挖矿) </td><td> 1 </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="尾槌龙的尾甲">尾槌龙的尾甲</a> <font color="#008000"><b>78%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a> <font color="#008000"><b>16%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a> <font color="#008000"><b>6%</b></font> </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%B0%BE%E7%94%B2" title="尾槌龙的坚尾甲">尾槌龙的坚尾甲</a> <font color="#008000"><b>74%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="尾槌龙的坚壳">尾槌龙的坚壳</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a> <font color="#008000"><b>8%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="尾槌龙的尾甲">尾槌龙的尾甲</a> <font color="#008000"><b>4%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E4%BB%99%E9%AA%A8" title="尾槌龙的仙骨">尾槌龙的仙骨</a> <font color="#008000"><b>4%</b></font> </td><td> 将尾槌龙的尾部尾端骨锤打掉之后，剥取尾槌龙骨锤
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴(剥取) </td><td> 1 </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="尾槌龙的尾甲">尾槌龙的尾甲</a> <font color="#008000"><b>81%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a> <font color="#008000"><b>16%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a> <font color="#008000"><b>3%</b></font> </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%B0%BE%E7%94%B2" title="尾槌龙的坚尾甲">尾槌龙的坚尾甲</a> <font color="#008000"><b>77%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="尾槌龙的坚壳">尾槌龙的坚壳</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E7%94%B2" title="尾槌龙的尾甲">尾槌龙的尾甲</a> <font color="#008000"><b>6%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a> <font color="#008000"><b>5%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E4%BB%99%E9%AA%A8" title="尾槌龙的仙骨">尾槌龙的仙骨</a> <font color="#008000"><b>2%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落物 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%8B%94%E7%94%B2" title="尾槌龙的苔甲">尾槌龙的苔甲</a> <font color="#008000"><b>20%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a> <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%B0%BE%E7%94%B2" title="尾槌龙的坚尾甲">尾槌龙的坚尾甲</a> <font color="#008000"><b>18%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a> <font color="#008000"><b>7%</b></font> </td><td> 尾槌龙大回旋攻击中将其击倒，或是其疲劳状态时自己跌倒，有几率掉落
</td></tr></tbody></table>

## 部位破壞

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头部（角） </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%A7%92" title="尾槌龙的角">尾槌龙的角</a>*1 <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a>*1 <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%96%E8%A7%92" title="尾槌龙的尖角">尾槌龙的尖角</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="尾槌龙的坚壳">尾槌龙的坚壳</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%A7%92" title="尾槌龙的角">尾槌龙的角</a>*2 <font color="#008000"><b>10%</b></font> </td><td> 头部共可破两次，一只角一次，双角全破获取几率提升
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%8B%94%E7%94%B2" title="尾槌龙的苔甲">尾槌龙的苔甲</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a>*1 <font color="#008000"><b>13%</b></font> </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a>*1 <font color="#008000"><b>26%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E8%8B%94%E7%94%B2" title="尾槌龙的坚苔甲">尾槌龙的坚苔甲</a>*1 <font color="#008000"><b>26%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a>*2 <font color="#008000"><b>22%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="尾槌龙的坚壳">尾槌龙的坚壳</a>*1 <font color="#008000"><b>12%</b></font> </td><td> 将尾槌龙驼峰击破之后，在他下次倒地的时候就可以剥取了
</td></tr></tbody></table>

## 捕獲

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a>*1<br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%8B%94%E7%94%B2" title="尾槌龙的苔甲">尾槌龙的苔甲</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%A7%92" title="尾槌龙的角">尾槌龙的角</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a>*1 <font color="#008000"><b>6%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E7%94%B2%E5%A3%B3" title="尾槌龙的甲壳">尾槌龙的甲壳</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E8%8B%94%E7%94%B2" title="尾槌龙的坚苔甲">尾槌龙的坚苔甲</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%9D%9A%E5%A3%B3" title="尾槌龙的坚壳">尾槌龙的坚壳</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%96%E8%A7%92" title="尾槌龙的尖角">尾槌龙的尖角</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E8%8B%94%E7%94%B2" title="尾槌龙的苔甲">尾槌龙的苔甲</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E9%A9%BC%E5%B3%B0" title="尾槌龙的驼峰">尾槌龙的驼峰</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E5%B0%BE%E9%AA%A8" title="尾槌龙的尾骨">尾槌龙的尾骨</a>*1 <font color="#008000"><b>7%</b></font><br><a href="/%E5%B0%BE%E6%A7%8C%E9%BE%99%E7%9A%84%E4%BB%99%E9%AA%A8" title="尾槌龙的仙骨">尾槌龙的仙骨</a>*1 <font color="#008000"><b>5%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>

## 出現任務

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>渾身のドボルベルク
</p>
</td><td>
<p>讨伐一头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>ドボルベルク流域
</p>
</td><td>
<p>讨伐一头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>山の嵐、川の氾濫
</p>
</td><td>
<p>讨伐一头尾槌龙和一头<a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>月夜の暗躍
</p>
</td><td>
<p>讨伐一头<a href="/%E9%9B%8C%E7%81%AB%E9%BE%99" title="雌火龙">雌火龙</a>和一头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8B%E4%BD%8D5%E6%98%9F" title="集会所下位5星">集会所下位5星</a>
</p>
</td><td>
<p>ドボルベルク流域
</p>
</td><td>
<p>讨伐一头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>動くこと、山の如し！
</p>
</td><td>
<p>讨伐一头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>増援要求
</p>
</td><td>
<p>讨伐一头<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>和一头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>雨に煙る、双子の山
</p>
</td><td>
<p>讨伐两头尾槌龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>集え！水没林の大連続戦
</p>
</td><td>
<p>一头<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>、一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>、一头尾槌龙连续讨伐
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>狩人舞闘曲
</p>
</td><td>
<p>一头<a href="/%E6%AF%92%E6%80%AA%E9%BE%99" title="毒怪龙">毒怪龙</a>、一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>、一头尾槌龙连续讨伐
</p>
</td></tr></tbody></table>

## 攻擊方式

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 龙车 </td><td> 后退几步后突进攻击猎人，突进前蓄力短的话突进结束会接尾槌锤地，蓄力长的话结束时会接甩尾（擦伤判定）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头突 </td><td> 朝前侧方一定范围内使用头突攻击，同时尾部也有擦伤判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身用身体撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 后退几步后使用近360°的甩尾
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大回转 </td><td> 以身体为圆心尾槌为圆边，进行多次旋转，旋转结束后会接：1.身体飞向高空用整个身体压向猎人；2.低空以尾槌朝向猎人直撞过去；3.以滑行状态甩向猎人用尾槌撞击
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】</td><td> 发现猎人或怒时使用的威吓攻击，属于高吼
</td></tr>
<tr style="background:#f9f9f9;">
<td> 扣尾 </td><td> 背向猎人用尾槌敲击地面攻击，附带地震效果
</td></tr></tbody></table>

# 恐暴龍
![](assets/恐暴龍.jpeg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>△<br>○</center></div>
</td><td><div><center>△<br>○</center></div>
</td><td><div><center>○<br>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>○<br>◎</center></div>
</td><td><div><center>头(未发怒)<br>腹部</center></div>
</td><td><div><center>头<br>腹部</center></div>
</td><td><div><center>头(未发怒)<br>腹部</center></div>
</td><td><div><center>大</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>
## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">65</font> (40) </td><td> <font color="#ff0000">70 (45) </font></td><td> <font color="#ff0000">65</font> (40) </td><td> 10 (15) </td><td> 10 (15) </td><td> 15 (20) </td><td> 5 (10) </td><td> 15 (20) </td><td> 100 </td><td> 350
</td></tr>
<tr style="background:#f9f9f9;">
<td> 背部 </td><td> 25 (21) </td><td> 25 (21) </td><td> 25 (20) </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 (5) </td><td> 0 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 腹部 </td><td> <font color="#ff0000">50 (80) </font></td><td> <font color="#ff0000">45 (80) </font></td><td> <font color="#ff0000">45 (75) </font></td><td> 15 (20) </td><td> 15 (20) </td><td> 25 (35) </td><td> 10 (15) </td><td> 25 (35) </td><td> 0 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 前爪 </td><td> 30 </td><td> 30 </td><td> 30 </td><td> 10 (15) </td><td> 10 (15) </td><td> 15 (20) </td><td> 0 </td><td> 15 (20) </td><td> 0 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 40 </td><td> 35 </td><td> 35 (25) </td><td> 5 (10) </td><td> 5 (10) </td><td> 10 (15) </td><td> 0 (0) </td><td> 10 (15) </td><td> 0 </td><td> 左220/右220
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 30 </td><td> 30 </td><td> 30 </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 (5) </td><td> 0 </td><td> 180
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 180 </td><td> 180 </td><td> 180 </td><td> 216 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 200 (980) </td><td> 200 (980) </td><td> 200 (980) </td><td> 180 (936) </td><td> 150 (780)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 10/10秒 </td><td> 10/10秒 </td><td> 10/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 30秒 </td><td> 15秒 </td><td> 60秒 </td><td> 15秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 450 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回10秒（疲劳20秒） → 2回8秒（疲劳10秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回5秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回15秒 → 2回10秒 → 3回开始5秒（疲劳+15秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★8（非常难逃） 摆脱束缚攻击
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> ○ </td><td> 耐力值一定一下，不管是普通、发怒、疲劳都会去吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 4 </td><td> 下位无此怪物 </td><td> <a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%BB%91%E9%B3%9E" title="恐暴龙的黑鳞">恐暴龙的黑鳞</a> <font color="#008000"><b>39%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%BB%91%E7%9A%AE" title="恐暴龙的黑皮">恐暴龙的黑皮</a> <font color="#008000"><b>28%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%92%A9%E7%88%AA" title="恐暴龙的钩爪">恐暴龙的钩爪</a><font color="#008000"><b>20%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%A4%A7%E7%89%99" title="恐暴龙的大牙">恐暴龙的大牙</a> <font color="#008000"><b>11%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%AE%9D%E7%8E%89" title="恐暴龙的宝玉">恐暴龙的宝玉</a> <font color="#008000"><b>2%</b></font> </td><td> 剥取或奖励
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 2 </td><td> 下位无此怪物 </td><td> <a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%B0%BE%E5%B7%B4" title="恐暴龙的尾巴">恐暴龙的尾巴</a> <font color="#008000"><b>65%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%BB%91%E9%B3%9E" title="恐暴龙的黑鳞">恐暴龙的黑鳞</a> <font color="#008000"><b>33%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%AE%9D%E7%8E%89" title="恐暴龙的宝玉">恐暴龙的宝玉</a> <font color="#008000"><b>2%</b></font> </td><td> 斩属性断尾限定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> 1 </td><td> 下位无此怪物 </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>35%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%92%A9%E7%88%AA" title="恐暴龙的钩爪">恐暴龙的钩爪</a> <font color="#008000"><b>34%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%AE%9D%E7%8E%89" title="恐暴龙的宝玉">恐暴龙的宝玉</a> <font color="#008000"><b>1%</b></font> </td><td> 恐暴龙撞毁障碍，进入冻土隐藏区域时掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 下位无此怪物 </td><td> <a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%A4%A7%E7%89%99" title="恐暴龙的大牙">恐暴龙的大牙</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="恐暴龙的头壳">恐暴龙的头壳</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%A4%A7%E7%89%99" title="恐暴龙的大牙">恐暴龙的大牙</a>*2 <font color="#008000"><b>15%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%94%BE%E6%B6%B2" title="恐暴龙的唾液">恐暴龙的唾液</a>*1 </td><td> 头部需要破坏2次
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> <a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%A4%B4%E5%A3%B3" title="恐暴龙的头壳">恐暴龙的头壳</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%BB%91%E7%9A%AE" title="恐暴龙的黑皮">恐暴龙的黑皮</a>*1 <font color="#008000"><b>36%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E9%92%A9%E7%88%AA" title="恐暴龙的钩爪">恐暴龙的钩爪</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E6%81%90%E6%9A%B4%E9%BE%99%E7%9A%84%E5%AE%9D%E7%8E%89" title="恐暴龙的宝玉">恐暴龙的宝玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 体力%以下<br>体力%以下(上位)
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>
<p><b>任务种类</b>
</p>
</td><td>
<p><b>任务名称</b>
</p>
</td><td>
<p><b>内容</b>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>ジャギィノス掃討作戦
</p>
</td><td>
<p>讨伐25头<a href="/%E9%9B%8C%E7%8B%97%E9%BE%99" title="雌狗龙">雌狗龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>進め！凍土調査隊！
</p>
</td><td>
<p>讨伐20只<a href="/%E7%9C%A0%E7%8B%97%E9%BE%99" title="眠狗龙">眠狗龙</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D7%E6%98%9F" title="集会所上位7星">集会所上位7星</a>
</p>
</td><td>
<p>足下にはご注意を！
</p>
</td><td>
<p>讨伐15头<a href="/%E7%86%94%E5%B2%A9%E5%85%BD" title="熔岩兽">熔岩兽</a>
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>貪食の恐王
</p>
</td><td>
<p>讨伐一头恐暴龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>恐暴竜の根城
</p>
</td><td>
<p>讨伐一头恐暴龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E9%9B%86%E4%BC%9A%E6%89%80%E4%B8%8A%E4%BD%8D8%E6%98%9F" title="集会所上位8星">集会所上位8星</a>
</p>
</td><td>
<p>決戦の火山
</p>
</td><td>
<p>讨伐一头<a href="/%E7%88%86%E9%94%A4%E9%BE%99" title="爆锤龙">爆锤龙</a>和一头恐暴龙
</p>
</td></tr>
<tr style="background:#f9f9f9;">
<td>
<p><a href="/%E6%9D%91%E9%95%BF%E4%BB%BB%E5%8A%A16%E6%98%9F" title="村长任务6星">村长任务6星</a>
</p>
</td><td>
<p>終焉を喰らう者
</p>
</td><td>
<p>一头恐暴龙、一头<a href="/%E8%BD%B0%E9%BE%99" title="轰龙">轰龙</a>、一头<a href="/%E8%BF%85%E9%BE%99" title="迅龙">迅龙</a>连续讨伐
</p>
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 分前咬，侧咬，转身咬（攻击力较低，它疲劳时被打中会陷入防御力DOWN异常）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大前咬 </td><td> 向猎人走近几步后使用前咬（攻击力比普通啃咬高，它疲劳时被打中会陷入防御力DOWN异常）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 连续啃咬 </td><td> 大步前进并连续左右交替使用啃咬，离猎人越远啃咬次数越多
</td></tr>
<tr style="background:#f9f9f9;">
<td> 踏步 </td><td> 右脚猛地蹬地，右方判定范围大（并在身下及右方存在地震判定）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 旋转180°使用甩尾攻击，可以连续甩多次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 大回转 </td><td> 向前一大步并360°旋转身体，前半身和尾巴都存在伤害判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧身用身体撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 岩块 </td><td> 用头从地面铲出岩块投向前方，同时前身和尾巴也存在攻击判定
</td></tr>
<tr style="background:#f9f9f9;">
<td> 捕食 </td><td> 使用飞扑扑向猎人（落地带地震判定），扑中即进入捕食状态
</td></tr>
<tr style="background:#f9f9f9;">
<td> 咆哮【大】 </td><td> 发现猎人或怒时使用的威吓攻击，属于高吼
</td></tr></tbody></table>