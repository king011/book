# vim-plug

vim-plug 是一個 開源(MIT)高效的 vim 插件管理器 可以管理 vim 插件 按需加載 異步 多線程 下載更新 

* 源碼 [https://github.com/junegunn/vim-plug](https://github.com/junegunn/vim-plug)
# 安裝

```bash
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
# Exmaple

```
#info="~/.vimrc"

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" 確保使用 單引號配置項目

" 從 github 安裝下述插件 https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" 可以從任何 合法的 git 地址 安裝插件
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" 可以在一行中 編寫多個 命令 使用 | 分隔
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" 按需加載插件
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" 可以指定 要安裝的 插件分支
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" 運行指定 安裝插件的指定 tag ; 運行使用通配符 但git要高於 1.9.2
Plug 'fatih/vim-go', { 'tag': '*' }

" 指定插件選項
Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" 非託管插件 需要手動安裝和更新
Plug '~/my-prototype-plugin'

" 初始化 插件系統
call plug#end()
```

# 指令

| 指令 | 含義 |
| -------- | -------- |
| PlugInstall [name ...] [#threads]     | 安裝插件     |
| PlugUpdate [name ...] [#threads]     | 安裝或更新插件     |
| PlugClean[!]     | 刪除未列出插件     |
| PlugUpgrade     | 更新 vim-plug 自己     |
| PlugStatus     | 檢查插件狀態     |
| PlugDiff     | 檢查插件版本變化     |
| PlugSnapshot[!] [output path]     | 生成插件還原快照     |

