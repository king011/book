# TiledObjectGroup

[cc.TiledObjectGroup](http://docs.cocos.com/creator/api/zh/classes/TiledObjectGroup.html) 封裝 對象層

```
#info=false
// 返回 對象
getObjects(): any[]
getObject(objectName: string): any
```

# getObject

getObject 返回的 一個 any 包含如下屬性

| 屬性 | 型別 | 值 |
| -------- | -------- | -------- |
| id     | string     |      |
| name     | string     | 對象名稱     |
| type     | number     |      |
| rotation     | number     |      |
| visible     | boolean     | 是否可見     |
| width     | number     | 寬     |
| height     | number     | 高     |
| x     |  number    | 對象左上角 x     |
| y     | number     | 對象左上角 y     |
| offset     | cc.Vec2     | 對象左上角 座標     |
| ...     | ...     | 自定義的 對象屬性     |

* x y 的座標原點是在 左下角 和 世界座標系 保持一致
* offset 的座標原點是在 左上角 和 TiledMap 保持一致

自定義屬性 最好以 **\_** 開始 否則 creator 可能不會解析 目前發現如下情況 不會 將自定義屬性 加入 getObject返回的 對象 屬性中

* 字符串型別
* 和 creator 定義的 已有屬性 重名

