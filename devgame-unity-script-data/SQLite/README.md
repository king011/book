# SQLite

unity 沒有直接提供內置的 SQLite 支持，不過 Mono 平臺已經提供了對 SQLite 的支持

[https://github.com/zuiwuchang/example/tree/main/unity/sqlite](https://github.com/zuiwuchang/example/tree/main/unity/sqlite) 這是後續例子的完整項目


1. 複製 Editor 路徑下的  **Editor/Data/MonoBleedingEdge/lib/mono/unityjit/Mono.Data.Sqlite.dll** 到項目的 **Assets/Plugins/Mono.Data.Sqlite.dll**

	* Assets/Plugins 是一個特殊的檔案夾，在裏面可以放置特定平臺的資源或原生代碼，unity 會將其打包的對應平臺
	* Mono.Data.Sqlite 是 Mono 開源架構提供的 c SQLite 在 c# 環境中的庫

2. 將對應平臺的 libsqlite3 庫拷貝到 **Assets/Plugins** 檔案夾

	* 例如 -> 對於 andorind 平臺下載 [android.libsqlite3.tar.gz](assets/android.libsqlite3.tar.gz) 並且將其解壓的 **Assets/Plugins/Android/libs**，當然你也可以從 [SQLite 官網](https://www.sqlite.org/download.html) 下載源碼自己爲 android 編譯庫
		* md5 **8a9c54fbbbd8019962b100b14fbb755c**  android.libsqlite3.tar.gz 
		* sha **d377167dfd31a8285bba9059a5d04a0799a203de**  android.libsqlite3.tar.gz
		* sha512 **0d965fd2418df646b1ac66ba7eceb7dd9400087568f9e571ca94746b535baf4882e3c0a86bb64a8f1e19efccd7a7d86ea0971f14d76584e04c1db92826a8d4ad**  android.libsqlite3.tar.gz
		* ![](assets/android.png)

3. 配置好了依賴後就可以使用 Mono 提供的 api 訪問數據庫

```
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.Sqlite;
public static class DbConnectionExtend
{
    public delegate T Execute<T>();
    public delegate void Execute();
    public static T Transaction<T>(this SqliteConnection db, Execute<T> f)
    {
        SqliteTransaction transaction = db.BeginTransaction();
        try
        {
            T result = f();
            transaction.Commit();
            return result;
        }
        catch (System.Exception)
        {
            transaction.Rollback();
            throw;
        }
    }
    public static void Transaction(this SqliteConnection db, Execute f)
    {
        SqliteTransaction transaction = db.BeginTransaction();
        try
        {
            f();
            transaction.Commit();
        }
        catch (System.Exception)
        {
            transaction.Rollback();
            throw;
        }
    }
}


public class Scripts : MonoBehaviour
{
    public string value;
    public Text message;
    public string dbname = "test.db";
    public string connectionString
    {
        get
        {
            var str = System.IO.Path.Combine(Application.persistentDataPath, dbname);
            // str = ":memory:";
            str = "URI=file:" + str;
            return str;
        }
    }
    private SqliteConnection _conn;
    private void OnDestroy()
    {
        _conn?.Close();
    }
    private void SetError(System.Exception e)
    {
        var msg = $"error: {e}";
        Debug.LogWarning(msg);
        message.text = msg;
    }

    public void OnClickOpen()
    {
        if (_conn != null)
        {
            message.text = "error: already opened";
            return;
        }
        try
        {
            message.text = $"connecting: {connectionString}";
            _conn = new SqliteConnection(connectionString);
            _conn.Open();
            message.text = $"connected: {connectionString}";
        }
        catch (System.Exception e)
        {
            if (_conn != null)
            {
                _conn.Close();
                _conn = null;
            }
            SetError(e);
        }
    }
    public void OnClickClose()
    {
        if (_conn == null)
        {
            message.text = "error: already closed";
        }
        else
        {
            _conn.Close();
            _conn = null;
            message.text = "closed";
        }
    }
    public void OnClickCreateTable()
    {
        try
        {
            SqliteCommand commnad = _conn.CreateCommand();
            commnad.CommandText = "CREATE TABLE IF NOT EXISTS List (id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR(20) UNIQUE NOT NULL)";
            int rows = commnad.ExecuteNonQuery();
            message.text = $"Create List; rows={rows}";
        }
        catch (System.Exception e)
        {
            SetError(e);
        }
    }
    public void OnClickDropTable()
    {
        try
        {
            SqliteCommand commnad = _conn.CreateCommand();
            commnad.CommandText = "DROP TABLE IF EXISTS List";
            int rows = commnad.ExecuteNonQuery();
            message.text = $"DROP List; rows={rows}";
        }
        catch (System.Exception e)
        {
            SetError(e);
        }
    }
    public void OnClickInsert()
    {
        try
        {
            var id = _conn.Transaction(() =>
              {
                  SqliteCommand commnad = _conn.CreateCommand();
                  commnad.Parameters.AddWithValue("@name", value);
                  commnad.CommandText = "INSERT INTO List (name) VALUES (@name); SELECT LAST_INSERT_ROWID();";
                  var id = (long)commnad.ExecuteScalar();
                  return id;
              });
            message.text = $"INSERT id: {id}";
        }
        catch (System.Exception e)
        {
            SetError(e);
        }
    }
    public void OnClickQuery()
    {
        try
        {
            SqliteCommand commnad = _conn.CreateCommand();
            commnad.Parameters.AddWithValue("@name", value);
            commnad.CommandText = "SELECT id FROM List WHERE name = @name";
            SqliteDataReader reader = commnad.ExecuteReader();
            while (reader.Read())
            {
                // var id = reader["id"];
                var id = reader.GetInt64(0);
                message.text = $"{value}'s id is {id}";
                return;
            }
            message.text = $"not found: {value}";
        }
        catch (System.Exception e)
        {
            SetError(e);
        }
    }
    public void OnClickDelete()
    {
        try
        {
            SqliteCommand commnad = _conn.CreateCommand();
            commnad.CommandText = "DELETE FROM List WHERE name = @name";
            commnad.Parameters.AddWithValue("@name", value);
            int rows = commnad.ExecuteNonQuery();
            message.text = $"DELETE rows: {rows}";
        }
        catch (System.Exception e)
        {
            SetError(e);
        }
    }
}
```
		