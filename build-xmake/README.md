# xmake

xmake 是一個 開源的( Apache-2.0 license) 跨平臺 自動化建構系統

通常用它來編譯 c/c++ 代碼，是 CMake 的一個可選替代，提供了更加簡單好用的語法(直接使用 lua)，並且提供了配套的包管理工具

* 官網 [https://xrepo.xmake.io/](https://xrepo.xmake.io/)
* 源碼 [https://github.com/xmake-io/xmake](https://github.com/xmake-io/xmake)
* 軟件包 [https://github.com/xmake-io/xmake-repo](https://github.com/xmake-io/xmake-repo)
* 文檔 [https://xmake.io/#/zh-cn/guide/installation](https://xmake.io/#/zh-cn/guide/installation)