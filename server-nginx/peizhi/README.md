#  location

location 用來 配置 如何被 處理

## root alias

root alias 用來 確定 最終url  
**如果 配置的 rott alias 不是一個 根目錄 要使用 絕對路徑**

下面 兩個 配置 都是 一樣  
如果訪問 http://XXX/abc/a.html nginx 會返回  /home/html/abc/a.html

```
location /abc/ {
    alias /home/html/abc/;
}
```
```
location /abc/ {
    root /home/html/;
}
```

* root 指定的是 根本 目錄 所以 要用 根目錄 + url
* alias 是別名 所以 要用 別名 替換掉 原url中 的 名稱

## url 匹配

多個 location 匹配 優先級 和 出現的先後 順序無關



| 匹配模式 | 含義 | 
| -------- | -------- |
| =     | 字符串 精確匹配     |
| ^~     | 匹配普通字符串     |
| ~*     | 匹配正則 但不區分大小寫     |
| ~     | 匹配正則     |

1. **= **被優先匹配 一旦匹配到 返回
2. 匹配常規 字符串 中 最長的<br>如果 使用 **^〜** 前缀 匹配到**^〜**後直接返回
3. 安 配置順序 匹配 正則
4. 返回 2 3 中 最長的 匹配項目

```
location  = / {
  # 只匹配"/".
  [ configuration A ] 
}
location  / {
  # 匹配任何请求，因为所有请求都是以"/"开始
  # 但是更长字符匹配或者正则表达式匹配会优先匹配
  [ configuration B ] 
}
location ^~ /images/ {
  # 匹配任何以 /images/ 开始的请求，并停止匹配 其它location
  [ configuration C ] 
}
location ~* .(gif|jpg|jpeg)$ {
  # 匹配以 gif, jpg, or jpeg结尾的请求. 
  # 但是所有 /images/ 目录的请求将由 [Configuration C]处理.   
  [ configuration D ] 
}
```

# 配置 http

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
 
events {
	worker_connections 768;
}
 
http {
	# 配置一個 服務器
	server{
		# 監聽地址
		listen 127.0.0.1:9000;
		server_name test;
		location / {
			# 配置 index 頁面
			index index.html;
			# 配置 http root 目錄 
			root /web/view;
		}
		# 使用正則 將 圖片 綁定到 其它目錄
		# 使用 ~ 開頭 代表 使用 正則 匹配 url
		# (?i) 忽略 大小寫
		location ~(?i)\.((jpg)|(png)|(gif))$ {
			root /web/imgs;
 
			# 啓用 緩存 緩存時間爲 30天
			expires 30d;
		}
		# 爲 js css 建立 緩存
		location ~(?i)\.((js)|(css))$ {
			root /web/public;
 
			# 啓用 緩存 緩存時間爲 1小時
			expires 1h;
		}
	}
}
```

> root 配置的 目錄 是相對 nginx 根目錄  
> (ubuntu 下是 /usr/share/nginx)  
> 如果 root 的值 不是一個根目錄 需要使用 絕對路徑
> 

# gzip

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
 
events {
	worker_connections 768;
}
 
http {
	# 啓用 gzip
	gzip on;
	gzip_disable "msie6";
 
	# 驗證 瀏覽器 支持 才啓用 gzip
	gzip_vary on;
 
	# 小於 1k 的 不需要壓縮
	gzip_min_length 1k;
	
	# gzip 緩存 大小
	gzip_buffers 16 8k;
 
	# http 1.1 才 壓縮 1.1以下不支持 gzip
	gzip_http_version 1.1;
}
```

# 列出檔案目錄

處於 安全 考慮 nginx 默認不會 列出 檔案夾下的 檔案列表

如果 需要此功能 在 location 中配置 autoindex on;即可

# 反向代理
```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
 
events {
        worker_connections 768;
}
 
http {
        server {
                listen  80;
                server_name     doc.king011.com;
                location / {
                        proxy_pass http://127.0.0.1:9666;
                        proxy_set_header        Host    $host;
                        proxy_set_header        X-Real-IP       $remote_addr;
                        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
                }
        }
 
        server {
                listen  80;
                server_name     king011.com www.king011.com;
                location / {
                        proxy_pass http://127.0.0.1:9000;
                        proxy_set_header        Host    $host;
                        proxy_set_header        X-Real-IP       $remote_addr;
                        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
                }
        }

        server {
                listen  443 ssl http2;
                ssl_certificate /etc/letsencrypt/live/grpc.king011.com/fullchain.pem;
                ssl_certificate_key /etc/letsencrypt/live/grpc.king011.com/privkey.pem;
                ssl_dhparam /etc/nginx/ssl/dhparams.pem;

                server_name h2c.king011.com;
                location /{
                    grpc_pass grpc://localhost:10700;
                }
        }
        server {
                listen  443 ssl http2;
                ssl_certificate /etc/letsencrypt/live/grpc.king011.com/fullchain.pem;
                ssl_certificate_key /etc/letsencrypt/live/grpc.king011.com/privkey.pem;
                ssl_dhparam /etc/nginx/ssl/dhparams.pem;

                server_name h2.king011.com;
                location /{
                    grpc_pass grpcs://localhost:10700;
                }
        }
}
```

> 從 1.13.10 開始 nginx 支持了 grpc 和 http/https 類似 使用
> 
>  grpc_pass grpc/grpcs 進行 反向代理
>

## 準確匹配
```
server {
     listen       80;
     server_name  domain.com  www.domain.com;
     ...
}
```

## 使用通配符*
```
server {
     listen       80;
     server_name  *.domain.com;
     ...
}
```
```
server {
     listen       80;
     server_name  www.*;
     ...
}
```
## 正則匹配
```
server {
     listen       80;
     server_name  ~^(?.+)\.domain\.com$;
     ...
}
```

> nginx 從上到下一旦匹配到一個server_name 就不會進行其它server_name的匹配
> 

## 負載均衡

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
 
events {
	worker_connections 768;
}
 
http {
	# 配置一個 負載 服務器 集羣
	upstream myServers0 {
		server 127.0.0.1:8881;
		server 127.0.0.1:8882;
		server 127.0.0.1:8888;
	}
 
	# weight 指定 權值 默認 爲1
	upstream myServers1 {
		# 機會 1/6
		server 127.0.0.1:8881;
		# 機會 2/6
		server 127.0.0.1:8882 weight=2;
		# 機會 3/6
		server 127.0.0.1:8888 weight=3;
	}
 
	# least_conn 指定優先連接 最少 連接的 服務器
	upstream myServers2 {
		least_conn;
		server 127.0.0.1:8881;
		server 127.0.0.1:8882;
		server 127.0.0.1:8888;
	}
	# ip_hash 相同ip請求 會被路由到 相同 後端服務器
	upstream myServers3 {
		ip_hash;
		server 127.0.0.1:8881;
		server 127.0.0.1:8882;
		server 127.0.0.1:8888;
	}
 
	server{ 
		listen 80;
		server_name doc.king011.com;
		location / {
			proxy_pass         http://myServers0;
			proxy_set_header   Host             $host;
			proxy_set_header   X-Real-IP        $remote_addr;
			proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
		}
	}
}
```

# websocket
要支持 websocket 只需要 在 location 中 增加如下 設置即可
```
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection "upgrade";
proxy_http_version 1.1;
```

```
#info="Example"
user www-data;
worker_processes auto;
pid /run/nginx.pid;
 
events {
	worker_connections 768;
}
 
http {
	server {
		listen 443;
		server_name	ssh.king011.com;
		location / { 
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection "upgrade";
			proxy_http_version 1.1;
 
			proxy_set_header	Host		$host;
			proxy_set_header	X-Real-IP	$remote_addr;
			proxy_set_header	X-Forwarded-For	$proxy_add_x_forwarded_for;
			proxy_pass https://127.0.0.1:1443;
		}
	}
}
```

# https

要支持 https 需要 在配置 中 爲listen 傳入 ssl參數 並且 配置 ssl證書 檔案位置

```
server {
	...
	
	listen 443 ssl;
 
	# 憑證與金鑰的路徑
	ssl_certificate /etc/nginx/ssl/nginx.crt;
	ssl_certificate_key /etc/nginx/ssl/nginx.key;
 
	...
}
```

## http2

要啟動 http2 只需要在 listen 加上 http2 參數 即可

```
server {
	...
	
	listen 443 ssl http2;
 
	# 憑證與金鑰的路徑
	ssl_certificate /etc/nginx/ssl/nginx.crt;
	ssl_certificate_key /etc/nginx/ssl/nginx.key;
 
	...
}
```

## 自己簽發證書
使用 openssl 即可自行簽名 證書
```sh
sudo mkdir /etc/nginx/ssl
sudo openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt
```

* req 使用 X.509 Certificate Signing Request（CSR） Management 產生憑證
* -x509 建立自行簽署的憑證
* -nodes 不要使用密碼保護 因為這個憑證是 NGINX 伺服器要使用的 如果設定密碼的話 會讓伺服器每次在啟動時書需要輸入密碼
* -days 設定憑證的使用期限 單位天
* -newkey rsa:2048 產生新的 RSA 2048 位元的金鑰
* -keyout 設定金鑰儲存的位置
* -out 設定憑證儲存的位置

![](assets/1.png)

```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
 
events {
	worker_connections 768;
}
 
http {
 
        server {
                listen 443 ssl;
 
                # 憑證與金鑰的路徑
                ssl_certificate /etc/nginx/ssl/nginx.crt;
                ssl_certificate_key /etc/nginx/ssl/nginx.key;
 
                server_name     ssh.king011.com;
                location / {
                        proxy_pass http://127.0.0.1:1443;
                        proxy_set_header        Host    $host;
                        proxy_set_header        X-Real-IP       $remote_addr;
                        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
 
                        proxy_http_version 1.1;
                        proxy_set_header Upgrade $http_upgrade;
                        proxy_set_header Connection "upgrade";
                }
        }
 
}
```

# unix

在 linux 等系統下 nginx 支持使用 unix socket

```
server {
    listen 80 default;

    location /soc1/ {
        proxy_pass http://soc1/;
    }
    location /soc2/ {
        proxy_pass http://soc2/;
    }
}

upstream soc1 {
    server unix:/home/ubuntu/soc1;
}

upstream soc2 {
    server unix:/home/ubuntu/soc2;
}
```