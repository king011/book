# [Topics](https://www.rabbitmq.com/tutorials/tutorial-five-go.html)

![](assets/python-five.webp)

主題交換機 使用 routing key 作爲主題 可以接收到 所有 和指定 主題相關的 消息

主題是 使用  **.** 分隔的 多個單詞 routing key 總長度不能超過 255 個字節

消費者在關注主題時可以使用 通配符 設置關注的 主題

* **\*(星號)** 用來匹配一個單詞
* **\#(井號)** 用來匹配多個單詞(或零個)

比如 routing key 設計爲 **&lt;speed&gt;\.&lt;colour&gt;\.&lt;species&gt;**

1. \*\.orange\.\*  將關注所有 橙色的動物
2. \*\.\*\.rabbit 將關注所有兔子
3. lazy\.\# 將 關注所有 lazy 的東西

```
#info="producer"

package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `topic`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 交換機 只有在交換機不存在時 才會創建
	e = channel.ExchangeDeclare(
		ExchangeName, // 交換機名稱
		"topic",      //使用 直接 交換機
		true,         // 是否持久化
		false,        // 是否在沒人使用時 自動刪除
		false,        // 是否爲 internal 如果爲 true 則爲 internal 不會接受消息發佈
		false,        // no-wait
		nil,          // arguments
	)
	if e != nil {
		log.Println(e)
		return
	}

	if e != nil {
		log.Println(e)
		return
	}

	// 發佈消息 一個 orange 主題消息
	routingKey := "any.orange.any"
	e = channel.Publish(
		ExchangeName,
		routingKey, // 指定 routing key
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(routingKey), //消息內容
		},
	)
	if e == nil {
		fmt.Println(`send ->`, routingKey)
	} else {
		log.Println(e)
	}

	// 發佈消息 一個 rabbit 主題消息
	time.Sleep(time.Second)
	routingKey = "any.any.rabbit"
	e = channel.Publish(
		ExchangeName,
		routingKey, // 指定 routing key
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(routingKey), //消息內容
		},
	)
	if e == nil {
		fmt.Println(`send ->`, routingKey)
	} else {
		log.Println(e)
	}

	// 發佈消息 一些 lazy 主題消息
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		if i%3 == 0 {
			routingKey = "lazy"
		} else {
			routingKey = fmt.Sprintf("lazy.cb.cd")
		}
		str := strconv.Itoa(i)
		e = channel.Publish(
			ExchangeName,
			routingKey, // 指定 routing key
			false,
			false,
			amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         []byte(str), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, str)
		} else {
			log.Println(e)
		}
	}
}
```

```
#info="consumer"

package main

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// ExchangeName 定義 交換機名稱
	ExchangeName = `topic`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	go consumer(c, "one", "*.orange.*")
	consumer(c, "two", "*.*.rabbit", "lazy.#")
}
func consumer(c *amqp.Connection, tag string, routingKeys ...string) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 交換機 只有在交換機不存在時 才會創建
	e = channel.ExchangeDeclare(
		ExchangeName, // 交換機名稱
		"topic",      //使用 主題 交換機
		true,         // 是否持久化
		false,        // 是否在沒人使用時 自動刪除
		false,        // 是否爲 internal 如果爲 true 則爲 internal 不會接受消息發佈
		false,        // no-wait
		nil,          // arguments
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		``,    // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		true,  // 是否持久化
		true,  // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 綁定 隊列到交換機
	for _, routingKey := range routingKeys {
		e = channel.QueueBind(
			q.Name,       // 隊列名
			routingKey,   // 路由鍵
			ExchangeName, // 交換機
			false,        // no wait
			nil,          // args
		)
		if e != nil {
			log.Println(e)
			return
		}
	}
	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		true, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	for msg := range ch {
		fmt.Println(tag, `recv <-`, msg.RoutingKey, string(msg.Body))
	}
}
```