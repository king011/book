# net/websocket

```bash
go get -u -v golang.org/x/net/websocket
```

golang.org/x/net/websocket 實現了一個 WebSocket 的客戶端 服務器

# Example

```
#info="server"
package main
 
import (
	"fmt"
	"golang.org/x/net/websocket"
	"log"
	"net/http"
)
 
func echoHandler(ws *websocket.Conn) {
	var e error
	var msg string
	for {
		//接收消息
		if e = websocket.Message.Receive(ws, &msg); e != nil {
			log.Println(e)
			break
		}
		fmt.Println("Receive:", msg)
 
		//發送消息
		msg = "s-" + msg
		if e = websocket.Message.Send(ws, msg); e != nil {
			log.Println(e)
			break
		}
	}
	//關閉 連接
	ws.Close()
}
 
func main() {
	addr := ":8080"
	http.Handle("/echo", websocket.Handler(echoHandler))
	fmt.Println("work at ", addr)
	e := http.ListenAndServe(addr, nil)
	if e != nil {
		log.Fatalln(e)
	}
}
```

```
#info="client"
package main
 
import (
	"fmt"
	"golang.org/x/net/websocket"
	"log"
)
 
func main() {
	//連接 websocket
	url := "ws://127.0.0.1:8080/echo"  //websocket 連接地址
	origin := "http://127.0.0.1:8080/" //此參數 沒有實質作用 只是作爲 RemoteAddr
	ws, e := websocket.Dial(url, "", origin)
	if e != nil {
		log.Fatal(e)
	}
	defer ws.Close()
 
	msg := "i'm a client"
	e = websocket.Message.Send(ws, msg)
	if e != nil {
		log.Fatalln(e)
	}
 
	//接收消息
	e = websocket.Message.Receive(ws, &msg)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(msg)
}
```