# catchError

catchError 可以捕獲 Observable 產生的異常 catchError 並且返回一個 Observable 一個操作繼續

catchError 第二個參數 是當前 Observable 可以直接返回 以重試 原Observable

```
import { of, empty } from 'rxjs'
import { map, catchError } from 'rxjs/operators'

of('a', 'b', 'c', 2, 'd').pipe(
    map(v => (v as string).toUpperCase()),
    catchError((e, caught) => {
        console.log("error :", e)
        return empty()
    }),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```

# retry

retry 可以在 出現錯誤時 進行重試操作 不帶參數的 retry 會無線重試下去 也可以傳入 最多重試的次數

```
import { of } from 'rxjs'
import { map, retry } from 'rxjs/operators'

of('a', 'b', 'c', 2, 'd').pipe(
    map(v => (v as string).toUpperCase()),
    retry(1),
).subscribe({
    next(v) {
        console.log(v)
    },
    error(e) {
        console.log(e)
    },
})
```

```
import { of } from 'rxjs'
import { map, retry, catchError } from 'rxjs/operators'

of('a', 'b', 'c', 2, 'd').pipe(
    map(v => (v as string).toUpperCase()),
    catchError((e) => new Promise((resolve, reject) => {
        console.log(e)
        reject(e)
    })),
    retry(1),
).subscribe({
    next(v) {
        console.log(v)
    },
    error(e) {
    },
})
```

# retryWhen
retryWhen 返回 一個 Observable 直到此 Observable next 才執行 重試

```
import { of } from 'rxjs'
import { map, retryWhen, delay } from 'rxjs/operators'

of('a', 'b', 'c', 2, 'd').pipe(
    map(v => (v as string).toUpperCase()),
    retryWhen(errors => errors.pipe(delay(1000))),
).subscribe({
    next(v) {
        console.log(v)
    },
    error(e) {
        console.log("error :", e)
    },
})
```

```
import { of, Observable, Subscriber } from 'rxjs'
import { map, retryWhen, delay } from 'rxjs/operators'

of('a', 'b', 'c', 2, 'd').pipe(
    map(v => (v as string).toUpperCase()),
    retryWhen((errors) => {
        return new Observable((subscriber: Subscriber<any>) => {
            let i = 0
            errors.subscribe({
                next(v) {
                    i++
                    if (i <= 2) {
                        console.log("retry", i)
                        subscriber.next(v)
                    } else {
                        // 已經重新 2次 拋出異常
                        subscriber.error(v)
                    }
                },
                error(e) {
                    subscriber.error(e)
                },
                complete() {
                    subscriber.complete()
                },
            })
        }).pipe(delay(1000))
    }),
).subscribe({
    next(v) {
        console.log(v)
    },
    error(e) {
        console.log("error :", e)
    },
})
```

# repeat

有時需要在沒有出錯時 執行重複 訂閱 可以使用 repeat 同樣如果沒有傳入參數 將一直 repeat

```
import { of } from 'rxjs'
import { delay, repeat } from 'rxjs/operators'
of(1, 2, 3).pipe(
    delay(1000),
    repeat(),
).subscribe({
    next(v) {
        console.log(v)
    },
})
```


