# 佈局

android 提供了多種 佈局 組件

* 線性佈局 LinearLayout
* 表格佈局 TableLayout
* 幀佈局 FrameLayout
* 相對佈局 RelativeLayout
* 絕對佈局 AbsoluteLayout
* 網格佈局 GridLayout

> AbsoluteLayout 在 android 2.0 就被標記爲 過期 不應該繼續使用
> 

# 線性佈局 LinearLayout

[android.widget.LinearLayout](https://developer.android.com/reference/android/widget/LinearLayout) 將組件 按照 垂直/水平 方式 佈局 

![](assets/LinearLayout.png)
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <!--垂直佈局-->
    <LinearLayout
        android:id="@+id/vertical"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center"
        android:background="@color/colorAccent"
        >

        <ImageView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:srcCompat="@android:drawable/btn_star_big_on" />

        <!--水平佈局-->
        <LinearLayout
            android:id="@+id/horizontal"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            android:gravity="center"
            >
            <ImageView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                app:srcCompat="@android:drawable/btn_star_big_on" />
            <ImageView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                app:srcCompat="@android:drawable/btn_star_big_on" />
            <ImageView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                app:srcCompat="@android:drawable/btn_star_big_on" />
        </LinearLayout>

        <ImageView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:srcCompat="@android:drawable/btn_star_big_on" />
    </LinearLayout>

</android.support.constraint.ConstraintLayout>
```

**android:orientation** 指定 horizontal(水平 默認值) vertical(垂直) 佈局

**android:gravity** 指定 組件對齊方式 可以使用 | 組合 屬性 (right|bottom 右下角)

| 屬性 | 值 | 含義 |
| -------- | -------- | -------- |
| bottom     | 50     | 將對象 推到容器 底部 而不改變其大小     |
| top     | 30     | 將對象 推到容器 頂部 而不改變其大小     |
| left     | 3     | 將對象 推到容器 左側 而不改變其大小     |
| right     | 5     | 將對象 推到容器 右側 而不改變其大小     |
| center_horizontal     | 1     | 將對象 推到容器 水平中心 而不改變其大小     |
| center_vertical     | 10     | 將對象 推到容器 垂直中心 而不改變其大小     |
| center     | 11     | 將對象 推到容器 中心 而不改變其大小     |
| start     | 800003     | 將對象 推到容器 開頭 而不改變其大小     |
| end     | 800005     | 將對象 推到容器 結尾 而不改變其大小     |
| fill_horizontal     | 7     | 如果需要水平填充容器 使其完全填充容器     |
| fill_vertical     | 70     | 如果需要垂直填充容器 使其完全填充容器     |
| fill     | 77     | 如果需要填充容器 使其完全填充容器     |
| clip_horizontal     | 8     | 如果需要水平剪裁組件     |
| clip_vertical     | 80     | 如果需要垂直剪裁組件     |

**android:layout\_width android:layout\_height** 用來 指定組件的 基本 寬高 其爲 ViewGroup.LayoutParams 支持的 所有屬性

| 屬性 | 值 | 含義 |
| -------- | -------- | -------- |
| fill_parent     | ffffffff     | 和match_parent完全相同 從 API 8 開始 使用 match_parent 替代     |
| match_parent     | ffffffff     | 和父元素 寬/高 相同     |
| wrap_content     | fffffffe     | 寬/高 剛好包裹住其 內容     |

**android:id** 指定id後 R.java 會自動派生一個對應屬性 在java代碼中 通過 findViewById 來獲取組件實例

> 如果 不需要 findViewById 可以忽略
> 

**android:background** 指定 組件背景 可以是 顏色/圖片

# 表格佈局 TableLayout

[android.widget.TableLayout](https://developer.android.com/reference/android/widget/TableLayout) 以表格佈局

* 每個 TableRow 佔用 一行 
* TableRow 的每個子元素 自動創建一列

![](assets/TableLayout.png)
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:id="@+id/vertical"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:gravity="center"
        >
        <TableLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            >
            <TableRow
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                >
                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="用戶名："
                    />
                <EditText
                    android:id="@+id/editText"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:ems="10"
                    android:inputType="textPersonName"
                    android:text=""
                    />
            </TableRow>
            <TableRow
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                >
                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="密碼："
                    />
                <EditText
                    android:id="@+id/editText3"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:ems="10"
                    android:inputType="textPassword"
                    />
            </TableRow>
            <TableRow
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                >
                <LinearLayout
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:gravity="center"
                    android:layout_column="1"
                    >
                    <Button
                        android:id="@+id/login"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:text="登入"
                        />
                    <Button
                        android:id="@+id/join"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:text="註冊"
                        />
                </LinearLayout>
            </TableRow>
        </TableLayout>
    </LinearLayout>

</android.support.constraint.ConstraintLayout>
```

| TableLayout 屬性 | 含義 |
| -------- | -------- |
| android:collapseColumns     | 設置要被 隱藏 的列 序號從0開始 多個序列以 , 分隔開     |
| android:shrinkColumns     | 設置允許被 收縮 的列 序號從0開始 多個序列以 , 分隔開     |
| android:stretchColumns     | 設置允許被 拉伸 的列 序號從0開始 多個序列以 , 分隔開     |

| 列元素屬性 | 含義 |
| -------- | -------- |
| android:layout_column     | 手動指定 列 序列     |
| android:layout_span     | 手動指定 元素 橫跨 多少個列     |

# 幀佈局 FrameLayout

[android.widget.FrameLayout](https://developer.android.com/reference/android/widget/FrameLayout) 幀佈局 中的元素 後添加的會覆蓋在 先添加的元素之上

![](assets/FrameLayout.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <FrameLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/colorAccent"
        android:foreground="@drawable/tux"
        android:foregroundGravity="right|bottom"
        >

        <TextView
            android:text="藍色背景 最下層"
            android:layout_width="360dp"
            android:layout_height="200dp"
            android:background="#FF0000FF"
            android:layout_gravity="center"/>
        <TextView
            android:text="天藍背景 中間層"
            android:layout_width="300dp"
            android:layout_height="150dp"
            android:background="#FF0077FF"
            android:layout_gravity="center"/>
        <TextView
            android:text="水藍背景 最上層"
            android:layout_width="240dp"
            android:layout_height="100dp"
            android:background="#FF00B4FF"
            android:layout_gravity="center"/>
    </FrameLayout>
</android.support.constraint.ConstraintLayout>
```

| 屬性 | 含義 |
| -------- | -------- |
| android:foreground     | 指定一個前景 圖像 會在所有元素之上 通常是一個 商標/水印     |
| android:foregroundGravity     | 指定 foreground 的位置     |

# 相對佈局 RelativeLayout

[android.widget.RelativeLayout](https://developer.android.com/reference/android/widget/RelativeLayout) 中的元素可以 相對另外一個元素的位置 進行 佈局

![](assets/RelativeLayout.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


  <RelativeLayout
      android:layout_width="match_parent"
      android:layout_height="match_parent"
      >

      <!--中間的 組件 作爲參考系-->
      <ImageView
          android:id="@+id/middle"
          android:layout_width="wrap_content"
          android:layout_height="wrap_content"
          app:srcCompat="@android:drawable/btn_star_big_on"
          android:layout_centerInParent="true"/>

      <!--上方組件-->
      <TextView
          android:layout_width="wrap_content"
          android:layout_height="wrap_content"
          android:text="上方組件"
          android:layout_above="@+id/middle"
          android:layout_centerHorizontal="true"/>

      <!--下方組件-->
      <TextView
          android:layout_width="wrap_content"
          android:layout_height="wrap_content"
          android:text="下方組件"
          android:layout_below="@+id/middle"
          android:layout_centerHorizontal="true"/>

      <!--左側組件-->
      <TextView
          android:layout_width="wrap_content"
          android:layout_height="wrap_content"
          android:text="左側組件"
          android:layout_toLeftOf="@+id/middle"
          android:layout_centerVertical="true"/>
      <!--右側組件-->
      <TextView
          android:layout_width="wrap_content"
          android:layout_height="wrap_content"
          android:text="右側組件"
          android:layout_toRightOf="@+id/middle"
          android:layout_centerVertical="true"/>

  </RelativeLayout>
</android.support.constraint.ConstraintLayout>
```



| RelativeLayout屬性 | 含義 |
| -------- | -------- |
| android:gravity     | 指定佈局管理器中 各子組件對齊方式     |
| android:ignoreGravity     | 指定 不受 gravity 影響的 組件    |

子組件 需要 指定 [RelativeLayout.LayoutParams](https://developer.android.com/reference/android/widget/RelativeLayout.LayoutParams)  來確定如何 相對佈局

| 屬性 | 含義 |
| -------- | -------- |
| android:layout_above     | 值爲其它組件id 指定本組件位於哪個組件 上方     |
| android:layout_below     | 值爲其它組件id 指定本組件位於哪個組件 下方     |
| android:layout_toLeftOf     | 值爲其它組件id 指定本組件位於哪個組件 左側     |
| android:layout_toRightOf     | 值爲其它組件id 指定本組件位於哪個組件 左右側     |
| android:layout_alignTop     | 值爲其它組件id 指定本組件 與哪個組件 上邊界對齊     |
| android:layout_alignBottom     | 值爲其它組件id 指定本組件 與哪個組件 下邊界對齊     |
| android:layout_alignLeft     | 值爲其它組件id 指定本組件 與哪個組件 左邊界對齊     |
| android:layout_alignRight     | 值爲其它組件id 指定本組件 與哪個組件 右邊界對齊     |
| android:layout_centerInParent     | 值爲boolean 指定本組件是否位於 佈局管理器 中心     |
| android:layout_centerHorizontal     | 值爲boolean 指定本組件是否位於 佈局管理器 水平中心     |
| android:layout_centerVertical     | 值爲boolean 指定本組件是否位於 佈局管理器 垂直中心     |
| android:layout_alignParentTop     | 值爲boolean 指定本組件是否位於 佈局管理器 頂部     |
| android:layout_alignParentBottom     | 值爲boolean 指定本組件是否位於 佈局管理器 底部     |
| android:layout_alignParentLeft     | 值爲boolean 指定本組件是否位於 佈局管理器 左邊對齊   |
| android:layout_alignParentRight     | 值爲boolean 指定本組件是否位於 佈局管理器 右邊對齊     |

# 網格佈局 GridLayout
[android.widget.GridLayout](https://developer.android.com/reference/android/widget/GridLayout) 是 android 4.0 API 21 開始提供的佈局 和 TableLayout 一樣 以表格佈局 

不過 GridLayout 需要在創建時就指定 表格的 行 列  子元素 依次 填充到表格中

![](assets/GridLayout.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:id="@+id/vertical"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center"
        >
        <GridLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            android:columnCount="4"
            android:rowCount="6">

            <TextView
                android:layout_columnSpan="4"
                android:layout_gravity="fill"
                android:layout_marginLeft="5dp"
                android:layout_marginRight="5dp"
                android:background="#FFCCCC"
                android:text="0"
                android:textSize="50sp" />

            <Button
                android:layout_columnSpan="2"
                android:layout_gravity="fill"
                android:text="回退" />

            <Button
                android:layout_columnSpan="2"
                android:layout_gravity="fill"
                android:text="清空" />

            <Button android:text="+" />

            <Button android:text="1" />

            <Button android:text="2" />

            <Button android:text="3" />

            <Button android:text="-" />

            <Button android:text="4" />

            <Button android:text="5" />

            <Button android:text="6" />

            <Button android:text="*" />

            <Button android:text="7" />

            <Button android:text="8" />

            <Button android:text="9" />

            <Button android:text="/" />

            <Button android:text="." />

            <Button android:text="0" />

            <Button android:text="=" />

        </GridLayout>
    </LinearLayout>

</android.support.constraint.ConstraintLayout>
```

