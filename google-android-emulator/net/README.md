# [設置網路模擬器](https://developer.android.com/studio/run/emulator-networking?hl=zh-cn)

```
# 將本機 127.0.0.1:5000 映射到模擬器 10.0.2.15:6000
redir add tcp:5000:6000

# 打印當前設置的映射
redir list

# 刪除映射
redir del tcp:5000:6000
```