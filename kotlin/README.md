# kotlin

kotlin 是java虛擬機上執行的靜態型別程式語言 (	Apache 2)

* 官網 [https://kotlinlang.org/](https://kotlinlang.org/)
* 中文社區 [https://www.kotlincn.net/](https://www.kotlincn.net/)


```
fun main() {
    println("Hello, World!")
}
```