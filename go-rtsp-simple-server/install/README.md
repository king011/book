# 安裝

直接[下載](https://github.com/aler9/rtsp-simple-server/releases)二進制檔案執行即可

```
./rtsp-simple-server
```

你可以使用 ffmpeg 來推送一個流到服務器

```
ffmpeg -re -stream_loop -1 -i file.ts -c copy -f rtsp rtsp://localhost:8554/mystream
```

你可以使用 ffplay 來測試播放流

```
ffplay rtsp://localhost:8554/mystream
```

你同樣也可以使用 ffmpeg 來將流保存下來

```
ffmpeg -i rtsp://localhost:8554/mystream -c copy output.mp4
```