# [項目結構](https://docs.flame-engine.org/1.16.0/flame/structure.html)

Flame 提供了推薦的項目檔案夾結構

* **assets/audio** 下存放音頻
* **assets/images** 下存放圖像
* **assets/tiles** 下存放 tiles 地圖

如果使用下述示例代碼:

```
void main() {
  FlameAudio.play('explosion.mp3');

  Flame.images.load('player.png');
  Flame.images.load('enemy.png');
  
  TiledComponent.load('level.tmx', tileSize);
}
```

flame 期望資產按照如下結構存儲：
```
.
└── assets
    ├── audio
    │   └── explosion.mp3
    ├── images
    │   ├── enemy.png
    │   └── player.png
    └── tiles
        └── level.tmx
```

> 你也可以選擇將音頻分成兩個資料夾，一個用於音樂(music)，一個用於音效(sfx)

不要忘記將資產添加到 **pubspec.yaml** 設定檔案：

```
flutter:
  assets:
    - assets/audio/explosion.mp3
    - assets/images/player.png
    - assets/images/enemy.png
    - assets/tiles/level.tmx
```

如果想改變此結構，可以使用 prefix 參數創建 AssetsCache Images AudioCache 實例，而不是使用 Flame 提供的全局實例。

此外 AssetsCache 和 Images 可以接收自定 AssetBundle。這可用於使用 Flame 在 rootBundle 之外的不同位置尋找資產，例如檔案系統