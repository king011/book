# [sqlite](https://deno.land/x/sqlite)

sqlite 是使用 WebAssembly 提供的 SQLite3 庫

因爲 SQLite3 本身沒有提供異步 api 所以 這個 WebAssembly 庫也沒提供異步 api，可行的解決方案是將所有數據庫操作放到 web worker 中去執行

```
import { DB } from "https://deno.land/x/sqlite/mod.ts";

// Open a database
const db = new DB("test.db");
db.execute(`
  CREATE TABLE IF NOT EXISTS people (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT
  )
`);

// Run a simple query
for (const name of ["Peter Parker", "Clark Kent", "Bruce Wayne"]) {
  db.query("INSERT INTO people (name) VALUES (?)", [name]);
}
db.execute()
// Print out data in table
for (const [name] of db.query("SELECT name FROM people")) {
  console.log(name);
}

// Close connection
db.close();
```

# execute

```
interface DB {
	execute(sql: string):void
}
```

execute 用來執行多個以 **;** 分隔的 sql 語句，它不支持參數綁定並且忽略所有的返回結果，通常用來執行初始化的 raw sql 語句(例如初始化表結構)

# query

```
interface DB {
	query(sql: string, params?: QueryParameterSet | undefined): Row[]
}
```

query 用來執行帶有返回結果的 sql 語句，並且它支持兩個方式來綁定參數


使用數組綁定參數
```
const rows = db.query<[string, number]>("SELECT name, age FROM people WHERE city = ?", [city]);
// rows = [["Peter Parker", 21], ...]
```

使用 Object 綁定參數
```
 const rows = db.query<[string, number]>(
   "SELECT name, age FROM people WHERE city = :city",
    { city },
  );
 // rows = [["Peter Parker", 21], ...]
```

# prepareQuery

```
interface DB {
	prepareQuery<Row, RowObject, QueryParameterSet>(sql: string): PreparedQuery<Row, RowObject, QueryParameterSet>
}
```

prepareQuery 可以準備一條被多次執行 sql 語句，它和 query 使用方式類似，但可以多次被重複使用它比多次執行 query 更高效

```
const query = db.prepareQuery(`select id,name from people where id!=?`);
try {
  // query.execute()
  // 查詢第一條匹配的數據
  const row = query.first([1]);
  console.log(row);
  // 查詢所有數據
  const rows = query.all([1]);
  console.log(rows);
} finally {
  // 不再使用釋放資源
  query.finalize();
}
```

# transaction
transaction 將一個函數在事務中執行，如果遇到了任何的異常則會自動回歸事務否則在函數結尾時提交事務

```
interface DB {
	transaction<V>(closure: () => V): V
}
```
