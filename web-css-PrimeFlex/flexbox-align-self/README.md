# [align-self](https://www.primefaces.org/primeflex/alignself)

align-self 類似 align-items 但它被設置到子項上，爲此子項指定單獨的對齊方式

| Class | Properties |
| -------- | -------- |
| align-self-auto     | align-self: auto;     |
| align-self-stretch     | align-self: stretch;     |
| align-self-start     | align-self: flex-start;     |
| align-self-end     | align-self: flex-end;     |
| align-self-center     | align-self: center;     |
| align-self-baseline     | align-self: baseline;     |

## align-self-auto

align-self-auto 將從 flex 容器獲取對齊的方式，這也是子項的默認值

![](assets/auto.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container blue-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="align-self-auto flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-self-stretch

align-self-stretch 項目被拉伸以適合容器

![](assets/stretch.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container indigo-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="align-self-stretch flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-self-start

align-self-start 項目位於容器開頭

![](assets/start.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container yellow-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="align-self-start flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-self-end

align-self-end 項目位於容器結尾

![](assets/end.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container purple-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="align-self-end flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```


## align-self-center

align-self-end 項目位於容器中心

![](assets/center.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container green-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="align-self-center flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-self-baseline

align-self-baseline 項目位於容器基線

![](assets/baseline.png)

```
<div class="card">
    <div class="flex align-items-stretch flex-wrap card-container orange-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="align-self-baseline flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```
# 響應式佈局
和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同