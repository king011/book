# [安裝](https://tesseract-ocr.github.io/tessdoc/Installation.html)

有兩個部分需要安裝，引擎本身和語言的訓練資料。

Tesseract 可直接從許多 Linux 發行版中取得。該軟體包通常稱為“tesseract”或“tesseract-ocr” - 搜尋您的發行版的儲存庫以找到它。

Linux 發行版還可以直接提供適用於 130 多種語言和超過 35 種腳本的軟體包。語言訓練資料包稱為“tesseract-ocr-langcode”和“tesseract-ocr-script-scriptcode”，其中langcode是三字母語言代碼，scriptcode是四字母腳本代碼

範例：tesseract-ocr-eng（英文）、tesseract-ocr-ara（阿拉伯語）、tesseract-ocr-chi-sim（簡體中文）、tesseract-ocr-script-latn（拉丁文腳本）、tesseract-ocr- script- deva（梵文腳本）等

** 僅供專家使用。 **

如果您正在嘗試 OCR 引擎模式，則需要手動安裝超出 Linux 發行版中可用的語言訓練資料。

各種類型的訓練資料可以在 GitHub 上找到。解壓縮 .traineddata 檔案並將其複製到“tessdata”目錄中。確切的目錄取決於訓練資料的類型和您的 Linux 發行版。可能是 /usr/share/tesseract-ocr/tessdata 或 /usr/share/tessdata 或 /usr/share/tesseract-ocr/4.00/tessdata。

過時的 Tesseract 版本 =< 3.02 的訓練資料駐留在另一個位置。

## Ubuntu

安裝程式和庫:
```
sudo apt install -y \
	tesseract-ocr \
	libtesseract-dev
```

安裝中文訓練數據:
```
sudo apt install -y tesseract-ocr-chi*
```

如果 apt 無法找到軟體包，請嘗試將 Universe 條目新增至sources.list 檔案中，如下所示

sudo vi /etc/apt/sources.list

```
deb http://archive.ubuntu.com/ubuntu bionic main
```

如果您使用的是不同版本的 ubuntu，請將 bionic 替換為對應的版本名稱

## MSYS2

安裝 tesseract 引擎:

```
pacman -S mingw-w64-{i686,x86_64}-tesseract-ocr
```

安裝數據檔案:
```
pacman -S mingw-w64-{i686,x86_64}-tesseract-data-eng
```

在上述命令中，「eng」可以替換為支援語言的 ISO 639 3 字母語言代碼。若要取得可用語言包的列表，請使用：

```
pacman -Ss tesseract-data
```

# 運行

Tesseract 是一個命令列程序，因此首先打開終端機或命令提示字元。該指令的使用方式如下：

```
tesseract imagename outputbase [-l lang] [-psm pagesegmode] [configfile...]
```

因此，對名為「myscan.png」的影像進行 OCR 並將結果儲存到「out.txt」的基本用法是：

```
tesseract myscan.png out
```

或對德語做同樣的事情：

```
tesseract myscan.png out -l deu
```

它甚至可以一次與多種語言的訓練資料一起使用，例如。英語和德語：

```
tesseract myscan.png out -l eng+deu
```

Tesseract 還包括 hOCR 模式，它會產生一個特殊的 HTML 文件，其中包含每個單字的座標。這可用於使用 Hocr2PDF 等工具建立可搜尋的 pdf。要使用它，請使用“hocr”配置選項，如下所示：

```
tesseract myscan.png out hocr
```

您也可以直接從 tesseract （版本 >=3.03）建立可搜尋的 pdf：

```
tesseract myscan.png out pdf
```

有關各種選項的更多信息，請參閱 [Tesseract 線上說明頁](https://github.com/tesseract-ocr/tesseract/blob/main/doc/tesseract.1.asc)。

# 其他語言

Tesseract 已支持[多種語言](https://github.com/tesseract-ocr/tesseract/blob/main/doc/tesseract.1.asc#languages)，請在 [Tessdata 儲存庫](https://github.com/tesseract-ocr/tessdata)中檢查需要的語言

它還可以經過訓練以支援其他語言和文字；有關更多詳細信息，請參閱 [TrainingTesseract](https://tesseract-ocr.github.io/tessdoc/TrainingTesseract)

