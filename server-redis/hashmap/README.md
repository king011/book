# 哈希表

redis 支持 存儲 Hash Map

每個 hash 可以存儲 pow(2,32-1) 個鍵值對(4294967295,超過40億)

| 命令 | 含義 |
| -------- | -------- |
| HDEL key field1 [field2]      | 刪除 字段     |
| HEXISTS key field      | 返回 是否存在 指定字段     |
| HGET key field      | 獲取字段值     |
| HGETALL key      | 返回所有字段和值     |
| HINCRBY key field increment      | 將 field 的值(必須可轉爲整型) + increment    |
| HINCRBYFLOAT key field increment      | 將 field 的值(必須可轉爲數字) + increment     |
| HKEYS key      | 獲取 所有字段     |
| HLEN key      | 獲取字段數量     |
| HMGET key field1 [field2]      | 獲取多個 field 值      |
| HMSET key field1 value1 [field2 value2 ]      | 設置多個 field 值     |
| HSET key field value      | 設置 field 值     |
| HSETNX key field value      | 只有 filed 不存在時 設置 filed     |
| HVALS key      | 獲取所有 filed 的值     |
| HSCAN key cursor [MATCH pattern] [COUNT count]      | 迭代 hash 的 filed 值     |

