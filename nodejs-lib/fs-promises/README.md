# [fs/promises](https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_promises_api)

promises 提供了 promise 風格的異步檔案操作，promises 使用底層的 node.js 線程池在事件循環線程之外執行檔案操作。這些操作不是同步或線程安全的。對同一檔案執行多個併發修改時需要小心，否則可能會發生數據損壞

promises 從 v10.1.0 開始在 fs 模塊中被提供，從 v14.0.0 被移動到 fs/promises 模塊

```
import { promises } from "fs";
import fsPromises from "fs/promises";

console.log(promises == fsPromises)
```

# access
access 用於檢查用戶對檔案或檔案夾是否有訪問權限，如果檢查失敗將 reject(Error)

mode 是可選的要檢查內容默認爲 fs.constants.F_OK， fs.constants.XXX 定義了可選取值常量，

```
function access(path: string | Buffer | UR, mode?: number): Promise<void>;
```

```
import { access } from "fs/promises";
import { constants } from 'fs';

access('/etc/passwd',
    constants.R_OK | constants.W_OK,
).then(() => {
    console.log('can access')
}).catch((e) => {
    console.error(e)
})
```

# appendFile

appendFile 用於將數據添加到檔案，如果檔案不存在則創建檔案

```
function appendFile(path: PathLike | FileHandle, data: string | Uint8Array, options?: (ObjectEncodingOptions & FlagAndOpenMode) | BufferEncoding | null): Promise<void>;
```

```
import { appendFile } from "fs/promises";

(async function () {
    try {
        await appendFile('a.txt', '123')
        await appendFile('a.txt', '456')
    } catch (e) {
        console.log(e)
    }
})()
```
# chmod

chmod 用於修改檔案的權限

```
function chmod(path: PathLike, mode: Mode): Promise<void>;
```

# chown

chown 修改檔案的所有者

```
function chown(path: PathLike, uid: number, gid: number): Promise<void>;
```

# copyFile

copyFile 複製檔案

```
function copyFile(src: PathLike, dest: PathLike, mode?: number): Promise<void>;
```

# lchmod
更改符號鏈接權限

此方法只在 macOS 上有效

```
function lchmod(path: PathLike, mode: Mode): Promise<void>;
```

# lchown

更改符號鏈接所有者

```
function lchown(path: PathLike, uid: number, gid: number): Promise<void>;
```

# lutimes

類似 utimes 但如果被修改的符號連接則不會取消引用連接，而是修改了符號連接本身的時間戳

```
function lutimes(path: PathLike, atime: string | number | Date, mtime: string | number | Date): Promise<void>;
```

# link

創建現有路徑的新路徑鏈接

```
function link(existingPath: PathLike, newPath: PathLike): Promise<void>;
```

# lstat

# mkdir

# mkdtemp

# open

打開檔案

```
function open(path: PathLike, flags: string | number = 'r', mode = 0o666): Promise<FileHandle>;
```

# opendir

打開檔案夾

```
function opendir(path: PathLike, options?: OpenDirOptions): Promise<Dir>;
```

```
import { opendir } from "fs/promises";
import { Input } from "./env";
(async function () {
    try {
        const dirents = await opendir(Input, { encoding: 'utf8' })
        for await (const dirent of dirents) {
            console.log(dirent.name)
        }
    } catch (e) {
        console.log(e)
    }
})()
```
# readdir
讀取檔案夾內容

```
function readdir(path: PathLike, options?: ObjectEncodingOptions): Promise<string[]>;
function readdir(path: PathLike, options: ObjectEncodingOptions & {
		withFileTypes: true;
}): Promise<Dirent[]>;
```

```
import { readdir } from "fs/promises";
import { Input } from "./env";
(async function () {
    try {
        const files = await readdir(Input, { encoding: 'utf8', withFileTypes: true })
        for (const file of files) {
            console.log(file)
        }
    } catch (e) {
        console.log(e)
    }
})()
```
# readFile

# readlink

# realpath

# rename
修改檔案名稱

```
function rename(oldPath: PathLike, newPath: PathLike): Promise<void>;
```

# rmdir

# rm

刪除檔案和目錄

```
function rm(path: PathLike, options?: RmOptions): Promise<void>;

export interface RmOptions {
		/**
		 * 如果爲 true 則忽略 path 不存在的情況
		 * @default false
		 */
		force?: boolean | undefined;
		/**
		 * 在 recursive模式下，如果遇到`EBUSY`, `EMFILE`, `ENFILE`, `ENOTEMPTY`, 或者
		 * `EPERM` 錯誤，nodejs會重試操作，此參數設置重試次數
		 * @default 0
		 */
		maxRetries?: number | undefined;
		/**
		 * 如果爲 true 則遞歸刪除目錄，在遞歸模式下失敗操作會進行重試
		 */
		recursive?: boolean | undefined;
		/**
		 * 重試之間等待的毫秒數
		 * 如果 recursive 不爲 true 則忽略此參數
		 * @default 100
		 */
		retryDelay?: number | undefined;
}
```

# stat

返回路徑的 Stats，bigint 指示 Stats 中的值是否應該是 bigint

```
function stat(
			path: PathLike,
			opts?: StatOptions & {
					bigint?: false | undefined;
			}
	): Promise<Stats>;
```

# symlink

# truncate

將檔案截斷或擴展到長的 len 指定的字節，檔案不存在將 reject

```
function truncate(path: PathLike, len = 0): Promise<void>;
```

# unlink

# utimes

更改檔案系統的時間戳，時間戳可以是 unix:number|string 或 Date

```
function utimes(path: PathLike, atime: string | number | Date, mtime: string | number | Date): Promise<void>;
```
# watch

# writeFile