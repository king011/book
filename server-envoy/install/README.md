# docker 
envoy 官網提供了 [docker](https://hub.docker.com/r/envoyproxy/envoy) 鏡像

```
#info="docker-compose.yml"
version: '1'
services:
  envoy:
    image: envoyproxy/envoy:v1.22.5
    restart: always
    ports:
      - "9000:10000"
    volumes:
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
      - ./conf/envoy.yaml:/etc/envoy/envoy.yaml
```