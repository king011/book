# client
上文爲 wireguard 僞造了 fake tcp 服務端，下面啓用一個客戶端來連接 fake

```
services:
  wireguard:
    ports:
      - 51820:51820/udp
    image: king011/udp2raw:20230206.0
    cap_add:
      - NET_ADMIN
    restart: unless-stopped
    command: [
      "udp2raw_amd64", "-c",
      "-l", "0.0.0.0:51820",
      "-r", "12.12.12.12:51820",
      "-k", "passwd",
      "--raw-mode", "faketcp",
      "--cipher-mode", "xor",
      -a
    ]
```

> 請將 12.12.12.12:51820 改爲實際的 fake 服務器端口和ip，注意 udp2raw_amd64 不支持域名需要是 ip