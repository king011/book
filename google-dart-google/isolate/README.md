# isolate

[isolate](https://pub.dev/packages/isolate) 庫 對 dark:isolate 提供了包裝 提供了更加 方便 的功能

```
import 'package:isolate/isolate.dart';

void main() async {
  /// 創建 2 個 dark:isolate 池執行代碼
  final pool = await LoadBalancer.create(2, spawn);

  for (var i = 0; i < 3; i++) {
    /// 調用 池中的 isolate 執行代碼
    pool.run(work, i).then((v) {
      print(v);
    }, onError: (e) {// run 可在正確捕獲到 異常
      print("$e");
    });
  }
}

work(int i) async {
  if (i == 1) {
    throw "123";
  }
  await Future.delayed(Duration(seconds: 1));

  return i * 10;
}

Future<IsolateRunner> spawn() {
  print("spawn");
  return IsolateRunner.spawn();
}
```