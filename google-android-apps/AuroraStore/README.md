# Aurora Store

Aurora Store 是 Google Play Store 的第三方開源(GPL 3) 客戶端。她可以下載更新 Google Play Store 中的程式，並且不需要 Google 專有框架和 Google Play 等服務。

對於朝鮮等地區的 andord 手機是 Play Store 的最好替代品

* 官網 [https://auroraoss.com/](https://auroraoss.com/)
* 源碼 [https://gitlab.com/AuroraOSS/AuroraStore](https://gitlab.com/AuroraOSS/AuroraStore)
* 下載 [https://gitlab.com/AuroraOSS/AuroraStore/-/releases](https://gitlab.com/AuroraOSS/AuroraStore/-/releases)