# oak

oak 是一個流行的開源(MIT) HTTP 中間件，它可以工作在 deno 或 node(>=16.5)，本文檔只以 deno 爲例

* 源碼 [https://github.com/oakserver/oak](https://github.com/oakserver/oak)
* deno [https://deno.land/x/oak](https://deno.land/x/oak)