# [border-width](https://www.primefaces.org/primeflex/borderwidth)

border-bottom-width 指定了邊框寬度

<table class="doc-table">
		<thead>
		<tr>
				<th>Class</th>
				<th>Properties</th>
		</tr>
		</thead>
		<tbody>
		<tr>
				<td>border-none</td>
				<td>border-width: 0px;</td>
		</tr>
		<tr>
				<td>border-1</td>
				<td>border-width: 1px;</td>
		</tr>
		<tr>
				<td>border-2</td>
				<td>border-width: 2px;</td>
		</tr>
		<tr>
				<td>border-3</td>
				<td>border-width: 3px;</td>
		</tr>
		<tr>
				<td>border-x-none</td>
				<td>border-right-width: 0px;<br>
						border-left-width: 0px;
				</td>
		</tr>
		<tr>
				<td>border-x-1</td>
				<td>border-right-width: 1px;<br>
						border-left-width: 1px;
				</td>
		</tr>
		<tr>
				<td>border-x-2</td>
				<td>border-right-width: 2px;<br>
						border-left-width: 2px;
				</td>
		</tr>
		<tr>
				<td>border-x-3</td>
				<td>border-right-width: 3px;<br>
						border-left-width: 3px;
				</td>
		</tr>
		<tr>
				<td>border-y-none</td>
				<td>border-top-width: 0px;<br>
						border-bottom-width: 0px;
				</td>
		</tr>
		<tr>
				<td>border-y-1</td>
				<td>border-top-width: 1px;<br>
						border-bottom-width: 1px;
				</td>
		</tr>
		<tr>
				<td>border-y-2</td>
				<td>border-top-width: 2px;<br>
						border-bottom-width: 2px;
				</td>
		</tr>
		<tr>
				<td>border-y-3</td>
				<td>border-top-width: 3px;<br>
						border-bottom-width: 3px;
				</td>
		</tr>
		<tr>
				<td>border-top-none</td>
				<td>border-top-width: 0px;</td>
		</tr>
		<tr>
				<td>border-top-1</td>
				<td>border-top-width: 1px;</td>
		</tr>
		<tr>
				<td>border-top-2</td>
				<td>border-top-width: 2px;</td>
		</tr>
		<tr>
				<td>border-top-3</td>
				<td>border-top-width: 3px;</td>
		</tr>
		<tr>
				<td>border-left-none</td>
				<td>border-left-width: 0px;</td>
		</tr>
		<tr>
				<td>border-left-1</td>
				<td>border-left-width: 1px;</td>
		</tr>
		<tr>
				<td>border-left-2</td>
				<td>border-left-width: 2px;</td>
		</tr>
		<tr>
				<td>border-left-3</td>
				<td>border-left-width: 3px;</td>
		</tr>
		<tr>
				<td>border-bottom-none</td>
				<td>border-bottom-width: 0px;</td>
		</tr>
		<tr>
				<td>border-bottom-1</td>
				<td>border-bottom-width: 1px;</td>
		</tr>
		<tr>
				<td>border-bottom-2</td>
				<td>border-bottom-width: 2px;</td>
		</tr>
		<tr>
				<td>border-bottom-3</td>
				<td>border-bottom-width: 3px;</td>
		</tr>
		<tr>
				<td>border-right-none</td>
				<td>border-right-width: 0px;</td>
		</tr>
		<tr>
				<td>border-right-1</td>
				<td>border-right-width: 1px;</td>
		</tr>
		<tr>
				<td>border-right-2</td>
				<td>border-right-width: 2px;</td>
		</tr>
		<tr>
				<td>border-right-3</td>
				<td>border-right-width: 3px;</td>
		</tr>
		</tbody>
</table>

![](assets/0.png)
	
```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container blue-container">
        <div class="border-none w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-none</div>
        <div class="border-1 border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-1</div>
        <div class="border-2 border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-2</div>
        <div class="border-3 border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-3</div>
    </div>
</div>
```

你可以只定義特定方向的邊框

![](assets/1.png)

```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container yellow-container">
        <div class="border-top-2 border-yellow-500 w-12rem h-6rem surface-overlay font-bold m-2 flex align-items-center justify-content-center">border-top-2</div>
        <div class="border-right-2 border-yellow-500 w-12rem h-6rem surface-overlay font-bold m-2 flex align-items-center justify-content-center">border-right-2</div>
        <div class="border-bottom-2 border-yellow-500 w-12rem h-6rem surface-overlay font-bold m-2 flex align-items-center justify-content-center">border-bottom-2</div>
        <div class="border-left-2 border-yellow-500 w-12rem h-6rem surface-overlay font-bold m-2 flex align-items-center justify-content-center">border-left-2</div>
    </div>
</div>
```

![](assets/2.png)
```
<div class="card">
    <div class="flex flex-wrap md:justify-content-start justify-content-center card-container green-container">
        <div class="border-x-2 border-green-500 w-12rem h-6rem surface-overlay font-bold m-2 flex align-items-center justify-content-center">border-x-2</div>
        <div class="border-y-2 border-green-500 w-12rem h-6rem surface-overlay font-bold m-2 flex align-items-center justify-content-center">border-y-2</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同