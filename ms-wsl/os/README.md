# 安裝 os

直接在 微軟提供的應用商店即可 安裝 子系統的 os

然這會被安裝到 **%LOCALAPPDATA%\[package name]\rootfs** 如果需要改變安裝位置需要手動安裝

# 卸載 os

```
wslconfig /u XXX
```

# 手動安裝 os
1. 創建一個目錄用於安裝 os (比如 d:\linux\ubuntu-20.04)
2. 到 微軟提供的 [linux發行版列表](https://docs.microsoft.com/en-us/windows/wsl/install-manual#downloading-distros) 下載 os 的 appx 檔案
3. 將下載內容以 zip 解壓到 step 1 創建的安裝目錄
4. 解壓出來的安裝包中通常存在一個 exe 檔案 運行之 進行系統安裝

step 4 的 exe 會完成 系統初始化(ext4.vhdx) 並將 os 註冊到 wsl 也可以使用下述指令 自定義註冊信息

```
wsl --import <DistributionName> <InstallLocation> <FileName>
```

* DistributionName 在 wsl 中的註冊名稱
* InstallLocation 安裝路徑
* FileName 安裝壓縮包位置(install.tar.gz) 和 step 4 的 exe 在相同目錄

```
wsl --import ubuntu d:\linux\ubuntu-20.04 d:\linux\ubuntu-20.04\install.tar.gz
```