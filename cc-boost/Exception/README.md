# [Exception](https://www.boost.org/doc/libs/1_76_0/libs/exception/doc/boost-exception.html)

```
#include <boost/exception/all.hpp>
```
```
#info="cmake"
find_package(Boost
    1.76.0
    REQUIRED
    COMPONENTS 
        exception
)
```

* Author(s) -> Emil Dotchevski
* First Release -> 1.36.0
* C++ Standard Minimum Level ->03
* Categories -> Language Features Emulation

傳統上當函數發生錯誤時 拋出異常 然而當調用鏈很長時 底層調用棧 缺少必要的調用信息故只能拋出較低階的異常類型 當異常向上冒泡時 上層調用者 catch 異常 包裝後再繼續拋出 然問題便在此:
1. 包裝異常對象，意味着底層對象必須被複製，這可能會盜賊碎片產生
2. 包裝異常很難在通用的上下文中使用，除非所有包裝類派生自己同一基類(然這會引發更多問題:很難寫代碼比如給一堆異常類取名字，實現基本接口，異常類很難維持中立性質)

爲此 boost::exception 提供了自己的解決方案 實現一個同用的異常類 派生自 boost::exception 然後 在異常冒泡時 boost::exception 允許添加額外的 異常信息

```
#include <iostream>
#include <boost/exception/all.hpp>
// 定義一個 標記用於 將 int 數據加入到異常中
typedef boost::error_info<struct err_code, int> err_code_t;
typedef boost::error_info<struct err_message, const char *> err_message_t;
// 定義通用的 異常類型 派生自 boost::exception
struct my_exception_t : virtual boost::exception, virtual std::exception
{
}; //(2)
void call_0()
{
    // 拋出異常 並附帶上一個消息
    throw my_exception_t() << err_code_t(1);
}
void call_1()
{
    try
    {
        call_0();
    }
    catch (const boost::exception &e)
    {
        // 附帶上額外消息 之後 拋個上層調用者
        e << err_message_t("call_1");
        throw;
    }
}
int main(int argc, char *argv[])
{
    try
    {
        call_1();
        return 0;
    }
    catch (const boost::exception &e)
    {
        // 使用 get_error_info 獲取 附帶信息
        int const *code = boost::get_error_info<err_code_t>(e);
        if (code)
        {
            std::cerr << "code: " << *code << "\n";
        }
        const char *const *message = boost::get_error_info<err_message_t>(e);
        if (message)
        {
            std::cerr << "message: " << *message << "\n";
        }

        std::cerr << std::flush;
    }
    return -1;
}
```

此外 boost 內部的一些類型 可以直接添加到 異常信息中 下例演示了 將 tuple 添加到異常
```
#include <boost/exception/all.hpp>

typedef boost::tuple<boost::errinfo_api_function, boost::errinfo_errno> clib_failure;
struct file_open_error : virtual boost::exception
{
};
boost::shared_ptr<FILE> file_open(char const *name, char const *mode)
{
    if (FILE *f = fopen(name, mode))
    {
        return boost::shared_ptr<FILE>(f, fclose);
    }

    throw file_open_error() << boost::errinfo_file_name(name) << clib_failure("fopen", errno);
}
```

# enable_error_info

有時 我們無法修改底層的異常從 boost::exception(比如由語言層拋出 會第三方庫拋出) 此時我們可以在自己的代碼中使用 enable_error_info 函數

enable_error_info&lt;T&gt; 爲我們獲取了一個未指定類型的對象 保證該對象從 boost::exception 和 T  派生，異步後續代碼 可以將其作爲 boost::exception 處理，並且因爲同時從 T 派生 所以 現有異常處理不會被中斷

```
#include <boost/exception/all.hpp>
#include <stdexcept>

typedef boost::error_info<struct tag_std_range_min,size_t> std_range_min;
typedef boost::error_info<struct tag_std_range_max,size_t> std_range_max;
typedef boost::error_info<struct tag_std_range_index,size_t> std_range_index;

template <class T>
class
my_container
    {
    public:

    size_t size() const;

    T const &
    operator[]( size_t i ) const
        {
        if( i > size() )
            throw boost::enable_error_info(std::range_error("Index out of range")) <<
                std_range_min(0) <<
                std_range_max(size()) <<
                std_range_index(i);
        //....
        }
    };
```

# 傳輸異常
boost::exception 支持通過 cloning 在線程間傳輸異常對象，這類似 [N2179](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2179.html)，但 bosst 不能依賴語言支持 因爲 boost::exception 在 c++03便被支持 ，所以需要在 throw時 使用 enable_current_exception 才能使用 cloning

由 boost::throw_exception 拋出的所有異常都保證從 boost::exception 派生 並且支持 cloning

```
#include <iostream>
#include <boost/exception/all.hpp>
#include <boost/thread.hpp>
struct work_error : virtual boost::exception
{
};
void do_work()
{
    throw boost::enable_current_exception(work_error());
}
void worker_thread(boost::exception_ptr &error)
{
    try
    {
        do_work();
        error = boost::exception_ptr();
    }
    catch (...)
    {
        // 將異常 cloning 返回到主線程
        error = boost::current_exception();
    }
}
int main(int argc, char *argv[])
{
    try
    {
        boost::exception_ptr error;
        boost::thread t(boost::bind(worker_thread, boost::ref(error)));
        t.join();
        if (error)
        {
            // 使用 rethrow_exception 將異常繼續外拋
            boost::rethrow_exception(error);
        }
    }
    catch (const boost::exception &e)
    {
        std::cerr << '\n';
    }

    return -1;
}
```

# virtual 繼承

自定義的異常類應該使用 virtual 派生 這可以防止異常處理中的 歧義問題

```
#include <iostream>
struct my_exc1 : std::exception
{
    char const *what() const throw()
    {
        return "my_exc1";
    }
};
struct my_exc2 : std::exception
{
    char const *what() const throw()
    {
        return "my_exc2";
    }
};
struct your_exc3 : my_exc1, my_exc2
{
};

int main()
{
    try
    {
        throw your_exc3();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "whoops!" << std::endl;
    }
}
```
上述代碼 會打印 **whoops** 因爲 轉換爲 std::exception 是不明確的，而如果使用 virtual 派生 編譯器會識別出這種錯誤

```
#include <iostream>
struct my_exc1 : virtual std::exception
{
    char const *what() const throw()
    {
        return "my_exc1";
    }
};
struct my_exc2 : virtual std::exception
{
    char const *what() const throw()
    {
        return "my_exc2";
    }
};
struct your_exc3 : my_exc1, my_exc2
{
    char const *what() const throw()
    {
        return "your_exc3";
    }
};

int main()
{
    try
    {
        throw your_exc3();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "whoops!" << std::endl;
    }
}
```
# 診斷信息

boost::diagnostic_information 函數 可以識別出所有 boost::exception 帶的信息 這對於調試和記錄日誌 比較有用

```
#include <iostream>
#include <boost/exception/all.hpp>
// 定義一個 標記用於 將 int 數據加入到異常中
typedef boost::error_info<struct err_code, int> err_code_t;
typedef boost::error_info<struct err_message, const char *> err_message_t;
// 定義通用的 異常類型 派生自 boost::exception
struct my_exception_t : virtual boost::exception, virtual std::exception
{
}; //(2)
void call_0()
{
    // 拋出異常 並附帶上一個消息
    throw my_exception_t() << err_code_t(1);
}
void call_1()
{
    try
    {
        call_0();
    }
    catch (const boost::exception &e)
    {
        // 附帶上額外消息 之後 拋個上層調用者
        e << err_message_t("call_1");
        throw;
    }
}
int main(int argc, char *argv[])
{
    try
    {
        call_1();
        return 0;
    }
    catch (const boost::exception &e)
    {
        std::cerr << boost::diagnostic_information(e) << std::flush;
    }
    return -1;
}
```

```
$ ./bin/console 
Throw location unknown (consider using BOOST_THROW_EXCEPTION)
Dynamic exception type: my_exception_t
[err_message*] = call_1
[err_code*] = 1
```