# nfs-kernel-server

nfs-kernel-server 是 ubuntu 上的 nfs 服務器

nfs-kernel-server 會自動安裝 rpcbind

```
sudo apt install nfs-kernel-server
```

# nfs-common
nfs-common 是 ubuntu 上的 nfs 客戶端程式

```
sudo apt install nfs-common
```