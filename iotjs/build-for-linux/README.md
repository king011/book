# [build for linux](https://github.com/jerryscript-project/iotjs/blob/master/docs/build/Build-for-x86-Linux.md)

官方推薦使用 ubuntu14.04 作爲開發環境，實現需要按照一些依賴的工具

```
sudo apt-get install -y gyp cmake build-essential valgrind gcc
```

# 編譯

```
./tools/build.py --jerry-profile es.next \
	--buildtype release \
	--link-flag=-static \
	--target-os linux \
	--target-arch x86_64 \
	--cmake-param=-DENABLE_MODULE_WEBSOCKET=ON \
	--cmake-param=-DENABLE_MODULE_TLS=ON \
	--cmake-param=-DENABLE_MODULE_HTTPS=ON \
	--clean
```

* **--jerry-profile** 指定支持的 es 版本，默認爲 5.1，但 5.1 並不支持 Promose，沒有 Promose 代碼只會混亂不堪，建議指定 es.next 以支持 Promose 但這會導致 iotjs 由變得更大
* **--buildtype** 指定了編譯模式默認爲 debug，帶有各種調試信息用於排錯
* **--link-flag** 指定了靜態鏈接依賴，嵌入式設備很多都缺少必要的組件，建議使用靜態鏈接，否則可能缺少依賴而無法運行
* **--target-os** 指定目標操作系統
* **--target-arch** 指定目標平臺
* **--cmake** 指定了傳遞給 cmake 的參數
	* **-DENABLE\_MODULE\_WEBSOCKET=ON** 啓用了 websocket 模塊，你可以啓動更多需要的模塊
	* **-DENABLE\_MODULE\_TLS=ON** 啓用 tls 模塊
	* **-DENABLE\_MODULE\_HTTPS** 啓用 https 模塊
* **--clean** 指定了刪除緩存重新編譯，如果改變了編譯設定最好傳入這個參數