# typedoc

typedoc 一個開源(Apache 2) typescript 工具 用於自動生成 typescript的api文檔

```
# 安裝
npm install typedoc --global
# 創建文檔
typedoc
# 查看使用說明
typedoc -h
```

```
export class Statistics {
  /**
   * Returns the average of two numbers.
   *
   * @remarks
   * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
   *
   * @param x - The first input number
   * @param y - The second input number
   * @returns The arithmetic mean of `x` and `y`
   *
   * @beta
   */
  public static getAverage(x: number, y: number): number {
    return (x + y) / 2.0;
  }
}
```