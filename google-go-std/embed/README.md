# [embed](https://pkg.go.dev/embed)

embed 是 go1.6 新加入的 特性和包 可以在編譯階段將 靜態資源 打包到編譯號的程式中 並提供了訪問這些資源的能力


```
package assets

// 所有要嵌入資源的檔案 都必須 import embed
import _ "embed"

//go:embed version.txt
// 這個字符串嵌入 version.txt 的內容
var Version string
```

使用 `//go:embed pattern` 指定想嵌入的資源 **//** 後不能由空格

pattern 是 path.Match 所支持的路徑統配符號

使用 `man 7 glob` 可以查看更詳細的規則

| 通配符 | 含義 |
| -------- | -------- |
| ?     | 代表任意一個字符(不包括半角中括號)     |
| *     | 代表任意字符串(不包括半角中括號)     |
| \[\.\.\.\] 和 \[\!\.\.\.\]    | 任意一個匹配括號內的字符,!任意不匹配括號中字符的字符     |
| \[a\-z\] 和 \[0\-9\]     | 分別代表任意字符 或 數字     |

```
//go:embed images
匹配 images 及其子目錄

//go:embed *
匹配 所有默認內容

//go:embed images/*
匹配 images 檔案夾下 所有內容
```

# 使用資源

在 go:embed 指令下定義一個 變量 golang 會將資源格式化到其中



| Column 1 | Column 2 |
| -------- | -------- |
| \[\]byte     | 檔案二進制資源     | 
| string     | utf8 編碼的單個文本資源     | 
| embed.FS     | 多個檔案和目錄，實現了 http.FileSystem 接口 以及 ReadDir ReadFile 函數     | 

# ModTime
**目前 go1.17.6 linux** 下測試，嵌入檔案的 **Stat().ModTime()** 將返回 zero，這樣的字節結果就默認的 http 接口以靜態數據返回檔案時無法設置有效的 **Last-Modified** 這將字節導致 http 無法使用 瀏覽器緩存

一個解決辦法是使用一個包裝器來 替代 http.FS

```
package static

import (
	"io/fs"
	"net/http"
	"time"
)

func FS(fsys fs.FS, modtime time.Time) http.FileSystem {
	return fileSystemModtime{
		FileSystem: http.FS(fsys),
		modtime:    modtime,
	}
}

type fileSystemModtime struct {
	http.FileSystem
	modtime time.Time
}

func (fs fileSystemModtime) Open(name string) (http.File, error) {
	f, e := fs.FileSystem.Open(name)
	if e != nil {
		return nil, e
	}
	return fileModtime{
		File:    f,
		modtime: fs.modtime,
	}, nil
}

type fileModtime struct {
	http.File
	modtime time.Time
}

func (f fileModtime) Stat() (fs.FileInfo, error) {
	fileInfo, e := f.File.Stat()
	if e != nil {
		return nil, e
	}
	return fileInfoModtime{
		FileInfo: fileInfo,
		modtime:  f.modtime,
	}, nil
}

type fileInfoModtime struct {
	fs.FileInfo
	modtime time.Time
}

func (f fileInfoModtime) ModTime() time.Time {
	return f.modtime
}
```
# 注意

1. 目前資源都是沒有經過壓縮的，如果需要壓縮要自己想變法處理
2. 無論以何種 類型嵌入 其數據都是直接存儲到內存中的 故如果嵌入資源很大 應該考慮自行壓縮
3.  如果同一資源被嵌入多次 爲創建多個相同副本 故寫嵌入規則時要小心別多次嵌入
4.  嵌入資源的路徑都是使用相對路徑 且無論平臺都以 **/** 作爲分隔符號

