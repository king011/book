# [UnityHub](https://docs.unity3d.com/hub/manual/InstallHub.html)
	
unity 提供了 UnityHub 用於管理 UnityEditor

使用 UnityHub 可以方便的管理 UnityEditor 的安裝 升級 添加刪除模塊 多版本環境

# Linux

1. 從官網 [https://unity3d.com/get-unity/download](https://unity3d.com/get-unity/download) 下載 UnityHub.AppImage 

2. 設置 可執行權限

	```sh
	chmod a+x UnityHub.AppImage
	```
	
3. 運行 Unity Hub

	```
	./UnityHub.AppImage
	```
	
# [Debian or Ubuntu](https://docs.unity3d.com/hub/manual/InstallHub.html#install-hub-linux)

官方提供了 Debian 的 apt 安裝支持

```
sudo sh -c 'echo "deb https://hub.unity3d.com/linux/repos/deb stable main" > /etc/apt/sources.list.d/unityhub.list'
wget -qO - https://hub.unity3d.com/linux/keys/public | sudo apt-key add -
sudo apt update
sudo apt-get install unityhub
```

目前 2022-01-11 似乎 apt 會安裝 3.0.0-beta 版本並不穩定，故暫時不推薦
	
# 西朝鮮
如果你在**西朝鮮**或類似地區記住要開上代理再 運行 UnityHub 來安裝或更新 
UnityEditor 不然可能會安裝一個特化版本

例如正常版本 2020.3.25.f1 在西朝鮮會安裝 2020.3.25.f1**c1** 版本，似乎西朝鮮特供版本都帶有 c1 後綴，這個版本似乎也沒有太大的不同只是不會從 .com 下載內置的 Editor 資源與工具而是從 .cn 下載，unity 本意應該是因爲網路封鎖從 .com 下載不了一些資源所以提供了特化的 .cn，但是她他們沒想到其實西朝鮮是一羣瘋狗在管理很多 unity 提供的 .cn 特化資源也是經常訪問不了或偶爾中斷從而導致訪問 .cn 的特化版本經常出現莫名其妙的不穩定問題

所以其實帶後綴的 **c1** 特化版本也沒解決西朝鮮網路封鎖的問題，反而導致一些代理方案無法訪問或訪問 .cn 資源存在問題或緩慢，故不要安裝這個版本，請掛上代理直接安裝正常人類的版本然後通過代理解決 UnityEditor 和 依賴 的安裝更新等問題

此外可以設置 HTTP\_PROXY 與 HTTPS\_PROXY 環境變量以告訴 unity 使用代理訪問網路

```
export HTTP_PROXY=http://127.0.0.1:8118
export HTTPS_PROXY=http://127.0.0.1:8118
```