# Virtual-Display-Driver

Virtual-Display-Driver 是開源(MIT)的虛擬屏幕，可以爲 windwos 10/11 提供一個虛擬的屏幕就像一個真的屏幕一樣，可以配合 Sunshine 來玩一些有趣的應用

下載最新安裝包 [Virtual.Display.Driver-v24.12.24-setup-x64.exe](https://github.com/VirtualDisplay/Virtual-Display-Driver/releases/latest) 安裝即可，安裝成功就會在設備中發現多了一個虛擬屏幕

# 多屏幕技巧

在 windows 中存在多個屏幕時，可以使用快捷鍵 win+shift+方向鍵 來將應用移到到其它屏幕去，這也是移到全屏應用到其它屏幕的最快捷有效的方法

# Sunshine

1. Virtual-Display-Driver 運行後會有個系統托盤，在裏面選擇綁定的顯卡，對於混合顯卡的用戶請選擇獨顯，否則會影響 Sunshine 遠程效率
2. 首先去 windows 顯示設置中，將多屏幕顯示方案設置爲  **擴展**，之後爲這個虛擬屏幕設置好分辨率刷新率等
3. 執行 Sunshine 提供的工具 **tools\dxgi-info.exe** 找到正確的顯卡名稱和輸出設備id，填寫到 Sunshine 網頁設置的 Audio/Video 中

注意這裏有個巨坑，dxgi-info 只輸出了 輸出設備名，網頁上也顯示的要填入屏幕的設備名稱，但實際上對於 windows 需要填寫屏幕設備 id才能正常工作(這在 Sunshine 文檔中有寫但如果不仔細去看文檔只看ui很任意被坑)。
要獲取設備 id，可以去看 Sunshine 的啓動日誌，裏面會有識別到的所有屏幕名稱(**display\_name**)和對應的設備 id(**device\_id**)

# 一些應用場景

* 最常見的就是讓 Sunshine 串流虛擬屏幕，這樣可以一邊串流玩遊戲，同時不影響主屏幕繼續做其它工作
* 對於混合顯卡，Sunshine 可能無法正確的使用獨顯串流，這時也可以選擇創建一個虛擬屏幕和獨顯綁定作爲串流的目標
* android tv 會把所有手柄識別爲同一個，我創建了一個額外的 Sunshine和虛擬屏幕，這樣通過多個不同的 Sunshine 服務串流，每個獨立服務的手柄能夠被單獨識別出來，於是我可以和家人一起使用多個手柄玩電視上的串流遊戲。(這個方案中主服務被串流到電視，次服務只是爲了傳輸手柄控制所以次服務的屏幕分辨率和Moonlight分辨率建議設置到最低)
