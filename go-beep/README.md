# beep

github.com/faiface/beep 是一個開源(MIT) 的 golang 跨平臺音樂播放庫

```
go get -u github.com/faiface/beep
```

* 源碼 [https://github.com/faiface/beep](https://github.com/faiface/beep)

# 特性

beep 建立在 [Streamer](https://pkg.go.dev/github.com/faiface/beep#Streamer) 接口之上，它類似與 [io.Reader](https://pkg.go.dev/io#Reader) 但用於音頻

* 解碼 和 播放 **WAV MP3 OGG FLAC**
* 編碼並保存 WAV
* 非常簡單的API。限制對 立體聲（雙聲道）音頻的支持使得簡化架構和 API 成爲可能
* 豐富的合成器和效果庫。循環 暫停/恢復 更改音量 混音 回放 更改播放速度
* 輕鬆創建新效果。使用 Streamer 接口 創建新效果非常容易
* 生成人造聲音
* 核心代碼很少
# 依賴

beep 使用 [github.com/hajimehoshi/oto](https://github.com/hajimehoshi/oto) 作爲後端播放引擎，oto 使用了 cgo 並且對於不同的系統平臺可能需要安裝不同的第三方庫

對於 ubuntu 可以這麼做

```
sudo apt install libasound2-dev
```