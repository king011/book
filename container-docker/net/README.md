# 網路
使用大寫 **\-P** 參數 docker 會自動創建一個 \[49000,49900\] 間隨機 端口 映射到容器
```sh
$ sudo docker run -d -P training/webapp python app.py
$ sudo docker ps -l
CONTAINER ID  IMAGE                   COMMAND       CREATED        STATUS        PORTS                    NAMES
bc533791f3f5  training/webapp:latest  python app.py 5 seconds ago  Up 2 seconds  0.0.0.0:49155->5000/tcp  nostalgic_morse
```

使用 小寫 **-p** 可以詳細指定 映射規則

-p 主機地址:主機端口:容器端口
* 如果省略 主機地址 則使用 0.0.0.0
* 如果省略 直接端口 則使用 隨機端口

```sh
sudo docker run -itp 9000  --name kt2 -v /home/king/test:/tshare ubuntu /bin/bash
sudo docker run -itp 9000/udp  --name kt2 -v /home/king/test:/tshare ubuntu /bin/bash
```

> -p 參數 可多次使用 以便 映射 多個 端口

# docker inspect
使用 docker inspect 可以 參看 容器 詳情
```sh
docker inspect 5260a1e81389
```

使用 -f 參數 可以 參看 指定 條目
```sh
# 參看 容器 ip
docker inspect -f "{{.NetworkSettings.IPAddress}}" 5260a1e81389
```

# 虛擬網卡
除了 使用 docker 提供的 端口映射外

docker 實際上 虛擬了一個 docker0 網卡 可以使用 docker0地址(主機地址) 和容器 地址 通過 ip協議 通信

```sh
$ ifconfig docker0
docker0   Link encap:Ethernet  HWaddr 02:42:67:bc:13:30  
          inet addr:172.17.0.1  Bcast:0.0.0.0  Mask:255.255.0.0
          inet6 addr: fe80::42:67ff:febc:1330/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:12820 errors:0 dropped:0 overruns:0 frame:0
          TX packets:13446 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:972980 (972.9 KB)  TX bytes:26839479 (26.8 MB)
```

# --link
因爲 docker 虛擬了網卡 所以只要知道 容器 ip就可以訪問 但這必須 依賴 ip地址

為了不依賴 ip docker run 支持 --link 參數 可以將 指定容器 在當前容器中創建一個別名 以便當前容器可以使用 別名而非 不確定的ip訪問 其它容器

docker run --link 被連接的容器:可選的別名

```sh
# 運行 kt1 容器 並將 kt2 容器 連接進來 別名為 web
sudo docker run -it --name kt1 --link kt2:web -v /home/king/test:/tshare ubuntu /bin/bash
```

# 自定義網絡

可以自定義一個網卡 以便爲容器指定 固定 ip

```
# 創建一個名稱爲 private_intranet 的虛擬網絡
docker network create private_intranet

# 創建虛擬網絡並指定子網
docker network create --subnet=172.18.0.0/16 private_intranet

# 查詢 docker 網絡
docker network ls
docker inspect private_intranet

# 創建容器並指定固定ip
# --net 指定網絡
# --ip 指定ip
docker run  --net private_intranet --ip 172.18.0.2 -p 80:80/tcp -d  nginx
```