# [gui](https://learn.microsoft.com/en-us/windows/wsl/tutorials/gui-apps)

wsl 可以運行 gui 程序但不提供完整的桌面支持，他們依賴 windows 的桌面一些以桌面爲中心的工具或程式可能需要額外支持才能運行或者根本無法運行

後續行文都假設 wsl 子系統使用的是 Ubuntu
# [vGPU]
首先應該安裝 vGPU 驅動，以使 wsl gui 能夠使用顯卡提供的硬件加速(這不是必須的但建議安裝)

* [Intel GPU driver for WSL](https://www.intel.com/content/www/us/en/download/19344/intel-graphics-windows-dch-drivers.html)
* [AMD GPU driver for WSL](https://www.amd.com/en/support/kb/release-notes/rn-rad-win-wsl-support)
* [NVIDIA GPU driver for WSL](https://developer.nvidia.com/cuda/wsl)

# nautilus

nautilus 是 GNOME  桌面的一個檔案管理器，雖然 windows 的 explorer.exe 也能操作 wsl 子系統檔案，但是 explorer.exe 是以 root 權限進行操作這會打亂 子系統 中檔案的權限配置，故推薦安裝
```
sudo apt install nautilus -y
```