# [泛型](https://go.dev/doc/tutorial/generics)

golang 從 1.18 開始正式支持泛型

此外你需要安裝或更新 gopls 以使用 IDE 支持泛型

```
go install golang.org/x/tools/gopls@latest
```

# 編寫泛型代碼

1. go 在函數名定義後使用方括號定義泛型型別，需要一個泛型名稱以及泛型約束

    ```
    func SumIntsOrFloats[K comparable, V int64 | float64](m map[K]V) V {
    	var s V
    	for _, v := range m {
    		s += v
    	}
    	return s
    }
    ```
	
	上述 K 和 V 是泛型名稱，在泛型函數中定義中可以將它們當作型別名稱使用
	
	泛型名使用空格和其約束定義隔開 
	* comparable 約束了 K 型別需要是可比較的
	* int64 | float64 約束了 V 型別只能是 int64 或 float64

2. 調用泛型函數和調用普通函數類似，可以使用方括號傳入泛型型別，但是如果編譯器可以推導正確型別則可以省略型別參數

	```
	func main() {
		ints := make(map[int]int64)
		floats := make(map[int]float64)
		for i := 0; i < 10; i++ {
			ints[i] = int64(i + 1)
			floats[i] = float64(i+1) + float64(i+1)/100
		}
		fmt.Println(SumIntsOrFloats(ints))
		fmt.Println(SumIntsOrFloats[int, float64](floats))
	}
	```

# 聲明約束

你可以聲明自己約束這可以簡化代碼，約束的語法類似接口但它還可以支持特定類型

```
type Number interface {
    int64 | float64
}
```

```
package main

import "os"

type Write interface {
	Write(p []byte) (n int, err error)
}

func Print[W Write](w W, b []byte) {
	w.Write(b)
}

func main() {
	Print(os.Stdout, []byte("cerberus is an idea\n"))
}
```