# 緩存
xorm 提供了 內存 緩衝功能

```go
//使用一個 1000 條記錄的 LRU算法緩存
cacher := xorm.NewLRUCacher(xorm.NewMemoryStore(), 1000)
//設置所有 表默認 緩存策略
engine.SetDefaultCacher(cacher)
 
//對指定表 不使用 緩存
//engine.MapCacher(&user, nil)
 
//對指定表 指定 特定的 緩存
//engine.MapCacher(&user, cacher) 
```

> xorm 只針對 有 pk 的表建立 緩存
> 
> 如果 調用 Exec 方法 會使 數據庫 和 緩存不一致   
> 此時需要手動 調用ClearCache  
> engine.Exec("update user set name = ? where id = ?", "xlw", 1)  
> engine.ClearCache(new(User))
> 
> xorm 使用的是 sql 命令 字符串 作爲 緩存的 key  
> (故 儘量使用 相同的 字符串 比如大小寫不同的 兩個相同命令 會創建出兩個 緩存)
> 

# Get
調用 Get 傳入的 &bean 在使用了緩存時 會直接 作爲 緩存 保存 故 不要 修改其值(需要要修改 修改創建一個 副本 修改副本) 否則 會 導致 緩存數據和 數據庫 不一致

```go
package mysql

import (
	"fmt"
	"github.com/go-xorm/xorm"
	"log"
)

var _Engine *xorm.Engine

// Person .
type Person struct {
	ID   int64 `xorm:"pk autoincr 'id'"`
	Name string
}

// Run .
func Run() {
	engine, e := xorm.NewEngine("mysql", "KingTest:12345678@/KingTest?charset=utf8")
	if e != nil {
		log.Fatalln(e)
	}
	e = engine.Ping()
	if e != nil {
		log.Fatalln(e)
	}

	//使用一個 1000 條記錄的 LRU算法緩存
	cacher := xorm.NewLRUCacher(xorm.NewMemoryStore(), 1000)
	//設置所有 表默認 緩存策略
	engine.SetDefaultCacher(cacher)

	var ok bool
	p0 := &Person{ID: 1}
	ok, e = engine.Get(p0)
	fmt.Println(ok, e, p0) // true <nil> &{1 king}
	p0.Name = "kk"

	p1 := &Person{ID: 1}
	ok, e = engine.Get(p1)
	fmt.Println(ok, e, p1) // true <nil> &{1 kk}
}
```

