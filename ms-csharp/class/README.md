# static

static 用於 定義 靜態 變量 方法

c#還可以 在 class 前 加 static 這樣的class 不能被 new 創建

#  訪問修飾符

* public 任何代碼可訪問
* protected 類和其派生類可訪問
* private 只能當前類訪問
* internal    當前項目可訪問

1. 類只能使用 internal(默認) 或 public 
2. 成員四種都可使用

# 修飾符
修飾符用於指定 類或成員特性
## 類修飾符
* abstract 抽象類
* sealed 密封類不可被繼承
* static 靜態類

## 字段修飾符號

* readonly 只讀
* static 靜態

## 方法修飾符號

* virtual 虛擬
* abstract 抽象
* override 重載 
* static 靜態
* sealed 密封

# get set

set 時 特殊變量 value 代表被設置的值

```
class People
{
    private int lv;
    private string name;
    public People(string name)
    {
        this.name = name;
    }
 
    public int Lv
    {
        set
        {
            if (value < 1 || value > 10)
            {
                lv = 1;
            }
            else
            {
                lv = value;
            }
        }
        get
        {
            return lv;
        }
    }
    public string Name
    {
        get
        {
            return name;
        }
    }
}
 
class Program
{
    static void Main(string[] args)
    {
        People one = new People("king");
        one.Lv = 100;
        Console.WriteLine("{0} lv = {1}",one.Name,one.Lv);
    }
}
```

# const
c# 使用 const 定義 常量  且 class的 常量成員 默認都是static
```
class XXX
{
    public const PI = 3.14;
}
```

readonly 修飾 class 的 只讀變量 只讀變量 只可以在 定義時設置值 或在 class的構造函數中 設置值 

static readonly 變量 只可在 定義時設置值 或在 class的static構造函數中 設置值 

# partial

c# 允許將 class 定義到多個 文件，但在 主文件之外的定義時 class前 都需要加上 partial修飾符

# 派生

c#中 子類只能從1個 基類派生

如果 在 class 前 加上 sealed 修飾符 class 將 不能被 繼承

c#在子類 構造中 使用 :base(...) 選擇 使用的 基類 構造函數

# virtual

c#中 使用 virtual 定義 虛函數 子類 使用 override 重寫子類

在 class 前 加上 abstract 定義一個 不能被 實例化的 抽象類