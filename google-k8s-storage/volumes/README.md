# [卷](https://kubernetes.io/docs/concepts/storage/volumes/)

容器中的檔案在磁盤上是臨時存放的，當容器崩潰時檔案會丟失。k8s 提供了卷(Volume)這一抽象概率來解決檔案丟失和多個 Pod 共享檔案的問題。

k8s 支持很多類型的卷。Pod 可以同時使用任意數目的卷類型。臨時卷類型的生命週期與 Pod 相同，但持久卷可以比 Pod 存活更長時間。

卷的核心是包括一些數據的一個目錄，Pod 中容器可以訪問該目錄。所採用的特定的卷類型將決定該目錄如何形成，使用何種介質保存數據。

使用卷時在 **.spec.volumes** 字段中設置爲 Pod 提供的卷，並在 **\.spec\.containers\[\*\]\.volumeMounts**  字段中聲明卷在容器中的掛載位置。卷不能掛載到其它卷之上，也不能硬鏈接。

# 卷類型

k8s 支持多種類型的卷

## cephfs

## configMap

configMap 卷 提供了向 Pod 注入配置數據的方法

```
apiVersion: v1
kind: Pod
metadata:
  name: configmap-pod
spec:
  containers:
    - name: test
      image: busybox
      volumeMounts:
        - name: config-vol
          mountPath: /etc/config
  volumes:
    - name: config-vol
      configMap:
        name: log-config
        items:
          - key: log_level
            path: log_level
```


## emptyDir

當 Pod 分配到某個 Node 上時， emptyDir 卷會被創建，並且在 Pod 在該節點上運行期間，卷一直存在。當 Pod 因爲某些原因被從節點上刪除時， emptyDir 卷中的數據也會被永久刪除

> 容器崩潰不會導致 Pod 被從節點上刪除，因此容器崩潰期間 emptyDir 卷中的數據上安全的

emptyDir 通常有 如下用途

* 緩存空間，例如基於磁盤的歸併排序
* 爲耗時較長的計算任務提供檢查點，以便任務能夠方便的從崩潰前的狀態恢復執行
* 在 web 服務器容器服務數據時，保存內容管理器容器獲取的檔案

取決與你的環境，emptyDir 卷存儲在該節點所使用的介質可以使用 磁盤 SSD 或 網路存儲。但是你可以將 emptyDir.medium 字段設置爲 **Memory** 告訴 k8s 爲你 掛載 tmpfs (基於 RAM 的檔案系統)。雖然 tmpfs 速度非常快，但與磁盤不同。tmpfs 在節點重啓時會被清除，並且所寫入的所以數據都會計入容器內存消耗，受容器內存限制制約。

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /cache
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir: {}
```

## gcePersistentDisk

gcePersistentDisk 卷能將 google 計算引擎(GEC)持久盤(PD) 掛載到 Pod 中


## glusterfs

## hostPath

hostPath 卷能將主機節點檔案系統上的 檔案或目錄掛載到 Pod

hostPath 卷除了必須的 path 屬性外，用戶可以指定可選的 type 屬性:


| 取值 | 行爲 | 
| -------- | -------- | 
|      | 空字符串(默認)用於向後兼容，者意味着在安裝 hostPath 卷之前不會執行任何檢查     | 
| DirectoryOrCreate     | 如果 path 不存在，那麼將根據需要創建空檔案夾，權限設置爲 0755，具有與 kubelet 相同的組和屬主信息     | 
| Directory     | path 必須是存在的檔案夾     | 
| FileOrCreate     | 如果 path 不存在，那麼將根據需要創建空檔案，權限設置爲 0755，具有與 kubelet 相同的組和屬主信息     | 
| File     | path 必須是檔案     | 
| Socket     | path 必須是 unix 套接字     | 
| CharDevice     | path 必須存在字符設備     | 
| BlockDevice     | path 必須存在塊設備     | 

使用 hostPath 卷要小心:
* 具有相同配置的多個 Pod 會由於節點上檔案的不同而在不同節點上有不同的行爲
* 下層主機上創建的檔案或目錄只能由 root 用戶寫入。你需要在 特權容器 中以 root 身份運行進程，或修改主機上的檔案權限以便容器能夠寫入 hostPath 卷

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /test-pd
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # 宿主機上的目錄位置
      path: /data
      # 此字段爲可選
      type: Directory
```

FileOrCreate 模式不會創建檔案的父目錄，如果父目錄不存在，Pod 會啓動失敗。此時可以 將 父目錄 和 檔案分開 掛載

```
apiVersion: v1
kind: Pod
metadata:
  name: test-webserver
spec:
  containers:
  - name: test-webserver
    image: k8s.gcr.io/test-webserver:latest
    volumeMounts:
    - mountPath: /var/local/aaa
      name: mydir
    - mountPath: /var/local/aaa/1.txt
      name: myfile
  volumes:
  - name: mydir
    hostPath:
      # 確保父目錄成功創建。
      path: /var/local/aaa
      type: DirectoryOrCreate
  - name: myfile
    hostPath:
      path: /var/local/aaa/1.txt
      type: FileOrCreate
```

## local

## nfs

## persistentVolumeClaim

## projected

projected 卷能夠將若干現有的卷來源映射到同一目錄上

目前可以映射的卷來源類型如下

* secret
* downwardAPI
* configMap
* serviceAccountToken

```
apiVersion: v1
kind: Pod
metadata:
  name: volume-test
spec:
  containers:
  - name: container-test
    image: busybox
    volumeMounts:
    - name: all-in-one
      mountPath: "/projected-volume"
      readOnly: true
  volumes:
  - name: all-in-one
    projected:
      sources:
      - secret:
          name: mysecret
          items:
            - key: username
              path: my-group/my-username
      - downwardAPI:
          items:
            - path: "labels"
              fieldRef:
                fieldPath: metadata.labels
            - path: "cpu_limit"
              resourceFieldRef:
                containerName: container-test
                resource: limits.cpu
      - configMap:
          name: myconfigmap
          items:
            - key: config
              path: my-group/my-config
```

```
apiVersion: v1
kind: Pod
metadata:
  name: volume-test
spec:
  containers:
  - name: container-test
    image: busybox
    volumeMounts:
    - name: all-in-one
      mountPath: "/projected-volume"
      readOnly: true
  volumes:
  - name: all-in-one
    projected:
      sources:
      - secret:
          name: mysecret
          items:
            - key: username
              path: my-group/my-username
      - secret:
          name: mysecret2
          items:
            - key: password
              path: my-group/my-password
              mode: 511
```

## secret

secret 卷將 Secret 映射到 Pod

# 使用 subPath

有時在單個 Pod 中共享卷 以供多方使用是有用的。 volumeMounts.subPath 屬性可用於指定所引用的卷內的子路徑，而不是其根路徑。

```
apiVersion: v1
kind: Pod
metadata:
  name: my-lamp-site
spec:
    containers:
    - name: mysql
      image: mysql
      env:
      - name: MYSQL_ROOT_PASSWORD
        value: "rootpasswd"
      volumeMounts:
      - mountPath: /var/lib/mysql
        name: site-data
        subPath: mysql
    - name: php
      image: php:7.0-apache
      volumeMounts:
      - mountPath: /var/www/html
        name: site-data
        subPath: html
    volumes:
    - name: site-data
      persistentVolumeClaim:
        claimName: my-lamp-site-data
```

## 使用帶有擴展環境變量的 subPath

使用 subPathExpr 字段可以基於 Downward API 環境變量來構造 subPath 目錄名稱。subPath 和 subPathExpr 屬性是互斥的

```
apiVersion: v1
kind: Pod
metadata:
  name: pod1
spec:
  containers:
  - name: container1
    env:
    - name: POD_NAME
      valueFrom:
        fieldRef:
          apiVersion: v1
          fieldPath: metadata.name
    image: busybox
    command: [ "sh", "-c", "while [ true ]; do echo 'Hello'; sleep 10; done | tee -a /logs/hello.txt" ]
    volumeMounts:
    - name: workdir1
      mountPath: /logs
      subPathExpr: $(POD_NAME)
  restartPolicy: Never
  volumes:
  - name: workdir1
    hostPath:
      path: /var/log/pods
```

# 資源

emptyDir 卷的存儲介質(磁盤 SSD 等)是由保存 kubelet 數據的根目錄(通常是 **/var/lib/kubelet**)的檔案系統的介質確定的。

# 樹外(Out-of-Tree) 插件

Out-of-Tree 插件 使存儲供應商能夠創建自定義的存儲插件而無需將它們的代碼添加到 k8s 倉庫