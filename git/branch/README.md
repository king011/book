# 常用指令
```bash
# 刪除 本地 分支
git branch -d 分支名

# 刪除 遠程 分支
git push origin :分支名
```