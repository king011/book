# 發佈包

dart 提供了 [https://pub.dartlang.org/](https://pub.dartlang.org/) 作爲 官方的 包 倉庫

任何 開源 項目都 可以 將 包 發佈到此 供世界使用

* 發佈的包 必須包含一個 LICENSE COPYING 其它變體名稱的檔案 聲明使用的 開源協議 官方推薦使用 BSD
* 包結果 gzip 壓縮後 大小 不能超過 10m
* 應該只有 託管依賴 雖然支持 git 依賴 但 這在dart中 不是一個 好注意
* 已經發佈的 包 不允許被刪除 但你可以持續更新 但舊版本 會一直生效 以便 依賴舊版本的 代碼 可以就緒運行
* 正式發佈前 應該始終使用 pub publish --dry-run 測試下

