# tcp

```
#include <ev.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct
{
    char *buffer;
    size_t cap;
    size_t n;
} buffer_t;
int init_buffer(buffer_t *buffer, size_t cap)
{
    char *data = malloc(cap);
    if (!data)
    {
        puts("malloc buffer error");
        return -1;
    }
    buffer->buffer = data;
    buffer->n = 0;
    buffer->cap = cap;
    return 0;
}
void free_buffer(buffer_t *buffer)
{
    if (buffer->cap)
    {
        buffer->buffer = 0;
        buffer->n = 0;
        buffer->cap = 0;
    }
}
typedef struct
{
    int fd;
    char *remote;
    int ok;
    buffer_t read;
    buffer_t write;
} conn_t;
void free_conn(conn_t *conn)
{
    if (!conn->ok)
    {
        return;
    }
    conn->ok = 0;
    free(conn->remote);
    free_buffer(&conn->read);
    free_buffer(&conn->write);
    close(conn->fd);
}
conn_t *malloc_conn(const int fd, const char *host, const char *port)
{
    size_t host_len = strlen(host);
    size_t port_len = strlen(port);
    char *remote = malloc(host_len + 1 + port_len + 1);
    if (!remote)
    {
        puts("malloc remote error");
        return 0;
    }
    memcpy(remote, host, host_len);
    remote[host_len] = ':';
    strcpy(remote, host);
    strcpy(remote + host_len + 1, port);

    conn_t *conn = malloc(sizeof(conn_t));
    if (!conn)
    {
        free(remote);
        puts("malloc conn_t error");
        return 0;
    }
    int err = init_buffer(&conn->read, 1024);
    if (err == -1)
    {
        free(remote);
        free(conn);
        return 0;
    }
    err = init_buffer(&conn->write, 1024);
    if (err == -1)
    {
        free(remote);
        free(conn);
        free(conn->read.buffer);
        return 0;
    }
    conn->fd = fd;
    conn->remote = remote;
    conn->ok = 1;
    return conn;
}
// 設置 非阻塞 fd
int set_nonblock(int fd)
{
    int flags, err;
    flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
    {
        return -1;
    }

    flags |= O_NONBLOCK;
    err = fcntl(fd, F_SETFL, flags);
    if (err == -1)
    {
        return -1;
    }
    return 0;
}
int tcp_listen(uint16_t port)
{
    // 創建socket
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1)
    {
        printf("socket error : %d %s\n", errno, strerror(errno));
        return -1;
    }
    // 設置爲非阻塞
    int err = set_nonblock(fd);
    if (err == -1)
    {
        printf("set nonblock error : %d %s\n", errno, strerror(errno));
        close(fd);
        return -1;
    }
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    //addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_port = htons(port);

    // bind
    err = bind(fd, (struct sockaddr *)&addr, sizeof(addr));
    if (err == -1)
    {
        printf("bind error : %d %s\n", errno, strerror(errno));
        close(fd);
        return -1;
    }

    // listen
    err = listen(fd, SOMAXCONN);
    if (err == -1)
    {
        printf("listen error : %d %s\n", errno, strerror(errno));
        close(fd);
        return -1;
    }
    return fd;
}
static void read_cb(EV_P_ ev_io *w, int revents)
{
    conn_t *conn = w->data;
    size_t n = read(w->fd, conn->read.buffer, conn->read.cap);
    if (n == 0)
    {
        printf("one out %s\n", conn->remote);
        ev_io_stop(loop, w);
        free(w);
        free_conn(conn);
        return;
    }
    else if (n < 0)
    {
        printf("read error : %d %s\n", errno, strerror(errno));
        ev_io_stop(loop, w);
        free(w);
        free_conn(conn);
        return;
    }
    conn->read.n = n;
    conn->read.buffer[n] = 0;
    printf("read %s : %s\n", conn->remote, conn->read.buffer);

    ev_io_stop(loop, w);
    ev_io_modify(w, EV_WRITE);
    ev_io_start(loop, w);
}
static void write_cb(EV_P_ ev_io *w, int revents)
{
    conn_t *conn = w->data;
    int n = write(w->fd, conn->read.buffer, conn->read.n);
    ev_io_stop(loop, w);
    if (n < 0)
    {
        printf("write error : %d %s\n", errno, strerror(errno));
        free(w);
        free_conn(conn);
        return;
    }
    ev_io_modify(w, EV_READ);
    ev_io_start(loop, w);
}
static void conn_cb(EV_P_ ev_io *w, int revents)
{
    if (revents & EV_READ)
    {
        read_cb(EV_A_ w, revents);
    }
    else if (revents & EV_WRITE)
    {
        write_cb(EV_A_ w, revents);
    }
}
static void accept_cb(EV_P_ ev_io *w, int revents)
{
    struct sockaddr addr;
    memset(&addr, 0, sizeof(addr));
    socklen_t len = sizeof(addr);
    int fd = accept(w->fd, (struct sockaddr *)&addr, &len);
    if (fd < 0)
    {
        printf("accept error : %d %s\n", errno, strerror(errno));
        return;
    }
    int err = set_nonblock(fd);
    if (err == -1)
    {
        printf("set nonblock error : %d %s\n", errno, strerror(errno));
        close(fd);
    }
    char host[NI_MAXHOST], port[NI_MAXSERV];
    err = getnameinfo(&addr, len,
                      host, sizeof(host),
                      port, sizeof(port),
                      NI_NUMERICHOST | NI_NUMERICSERV);
    conn_t *conn = malloc_conn(fd, host, port);
    if (!conn)
    {
        close(fd);
        return;
    }
    ev_io *conn_watcher = malloc(sizeof(ev_io));
    if (!conn_watcher)
    {
        free_conn(conn);
        puts("malloc error");
        return;
    }
    printf("one in %s\n", conn->remote);
    conn_watcher->data = conn;
    ev_io_init(conn_watcher, conn_cb, conn->fd, EV_READ);
    ev_io_start(loop, conn_watcher);
}
int main(void)
{
    uint16_t port = 7000;
    int listener = tcp_listen(port);
    if (listener == -1)
    {
        return 0;
    }
    printf("work at %d\n", port);

    struct ev_loop *loop = EV_DEFAULT;

    ev_io listener_watcher;
    ev_io_init(&listener_watcher, accept_cb, listener, EV_READ);
    ev_io_start(loop, &listener_watcher);
    ev_run(loop, 0);
    return 0;
}
```