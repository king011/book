# 命令選項

* -f,--file 指定 docker-compose.yml 路徑，默認爲 docker-compose.yml
* -p,--project-name 指定項目名稱，默認爲 docker-compose.yml 所在檔案夾名稱
* --verbose 輸出更多調試信息
* -v,--version 打印版本並退出

# up

構建並運行服務

* -d 在後臺運行服務容器

# down

停止 up 命令啓動的容器，並移除網路

# exec

進入指定容器

# images 

列出容器包含的鏡像

# ps

# start
# stop
# restart
# rm

# top

# kill
# scale

# port
# pause
# run
# log


