# netflix ipv6

當家庭網路啓動了 ipv6 時，客廳使用 android 電視，這時可能代理無法很好的處理 ipv6 流量。

這時android經常會發生 ipv6 解析搶佔響應導致代理失效從而無法訪問到 netflix。如果 android 版本在9以上，可以使用 core 配置一個 dot 服務，然後在 android 電視上設置使用 dot 進行 dns 解析

```
tls://.:853 {
    tls /letsencrypt/fullchain.pem /letsencrypt/privkey.pem
    rewrite type AAAA A
    forward . 8.8.8.8
    cache   300
    log
}
```

上面第三行將 AAAA 記錄攔截爲 A 記錄從而達到禁用 ipv6 服務的效果