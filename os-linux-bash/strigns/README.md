# 大括號

bash 支持使用大括號展開機制(brace expansion),其含義是生成 字面字符串 序列(枚舉每個條目)

```
# 生成字母表 a-z
echo {a..z}

# 無空格分隔的輸出字母表
printf "%c" {a..z}

# 以換行 join 字母表
printf "%s" {a..z}$'\n'

# 將生成內容賦值給變量 alphabet
printf -v alphabet "%c" {a..z}

# 生成數字 [1,100]
echo {1..100}

# 生成的數字最小長度爲2，用0補位
printf "%02d " {0..9}

# 多個大括號，會生成全排的組合
echo {w,t,}h{e{n{,ce{,forth}},re{,in,fore,with{,al}}},ither,at}
echo {a,b,c}{1,2,3}
# 利用全排重複字符串
echo foo{,,,,,,,,,,}
```

# 賦值和連接

```
#  輸出時需要加 \" 否則一些特殊字符會引號歧義導致奇怪結果
x=-n
y="bar"
echo "$x$y"

# 賦值時不需要 \"
z0=$x
z1=$x$y
```

可以使用 IFS 來分割字符串

```
# 分割到變量
str=foo-bar-baz
IFS=- read -r x y z <<< "$str"
echo "$x"
echo "$y"
echo "$z"

# 分割到數組
IFS=- read -ra strs <<< "foo-bar-baz"
for s in "${strs[@]}";do
    echo "$s"
done

# 遍歷字符
while IFS= read -rn1 c; do
    # 對變量 $c 即字符串中的字符進行處理
done <<< "$str"
```


# 替換
bash 提供來多種方法來替換字符串中的子串

| 語法 | 含義 | 示例 | 示例解釋 |
| -------- | -------- | -------- | -------- |
| ${變量/關鍵字/字符串}     | 從左向右第一個關鍵字被替換爲指定字符串     | s="a0-b0-c0-b0" && echo "${s/b0/b1}"     | 將 s 中第一個遇到的 b0 替換爲 b1     |
| ${變量//關鍵字/字符串}     | 將變量中所有關鍵字替換爲指定字符串     | s="a0-b0-c0-b0" && echo "${s//b0/b1}"     | 將 s 中所有遇到的 b0 替換爲 b1     |
| ${變量#關鍵字}     | 從左向右匹配關鍵字的最短子串删除     | s="host:port/a/b/c" && echo "${s#\*/}"     | 删除 host:port       |
| ${變量##關鍵字}     | 從左向右匹配關鍵字的最长子串删除     | s="/a/b/c" && echo "${s##\*/}"     | basename     |
| ${變量%關鍵字}     | 從右向左匹配關鍵字的最短子串删除     | s="/a/b/c" && echo "${s%/\*}"     | dirname      |
| ${變量%%關鍵字}     | 從右向左匹配關鍵字的最长子串删除     | s="host:port/a/b/c" && echo "${s%%/\*}"     | 获取 host:port     |


# 匹配

使用 **=** 进行模式匹配

```
if [[ $file = *.zip ]]; then
    # matched
fi
```

1. **\*** 匹配任意数量的任意字符
2. **\?** 匹配单个字符
3. **\[...\]** 匹配返回 [0-9]
4. **\[\]** 匹配括号内任意字符

使用 **=~** 进行正则表达式匹配

 匹配變量時最好爲變量加上雙引號，否當變量中存儲者一些特殊字符時比較結果可能會出乎意料，下面是一些意外例子
 
 ```
 a="a"
b="[abc]"
if [[ $a == $b ]];then
    echo '相等，因爲 == 右邊變量如果沒有引號會將其認爲是要匹配的模式'
    echo '代碼和 if [[ $a == [abc];then 等效'
fi
 ```

# 截取子串

```
str="hello world"

# 返回字符串长度
echo ${#str}

# 截取子串
echo ${str:6}
i=2
echo ${str:i:i} # ll
```

${var:offset:count}

1. offset 表示的整數無效會返回原字符串
2. count 表示的整數超過最大有效值會自動使用最大有效值

# 大小寫轉換

```
# 使用 -u 聲明的變量在賦值時會自動轉爲大寫
declare -u upper
for c in {a..z}; do
    upper=$c
    echo "$c -> $upper"
done

# 使用 -l 聲明的變量在賦值時會自動轉爲大寫
declare -l lower
for c in {A..Z}; do
    lower=$c
    echo "$c -> $lower"
done
```

bash 在 version 4 之後增加來 ${var^^} 和 ${var,,} 的寫法用於轉大小寫

```
# 轉大寫 ${var^^}
for c in {a..z}; do
    echo "$c -> ${c^^}"
done

# 轉小寫 ${var,,}
declare -l lower
for c in {A..Z}; do
    echo "$c -> ${c,,}"
done
```