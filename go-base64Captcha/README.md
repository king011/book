# base64Captcha

base64Captcha 是一個開源(Apache 2.0) 的golang 驗證碼庫

* 源碼 [https://github.com/mojocn/base64Captcha](https://github.com/mojocn/base64Captcha)
* 文檔 [https://zh.mojotv.cn/go/refactor-base64-captcha](https://zh.mojotv.cn/go/refactor-base64-captcha)
* 在線測試 [https://captcha.mojotv.cn/.netlify/functions/captcha](https://captcha.mojotv.cn/.netlify/functions/captcha)