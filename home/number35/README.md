# 哲學
一些和文化思考方式相關的東西

# 特級廚師
* [中華食物](cook/0)

# 股票
* 概述
    * [數據分析](stock-data/0) 
    * [投資方案](stock-solution/0)
    * [股價估值](stock-value/0)
    * [成交量](stock-volume/0)
    * [蠟燭圖](stock-candle/0)
    * [布林帶](stock-boll/0)
    * [算法](stock-algorithm/0)
* api
    * [yahoo](stock-api-yahoo/0)


# 文字輸入
* [倉頡輸入法](number35-oiargrmbc/0)
* [rime](number35-rime/0)

# 文字排版
* [Markdown](number35-markdown/0)

# 英語
* [Longman Communication 3000](es-us-Longman-Communication-3000/0)
* [美式英語](en-us/0)
* [初級文法](en-grammar/0)

# 西班牙語

* [西班牙語](spanish/0)