# debug

* Android Studio 提供了 完整的 調試器
* Log 提供了多個 static 方法 打印日誌

# Log

[android.util.Log](https://developer.android.com/reference/android/util/Log) 提供了多個 方法 打印日誌 

可以在 Logcat 頁面 查看

* ASSERT wtf 不應該出現的東西 通常是發生了 永遠不應該發送的事件 通常是嚴重的代碼問題才會遇到
* ERROR e 錯誤信息 通常是發生了致命錯誤 程式無法繼續執行
* WARN w 故障信息 通常是發送了 意外的錯誤 但程式 可以 自行處理後繼續 運行
* INFO i 運行信息
* DEBUG d 調試信息
* VERBOSE v 冗餘信息

> VERBOSE 只在開發階段存在 發佈版本時 會自動剝離其代碼
> 
> DEBUG 日誌會 在 運行時會被剝離 在調試時才會 執行
> 
> 其它日誌始終會 執行

```java
#info=false
// tag 通常 指示出現問題的 class 
// msg 則指示 日誌信息
// tr 是當前發送的異常
static int	wtf(String tag, String msg)
static int	wtf(String tag, Throwable tr)
static int	wtf(String tag, String msg, Throwable tr)
static int	e(String tag, String msg)
static int	e(String tag, String msg, Throwable tr)
static int	w(String tag, String msg)
static int	w(String tag, String msg, Throwable tr)
static int	i(String tag, String msg)
static int	i(String tag, String msg, Throwable tr)
static int	d(String tag, String msg)
static int	d(String tag, String msg, Throwable tr)
static int	v(String tag, String msg)
static int	v(String tag, String msg, Throwable tr)
```