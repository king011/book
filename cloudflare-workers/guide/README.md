# 快速開始

執行下述指令,依據提示創建一個新的 worker 項目
```
npm create cloudflare@latest -- my-first-worker
```

執行下述指令在本地運行項目
```
npm run dev
```

編輯 index.ts 來完成代碼

最後執行下述代碼來部署應用到 cloudflare
```
npx wrangler deploy
```