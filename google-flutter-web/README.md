# flutter web

flutter 已經正式支持了 web 平臺

```
# 在 chrome 上以 auto 模式渲染運行
flutter run -d chrome

# 在 chrome 上 以 HMTL 渲染器 在 profile 模式下渲染運行
flutter run -d chrome --web-renderer html --profile

# 使用 auto 渲染器 構建應用
flutter build web --release

# 使用 CanvasKit 渲染器 構建應用
flutter build web --web-renderer canvaskit --release

# 使用 HTML 渲染器 構建應用
flutter build web --web-renderer html --release
```

不過 flutter web 發佈的 web 檔案偏大 除了遊戲外一般不建議使用

