# [ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/)

ConfigMap 允許將配置檔案與鏡像檔案分離，以使容器化的應用程式具有可移植性。

* 當 ConfigMap 映射到 volumes 時 如果 ConfigMap 更新了 k8s 會在一個 緩存 ttl 時間內/watch傳播延遲 將 volumes 更新 
* 使用 ConfigMap 作爲 subPath 掛接的 volumes 不會收到更新
* 使用 ConfigMap 設置的環境變量 不會收到更新


# 創建 ConfigMap

使用 `kubectl create configmap` 指令 創建 ConfigMap

* --from-literal 指定 key value

	```
	kubectl create configmap my-config --from-literal=key1=config1 --from-literal=key2=config2
	```
	
* --from-file 依據檔案夾下的檔案 或 指定檔案 創建 檔案名作爲key 檔案內容作爲 value

	--from-file 也可以指定 key 名稱 `--from-file=<my-key-name>=<path-to-file>`
	
	```
	#info="game.properties"
	enemies=aliens
	lives=3
	enemies.cheat=true
	enemies.cheat.level=noGoodRotten
	secret.code.passphrase=UUDDLRLRBABAS
	secret.code.allowed=true
	secret.code.lives=30
	```

	```
	#info="ui.properties"
	color.good=purple
	color.bad=yellow
	allow.textmode=true
	how.nice.to.look=fairlyNice
	```

	```
	kubectl create configmap game-config --from-file=configure-pod-container/configmap/
	```

	```
	kubectl create configmap game-config-2 --from-file=configure-pod-container/configmap/game.properties
	```
	
* 查看 configmap 

	```
	kubectl describe configmaps game-config
	```
	
	```
	kubectl get configmaps game-config -o yaml
	```
	
* --from-env-file 從環境檔案創建 configmap

	* Env 檔案每行必須爲 KEY=VALUE 格式
	* 以 # 開頭的行將作爲註釋被忽略
	* 空行會被忽略
	* 引號不會被特殊處理

	```
	kubectl create configmap config-multi-env-files \
					--from-env-file=configure-pod-container/configmap/game-env-file.properties \
					--from-env-file=configure-pod-container/configmap/ui-env-file.properties
	```
	
	> 多次使用 --from-env-file 只有最後一個檔案有效

# kustomization.yaml

自 1.14 開始 kubectl 開始支持 kustomization.yaml，可以在 kustomization.yaml 中編輯 ConfigMap 生成器之後 使用下述指令來更新 ConfigMap

```
kubectl apply -k .
```

> 使用此方法創建的 ConfigMap 會加上一個 內容hash作爲後綴 以保證每次修改內容後都會生成一個新的 ConfigMap

從字面值創建 ConfigMap
```
configMapGenerator:
- name: special-config-2
  literals:
  - special.how=very
  - special.type=charm
```

從 檔案創建

```
configMapGenerator:
- name: game-config-4
  files:
  - configure-pod-container/configmap/kubectl/game.properties
```

從檔案創建時也可以指定 key
```
configMapGenerator:
- name: game-config-5
  files:
  - game-special-key=configure-pod-container/configmap/kubectl/game.properties
```

# 使用單個 ConfigMap數據定義容器環境變量

1. 創建一個 ConfigMap

	```
	kubectl create configmap special-config --from-literal=special.how=very
	```
	
2. 將 ConfigMap 中定義的 special.how 值分配給 Pod 規範中的 SPECIAL\_LEVEL\_KEY 環境變量

	```
	apiVersion: v1
	kind: Pod
	metadata:
		name: dapi-test-pod
	spec:
		containers:
			- name: test-container
				image: k8s.gcr.io/busybox
				command: [ "/bin/sh", "-c", "env" ]
				env:
					# Define the environment variable
					- name: SPECIAL_LEVEL_KEY
						valueFrom:
							configMapKeyRef:
								# The ConfigMap containing the value you want to assign to SPECIAL_LEVEL_KEY
								name: special-config
								# Specify the key associated with the value
								key: special.how
		restartPolicy: Never
	```
	
# 使用 多個 ConfigMap 數據定義容器環境變量

多次使用 valueFrom 指定不同的 ConfigMap name 即可

```
apiVersion: v1
kind: Pod
metadata:
  name: dapi-test-pod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh", "-c", "env" ]
      env:
        - name: SPECIAL_LEVEL_KEY
          valueFrom:
            configMapKeyRef:
              name: special-config
              key: special.how
        - name: LOG_LEVEL
          valueFrom:
            configMapKeyRef:
              name: env-config
              key: log_level
  restartPolicy: Never
```

# 將 ConfigMap 中的所有鍵值對配置爲容器環境變量

k8s 1.6 開始 支持此功能

1. 創建一個包含多個 key value 的 ConfigMap

	```
	#info="configmap-multikeys.yaml"
	apiVersion: v1
	kind: ConfigMap
	metadata:
		name: special-config
		namespace: default
	data:
		SPECIAL_LEVEL: very
		SPECIAL_TYPE: charm
	```
	
	```
	kubectl create -f configmap-multikeys.yaml
	```
	
1. 使用 envFrom 將 所有 ConfigMap 定義爲容器 環境變量

	```
	apiVersion: v1
	kind: Pod
	metadata:
		name: dapi-test-pod
	spec:
		containers:
			- name: test-container
				image: k8s.gcr.io/busybox
				command: [ "/bin/sh", "-c", "env" ]
				envFrom:
				- configMapRef:
						name: special-config
		restartPolicy: Never
	```
	
# 在 Pod 命令中 使用 ConfigMap 定義的環境變量

可以使用 $(VAR_NAME) 的 k8s 語法 在容器的 command 和 args 部分使用 ConfigMap 定義的 環境變量

```
apiVersion: v1
kind: Pod
metadata:
  name: dapi-test-pod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh", "-c", "echo $(SPECIAL_LEVEL_KEY) $(SPECIAL_TYPE_KEY)" ]
      env:
        - name: SPECIAL_LEVEL_KEY
          valueFrom:
            configMapKeyRef:
              name: special-config
              key: SPECIAL_LEVEL
        - name: SPECIAL_TYPE_KEY
          valueFrom:
            configMapKeyRef:
              name: special-config
              key: SPECIAL_TYPE
  restartPolicy: Never
```

# 將 ConfigMap 數據添加到 volumes 中 

ConfigMap 是 一個 key value 可以 將其 添加到  volumes 中

之後 使用 volumeMounts 可以將 ConfigMap 的所有 key 創建爲檔案 並將 value 作爲檔案內容

```
apiVersion: v1
kind: Pod
metadata:
  name: dapi-test-pod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh", "-c", "ls /etc/config/" ]
      volumeMounts:
      - name: config-volume
        mountPath: /etc/config
  volumes:
    - name: config-volume
      configMap:
        # Provide the name of the ConfigMap containing the files you want
        # to add to the container
        name: special-config
  restartPolicy: Never
```

* volumeMounts 的 name 屬性指定了 要引用的 volumes
* volumeMounts 的 mountPath 屬性指定了 要創建的 檔案目錄

檔案會以 utf8 編碼 存儲  如果要使用其它編碼 可以使用 binaryData

可以使用 path 爲 volumes 指定 要創建的 檔案名稱

```
apiVersion: v1
kind: Pod
metadata:
  name: dapi-test-pod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh","-c","cat /etc/config/keys" ]
      volumeMounts:
      - name: config-volume
        mountPath: /etc/config
  volumes:
    - name: config-volume
      configMap:
        name: special-config
        items:
        - key: SPECIAL_LEVEL
          path: keys
  restartPolicy: Never
```