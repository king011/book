# 類和繼承

使用 class 定義 類 如果 類沒有主體 可以省略 {}

```
package com.example

class Animal

fun main() {
    var a = Animal()
    println("$a")
}
```

## 主構造函數

class 可以有一個可選的主構造函數 和 多個可選的次構造函數 

主構造函數 跟在類名和 類型參數之後

主構造的參數 可以 直接在次構造 或 屬性初始化時使用

```
package com.example

class Animal constructor(str: String) {
    private val name = str

    fun speak() {
        println("$name speak")
    }
}

fun main() {
    var cat = Animal("cat")
    cat.speak()
}
```

如果主構造函數沒有註解或者可見性修飾 可以省略 關鍵字 constructor 並且 

構造中 可以直接定義 class的 屬性 使用 var 或 val

```
package com.example

class Animal(private val name: String) {

    fun speak() {
        println("$name speak")
    }
}

fun main() {
    var cat = Animal("cat")
    cat.speak()
}
```

如果存在 修飾 或 註解 則 必須使用 constructor 關鍵字
```
class Customer public @Inject constructor(name: String) { /*……*/ }
```
## init

主構造函數不能包含任何 代碼 初始化 代碼 需要放到 init 塊中 

初始化時 將按照 出現順序執行 和 屬性初始化 交織在一起

```
package com.example

class Animal(private val name: String) {

    init {
        println(1)
    }

    private val lv = {
        println(2)
        2
    }()

    init {
        println(3)
    }

    fun speak() {
        println("$name $lv speak")
    }
}

fun main() {
    var cat = Animal("cat")
    cat.speak()
}
```

## 次構造函數
在 類體中使用 constructor 定義次構造函數 

如果一個類有一個主構造函數 每個次構造函數 都需要 直接或間接委託給主構造函數 使用 this 即可委託給主構造函數

```
package com.example

class Animal(private val name: String) {
    private var lv: Int = 0

    constructor(lv: Int, name: String) : this(name) {
        this.lv = lv
    }

    fun speak() {
        println("$name $lv speak")
    }
}

fun main() {
    var cat = Animal(10,"cat")
    cat.speak()
}
```

如果一個非抽象類 沒有聲任何構造函數，編譯器會爲它會有一個不帶參數的 public 主構造函數。如果不希望它有 public 的構造函數 可以顯示聲明 private

```
class Animal private constructor()
```

# 創建實例

kotlin 沒有 new 關鍵字 創建一個新實例 直接以函數調用的寫法調用 構造函數即可

```
val invoice = Invoice()

val customer = Customer("Joe Smith")
```
# 繼承

所有class 都 派生自 Any 並且 Any 提供了三個 方法

* equals(other: Any?): Boolean
* hashCode(): Int
* toString(): String

因此所有 類都定義了這些方法

```
package com.example

fun main() {
    println(null.toString())
}
```

* 默認 class 是最終狀態(final) 不允許派生 如果要允許被派生需要使用 open 定義
* 使用 abstract 定義 抽象類和 抽象方法
* 使用 super 調用父類實現
* 使用 override 覆蓋父類實現
* **函數** 和 **屬性** 也允許覆蓋 (同樣默認是 final 要允許被 override 需要標註 open )
* 子類可以使用 val 屬性覆蓋父類的 var 屬性 但反之則不行

```
package com.example

open abstract class Animal(val name: String) {
    abstract var lv: Int
    abstract fun speak()
}

open class Cat(name: String) : Animal(name) {
    // final 標記 不允許 子類 覆蓋
    final override var lv: Int = 10
    override fun speak() {
        println("i'm $name,a cat,lv is $lv")
    }
}

class WhiteCat : Cat {
    constructor() : super("white")

    override fun speak() {
        super.speak()
        println("喵~")
    }
}

fun main() {
    val cat = WhiteCat()
    cat.speak()
}
```

## 調用超類

使用 super 關鍵字調用超類

```
open class Rectangle {
    open fun draw() { println("Drawing a rectangle") }
    val borderColor: String get() = "black"
}

class FilledRectangle : Rectangle() {
    override fun draw() {
        super.draw()
        println("Filling the rectangle")
    }

    val fillColor: String get() = super.borderColor
}
```

使用 super@外部類名 可以在內部類中調用外部類的超類

```
class FilledRectangle: Rectangle() {
    override fun draw() { 
        val filler = Filler()
        filler.drawAndFill()
    }

    inner class Filler {
        fun fill() { println("Filling") }
        fun drawAndFill() {
            super@FilledRectangle.draw() // 調用 Rectangle 的 draw() 實現
            fill()
            println("Drawn a filled rectangle with color ${super@FilledRectangle.borderColor}") // 使用 Rectangle 所實現的 borderColor 的 get()
        }
    }
}
```

## 覆蓋規則

在 kotlin 中一個類如果 繼承類多個相同的屬性，此時如果多個超類中有相同的函數 必須覆蓋，使用 super&lt;Base&gt; 來訪問具體的超類

```
open class Rectangle {
    open fun draw() { /* …… */ }
}

interface Polygon {
    fun draw() { /* …… */ } // 接口成員默認就是"open" 的
}

class Square() : Rectangle(), Polygon {
    // 編譯器要求覆蓋 draw()：
    override fun draw() {
        super<Rectangle>.draw() // 調用 Rectangle.draw()
        super<Polygon>.draw() // 調用 Polygon.draw()
    }
}
```
# 屬性字段

完整的 屬性字段 語法如下
```
var <propertyName>[: <PropertyType>] [= <property_initializer>]
    [<getter>]
    [<setter>]
```

如果 只修改 屬性 修飾符 不改變默認實現 可以 用空 get set

```
var setterVisibility: String = "abc"
    private set // 私有 set

var setterWithAnnotation: Any? = null
    @Inject set // Inject 註解 set
```

## 幕後字段

當一個屬性需要 一個幕後字段時 kotlin 將自動 提供 使用 field 訪問它

```
var counter = 0 // 這個初始器的值 直接爲幕後字段賦值
    set(value) {
        if (value >= 0) field = value
    }
```

## 幕後屬性

如果不想使用 默認的 幕後字段 也可以使用 自定義的 幕後屬性
```
private var _table: Map<String, Int>? = null
public val table: Map<String, Int>
    get() {
        if (_table == null) {
            _table = HashMap() 
        }
        return _table ?: throw AssertionError("Set to null by another thread")
    }
```
## 延遲初始化

kotlin 要求不爲空的變量 需要在定義時初始化 此時可以加上 lateinit 告訴編譯器 變量將延遲初始化

```
public class MyTest {
    lateinit var subject: TestSubject

    @SetUp fun setup() {
        subject = TestSubject()
    }

    @Test fun test() {
        subject.method()
    }
}
```