# [min-width](https://www.primefaces.org/primeflex/minwidth)

min-width 指定了元素的最小寬度

| Class | Properties |
| -------- | -------- |
| min-w-0     | min-width: 0px;     |
| min-w-full     | min-width: 100%;     |
| min-w-screen     | min-width: 100vw;     |
| min-w-min     | min-width: min-content;     |
| min-w-max     | min-width: max-content;     |

![](assets/min.png)

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="border-round bg-blue-100 w-12rem p-3 m-3">
            <div class="min-w-full border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">min-w-full</div>
        </div>
    </div>
</div>
```

# [max-width](https://www.primefaces.org/primeflex/maxwidth)

max-width 指定來元素的最大寬度

![](assets/max.png)

| Class | Properties |
| -------- | -------- |
| max-w-0     | max-width: 0px;     |
| max-w-full     | max-width: 100%;     |
| max-w-screen     | max-width: 100vw;     |
| max-w-min     | max-width: min-content;     |
| max-w-max     | max-width: max-content;     |
| max-w-1rem     | max-width: 1rem;     |
| max-w-2rem     | max-width: 2rem;     |
| ...    | ...     |
| max-w-1rem     | max-width: 1rem;     |
| max-w-30rem     | max-width: 30rem;     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="border-round bg-blue-100 w-12rem p-3 m-3">
            <div class="max-w-full border-round bg-blue-500 text-white font-bold p-3 flex align-items-center justify-content-center">max-w-full</div>
        </div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同