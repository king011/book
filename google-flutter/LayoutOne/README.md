# 單一孩子佈局

flutter 提供了 多個 佈局 widget 容器 來對其擁有的單 子 widget 進行 佈局

# Center 居中

[Center](https://docs.flutter.io/flutter/widgets/Center-class.html) 將 子 widget 佈局到自己的 中心 



| 可選參數 | 描述 |
| -------- | -------- |
| Widget child     | 子 widget     |
| double widthFactor    | 寬度因子    |
| double heightFactor     | 高度因子     |

如果 child 沒有 限定 寬度 和 高度 Center 會儘量 使用 child 達到最大 且此時如果 widthFactor/heightFactor 非 null 還會 使用 最大 寬/高 \* widthFactor/heightFactor 作爲最終 寬高

![](assets/Center.png)
![](assets/layout_center.png)

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: Center(
        child: Container(
          color: Color.fromARGB(255, 255, 0, 0),
          width: 200,
          height: 200,
          child: Center(
            child: Container(
              color: Color.fromARGB(255, 0, 255, 0),
              width: 150,
              height: 150,
              child: Center(
                child: Container(
                  color: Color.fromARGB(255, 0, 0, 255),
                  width: 100,
                  height: 100,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
```

# Align 對齊

[Align](https://docs.flutter.io/flutter/widgets/Align-class.html) 類似 Center 將 子元素 相對自己進行 對齊

| 可選參數 | 描述 |
| -------- | -------- |
| Widget child     | 子 widget     |
| AlignmentGeometry alignment     | 對齊規則    |
| double widthFactor    | 寬度因子    |
| double heightFactor     | 高度因子     |

![](assets/Align.png)
![](assets/layout_align.png)

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).appName),
        ),
        body: Center(
          child: Container(
            height: 100.0,
            width: 100.0,
            color: Colors.yellow,
            child: Align(
              alignment: FractionalOffset(0.2, 0.6),
              child: Container(
                height: 40.0,
                width: 40.0,
                color: Colors.red,
              ),
            ),
          ),
        ));
  }
}
```

# Padding 內邊距

[Padding](https://docs.flutter.io/flutter/widgets/Padding-class.html) 會在和子元素件 填充一個 內部邊距 以和子元素 保持間隔

| 參數 | 描述 |
| -------- | -------- |
| Widget child     | 子 widget     |
| @required EdgeInsetsGeometry padding    | 填充規則    |

![](assets/Padding.png)
![](assets/layout_padding.png)

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const double left = 8;
    const double top = 16;
    const double right = 24;
    const double bottom = 32;
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: Padding(
        // padding: EdgeInsets.only(), // padding 0
        padding: EdgeInsets.all(8),
        child: Container(
          color: Color.fromARGB(255, 255, 0, 0),
          child: Padding(
            //padding: EdgeInsets.fromLTRB(left, top, right, bottom),
            padding: EdgeInsets.only(
              left: left,
              top: top,
              right: right,
              bottom: bottom,
            ),
            child: Container(
              color: Color.fromARGB(255, 0, 255, 0),
            ),
          ),
        ),
      ),
    );
  }
}
```

# Container 容器

[Container](https://docs.flutter.io/flutter/widgets/Container-class.html) 是一個 擁有 繪製 定位 調整大小的 widget

![](assets/layout_container.png)


# Offstage 顯示 隱藏

[Offstage](https://docs.flutter.io/flutter/widgets/Offstage-class.html) 允許 動態改變 子元素 的顯示 隱藏

| 參數 | 描述 |
| -------- | -------- |
| Widget child     | 子 widget     |
| bool offstage    | 是否隱藏    |

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: Center(
        child: Offstage(
          offstage: false, // 爲 true 隱藏
          child: Container(
            color: Colors.red,
            width: 200,
            height: 200,
          ),
        ),
      ),
    );
  }
}
```

# SizedBox 限制大小

[SizedBox](https://docs.flutter.io/flutter/widgets/SizedBox-class.html) 可以強制限制 子節點的大小

```
SizedBox(
  width: 200.0,
  height: 300.0,
  child: const Card(child: Text('Hello World!')),
)
```

# Transform 變換

[Transform](https://docs.flutter.io/flutter/widgets/Transform-class.html) 可以將子節點 進行 旋轉 縮放 平移 等變換

![](assets/Transform.png)

```
import 'package:flutter/material.dart';
import 'dart:math' as math;

class ExamplePage extends StatefulWidget {
  ExamplePage({Key key}) : super(key: key);

  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  @override
  Widget build(BuildContext context) {
    // 旋轉
    final rotate = List<Widget>();
    for (var i = 0; i < 8; i++) {
      rotate.add(
        Transform.rotate(
          angle: math.pi / 4 * i,
          child: Icon(
            Icons.android,
            color: Theme.of(context).primaryColor,
            size: 64,
          ),
        ),
      );
    }
    // 縮放
    final scale = List<Widget>();
    for (var i = 0; i < 5; i++) {
      scale.add(
        Transform.scale(
          scale: 0.2 * (i + 1),
          child: Icon(
            Icons.android,
            color: Theme.of(context).primaryColor,
            size: 64,
          ),
        ),
      );
    }
    // 平移
    final translate = List<Widget>();
    for (double i = 0; i < 7; i++) {
      double v = 0;
      if (i < 3) {
        v = 3 - i;
      } else if (i > 3) {
        v = i - 3;
      }
      translate.add(
        Transform.translate(
          offset: Offset(0, 10.0 * v),
          child: Icon(
            Icons.android,
            color: Theme.of(context).primaryColor,
            size: 64,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Transform"),
      ),
      body: Center(
        child: IntrinsicHeight(
          child: Column(
            children: <Widget>[
              Wrap(
                children: rotate,
              ),
              Wrap(
                children: scale,
              ),
              Wrap(
                children: translate,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }
}
```

# ConstrainedBox 最大最小

ConstrainedBox 約束了 子節點的 最大/最小尺寸
```
ConstrainedBox(
	constraints: BoxConstraints(minHeight: 36),
	child: IntrinsicHeight(
		child: Center(
			child: CircularProgressIndicator(),
		),
	),
);
```

# IntrinsicHeight/IntrinsicWidth 原始尺寸

IntrinsicHeight/IntrinsicWidth 將子節點 調整到 其原本的 高度/寬度