# [Button](https://primeng.org/button)

```
import {ButtonModule} from 'primeng/button';
```

Button 是帶有圖標和主題的標準按鈕的擴展

你有兩種方式定義它
1. 使用指令 **pButton**
2. 使用 **p-button** 標籤

建議使用方式2因爲它打的字更少

```
<button pButton type="button" label="Click" ></button>
<p-button label="Click" ></p-button>
```

按鈕的文本是有 **label** 屬性定義的(不要直接寫在 標籤內 而是使用 label 因爲直接書寫的可能會錯過 PrimeNG 定義的一些 css 效果)

![](assets/primeng.org_button.png)

## Icons

可以爲 button 指定一個圖標，它默認在文本的左側，也可以沒有文本只要不定義 label 即可

```
<button pButton type="button" icon="pi pi-check" iconPos="left"></button>
<p-button label="Click" icon="pi pi-check" iconPos="left"></p-button>
```

## Events

使用 click/onClick 來指定按鈕點擊事件

```
<button pButton type="button" label="Click" (click)="handleClick($event)"></button>
<p-button label="Click" (onClick)="handleClick($event)"></p-button>
```

## Severity

可以使用不同等級來讓按鈕顯示不同的顏色

* .p-button-secondary
* .p-button-success
* .p-button-info
* .p-button-warning
* .p-button-help
* .p-button-danger

```
<button pButton type="button" class="p-button-info"></button>
<button pButton type="button" label="Primary"></button>
<button pButton type="button" label="Secondary" class="p-button-secondary"></button>
<button pButton type="button" label="Success" class="p-button-success"></button>
<button pButton type="button" label="Info" class="p-button-info"></button>
<button pButton type="button" label="Warning" class="p-button-warning"></button>
<button pButton type="button" label="Help" class="p-button-help"></button>
<button pButton type="button" label="Danger" class="p-button-danger"></button>
```

## Text Buttons

文本按鈕具有透明的背景和邊框，指定 class  **p-button-text** 即可。

```
<button pButton type="button" label="Submit" class="p-button-text"></button>
<button pButton type="button" icon="pi pi-check" class="p-button-text"></button>
<button pButton type="button" label="Cancel" icon="pi pi-times" class="p-button-text"></button>
<button pButton type="button" label="Search" icon="pi pi-search" iconPos="right" class="p-button-text"></button>
```

## Raised and Rounded Buttons

使用 class **p-button-raised** 和 **p-button-rounded** 指定突起和圓形的按鈕

```
<button pButton type="button" class="p-button-raised p-button-rounded"></button>
```

## Outlined Buttons
使用 class **p-button-outlined** 指定另外一種輪廓的按鈕

```
<button pButton type="button" label="Primary" class="p-button-outlined"></button>
```

## Link Buttons

使用 class **p-button-link** 將按鈕作爲 link

```
<button pButton type="button" label="Link" class="p-button-link"></button>
```

## Badges

badge 屬性可以爲按鈕指定一個小的狀態徽章

```
<button pButton type="button" label="Emails" badge="8"></button>
<button pButton type="button" label="Messages" icon="pi pi-users" class="p-button-warning" badge="8" badgeClass="p-badge-info"></button>
```

## ButtonSet

使用 class **p-buttonset** 將按鈕並排分組到同一組中
```
<span class="p-buttonset">
    <button pButton type="button" label="Save" icon="pi pi-check"></button>
    <button pButton type="button" label="Delete" icon="pi pi-trash"></button>
    <button pButton type="button" label="Cancel" icon="pi pi-times"></button>
</span>
```

## Sizes

使用 class **p-button-sm** **p-button** **p-button-lg** 來指定按鈕預設的不同大小

```
<button pButton type="button" label="Small" icon="pi pi-check" class="p-button-sm"></button>
<button pButton type="button" label="Normal" icon="pi pi-check" class="p-button"></button>
<button pButton type="button" label="Large" icon="pi pi-check" class="p-button-lg"></button>
```

## Loading State

設置 loading 屬性爲 true 按鈕將綁定一個 加載圖標
```
<button pButton type="button" label="Small" icon="pi pi-check" [loading]="isLoading" ></button>
<p-button label="Search" icon="pi pi-search" [loading]="isLoading"></p-button>
```

# Properties of pButton

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| label     | string     | null     | 按鈕文本     |
| icon     | string     | null     | 按鈕圖標     |
| iconPos     | string     | left     | 圖標在按鈕中的位置: left right     |
| loading     | boolean     | false     | 按鈕是否處於加載中     |
| loadingIcon     | string     | pi pi-spinner pi-spin     | 按鈕處於加載中顯示的圖標     |

# Properties of p-button


| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| type     | string     | null     | 按鈕類型     |
| label     | string     | null     | 按鈕文本     |
| icon     | string     | null     | 按鈕圖標     |
| iconPos     | string     | left     | 圖標在按鈕中的位置: left right     |
| badge     | string     | null     | 按鈕顯示的一個小徽章     |
| badgeClass     | string     | null     | 徽章 的 css class     |
| loading     | boolean     | false     | 按鈕是否處於加載中     |
| loadingIcon     | string     | pi pi-spinner pi-spin     | 按鈕處於加載中顯示的圖標     |
| disabled     | boolean     | false     | 如果爲 true 則禁用按鈕     |
| style     | string     | null     | 組件內聯的 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| onClick     | event     | null     | 按鍵點擊回調     |
| onFocus     | event     | null     | 按鈕獲取焦點回調     |
| onBlur     | event     | null     | 按鈕失去焦點回調     |
| ariaLabel     | string     | null     | 按鈕的 aria label     |

# Templates

| Name | Parameters |
| -------- | -------- |
| content     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-button     | Button element     |
| p-button-icon     | Icon element     |
| p-button-label     | Label element of the button     |




