# 清理磁盤

docker 檔案存儲在 **/var/lib/docker** 檔案夾下

使用 `docker system df` 指令可以查看 docker 佔用磁盤情況

```
root@king-HP-Company:/var/lib# docker system df 
TYPE            TOTAL     ACTIVE    SIZE      RECLAIMABLE
Images          3         3         4.756GB   610MB (12%)
Containers      3         3         533.2MB   0B (0%)
Local Volumes   80        0         1.028GB   1.028GB (100%)
Build Cache     0         0         0B        0B
```

在 磁盤佔用嚴重時可以使用 `docker system prune` 指令清理磁盤，這會刪除 關閉的容器 無用的數據 無用的網路 無tag的鏡像

```
docker system prune
```

使用 -a 參數會刪除所有未使用的 容器 網路 鏡像

```
docker system prune -a
```
