# 使用強 Diffie-Hellman
1. 生成 2048 的key

  openssl dhparam -out dhparams.pem 2048

1. 如果需要 生成 更安全 4096 的key

  openssl dhparam -out dhparams.pem 4096

1. 為安全 修改 讀寫權限

  sudo chmod 400 dhparams.pem

1. 在 nginx中 添加設置 

  ssl_dhparam /etc/nginx/ssl/dhparams.pem;

# ssl 協議

一些 tls 版本和加密算法並不安全，推薦禁用掉

```
##
# SSL Settings
##

ssl_protocols TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
ssl_prefer_server_ciphers on;
ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:!SHA1:!SHA256:!SHA384:!DSS:!aNULL;
ssl_stapling on;
ssl_stapling_verify on;
```