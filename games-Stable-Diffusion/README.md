# Stable Diffusion

Stable Diffusion 是 2022年發佈的開源(CreativeML Open RAIL-M)深度文字圖像生成模型

* 源碼 [https://github.com/Stability-AI/stablediffusion](https://github.com/Stability-AI/stablediffusion)
* 官網 [https://stability.ai/](https://stability.ai/)