# peek

peek 是一個 屏幕 錄製 工具  通常用來 將屏幕操作錄製爲 gif

```sh
#info=false
sudo add-apt-repository ppa:peek-developers/stable
sudo apt update
sudo apt install peek
```