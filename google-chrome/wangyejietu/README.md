# 網頁截圖
chrome 開發者工具中 Capture 工具 可以將整個網頁 或指定 元素 渲染到 png圖片 通過這個可以實現 截圖

1. 按F12打開 開發者 工具
2. ctrl+shift+p 輸入截圖指令  
    * 截取這個網頁 Capture full size screenshot
    * 只截取 選中的元素 Capture node screenshot
    * 只截取 熒幕顯示的 內容 Capture screenshot