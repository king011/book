# 時間欄目

cron 使用 Parser 解析時間規則其默認值爲

```
var standardParser = NewParser(
	Minute | Hour | Dom | Month | Dow | Descriptor,
)
```

你可以使用 WithParser 在 cron.New 時指定要使用的時間欄目

此外提供了一個 WithSeconds 的語法糖函數來支持秒的欄目

```
func WithSeconds() Option {
	return WithParser(NewParser(
		Second | Minute | Hour | Dom | Month | Dow | Descriptor,
	))
}
```