# priority_queue

std::priority_queue 是一個 優先隊列

```
#include <queue>
```

```
template<typename _Tp, typename _Sequence = vector<_Tp>,
	   typename _Compare  = less<typename _Sequence::value_type> >
    class priority_queue;
```

```
#include <iostream>
#include <queue>
#include <ctime>
#include <cmath>
int main(int argc, char *argv[])
{
    std::time_t sd = std::time(NULL);
    std::size_t count = 10;
    // 最大堆
    {
        srand(sd);
        std::priority_queue<int> queue;
        for (size_t i = 0; i < count; i++)
        {
            queue.push(rand() % 100);
        }
        std::cout << "maximum heap: ";
        while (!queue.empty())
        {
            std::cout << queue.top() << " ";
            queue.pop();
        }
        std::cout << std::endl;
    }
    // 最小堆
    {
        srand(sd);
        typedef std::greater<int> compare_t;
        std::priority_queue<int, std::vector<int>, compare_t> queue;
        for (size_t i = 0; i < count; i++)
        {
            queue.push(rand() % 100);
        }
        std::cout << "minimum heap: ";
        while (!queue.empty())
        {
            std::cout << queue.top() << " ";
            queue.pop();
        }
        std::cout << std::endl;
    }
    return 0;
}
```