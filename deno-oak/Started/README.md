# Application Middleware Context

class Application 用於管理 http 服務器，通常會調用 use 方法來註冊中間件，在一切就緒後調用 listen 來監聽端口並開始 http 服務

```
import { Application } from "../deps/x/oak/mod.ts";
const app = new Application();
app.addEventListener("listen", (evt) => {
  console.log(
    `${evt.secure ? "https" : "http"} listen on ${evt.hostname}:${evt.port}`,
  );
});
// 註冊中間件
app.use((ctx) => {
  ctx.response.body = "Hello World!";
});
// 監聽並開始工作
await app.listen({
  port: 9000,
  //   secure: true,
  //   cert: Deno.readTextFileSync(
  //     "fullchain.pem",
  //   ),
  //   key: Deno.readTextFileSync(
  //     "privkey.pem",
  //   ),
});
```

## Middleware

Middleware 作爲 stack 進行處理，其中每個 Middleware 函數都可以控制響應流程。當 Middleware 函數被調用時，它會傳入一個 Context(包含了上下文) 以及下一個 Middleware 方法

```
import { Application } from "../deps/x/oak/mod.ts";
const app = new Application();
app.addEventListener("listen", (evt) => {
  console.log(
    `${evt.secure ? "https" : "http"} listen on ${evt.hostname}:${evt.port}`,
  );
});
// Logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.headers.get("X-Response-Time");
  console.log(`${ctx.request.method} ${ctx.request.url} - ${rt}`);
});

// Timing
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.response.headers.set("X-Response-Time", `${ms}ms`);
});

// Hello World!
app.use((ctx) => {
  ctx.response.body = "Hello World!";
});

// 監聽並開始工作
await app.listen({
  port: 9000,
});
```
## handle()

你可以使用 Application 提供的 handle 來處理直接來自 Deno.Conn 的 http 請求。

```
(
	request: Request,
	conn?: Deno.Conn,
	secure?: boolean,
): Promise<Response | undefined>;
```

```
import { Application } from "../deps/x/oak/mod.ts";
const app = new Application();

app.use((ctx) => {
  ctx.response.body = "Hello World!";
});

const listener = Deno.listen({ hostname: "localhost", port: 9000 });

for await (const conn of listener) {
  (async (conn) => {
    const requests = Deno.serveHttp(conn);
    for await (const { request, respondWith } of requests) {
      const response = await app.handle(request, conn);
      if (response) {
        respondWith(response);
      }
    }
  })(conn);
}
```

## Context

oak 會爲中間件傳入 class Context 實例作爲第一個參數，Context 包含下述一些常用的屬性：

* **app** 這是關聯的 Application 的實例
* **cookies** 可用於讀取寫入 cookie，是 class SecureCookieMap 的實例
* **request** 包含了請求信息，是 class Request 的實例
* **respond** 一個 boolean 值，默認爲 true，如果爲 true 會將 response 發送到客戶端。但某些方法例如 upgrade() sendEvents() 等會將它設置爲 false
* **response** 包含了響應信息用於發回給請求者，是 class Response 的實例
* **socket** 如果已經升級到 websocket 則返回 WebSocket 實例，否則返回 undefined
* **state** Application 狀態記錄，可以在創建 Application 時指定供各個 Middleware 間共享數據

Context 還包含了如下一些常用的方法:

* **assert(condition: any,errorStatus: ErrorStatus = 500,message?: string)** 做出一個斷言，如果斷言不爲 true 則拋出 HTTPError 異常
* **send(options: ContextSendOptions)** 將檔案串流傳輸到客戶端
* **sendEvents(options?: ServerSentEventTargetOptions)** 將連接轉爲 串流事件(http2)
* **throw(errorStatus: ErrorStatus,message?: string): never** 拋出 HTTPError 異常
* **upgrade(options?: UpgradeWebSocketOptions): WebSocket** 嘗試間請求轉爲 WebSocket

# Cookies

Context 的 cookies 屬性允許讀寫 cookie，如果在 Application 中設置了 keys，他會爲 cookie 進行加密保護。但是它使用 web api 來簽名與驗證 cookie 所以它是 async api 提供

* **get(key: string, options?: CookieGetOptions): Promise&lt;string | undefined&gt;**
* **set(key: string, value: string, options?: CookieSetDeleteOptions): Promise&lt;void&gt;**

# Request

Context 的 request 屬性包含了請求信息。它包含如下常用屬性：
* **hasBody** 如果請求可能有正文，則設置爲 true，否則爲 false。這是不可靠的 API，在 http2 中由於支持串流特性，在許多情況下無法確定請求是否具有正文，確定是否有正文的唯一可靠方法是嘗試讀取正文
* **headers** 包含了請求頭
* **method** 請求方法
* **originalRequest** 原始的請求，通常應該避免使用它
* **secure** 如果請求來自加密的 tls 則返回 true
* **url** 請求的完整 URL

同樣，它提供了一些常用方法：
* **accepts(...types: string[])** 協商請求支持的返回內容類型
* **acceptsEncodings(...encodings: string[])** 協商請求支持的返回內容編碼
* **acceptsLanguages(...languages: string[])** 協商請求支持的返回內容語言
* **body(options?: BodyOptions)** 返回請求正文的表示形式
body 返回值有兩個屬性 type 和 value。type 可能值如下：


| type | value |
| -------- | -------- |
| "bytes"     | Promise&lt;Uint8Array&gt;     |
| "form"     | Promise&lt;URLSearchParams&gt;     |
| "form-data"     | FormDataReader     |
| "json"     | Promise&lt;unknown&gt;     |
| "reader"     | Deno.Reader     |
| "stream"     | ReadableStream&lt;Uint8Array&gt;     |
| "text"     | Promise&lt;string&gt;     |
| "undefined"     | undefined     |

如果沒有 body 則 type 爲 "undefined"。如果請求的內容類型未被識別，則 type 爲 "bytes"

你可以顯示指定 type 格式。例如使用 reader 來獲取 Deno.Reader:

```
import { readAll } from "https://deno.land/x/std/io/util.ts";

app.use(async (ctx) => {
  const result = ctx.request.body({ type: "reader" });
  result.type; // "reader"
  await readAll(result.value); // a "raw" Uint8Array of the body
});
```

