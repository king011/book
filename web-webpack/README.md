# webpack

webpack 是一個開源的(MIT) 前端打包工具

```
npm install --global webpack webpack-cli
```

* 官網 [https://webpack.js.org/](https://webpack.js.org/)
* 源碼 [https://github.com/webpack/webpack](https://github.com/webpack/webpack)

