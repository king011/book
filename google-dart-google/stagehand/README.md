# stagehand

stagehand 提供了 dart 項目的 創建

[https://pub.dartlang.org/packages/stagehand#-readme-tab-](https://pub.dartlang.org/packages/stagehand#-readme-tab-)

# 安裝

安裝與更新 都執行如下指令 即可

```
#info=false
pub global activate stagehand
```

# 常用指令

```sh
# 創建一個 完整的 命令行 程式
stagehand console-full

# 將當前 檔案夾 創建爲一個 標準的 dart 包 或 命令行程式
stagehand package-simple

# 創建使用 shelf 構建的 服務器
stagehand server-shelf 

# 創建 使用 angular  構建的 網頁 前端
stagehand web-angular

# 創建 僅使用 dart 核心庫的 網頁前端
stagehand web-simple 

# 創建 2d 遊戲項目
stagehand web-stagexl
```

# 命令行程式

對於 命令行 程式 需要 在 **pubspec.yaml** 中 添加 executables

這樣 pub 才會爲 可執行檔案 創建 執行腳本到 bin 目錄

```
# Add the bin/generate.dart script to the scripts pub installs.
executables:
  generate:
```