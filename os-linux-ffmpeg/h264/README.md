# [h.264](https://trac.ffmpeg.org/wiki/Encode/H.264)

此文檔記錄了 ffmpeg 使用 x264 編碼器。它假設已經使用 --enable-libx264 編譯了 ffmpeg

```
ffmpeg -i input -c:v libx264 -preset slow -crf 17 -c:a copy output.mp4
```

```
ffmpeg -i input -c:v libx264 -preset slow -crf 17 -vf format=yuv420p -c:a copy output.mp4
```

> h265 大部分使用 10bit 顏色，設置 **-vf format=yuv420p** 轉換爲 8bit 否則一定2設備可能不支持在瀏覽器中播放
# Constant Rate Factor (CRF)

如果想保持最佳質量並且不太關心檔案大小，應該使用此速率控制模式。這是大多數用途的推薦模式

此方法允許編碼器在輸出檔案大小不太重要時嘗試爲整個檔案實現一定質量的輸出。通過爲每一幀調整量化器，它可以獲得保持所要求的質量水平所需要的比特率。缺點是你無法告訴它獲取特定的檔案大小或特定大小比特率，這意味着不建議將此方法用於編碼視頻以進行流式傳輸

## 選擇 CRF 值

1. 使用 **-crf number**  設置 CRF 值，其中 0 是無損的(僅適用與 8 bit，對於 10 bit 使用 **-qp 0**)
2. 取值範圍是 \[0,51\]，0是無損的，23是默認值，51是最差質量。主觀上合理範圍是\[17,28\]。但 17 和 18 在人類視覺上幾乎是無損的，但它看起來應該和輸入幾乎相同，但它在技術上不是無損的
3. 該範圍是指數的，因此 CRF+6會導致大約一半的比特率/檔案大小，-6 會導致大約兩倍比特率

# 選擇預設和調整

可以使用 **-preset** 選擇預設的編碼速度與壓縮比。較慢的預設將提供更好的壓縮(壓縮是每個檔案大小的質量)。這意味着，例如，如果以特定檔案大小或恆定比特率爲目標，你將使用較慢的預設以獲得更好的質量。同樣，對於恆定質量的編碼，你只需要選擇較慢的預設即可節省比特率。

使用你有耐心的最慢預設。按速度降序排列的可選預設是：

1. ultrafast
2. superfast
3. veryfast
4. faster
5. fast
6. medium – 默認值
7. slow
8. slower
9. veryslow
10. placebo – 忽略它，因爲它沒有用 (see [FAQ](https://trac.ffmpeg.org/wiki/Encode/H.264#FAQ))

可以使用 **-tune** 來調整設置，目前包括：

* film – use for high quality movie content; lowers deblocking
* animation – good for cartoons; uses higher deblocking and more reference frames
* grain – preserves the grain structure in old, grainy film material
* stillimage – good for slideshow-like content
* fastdecode – allows faster decoding by disabling certain filters
* zerolatency – good for fast encoding and low-latency streaming
* psnr – ignore this as it is only used for codec development
* ssim – ignore this as it is only used for codec development

如果你不確定要使用什麼，忽略 -tune 參數即可