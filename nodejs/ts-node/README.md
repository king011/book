# ts-node

ts-node 提供了一個 typescript 的 node 運行環境 

```bash
npm install -g typescript
npm install -g ts-node
```

```
ts-node [options] [ -e script | script.ts ] [arguments]
```

ts-node 會自動 編譯ts並運行

# --files

從 v7.0.0 開始 ts-node 不會自動 加載 tsconfig.json 等檔案 使用 **--files** 參數 可以要求 使用 **tsconfig.json** 的定義運行 腳本