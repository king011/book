# mustache
[mustache](http://mustache.github.io/mustache.5.html) 是一個 輕量的 模板 語法 dart 中的 [第三方庫](https://pub.dartlang.org/packages/mustache) 實現了 這個模板引擎

```
dependencies:
  mustache: ^1.1.1
```

# 訪問變量

mustache 使用 \{\{name\}\} 來訪問變量

使用 \{\{#name\}\}true\{\{/name\}\} 來創建一個 section  
變量 不 爲空列表/flase/null 時 

使用 \{\{^name\}\}true\{\{/name\}\} 來創建一個 section  
變量 爲空列表/flase/null 時 渲染

使用 {{!xxx}} 設置註釋 (渲染時會被 替換爲 空白)

* 如果 # 的變量是 List 且 不爲空 則會對 每項進行 渲染 forEach
* dart 對 Map class 不支持 forEach
* 儘量使用 class 而不是 Map 因爲 使用 Map 如果沒有設置 key 會拋出異常 而 class 可以包裝 key 一定存在
* dart 的 ^ 不支持 一些 基本型別 比如 int

```
import 'package:mustache/mustache.dart';

class Person {
  final String name;
  final int lv;
  final Map<String, dynamic> keys;
  Person(this.name, this.lv, this.keys);

  /// dart ^ 不支持 int 所以 創建一個 has 方法
  bool get hasLevel => lv != null;
}

main() {
  // 創建模板
  const source = '''
{{what}} is an idea
persons:
  {{#person}}
    name : {{name}} 
    {{#lv}}lv : {{lv}} {{/lv}}{{^hasLevel}}no lv{{/hasLevel}}
    {{#keys}}{{speak}} {{eat}}{{/keys}}
  {{/person}} {{! ignore me }}
''';
  final template = Template(
    source,
  );

  // 傳入 上下文 渲染模板
  final output = template.renderString({
    "what": "cerberus",
    "person": [
      Person("king", null, null),
      Person("kate", 0, {
        "speak": "喵",
        "eat": 1,
      }),
    ],
  });
  print(output);
}
```

# html 轉義

默認 情況下 mustache 會轉義 html 可以使用 **{{{name}}}** 輸出未轉義的 內容

或者 可以 在創建模板時 顯示 要求 不需要 轉義

```
import 'package:mustache/mustache.dart';

main() {
  // 創建模板
  const source = '''
{{{what}}} is an idea

{{#what}}
  {{what}} is an idea
{{/what}}
''';
  final template = Template(
    source,
    //htmlEscapeValues: false, // 關閉 html 轉義
  );

  // 傳入 上下文 渲染模板
  final output = template.renderString({
    "what": "<div>cerberus</div>",
  });
  print(output);
}
```

# strict/lenient mode
dart mustache 庫默認使用 strict mode 的嚴格模式 

* tag 只能是 \[a-zA-Z0-9_.\-\]  否則將 throw TemplateException
* rendering 時 tag 未定義 將 throw TemplateException

可以在創建模式時 顯示 設置 lenient = true 來使用 lenient mode 的寬鬆模式
* tag 可以使用 任意字符
* rendering 時 tag 未定義 將 不渲染任何內容

```
import 'package:mustache/mustache.dart';

main() {
  // 創建模板
  const source = '''
{{{what}}} is an idea
''';
  final template = Template(
    source,
    lenient: true,
  );

  // 傳入 上下文 渲染模板
  final output = template.renderString(null);
  print(output);
}
```
# Lambdas
可以將一個 Object Function(LambdaContext context) 傳入 mustache 此時 mustache會將 未渲染的 文本 傳入 此回調 可以在此 過濾/緩存 渲染

```
import 'package:mustache/mustache.dart';

main() {
  // 創建模板
  const source = '''
{{now}}
{{now}}
''';
  final template = Template(
    source,
    lenient: true,
  );

  DateTime now;
  // 傳入 上下文 渲染模板
  final output = template.renderString({
    "now": (LambdaContext ctx) {
      // 第一次 直接 返回 時間
      if (now == null) {
        now = DateTime.now();
        return now;
      }
      // 後續 返回 緩存時間
      // 並且 使用 新的 source(模擬過濾器)
      return ctx.renderSource("{{now}}\n{{what}} is an idea", value: {
        "now": now,
        "what": "cerberus",
      });
    },
  });
  print(output);
}
```

# Partials

Partials 運行 將一個 模板 嵌入其中 但要注意 不要形成 互相嵌套的 死循環

使用 **{{>name}}** 來嵌入一個 模板

```
import 'package:mustache/mustache.dart';

main() {
  // 創建 一個 用於嵌入的 模板
  final idea = Template('{{what}} is an idea', name: 'idea');

  // 創建 主模板
  final template = Template(
    '''
Example Partials:
  {{>idea}}
''',
    partialResolver: (name) {
      if (name == "idea") {
        return idea;
      }
    },
  );
  // 傳入 上下文 渲染模板
  final output = template.renderString({
    "what": "cerberus",
  });
  print(output);
}
```
