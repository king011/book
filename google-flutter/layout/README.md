# Container

Container 是 flutter 中廣泛使用的容器組件

![](assets/Container.jpg)

```
Container({
	super.key,
	this.alignment,
	this.padding,
	this.color,
	this.decoration,
	this.foregroundDecoration,
	double? width,
	double? height,
	BoxConstraints? constraints,
	this.margin,
	this.transform,
	this.transformAlignment,
	this.child,
	this.clipBehavior = Clip.none,
})
```

* **alignment** 指定了 child 如何對齊 Alignment.XXX(topLeft topCenter topRight centerLeft center centerRight bottomLeft bottomCenter bottomRight)
* **padding** 指定了內邊距 EdgeInsets.all(8) EdgeInsets.only(top: 8)
* **color** 指定了背景顏色 Colors.XXX Color.fromARGB()
* **decoration** 裝飾器，設置邊框 圓角 背景圖片等。不能同時設置 color 和 decoration
* **foregroundDecoration** 前景裝飾器
* **width** 寬度
* **height** 高度
* **constraints** 大小範圍約束 const BoxConstraints(minHeight: 0, minWidth: 0, maxWidth: 1128, maxHeight: 128)
* **margin** 外邊距
* **transform** 變換,旋轉 翻轉 變形等... Matrix4.XXX
* **child** 子組件

## Decoration

裝飾器可以支持背景圖 線性漸變 邊框 圓角 陰影 等

* **BoxDecoration** 實現了邊框 圓角 陰影 形狀漸變 背景圖像
* **ShapeDecoration** 實現四邊分別指定顏色和寬度 底部線 矩形邊色 圓形邊色 豎向橢圓 八變形邊色

## SizedBox

SizedBox 相當於一個只有 key width height child 屬性的 Container

通常可以用來限制 child 的大小或者將 SizedBox 作爲組件間距

# 線性佈局

線性佈局就是指沿着 水平/垂直方向排列子組件。

Row/Column 都派生自 Flex 用來實現線性佈局

Row  
![](assets/row.jpg)

Column  
![](assets/col.jpg)

```
Row(
	children: <Widget>[
		Container(
			height: 50,
			width: 100,
			color: Colors.red,
		),
		Container(
			height: 50,
			width: 100,
			color: Colors.green,
		),
		Container(
			height: 50,
			width: 100,
			color: Colors.blue,
		),
	],
);
```

## 主軸交叉軸

![](assets/axis.jpg)

在線性佈局中，與佈局方向一致的軸被稱爲主軸(MainAxis)，與主軸垂直的軸被稱爲交叉軸(MainAxis)

* 可以通過 **mainAxisAlignment** 屬性設置子元素如何在主軸上排列默認爲 MainAxisAlignment.start
* 可以通過 **crossAxisAlignment** 屬性設置子元素如何在交叉軸上排列默認爲 MainAxisAlignment.center

# 彈性佈局 Flex

Row Column 都派生自 Flex

## Flexible

Flexible 作爲 Flex 的子元素會隨着 Flex 的縮放而進行縮放

* **flex** 屬性確定了縮放因子(默認爲 1)
* **fit** 屬性確定，Flexible 如何填充父 Flex 的剩餘空間
	* **FlexFit.tight** 必須填滿剩餘空間
	* **FlexFit.loose** 儘可能填滿剩餘空間，但可以不填滿(這是默認值)
  
```
Row(
	children: <Widget>[
		Container(
			color: Colors.blue,
			height: 50,
			width: 100,
		),
		Flexible(
			flex: 2,
			child: Container(
				color: Colors.red,
				height: 50,
			),
		),
		Flexible(
			child: Container(
				color: Colors.blue,
				height: 50,
			),
		),
	],
)
```

設置 FlexFit.loose 時，Flexible 的子元素能夠確定佔用空間時就不會跟隨父元素縮放

```
Row(
	children: <Widget>[
		Container(
			color: Colors.blue,
			height: 50,
			width: 100,
		),
		Flexible(
			child: Container(
				color: Colors.red,
				height: 50,
				child: const Text(
					'Container',
					style: TextStyle(color: Colors.white),
				),
			),
		),
		Container(
			color: Colors.blue,
			height: 50,
			width: 100,
		),
	],
)
```

## Expanded

Expanded 派生自 Flexible，它只是將 fit 屬性固定設置爲了 FlexFit.loose 

## Spacer

Spacer 實際上是一個默認將 flex 設置爲 1 的 Expanded，並且不允許有子控件

所以 Spacer 實際上是在 Flex 中使用 Expanded 設置 空白間距的語法糖而已

# 流式佈局 Wrap

Wrap 爲子元素進行 水平/垂直 方向的佈局，當空間用完時自動進行換行

```
Wrap(
	direction: Axis.vertical,
	children: List.generate(
		50,
		(index) => Container(
			alignment: Alignment.center,
			color: Color(_random.nextInt(65535 * 65535)).withAlpha(255),
			height: 50,
			width: 50,
			child: Text('$index'),
		),
	),
),
```

* **direction** 指定主軸方向
* **alignment** 類似 mainAxisAlignment，確定了在主軸上如何排列子元素，默認爲 WrapAlignment.start
* **runAlignment** 類似 crossAxisAlignment，確定了在交叉軸上如何排列子元素，默認爲 WrapAlignment.start
* **spacing** 指定在主軸上的子元素間隙
* **runSpacing** 指定在交叉軸上的子元素間隙

# 疊加佈局 Stack

疊加佈局將組件依據順序疊加顯示

```
Stack(
	alignment: Alignment.topRight,
	children: List.generate(
		3,
		(index) {
			final size = (200 - 40 * index).toDouble();
			return Container(
				color: Color(_random.nextInt(65535 * 65535)).withAlpha(255),
				width: size,
				height: size,
			);
		},
	),
)
```
* **fit** 默認爲 StackFit.loose 表示和子元素一樣大，設置爲 StackFit.expand 則與 父元素一樣大
* **clipBehavior** 指定超出 Stack 的部分如何進行剪裁，默認爲 Clip.hardEdge

## IndexedStack

IndexedStack 類似 Stack 但是它接受一個索引值，只有在子元素數組中與索引值相等的元素才會顯示

```
Column(
	children: <Widget>[
		IndexedStack(
			index: _index,
			children: List.generate(
				3,
				(index) {
					final size = (200 - 40 * index).toDouble();
					return Container(
						color: Color(_random.nextInt(65535 * 65535)).withAlpha(255),
						width: size,
						height: size,
					);
				},
			),
		),
		Wrap(
			children: List.generate(
				3,
				(index) => TextButton(
					onPressed: () => setState(() => _index = index),
					child: Text("$index"),
				),
			),
		),
	],
)
```

# 寬高比 AspectRatio

AspectRatio 可以固定子元素的寬高比，這在顯示 圖像 和 視頻 時很有用

```
Container(
	height: 300,
	width: 300,
	color: Colors.blue,
	alignment: Alignment.center,
	child: AspectRatio(
		aspectRatio: 2 / 1,
		child: Container(
			color: Colors.red,
		),
	),
)
```

# 佈局約束

## FittedBox 縮放佈局

FittedBox 會在自己尺寸範圍內縮放並調整 child 的位置，使用 child 適合其尺寸。這和 android 中的 ImageView 類似

* 如果外部有約束，按照外部約束調整自身尺寸，然後縮放調整 child
* 如果外部沒有約束，則跟 child 尺寸一樣

```
const FittedBox({
	super.key,
	this.fit = BoxFit.contain,
	this.alignment = Alignment.center,
	this.clipBehavior = Clip.none,
	super.child,
});
```

**fit** 只是了如何將子元素填充到體內


* **BoxFit.fill** 不按照寬高比填充，內容不會超過容器範圍
* **BoxFit.contain** 按照寬高比填充，內容不會超過容器範圍
* **BoxFit.cover** 按照原始尺寸填充整個容器。內容可能會超過容器範圍
* **BoxFit.fitWidth** 確保顯示完整的寬度。內容可能會超過容器範圍
* **BoxFit.fitHeight** 確保顯示完整的高度。內容可能會超過容器範圍
* **BoxFit.none** 沒有任何填充模式
* **BoxFit.scaleDown** 根據需要，縮小內容

## FractionallySizedBox

FractionallySizedBox 是相等父組件的大小，來對子組件縮放一個縮放因子。當子組件需要按照父組件的百分比確定大小時可以使用 FractionallySizedBox

```
Container(
	color: Colors.blue,
	alignment: Alignment.center,
	child: FractionallySizedBox(
		widthFactor: 0.5,
		heightFactor: 0.5,
		child: Container(
			color: Colors.red,
		),
	),
)
```

## ConstrainedBox

ConstrainedBox 用於指定子元素的尺寸範圍(最大/最小 寬度/高度)

```
ConstrainedBox({
	super.key,
	required this.constraints,
	super.child,
})
```