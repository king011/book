# 狗龍王
![](assets/狗龍.jpg)

## 基本抗性
◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性耐性</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">85 </font></td><td> <font color="#ff0000">85 </font></td><td> <font color="#ff0000">65 </font></td><td> 30 </td><td> 20 </td><td> 20 </td><td> 20 </td><td> 5 </td><td> 100 </td><td> 120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头以外 </td><td> <font color="#ff0000">65 </font></td><td> <font color="#ff0000">63 </font></td><td> <font color="#ff0000">58 </font></td><td> 30 </td><td> 20 </td><td> 20 </td><td> 20 </td><td> 5 </td><td> 0 </td><td> 120
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 90 </td><td> 150 </td><td> 150 </td><td> 120 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 30 (300) </td><td> 20 (230) </td><td> 15 (210) </td><td> 30 (240） </td><td> 50 (350)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/5秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 15秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 75 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回12秒（疲劳20秒） → 2回7秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度3★（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> ○ </td><td> 疲劳时就算是发现状态也会去吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="狗龙的皮">狗龙的皮</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%B0%8F%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小狗龙的鳞">小狗龙的鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E5%B0%8F%E3%80%91" title="龙骨【小】">龙骨【小】</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="狗龙的爪">狗龙的爪</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a> <font color="#008000"><b>15%</b></font><br> </td><td> <a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="狗龙的皮">狗龙的皮</a> <font color="#008000"><b>43%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="狗龙的爪">狗龙的爪</a> <font color="#008000"><b>30%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a> <font color="#008000"><b>19%</b></font><br><a href="/%E5%B0%8F%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小狗龙的鳞">小狗龙的鳞</a> <font color="#008000"><b>8%</b></font> </td><td> <a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="狗龙的上皮">狗龙的上皮</a> <font color="#008000"><b>40%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="狗龙的尖爪">狗龙的尖爪</a> <font color="#008000"><b>28%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a> <font color="#008000"><b>17%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="狗龙的皮">狗龙的皮</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="狗龙的爪">狗龙的爪</a> <font color="#008000"><b>7%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%8F%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小狗龙的鳞">小狗龙的鳞</a> <font color="#008000"><b>25%</b></font><br> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%8F%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小狗龙的鳞">小狗龙的鳞</a> <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>50%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>25%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="狗龙的上皮">狗龙的上皮</a> <font color="#008000"><b>23%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a> <font color="#008000"><b>2%</b></font> </td><td> 濒死逃走时可能掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> 集会浴场★1~2<br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E5%B0%8F%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小狗龙的鳞">小狗龙的鳞</a>*1 <font color="#008000"><b>80%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*1 <font color="#008000"><b>20%</b></font><br> </td><td> <a href="/%E7%8E%8B%E8%80%85%E5%9B%B4%E5%B7%BE" title="王者围巾">王者围巾</a>*1 <font color="#008000"><b>65%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E5%A4%B4" title="狗龙的头">狗龙的头</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E7%8E%8B%E8%80%85%E5%9B%B4%E5%B7%BE" title="王者围巾">王者围巾</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E5%A4%B4" title="狗龙的头">狗龙的头</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>8%</b></font> </td><td> 头部受到一定伤害（胆怯1回），围脖变得破烂
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="狗龙的爪">狗龙的爪</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%A7%8D%E4%B9%8B%E7%89%99" title="鸟龙种之牙">鸟龙种之牙</a>*2 <font color="#008000"><b>22%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="狗龙的皮">狗龙的皮</a>*1 <font color="#008000"><b>15%</b></font><br> </td><td> <a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>45%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="狗龙的爪">狗龙的爪</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="狗龙的皮">狗龙的皮</a>*1 <font color="#008000"><b>18%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E5%A4%B4" title="狗龙的头">狗龙的头</a>*1 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>43%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="狗龙的尖爪">狗龙的尖爪</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="狗龙的上皮">狗龙的上皮</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E7%8B%97%E9%BE%99%E7%9A%84%E5%A4%B4" title="狗龙的头">狗龙的头</a>*1 <font color="#008000"><b>8%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 体力40%以下（下位）<br>体力30%以下（上位）
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2"> 体型
<p>倍率
</p>
</td><td style="text-align: center"> 基础
<p>体力
</p>
</td><td style="text-align: center"> 攻击力
<p>倍率
</p>
</td><td style="text-align: center"> 全体
<p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td style="text-align: center" rowspan="9"> 村长
</td><td style="text-align: center" rowspan="9"> 下位
</td><td style="text-align: center" rowspan="4"> ★2
</td><td> 示范如何狩猎狗龙!
</td><td> 讨伐一头狗龙王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td> 击退溪流的雌狗龙
</td><td> 讨伐3头<a href="/%E9%9B%8C%E7%8B%97%E9%BE%99" title="雌狗龙">雌狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td> 砂原游荡者的首领
</td><td> 讨伐一头狗龙王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_5">
<td> 古代的浪漫？龙骨结晶
</td><td> 讨伐10头<a href="/%E5%B0%8F%E7%8B%97%E9%BE%99" title="小狗龙">小狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_6">
<td style="text-align: center" rowspan="5"> ★3
</td><td> <font color="#f40"> 跳舞的彩鸟 </font>
</td><td>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_7">
<td> <font color="#f40"> 追赶狡诈之星！ </font>
</td><td>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_8">
<td> 闹事的坚甲食草龙
</td><td> 讨伐8头<a href="/%E8%8D%89%E9%A3%9F%E9%BE%99" title="草食龙">草食龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_9">
<td> 狩猎凶兽首领
</td><td> 讨伐两头<a href="/%E9%87%8E%E7%8C%AA%E7%8E%8B" title="野猪王">野猪王</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_10">
<td> 在特产蘑菇上押上未来
</td><td>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 900
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_11">
<td style="text-align: center" rowspan="22"> 集会浴场
</td><td style="text-align: center" rowspan="5"> 初心者
</td><td style="text-align: center" rowspan="3"> ★1
</td><td> 示范如何狩猎狗龙!
</td><td> 讨伐一头狗龙王
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1000
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_12">
<td> 砂原的小狗龙们
</td><td> 讨伐10头小狗龙
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1000
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_13">
<td> 古代的浪漫？龙骨结晶
</td><td> 讨伐10头<a href="/%E5%B0%8F%E7%8B%97%E9%BE%99" title="小狗龙">小狗龙</a>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1000
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_14">
<td style="text-align: center" rowspan="2"> ★2
</td><td> 击退溪流的雌狗龙
</td><td> 讨伐3头<a href="/%E9%9B%8C%E7%8B%97%E9%BE%99" title="雌狗龙">雌狗龙</a>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1200
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_15">
<td> <font color="#f40"> 追赶狡诈之星！ </font>
</td><td>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1200
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_16">
<td style="text-align: center" rowspan="8"> 下位
</td><td style="text-align: center" rowspan="7"> ★3
</td><td> 示范如何狩猎狗龙!
</td><td> 讨伐一头狗龙王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_17">
<td> 溪流小狗龙讨伐作战
</td><td> 讨伐20头<a href="/%E5%B0%8F%E7%8B%97%E9%BE%99" title="小狗龙">小狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_18">
<td> 砂原游荡者的首领
</td><td> 讨伐一头狗龙王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_19">
<td> 闹事的坚甲食草龙
</td><td> 讨伐8头<a href="/%E8%8D%89%E9%A3%9F%E9%BE%99" title="草食龙">草食龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_20">
<td> 砂原里畅游的美味！？
</td><td> 缴纳10个<a href="/%E6%80%AA%E7%89%A9%E7%9A%84%E8%82%9D" title="怪物的肝">怪物的肝</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_21">
<td> <font color="#f40"> 狩猎彩鸟库鲁佩科！ </font>
</td><td>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_22">
<td> 女性当权不也很开心
</td><td> 讨伐8头<a href="/%E9%9B%8C%E7%8B%97%E9%BE%99" title="雌狗龙">雌狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2300
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_23">
<td style="text-align: center"> ★5
</td><td> 孤岛之森
</td><td> 讨伐一头 <a href="/%E7%82%8E%E6%88%88%E9%BE%99" title="炎戈龙">炎戈龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2400
</td><td style="text-align: right"> 145
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_24">
<td style="text-align: center" rowspan="9"> 上位
</td><td style="text-align: center" rowspan="6"> ★6
</td><td> 孤岛的采集任务
</td><td> 缴纳<a href="/index.php?title=%E7%8C%AB%E5%AE%85%E5%88%B8&amp;action=edit&amp;redlink=1" class="new" title="猫宅券（尚未撰写）">猫宅券</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3400
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_25">
<td> 溪流小狗龙讨伐作战
</td><td> 讨伐20头<a href="/%E5%B0%8F%E7%8B%97%E9%BE%99" title="小狗龙">小狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3400
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_26">
<td> 喧哗两成败！
</td><td> 讨伐两头狗龙王
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2400<br>2400
</td><td style="text-align: right"> 260<br>260
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_27">
<td> 土砂龙波鲁波罗斯！
</td><td> 讨伐一头<a href="/%E5%9C%9F%E7%A0%82%E9%BE%99" title="土砂龙">土砂龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2000
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_28">
<td> 狩猎凶兽首领
</td><td> 讨伐两头<a href="/%E9%87%8E%E7%8C%AA%E7%8E%8B" title="野猪王">野猪王</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3400
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_29">
<td> <font color="#f40"> 狩猎彩鸟库鲁佩科！ </font>
</td><td>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3400
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_30">
<td style="text-align: center" rowspan="2"> ★7
</td><td> 沙漠上的风牙龙卷
</td><td> 讨伐一头<a href="/%E9%A3%8E%E7%89%99%E9%BE%99" title="风牙龙">风牙龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2200
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_31">
<td> 狩猎雌火龙女王
</td><td> 讨伐一头<a href="/%E9%9B%8C%E7%81%AB%E9%BE%99" title="雌火龙">雌火龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2200
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_32">
<td style="text-align: center"> ★8
</td><td> 在湍急的溪流中
</td><td> 讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2400<br>2400
</td><td style="text-align: right"> 290<br>290
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_33">
<td style="text-align: center" rowspan="2"> 番台
</td><td style="text-align: center"> 温泉
</td><td style="text-align: center"> ★2
</td><td> 温泉设备增强作战！
</td><td> 讨伐一头狗龙王
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 800
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_34">
<td style="text-align: center"> 饮料
</td><td style="text-align: center"> ★4
</td><td> 在鸟龙种的围栏中
</td><td> 讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2200
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_35">
<td style="text-align: center" rowspan="4"> 训练所
</td><td style="text-align: center" rowspan="2"> 基础
</td><td style="text-align: center" rowspan="2"> -
</td><td> 基础训练《狩猎其二》
</td><td>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 500
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_36">
<td> 基础训练《狩猎其三》
</td><td>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 500
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_37">
<td style="text-align: center"> 武器
</td><td style="text-align: center"> -
</td><td> *的使用
</td><td>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 500
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_38">
<td style="text-align: center"> 挑战
</td><td style="text-align: center"> ★6
</td><td> 练习任务07
</td><td>
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 2200
</td><td style="text-align: right"> 380
</td><td style="text-align: right"> 70
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 前进2-3步啃咬，能连续咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬 </td><td> 侧身45°-180°啃咬，准备动作小，容易被打到
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧过身来撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 转身体用尾巴甩猎人，能甩2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 召集指令 </td><td> 召唤狗龙群
</td></tr>
<tr style="background:#f9f9f9;">
<td> 攻击指令 </td><td> 使狗龙群攻击意识大大提升
</td></tr></tbody></table>

# 毒狗龍
![](assets/毒狗龍.jpg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性相克</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>×</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>头<br>头以外</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">50 </font></td><td> 0 </td><td> 20 </td><td> 5 </td><td> 40 </td><td> 0 </td><td> 100 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头以外 </td><td> <font color="#ff0000">45 </font></td><td> <font color="#ff0000">60 </font></td><td> 40 </td><td> 0 </td><td> 20 </td><td> 5 </td><td> 40 </td><td> 0 </td><td> 0 </td><td> 180
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 300 </td><td> 150 </td><td> 150 </td><td> 120 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 50 (500) </td><td> 20 (230) </td><td> 15 (210) </td><td> 50 (320) </td><td> 50 (350)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/5秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 20秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 100 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回12秒（疲劳20秒） → 2回7秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> ○ </td><td> 疲劳时就算是发现状态也会去吃
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒狗龙的皮">毒狗龙的皮</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小毒狗龙的鳞">小毒狗龙的鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a> <font color="#008000"><b>20%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="毒狗龙的腕甲">毒狗龙的腕甲</a> <font color="#008000"><b>15%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E5%B0%8F%E3%80%91" title="龙骨【小】">龙骨【小】</a> <font color="#008000"><b>15%</b></font><br> </td><td> <a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒狗龙的皮">毒狗龙的皮</a> <font color="#008000"><b>40%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="毒狗龙的腕甲">毒狗龙的腕甲</a> <font color="#008000"><b>27%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a> <font color="#008000"><b>25%</b></font><br><a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小毒狗龙的鳞">小毒狗龙的鳞</a> <font color="#008000"><b>8%</b></font><br> </td><td> <a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒狗龙的上皮">毒狗龙的上皮</a> <font color="#008000"><b>40%</b></font><br><a href="/%E7%8C%9B%E6%AF%92%E8%A2%8B" title="猛毒袋">猛毒袋</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E5%9D%9A%E8%85%95%E7%94%B2" title="毒狗龙的坚腕甲">毒狗龙的坚腕甲</a> <font color="#008000"><b>20%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒狗龙的皮">毒狗龙的皮</a> <font color="#008000"><b>8%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="毒狗龙的腕甲">毒狗龙的腕甲</a> <font color="#008000"><b>7%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小毒狗龙的鳞">小毒狗龙的鳞</a> <font color="#008000"><b>25%</b></font><br> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小毒狗龙的鳞">小毒狗龙的鳞</a> <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>50%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒狗龙的上皮">毒狗龙的上皮</a> <font color="#008000"><b>23%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a> <font color="#008000"><b>2%</b></font> </td><td> 头部破坏时<br>濒死逃走时可能掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> 集会浴场★1~2<br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小毒狗龙的鳞">小毒狗龙的鳞</a>*1 <font color="#008000"><b>55%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a>*1 <font color="#008000"><b>45%</b></font><br> </td><td> <a href="/%E7%8E%8B%E8%80%85%E4%B9%8B%E5%96%99" title="王者之喙">王者之喙</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a>*1 <font color="#008000"><b>40%</b></font><br> </td><td> <a href="/%E7%8E%8B%E8%80%85%E4%B9%8B%E5%96%99" title="王者之喙">王者之喙</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E7%8C%9B%E6%AF%92%E8%A2%8B" title="猛毒袋">猛毒袋</a>*2 <font color="#008000"><b>30%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a>*1 <font color="#008000"><b>10%</b></font> </td><td> 头部受到一定伤害（胆怯2回），脸颊的毒袋破坏后瘪掉
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> 集会浴场★1~2<br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="毒狗龙的腕甲">毒狗龙的腕甲</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E5%B0%8F%E3%80%91" title="龙骨【小】">龙骨【小】</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒狗龙的皮">毒狗龙的皮</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E6%AF%92%E7%89%99" title="小毒狗龙的毒牙">小毒狗龙的毒牙</a>*2 <font color="#008000"><b>15%</b></font><br> </td><td> <a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E8%85%95%E7%94%B2" title="毒狗龙的腕甲">毒狗龙的腕甲</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="毒狗龙的皮">毒狗龙的皮</a>*1 <font color="#008000"><b>20%</b></font><br> </td><td> <a href="/%E7%8C%9B%E6%AF%92%E8%A2%8B" title="猛毒袋">猛毒袋</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E5%9D%9A%E8%85%95%E7%94%B2" title="毒狗龙的坚腕甲">毒狗龙的坚腕甲</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E6%AF%92%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="毒狗龙的上皮">毒狗龙的上皮</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E6%AF%92%E8%A2%8B" title="毒袋">毒袋</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a>*1 <font color="#008000"><b>4%</b></font> </td><td> 体力40%以下（下位）<br>体力30%以下（上位）
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2">
<p>体型
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>基础
</p><p>体力
</p>
</td><td style="text-align: center">
<p>攻击力
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>全体
</p><p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td style="text-align: center" rowspan="4"> 村长
</td><td style="text-align: center" rowspan="4"> 下位
</td><td style="text-align: center"> ★3
</td><td> 水没林的暴力队
</td><td style="text-align: center">讨伐一头毒狗龙
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1125
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td style="text-align: center" rowspan="2"> ★4
</td><td> 博士的毒毒研究
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1125<br>1125
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td> 天降毒雨
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1250
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_5">
<td style="text-align: center"> ★5
</td><td> 没有仁义的抗争
</td><td style="text-align: center">讨伐一头毒狗龙和一头<a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1900
</td><td style="text-align: right"> 125
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_6">
<td style="text-align: center" rowspan="16"> 集会浴場
</td><td style="text-align: center" rowspan="2"> 初心者
</td><td style="text-align: center" rowspan="2"> ★1
</td><td> 水没林的暴力队
</td><td style="text-align: center">讨伐一头毒狗龙
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1250
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_7">
<td> 毒之毒狗龙
</td><td style="text-align: center"> 讨伐10头<a href="/%E5%B0%8F%E6%AF%92%E7%8B%97%E9%BE%99" title="小毒狗龙">小毒狗龙</a>
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 1250
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_8">
<td style="text-align: center" rowspan="5"> 下位
</td><td style="text-align: center" rowspan="2"> ★3
</td><td> 水没林的暴力队
</td><td style="text-align: center">讨伐一头毒狗龙
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2625
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_9">
<td> 狩猎水生兽！
</td><td style="text-align: center"> 讨伐一头<a href="/%E6%B0%B4%E5%85%BD" title="水兽">水兽</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2625
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_10">
<td style="text-align: center" rowspan="2"> ★4
</td><td> 火山的毒物
</td><td style="text-align: center">讨伐一头毒狗龙
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2750
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_11">
<td> 注意脚下安全！
</td><td style="text-align: center"> 暂缺
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2750
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_12">
<td style="text-align: center"> ★5
</td><td> 为了吾主前往火山吧
</td><td style="text-align: center">讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2500
</td><td style="text-align: right"> 145
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_13">
<td style="text-align: center" rowspan="8"> 上位
</td><td style="text-align: center" rowspan="4"> ★6
</td><td> 洗澡的牙兽
</td><td style="text-align: center">暂无
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3250
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_14">
<td> 水没林的紫雾
</td><td style="text-align: center">讨伐一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2750<br>2750
</td><td style="text-align: right"> 250<br>250
</td><td style="text-align: right"> 85
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_15">
<td> 集结！水没林的紫水兽战！
</td><td style="text-align: center">讨伐一头<a href="/%E7%B4%AB%E6%B0%B4%E5%85%BD" title="紫水兽">紫水兽</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2250
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_16">
<td> 蓝蘑菇+解毒草＝？
</td><td style="text-align: center"> 暂缺
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3500
</td><td style="text-align: right"> 250
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_17">
<td style="text-align: center" rowspan="4"> ★7
</td><td> 黑暗中的极速战斗
</td><td style="text-align: center">讨伐一头<a href="/%E7%BB%BF%E8%BF%85%E9%BE%99" title="绿迅龙">绿迅龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2250
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_18">
<td> 赤之麻痹、赤之毒
</td><td style="text-align: center">讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 3000
</td><td style="text-align: right"> 270
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_19">
<td> 威胁！火山的铁锤！
</td><td style="text-align: center">讨伐一头<a href="/%E7%88%86%E9%94%A4%E9%BE%99" title="爆锤龙">爆锤龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 2250
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_20">
<td> クローベリング・タイム！
</td><td style="text-align: center">讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 1575<br>1575
</td><td style="text-align: right"> 270
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_21">
<td style="text-align: center"> 配信
</td><td style="text-align: center"> ★4
</td><td> ファミ通･状態異常を克服せよ
</td><td style="text-align: center">
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 3000
</td><td style="text-align: right"> 150
</td><td style="text-align: right"> 85
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_22">
<td style="text-align: center" rowspan="2"> 番台
</td><td style="text-align: center"> 温泉
</td><td style="text-align: center"> ★4
</td><td> 沸水和喷烟
</td><td style="text-align: center">讨伐一头毒狗龙和一头<a href="/%E8%B5%A4%E7%94%B2%E5%85%BD" title="赤甲兽">赤甲兽</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1250
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_23">
<td style="text-align: center"> 饮料
</td><td style="text-align: center"> ★4
</td><td> 在鸟龙种的围栏中
</td><td style="text-align: center">讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2500
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_24">
<td style="text-align: center"> 训练所
</td><td style="text-align: center"> 挑战
</td><td style="text-align: center"> ★3
</td><td> 练习任务01?
</td><td style="text-align: center"> <font color="#F00">&nbsp;</font>
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 100
</td><td style="text-align: right"> 1825
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 80
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 前进2-3步啃咬，能连续咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬 </td><td> 侧身45°-180°啃咬，准备动作小，容易被打到
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧过身来撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 转身体用尾巴甩猎人，能甩2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 喷毒雾 </td><td> 吐出毒雾，能在空气中漂浮一段时间
</td></tr>
<tr style="background:#f9f9f9;">
<td> 召集指令 </td><td> 召唤狗龙群
</td></tr>
<tr style="background:#f9f9f9;">
<td> 攻击指令 </td><td> 使狗龙群攻击意识大大提升
</td></tr></tbody></table>

# 眠狗龍
![](assets/眠狗龍.jpg)
## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性耐性</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>◎</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>无</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">55 </font></td><td> 40 </td><td> 20 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 100 </td><td> 300
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头以外 </td><td> 40 </td><td> 40 </td><td> 35 </td><td> 40 </td><td> 20 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 0 </td><td> 350
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 90 </td><td> 150 </td><td> 300 </td><td> 180 </td><td> 200
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 30 (300) </td><td> 20 (230) </td><td> 50 (500) </td><td> 50 (380) </td><td> 50 (400)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 15秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 75 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回15秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回12秒（疲劳20秒） → 2回7秒（疲劳15秒） → 3回5秒（疲劳10秒） → 4回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> ○ </td><td> 疲劳时就算是发现状态也会去吃
</td></tr></tbody></table>

## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="眠狗龙的皮">眠狗龙的皮</a> <font color="#008000"><b>30%</b></font><br><a href="/%E5%B0%8F%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小眠狗龙的鳞">小眠狗龙的鳞</a> <font color="#008000"><b>20%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E5%B0%8F%E3%80%91" title="龙骨【小】">龙骨【小】</a> <font color="#008000"><b>20%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="眠狗龙的爪">眠狗龙的爪</a> <font color="#008000"><b>15%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a> <font color="#008000"><b>15%</b></font><br> </td><td> <a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="眠狗龙的皮">眠狗龙的皮</a> <font color="#008000"><b>40%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="眠狗龙的爪">眠狗龙的爪</a> <font color="#008000"><b>27%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a> <font color="#008000"><b>25%</b></font><br><a href="/%E5%B0%8F%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小眠狗龙的鳞">小眠狗龙的鳞</a> <font color="#008000"><b>8%</b></font><br> </td><td> <a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="眠狗龙的上皮">眠狗龙的上皮</a> <font color="#008000"><b>38%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="眠狗龙的尖爪">眠狗龙的尖爪</a> <font color="#008000"><b>25%</b></font><br><a href="/%E6%98%8F%E7%9D%A1%E8%A2%8B" title="昏睡袋">昏睡袋</a> <font color="#008000"><b>22%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="眠狗龙的皮">眠狗龙的皮</a> <font color="#008000"><b>8%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="眠狗龙的爪">眠狗龙的爪</a> <font color="#008000"><b>7%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%8F%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小眠狗龙的鳞">小眠狗龙的鳞</a> <font color="#008000"><b>25%</b></font><br> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%B0%8F%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小眠狗龙的鳞">小眠狗龙的鳞</a> <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>50%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>25%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="眠狗龙的上皮">眠狗龙的上皮</a> <font color="#008000"><b>23%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a> <font color="#008000"><b>2%</b></font> </td><td> 头部破坏时<br>濒死逃走时可能掉落
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> 集会浴场★1~2<br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E5%B0%8F%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E9%B3%9E" title="小眠狗龙的鳞">小眠狗龙的鳞</a>*1 <font color="#008000"><b>70%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a>*1 <font color="#008000"><b>30%</b></font><br> </td><td> <a href="/%E7%8E%8B%E8%80%85%E7%9A%84%E5%A4%B4%E5%86%A0" title="王者的头冠">王者的头冠</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a>*1 <font color="#008000"><b>40%</b></font> </td><td> <a href="/%E7%8E%8B%E8%80%85%E7%9A%84%E5%A4%B4%E5%86%A0" title="王者的头冠">王者的头冠</a>*1  <font color="#008000"><b>60%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a>*1 <font color="#008000"><b>12%</b></font> </td><td> 头部受到一定伤害（胆怯2回），顶冠中间折断
</td></tr></tbody></table>

## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td>  <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%B8%9F%E9%BE%99%E7%A7%8D%E4%B9%8B%E7%89%99" title="鸟龙种之牙">鸟龙种之牙</a>*2 <font color="#008000"><b>33%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="眠狗龙的爪">眠狗龙的爪</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="眠狗龙的皮">眠狗龙的皮</a>*1 <font color="#008000"><b>17%</b></font><br> </td><td> <a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%88%AA" title="眠狗龙的爪">眠狗龙的爪</a>*1 <font color="#008000"><b>48%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a>*1 <font color="#008000"><b>32%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E7%9A%AE" title="眠狗龙的皮">眠狗龙的皮</a>*1 <font color="#008000"><b>20%</b></font><br> </td><td> <a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E5%B0%96%E7%88%AA" title="眠狗龙的尖爪">眠狗龙的尖爪</a>*1 <font color="#008000"><b>45%</b></font><br><a href="/%E6%98%8F%E7%9D%A1%E8%A2%8B" title="昏睡袋">昏睡袋</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E7%9C%A0%E7%8B%97%E9%BE%99%E7%9A%84%E4%B8%8A%E7%9A%AE" title="眠狗龙的上皮">眠狗龙的上皮</a>*1 <font color="#008000"><b>14%</b></font><br><a href="/%E7%9D%A1%E7%9C%A0%E8%A2%8B" title="睡眠袋">睡眠袋</a>*2 <font color="#008000"><b>8%</b></font><br><a href="/%E9%B8%9F%E9%BE%99%E7%8E%89" title="鸟龙玉">鸟龙玉</a>*1 <font color="#008000"><b>5%</b></font> </td><td> 体力40%以下（下位）<br>体力30%以下（上位）
</td></tr></tbody></table>

## 出現任務

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2">
<p>体型
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>基础
</p><p>体力
</p>
</td><td style="text-align: center">
<p>攻击力
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>全体
</p><p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td style="text-align: center" rowspan="4"> 村长
</td><td style="text-align: center" rowspan="4"> 下位
</td><td style="text-align: center" rowspan="2"> ★4
</td><td> 引导眠意的苍白
</td><td> 讨伐一头眠狗龙
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 1650
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td> 前进！冻土调查队
</td><td> 讨伐20头<a href="/%E5%B0%8F%E7%8B%97%E9%BE%99" title="小狗龙">小狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 1650
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td style="text-align: center" rowspan="2"> ★5
</td><td> 难以抗拒的睡魔之战
</td><td> 讨伐一头眠狗龙
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 1650<br>1650
</td><td style="text-align: right"> 120<br>120
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_5">
<td> 白银的抱拥
</td><td> 讨伐所有大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 2310
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_6">
<td style="text-align: center" rowspan="8"> 集会浴场
</td><td style="text-align: center" rowspan="3"> 下位
</td><td style="text-align: center" rowspan="2"> ★4
</td><td> 引导眠意的苍白
</td><td> 讨伐一头眠狗龙
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 3630
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_7">
<td> 前进！冻土调查队
</td><td> 讨伐20头<a href="/%E5%B0%8F%E7%8B%97%E9%BE%99" title="小狗龙">小狗龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 3630
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_8">
<td style="text-align: center"> ★5
</td><td> 强豪集结
</td><td> 讨伐全部全部大型怪物
</td><td style="text-align: right"> 90
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 2970
</td><td style="text-align: right"> 140
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_9">
<td style="text-align: center" rowspan="4"> 上位
</td><td style="text-align: center" rowspan="2"> ★7
</td><td> 蓝与白的挽歌
</td><td> 讨伐一头眠狗龙和一头<a href="/%E7%99%BD%E5%85%94%E5%85%BD" title="白兔兽">白兔兽</a>
</td><td style="text-align: right"> 88
</td><td style="text-align: right"> 125
</td><td style="text-align: right"> 3960
</td><td style="text-align: right"> 270
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_10">
<td> 冰牙龙贝利欧罗斯！
</td><td> 讨伐一头<a href="/%E5%86%B0%E7%89%99%E9%BE%99" title="冰牙龙">冰牙龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 80 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 140 </font></b>
</td><td style="text-align: right"> 2640
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_11">
<td style="text-align: center" rowspan="2"> ★8
</td><td> 冰之楔
</td><td> 讨伐一头<a href="/%E5%86%BB%E6%88%88%E9%BE%99" title="冻戈龙">冻戈龙</a>
</td><td style="text-align: right"> <b><font color="#ff4"> 80 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 140 </font></b>
</td><td style="text-align: right"> 3465
</td><td style="text-align: right"> 300
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_12">
<td> 被放逐到冻土的怪物们
</td><td> 讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 80 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 140 </font></b>
</td><td style="text-align: right"> 1980<br>1980
</td><td style="text-align: right"> 290<br>290
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_13">
<td style="text-align: center"> 配信
</td><td style="text-align: center"> ★4
</td><td> 犬夜叉・大妖の牙を求めて
</td><td>
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 2640
</td><td style="text-align: right"> 135
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_14">
<td style="text-align: center" rowspan="2"> 番台
</td><td style="text-align: center"> 温泉
</td><td style="text-align: center"> ★4
</td><td> 寻找永久冻土的神秘温泉
</td><td> 暂缺
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 1155<br>1155
</td><td style="text-align: right"> 110<br>110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_15">
<td style="text-align: center"> 饮料
</td><td style="text-align: center"> ★4
</td><td> 在鸟龙种的围栏中
</td><td> 讨伐全部大型怪物
</td><td style="text-align: right"> <b><font color="#ff4"> 83 </font></b>
</td><td style="text-align: right"> 129
</td><td style="text-align: right"> 3300
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr></tbody></table>

## 攻擊方式

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 啃咬 </td><td> 前进2-3步啃咬，能连续咬2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 转身啃咬 </td><td> 侧身45°-180°啃咬，准备动作小，容易被打到
</td></tr>
<tr style="background:#f9f9f9;">
<td> 铁山靠 </td><td> 侧过身来撞击猎人
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 转身体用尾巴甩猎人，能甩2次
</td></tr>
<tr style="background:#f9f9f9;">
<td> 睡眠液 </td><td> 吐出催眠液体，使人睡着
</td></tr>
<tr style="background:#f9f9f9;">
<td> 召集指令 </td><td> 召唤狗龙群
</td></tr>
<tr style="background:#f9f9f9;">
<td> 攻击指令 </td><td> 使狗龙群攻击意识大大提升
</td></tr></tbody></table>
# 彩鳥
![](assets/彩鳥.jpg)

## 基本抗性
◎＞○＞△＞×
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性耐性</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td><div><center><b>斩</b></center></div>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>△</center></div>
</td><td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头<br>喉袋<br>身体</center></div>
</td><td><div><center>头<br>喉袋<br>身体</center></div>
</td><td><div><center>头<br>喉袋<br>身体</center></div>
</td><td><div><center>无</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>
## 肉質

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">70 </font></td><td> <font color="#ff0000">90 </font></td><td> <font color="#ff0000">60 </font></td><td> 10 </td><td> 25 </td><td> 15 </td><td> 35 </td><td> 5 </td><td> 100 </td><td> 130
</td></tr>
<tr style="background:#f9f9f9;">
<td> 喉袋（唱歌时） </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">55 </font></td><td> <font color="#ff0000">70 </font></td><td> 5 </td><td> 15 </td><td> 10 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 80
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">50 </font></td><td> <font color="#ff0000">50 </font></td><td> 5 </td><td> 15 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 火打石 </td><td> 25 </td><td> 25 </td><td> 30 </td><td> 10 </td><td> 25 </td><td> 15 </td><td> 35 </td><td> 0 </td><td> 0 </td><td> 左120/右120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼膜 </td><td> 40 </td><td> 40 </td><td> 30 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 15 </td><td> 10 </td><td> 0 </td><td> 左120/右120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼骨 </td><td> 36 </td><td> 36 </td><td> 30 </td><td> 5 </td><td> 10 </td><td> 5 </td><td> 15 </td><td> 10 </td><td> 0 </td><td> 左120/右120
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 40 </td><td> 40 </td><td> 40 </td><td> 5 </td><td> 10 </td><td> 10 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 左150/右150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 40 </td><td> 30 </td><td> 30 </td><td> 5 </td><td> 15 </td><td> 10 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 120
</td></tr></tbody></table>

## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 150 </td><td> 150 </td><td> 100 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (550) </td><td> 100 (550) </td><td> 100 (550) </td><td> 100 (500) </td><td> 75 (450)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> ○ </td><td> 初回20秒 → 2回15秒 → 3回10秒 → 4回开始5秒 （疲劳+10秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> ○ </td><td> 彩鸟在唱歌演奏时，使用后造成3秒硬直，发怒时有效。
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★3（非常容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E9%B3%9E" title="彩鸟的鳞">彩鸟的鳞</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E7%BE%BD%E6%AF%9B" title="彩鸟的羽毛">彩鸟的羽毛</a> <font color="#008000"><b>27%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E4%B8%AD%E3%80%91" title="龙骨【中】">龙骨【中】</a> <font color="#008000"><b>25%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E5%B0%8F%E3%80%91" title="龙骨【小】">龙骨【小】</a> <font color="#008000"><b>10%</b></font><br> </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E9%B3%9E" title="彩鸟的鳞">彩鸟的鳞</a> <font color="#008000"><b>45%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E7%BE%BD%E6%AF%9B" title="彩鸟的羽毛">彩鸟的羽毛</a> <font color="#008000"><b>32%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E4%B8%AD%E3%80%91" title="龙骨【中】">龙骨【中】</a><font color="#008000"><b>12%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a> <font color="#008000"><b>9%</b></font><br> <a href="/%E5%A5%87%E6%80%AA%E7%9A%84%E5%96%99" title="奇怪的喙">奇怪的喙</a> <font color="#008000"><b>2%</b></font> </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E4%B8%8A%E9%B3%9E" title="彩鸟的上鳞">彩鸟的上鳞</a> <font color="#008000"><b>40%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E4%B9%8B%E7%BF%BC" title="彩鸟之翼">彩鸟之翼</a> <font color="#008000"><b>32%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a> <font color="#008000"><b>12%</b></font><br><a href="/%E4%B8%8A%E9%BE%99%E9%AA%A8" title="上龙骨">上龙骨</a> <font color="#008000"><b>10%</b></font><br><a href="/%E5%A5%87%E6%80%AA%E7%9A%84%E5%96%99" title="奇怪的喙">奇怪的喙</a> <font color="#008000"><b>2%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E9%B3%9E" title="彩鸟的鳞">彩鸟的鳞</a> <font color="#008000"><b>25%</b></font><br> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>75%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E9%B3%9E" title="彩鸟的鳞">彩鸟的鳞</a> <font color="#008000"><b>18%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a> <font color="#008000"><b>7%</b></font> </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a> <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E4%B8%8A%E9%B3%9E" title="彩鸟的上鳞">彩鸟的上鳞</a> <font color="#008000"><b>15%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a> <font color="#008000"><b>10%</b></font> </td><td> 唱歌时攻击音袋让它硬直（用音爆弹的话不会有掉落物）
</td></tr></tbody></table>

## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> 集会浴场★1~2<br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E9%B3%9E" title="彩鸟的鳞">彩鸟的鳞</a>*2 <font color="#008000"><b>60%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*1 <font color="#008000"><b>40%</b></font><br> </td><td> <a href="/%E5%A5%87%E6%80%AA%E7%9A%84%E5%96%99" title="奇怪的喙">奇怪的喙</a>*1 <font color="#008000"><b>75%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>25%</b></font> </td><td> <a href="/%E6%BC%82%E4%BA%AE%E7%9A%84%E5%96%99" title="漂亮的喙">漂亮的喙</a>*1 <font color="#008000"><b>55%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E5%A5%87%E6%80%AA%E7%9A%84%E5%96%99" title="奇怪的喙">奇怪的喙</a>*1 <font color="#008000"><b>22%</b></font> </td><td> 头部受到一定伤害（硬直2次），嘴喙的前端破裂
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> <a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E5%B0%8F%E3%80%91" title="龙骨【小】">龙骨【小】</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E7%BE%BD%E6%AF%9B" title="彩鸟的羽毛">彩鸟的羽毛</a>*1 <font color="#008000"><b>30%</b></font><br> </td><td> <a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*1 <font color="#008000"><b>50%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E7%BE%BD%E6%AF%9B" title="彩鸟的羽毛">彩鸟的羽毛</a>*1 <font color="#008000"><b>40%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a>*1 <font color="#008000"><b>10%</b></font> </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E4%B9%8B%E7%BF%BC" title="彩鸟之翼">彩鸟之翼</a>*1 <font color="#008000"><b>43%</b></font><br><a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*1 <font color="#008000"><b>30%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a>*1 <font color="#008000"><b>19%</b></font><br><a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*2 <font color="#008000"><b>5%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>3%</b></font> </td><td> 翼部受到一定伤害（硬直1次），破坏一边后得到奖励，火打石碎掉后变小
</td></tr></tbody></table>
## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>村/集会浴场<br>下位</b> </td><td> <b>集会浴场<br>上位</b> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*1 <font color="#008000"><b>35%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E4%B8%AD%E3%80%91" title="龙骨【中】">龙骨【中】</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E7%BE%BD%E6%AF%9B" title="彩鸟的羽毛">彩鸟的羽毛</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*1 <font color="#008000"><b>12%</b></font><br> </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E7%BE%BD%E6%AF%9B" title="彩鸟的羽毛">彩鸟的羽毛</a>*1 <font color="#008000"><b>28%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a>*1 <font color="#008000"><b>12%</b></font><br><a href="/%E9%BE%99%E9%AA%A8%E3%80%90%E4%B8%AD%E3%80%91" title="龙骨【中】">龙骨【中】</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E6%BC%82%E4%BA%AE%E7%9A%84%E5%96%99" title="漂亮的喙">漂亮的喙</a>*1 <font color="#008000"><b>5%</b></font> </td><td> <a href="/%E5%BD%A9%E9%B8%9F%E4%B9%8B%E7%BF%BC" title="彩鸟之翼">彩鸟之翼</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>20%</b></font><br><a href="/%E7%81%AB%E6%89%93%E7%9F%B3" title="火打石">火打石</a>*1 <font color="#008000"><b>20%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a>*1 <font color="#008000"><b>15%</b></font><br><a href="/%E5%BD%A9%E9%B8%9F%E7%9A%84%E4%B8%8A%E9%B3%9E" title="彩鸟的上鳞">彩鸟的上鳞</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E6%BC%82%E4%BA%AE%E7%9A%84%E5%96%99" title="漂亮的喙">漂亮的喙</a>*1 <font color="#008000"><b>9%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>2%</b></font> </td><td> 体力30%以下（下位）<br>体力25%以下（上位）
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2">
<p>体型
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>基础
</p><p>体力
</p>
</td><td style="text-align: center">
<p>攻击力
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>全体
</p><p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td style="text-align: center" rowspan="3"> 村长
</td><td style="text-align: center" rowspan="3"> 下位
</td><td style="text-align: center" rowspan="2"> ★3
</td><td> 跳舞的彩鸟
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1710
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td> 追杀彩鸟！
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 1710
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td style="text-align: center"> ★4
</td><td> 狩猎彩鸟库鲁佩科
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 2128
</td><td style="text-align: right"> 110
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_5">
<td style="text-align: center" rowspan="8"> 集会浴场
</td><td style="text-align: center"> 初心
</td><td style="text-align: center"> ★2
</td><td> 追杀彩鸟！
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 105
</td><td style="text-align: right"> 2280
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 100
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_6">
<td style="text-align: center" rowspan="3"> 下位
</td><td style="text-align: center"> ★3
</td><td> 狩猎彩鸟库鲁佩科
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 3990
</td><td style="text-align: right"> 120
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_7">
<td style="text-align: center"> ★4
</td><td> 溪流的吵闹小子
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 4560
</td><td style="text-align: right"> 130
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_8">
<td style="text-align: center"> ★5
</td><td> 追杀彩鸟！
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 90 </font></b>
</td><td style="text-align: right"> 117
</td><td style="text-align: right"> 4940
</td><td style="text-align: right"> 140
</td><td style="text-align: right"> 95
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_9">
<td style="text-align: center" rowspan="3"> 上位
</td><td style="text-align: center"> ★6
</td><td> 狩猎彩鸟库鲁佩科
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 5624
</td><td style="text-align: right"> 270
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_10">
<td style="text-align: center"> ★7
</td><td> 追杀彩鸟！
</td><td style="text-align: center">讨伐一头彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 88 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 6080
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_11">
<td style="text-align: center"> ★8
</td><td> 歌唱！溪流的彩鸟！
</td><td style="text-align: center">
</td><td style="text-align: right"> 95
</td><td style="text-align: right"> 115
</td><td style="text-align: right"> 4370<br>4446<br>4560<br>4674<br>4826
</td><td style="text-align: right"> 290<br>300<br>310<br>320<br>330
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_12">
<td> 配信
</td><td style="text-align: center"> ★8
</td><td> モンハン部・特選クエスト！
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 40 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 40 </font></b>
</td><td style="text-align: right"> 6232
</td><td style="text-align: right"> 300
</td><td style="text-align: right"> 70
</td></tr></tbody></table>
## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 无准备动作，向前突进一段距离
</td></tr>
<tr style="background:#f9f9f9;">
<td> 4连啄 </td><td> 用嘴连续啄四下，也能接于突进后
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 回转身体用尾巴甩人，附带风压【小】效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 打火 </td><td> 向前跃出并撞击两翼的火打石，打出火花来攻击；最多可以连续3次，每次重新修正方向；被打中时会陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 打火（原地） </td><td> 原地小后跃并敲击火打石，用火花攻击；被打中时会陷入燃烧异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞行 </td><td> 向后跃起并进去低空飞行状态，飞起和降落时附带风压【小】效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 粘液球 </td><td> 同时吐出3个粘液球，方向分别是左中右
</td></tr>
<tr style="background:#f9f9f9;">
<td> 演奏 </td><td> 鼓起喉袋演奏：红波（范围内自己以外的大型怪物强制进入怒状态）；橙波（范围内硬化怪物的肉质）；绿波（范围内回复怪物的体力）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 呼叫增援 </td><td> HP被打到一定程度时，呼叫其他大型怪物作为第三方战斗（主要攻击目标依然是猎人）
</td></tr></tbody></table>
# 紅彩鳥
![](assets/紅彩鳥.jpg)

## 基本抗性
◎＞○＞△＞×

<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td colspan="5">
<div><center><b>属性耐性</b></center></div>
</td><td colspan="3">
<div><center><b>弱点部位</b></center></div>
</td><td colspan="2">
<div><center><b>特殊</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;" align="center">
<td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>斩</b>
</td><td><div><center><b>打</b></center></div>
</td><td><div><center><b>弹</b></center></div>
</td><td><div><center><b>咆哮</b></center></div>
</td><td><div><center><b>风压</b></center></div>
</td></tr>
<tr style="background:#f9f9f9;">
<td><div><center>○</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>×</center></div>
</td><td><div><center>◎</center></div>
</td><td><div><center>△</center></div>
</td><td><div><center>头<br>喉袋<br>身体</center></div>
</td><td><div><center>头</center></div>
</td><td><div><center>头<br>喉袋</center></div>
</td><td><div><center>小</center></div>
</td><td><div><center>小</center></div>
</td></tr></tbody></table>

## 肉質
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>斩击</b> </td><td> <b>打击</b> </td><td> <b>射击</b>
</td><td style="background:#ff3300; color:#ffffff"> <b>火</b>
</td><td style="background:#0066ff; color:#ffffff"> <b>水</b>
</td><td style="background:#ffcc00; color:#ffffff"> <b>雷</b>
</td><td style="background:#66ccff; color:#ffffff"> <b>冰</b>
</td><td style="background:#669900; color:#ffffff"> <b>龙</b>
</td><td> <b>气绝</b> </td><td> <b>硬直</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> <font color="#ff0000">60 </font></td><td> <font color="#ff0000">70 </font></td><td> <font color="#ff0000">60 </font></td><td> 20 </td><td> 10 </td><td> 0 </td><td> 30 </td><td> 5 </td><td> 100 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 喉袋（唱歌时） </td><td> <font color="#ff0000">50 </font></td><td> 40 </td><td> <font color="#ff0000">70 </font></td><td> 15 </td><td> 5 </td><td> 0 </td><td> 20 </td><td> 0 </td><td> 0 </td><td> 100
</td></tr>
<tr style="background:#f9f9f9;">
<td> 身体 </td><td> <font color="#ff0000">50 </font></td><td> 40 </td><td> 40 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 15 </td><td> 0 </td><td> 0 </td><td> 180
</td></tr>
<tr style="background:#f9f9f9;">
<td> 电气石 </td><td> 21 </td><td> 21 </td><td> 30 </td><td> 15 </td><td> 15 </td><td> 0 </td><td> 30 </td><td> 0 </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼膜 </td><td> 40 </td><td> 40 </td><td> 30 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 15 </td><td> 10 </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼骨 </td><td> 35 </td><td> 35 </td><td> 30 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 15 </td><td> 10 </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 脚 </td><td> 40 </td><td> 40 </td><td> 40 </td><td> 10 </td><td> 5 </td><td> 0 </td><td> 10 </td><td> 0 </td><td> 0 </td><td> 左160/右160
</td></tr>
<tr style="background:#f9f9f9;">
<td> 尾巴 </td><td> 40 </td><td> 30 </td><td> 30 </td><td> 15 </td><td> 15 </td><td> 0 </td><td> 20 </td><td> 10 </td><td> 0 </td><td> 120
</td></tr></tbody></table>
## 屬性攻擊
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>属性</b>
</td><td style="background:#996699; color:#FFFFFF"> <b>毒</b>
</td><td style="background:#be9f22; color:#FFFFFF"> <b>麻痹</b>
</td><td style="background:#6b96d3; color:#FFFFFF"> <b>睡眠</b>
</td><td style="background:#258e04; color:#FFFFFF"> <b>气绝</b>
</td><td style="background:#330099; color:#FFFFFF"> <b>减气</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值初期 </td><td> 150 </td><td> 150 </td><td> 150 </td><td> 130 </td><td> 150
</td></tr>
<tr style="background:#f9f9f9;">
<td> 耐性值上升(最大) </td><td> 100 (550) </td><td> 100 (550) </td><td> 100 (550) </td><td> 100 (530) </td><td> 75 (450)
</td></tr>
<tr style="background:#f9f9f9;">
<td> 积累值减少 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒 </td><td> 5/10秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 效果持续时间 </td><td> 60秒 </td><td> 10秒 </td><td> 30秒 </td><td> 10秒 </td><td> 120秒
</td></tr>
<tr style="background:#f9f9f9;">
<td> 伤害 </td><td> 150 </td><td> - </td><td> - </td><td> - </td><td> -
</td></tr></tbody></table>
## 道具效果
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>道具名</b> </td><td> <b>效果</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%90%BD%E7%A9%B4" title="落穴">落穴</a> </td><td> ○ </td><td> 初回12秒（疲劳25秒） → 2回8秒（疲劳15秒） → 3回开始8秒（疲劳8秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%BA%BB%E7%97%B9%E9%99%B7%E9%98%B1" title="麻痹陷阱">麻痹陷阱</a> </td><td> ○ </td><td> 初回8秒（疲劳15秒） → 2回5秒（疲劳10秒） → 3回开始5秒（疲劳5秒）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%97%AA%E5%85%89%E5%BC%B9" title="闪光弹">闪光弹</a> </td><td> × </td><td> 无效
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E9%9F%B3%E7%88%86%E5%BC%B9" title="音爆弹">音爆弹</a> </td><td> ○ </td><td> 红彩鸟在唱歌演奏时，使用后造成3秒硬直，发怒时有效。
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/index.php?title=%E8%82%A5%E6%96%99%E5%BC%B9&amp;action=edit&amp;redlink=1" class="new" title="肥料弹（尚未撰写）">肥料弹</a> </td><td> ○ </td><td> 危险度★5（稍微容易逃）
</td></tr>
<tr style="background:#f9f9f9;">
<td> <a href="/%E8%82%89%E7%B1%BB" title="肉类">肉类</a> </td><td> × </td><td> 无效
</td></tr></tbody></table>
## 剝取 掉落
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> <b>回数</b> </td><td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 本体 </td><td> 3 </td><td> 下位无此怪物 </td><td> 下位无此怪物 </td><td> <a href="/%E7%BA%A2%E5%BD%A9%E9%B8%9F%E7%9A%84%E4%B8%8A%E9%B3%9E" title="红彩鸟的上鳞">红彩鸟的上鳞</a> <font color="#008000"><b>40%</b></font><br><a href="/%E7%BA%A2%E5%BD%A9%E9%B8%9F%E4%B9%8B%E7%BF%BC" title="红彩鸟之翼">红彩鸟之翼</a> <font color="#008000"><b>32%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a> <font color="#008000"><b>16%</b></font><br><a href="/%E4%B8%8A%E9%BE%99%E9%AA%A8" title="上龙骨">上龙骨</a> <font color="#008000"><b>16%</b></font><br><a href="/%E6%BC%82%E4%BA%AE%E7%9A%84%E5%96%99" title="漂亮的喙">漂亮的喙</a> <font color="#008000"><b>2%</b></font> </td><td>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 掉落 </td><td> - </td><td> 下位无此怪物 </td><td> 下位无此怪物 </td><td> <a href="/%E9%BE%99%E4%B9%8B%E6%B3%AA" title="龙之泪">龙之泪</a>*1 <font color="#008000"><b>38%</b></font><br><a href="/%E5%A4%A7%E9%A2%97%E9%BE%99%E6%B3%AA" title="大颗龙泪">大颗龙泪</a> <font color="#008000"><b>37%</b></font><br><a href="/%E7%BA%A2%E5%BD%A9%E9%B8%9F%E7%9A%84%E4%B8%8A%E9%B3%9E" title="红彩鸟的上鳞">红彩鸟的上鳞</a> <font color="#008000"><b>15%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a> <font color="#008000"><b>10%</b></font> </td><td> 唱歌时攻击音袋让它硬直（用音爆弹的话不会有掉落物）
</td></tr></tbody></table>
## 部位破壞
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>部位</b> </td><td> 集会浴场★1~2<br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>破坏条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 头 </td><td> 下位无此怪物 </td><td> 下位无此怪物 </td><td> <a href="/%E6%BC%82%E4%BA%AE%E7%9A%84%E5%96%99" title="漂亮的喙">漂亮的喙</a>*1 <font color="#008000"><b>55%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a>*2 <font color="#008000"><b>25%</b></font><br><a href="/%E5%A5%87%E6%80%AA%E7%9A%84%E5%96%99" title="奇怪的喙">奇怪的喙</a>*1 <font color="#008000"><b>20%</b></font> </td><td> 头部受到一定伤害（硬直2次），嘴喙的前端破裂
</td></tr>
<tr style="background:#f9f9f9;">
<td> 翼 </td><td> 下位无此怪物 </td><td> 下位无此怪物 </td><td> <a href="/%E7%94%B5%E6%B0%94%E7%9F%B3" title="电气石">电气石</a>*1 <font color="#008000"><b>60%</b></font><br><a href="/%E7%BA%A2%E5%BD%A9%E9%B8%9F%E4%B9%8B%E7%BF%BC" title="红彩鸟之翼">红彩鸟之翼</a>*1 <font color="#008000"><b>27%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>20%</b></font> </td><td> 翼部受到一定伤害（硬直1次），破坏一边后得到奖励，电气石碎掉后变小
</td></tr></tbody></table>

## 捕獲
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>集会浴场★1~2</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>村/集会浴场下位</b><br><font color="#008000"><b>掉率</b></font><br> </td><td> <b>集会浴场上位</b><br><font color="#008000"><b>掉率</b></font> </td><td> <b>捕获条件</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 下位无此怪物 </td><td> 下位无此怪物 </td><td> <a href="/%E7%94%B5%E6%B0%94%E7%9F%B3" title="电气石">电气石</a>*1 <font color="#008000"><b>25%</b></font><br><a href="/%E7%BA%A2%E5%BD%A9%E9%B8%9F%E4%B9%8B%E7%BF%BC" title="红彩鸟之翼">红彩鸟之翼</a>*1 <font color="#008000"><b>24%</b></font><br><a href="/%E9%B8%A3%E8%A2%8B" title="鸣袋">鸣袋</a> *2 <font color="#008000"><b>20%</b></font><br><a href="/%E7%BA%A2%E5%BD%A9%E9%B8%9F%E7%9A%84%E4%B8%8A%E9%B3%9E" title="红彩鸟的上鳞">红彩鸟的上鳞</a>*2 <font color="#008000"><b>10%</b></font><br><a href="/%E6%9E%81%E5%BD%A9%E8%89%B2%E7%9A%84%E7%BE%BD%E6%AF%9B" title="极彩色的羽毛">极彩色的羽毛</a>*1 <font color="#008000"><b>10%</b></font><br><a href="/%E6%BC%82%E4%BA%AE%E7%9A%84%E5%96%99" title="漂亮的喙">漂亮的喙</a>*1 <font color="#008000"><b>9%</b></font><br><a href="/%E9%BE%99%E7%8E%89" title="龙玉">龙玉</a>*1 <font color="#008000"><b>2%</b></font> </td><td> 体力25%以下（上位）
</td></tr></tbody></table>
## 出現任務
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" rowspan="2" colspan="2"> 区分
</td><td style="text-align: center" rowspan="2"> 难度
</td><td style="text-align: center" rowspan="2"> 任务名
</td><td style="text-align: center" rowspan="2"> 任务内容
</td><td style="text-align: center" colspan="5"> 怪物体型
</td></tr>
<tr style="background:#e2e2e2;" align="center" class="atwiki_tr_odd atwiki_tr_1">
<td style="text-align: center" colspan="2">
<p>体型
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>基础
</p><p>体力
</p>
</td><td style="text-align: center">
<p>攻击力
</p><p>倍率
</p>
</td><td style="text-align: center">
<p>全体
</p><p>防御率
</p>
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_2">
<td rowspan="3"> 集会浴场
</td><td style="text-align: center" rowspan="2"> 上位
</td><td style="text-align: center"> ★7
</td><td> 在水没林绽放
</td><td style="text-align: center">讨伐一头红彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 95 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 6400
</td><td style="text-align: right"> 280
</td><td style="text-align: right"> 80
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_odd atwiki_tr_3">
<td style="text-align: center"> ★8
</td><td> 呼唤狂风！红彩鸟
</td><td style="text-align: center">讨伐一头红彩鸟
</td><td style="text-align: right"> <b><font color="#ff4"> 95 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 125 </font></b>
</td><td style="text-align: right"> 6800
</td><td style="text-align: right"> 300
</td><td style="text-align: right"> 75
</td></tr>
<tr style="background:#f9f9f9;" class="atwiki_tr_even atwiki_tr_4">
<td> 配信
</td><td style="text-align: center"> ★8
</td><td> JUMP・小兵の狂乱
</td><td style="text-align: center">
</td><td style="text-align: right"> <b><font color="#ff4"> 40 </font></b>
</td><td style="text-align: right"> <b><font color="#ff4"> 40 </font></b>
</td><td style="text-align: right"> 7400
</td><td style="text-align: right"> 380
</td><td style="text-align: right"> 70
</td></tr></tbody></table>

## 攻擊方式
<table style="background:#999999;" border="0" cellpadding="3" cellspacing="1" width="800">

<tbody><tr style="background:#e2e2e2;" align="center">
<td> <b>攻击方式</b> </td><td> <b>备注</b>
</td></tr>
<tr style="background:#f9f9f9;">
<td> 突进 </td><td> 无准备动作，向前突进一段距离
</td></tr>
<tr style="background:#f9f9f9;">
<td> 4连啄 </td><td> 用嘴连续啄四下，也能接于突进后
</td></tr>
<tr style="background:#f9f9f9;">
<td> 甩尾 </td><td> 回转身体用尾巴甩人，附带风压【小】效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 闪光 </td><td> 鸣3声后放出强光，使人陷入眩晕异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 点雷 </td><td> 向前跃出并撞击两翼的电气石，擦出出电击来攻击；最多可以连续3次，每次重新修正方向；被打中时会陷入带电异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 点雷（原地） </td><td> 原地小后跃并敲击电气石，用电击攻击；被打中时会陷入带电异常
</td></tr>
<tr style="background:#f9f9f9;">
<td> 飞行 </td><td> 向后跃起并进去低空飞行状态，飞起和降落时附带风压【小】效果
</td></tr>
<tr style="background:#f9f9f9;">
<td> 粘液球 </td><td> 同时吐出3个粘液球，方向分别是左中右
</td></tr>
<tr style="background:#f9f9f9;">
<td> 演奏 </td><td> 鼓起喉袋演奏：红波（范围内自己以外的大型怪物强制进入怒状态）；橙波（范围内硬化怪物的肉质）；绿波（范围内回复怪物的体力）
</td></tr>
<tr style="background:#f9f9f9;">
<td> 呼叫增援 </td><td> HP被打到一定程度时，呼叫其他大型怪物作为第三方战斗（主要攻击目标依然是猎人）
</td></tr></tbody></table>
