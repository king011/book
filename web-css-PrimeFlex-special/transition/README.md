# [transition-property](https://www.primefaces.org/primeflex/transitionproperty)

transition-property 指定哪些 css 屬性在變化時需要平滑的過渡

| Class | Properties |
| -------- | -------- |
| transition-none     | transition-property: none;     |
| transition-all     | transition-property: all;     |
| transition-colors     | transition-property: background-color,border-color,color;     |
| transition-transform     | transition-property: transform;     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="transition-colors transition-duration-500 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
    </div>
</div>
```


# [transition-duration](https://www.primefaces.org/primeflex/transitionduration)


transition-timing-function 指定屬性在平滑過渡所要花費的時間


| Class | Properties | 
| -------- | -------- |
| transition-duration-100     | transition-duration: 100ms;     |
| transition-duration-150     | transition-duration: 150ms;     |
| transition-duration-200     | transition-duration: 200ms;     |
| transition-duration-300     | transition-duration: 300ms;     |
| transition-duration-400     | transition-duration: 400ms;     |
| transition-duration-500     | transition-duration: 500ms;     |
| transition-duration-1000     | transition-duration: 1000ms;     |
| transition-duration-2000     | transition-duration: 2000ms;     |
| transition-duration-3000     | transition-duration: 3000ms;     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="transition-colors transition-duration-100 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-colors transition-duration-500 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-colors transition-duration-1000 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
    </div>
</div>
```

# [transition-timing-function](https://www.primefaces.org/primeflex/transitiontimingfunction)

transition-timing-function 定義過渡的速度曲線

| Class | Properties |
| -------- | -------- |
| transition-linear     | transition-timing-function: linear;     |
| transition-ease-in     | transition-timing-function: cubic-bezier(0.4, 0, 1, 1);     |
| transition-ease-out     | transition-timing-function: cubic-bezier(0, 0, 0.2, 1);     |
| transition-ease-in-out     | transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="transition-linear transition-duration-500 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-ease-in transition-duration-500 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-ease-out transition-duration-500 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-ease-in-out transition-duration-500 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
    </div>
</div>
```


# [transition-delay](https://www.primefaces.org/primeflex/transitiondelay)

transition-delay 定義延遲多長時間後開始過渡 css 屬性


| Class | Properties |
| -------- | -------- |
| transition-delay-100     | transition-delay: 100ms;     |
| transition-delay-150     | transition-delay: 150ms;     |
| transition-delay-200     | transition-delay: 200ms;     |
| transition-delay-300     | transition-delay: 300ms;     |
| transition-delay-400     | transition-delay: 400ms;     |
| transition-delay-500     | transition-delay: 500ms;     |
| transition-delay-1000     | transition-delay: 1000ms;     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="transition-delay-100 transition-colors transition-duration-300 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-delay-500 transition-colors transition-duration-300 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
        <div class="transition-delay-1000 transition-colors transition-duration-300 bg-blue-500 hover:bg-yellow-500 text-white hover:text-gray-900
    flex align-items-center justify-content-center font-bold border-round cursor-pointer m-2 px-5 py-3">Hover me</div>
    </div>
</div>
```

