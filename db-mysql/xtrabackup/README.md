# xtrabackup

xtrabackup 是一個開源的 mysql 備份還原工具 支持innodb和xtradb的熱備份 增量備份

* 官網 [https://www.percona.com/software/mysql-database/percona-xtrabackup](https://www.percona.com/software/mysql-database/percona-xtrabackup)
* 源碼 [https://github.com/percona/percona-xtrabackup](https://github.com/percona/percona-xtrabackup)
# 完整備份

```
CREATE USER 'xtrabackup'@'localhost' IDENTIFIED BY '123';
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'xtrabackup';
GRANT RELOAD, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'xtrabackup'@'localhost';
FLUSH PRIVILEGES;
```

1. 創建備份

	```
	xtrabackup --backup --host=127.0.0.1 --user=root --target-dir=/backup/$(date '+%y-%m-%d')_fullbackup
	```
	
2. 準備備份

	```
	xtrabackup --prepare --target-dir=/backup/$(date '+%y-%m-%d')_fullbackup
	```
	
3. 還原數據

	```
	xtrabackup --copy-back --target-dir=/backup/$(date '+%y-%m-%d')_fullbackup
	```
	
# 增量備份

* --incremental-basedir 參數 指定增加備份的基礎目錄

1. 創建備份

	```
	xtrabackup --backup --target-dir=/var/mariadb/backup    --user=root --password=
	xtrabackup --backup --target-dir=/var/mariadb/inc1    --incremental-basedir=/var/mariadb/backup    --user=root --password=
	xtrabackup --backup --target-dir=/var/mariadb/inc2    --incremental-basedir=/var/mariadb/inc1    --user=root --password=
	xtrabackup --backup --target-dir=/var/mariadb/inc3    --incremental-basedir=/var/mariadb/inc2    --user=root --password=
	```
	
2. 準備還原

	```
	xtrabackup --prepare --target-dir=/var/mariadb/backup
	xtrabackup --prepare --target-dir=/var/mariadb/backup    --incremental-dir=/var/mariadb/inc1
	xtrabackup --prepare --target-dir=/var/mariadb/backup    --incremental-dir=/var/mariadb/inc2
	xtrabackup --prepare --target-dir=/var/mariadb/backup    --incremental-dir=/var/mariadb/inc3
	```
	
3. 還原數據

	```
	xtrabackup --copy-back --target-dir=/var/mariadb/backup
	```