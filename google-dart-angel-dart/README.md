# angel-dart

[angel-dart](https://pub.dev/publishers/angel-dart.dev/packages) 是一個 開源的 全棧 dart web 框架

* 官網 [https://angel-dart.dev/](https://angel-dart.dev/)
* 源碼 [https://github.com/angel-dart/angel](https://github.com/angel-dart/angel)