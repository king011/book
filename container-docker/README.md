# Docker
Docker 一個 使用 go 開發的 開源(Apache2.0) 輕量 容器

* 官網 [https://www.docker.com/](https://www.docker.com/)
* 源碼 [https://github.com/docker](https://github.com/docker)
* 倉庫 [https://hub.docker.com/](https://hub.docker.com/)

# ubuntu 安裝
```sh
sudo apt update
sudo apt install -y docker.io
```