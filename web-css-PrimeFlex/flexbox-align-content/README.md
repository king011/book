# [align-content](https://www.primefaces.org/primeflex/aligncontent)

align-content 類似 justify-content 但用於定義 flex 子項在與主軸交叉的軸上如何對齊

| Class | Properties |
| -------- | -------- |
| align-content-start     | align-content: flex-start;     |
| align-content-end     | align-content: flex-end;     |
| align-content-center     | align-content: center;     |
| align-content-between     | align-content: space-between;     |
| align-content-around     | align-content: space-around;     |
| align-content-evenly     | align-content: space-evenly;     |

## align-content-start

align-content-start 使 flex 的子項位於容器交叉軸的開頭

![](assets/start.png)

```
<div class="card">
    <div class="flex align-content-start flex-wrap card-container blue-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-blue-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-content-end
align-content-end 讓 flex 的子項位於容器交叉軸的結尾

![](assets/end.png)

```
<div class="card">
    <div class="flex align-content-end flex-wrap card-container green-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-green-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-content-center

align-content-center 讓 flex 的子項位於容器交叉軸的中心

![](assets/center.png)

```
<div class="card">
    <div class="flex align-content-center flex-wrap card-container yellow-container" style="min-height: 200px">
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-yellow-500 font-bold text-gray-900 border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
    </div>
</div>
```

## align-content-between

align-content-between 使 flex 子項之間在容器交叉軸上保持間隔均勻的分佈

![](assets/between.png)

```
<div class="card">
    <div class="flex align-content-between flex-wrap card-container purple-container" style="min-height: 320px; max-width: 700px">
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">4</div>
        <div class="flex align-items-center justify-content-center bg-purple-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">5</div>
    </div>
</div>
```

## align-content-around

align-content-around 類似 align-content-between 但同時使子項和 flex 盒也保持間距

flex 子項與 flex 盒子的間距是 flex 子項與子項間 間距的 1/2

![](assets/around.png)

```
<div class="card">
    <div class="flex align-content-around flex-wrap card-container indigo-container" style="min-height: 320px; max-width: 700px">
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">4</div>
        <div class="flex align-items-center justify-content-center bg-indigo-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">5</div>
    </div>
</div>
```

## align-content-evenly

align-content-evenly 類似 align-content-around，但 flex 子項 與 子項/盒子 的間距 相等

![](assets/evenly.png)

```
<div class="card">
    <div class="flex align-content-evenly flex-wrap card-container orange-container" style="min-height: 320px; max-width: 700px">
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">1</div>
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">2</div>
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">3</div>
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">4</div>
        <div class="flex align-items-center justify-content-center bg-orange-500 font-bold text-white border-round m-2" style="min-width: 200px; min-height: 50px">5</div>
    </div>
</div>
```

# 響應式佈局
和 display 一樣這些 class 也支持響應式佈局，使用方式和 display 相同