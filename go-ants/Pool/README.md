# Pool

ants 提供了兩個 默認的 Pool 並已經 將其成員函數 都 導出作爲 包函數 除了使用 默認Pool 也可以使用 New 創建一個更加可控的 Pool

Pool 支持 以下設定
```
// Options contains all options which will be applied when instantiating a ants pool.
type Options struct {
	// ExpiryDuration is a period for the scavenger goroutine to clean up those expired workers,
	// the scavenger scans all workers every `ExpiryDuration` and clean up those workers that haven't been
	// used for more than `ExpiryDuration`.
	ExpiryDuration time.Duration

	// PreAlloc indicates whether to make memory pre-allocation when initializing Pool.
	PreAlloc bool

	// Max number of goroutine blocking on pool.Submit.
	// 0 (default value) means no such limit.
	MaxBlockingTasks int

	// When Nonblocking is true, Pool.Submit will never be blocked.
	// ErrPoolOverload will be returned when Pool.Submit cannot be done at once.
	// When Nonblocking is true, MaxBlockingTasks is inoperative.
	Nonblocking bool

	// PanicHandler is used to handle panics from each worker goroutine.
	// if nil, panics will be thrown out again from worker goroutines.
	PanicHandler func(interface{})

	// Logger is the customized logger for logging info, if it is not set,
	// default standard logger from log package is used.
	Logger Logger
}
```

```
package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/panjf2000/ants"
)

type _Logger struct {
}

func (l _Logger) Printf(format string, args ...interface{}) {
	fmt.Printf(format, args...)
}
func main() {
	p, e := ants.NewPool(
		5,                                    //math.MaxUint32, //設置 工作的 goroutine 上限
		ants.WithExpiryDuration(time.Second), // 設置多長時間 清理一次 空閒 goroutine
		ants.WithPreAlloc(false),             // 如果爲 true 爲在 New時 創建好 工作 goroutine 需要的內存 否則 安需創建
		ants.WithMaxBlockingTasks(0),         // 設置 達到 goroutine 上限後 Submit 最多阻塞多少個 超過的 Submit 將返回 錯誤
		ants.WithNonblocking(false),          // 設置是否爲非阻塞模式，阻塞模式下 如果達到goroutine數量上限 將 阻塞Submit，非阻塞模式會直接返回錯誤
		ants.WithPanicHandler(func(msg interface{}) {
			fmt.Println(msg)
		}), // 設置 panic 處理回調
		ants.WithLogger(_Logger{}), // 設置 日誌
	)
	if e != nil {
		log.Fatalln(e)
	}

	var wait sync.WaitGroup
	wait.Add(6)
	for i := 0; i < 6; i++ {
		// 提交一個 任務給 goroutine 執行
		func(i int) {
			p.Submit(func() {
				fmt.Println(`submit`, i)
				time.Sleep(time.Second)
				wait.Done()
			})
		}(i)
	}
	wait.Wait()

	p.Submit(func() {
		panic(`painc test`)
	})
	time.Sleep(time.Second)
}
```