# 伺服器配置

* /etc/exports -> nfs 主要設定檔
* /usr/sbin/exportfs -> 維護 NFS 分享資源的指令，可以利用這個指令重寫分享 /etc/exports 變更的目錄資源，將 NFS Server 分享的目錄卸載或重新分享等
* /var/lib/nfs/\*tab -> nfs 伺服器登入檔都放在 /var/lib/nfs/ 目錄下。該目錄下 etab 記錄了 nfs 所分享出來的目錄的完整權限設定值；xtab 則記錄曾經連接到此 nfs 伺服器的相關用戶端資料。
* /usr/sbin/showmount -> 客戶端用到的 nfs 命令，可以用來觀察 nfs 分享出來的目錄資源

# /etc/exports

**/etc/exports** 是核心設定檔

```
#info="/etc/exports"
# 分享目錄	[第一部主機]			[可用主機名]			[可用域名]
/tmp         192.168.100.0/24(ro)   localhost(rw)   *.ev.ncku.edu.tw(ro,sync)
```

設定檔 每行一個配置可以使用 **#** 添加註解

* 第一列是要分享的目錄
* 後續可選列都是 允許分享的主機名稱和權限，主機名稱可以使用兩種方式指定
	* 完整的ip或網域，例如 192.168.100.10 或 192.168.100.0/24 或 192.168.100.0/255.255.255.0
	* 也可以使用主機名稱，但主機名必須在 /etc/hosts內，或者可以使用 DNS 找到該名稱才行。對於主機名可以支持 * 和 ? 通配符 

| 權限取值 | 含義 | 
| -------- | -------- |
| rw ro     | 該目錄分享的權限是可讀寫(read-write)或只讀(read-only),最終是否可讀寫，還與檔案系統 rwx 及身份有關     |
| sync async     | sync 代表資料同步寫入內存和硬盤中，async 則代表資料會向暫存與內存中，而非直接寫入硬盤    | 
| no\_root\_squash root\_squash     | 客戶端如果身份爲 root,默認處理方式是root\_squash 既將身份修改爲 nfsnobody，如果設置 no\_root\_squash 則會保持 root 身份     | 
| all\_squash     | 無論登入者是何身份偶，都作爲 匿名訪問者，nobody 或 nfsnobody     | 
| anonuid anongid     | anonuid 指定訪問者的 uid，anongid指定訪問者的 gid     | 

## 讓 root 保有 root 權限

```
# vim /etc/exports
# 任何人都可以用我的 /tmp ，用萬用字元來處理主機名稱，重點在 no_root_squash
/tmp  *(rw,no_root_squash)
```

## 同一目錄對不同範圍開放不同權限

192.168.100.0/24 這個網域且加入 vbirdgroup (第一章的例題建立的群組) 的用戶才能夠讀寫，其他來源則只能讀取。

```
[root@www ~]# mkdir /home/public
[root@www ~]# setfacl -m g:vbirdgroup:rwx /home/public
[root@www ~]# vim /etc/exports
/tmp          *(rw,no_root_squash)
/home/public  192.168.100.0/24(rw)    *(ro)
```
## 僅給某個單一主機使用的目錄設定

將一個私人的目錄 /home/test 開放給 192.168.100.10 且使用者的身份是 dmtsai 才具有完整的權限時。

```
[root@www ~]# mkdir /home/test
[root@www ~]# setfacl -m u:dmtsai:rwx /home/test
[root@www ~]# vim /etc/exports
/tmp          *(rw,no_root_squash)
/home/public  192.168.100.0/24(rw)    *(ro)
/home/test    192.168.100.10(rw)
# 只要設定 IP 正確即可！
```

## 開放匿名登入的情況

讓 \*.centos.vbird 網域的主機，登入我的 NFS 主機時，可以存取 /home/linux ，但是他們存資料的時候，我希望他們的 UID 與 GID 都變成 45 這個身份的使用者

```

[root@www ~]# groupadd -g 45 nfsanon
[root@www ~]# useradd -u 45 -g nfsanon nfsanon
[root@www ~]# mkdir /home/linux
[root@www ~]# setfacl -m u:nfsanon:rwx /home/linux
[root@www ~]# vim /etc/exports
/tmp          *(rw,no_root_squash)
/home/public  192.168.100.0/24(rw)    *(ro)
/home/test    192.168.100.10(rw)
/home/linux   *.centos.vbird(rw,all_squash,anonuid=45,anongid=45)
# 如果要開放匿名，那麼重點是 all_squash，並且要配合 anonuid 喔！
```

# 連線觀察

設置好 nfs 後 可以使用 showmount 指令來觀察 伺服器是否設置妥當

```
[root@www ~]# showmount [-ae] [hostname|IP]
選項與參數：
-a ：顯示目前主機與用戶端的 NFS 連線分享的狀態；
-e ：顯示某部主機的 /etc/exports 所分享的目錄資料。

# 1. 請顯示出剛剛我們所設定好的相關 exports 分享目錄資訊
[root@www ~]# showmount -e localhost
Export list for localhost:
/tmp         *
/home/linux  *.centos.vbird
/home/test   192.168.100.10
/home/public (everyone)
```

具體的權限設定則需要查看 **/var/lib/nfs/etab** 檔案

```
[root@www ~]# tail /var/lib/nfs/etab
/home/public    192.168.100.0/24(rw,sync,wdelay,hide,nocrossmnt,secure,root_squash,
no_all_squash,no_subtree_check,secure_locks,acl,anonuid=65534,anongid=65534)
# 上面是同一行，可以看出除了 rw, sync, root_squash 等等，
# 其實還有 anonuid 及 anongid 等等的設定！
```

當重新編輯了 **/etc/exports** 檔案可以使用 **exportfs** 指令來重載 nfs 服務

```
[root@www ~]# exportfs [-aruv]
選項與參數：
-a ：全部掛載(或卸載) /etc/exports 檔案內的設定
-r ：重新掛載 /etc/exports 裡面的設定，此外，亦同步更新 /etc/exports
     及 /var/lib/nfs/xtab 的內容！
-u ：卸載某一目錄
-v ：在 export 的時候，將分享的目錄顯示到螢幕上！

# 1. 重新掛載一次 /etc/exports 的設定
[root@www ~]# exportfs -arv
exporting 192.168.100.10:/home/test
exporting 192.168.100.0/24:/home/public
exporting *.centos.vbird:/home/linux
exporting *:/home/public
exporting *:/tmp

# 2. 將已經分享的 NFS 目錄資源，通通都卸載
[root@www ~]# exportfs -auv
# 這時如果你再使用 showmount -e localhost 就會看不到任何資源了！
```

# 端口設置

* NFS 使用衆所周知的 2049 端口
* portmapper 也使用衆所周知的 111 端口
* Network Lock Manager,Mount,Statd,rquotad 則使用 臨時端口

Network Lock Manager 是內核啓動時加載的模塊 故修改後需要重啓系統才能生效

使用 rpcinfo 指令查看 nfs 使用的 端口
```
rpcinfo -p |egrep "(portmapper|nfs|mountd|rquotad|status|nlockmgr)"
```

1. 設置 nlockmgr 監聽在 50006 的 tcp 和 udp 端口

	```
	#info="/etc/modprobe.d/nfs-lockd.conf"
	options lockd nlm_tcpport=50006
	options lockd nlm_udpport=50006
	```

1.  設置 statd 監聽 50000 發送到 50001

	```
	#info="/etc/default/nfs-common"
	STATDOPTS="--port 50000 --outgoing-port 50001"
	```
	
1. 設置 rpc.mountd 工作在 50003 端口

	```
	#info="/etc/default/nfs-kernel-server"
	RPCMOUNTDOPTS="--manage-gids --port 50003"
	```
	
1. 設置 quota

	```
	#info="/etc/default/quota"
	RPCRQUOTADOPTS="-p 50004"
	```
	
1. 增加防火牆規則

	```
	sudo iptables -A INPUT -p tcp -m state --state NEW -m multiport --dport 111,2049,50003,50006 -j ACCEPT
	sudo iptables -A INPUT -p udp -m state --state NEW -m multiport --dport 111,2049,50003,50006 -j ACCEPT
	```