# [Work Queues](https://www.rabbitmq.com/tutorials/tutorial-two-go.html)

![](assets/python-two.webp)

工作隊列 有生產者將 消息 投入隊列 多個消費者 從隊列中取出數據進行 消費

* 上圖只有一個生產者 但實際可以有多個 但 多個生產者  間的消息順序無法保證
* 上圖也只有兩個消費者 但實際當然可以有更多個消費者


只需要 將 [Hellow World](server-RabbitMQ/Hello-World) 中的代碼 調用多次 **consumer** 函數  創建多個 消費者 即可模擬 工作隊列

```
package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-two`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	go consumer(c, "one")
	consumer(c, "two")
}
func consumer(c *amqp.Connection, tag string) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		true, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	for msg := range ch {
		v, e0 := strconv.Atoi(string(msg.Body))
		fmt.Println(tag, `recv <-`, v, e0)
	}
}
```
> 上述代碼中第15行改變了 隊列名 生產者也需要改變到同名隊列

# Qos ACK

當存在多個 消費者時 需要確認 RabbitMQ 如何向多個消費者 派發消息 Qos 用來做控制

每個消息 都需要 消費者 使用 ACK 回覆 如果設置爲手動回覆 則需要在 消息處理完時 手動調用 ACK

默認情況下 RabbitMQ 使用 公平分發 RabbitMQ 會依次給生產者發送消息

消費者可以使用 Qos(1,0,false) 設置 服務器允許的此消費者 最多積壓多少個未確認的 消息 保證 消費者 ACK後 存在允許的空餘積壓ACK 才會繼續向 消費者 派發消息

```
#info="consumer"
package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-two`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	go consumer(c, "one", time.Millisecond*200)
	consumer(c, "two", time.Millisecond*100)
}
func consumer(c *amqp.Connection, tag string, wait time.Duration) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}
	// 設置每次 派發一個 消息 並且等待 ACK後才 繼續向此消費者 發消息
	e = channel.Qos(1, // 每次確認消息數量
		0, // 每次確認消息大小
		false,
	)
	if e != nil {
		log.Println(e)
		return
	}
	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		false, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	num := 0
	for msg := range ch {
		v, e0 := strconv.Atoi(string(msg.Body))
		num++
		fmt.Println(tag, `recv <-`, v, e0, num)
		time.Sleep(wait)
		msg.Ack(false)
	}
}
```

> 上面例子 第57行 設定了 每次 收取一個消息 並且發送 ack後 才繼續收消息
>
> 上面例子 消費者 分別 延遲了 100ms 200ms 回報 ack 在運行平穩時 延遲100ms收到的消息數量會比200ms 多2倍 如果未設置 qos則幾乎會收到一樣多的消息


# ACK Reject Nack

對於手動 回覆的消息 消費者 必須 回覆 ACK/Reject/Nack 

* ACK 消息 已經確認 並通過處理 服務器可以將其 刪除
* Reject 消息 已經否定確認 並沒有被允許處理 服務器可以將其 刪除
* Nack 是 Reject 的擴展 做出來 批量確認 (nack 是 ampq 0.9.1 擴展支持的 使用時需要確認 RabbitMQ版本是否支持)

ACK/Nack 的 multiple 參數如果爲 true 則 執行 批量確認  比如 有 消息 5，6，7，8

* 7號消息 使用 Ack(true) 則 5，6，7 消息 都會被確認 Nack(true,false) 會否定確認 5，6，7
*  Ack(false) Nack(false,false) 只會確認 7 號消息


reject nack 的 requeue 參數 如果爲false 服務器 會將此消息 刪除 如果爲 true 則會 儘量將消息 放回隊列原位置 以便其它消費者 可以處理


# 持久化

對於沒有返回 ACK 的消息當 消費者 斷開連接時 RabbitMQ 會重發 此消息 但如果RabbitMQ重啓或崩潰了 此消息依然會 丟失 持久是一個解決辦法

要使消息 持久化 需要

1. QueueDeclare 聲明隊列時 指示 durable 參數爲true
2. 生產者發佈消息時 設置消息 DeliveryMode 爲 amqp.Persistent

```
package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-two`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		true,  // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {
		var deliveryMode uint8
		if i%2 == 0 {
			deliveryMode = amqp.Transient // 0 or 1 臨時消息
		} else {
			deliveryMode = amqp.Persistent // 持久化消息
		}
		// 發佈消息
		e = channel.Publish(
			``,
			q.Name,
			false,
			false,
			amqp.Publishing{
				DeliveryMode: deliveryMode,
				ContentType:  "text/plain",
				Body:         []byte(strconv.Itoa(i)), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, i)
		} else {
			log.Println(e)
			break
		}
		time.Sleep(time.Millisecond * 100)
	}
}
```

durable 指示告訴 RabbitMQ 將消息保存到磁盤 但 RabbitMQ 不會對每個消息 都設置 fsync 並且可能在 接收到消息未寫入磁盤時 崩潰 雖然概率很小 但此時消息將被丟失 

通常對於應用使用durable就綽綽有餘了 但如果要解決此問題 需要使用 [Publisher Confirms](server-RabbitMQ/Publisher-Confirms)