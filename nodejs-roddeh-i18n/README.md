# roddeh-i18n

roddeh-i18n 一個開源(MIT)的 js i18n 組件

```
npm install roddeh-i18n
```

* 官網 [https://www.npmjs.com/package/roddeh-i18n](https://www.npmjs.com/package/roddeh-i18n)
* 源碼 [https://github.com/roddeh/i18njs](https://github.com/roddeh/i18njs)