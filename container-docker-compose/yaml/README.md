# [模板](https://docs.docker.com/compose/compose-file/)

compose 模板默認名稱爲 docker-compose.yml

```
version: '1'
services:
  main:
    image: wordpress
    restart: always
    ports:
      - 9000:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_NAME: data
      WORDPRESS_DB_USER: king
      WORDPRESS_DB_PASSWORD: 12345678
    volumes:
      - ./data/main:/var/www/html
  db:
    image: mariadb:10.8
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 123
      MYSQL_DATABASE: data
      MYSQL_USER: king
      MYSQL_PASSWORD: 12345678
    volumes:
      - ./data/db:/var/lib/mysql
```
# [version](https://docs.docker.com/compose/compose-file/compose-versioning/)
compose 檔案的具體版本，不同的版本涉及到了 docker 的最低版本要求以及提供的功能

* [3](https://docs.docker.com/compose/compose-file/compose-versioning/#version-3)
* [2](https://docs.docker.com/compose/compose-file/compose-versioning/#version-2)
* [1](https://docs.docker.com/compose/compose-file/compose-versioning/#version-1-deprecated) 這個以及被棄用

# services

services 下的每個屬性都是一個獨立的 服務
# image

指定容器使用的 鏡像名稱，image 和 build 必須有一個被設置爲有效值

# build
build 用於指定 Dockerfile 檔案位置(可以是絕對路徑或者相對 docker-compose.yml 的路徑)

```
version: '3'
services:

  webapp:
    build: ./dir
```

也可以使用 
* context 指定 Dockerfile 所在檔案夾
* dockerfile 指定 Dockerfile 檔案名稱
* arg 指定 build 變量

```
version: '3'
services:

  webapp:
    build:
      context: ./dir
      dockerfile: Dockerfile-alternate
      args:
        buildno: 1
```

# cap\_add cap\_drop

cap_add指定容器內核能力(capacity)

```
#info="用於所有能力"
cap_add:
  - ALL
```

```
#info="去掉 NET_ADMIN"

cap_drop:
  - NET_ADMIN
```

# ports

暴露到宿主機的端口，如果不指定宿主機端口宿主機將使用隨機端口

```
ports:
 - "3000"
 - "8000:8000"
 - "49100:22"
 - "127.0.0.1:8001:8001"
```

> YAML 在解析 **xx:yy** 這種數字格式時如果 yy  小於 60 會自動解析爲60進制既 (xx\*60+yy) 所以如果容器端口 小於60 需要使引號將值擴起來明確表示字符串

# environment

設置容器環境變量

如果變量名稱或值中用到 true false yes no 等表示布爾的詞彙，最好放到引號中，避免 YAML 自動解析內容爲布爾。這些詞彙包括：

```
y|Y|yes|Yes|YES|n|N|no|No|NO|true|True|TRUE|false|False|FALSE|on|On|ON|off|Off|OFF
```

# env\_file

通過檔案來指定環境變量

```
env_file:
  - ./common.env
  - ./apps/web.env
  - /opt/secrets.env
```

檔案中每行一個變量，支持以 **#** 開頭註釋行
```
# common.env: Set development environment
PROG_ENV=development
```

# sysctls

配置容器內核參數

```
sysctls:
  net.core.somaxconn: 1024
  net.ipv4.tcp_syncookies: 0

sysctls:
  - net.core.somaxconn=1024
  - net.ipv4.tcp_syncookies=0
```

# ulimits

配置 ulimits

```
  ulimits:
    nproc: 65535
    nofile:
      soft: 20000
      hard: 40000
```

上面例子中，最大進程數爲 65535，文件句柄軟限制爲 20000 硬限制爲 40000

# volumes

配置掛接的卷

支持相對路徑和絕對路徑

```
volumes:
 - /var/lib/mysql
 - cache/:/tmp/cache
 - ~/configs:/etc/configs/:ro
```

如果路徑爲卷名稱，則必須在頂層 volumes 中 配置卷
```
version: "3"

services:
  my_src:
    image: mysql:8.0
    volumes:
      - mysql_data:/var/lib/mysql

volumes:
  mysql_data:  
```

# 讀取變量

compose 模板支持讀取系統變量以及 docker-compose.yml 同目錄下 .env 檔案中的 變量，此外可以設置變量 **COMPOSE\_PROJECT\_NAME** 來覆默認的 project name

```
version: "3"
services:

db:
  image: "mongo:${MONGO_VERSION}"
```

```
#info=".env"
# 支持 # 号注释
MONGO_VERSION=3.6
```