# js-xlsx

js-xlsx 是一個 開源(Apache-2.0)的 javascript xlsx 庫 可以在 node 或 瀏覽器等環境 使用 js 讀寫 xlsx

```
npm i xlsx
```

* 官網 [https://sheetjs.com/](https://sheetjs.com/)
* npm [https://www.npmjs.com/package/xlsx](https://www.npmjs.com/package/xlsx) 
* 源碼 [https://github.com/SheetJS/sheetjs](https://github.com/SheetJS/sheetjs)