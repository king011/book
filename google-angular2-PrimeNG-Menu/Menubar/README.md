# [Menubar](https://primeng.org/menubar)

```
import {MenubarModule} from 'primeng/menubar';
import {MenuItem} from 'primeng/api';
```

Menubar 是一個水平菜單組件

![](assets/primeng.org_menubar.png)

```
<p-menubar [model]="items"></p-menubar>
```

```
export class MenubarDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'File',
                items: [{
                        label: 'New', 
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {label: 'Project'},
                            {label: 'Other'},
                        ]
                    },
                    {label: 'Open'},
                    {label: 'Quit'}
                ]
            },
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-trash'},
                    {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
                ]
            }
        ];
    }
}
```

## Custom Content

自定義內容可以放在 **p-menubar** 標籤之間

```
<p-menubar [model]="items">
    <input type="text" pInputText placeholder="Search">
    <button pButton label="Logout" icon="pi pi-power-off"></button>
</p-menubar>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| model     | array     | null     | 菜單數組     |
| style     | string     | null     | 內聯的組件 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| baseZIndex     | number     | 0     | 分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| autoDisplay     | boolean     | false     | 鼠標懸停時是否顯示根子菜單     |
| autoHide     | boolean     | false     | 鼠標離開時是否隱藏根子菜單     |
| autoHideDelay     | number     | 100     | 鼠標離開時延遲多少毫秒隱藏根子菜單     |

# Templates

| Name | Parameters |
| -------- | -------- |
| start     | -     |
| end     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-menubar     | Container element.     |
| p-menu-list     | List element.     |
| p-menuitem     | Menuitem element.     |
| p-menuitem-text     | Label of a menuitem.     |
| p-menuitem-icon     | Icon of a menuitem.     |
| p-submenu-icon     | Arrow icon of a submenu.     |

