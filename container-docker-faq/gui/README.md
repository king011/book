# 運行 gui 程式

linux 本身是沒有圖形界面的。圖形界面基於 X 協議實現的

X 協議由 X server 和 X client 組成

* X server 管理主機上與顯示相關的硬件(顯卡 鼠標 鍵盤 ...)，它負責屏幕的繪製與顯示，以及將輸入設置(鍵盤 鼠標 ...)的動作告知 X client
* X client 則主要負責事件的處理

比如當用戶點擊了鼠標，X server 會捕獲到這個動作，然後將此動作告訴 X client。X cleint 負責事件邏輯，於是 X client 就根據程式代碼執行響應處理(比如畫一個圓)。因爲繪製是 X server 處理，所以 X client 將繪製告訴 X server，最後 X server 就在鼠標點擊出繪製出一個圓。

![](assets/X.jpeg)

雖然 X server 和 X client 通常位於同一設備，但依據其原理完全可以運行在不同設備，比如遠程服務器+本地顯示器，或者 顯示器+docker 容器
# X11

X11 是 X 協議的第十一個版本，也是當前(2022年) linux 使用的版本

默認情況下，X11的 server 會監聽本地的 **unix:0** 端口，則 **DISPLAY** 默認值爲 **:0**，這實際是 **unix:0** 的簡寫。因此 linux 控制檯啓動一個圖形程式，它就會出現在當前主機的顯示屏幕中。基於此在 linux 下的 docker 可以運行 docker 容器中的圖形程式

1. 使用 xhost 指令 允許 docker 主機連接 X server

	```
	sudo apt install x11-xserver-utils
	```
	
	```
	xhost + local:
	```
	
	**\+ local:** 僅允許通過本地連接到 X server，**+** 則可以運行任何地址連接 X server
	
	```
	xhost +
	```

2. 安裝一個帶圖形程式的容器比如下面這個用於測試的容器

	```
	#info="Dockerfile"
	FROM ubuntu:18.04
	RUN set -eux; \
			apt-get update; \
			apt-get install -y x11-apps; \
			rm -rf /var/lib/apt/lists/*;
	CMD xclock
	```
	```
	docker build -t xclock:18.04 .
	```

3. 因爲每個 unix socket 實際上就是系統 /tmp/.X11-unix 檔案夾下依據 socket 編號命名的一個特殊檔案，所以使用 -v 讓容器和主機共享 X11 的 unix socket

	```
	docker run \
			-it --rm  \
			-v /tmp/.X11-unix:/tmp/.X11-unix \
			-e DISPLAY=$DISPLAY \
			xclock:18.04
	```
	
	如果 docker 容器中的圖形程序需要訪問受限的硬件資源需要假設 **--privileged** 參數讓容器獲得真實的 root 權限，更多詳情可以查看 docker 使用說明

	```
	docker run \
			-it --rm  \
			--privileged \
			-v /tmp/.X11-unix:/tmp/.X11-unix \
			-e DISPLAY=$DISPLAY \
			xclock:18.04
	```
