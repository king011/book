# 加載和切換場景

cscos 同時 只能運行 一個 場景 調用 cc.director.loadScene 傳入場景名 即可 加載場景

```ts
// loadScene(sceneName: string, onLaunched?: Function): boolean
cc.director.loadScene("MyScene");
```

sceneName 是場景資源 相對 assets/Scene 的路徑名稱 (不包含後綴名)

# 常駐節點和場景數據共享

cscos 同時只能 運行一個場景 當場景切換時 會銷毀 場景內所有節點

此時 將一個 節點 設置爲 常駐節點 則此節點 不會被銷毀 

```
// 添加一個常駐節點
cc.game.addPersistRootNode(node)

// 取消常駐節點
cc.game.removePersistRootNode(node)
```

> removePersistRootNode 不會 銷毀 節點 只是將其設置爲 非常駐節點
> 

可以通過 常駐節點 在場景間 共享 數據 此外亦可以通過 全局變量 ts模塊 實現數據共享

# 場景回調

加載場景 時 可以 傳遞一個ie回調函數 在 加載成功後 回調 通常可以在此 設置 常駐節點 在常駐節點上掛接腳本

```
cc.director.loadScene("MyScene", onSceneLaunched)
```

# 預加載場景

使用 cc.director.loadScene 會在場景加載成功後 自動切換到新 場景

有時 我們需要在後臺 靜默加載新場景 並在加載成功後 手動切換 可以使用 preloadScene 進行 預加載

```
cc.director.preloadScene("table", function () {
    cc.log("Next scene preloaded")
});
```

之後使用 loadScene 切換場景

```
cc.director.loadScene("table")
```

> 即使 preloadScene 沒有完成加載 也可以調用 loadScene 
> 
> loadScene 會在preloadScene 成功後 切換到新場景
>  


