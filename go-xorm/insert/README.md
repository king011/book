# 插入數據

xorm 提供了 Insert 用於插入數據 可以傳入 一個或 多個 struct 指針，如果傳入的是 Slice 並且當數據庫支持批量插入時 Insert 會使用批量插入方式進行插入

* 插入一條數據 可以使用 Insert 或 InsertOne

	```
	user := new(User)
	user.Name = "myname"
	affected, err := engine.Insert(user)
	// INSERT INTO user (name) values (?)
	```
	
	在插入單條數據成功後 如果struct存在自增字段(autoincr) 則字段會被自動賦值爲數據庫中的 id。如果該字段已經被賦值 Insert 會作爲非自增字段插入

* 插入同一個表的多條數據 此時如果數據庫支持批量插入 那麼會進行批量插入 但這樣每條記錄無法被自動賦予 id 值。如果數據庫不支持批量插入 會一條一條插入

	```
	users := make([]User, 1)
	users[0].Name = "name0"
	...
	affected, err := engine.Insert(&users)
	```
	
* 使用指針 Slice 插入 多條記錄

	```
	users := make([]*User, 1)
	users[0] = new(User)
	users[0].Name = "name0"
	...
	affected, err := engine.Insert(&users)
	```

* 插入多條記錄並且不使用批量插入，此時實際會生成多條插入語句，每條均會自動賦值 id

	```
	users := make([]*User, 1)
	users[0] = new(User)
	users[0].Name = "name0"
	...
	affected, err := engine.Insert(users...)
	```

* 插入不同表的一條記錄

	```
	user := new(User)
	user.Name = "myname"
	question := new(Question)
	question.Content = "whywhywhwy?"
	affected, err := engine.Insert(user, question)
	```
	
* 插入不同表的多條記錄

	```
	users := make([]User, 1)
	users[0].Name = "name0"
	...
	questions := make([]Question, 1)
	questions[0].Content = "whywhywhwy?"
	affected, err := engine.Insert(&users, &questions)
	```
	
* 插入不同表的一條或多條記錄

	```
	user := new(User)
	user.Name = "myname"
	...
	questions := make([]Question, 1)
	questions[0].Content = "whywhywhwy?"
	affected, err := engine.Insert(user, &questions)
	```
	
1. 雖然支持同時插入 但這些插入並沒有事務關係。因此可能中間插入出錯，後續插入將不會繼續，已插入數據也不會回滾
2. 批量插入會自動生成 `Insert into table values (),(),()` 語句，因此各個數據庫對SQL語句長度有限制太長可能導致執行失敗。經驗表示通常不要超過150條記錄

# 創建時間 created

xorm 支持用 created tag 將在 插入數據時自動設置爲 當前時間 字段型別可以是 time.Time int in64 等

```
type User struct {
    Id int64
    Name string
    CreatedAt time.Time `xorm:"created"`
}
```
```
type JsonTime time.Time
func (j JsonTime) MarshalJSON() ([]byte, error) {
    return []byte(`"`+time.Time(j).Format("2006-01-02 15:04:05")+`"`), nil
}

type User struct {
    Id int64
    Name string
    CreatedAt JsonTime `xorm:"created"`
}
```
```
type User struct {
    Id int64
    Name string
    CreatedAt int64 `xorm:"created"`
}
```

在 Insert 或 InsertOne 時，created tag 的字段將會被自動更新爲當前時間或當前時間的秒數(time.Unix())

```
var user User
engine.Insert(&user)
// INSERT user (created...) VALUES (?...)
```

最後 xorm 默認使用 Local 時區，所以莫熱調用 time.Now() 會先被轉換成對應時區。要改變 xorm 時區 可以適應:
```
engine.TZLocation, _ = time.LoadLocation("Asia/Shanghai")
```