# rxdart
```
dependencies:
  rxdart: ^0.21.0
```

[rxdart](https://pub.dartlang.org/packages/rxdart) 封裝了內置的 Stream/StreamController 來實現了 ReactiveX 

* Observable 派生自 Stream
* Subject 派生自 StreamController

Observable/Subject 完全符合 Stream/StreamController 標準 只是 同時 提供了對 ReactiveX 的 實現

可以直接使用 Observable/Subject 替代 Stream/StreamController

# Observable
* Stream 在遇到 error 時不會關閉 stream
* Observable 在遇到 error 時不會關閉 stream

```dart
import 'package:rxdart/rxdart.dart';

main() {
  var observable =
      Observable.fromIterable([1, 2, 3, 4, 5]).interval(Duration(seconds: 1));
  //observable.listen((v) => print("$v"));
  () async {
    await for (var v in observable) {
      print("$v");
    }
  }();
}
```

# PublishSubject

Subject 是 abstract class

PublishSubject 是 Subject 的 實現

```dart
import 'package:rxdart/rxdart.dart';

main() {
  var subject = PublishSubject<int>();
  // 沒有聽衆 這個值 不會被任何人接收 所以就是 丟失了
  subject.add(50);

  // 註冊聽總
  read(subject, "one");
  subject.add(80); // one 會接收到此值 後續 聽總不會等待 此值
  read(subject, "two");

  // 向聽衆廣播
  write(subject);
}

write(Subject<int> subject) async {
  var observable = Observable.fromIterable([100, 101, 102, 103, 104])
      .interval(Duration(milliseconds: 100));
  await for (var v in observable) {
    subject.add(v);
  }

  // 關閉 主題 通知 所有 聽衆 結束
  subject.close();
}

read(Subject<int> subject, String name) async {
  var i = 0;
  await for (var v in subject) {
    print("$name ${i++}=$v");
  }
  print("$name exit");
}
```

```
one 0=80
one 1=100
two 0=100
one 2=101
two 1=101
one 3=102
two 2=102
one 4=103
two 3=103
one 5=104
two 4=104
one exit
two exit
```

# BehaviorSubject

BehaviorSubject 類似 PublishSubject 不過 PublishSubject 會緩存最近一次的 廣播值 當新聽衆加入時 會將最近的 廣播值 傳遞給 新聽衆

```dart
import 'package:rxdart/rxdart.dart';

main() {
  var subject = BehaviorSubject<int>();
  // one 會 得到此值 因爲被 緩存下來
  // two 不會得到 因爲 緩存 被 add(80) 覆蓋了
  subject.add(50);

  // 註冊聽總
  read(subject, "one");
  subject.add(80); // two 會 得到此值 因爲被 緩存下來
  read(subject, "two");

  // 向聽衆廣播
  write(subject);
}

write(Subject<int> subject) async {
  var observable = Observable.fromIterable([100, 101, 102, 103, 104])
      .interval(Duration(milliseconds: 100));
  await for (var v in observable) {
    subject.add(v);
  }

  // 關閉 主題 通知 所有 聽衆 結束
  subject.close();
}

read(Subject<int> subject, String name) async {
  var i = 0;
  await for (var v in subject) {
    print("$name ${i++}=$v");
  }
  print("$name exit");
}
```

```
#info=false
one 0=50
one 1=80
two 0=80
one 2=100
two 1=100
one 3=101
two 2=101
one 4=102
two 3=102
one 5=103
two 4=103
one 6=104
two 5=104
one exit
two exit
```

# ReplaySubject
BehaviorSubject 只會緩存 最近的 廣播值

ReplaySubject 可以將所有廣播數據都 緩存下來 以便 後續 聽衆 不會錯過 任何 信息

ReplaySubject 提供一個 可選參數 maxSize 設置 最多緩存的 數據 而不是 默認的緩存全部 **通常你應該設置 一個合理的 緩存大小**

```dart
import 'package:rxdart/rxdart.dart';

main() {
  var subject = ReplaySubject<int>(maxSize: 2);

  subject.add(50);

  // 註冊聽總
  read(subject, "one");
  subject.add(80);
  read(subject, "two");

  // 向聽衆廣播
  write(subject);
}

write(Subject<int> subject) async {
  var observable = Observable.fromIterable([100, 101, 102, 103, 104])
      .interval(Duration(milliseconds: 100));
  await for (var v in observable) {
    subject.add(v);
  }

  // 關閉 主題 通知 所有 聽衆 結束
  subject.close();
}

read(Subject<int> subject, String name) async {
  var i = 0;
  await for (var v in subject) {
    print("$name ${i++}=$v");
  }
  print("$name exit");
}
```

```
one 0=50
one 1=80
two 0=50
two 1=80
one 2=100
two 2=100
one 3=101
two 3=101
one 4=102
two 4=102
one 5=103
two 5=103
one 6=104
two 6=104
onCancel
one exit
two exit
```