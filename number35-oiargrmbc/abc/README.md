# 字根

倉頡輸入法 由 26個字根組成 和 26個英文字母 對應

![](assets/base.png)

## 哲學類

* 日 A
* 月 B
* 金 C
* 木 D
* 水 E
* 火 F
* 土 G

## 筆畫類

* 竹 H
* 戈 I
* 十 J
* 大 K
* 中 L
* 一 M
* 弓 N

## 人體類

* 人 O
* 心 P
* 手 Q
* 口 R

## 字形類

* 尸 S
* 廿 T
* 山 U
* 女 V
* 田 W
* 卜 Y

## 特殊鍵

* 難/重 X
* 片/造 Z

# 鍵盤排列

![](assets/Keyboard_layout_cangjie.png)

![](assets/keyboard.jpeg)