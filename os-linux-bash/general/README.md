# 腳本 路徑 名稱

```
#!/bin/bash
echo "腳本路徑	= $BASH_SOURCE"
name=`basename "$BASH_SOURCE"`
echo "腳本名稱	= $name"
dir=`cd "$(dirname "$BASH_SOURCE")" && pwd`
echo "腳本檔案夾	= $dir"
echo "工作目錄	= `pwd`"
```

# 解析參數

bash 內置了 getopts 用於解析參數 

```
getopts [option[:]] VARIABLE
```

option 指定要解析的參數 只能接收 短命令的參數 如果帶上 : 則可接收一個 字符串 存儲到 OPTARG 變量 否則爲 bool 參數

```
#!/usr/bin/env bash

while getopts "ha:" opt
do
    case $opt in
      h)
        echo help
        exit 0
      ;;

      a)
        echo "a = $OPTARG"
      ;;

      ?)
        exit 1
    esac
done
```

有一些參數在 linux 存在習慣性的含義 通常應該儘量保持這種一致的習慣

* -a 顯示所有對象
* -c 生產一個計數
* -d 指定一個檔案夾
* -e 擴展一個對象
* -f 指示讀入數據的檔案
* -h 顯示命令幫助消息
* -i 忽略文本大小寫
* -l 產生輸出長格式文本
* -n 使用非交互模式
* -o 指定將所有輸出重定向到輸出檔案
* -q 以安靜模式運行
* -r 遞歸處理所有檔案和檔案夾
* -s 以安靜模式運行
* -v 生成詳細輸出
* -x 排除某個對象
* -y 對所有問題回答 yes

## getopt

* getopt 是一個外部命令，通常linux發行版會自帶，用於提供比 getopts 更強大的 功能
* getopt 支持短選項和長選項

```
#!/bin/bash

echo original parameters=[$@]

# -o/--options 後設置短選項 如ab:c:: 表示接受短選項爲-a -b -c
# 其中 -a 選項不接受參數 -b选项後必須有參數 -c 選項後的參數是可選的
# -l/--long 後設置可接受的長選項 使用逗號分隔 冒號含義同短選項
# -n 選項後 接選項解析出錯時提示的 腳本名字
ARGS=`getopt -o ab:c:: --long along,blong:,clong:: -n "$0" -- "$@"`
if [ $? != 0 ]; then
    echo "Terminating..."
    exit 1
fi

echo ARGS=[$ARGS]
# 將規範後的參數分配到位置參數($1,$2,...)
eval set -- "${ARGS}"
echo formatted parameters=[$@]

while true
do
    case "$1" in
        -a|--along) 
            echo "Option a";
            shift
            ;;
        -b|--blong)
            echo "Option b, argument $2";
            shift 2
            ;;
        -c|--clong)
            case "$2" in
                "")
                    echo "Option c, no argument";
                    shift 2  
                    ;;
                *)
                    echo "Option c, argument $2";
                    shift 2;
                    ;;
            esac
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Internal error!"
            exit 1
            ;;
    esac
done

# 處理剩餘的參數
echo remaining parameters=[$@]
echo \$1=[$1]
echo \$2=[$2]
```

# 統計

```
# 統計 dart 語言代碼 行數
find . -name '*.dart' | xargs wc -l
```

# 計算時間間隔

可以使用 date 指令來計算兩次時間間隔的秒數

```
start=$(date +%s)

sleep 1

end=$(date +%s)
echo "used $((end-start))s"
```