# lumberjack

lumberjack 是一個開源(MIT)的 golang 日誌輪轉底層庫，lumberjack 並沒有實現日誌功能只是實現了 io.Writer 接口用於將日誌寫入到輪替的檔案中

* 源碼 [https://github.com/natefinch/lumberjack](https://github.com/natefinch/lumberjack)

```
go get gopkg.in/natefinch/lumberjack.v2
```

```
import "gopkg.in/natefinch/lumberjack.v2"
```

```
log.SetOutput(&lumberjack.Logger{
    Filename:   "/var/log/myapp/foo.log",
    MaxSize:    500, // megabytes
    MaxBackups: 3,
    MaxAge:     28, //days
    Compress:   true, // disabled by default
})
```