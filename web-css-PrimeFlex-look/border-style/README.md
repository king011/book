# [border-style](https://www.primefaces.org/primeflex/borderstyle)

border-style 用於指示邊框的風格

| Class | Properties |
| -------- | -------- |
| border-solid     | border-style: solid;     |
| border-dashed     | border-style: dashed     |
| border-dotted     | border-style: dotted;     |
| border-double     | border-style: double;     |

![](assets/0.png)

```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container blue-container">
        <div class="border-solid border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-solid</div>
        <div class="border-dashed border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-dashed</div>
        <div class="border-dotted border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-dotted</div>
        <div class="border-double border-blue-500 w-12rem h-6rem m-2 surface-overlay font-bold flex align-items-center justify-content-center">border-double</div>
    </div>
</div>
```

# 響應式佈局

和 display 一樣這些 class 也支持響應式佈局，使用方式和 [display](web-css-PrimeFlex/layout-display) 相同