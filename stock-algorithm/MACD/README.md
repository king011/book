# MACD

Moving Average Convergence / Divergence 指數平滑異同移動平均線 簡寫 MACD 又稱指數平滑移動平均線

1970年由 Gerald Appel 提出 用於研判股票價格變化的 強度 方向 能量 趨勢週期

1. MACD 指標由 **一組曲線** 與 **棒形圖** 組成
2. 通過收盤股價指數的 **快變** 以及 **慢變** 的 EMA(指數移動平均值) 之間的 **差** 計算出來
3. 快指更短的 EMA 而 慢指更長的EMA 通常是 12日EMA 和 26日EMA

![](assets/macd.png)

1. 上表爲收盤市價圖表
2. 下表 綠線是**差離值(DIF)** 紅線是**訊號線(DEM)** 白色區塊是**棒形圖(OSC)**

# 計算方法

差離值 是由 12日EMA和26日EMA的差值計算出的

<i>DIF</i> = <i>EMA</i><sub><i>12</i></sub> - <i>EMA</i><sub><i>26</i></sub>

訊號線 通常由DIF的9日指數移動平均數繪製

<i>DEM</i> = <i>EMA</i><sub><i>(DIF,9)</i></sub>

棒形圖 OSC 等於 DIF 和 DEM的差

<i>OSC</i> = <i>DIF</i> - <i>DEM</i>

```go
package main

import (
	"fmt"
)

const (
	// Fast 快變使用 12日EMA
	Fast = 12
	// Slow 慢變使用 26日EMA
	Slow = 26
	// DEM DEM 使用9日DIF
	DEM = 9
)

// Macd .
type Macd struct {
	DIF float32
	DEM float32
	OSC float32
}

func main() {
	// 模擬數據
	values := []float32{
		43.34, 42.02, 42.19, 41.85, 43.29, 43.93, 44.86, 45.53, 46.02, 44.81, 48.15, 51, 50.95, 51.4, 51.04, 52.52, 51.5, 51.95, 51.2, 51.12, 52.28, 51.72, 50.85, 51.65, 51.06, 49.3, 49.58, 50.29, 50.34, 51.42, 52.93, 52.89, 53.35, 53.76, 52.86, 52.12, 50.89, 51.04, 50.14, 50.39, 50.01, 50.87, 52.52, 51.27, 51.29, 51.67, 52.08, 51.66, 51.73, 50.14, 50.22, 51.46, 51.84, 50.15, 52.2, 52.65, 49.45, 50.88, 50.32, 51.97, 50.55, 50.35, 51.1, 50.29, 49.58, 52.68, 50.81, 52.95, 52.06, 52.92, 53.04, 53, 54.45, 52.98, 52.25, 48.39, 55.86, 59.5, 61.24, 58.86, 55.65, 53.34, 50.84, 49.45, 47.91, 49.16, 52.26, 48.17, 44.1, 45, 45.47, 44.43, 45.67, 41.92, 42.06, 40.6, 42.35, 44.07, 46.32, 44.64, 42.95, 43.95, 45.12, 45.52, 48.56, 50.26, 47.21, 46.56, 43.1, 42.46, 42.24, 41.57, 44.01, 45.45, 40.52, 42.86, 45.42, 46.68, 47.41, 44.26, 41.25, 41.12, 37.49, 39.23, 38.51, 42.12, 44.08, 43.8, 46.02, 41.86, 40.66, 35.39, 38.06, 39.64, 36.43, 40.25, 41.19,
	}
	fmt.Println(`length`, len(values))
	// 計算10天的 macd
	count := 10
	items := make([]Macd, count)

	// 計算 DIF
	difCount := EMAK(DEM, count)
	fmt.Println(`difCount`, difCount)
	dif := make([]float32, difCount)
	fmt.Println(`valuesCount`, EMAK(Slow, difCount))
	for i := 0; i < difCount; i++ {
		dif[i] = EMA(values[i:], Fast) - EMA(values[i:], Slow)
		if i < count {
			items[i].DIF = dif[i]
		}
	}
	// 計算 DEM OSC
	for i := 0; i < count; i++ {
		items[i].DEM = EMA(dif[i:], DEM)
		items[i].OSC = items[i].DIF - items[i].DEM
	}

	// 打印 ema
	for i := 0; i < count; i++ {
		fmt.Println(i, items[i])
	}
}

// EMAK 返回 最少需要多少 數據源
func EMAK(days, count int) int {
	return int(3.45*float32(days+1)) + 1 + count - 1
}

// EMA 計算指數平均數
func EMA(values []float32, days int) (value float32) {
	if days < 2 {
		panic(`days < 2`)
	}
	// 保證至少追溯到 3.45*(N+1) 天
	k := int(3.45*float32(days+1)) + 1
	if len(values) < k {
		panic(fmt.Sprint(`length < k `, k))
	}
	// 計算 1-α
	subAlpha := float32(1) - float32(2)/float32(days+1)
	// 緩存 pow 結果
	var pow float32 = 1
	// 保存 分母
	var denominator float32 = 1.0
	// 保存 分子
	var numerator float32 = 0
	for i := 0; i < k; i++ {
		if i == 0 {
			// 分子 p1
			numerator = values[i]
			// 分母 1
			//denominator = 1
		} else {
			pow *= subAlpha
			// 分子 previous + pn*pow(subAlpha,n)
			numerator += values[i] * pow
			// 分母 previous + pow(subAlpha,n)
			denominator += pow
		}
	}
	return numerator / denominator
}
```

# 解讀

1. DIF形成快線 DEM 形成慢線
2. 當DIF從上而下穿過DEM爲 買入信號。相反則爲賣出信號。

棒形圖 的作用是顯示出 DIF 與 DEM的差 正在下降的 棒形圖 代表兩條線的差值朝負方向走 趨勢向下；靠近零軸時，DIF和DEM將相交出現 買賣信號
