# CMake

CMake 是一個 開源的(BSD) 跨平臺 自動化建構系統

* 官網 [https://cmake.org/](https://cmake.org/)
* wiki [https://zh.wikipedia.org/wiki/CMake](https://zh.wikipedia.org/wiki/CMake)
* learn [https://zh.wikibooks.org/wiki/CMake_入門](https://zh.wikibooks.org/wiki/CMake_%E5%85%A5%E9%96%80)

```bash
cmake_minimum_required(VERSION 3.0)

# 定義 項目
project(animal)
project(cat)
project(myexec)

# 增加 include 目錄
include_directories(src)

# ...

# 增加 庫 目錄
link_directories(lib)

# ...

# 增加 編譯選項
add_compile_options(-std=gnu++17)

# ...


# 編譯一個 靜態庫
add_library(animal STATIC
    src/animal/animal.cpp
)

# 編譯一個 動態庫
add_library(cat SHARED
    src/cat/cat.cpp
)
if(MINGW)
    target_link_libraries(cat 
        animal
        stdc++
    )
endif(MINGW)
if(WIN32)
    target_link_libraries(cat 
        animal
    )
endif(WIN32)

# 編譯一個 可執行 檔案
add_executable(myexec 
    src/main.cpp
)
# 增加 連接的 庫
if(UNIX)
    target_link_libraries(myexec 
        cat
        animal
    )
endif(UNIX)
if(MINGW)
    target_link_libraries(myexec 
        animal
        stdc++
    )
endif(MINGW)
if(WIN32)
    target_link_libraries(myexec 
        animal
    )
endif(WIN32)
```