# 監控檔案數量

ubuntu 下默認 最多監控 8192 個檔案
```sh
$ cat /proc/sys/fs/inotify/max_user_watches
8192
```

如果 要監控的 檔案 太多 

1. sudo vi /etc/sysctl.conf 添加 設置

	```
	#info=false
	# 允許的最大值 是 524288
	fs.inotify.max_user_watches=524288
	```

1. 執行 sudo sysctl -p 使用配置立刻生效

