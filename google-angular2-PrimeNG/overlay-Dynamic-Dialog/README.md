# [Dynamic Dialog](https://primeng.org/dynamicdialog)

```
import {DynamicDialogModule} from 'primeng/dynamicdialog';
```

```
@Component({
    templateUrl: './dynamicdialogdemo.html',
    providers: [DialogService]
})
export class DynamicDialogDemo {
    constructor(public dialogService: DialogService) {}
}
```

動態對話框允許使用任意的組件來作爲對話框顯示，它需要使用 DialogService 來顯示對話框

```
show() {
    const ref = this.dialogService.open(CarsListDemo, {
        header: 'Choose a Car',
        width: '70%'
    });
}
```

## DynamicDialogConfig DynamicDialogRef

你可以在對話框中注入 DynamicDialogRef 和 DynamicDialogRef

DynamicDialogConfig 用於引用對話框設定，其中的 data 屬性可以由 dialogService.open 傳入一些客製化數據給對話框

DynamicDialogRef 是對當前對話框實例的引用其 close 函數可用於關閉對話框並返回一個值

```
const ref = this.dialogService.open(DialogComponent, {
	data: {
		id: 123,
	}
})
ref.onClose.subscribe((v) => {
	console.log("result:", v)
})
```

```
import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig<{
    id: number
  }>
  ) {
    console.log(this.config.data)
  }
  ngOnInit(): void {
  }
  onClickOk() {
    this.ref.close(true)
  }
}
```

## Properties for DynamicDialog

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| data     | any     | null     | 傳遞給對話框對象的自定義數據     |
| header     | string     | null     | 對話框標題文本     |
| footer     | string     | null     | 對話框頁腳文本     |
| width     | string     | null     | 對話框寬度     |
| height     | string     | null     | 對話框高度     |
| closeOnEscape     | boolean     | true     | 鍵盤 Esc 被按下時是否關閉對話框     |
| dismissableMask     | boolean     | false     | 單擊遮罩層時是否關閉對話框     |
| baseZIndex     | number     | 0     | 用於分層的基礎 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| rtl     | boolean     | false     | 如果爲 true 對話框以 RTL 方向顯示     |
| style     | string     | null     | 爲組件嵌入內聯的 css style     |
| styleClass     | string     | null     | 爲組件嵌入的 css style     |
| contentStyle     | string     | null     | 爲顯示內容嵌入的 css style     |
| transitionOptions     | string     | 400ms cubic-bezier(0.25, 0.8, 0.25, 1)     | 過渡動畫     |
| closable     | boolean     | true     | 向標題中添加關閉圖標按鈕以隱藏對話框     |

| showHeader     | boolean     | true     | 顯示標題     |
| maximizable     | boolean     | false     | 是否 可以全屏顯示對話框     |
| minimizeIcon     | string     | pi pi-window-minimize     | 最小化按鈕圖標     |
| maximizeIcon     | string     | pi pi-window-maximize     | 最大化按鈕圖標     |
| minX     | number     | 0     | 拖動對話框 left 的最小值     |

| minY     | number     | 0     | 拖動對話框 top 的最小值     |
| position     | string     | center     | 對話框顯示位置: center top botto left right top-left top-right bottom-left bottom-right     |
| keepInViewport     | boolean     | true     | 保持對話框在 viewport 中     |
| resizable     | boolean     | true     | 允許調整 content 大小     |
| draggable     | boolean     | true     | 是否可以拖動標題來改變對話框位置     |



# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onClose     | event: Event object     | 當對話框關閉時回調     |
| onDestroy     | event: Event object     | 當對話框銷毀時回調     |
| onMaximize     | event: Event object     | 當對話框最大化或取消最大化時回調     |
| onDragStart     | event: Event object     | 當開始拖動對話框時回調     |
| onDragEnd     | event: Event object     | 當拖動對話框結束時回調     |
| onResizeInit     | event: Event object     | 當開始調整對話框尺寸時回調     |
| onResizeEnd     | event: Event object     | 當調整對話框尺寸結束時回調     |


# Styling
| Name | Element |
| -------- | -------- |
| p-dialog     | Container element     |
| p-dynamicdialog     | Container element     |
| p-dialog-titlebar     | Container of header     |
| p-dialog-title     | Header element     |
| p-dialog-titlebar-icon     | Icon container inside header     |
| p-dialog-titlebar-close     | Close icon element     |
| p-dialog-content     | Content element     |
| p-dialog-footer     | Footer element     |

