# nerdtree

nerdtree 是一個 檔案夾瀏覽插件 可以方便的 在目錄樹中 打開檔案

* 源碼 [https://github.com/preservim/nerdtree](https://github.com/preservim/nerdtree)

指令 :NERDTreeToggle 打開關閉 瀏覽窗口

## 映射快捷鍵

```
" 使用 ctrl+b 打開關閉
map <C-b> :NERDTreeToggle<CR>
```