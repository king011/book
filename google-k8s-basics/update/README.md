# [應用更新](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/)

用戶希望應用始終可見，而開發人員則需要每天多次部署新的版本。在 k8s 中 這些是通過滾動更新(Rolling Updates)完成的。滾動更新允許通過使用新的實例逐步更新 Pod 實例，零停機進行 Deployment 更新。新的 Pod 將在具有可用資源的節點上進行調度。

默認情況下更新期間不可用的 pod 最大值和可以創建的新 pod 數都是1。這兩個選項都可以配置爲 pod 數字或百分比。在 k8s 中，更新是經過版本控制的，任何 Deployment 更新都可以恢復到以前的版本。

# 滾動更新

![](assets/module_06_rollingupdates1.svg)

![](assets/module_06_rollingupdates2.svg)

![](assets/module_06_rollingupdates3.svg)

![](assets/module_06_rollingupdates4.svg)

如果公開了 Deployment ，服務將在更新期間僅對可用的 pod 進行負載均衡。

滾動更新允許以下操作
* 將應用程序從一個環境提升到另一個環境(通過容器鏡像更新)
* 回滾以前的版本
* 持續集成和持續交付應用程序，無需停機

# 更新 app 版本


使用 set image 更新 鏡像

```
kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2
```

# 驗證更新 

```
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT
curl $(minikube ip):$NODE_PORT
```

也可以通過 rollout status 信息來驗證更新

```
kubectl rollout status deployments/kubernetes-bootcamp
```

# 回滾更新

使用 rollout undo 回滾更新

```
kubectl rollout undo deployments/kubernetes-bootcamp
```

```
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT
curl $(minikube ip):$NODE_PORT
```