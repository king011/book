# 移動平均

moving average 既 移動平均 又稱爲 滾動平均 或 滑動平均 是一種通過創建整個數據集中不同子集的一系列平均數來分析數據點的計算方法

* sma 簡單移動平均
* ema 指數移動平均
* wma 加權移動平均
* cma 累計移動平均

# 簡單移動平均

簡單移動平均 **SMA** 就是幾個未加權的數值的平均數

![](assets/sma0.svg)


當已經計算出一個sma的值 要計算下個值時 可以直接簡單的 刪除舊值計入新值即可(每個值的1/n之和就是 sma)

![](assets/sma1.svg)

```go
package main

import (
	"fmt"
)

func main() {
	// 模擬數據
	values := []float32{43.34, 42.02, 42.19, 41.85, 43.29, 43.93, 44.86, 45.53, 46.02, 44.81, 48.15, 51, 50.95, 51.4, 51.04, 52.52, 51.5, 51.95, 51.2, 51.12, 52.28, 51.72, 50.85, 51.65, 51.06, 49.3, 49.58, 29, 50.34, 51.42, 52.93, 52.89, 53.35, 53.76, 52.86, 52.12, 50.89, 51.04, 50.14, 50.39, 50.01, 50.87, 52.52, 51.27, 51.29, 51.67, 52.08, 51.66, 51.73, 50.14, 50.22, 51.46, 51.84, 50.15, 52.2, 52.65, 49.45, 50.88, 50.32, 51.97}

	// 計算20天的 sma
	count := 20
	items := make([]float32, count)
	for i := 0; i < count; i++ {
		if i == 0 {
			items[i] = SMA(values[i:], count)
		} else {
			if len(values) < i-1+count {
				panic(`len(values) < i-1+count`)
			}
			items[i] = items[i-1] + (values[i-1+count]-values[i-1])/float32(count)
		}
	}

	// 打印 ema
	for i := 0; i < count; i++ {
		fmt.Println(i, items[i])
	}
}

// SMA 計算簡單平均數
func SMA(values []float32, days int) (value float32) {
	if days < 2 {
		panic(`days < 2`)
	}
	if len(values) < days {
		panic(fmt.Sprint(`length < days `, days))
	}

	// 保存 分子
	var numerator float32 = 0
	for i := 0; i < days; i++ {
		numerator += values[i]
	}
	return numerator / float32(days)
}
```

# 指數移動平均
exponential moving average 簡寫 **EMA** 或 **EXMA** 是以指數式遞減加權的移動平均

各數字的加權影響力隨時間而指數遞減 越近期的數據加權影響力越重 但較舊的數據也給予一定的加權值

![](assets/220px-Exponential_moving_average_weights_N=15.png)

加權的程度通常以常數 α 表示， α 介於 0到1。α 也可以用天數 N 來代表 ![](assets/α.svg),故當N=19天，則α=0.1

1. 設時間t的實際數值為<i>Y</i><sub><i>t</i></sub>，而時間t的EMA則為<i>S</i><sub><i>t</i></sub>；時間t-1的EMA則為<i>S</i><sub><i>t</i></sub>-1，計算時間t≥2是方程式為：

   ![](assets/1.svg)
	 
2. 設今日（t1）價格為p，則今日（t1）EMA的方程式為：

   ![](assets/2.svg)
	 
3. 將<i>EMA</i><sub><i>t0</i></sub>分拆開來如下：

   ![](assets/3.svg)

上述步驟3得出的公式是一個無窮級數 然而由於 1-α 小於1 故各項數值越來越小 通常 包含 k=3\.45\*\(N\+1\) 個數據即可包含99.9%的加權  
既只需要計算到 <i>P</i><sub><i>k</i></sub> 即可


```go
package main

import (
	"fmt"
)

func main() {
	// 模擬數據
	values := []float32{
		43.34, 42.02, 42.19, 41.85, 43.29, 43.93, 44.86, 45.53, 46.02, 44.81, 48.15, 51, 50.95, 51.4, 51.04, 52.52, 51.5, 51.95, 51.2, 51.12, 52.28, 51.72, 50.85, 51.65, 51.06, 49.3, 49.58, 29, 50.34, 51.42, 52.93, 52.89, 53.35, 53.76, 52.86, 52.12, 50.89, 51.04, 50.14, 50.39, 50.01, 50.87, 52.52, 51.27, 51.29, 51.67, 52.08, 51.66, 51.73, 50.14, 50.22, 51.46, 51.84, 50.15, 52.2, 52.65,
	}
	fmt.Println(`length`, len(values))

	// 計算12天的 ema
	count := 12
	fmt.Println(`valuesCount`, EMAK(count, count))
	items := make([]float32, count)
	for i := 0; i < count; i++ {
		items[i] = EMA(values[i:], count)
	}

	// 打印 ema
	for i := 0; i < count; i++ {
		fmt.Println(i, items[i])
	}
}

// EMAK 返回 最少需要多少 數據源
func EMAK(days, count int) int {
	return int(3.45*float32(days+1)) + 1 + count - 1
}

// EMA 計算指數平均數
func EMA(values []float32, days int) (value float32) {
	if days < 2 {
		panic(`days < 2`)
	}
	// 保證至少追溯到 3.45*(N+1) 天
	k := int(3.45*float32(days+1)) + 1
	if len(values) < k {
		panic(fmt.Sprint(`length < k `, k))
	}

	// 計算 1-α
	subAlpha := float32(1) - float32(2)/float32(days+1)

	// 緩存 pow 結果
	var pow float32 = 1
	// 保存 分母
	var denominator float32 = 1.0
	// 保存 分子
	var numerator float32 = 0
	for i := 0; i < k; i++ {
		if i == 0 {
			// 分子 p1
			numerator = values[i]

			// 分母 1
			//denominator = 1
		} else {
			pow *= subAlpha
			// 分子 previous + pn*pow(subAlpha,n)
			numerator += values[i] * pow

			// 分母 previous + pow(subAlpha,n)
			denominator += pow
		}
	}
	return numerator / denominator
}
```