# 連接數據庫

xorm 支持多種數據庫驅動 並且直接調用驅動提供的api連接數據庫

* [sqlite3](https://pkg.go.dev/github.com/mattn/go-sqlite3#SQLiteDriver.Open)
* [sqlite](https://gitlab.com/cznic/sqlite)
* [mysql](https://github.com/go-sql-driver/mysql#dsn-data-source-name)
* [postgres](http://pkg.go.dev/github.com/lib/pq)
* [mssql](https://github.com/denisenkom/go-mssqldb)
* [oracle](https://github.com/mattn/go-oci8)


對於 sqlite 多個事務併發寫時，會返回 sqlite 驅動會立刻返回 SQLITE\_BUSY, sqlite3 驅動會嘗試等待幾秒並多次嘗試再次啓動事務。而其它非嵌入式數據庫會等待一個事務完成後自動運行併發的互斥事務。

簡單來說 sqlite 不支持併發寫入，對於 sqlite 來說一個解決方案是，引入一個全局鎖來避免併發寫入(注意 begin 時不會返回 BUSY 只有在真實寫入 insert/update/delete 時 才會返回 BUSY)

```
package dblock

import (
	"context"
)

type DBLock chan bool

func New(sqlite bool) DBLock {
	if sqlite {
		ch := make(DBLock, 1)
		ch <- true
		return ch
	} else {
		return nil
	}
}
func (d DBLock) Lock(ctx context.Context) (e error) {
	if d == nil {
		return
	}
	select {
	case <-d:
	case <-ctx.Done():
		e = ctx.Err()
	}
	return
}
func (d DBLock) Unlock() (e error) {
	if d == nil {
		return
	}
	select {
	case d <- true:
	default:
		panic("not locked")
	}
	return
}
```

目前測試 2024-10-23
* 對於 sqlite3 只需要在真實寫入前加鎖即可
* 對於 sqlite 需要在事務啓動前加鎖，否則還是會返回 SQLITE\_BUSY

## [sqlite](https://gitlab.com/cznic/sqlite)

這是一個沒有使用 CGO 的 sqlite 驅動

```
go get modernc.org/sqlite
```


## [sqlite3](https://pkg.go.dev/github.com/mattn/go-sqlite3#SQLiteDriver.Open)

sqlite3 使用了 cgo 但它支持從 linux 交叉編譯，但需要設置 ldflags

```
export CGO_ENABLED=1
export GOOS=windows
export GOARCH=amd64
export CC="x86_64-w64-mingw32-gcc-posix"
export CXX="x86_64-w64-mingw32-g++-posix"
go build -ldflags '-linkmode external -extldflags -static'
```

test.db?cache=shared&mode=memory&immutable=false

[mode](https://www.sqlite.org/c3ref/open.html)
* ro
* rw
* rwc
* memory

[cache](https://www.sqlite.org/sharedcache.html)
* shared
* private

immutable
* true 1 yes on
* false 0 no off

```go
package main

import (
	"fmt"

	_ "github.com/mattn/go-sqlite3"
	"xorm.io/xorm"
)

type User struct {
	ID   uint64 `xorm:"pk autoincr notnull 'id'"`
	Name string `xorm:"notnull"`
}

func checkEror(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	engine, e := xorm.NewEngine(`sqlite3`, `my.db`)
	checkEror(e)
	engine.ShowSQL(true)
	e = engine.CreateTables(&User{})
	checkEror(e)
	_, e = engine.Insert(&User{Name: `kate`}, &User{Name: `anita`})
	checkEror(e)
	var users []User
	e = engine.Find(&users)
	checkEror(e)
	fmt.Println(users)
}
```

## [mysql](https://github.com/go-sql-driver/mysql#dsn-data-source-name)
[username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]

```
package main

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm"
)

type User struct {
	ID   uint64 `xorm:"pk autoincr notnull 'id'"`
	Name string `xorm:"notnull"`
}

func checkEror(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	engine, e := xorm.NewEngine(`mysql`, `user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local`)
	checkEror(e)
	engine.ShowSQL(true)
	e = engine.CreateTables(&User{})
	checkEror(e)
	_, e = engine.Insert(&User{Name: `kate`}, &User{Name: `anita`})
	checkEror(e)
	var users []User
	e = engine.Find(&users)
	checkEror(e)
	fmt.Println(users)
}
```