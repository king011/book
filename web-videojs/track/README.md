# 字幕

video.js 支持字幕，但格式需要是 WEBVTT 如果有其它格式的字幕可以使用 萬能的 ffmpeg 轉換

```
ffmpeg -i 001.ass 001.vtt
```

可以在 html 中使用 track 標籤添加字幕

```
<!-- 定義視頻元素 class 至少要指定 video-js  -->
<video id="player" class="video-js vjs-default-skin vjs-big-play-centered">
		<track kind='captions' src="movie/001.vtt" label="Chinese" default />
		<p class="vjs-no-js">
				To view this video please enable JavaScript, and consider upgrading to a
				web browser that
				<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
		</p>
</video>
```

此外也可以使用代碼添加字幕

```
this.addRemoteTextTrack({
		kind: 'captions',
		src: 'movie/001.vtt',
		default: true,
		label: 'Chinese',
}, false)
```