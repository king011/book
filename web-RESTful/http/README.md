# http動詞
RESETFul api 充分利用了 http 動詞 來 定義如何操作資源

| 動詞 | 含義 | 特性 |
| -------- | -------- | -------- |
| GET     | 讀取資源     | safe & idempotent     |
| PUT     | 替換資源     | idempotent     |
| DELETE     | 刪除資源     | idempotent     |
| POST     | 新增資源；也作為萬用動詞，處理其它要求     |      |
| PATCH     | 更新資源部份內容     |      |
| HEAD     | 類似GET，但只回傳HTTP header     | safe & idempotent     |
| ...     |      |      |

* safe 指操作不會改變服務器上資源狀態 而且結果可以被cache
* idempotent 指操作不管執行1次或n次都會得到同樣的資源結果反返回值不一定一樣(例如第二次DELETE可能會返回404) client可以放心的retry

# PUT POST PATCH

* PUT 通常用來替換單一資源或資源集合
* POST 通常用來新增資源 或作爲 utility api(不同與一般資源讀寫操作的請求類型)
* PATCH 用來更新資源的部分內容