# 自定義 UI

unity 已經開源了 [uGUI](https://github.com/Unity-Technologies/uGUI)，一些實現細節應該參考 uGUI 源碼

此文章只在於提供一個自定義 UI 的基本流程，以供記錄與查看

後文例子用於創建一個 **Switch** 的開關 ui

# MenuOptions

首先創建一個 **MenuOptions** 腳本，它用於在 unity 可視化節界面上爲 **GameObject** 菜單下添加一個選項，當選擇此對象後自動創建一個完整的 Switch ui

```
#info="MenuOptions.cs"

using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor.Experimental.SceneManagement;

static internal class MenuOptions
{
    private const string kUILayerName = "UI";

    [MenuItem("GameObject/UI-K/Switch", false, 0)] // 添加菜單項目到 GameObject 中
    static public void AddSwitch(MenuCommand menuCommand)
    {
        // 創建對象
        GameObject go = new GameObject("Switch");
        // 修改創建對象的父節點
        PlaceUIElementRoot(go, menuCommand);
    }

    private static void PlaceUIElementRoot(GameObject element, MenuCommand menuCommand)
    {
        GameObject parent = menuCommand.context as GameObject;
        Debug.Log($"{parent}");
        bool explicitParentChoice = true;
        if (parent == null)
        {
            parent = GetOrCreateCanvasGameObject();
            explicitParentChoice = false;

            // If in Prefab Mode, Canvas has to be part of Prefab contents,
            // otherwise use Prefab root instead.
            PrefabStage prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (prefabStage != null && !prefabStage.IsPartOfPrefabContents(parent))
                parent = prefabStage.prefabContentsRoot;
        }
        if (parent.GetComponentsInParent<Canvas>(true).Length == 0)
        {
            // Create canvas under context GameObject,
            // and make that be the parent which UI element is added under.
            GameObject canvas = MenuOptions.CreateNewUI();
            canvas.transform.SetParent(parent.transform, false);
            parent = canvas;
        }

        // Setting the element to be a child of an element already in the scene should
        // be sufficient to also move the element to that scene.
        // However, it seems the element needs to be already in its destination scene when the
        // RegisterCreatedObjectUndo is performed; otherwise the scene it was created in is dirtied.
        SceneManager.MoveGameObjectToScene(element, parent.scene);

        Undo.RegisterCreatedObjectUndo(element, "Create " + element.name);

        if (element.transform.parent == null)
        {
            Undo.SetTransformParent(element.transform, parent.transform, "Parent " + element.name);
        }

        GameObjectUtility.EnsureUniqueNameForSibling(element);

        // We have to fix up the undo name since the name of the object was only known after reparenting it.
        Undo.SetCurrentGroupName("Create " + element.name);

        GameObjectUtility.SetParentAndAlign(element, parent);
        if (!explicitParentChoice) // not a context click, so center in sceneview
            SetPositionVisibleinSceneView(parent.GetComponent<RectTransform>(), element.GetComponent<RectTransform>());

        Selection.activeGameObject = element;
    }
    private static void SetPositionVisibleinSceneView(RectTransform canvasRTransform, RectTransform itemTransform)
    {
        SceneView sceneView = SceneView.lastActiveSceneView;

        // Couldn't find a SceneView. Don't set position.
        if (sceneView == null || sceneView.camera == null)
            return;

        // Create world space Plane from canvas position.
        Vector2 localPlanePosition;
        Camera camera = sceneView.camera;
        Vector3 position = Vector3.zero;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRTransform, new Vector2(camera.pixelWidth / 2, camera.pixelHeight / 2), camera, out localPlanePosition))
        {
            // Adjust for canvas pivot
            localPlanePosition.x = localPlanePosition.x + canvasRTransform.sizeDelta.x * canvasRTransform.pivot.x;
            localPlanePosition.y = localPlanePosition.y + canvasRTransform.sizeDelta.y * canvasRTransform.pivot.y;

            localPlanePosition.x = Mathf.Clamp(localPlanePosition.x, 0, canvasRTransform.sizeDelta.x);
            localPlanePosition.y = Mathf.Clamp(localPlanePosition.y, 0, canvasRTransform.sizeDelta.y);

            // Adjust for anchoring
            position.x = localPlanePosition.x - canvasRTransform.sizeDelta.x * itemTransform.anchorMin.x;
            position.y = localPlanePosition.y - canvasRTransform.sizeDelta.y * itemTransform.anchorMin.y;

            Vector3 minLocalPosition;
            minLocalPosition.x = canvasRTransform.sizeDelta.x * (0 - canvasRTransform.pivot.x) + itemTransform.sizeDelta.x * itemTransform.pivot.x;
            minLocalPosition.y = canvasRTransform.sizeDelta.y * (0 - canvasRTransform.pivot.y) + itemTransform.sizeDelta.y * itemTransform.pivot.y;

            Vector3 maxLocalPosition;
            maxLocalPosition.x = canvasRTransform.sizeDelta.x * (1 - canvasRTransform.pivot.x) - itemTransform.sizeDelta.x * itemTransform.pivot.x;
            maxLocalPosition.y = canvasRTransform.sizeDelta.y * (1 - canvasRTransform.pivot.y) - itemTransform.sizeDelta.y * itemTransform.pivot.y;

            position.x = Mathf.Clamp(position.x, minLocalPosition.x, maxLocalPosition.x);
            position.y = Mathf.Clamp(position.y, minLocalPosition.y, maxLocalPosition.y);
        }

        itemTransform.anchoredPosition = position;
        itemTransform.localRotation = Quaternion.identity;
        itemTransform.localScale = Vector3.one;
    }

    static public GameObject CreateNewUI()
    {
        // Root for the UI
        var root = new GameObject("Canvas");
        root.layer = LayerMask.NameToLayer(kUILayerName);
        Canvas canvas = root.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        root.AddComponent<CanvasScaler>();
        root.AddComponent<GraphicRaycaster>();

        // Works for all stages.
        StageUtility.PlaceGameObjectInCurrentStage(root);
        bool customScene = false;
        PrefabStage prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
        {
            root.transform.SetParent(prefabStage.prefabContentsRoot.transform, false);
            customScene = true;
        }

        Undo.RegisterCreatedObjectUndo(root, "Create " + root.name);

        // If there is no event system add one...
        // No need to place event system in custom scene as these are temporary anyway.
        // It can be argued for or against placing it in the user scenes,
        // but let's not modify scene user is not currently looking at.
        if (!customScene)
            CreateEventSystem(false);
        return root;
    }
    private static void CreateEventSystem(bool select)
    {
        CreateEventSystem(select, null);
    }
    private static void CreateEventSystem(bool select, GameObject parent)
    {
        StageHandle stage = parent == null ? StageUtility.GetCurrentStageHandle() : StageUtility.GetStageHandle(parent);
        var esys = stage.FindComponentOfType<EventSystem>();
        if (esys == null)
        {
            var eventSystem = new GameObject("EventSystem");
            if (parent == null)
                StageUtility.PlaceGameObjectInCurrentStage(eventSystem);
            else
                GameObjectUtility.SetParentAndAlign(eventSystem, parent);
            esys = eventSystem.AddComponent<EventSystem>();
            eventSystem.AddComponent<StandaloneInputModule>();

            Undo.RegisterCreatedObjectUndo(eventSystem, "Create " + eventSystem.name);
        }

        if (select && esys != null)
        {
            Selection.activeGameObject = esys.gameObject;
        }
    }
    // Helper function that returns a Canvas GameObject; preferably a parent of the selection, or other existing Canvas.
    static public GameObject GetOrCreateCanvasGameObject()
    {
        GameObject selectedGo = Selection.activeGameObject;

        // Try to find a gameobject that is the selected GO or one if its parents.
        Canvas canvas = (selectedGo != null) ? selectedGo.GetComponentInParent<Canvas>() : null;
        if (IsValidCanvas(canvas))
            return canvas.gameObject;

        // No canvas in selection or its parents? Then use any valid canvas.
        // We have to find all loaded Canvases, not just the ones in main scenes.
        Canvas[] canvasArray = StageUtility.GetCurrentStageHandle().FindComponentsOfType<Canvas>();
        for (int i = 0; i < canvasArray.Length; i++)
            if (IsValidCanvas(canvasArray[i]))
                return canvasArray[i].gameObject;

        // No canvas in the scene at all? Then create a new one.
        return MenuOptions.CreateNewUI();
    }
    static bool IsValidCanvas(Canvas canvas)
    {
        if (canvas == null || !canvas.gameObject.activeInHierarchy)
            return false;

        // It's important that the non-editable canvas from a prefab scene won't be rejected,
        // but canvases not visible in the Hierarchy at all do. Don't check for HideAndDontSave.
        if (EditorUtility.IsPersistent(canvas) || (canvas.hideFlags & HideFlags.HideInHierarchy) != 0)
            return false;

        if (StageUtility.GetStageHandle(canvas.gameObject) != StageUtility.GetCurrentStageHandle())
            return false;

        return true;
    }
}
```

上述代碼只需要關注 **AddSwitch** 函數即可，其它都是從 uGUI 源碼中抄過來的，AddSwitch 只做來兩件事情

1. `new GameObject("Switch");` 創建來一個名爲 **Switch** 的 GameObject
2. 調用 **PlaceUIElementRoot** 將 GameObject 設置到合適的位置

**PlaceUIElementRoot** 的實現和其它代碼都抄襲自 uGUI 源碼，基本上在 PlaceUIElementRoot 裏面就是判斷來下當前場景是否創建了 ui系統 需要的 Canvas EventSystem，如果沒有就創建，還有就是將 new 的 GameObject 設置到正確的父節點之下，以及把此操作加入到 Undo 流程以便可以在 unity 開發界面中撤銷創建操作

當你添加來更多的自定義 UI 時，就爲 MenuOptions 實現更多的 AddXXX 函數即可

我們創建一個 ui 不能只是 new 一個 GameObject 即可，還修改爲 GameObject 添加上必要的組件或子節點以及其它的初始化操作，爲此我們還是模仿 uGUI 創建一個 DefaultControls 來初始化 ui 組件

現在先修改 AddSwitch 函數實現爲：
```
#info="MenuOptions.cs"
static internal class MenuOptions
{
    private const string kUILayerName = "UI";
    private const string kStandardSpritePath = "UI/Skin/UISprite.psd";
    private const string kBackgroundSpritePath = "UI/Skin/Background.psd";
    private const string kInputFieldBackgroundPath = "UI/Skin/InputFieldBackground.psd";
    private const string kKnobPath = "UI/Skin/Knob.psd";
    private const string kCheckmarkPath = "UI/Skin/Checkmark.psd";
    private const string kDropdownArrowPath = "UI/Skin/DropdownArrow.psd";
    private const string kMaskPath = "UI/Skin/UIMask.psd";

    static private DefaultControls.Resources s_StandardResources;

    static private DefaultControls.Resources GetStandardResources()
    {
        if (s_StandardResources.standard == null)
        {
            s_StandardResources.standard = AssetDatabase.GetBuiltinExtraResource<Sprite>(kStandardSpritePath);
            s_StandardResources.background = AssetDatabase.GetBuiltinExtraResource<Sprite>(kBackgroundSpritePath);
            s_StandardResources.inputField = AssetDatabase.GetBuiltinExtraResource<Sprite>(kInputFieldBackgroundPath);
            s_StandardResources.knob = AssetDatabase.GetBuiltinExtraResource<Sprite>(kKnobPath);
            s_StandardResources.checkmark = AssetDatabase.GetBuiltinExtraResource<Sprite>(kCheckmarkPath);
            s_StandardResources.dropdown = AssetDatabase.GetBuiltinExtraResource<Sprite>(kDropdownArrowPath);
            s_StandardResources.mask = AssetDatabase.GetBuiltinExtraResource<Sprite>(kMaskPath);
        }
        return s_StandardResources;
    }

    [MenuItem("GameObject/UI-K/Switch", false, 0)] // 添加菜單項目到 GameObject 中
    static public void AddSwitch(MenuCommand menuCommand)
    {
        // 創建對象
        GameObject go = DefaultControls.CreateSwitch(GetStandardResources());
        // 修改創建對象的父節點
        PlaceUIElementRoot(go, menuCommand);
    }
    // ...
}
```

再創建一個 DefaultControls 腳本

```
#info="DefaultControls.cs"
using UnityEngine;
public static class DefaultControls
{
    /// <summary>
    /// Object used to pass resources to use for the default controls.
    /// </summary>
    public struct Resources
    {
        /// <summary>
        /// The primary sprite to be used for graphical UI elements, used by the button, toggle, and dropdown controls, among others.
        /// </summary>
        public Sprite standard;

        /// <summary>
        /// Sprite used for background elements.
        /// </summary>
        public Sprite background;

        /// <summary>
        /// Sprite used as background for input fields.
        /// </summary>
        public Sprite inputField;

        /// <summary>
        /// Sprite used for knobs that can be dragged, such as on a slider.
        /// </summary>
        public Sprite knob;

        /// <summary>
        /// Sprite used for representation of an "on" state when present, such as a checkmark.
        /// </summary>
        public Sprite checkmark;

        /// <summary>
        /// Sprite used to indicate that a button will open a dropdown when clicked.
        /// </summary>
        public Sprite dropdown;

        /// <summary>
        /// Sprite used for masking purposes, for example to be used for the viewport of a scroll view.
        /// </summary>
        public Sprite mask;
    }
    public static GameObject CreateSwitch(Resources resources)
    {
        return new GameObject("Switch");
    }
}
```

CreateSwitch 依然只是返回了 new GameObject，我們後續再來完善它，不過那和 MenuOptions 無關，MenuOptions 已經可以完美工作了

# Switch

我們還需要定義一個 **Switch** 組件來完整 ui 功能代碼以及渲染相關代碼

```
#info="Switch.cs"
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


[AddComponentMenu("UI-K/Switch", 0)]
public class Switch : Selectable, IPointerClickHandler
{
    public class State
    {
        public bool opened = false;
        public bool completed = false;
        public Vector3 speed;
        public Vector3 target = new Vector3(0, 0, 0);
    }

    protected Switch() { }

    // 動畫持續時間
    public float duration = 0.25f;
    // 是否被選中
    public bool opened = false;
    private State state = new State();

    private RectTransform rectTransform_;
    public RectTransform rectTransform
    {
        get
        {
            if (rectTransform_ == null)
            {
                rectTransform_ = GetComponent<RectTransform>();
            }
            return rectTransform_;
        }
    }
    private RectTransform knobRectTransform_;
    public RectTransform knobRectTransform
    {
        get
        {
            if (knobRectTransform_ == null)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i);
                    if (child.name == "Knob")
                    {
                        knobRectTransform_ = child.GetComponent<RectTransform>();
                        if (knobRectTransform_ != null)
                        {
                            break;
                        }
                    }
                }
            }
            return knobRectTransform_;
        }
    }
    // 點擊事件 切換 狀態
    public virtual void OnPointerClick(PointerEventData eventData)
    {
        opened = !opened;
    }
    private void Update()
    {
        UpdateKnob();
    }
    private void UpdateKnob()
    {
        RectTransform knob;
        if (state.opened != opened) // 狀態變化 重置動畫
        {
            state.opened = opened;
            knob = knobRectTransform;
            if (knob == null)
            {
                Debug.LogWarning($"Switch not found Knob");
                state.completed = true;
                return;
            }
            state.completed = false;

            var x = rectTransform.rect.width / 2;
            if (opened)
            {
                state.target.x = x;
            }
            else
            {
                state.target.x = -x;
            }
#if UNITY_EDITOR
            if (!Application.isPlaying) // 在 編輯器中 且沒有運行遊戲
            {
                // 跳過動畫直接到最終狀態
                state.completed = true;
                knob.localPosition = state.target;
                return;
            }
#endif
            state.speed = (state.target - knob.localPosition) / duration;
        }
        else if (state.completed)
        {
            return;
        }
        else
        {
            knob = knobRectTransform;
            if (knob == null)
            {
                Debug.LogWarning($"Switch not found Knob");
                state.completed = true;
                return;
            }
        }
        // 移動 knob

        var move = state.speed * Time.deltaTime;
        var target = knob.localPosition + move;

        if ((move.x > 0 && target.x >= state.target.x) ||
            (move.x < 0 && target.x <= state.target.x))
        {
            target.x = state.target.x;
            target.y = 0;
            target.z = 0;
            state.completed = true;
        }
        else
        {
            if ((move.y > 0 && target.y > 0) || (move.y < 0 && target.y < 0))
            {
                target.y = 0;
            }
            if ((move.z > 0 && target.z > 0) || (move.z < 0 && target.z < 0))
            {
                target.z = 0;
            }
        }

        knob.localPosition = target;
    }
}
```

上面例子中我們在 Update 函數中完成了 ui 狀態切換時的動畫效果(移動 knob)，第 94 行 通過 **UNITY\_EDITOR** 宏以及 **Application.isPlaying** 變量判斷是否在編輯器節目且沒有運行遊戲，如果在編輯器節目且沒有運行遊戲，Update 函數只會在編輯界面或 屬性值變化時被調用一次，所以在此狀態直接將 動畫跳過到最終樣子以實現編輯界面的正確預料效果

# DefaultControls

最後我們再來修改 DefaultControls 的 CreateSwitch 函數，在裏面創建好 節點並爲節點設置上所有需要的 組件即可

```
#info="DefaultControls.cs"
using UnityEngine;
using UnityEngine.UI;

public static class DefaultControls
{
    /// <summary>
    /// Object used to pass resources to use for the default controls.
    /// </summary>
    public struct Resources
    {
        /// <summary>
        /// The primary sprite to be used for graphical UI elements, used by the button, toggle, and dropdown controls, among others.
        /// </summary>
        public Sprite standard;

        /// <summary>
        /// Sprite used for background elements.
        /// </summary>
        public Sprite background;

        /// <summary>
        /// Sprite used as background for input fields.
        /// </summary>
        public Sprite inputField;

        /// <summary>
        /// Sprite used for knobs that can be dragged, such as on a slider.
        /// </summary>
        public Sprite knob;

        /// <summary>
        /// Sprite used for representation of an "on" state when present, such as a checkmark.
        /// </summary>
        public Sprite checkmark;

        /// <summary>
        /// Sprite used to indicate that a button will open a dropdown when clicked.
        /// </summary>
        public Sprite dropdown;

        /// <summary>
        /// Sprite used for masking purposes, for example to be used for the viewport of a scroll view.
        /// </summary>
        public Sprite mask;
    }
    public static GameObject CreateSwitch(Resources resources)
    {
        // 創建節點
        var size = new Vector2(200, 80);
        GameObject switchRoot = CreateUIElementRoot("Switch", size);

        // 添加一個 圖像作爲背景
        Image image = switchRoot.AddComponent<Image>();
        image.sprite = resources.standard;
        image.type = Image.Type.Sliced;
        image.color = new Color(1f, 1f, 1f, 1f);

        // 添加 knob
        GameObject knob = CreateUIObject("Knob", switchRoot);
        knob.transform.localPosition = new Vector3(-size.x / 2, 0, 0);
        Image knobImage = knob.AddComponent<Image>();
        knobImage.sprite = resources.knob;
        knobImage.color = new Color(1f, 1f, 1f, 1f);
        Selectable knobSelectable = knob.AddComponent<Selectable>();
        knobSelectable.transition = Selectable.Transition.ColorTint;

        // 添加組件腳本
        Switch s = switchRoot.AddComponent<Switch>();
        // 爲組件腳本設置初始化值
        s.transition = Selectable.Transition.None;

        return switchRoot;
    }

    private static GameObject CreateUIElementRoot(string name, Vector2 size)
    {
        GameObject child = new GameObject(name);
        RectTransform rectTransform = child.AddComponent<RectTransform>();
        rectTransform.sizeDelta = size;
        return child;
    }

    static GameObject CreateUIObject(string name, GameObject parent)
    {
        GameObject go = new GameObject(name);
        go.AddComponent<RectTransform>();
        SetParentAndAlign(go, parent);
        return go;
    }
    private static void SetParentAndAlign(GameObject child, GameObject parent)
    {
        if (parent == null)
            return;

        child.transform.SetParent(parent.transform, false);
        SetLayerRecursively(child, parent.layer);
    }

    private static void SetLayerRecursively(GameObject go, int layer)
    {
        go.layer = layer;
        Transform t = go.transform;
        for (int i = 0; i < t.childCount; i++)
            SetLayerRecursively(t.GetChild(i).gameObject, layer);
    }
}
```