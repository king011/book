# [Dialog](https://primeng.org/dialog)

```
import {DialogModule} from 'primeng/dialog';
```

使用 **p-dialog** 來創建對話框，默認情況下對話框將被隱藏，你可以使用 **visible** 屬性來控制顯示(這個屬性支持雙向綁定)

```
<p-dialog header="Title" [(visible)]="display">
    Content
</p-dialog>

<button type="button" (click)="showDialog()" icon="pi pi-info-circle" label="Show"></button>
```

## Dimensions

你可以使用 **style** 來指定對話框寬度

```
<p-dialog header="Title" [(visible)]="display" [style]="{width: '50vw'}">
    Content
</p-dialog>
```

## Responsive

你可以使用 **breakpoints** 屬性來支持響應式佈局，下面例子中默認寬度是 50vw，屏幕寬度低於 961px 寬度爲 75vw，屏幕寬度低於 641px 寬度是 100vw。（breakpoints 的 key 是屏幕最大尺寸，value 是對應屏幕下對話框的寬度）

```
<p-dialog [(visible)]="display" [breakpoints]="{'960px': '75vw', '640px': '100vw'}" [style]="{width: '50vw'}">
    Content
</p-dialog>
```

## Header and Footer

可以使用 header/footer 模板來客製化頁眉和頁腳

```
<p-dialog [(visible)]="display">
    <ng-template pTemplate="header">
        Header content here
    </ng-template>
    Content
    <ng-template pTemplate="footer">
        //buttons
    </ng-template>
</p-dialog>
```

## position

使用 **position** 屬性來指定對話框的顯示位置

* center
* top
* bottom
* left
* right
* top-left
* top-right
* bottom-left
* bottom-right

```
<p-dialog position="top" [(visible)]="display">
Content
</p-dialog>
```

## Overlays Inside

當對話框包含其它有 overlay 組件的組件時(例如 dropdown)，overlay 部分不能因爲 overflow 超出對話框的邊界。爲了解決這個問題，你可以將 overlay 層附加到正文或允許在對話框中溢出

```
<p-dialog>
    <p-dropdown appendTo="body"></p-dropdown>
</p-dialog>
```

## Initial Focus

使用 **autofocus** 來讓對話框元素在顯示時自動獲取焦點

```
<p-dialog [(visible)]="display">
    <input type="text" pInputText >
    <input type="text" pInputText >
    <button type="button" pButton autofocus></button>
</p-dialog>
```

## Animation Configuration

使用 **transitionOptions** 屬性可以指定對話框顯示和隱藏的過渡動畫，默認值是 **150ms cubic-bezier(0, 0, 0.2, 1)**，下面的例子將完全禁用過渡動畫

```
<p-dialog [(visible)]="display" [transitionOptions]="'0ms'"></p-dialog>
```
# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onShow     | event: Event object     | 對話框顯示時回調     |
| onHide     | event: Event object     | 對話框隱藏時回調     |
| onResizeInit     | event: Event object     | 	對話框開始調整尺寸時回調     |
| onResizeEnd     | event: Event object     | 對話框調整尺寸結束時回調     |
| onDragEnd     | event: Event object     | 對話框拖動結束時回調     |
| onMaximize     | event: Event object     | 對話框最大化或最小化時回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| header     | string     | null     | 標題文本     |
| draggable     | boolean     | true     | 是否可以拖動標題來改變對話框位置     |
| keepInViewport     | boolean     | true     | 保持對話框在 viewport 中     |
| resizable     | boolean     | true     | 允許調整 content 大小     |
| contentStyle     | object     | null     | Style of the content section     |
| visible     | boolean     | false     | 是否顯示對話框     |
| modal     | boolean     | false     | 是否以模式對話框顯示     |
| position     | string     | center     | 對話框顯示位置: center top botto left right top-left top-right bottom-left bottom-right    |
| blockScroll     | boolean     | false     | 當對話框可見時是否應阻止背景滾動     |
| closeOnEscape     | boolean     | true     | 鍵盤 Esc 被按下時是否關閉對話框     |
| dismissableMask     | boolean     | false     | 單擊遮罩層時是否關閉對話框     |
| rtl     | boolean     | false     | 如果爲 true 對話框以 RTL 方向顯示     |
| closable     | boolean     | true     | 向標題中添加關閉圖標按鈕以隱藏對話框     |
| appendTo     | any     | null     | Target element to attach the dialog, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| style     | object     | null     | 爲組件嵌入內聯的 css style     |
| styleClass     | string     | null     | 爲組件嵌入的 css style     |
| maskStyleClass     | string     | null     | 爲遮罩層設置 css class     |
| contentStyle     | object     | null     | 爲顯示內容嵌入的 css style     |
| contentStyleClass     | string     | null     | 爲顯示內容設置 css class     |
| showHeader     | boolean     | true     | 顯示標題     |
| baseZIndex     | number     | 0     | 用於分層的基礎 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| minX     | number     | 0     | 拖動對話框 left 的最小值     |
| minY     | number     | 0     | 拖動對話框 top 的最小值     |
| focusOnShow     | boolean     | true     | 啓用後，第一個按鈕將獲得焦點     |
| focusTrap     | boolean     | true     | 啓用後，只有對話框內元素能獲取焦點     |
| maximizable     | boolean     | false     | 是否 可以全屏顯示對話框     |
| breakpoints     | object     | null     | 爲對話框設置響應式寬度     |
| transitionOptions     | string     | 150ms cubic-bezier(0, 0, 0.2, 1)     | 過渡動畫     |
| closeIcon     | string     | pi pi-times     | 關閉按鈕圖標     |
| closeAriaLabel     | string     | null     | 關閉按鈕 aria label     |
| closeTabindex     | string     | null     | 關閉按鈕的 tab index     |
| minimizeIcon     | string     | pi pi-window-minimize     | 最小化按鈕圖標     |
| maximizeIcon     | string     | pi pi-window-maximize     | 最大化按鈕圖標     |



# Templates

| Name | Parameters | 
| -------- | -------- |
| header     | -     |
| content     | -     |
| footer     | -     |

# Styling

| Name | Element |
| -------- | -------- |
| p-dialog     | Container element     |
| p-dialog-titlebar     | Container of header     |
| p-dialog-title     | Header element.     |
| p-dialog-titlebar-icon     | Icon container inside header     |
| p-dialog-titlebar-close     | Close icon element     |
| p-dialog-content     | Content element     |
| p-dialog-footer     | Footer element     |


