# [安裝](https://wails.io/docs/gettingstarted/installation)

首先需要確定支持的平臺

* Windows 10/11 AMD64/ARM64
* MacOS 10.13+ AMD64
* MacOS 11.0+ ARM64
* Linux AMD64/ARM64

# 安裝依賴的開發工具

Go 1.18+
NPM (Node 15+)

# 安裝 wails

```
go install github.com/wailsapp/wails/v2/cmd/wails@latest
```

執行 doctor 檢查缺少的依賴並依據提示進行安裝

```
wails doctor
```