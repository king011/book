# vscode

vscode 在 **EmmyLua** 插件前對 lua 支持很弱，直到 EmmyLua 出現才改變這一狀態這也是本喵重新使用 lua 的原因之一(打獵需要好的武器否則換個獵場是個好主意)

EmmyLua 使用 **Annotations**(註解) 來幫助 IDE 完成代碼提示與類型識別，提供了一個不錯的代碼書寫環境

1. 在 vscode 中安裝 EmmyLua 即可
2. 此外還需要安裝 java 運行環境(EmmyLua 運行需要)

* 源碼 [https://github.com/EmmyLua/VSCode-EmmyLua](https://github.com/EmmyLua/VSCode-EmmyLua)
* 文檔 [https://emmylua.github.io/](https://emmylua.github.io/)