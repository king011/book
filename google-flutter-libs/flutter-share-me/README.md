# flutter\_share\_me

[flutter\_share\_me](https://pub.dev/packages/flutter_share_me) 可以將內容分享到 flutter 外部

```
flutter pub add flutter_share_me
```

```
final flutterShareMe = FlutterShareMe();
await flutterShareMe.shareToSystem(msg: "https://github.com/zuiwuchang/anime");
```