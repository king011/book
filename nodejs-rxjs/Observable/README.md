# Observable

Observable 是一個 可觀察/可訂閱 對象 提供了 subscribe 函數用於 訂閱此 對象 

Observable 既可以處理 同步 亦可處理 異步 數據

Observable 的構造函數 傳入了一個 帶有 訂閱者subscriber的 回調函數 通過 subscriber 可以將 資料 push 給 Observer 

```
import { Observable, Subscriber } from 'rxjs'

// 創建一個 number 的 可觀察對象
const observable = new Observable<number>(function (subscriber: Subscriber<number>) {
    // push 資料
    subscriber.next(1)
    subscriber.next(2)

    // 發送錯誤時 調用error push 錯誤信息 之後 next 和 complete 都不在可用
    //subscriber.error("any error")

    setTimeout(() => {
        subscriber.next(3)
        // 通知 push 完成
        subscriber.complete()
    }, 1000)
})
```

# Observer
每當  Observable 發生事件時 觀察者 Observer 對應方法 便被回調通知

Observer 提供了 三個 方法

* next 每當 Observable 發出新值時 next 被回調
* complete 當 Observable 沒有資料可取得時 complete 被調用 之後 next 不在起作用
* error 每當 Observable 內發送錯誤時 error 被 回調

complete/error 任意函數只會被調用一次 之後 next/complete/error 都會變得無效

```
import { Observable, Subscriber } from 'rxjs'

// 創建一個 number 的 可觀察對象
const observable = new Observable<number>(function (subscriber: Subscriber<number>) {
    // push 資料
    subscriber.next(1)
    subscriber.next(2)

    // 發送錯誤時 調用error push 錯誤信息 之後 next 和 complete 都不在可用
    // subscriber.error("any error")

    setTimeout(() => {
        subscriber.next(3)
        // 通知 push 完成
        subscriber.complete()
    }, 1000)
})

// 創建一個 觀察者
const observer = {
    next(v: number) {
        console.log(v)
    },
    error(e: any) {
        console.log("error :", e)
    },
    complete() {
        console.log('complete')
    }
}

// 使用 Observer 訂閱 Observable
observable.subscribe(observer)
```

observer 可以是不完整的 只包含 next 即可 **但如果此時發生 error 事件 將會導致 代碼 throw 異常**


subscribe 提供了 重載函數 可以直接 傳入 三個 回調函數 進行 訂閱 subscribe 會自動組合成爲符合接口的 observer

```
import { Observable, Subscriber } from 'rxjs'

// 創建一個 number 的 可觀察對象
const observable = new Observable<number>(function (subscriber: Subscriber<number>) {
    // push 資料
    subscriber.next(1)
    subscriber.next(2)

    // 發送錯誤時 調用error push 錯誤信息 之後 next 和 complete 都不在可用
    // subscriber.error("any error")

    setTimeout(() => {
        subscriber.next(3)
        // 通知 push 完成
        subscriber.complete()
    }, 1000)
})


// 訂閱 Observable
observable.subscribe(
    (v) => {
        console.log(v)
    },
    (e) => {// error 回調是可選的 默認不做任何操作
        console.log("error :", e)
    },
    () => {// complete 回調是可選的 默認不做任何操作
        console.log('complete')
    },
)
```

# Subscription
在 訂閱 Observable 後 會 返回一個 Subscription 代表訂閱

如果 在訂閱後 變得不再需要 此訂閱 可以調用 unsubscribe 取消訂閱

```
import { Observable, Subscriber } from 'rxjs'

// 創建一個 number 的 可觀察對象
const observable = new Observable<number>(function (subscriber: Subscriber<number>) {
    // push 資料
    subscriber.next(1)
    subscriber.next(2)

    // 發送錯誤時 調用error push 錯誤信息 之後 next 和 complete 都不在可用
    //subscriber.error("any error")

    setTimeout(() => {
        subscriber.next(3)
        // 通知 push 完成
        subscriber.complete()
    }, 1000)
})


// 訂閱 Observable
const subscription = observable.subscribe(
    (v) => {
        console.log(v)
    },
)

// 500 ms 後 取消訂閱
setTimeout(() => {
    subscription.unsubscribe()
}, 500)
```
