# 控制事件循環

事件循環由 **struct&nbsp;ev\_loop** 描述

libev 支持兩類 loop

* default loop 支持 child process event
* 動態創建的 loops 不支持 child process event

# ev\_default\_loop

ev\_default\_loop 用來初始化 默認的 event loop 如果 已經創建過 則忽略參數 直接返回 實例

```c
// 初始化或返回默認 事件循環
struct ev_loop *ev_default_loop (unsigned int flags);

// 一個宏 返回默認事件循環
# define EV_DEFAULT  ev_default_loop (0)
```

```
  if (!ev_default_loop (0))
     fatal ("could not initialise libev, bad $LIBEV_FLAGS in environment?");
```
# ev\_loop\_new

ev\_loop\_new 用於動態創建 event loop

```
struct ev_loop *ev_loop_new (unsigned int flags)
```

flags 有如下取值 多個值可 **|** `EVBACKEND_POLL | EVBACKEND_SELECT | EVFLAG_NOENV`

| 取值 | 含義 |
| -------- | -------- |
| EVFLAG_AUTO     | 默認值     |
| EVFLAG_NOENV     | 不能檢查環境變量 LIBEV_FLAGS     |
| EVFLAG_FORKCHECK     | 除了在 fork 後手動調用 ev_loop_fork外 也可以指定 此值     |
| EVFLAG_NOINOTIFY     | ev_stat 不使用 inotify API     |
| EVFLAG_SIGNALFD     | ev_signal 使用 signalfd API     |
| EVFLAG_NOSIGMASK     | 使用libev 儘量避免修改 signal mask     |
| EVFLAG_NOTIMERFD     | 避免使用 timerfd 檢查時間 調變。仍然能檢查到時間調變 但會花更長時間並且準確性較低     |
| EVBACKEND_SELECT     | 使用 select 後端     |
| EVBACKEND_POLL     | 使用 poll 後端     |
| EVBACKEND_EPOLL     | 使用 epoll 後端     |
| EVBACKEND_LINUXAIO     | 使用 linux aio 後端     |
| EVBACKEND_KQUEUE     | 使用 BSD kqueue 後端     |
| EVBACKEND_DEVPOLL     | 使用 Solaris 8 devpoll 後端     |
| EVBACKEND_PORT     | 使用 Solaris 10 event port 後端     |
| EVBACKEND_ALL     | 嘗試所有後端(所有後端的合集)     |
| EVBACKEND_MASK     | 後端掩碼合集(包括了現在不存在 未來可能會支持的後端)     |

```
struct ev_loop *epoller = ev_loop_new (EVBACKEND_EPOLL | EVFLAG_NOENV);
   if (!epoller)
     fatal ("no epoll found here, maybe it hides under your chair");
```

# ev\_loop\_destroy

ev\_loop\_destroy 銷毀 event loop 釋放所有內存和內核狀態

所有活動事件的觀察者 不會停止 

```
ev_loop_destroy (loop);
```

# 其她函數
```
ev_loop_fork (loop)
int ev_is_default_loop (loop)
unsigned int ev_iteration (loop)
unsigned int ev_depth (loop)

// 返回正在所有的後端  EVBACKEND_* 
unsigned int ev_backend (loop)

// 返回 事件循環時間 正在處理回調時 此值不會變化
ev_tstamp ev_now (loop)

// 通過檢查內核來確定當前時間 並更新 ev_now 值
// 此操作很昂貴 通常 ev_run 中自動完成
ev_now_update (loop)

// 掛起 事件循環
ev_suspend (loop)
// 恢復掛起的 事件循環
ev_resume (loop)

// 執行事件 循環 flags 爲 0 一直執行知道沒有 事件
bool ev_run (loop, int flags)

// 跳出循環使用 ev_run 
// 返回 EVBREAK_ONE 跳出最內層循環
// EVBREAK_ALL 跳出所有嵌入的 ev_run
// 在下次執行 ev_run 時將 清除此標記 即時在 循環外 也可安全調用此函數 此時函數無效
ev_break (loop, how)

// 在 事件循環上 添加 引用計數
ev_ref (loop)
// 在 事件循環上 刪除 引用計數
ev_unref (loop)

// 設置等待事件花費的時間
ev_set_io_collect_interval (loop, ev_tstamp interval)
ev_set_timeout_collect_interval (loop, ev_tstamp interval)

ev_invoke_pending (loop)
int ev_pending_count (loop)
ev_set_invoke_pending_cb (loop, void (*invoke_pending_cb)(EV_P))
ev_set_loop_release_cb (loop, void (*release)(EV_P) throw (), void (*acquire)(EV_P) throw ())

// 設置 用戶自定義 數據 關聯到 loop
ev_set_userdata (loop, void *data)
void *ev_userdata (loop)

ev_verify (loop)
```