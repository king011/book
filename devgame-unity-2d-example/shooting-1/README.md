# 射擊 1/2

現在有了可怕的章魚敵人，我們來製作一些武器和彈藥，以讓遊戲角色能夠有反擊之力

# 發射物

首先我們來定義一個發射物，使用如下精靈

![](assets/shot.png)

因爲屏幕上會出現很多發射物，所以你應該將其製作爲 Prefab

1. 導入圖片資源
2. 使用資源創建一個新的 **Sprite Renderer** 組件
3. 將 **Sorting Layer** 設置到之前創建的 **Bullets**
4. 添加一個 **Rigidbody 2D** 組件，並設置 **Gravity Scale** 爲0，勾選 **Freeze Rotaion Z**
5. 添加 **Box Collider 2D** 組件
6. 將 Scale 設置爲 (0.5,0.5,1) 讓精靈變到合適等待大小

最後將 **Box Collider 2D** 中的 **Is Trigger** 勾選，這會告訴物理引擎這個碰撞體是一個 trigger(觸發器)，當發生碰撞時，不會進行物理模擬，之觸發碰撞事件。意思是發射物會在接觸對象後穿過它，將不會再有**真實**的相互作用力。同時，另外一個碰撞體的 **OnTriggerEnter2D** 事件將被調用

現在來爲它寫一些腳本

```
#info="ShotScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotScript : MonoBehaviour
{
    // 定義將造成多少傷害
    public int damage = 1;
    // 標記是敵人還是玩家發射的彈藥
    public bool isEnemyShot = false;
    void Start()
    {
        // 延遲20秒後銷毀射擊物，即彈藥被發射後的最大存活時間爲20秒
        Destroy(gameObject, 20);
    }
}
```

將 **ShotScript** 添加到發射物，同時記得把之前寫的 **MoveScript** 也添加到發射物這樣它才能移動

一切就緒後記得將其設置爲 Prefab 以便重複使用

![](assets/shot_config1.png)

# 碰撞和傷害

現在射擊物不會摧毀任何東西，我們創建一個新的腳本來管理傷害 **HealthScript**

```
#info="HealthScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    //  定義總血量
    public int hp = 1;
    // 是否是敵人
    public bool isEnemy = true;
    // 此函數用於計算傷害
    public void Damage(int damageCount)
    {
        hp -= damageCount;
        if (hp <= 0)
        {
            // 死亡
            Destroy(gameObject);
        }
    }
    // 和觸發器發生了碰撞
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        // 獲取發射物腳本
        var shot = otherCollider.gameObject.GetComponent<ShotScript>();
        if (shot != null) // 只有和發射物腳本碰撞才執行
        {
            if (shot.isEnemyShot != isEnemy)    // 只有被對立面的發射物擊中才執行
            {
                // 計算傷害
                Damage(shot.damage);

                // 銷毀發射物
                Destroy(shot.gameObject); // 記住應該始終以 GameObject 爲目標，否則你將只刪除腳本
            }
        }
    }
}
```

將 HealthScript 添加到敵人的 Prefab 上，如果你沒有對 Prefab 修改只是修改了場景中的敵人實例記得在 **Overrides** 菜單中選擇 **Apply ALL** 來將修改覆蓋到 Prefab 

確保子彈和章魚在同一條線上，測試一下(2D物理引擎使用的Box2d 不會用到 z坐標，Colliders 2D將總是在同一平面，除非你的遊戲對象不存在)

![](assets/bang.gif)

如果 敵人的生命值大於射擊的傷害，它就不會死亡

![](assets/bang2.gif)

# 開火

將發射物從場景刪除，我們已經完成它了(確定你已爲其創建 Prefab)，不再需要了。

我們需要一個新的腳本 **WeaponScript**，這個腳本將重複用於 (players enemies 等)，它的作用是在被掛載的GameObject 前實例化一個發射物

```
#info="WeaponScript.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{
    // 定義要實例化的發射物
    public Transform shotPrefab;
    // 兩次射擊的 冷卻時間
    public float shootingRate = 0.25f;
    // 還有多久冷卻
    private float shootCooldown;
    // Start is called before the first frame update
    void Start()
    {
        // 設置冷卻
        shootCooldown = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (shootCooldown > 0)// 如果武器過熱
        {
            // 拖進時間，以使武器冷卻
            shootCooldown -= Time.deltaTime;
        }
    }
    // 此函數用於武器進行射擊
    public void Attack(bool isEnemy)
    {
        if (CanAttack)//判斷是否冷卻
        {
            shootCooldown = shootingRate;// 射擊後設置冷卻時間

            // 創建一個發射物實例
            var shotTransform = Instantiate(shotPrefab) as Transform;

            // 修改彈藥方向和武器方向一致
            shotTransform.position = transform.position;

            // 設置武器是否是敵人在使用
            ShotScript shot = shotTransform.gameObject.GetComponent<ShotScript>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            // 使用彈藥方向始終和移動方向一致
            MoveScript move = shotTransform.gameObject.GetComponent<MoveScript>();
            if (move != null)
            {
                move.direction = this.transform.right; // 在 2D 空間中，精靈的朝向是精靈的左側
            }
        }
    }
    // 返回是否冷卻
    public bool CanAttack
    {
        get
        {
            return shootCooldown <= 0f;
        }
    }
}
```

將 WeaponScript 腳本添加到 Player 並設置好 **shotPrefab** 和 **shootingRate** 屬性

![](assets/dnd_prefab.png)

# 使用武器

前面我們創建了武器腳本 **WeaponScript**，但我們還沒有使用它，記得它裏面有個 Attack 函數，我們只需要在獲取玩家輸入後調用 Attack 即可，現在來修改 PlayerScript 腳本的 Update 函數以獲取用戶輸入

```

void Update()
{
		//...
		
		bool shoot = Input.GetButtonDown("Fire1");
		if (!shoot)
		{
				shoot = Input.GetButtonDown("Fire2");
		}

		if (shoot)
		{
				WeaponScript weapon = GetComponent<WeaponScript>();
				if (weapon != null)
				{
						// 因爲是玩家在使用所以傳入 false 參數
						weapon.Attack(false);
				}
		}
		
		// ...
}
```

你可以把使用武器的代碼放到 移動以前或之後 這不會有什麼差別

![](assets/shooting.gif)

**注意** 我們使用 **GetButtonDown** 方法來獲取輸入，最後的 **Down** 允許我們在按鈕被按下一次只有一次獲得輸入，**GetButton** 則在每一幀都返回 true直到按鈕被釋放

試着給 player 添加一個傾斜度像是(0,0,45)，射擊將會有一個45度的移動，但射擊物的旋轉角度並不正確，因爲我們沒有改變它

![](assets/shooting_rotation.png)