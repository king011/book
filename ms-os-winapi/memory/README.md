# 內存
[GlobalMemoryStatusEx](https://docs.microsoft.com/zh-tw/windows/desktop/api/sysinfoapi/nf-sysinfoapi-globalmemorystatusex) 可以返回 系統 內存信息
```cpp
#info=false
// 返回 內存 信息
BOOL GlobalMemoryStatusEx(
  LPMEMORYSTATUSEX lpBuffer
);

typedef struct _MEMORYSTATUSEX {
  DWORD     dwLength;
  DWORD     dwMemoryLoad;
  DWORDLONG ullTotalPhys;
  DWORDLONG ullAvailPhys;
  DWORDLONG ullTotalPageFile;
  DWORDLONG ullAvailPageFile;
  DWORDLONG ullTotalVirtual;
  DWORDLONG ullAvailVirtual;
  DWORDLONG ullAvailExtendedVirtual;
} MEMORYSTATUSEX, *LPMEMORYSTATUSEX;
```

| 成員名 | 含義 |
| -------- | -------- | 
| dwLength     | sizeof(MEMORYSTATUSEX) 必須在調用前設置     | 
| dwMemoryLoad     | [0,100] 指示當前物理內存使用 百分比     | 
| ullTotalPhys     | 實際物理內存     | 
| ullAvailPhys     | 當前可用物理內存     | 
| ullTotalPageFile     | 系統當前進程已提交內存限制     | 
| ullAvailPageFile     | 系統當前進程可提交內存限制 <=  ullTotalPageFile    | 
| ullTotalVirtual     | 虛擬地址用戶態大小     | 
| ullAvailVirtual     | 當前在調用進程的虛擬地址空間的用戶模式部分中的未保留和未提交的內存量，以字節為單位。     | 
| ullAvailExtendedVirtual     | 系統保留 始終爲0     | 

# GetSystemTimes

GetSystemTimes 可獲取 cpu 時間 統計段時間 即可算出 cpu 使用頻

```
uint64_t to_utc(const FILETIME &ftime)
{
    LARGE_INTEGER li;
    li.LowPart = ftime.dwLowDateTime;
    li.HighPart = ftime.dwHighDateTime;
    return li.QuadPart;
}
bool get_times(uint64_t &idle, uint64_t &total)
{
    FILETIME fidle;
    FILETIME kernel;
    FILETIME user;
    if (GetSystemTimes(&fidle, &kernel, &user))
    {
        idle = to_utc(fidle);
        total = to_utc(kernel) + to_utc(user);
        return true;
    }
    return false;
}
int main(int argc, char *argv[])
{
    uint64_t lastTotal = 0;
    uint64_t lastIDLE, nowTotal, nowIDLE;

    while (true)
    {
        if (get_times(nowIDLE, nowTotal))
        {
            if (lastTotal != 0)
            {
                uint64_t total = nowTotal - lastTotal;
                uint64_t use = total - (nowIDLE - lastIDLE);
                std::cout << use * 100 / total << std::endl;
            }
            lastIDLE = nowIDLE;
            lastTotal = nowTotal;
        }
        // 每秒統計
        Sleep(1000);
    }
    return 1;
}
```

