# [animations](https://www.primefaces.org/primeflex/animations)

animations 屬性用於爲元素指定動畫，@keyframes 用於爲動畫指定關鍵幀

<table class="doc-table">
				<thead>
				<tr>
						<th>Class</th>
						<th>Properties</th>
				</tr>
				</thead>
				<tbody>
				<tr>
						<td>fadein</td>
						<td style="white-space: pre" class="p-0">
animation: fadein .15s linear;

@keyframes fadein {
		0%   {
				opacity: 0;
		}
		100% {
				opacity: 1;
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeout</td>
						<td style="white-space: pre" class="p-0">
animation: fadeout .15s linear;

@keyframes fadeout {
		0%   {
				opacity: 1;
		}
		100% {
				opacity: 0;
		}
}
						</td>
				</tr>
				<tr>
						<td>slidedown</td>
						<td style="white-space: pre" class="p-0">
animation: slidedown .45s ease-in-out;

@keyframes slidedown {
		0% {
				max-height: 0;
		}
		100% {
				max-height: auto;
		}
}
						</td>
				</tr>
				<tr>
						<td>slideup</td>
						<td style="white-space: pre" class="p-0">
animation: slideup .45s cubic-bezier(0, 1, 0, 1);

@keyframes slideup {
		0% {
				max-height: 1000px;
		}
		100% {
				max-height: 0;
		}
}
						</td>
				</tr>
				<tr>
						<td>scalein</td>
						<td style="white-space: pre" class="p-0">
animation: scalein .15s linear;

@keyframes scalein {
		0% {
				opacity: 0;
				transform: scaleY(0.8);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: scaleY(1);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeinleft</td>
						<td style="white-space: pre" class="p-0">
animation: fadeinleft .15s linear;

@keyframes fadeinleft {
		0%   {
				opacity: 0;
				transform: translateX(-100%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateX(0%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeoutleft</td>
						<td style="white-space: pre" class="p-0">
animation: fadeoutleft .15s linear;

@keyframes fadeoutleft {
		0%   {
				opacity: 0;
				transform: translateX(0%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateX(-100%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeinright</td>
						<td style="white-space: pre" class="p-0">
animation: fadeinright .15s linear;

@keyframes fadeinright {
		0%   {
				opacity: 0;
				transform: translateX(100%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateX(0%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeoutright</td>
						<td style="white-space: pre" class="p-0">
animation: fadeoutright .15s linear;

@keyframes fadeoutright {
		0%   {
				opacity: 0;
				transform: translateX(0%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateX(100%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeinup</td>
						<td style="white-space: pre" class="p-0">
animation: fadeinup .15s linear;

@keyframes fadeinup {
		0%   {
				opacity: 0;
				transform: translateY(-100%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateY(0%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeoutup</td>
						<td style="white-space: pre" class="p-0">
animation: fadeoutup .15s linear;

@keyframes fadeoutup {
		0%   {
				opacity: 0;
				transform: translateY(0%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateY(-100%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeindown</td>
						<td style="white-space: pre" class="p-0">
animation: fadeindown .15s linear;

@keyframes fadeindown {
		0%   {
				opacity: 0;
				transform: translateY(100%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateY(0%);
		}
}
						</td>
				</tr>
				<tr>
						<td>fadeoutdown</td>
						<td style="white-space: pre" class="p-0">
animation: fadeoutdown .15s linear;

@keyframes fadeoutdown {
		0% {
				opacity: 0;
				transform: translateY(0%);
				transition: transform .12s cubic-bezier(0, 0, 0.2, 1), opacity .12s cubic-bezier(0, 0, 0.2, 1);
		}
		100% {
				opacity: 1;
				transform: translateY(100%);
		}
}
						</td>
				</tr>
				<tr>
						<td>animate-width</td>
						<td style="white-space: pre" class="p-0">
animation: animate-width 1000ms linear;

@keyframes animate-width {
		0% {
				width: 0;
		}
		100% {
				width: 100%;
		}
}
						</td>
				</tr>
				<tr>
						<td>flip</td>
						<td style="white-space: pre" class="p-0">
animation: flip .15s linear;

@keyframes flip {
		from {
				transform: perspective($animationPerspective) rotateX(-100deg);
		}

		to {
				transform: perspective($animationPerspective) rotateX(0);
		}
}
						</td>
				</tr>
				<tr>
						<td>flipleft</td>
						<td style="white-space: pre" class="p-0">
animation: flipleft .15s linear;

@keyframes flipleft {
		from {
				transform: perspective($animationPerspective) rotateY(-100deg);
				opacity: 0;
		}

		to {
				transform: perspective($animationPerspective) rotateY(0);
				opacity: 1;
		}
}
						</td>
				</tr>
				<tr>
						<td>flipright</td>
						<td style="white-space: pre" class="p-0">
animation: flipright .15s linear;

@keyframes flipright {
		from {
				transform: perspective($animationPerspective) rotateY(100deg);
				opacity: 0;
		}

		to {
				transform: perspective($animationPerspective) rotateY(0);
				opacity: 1;
		}
}
						</td>
				</tr>
				<tr>
						<td>flipup</td>
						<td style="white-space: pre" class="p-0">
animation: flipup .15s linear;

@keyframes flipup {
		from {
				transform: perspective($animationPerspective) rotateX(-100deg);
				opacity: 0;
		}

		to {
				transform: perspective($animationPerspective) rotateX(0);
				opacity: 1;
		}
}
						</td>
				</tr>
				<tr>
						<td>zoomin</td>
						<td style="white-space: pre" class="p-0">
animation: zoomin .15s linear;

@keyframes zoomin {
		from {
				opacity: 0;
				transform: scale3d(0.3, 0.3, 0.3);
		}

		50% {
				opacity: 1;
		}
}
						</td>
				</tr>
				<tr>
						<td>zoomindown</td>
						<td style="white-space: pre" class="p-0">
animation: zoomindown .15s linear;

@keyframes zoomindown {
		from {
				opacity: 0;
				transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);
		}

		60% {
				opacity: 1;
				transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
		}
}
						</td>
				</tr>
				<tr>
						<td>zoominleft</td>
						<td style="white-space: pre" class="p-0">
animation: zoominleft .15s linear;

@keyframes zoominleft {
		from {
				opacity: 0;
				transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);
		}

		60% {
				opacity: 1;
				transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);
		}
}
						</td>
				</tr>
				<tr>
						<td>zoomninright</td>
						<td style="white-space: pre" class="p-0">
animation: zoomninright .15s linear;

@keyframes zoomninright {
		from {
				opacity: 0;
				transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);
		}

		60% {
				opacity: 1;
				transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);
		}
}
						</td>
				</tr>
				<tr>
						<td>zoominup</td>
						<td style="white-space: pre" class="p-0">
animation: zoominup .15s linear;

@keyframes zoominup {
		from {
				opacity: 0;
				transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);
		}

		60% {
				opacity: 1;
				transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
		}
}
						</td>
				</tr>
				</tbody>
		</table>

## fadeinleft

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="fadeinleft animation-duration-1000 animation-iteration-infinite flex align-items-center justify-content-center
         font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">fadeinleft</div>
    </div>
</div>
```

## fadeoutleft
```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container yellow-container">
        <div class="fadeoutleft animation-duration-1000 animation-iteration-infinite flex align-items-center justify-content-center
     font-bold bg-yellow-500 text-gray-900 border-round m-2 px-5 py-3">fadeoutleft</div>
    </div>
</div>
```

## scalein
```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container green-container">
        <div class="scalein animation-duration-1000 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-green-500 text-white border-round m-2 px-5 py-3">scalein</div>
    </div>
</div>
```

# [animation-iteration-count](https://www.primefaces.org/primeflex/animationiteration)

animation-iteration-count 用於指定動畫要執行的次數，默認爲執行1次

| Class | Properties |
| -------- | -------- |
| animation-iteration-1     | animation-iteration-count: 1;     | 
| animation-iteration-2     | animation-iteration-count: 2;     | 
| animation-iteration-infinite     | animation-iteration-count: infinite;     | 

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="scalein animation-duration-500 animation-iteration-1 flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">iteration-1</div>
        <div class="scalein animation-duration-500 animation-iteration-2 flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">iteration-2</div>
        <div class="scalein animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">iteration-infinite</div>
    </div>
</div>
```

# [animation-fill-mode](https://www.primefaces.org/primeflex/animationfill)

animation-fill-mode 定義執行後的樣式

| Class | Properties | Note |
| -------- | -------- | -------- |
| animation-fill-none     | animation-fill-mode: none;     | 默認值，動畫結束後返回最初的狀態     |
| animation-fill-forwards     | animation-fill-mode: forwards;     | 動畫結束後，保持最後一幀的狀態     |
| animation-fill-backwards     | animation-fill-mode: backwards;     | 動畫結束後，保持第一幀狀態     |
| animation-fill-both     | animation-fill-mode: both;     | 依據播放次數和方向，保持在第一幀或最後一幀狀態     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="animation-color animation-fill-none flex align-items-center justify-content-center font-bold border-round m-2 px-5 py-3">fill-none</div>
        <div class="animation-color animation-fill-forwards flex align-items-center justify-content-center font-bold border-round m-2 px-5 py-3">fill-forwards</div>
        <div class="animation-color animation-fill-backwards flex align-items-center justify-content-center font-bold border-round m-2 px-5 py-3">fill-backwards</div>
        <div class="animation-color animation-fill-both flex align-items-center justify-content-center font-bold border-round m-2 px-5 py-3">fill-both</div>
    </div>
</div>


<style lang="scss">
@keyframes animation-color {
    0%{
        background-color: var(--blue-500);
        color: var(--gray-50);
    }
    100%{
        background-color: var(--yellow-500);
        color: var(--gray-900);
    }
}
.animation-color {
    animation: animation-color 3s linear;
}
</style>
```

# [animation-duration](https://www.primefaces.org/primeflex/animationduration)

animation-duration 指定動畫需要花費多少時間

| Class | Properties |
| -------- | -------- |
| animation-duration-100     | animation-duration: 100ms;     | 
| animation-duration-150     | animation-duration: 150ms;     | 
| animation-duration-200     | animation-duration: 200ms;     | 
| animation-duration-300     | animation-duration: 300ms;     | 
| animation-duration-400     | animation-duration: 400ms;     | 
| animation-duration-500     | animation-duration: 500ms;     | 
| animation-duration-1000     | animation-duration: 1000ms;     | 
| animation-duration-2000     | animation-duration: 2000ms;     | 
| animation-duration-3000     | animation-duration: 3000ms;     | 

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="scalein animation-duration-200 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">scalein</div>
        <div class="scalein animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">scalein</div>
        <div class="scalein animation-duration-1000 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">scalein</div>
    </div>
</div>
```

# [animation-timing-function](https://www.primefaces.org/primeflex/animationtimingfunction)

animation-timing-function 指定動畫的速度曲線

| Class | Properties |
| -------- | -------- |
| animation-linear     | animation-timing-function: linear;     |
| animation-ease-in     | animation-timing-function: cubic-bezier(0.4, 0, 1, 1);     |
| animation-ease-out     | animation-timing-function: cubic-bezier(0, 0, 0.2, 1);     |
| animation-ease-in-out     | animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="scalein animation-linear animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">linear</div>
        <div class="scalein animation-ease-in animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">ease-in</div>
        <div class="scalein animation-ease-out animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">ease-out</div>
        <div class="scalein animation-ease-in-out animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">ease-in-out</div>
    </div>
</div>
```

# [animation-delay](https://www.primefaces.org/primeflex/animationdelay)

animation-delay 指定動畫延遲多久播放

| Class | Properties |
| -------- | -------- |
| animation-delay-100     | animation-delay: 100ms;     |
| animation-delay-150     | animation-delay: 150ms;     |
| animation-delay-200     | animation-delay: 200ms;     |
| animation-delay-300     | animation-delay: 300ms;     |
| animation-delay-400     | animation-delay: 400ms;     |
| animation-delay-500     | animation-delay: 500ms;     |
| animation-delay-1000     | animation-delay: 1000ms;     |

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container blue-container">
        <div class="scalein animation-delay-200 animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">scalein</div>
        <div class="scalein animation-delay-300 animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">scalein</div>
        <div class="scalein animation-delay-500 animation-duration-500 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-blue-500 text-white border-round m-2 px-5 py-3">scalein</div>
    </div>
</div>
```

# animation-play-state

animation-play-state 用於指定動畫狀態
* running ，此爲默認值
* paused

PrimeFlex 沒有爲 動畫 狀態定義 class，但你可以設置自己設置這個屬性來完成一些有趣的功能，比如在鼠標懸停時暫停播放動畫

```
<div class="card">
    <div class="flex flex-wrap align-items-center justify-content-center card-container green-container">
        <div class="scalein animation-duration-1000 animation-iteration-infinite flex align-items-center justify-content-center
    font-bold bg-green-500 text-white border-round m-2 px-5 py-3">Hover ME</div>
    </div>
</div>
<style>
    .scalein {
        animation-play-state: running;
    }

    .scalein:hover {
        animation-play-state: paused;
    }
</style>
```
