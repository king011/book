# 發生器

dart 支持 兩種 發生器

* Synchronous generator 返回 一個 Iterable object.
* Asynchronous generator 返回一個 Stream object.

# Synchronous
使用 sync\* 聲明 函數 使用 yield 傳遞 值

```dart
Iterable<int> naturalsTo(int n) sync* {
  int k = 0;
  while (k < n) yield k++;
}
```

# asynchronous
使用 async\* 聲明 函數 使用 yield 傳遞 值

```dart
Stream<int> asynchronousNaturalsTo(int n) async* {
  int k = 0;
  while (k < n) yield k++;
}
```

# 遞歸

如果 使用了 遞歸 用 yield\* 可以提高 效率

```dart
Iterable<int> naturalsDownFrom(int n) sync* {
  if (n > 0) {
    yield n;
    yield* naturalsDownFrom(n - 1);
  }
}
```