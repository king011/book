# npm

npm 是 node 官方提供的 包 管理 工具  
可以方便的 進行 包的 發佈 下載 安裝 升級 刪除

[http://npmjs.org/](http://npmjs.org/) 是一個 包含了各個發佈包 信息的 web

# 常用命令
```txt
# 查找可用的包
npm search 包名

# 本地 模式 安裝包
npm i 包名
npm install 包名
	包安裝在 當前目錄的 node_modules 下

# 全局 模式 安裝包
npm i -g 包名
npm install -g 包名
	包安裝在 當前目錄的 $NODE_HOME/lib/node_modules 下

# 刪除 包
npm uninstall 包名
npm uninstall -g 包名
	aliases: remove, rm, r, un, unlink

# 更新 已安全的 全部 包
npm update
npm update -g

# 創建一個 用戶 以便可以 發佈 包
npm adduser

# 創建登入令牌 以便可以 發佈包
npm login

# 銷毀登入 令牌
npm logout

# 創建 符合 包要求 的 package.json 檔案
npm init

# 發佈包
npm publish

# 刪除 發佈的 包
npm unpublish
```

# 創建 包
在 包 的根路徑下 執行 **npm init** 創建包  
命令執行後 會要求 輸入 如下 包 信息
```sh
package name: (king-node) 
version: (1.0.0) 
description: some general tool
entry point: (index.js) 
test command: 
git repository: 
keywords: 
author: king
license: (ISC) GPL-3.0
About to write to /home/king/project/nodejs/king-node/package.json:
 
{
  "name": "king-node",
  "version": "1.0.0",
  "description": "some general tool",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "king",
  "license": "GPL-3.0"
}
 
 
Is this OK? (yes)
```
## 條目 信息
1. **package name**  
 包名 默認是 所在 檔案夾 名稱 一旦 發佈後 使用 npm install XXX 來安裝模塊
 
1. **version**  
 包含 X.Y.Z 的包版本 分別 指定了 主版本 此版本 補丁版本  
 通常 修改bug 只改動 Z  
 添加了新功能 改動 Y  
 出現 大變得 不向下兼容 改動 X
 
 1. **description**  
 包簡介
 
 1. **main**  
 包入口 執行 require('xxx') 加載包時會加載的 檔案
 
  1. **test command**  
 測試命令
 
  1. **git repository**  
git 地址
 
  1. **keywords**  
 在使用npm搜索 那些關鍵字時 顯示此包
 
  1. **author**  
作者信息

  1. **license**  
許可協議

## 發佈包
在 創建好 package.json 和js代碼後 執行下述 步驟 發佈包
```sh
# 創建一個 用戶 以便可以 發佈 包
npm adduser

# 創建登入令牌 以便可以 發佈包
npm login

# 銷毀登入 令牌
npm logout

# 發佈包
npm publish --access=public

# 刪除 發佈的 包
npm unpublish
```
> 更新了包後 需要 增加 version 之後執行 npm publish 以便發佈最新包到 npm 服務器
> 

## bin
對於 cli 工具 可以在 package.json 中 添加一個 bin 屬性 在npm install時 會自動 爲其 創建 link 到 /usr/local/bin/

```json
{
  "name": "@king011/cli",
  "version": "1.0.1",
  "description": "nodejs typescript env tools",
  "main": "dist/k-nj",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [
    "ts",
    "cli",
    "project"
  ],
  "bin": {
    "k-nc": "./dist/main.js"
  },
  "author": "king011",
  "license": "GPL-3.0"
}
```

