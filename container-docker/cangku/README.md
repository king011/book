# 倉庫
[Docker Hub](https://hub.docker.com/) 是 docker 官方 維護的 公共倉庫 註冊後 就可以 上傳 自己的 映像檔

# 基本操作
* 使用 **docker login** 輸入 用戶 信息即可 登入
* docker logout 刪除 登入 憑證
* docker push 上傳
* docker pull 下載
* docker search 查找

登入 成功後 會在 ~/.docker/config.json 中 存入 登入憑證

# 鏡像

西朝鮮訪問 docker hub 很慢，可以選擇爲 docekr 設置訪問鏡像，**vi /etc/docker/daemon.json**

```
#info="阿里雲鏡像"
{
  "registry-mirrors": ["https://uyah70su.mirror.aliyuncs.com"]
}
```

```
#info="華爲雲"
{
    "registry-mirrors": ["https://7bafc985f90c43b887a96c2b846cf984.mirror.swr.myhuaweicloud.com"]
}
```

重啓服務後執行下述指令查看鏡像是否有效

```
$ docker info | grep Mirrors -A1
WARNING: No swap limit support
 Registry Mirrors:
  https://7bafc985f90c43b887a96c2b846cf984.mirror.swr.myhuaweicloud.com/
```