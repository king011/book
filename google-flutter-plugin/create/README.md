# 創建架構代碼

執行 `flutter create -t plugin` 指令即可插件 一個 插件項目

```sh
flutter create -t plugin --org com.king011 --project-name coll --platforms android coll
```

在項目根目錄執行下述指令可以添加相應平臺的 架構代碼

```
flutter create -t plugin --platforms ios .
```

不同平臺需要依據 平臺特性去實現接口，而 flutter plugin 爲 flutter 磨平了差異

# Platform Channels

flutter 不直接調用平臺 api 而是通過 platform channels 提供的消息機制實現溝通

通常在 dart 代碼中 發送消息調用 在平臺代碼接收消息並返回結果(消息的發送和接收都是異步執行的)

標準的 platform channel 使用 standard message codec，此編碼器支持簡單的類型 JSON-like 的值的高效二進制序列化，例如將 bool number string bytes list map (更多細節見 [StandardMessageCodec](https://api.flutter.dev/flutter/services/StandardMessageCodec-class.html))。當發送和接收值時，這些值與消息間的序列化和反序列化會自動發生


| Dart | Java | Kotlin |
| -------- | -------- | -------- |
| null     | null     | null     |
| bool     | java.lang.Boolean     | Boolean     |
| int     | java.lang.Integer     | Int     |
| int 超過 32 bits     | java.lang.Long     | Long     |
| double     | java.lang.Double     | Double     |
| String     | java.lang.String     | String     |
| Uint8List     | byte[]     | ByteArray     |
| Int32List     | int[]     | IntArray     |
| Int64List     | long[]     | LongArray     |
| Float32List     | float[]     | FloatArray     |
| Float64List     | double[]     | DoubleArray     |
| List     | java.util.ArrayList     | List     |
| Map     | java.util.HashMap     | HashMap     |


# dart

**lib/coll.dart**  會被初始化爲以下代碼 

* coll 是創建項目時指定的 項目名稱

```

import 'dart:async';

import 'package:flutter/services.dart';

class Coll {
  static const MethodChannel _channel = MethodChannel('coll');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
```

1. 第6行插件了一個 MethodChannel 用於調用 平臺提供的代碼
2. 第9行通過 MethodChannel.invokeMethod 去調用平臺代碼 並等待平臺返回結果

MethodChannel 需要傳入一個 通道名稱 需要和平臺代碼中註冊的名稱一致

```
#info="lib/coll.dart"
import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Coll {
  static const MethodChannel _channel = MethodChannel('coll');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> get error async {
    // result.error 返回的結果將作爲 PlatformException 異常可以被 dart 捕獲
    await _channel.invokeMethod('error');
  }

  static Future<void> get exception async {
    // 原生java異常 無法被捕獲
    //
    // 但是會在 dart 中會引發 MissingPluginException 異常(可以被捕獲 MissingPluginException)
    await _channel.invokeMethod('exception');
  }

  static Future<void> get empty async {
    // 沒有通過 result 返回結果的調用將一直阻塞
    await _channel.invokeMethod('empty');
  }

  static Future<int> value() async {
    final result = await _channel.invokeMethod('value', 1);
    debugPrint('${result.runtimeType}: $result');
    return result;
  }

  static Future<Int32List> int32list() async {
    final result =
        await _channel.invokeMethod('int32list', Int32List.fromList([1, 2, 3]));
    debugPrint('${result.runtimeType}: $result');
    return result;
  }

  static Future<List<int>> list() async {
    // 傳入 list 參數
    final result = await _channel.invokeMethod('list', [1, 2, 3]);
    // 返回 list 參數
    return result.map<int>((e) => e as int).toList();
  }

  static Future<Map<String, dynamic>> map() async {
    // 傳入 map 參數
    final Map<dynamic, dynamic> result = await _channel.invokeMethod('map', {
      "id": 1,
      "name": "k0",
    });
    // 返回 map 參數
    return result.map((key, value) => MapEntry(key as String, value));
  }
}
```

```
#info="example/lib/main.dart"
import 'package:flutter/material.dart';
import 'package:coll/coll.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: ListView(
          children: [
            TextButton(
              child: const Text('error'),
              onPressed: () async {
                try {
                  await Coll.error;
                } catch (e) {
                  debugPrint('error: $e');
                }
              },
            ),
            TextButton(
              child: const Text('exception'),
              onPressed: () async {
                try {
                  await Coll.exception;
                } catch (e) {
                  debugPrint('exception: $e');
                }
              },
            ),
            TextButton(
              child: const Text('empty'),
              onPressed: () async {
                try {
                  debugPrint('empty begin');
                  await Coll.empty;
                  debugPrint('empty end');
                } catch (e) {
                  debugPrint('exception: $e');
                }
              },
            ),
            TextButton(
              child: const Text('value'),
              onPressed: () async {
                try {
                  await Coll.value();
                } catch (e) {
                  debugPrint('value: $e');
                }
              },
            ),
            TextButton(
              child: const Text('int32list'),
              onPressed: () async {
                final result = await Coll.int32list();
                debugPrint('int32list: $result');
              },
            ),
            TextButton(
              child: const Text('list'),
              onPressed: () async {
                final result = await Coll.list();
                debugPrint('list: $result');
              },
            ),
            TextButton(
              child: const Text('map'),
              onPressed: () async {
                final result = await Coll.map();
                debugPrint('list: $result');
              },
            ),
          ],
        ),
      ),
    );
  }
}
```
# android

android 自動創建的代碼通常如下


```
package com.king011.coll

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** CollPlugin */
class CollPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "coll")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
```


1. 第9行的回調函數 onAttachedToEngine(插件被附加到引擎) 中 創建了 名稱爲 **coll** 的通道，之後設置了響應 dart 調用的回調函數
2. 第24行的回調函數 onMethodCall 會再收到dart調用時回調，需要在此解析調用的函數以及參數並在處理結束後通過  result 返回結果給 dart
3. 第32行的回調函數 onDetachedFromEngine(插件被分離出引擎) 中 取消了onMethodCall 回調函數的註冊

通常android代碼只需要修改 onMethodCall 回調函數的內容即可

> 注意編寫原生代碼時應該打開項目 your\_plugin/example/android(之後通過左上角資源窗口顯示 Android代碼 編寫源碼) 而非 your\_plugin/android

```
package com.king011.coll

import android.util.Log
import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.lang.Exception

/** CollPlugin */
class CollPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "coll")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method){
      "getPlatformVersion"->{
        result.success("Android ${android.os.Build.VERSION.RELEASE}")
      }
      "error"->{
        // result.error 返回的結果將作爲 PlatformException 異常可以被 dart 捕獲
        result.error("errorCode","errorMessage: String?","errorDetails:Any?")
      }
      "exception"->{
        // 原生java異常 無法被捕獲
        //
        // 但是會在 dart 中會引發 MissingPluginException 異常(可以被捕獲 MissingPluginException)
        throw Exception("exception")
      }
      "empty"->{
        // 沒有通過 result 返回結果的調用將一直阻塞
      }
      "value"->{
        // 個參數 直接強制轉換即可
        var v = call.arguments as Int
        result.success(++v);
      }
      "int32list"->{
        // 一些 基礎型別的 XXXArray 可以直接轉換
        val list = call.arguments as IntArray
        val resp = list.map { it->it*it }.toIntArray()
        // return IntArray
        result.success(resp)
      }
      "list"->{
        // 數組參數 直接強制轉換即可
        val list = call.arguments as ArrayList<Int>
        val resp = list.map { it->it*it }
        // return List<int>
        result.success(resp)
      }
      "map"->{
        // map 也可以直接強轉 call.arguments，此外 call 也提供了 call.argument<T> 用於獲取 key 對應的值
        val id = call.argument<Int>("id")!!
        val name = call.argument<String>("name")!!
        val resp = mapOf("id" to id+id, "name" to name+name)
        result.success(resp)
      }
      else->{
        // 在 dart 中引發 MissingPluginException 異常
        result.notImplemented()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
```

## ActivityAware

插件可以實現 ActivityAware 這樣 插件關聯的 Activity 生命週期發生變化時就會被調用其回調函數

```
public class MyPlugin implements FlutterPlugin, ActivityAware {
  //...normal plugin behavior is hidden...

  @Override
  public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
    // TODO: 插件附加到了一個 Activity 可以在此執行一些初始化代碼
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    // TODO: 插件關聯的 Activity 被破壞 你可以再次保存狀態
  }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding activityPluginBinding) {
    // TODO: 你的插件附加到了新的 Activity 你可以在此恢復之前保存的狀態
  }

  @Override
  public void onDetachedFromActivity() {
    // TODO: 你的插件不再與 Activity 關聯，請在此執行清理代碼
  }
}
```