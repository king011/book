# zap

zap 是一個 高效 開源(MIT) 的 golang 日誌庫

[https://github.com/uber-go/zap](https://github.com/uber-go/zap)

```go
#info=false
go get -u -v go.uber.org/zap
```