# c++

vs 提供了 豐富的 插件 和 自定義 能力 可以 將其 配置到 合適 c++開發的 環境

此文 以 vscode 配合 gcc cmake 爲例

在vscode 中 安裝 插件
* c/c++	支持 c/c++ 語法提示高亮
* CMake	支持 cmake 語法提示 高亮

# tasks.json

創建 tasks.json 爲 c++項目 提供 build 和 run 的任務

```json
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        /***    設置 cmake 任務    ***/
        {
            "label": "cmake debug",
            "type": "shell",
            "command": "cmake -DCMAKE_BUILD_TYPE=Debug ${workspaceRoot}",
            "group": {
                "kind": "build",
            },
            "problemMatcher":"$gcc",
            "options": {
                //設置 task 工作目錄
                "cwd": "${workspaceRoot}/build/debug"
            },
        },
        {
            "label": "cmake release",
            "type": "shell",
            "command": "cmake -DCMAKE_BUILD_TYPE=Release ${workspaceRoot}",
            "group": {
                "kind": "build",
            },
            "problemMatcher":"$gcc",
            "options": {
                "cwd": "${workspaceRoot}/build/release"
            },
        },
        /***    設置 make 任務    ***/
        {
            "label": "make debug",
            "type": "shell",
            "command": "make",
            "group": {
                "kind": "build",
            },
            "problemMatcher":"$gcc",
            "options": {
                "cwd": "${workspaceRoot}/build/debug"
            },
        },
        {
            "label": "make release",
            "type": "shell",
            "command": "make",
            "group": {
                "kind": "build",
            },
            "problemMatcher":"$gcc",
            "options": {
                "cwd": "${workspaceRoot}/build/release"
            },
        },
        /***    設置 run 任務    ***/
        {
            "label": "run debug",
            "type": "shell",
            "command": "../debug/app",
            "problemMatcher":"$gcc",
            "options": {
                "cwd": "${workspaceRoot}/build/bin"
            },
        },
        {
            "label": "run release",
            "type": "shell",
            "command": "../release/app",
            "problemMatcher":"$gcc",
            "options": {
                "cwd": "${workspaceRoot}/build/bin"
            },
        },
    ]
}
```

# launch.json

創建 launch.json 爲 c++項目 提供 調試配置

```json
{
    // 使用 IntelliSense 以得知可用的屬性。
    // 暫留以檢視現有屬性的描述。
    // 如需詳細資訊，請瀏覽: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "(gdb) Launch",
            "type": "cppdbg",
            "request": "launch",
            //待調試的 程式
            "program": "${workspaceRoot}/build/debug/app",
            "args": [],
            "stopAtEntry": false,
            //程式 工作目錄
            "cwd": "${workspaceRoot}/build/bin",
            "environment": [],
            "externalConsole": true,
            "MIMode": "gdb",
            "setupCommands": [
                {
                    "description": "Enable pretty-printing for gdb",
                    "text": "-enable-pretty-printing",
                    "ignoreFailures": true
                }
            ]
        }
    ]
}
```

# c_cpp_properties.json
安裝 插件 cpptools 後

c_cpp_properties.json 可以 定義 一些 c++ 環境

```json
{
    "configurations": [
        {
            "name": "Linux",
            "intelliSenseMode": "clang-x64",
            "_commit":"//設置include目錄",
            "includePath": [
                "/usr/include/c++/5",
                "/usr/include/x86_64-linux-gnu/c++/5",
                "/usr/include/c++/5/backward",
                "/usr/lib/gcc/x86_64-linux-gnu/5/include",
                "/usr/local/include",
                "/usr/lib/gcc/x86_64-linux-gnu/5/include-fixed",
                "/usr/include/x86_64-linux-gnu",
                "/usr/include",
                "${workspaceRoot}"
            ]
        }
    ],
    "version": 3
}
```

# init-cpp-cmake.sh
這個 bash腳本 會在當前 目錄下 創建 c++ 開發 必要的 環境

```bash
#!/bin/bash
#Program:
#       創建/清空 vscode cmake 需要的 目錄
#History:
#       2018-02-07 king first release
#Email:
#       zuiwuchang@gmail.com
 
# 獲取 項目路徑作爲 項目 名稱
appName=`pwd`
appName=${appName##*/}
echo create project [ $appName ]
 
# 定義的 各種 輔助 函數
MkDir(){
	mkdir -p "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
MkOrClear(){
	if test -d "$1";then
		declare path="$1"
		path+="/*"
		rm "$path" -rf
		if [ "$?" != 0 ] ;then
			exit 1
		fi
	else
		MkDir $1
	fi
}
NewFile(){
	echo "$2" > "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
WriteFile(){
	echo "$2" >> "$1"
	if [ "$?" != 0 ] ;then
		exit 1
	fi
}
 
# 創建 bin 目錄
MkDir build/bin
 
# 創建/清空 debug release 目錄
MkOrClear build/debug
MkOrClear build/release
 
 
# 創建 cmake 檔案
fileName=CMakeLists.txt
if ! test -f $fileName ;then
	NewFile $fileName	'cmake_minimum_required(VERSION 3.0)'
	WriteFile $fileName	"project($appName)"
	WriteFile $fileName	'add_compile_options(-std=gnu++17)'
	WriteFile $fileName	"add_executable($appName main.cpp)"
fi
 
 
#創建 vscode 配置目錄
MkDir .vscode
 
# 創建 vscode tasks.json 檔案
fileName=.vscode/tasks.json
if ! test -f $fileName ;then
	NewFile $fileName	'{'
	WriteFile $fileName	'	"version": "2.0.0",'
	WriteFile $fileName	'	"type": "shell",'
	WriteFile $fileName	'	"tasks": ['
	WriteFile $fileName	'		/***    設置 cmake 任務    ***/'
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"label": "cmake debug",'
	WriteFile $fileName	'			"command": "cmake -DCMAKE_BUILD_TYPE=Debug ${workspaceRoot}",'
	WriteFile $fileName	'			"problemMatcher":"$gcc",'
	WriteFile $fileName	'			"group": {'
	WriteFile $fileName	'				"kind": "build",'
	WriteFile $fileName	'				"isDefault": true'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"options": { "cwd": "${workspaceRoot}/build/debug" }'
	WriteFile $fileName	'		},'
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"label": "cmake release",'
	WriteFile $fileName	'			"command": "cmake -DCMAKE_BUILD_TYPE=Release ${workspaceRoot}",'
	WriteFile $fileName	'			"problemMatcher":"$gcc",'
	WriteFile $fileName	'			"group": {'
	WriteFile $fileName	'				"kind": "build",'
	WriteFile $fileName	'				"isDefault": true'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"options": { "cwd": "${workspaceRoot}/build/release" }'
	WriteFile $fileName	'		},'
	WriteFile $fileName	'		/***    設置 make 任務    ***/'
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"label": "make debug",'
	WriteFile $fileName	'			"command": "make",'
	WriteFile $fileName	'			"problemMatcher":"$gcc",'
	WriteFile $fileName	'			"group": {'
	WriteFile $fileName	'				"kind": "build",'
	WriteFile $fileName	'				"isDefault": true'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"options": { "cwd": "${workspaceRoot}/build/debug" }'
	WriteFile $fileName	'		},'
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"label": "make release",'
	WriteFile $fileName	'			"command": "make",'
	WriteFile $fileName	'			"problemMatcher":"$gcc",'
	WriteFile $fileName	'			"group": {'
	WriteFile $fileName	'				"kind": "build",'
	WriteFile $fileName	'				"isDefault": true'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"options": { "cwd": "${workspaceRoot}/build/release" }'
	WriteFile $fileName	'		},'
	WriteFile $fileName	'		/***    設置 run 任務    ***/'
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"label": "run debug",'
	WriteFile $fileName	'			"command": "${workspaceRoot}/build/debug/'"${appName}"'",'
	WriteFile $fileName	'			"windows": {'
	WriteFile $fileName	'				"command": "${workspaceRoot}/build/debug/'"${appName}"'.exe",'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"problemMatcher":"$gcc",'
	WriteFile $fileName	'			"options": { "cwd": "${workspaceRoot}/build/bin" }'
	WriteFile $fileName	'		},'
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"label": "run release",'
	WriteFile $fileName	'			"command": "${workspaceRoot}/build/release/'"${appName}"'",'
	WriteFile $fileName	'			"windows": {'
	WriteFile $fileName	'				"command": "${workspaceRoot}/build/release/'"${appName}"'.exe",'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"problemMatcher":"$gcc",'
	WriteFile $fileName	'			"options": { "cwd": "${workspaceRoot}/build/bin" }'
	WriteFile $fileName	'		},'
	WriteFile $fileName	'	]'
	WriteFile $fileName	'}'
fi
# 創建 vscode launch.json 檔案
fileName=.vscode/launch.json
if ! test -f $fileName ;then
	NewFile $fileName	'{'
	WriteFile $fileName	'	"version": "0.2.0",'
	WriteFile $fileName	'	"configurations": ['
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"name": "(gdb) Launch",'
	WriteFile $fileName	'			"type": "cppdbg",'
	WriteFile $fileName	'			"request": "launch",'
	WriteFile $fileName	'			"program": "${workspaceRoot}/build/debug/'"$appName"'",'
	WriteFile $fileName	'			"windows": {'
	WriteFile $fileName	'				"program": "${workspaceRoot}/build/debug/'"$appName"'.exe",'
	WriteFile $fileName	'			},'
	WriteFile $fileName	'			"args": [],'
	WriteFile $fileName	'			"stopAtEntry": false,'
	WriteFile $fileName	'			"cwd": "${workspaceRoot}/build/bin",'
	WriteFile $fileName	'			"environment": [],'
	WriteFile $fileName	'			"externalConsole": true,'
	WriteFile $fileName	'			"MIMode": "gdb",'
	WriteFile $fileName	'			"setupCommands": ['
	WriteFile $fileName	'				{'
	WriteFile $fileName	'					"description": "Enable pretty-printing for gdb",'
	WriteFile $fileName	'					"text": "-enable-pretty-printing",'
	WriteFile $fileName	'					"ignoreFailures": true'
	WriteFile $fileName	'				}'
	WriteFile $fileName	'			]'
	WriteFile $fileName	'		}'
	WriteFile $fileName	'	]'
	WriteFile $fileName	'}'
fi
# 創建 vscode c_cpp_properties.json
fileName=.vscode/c_cpp_properties.json
if ! test -f $fileName ;then
	NewFile $fileName	'{'
	WriteFile $fileName	'	"configurations": ['
	WriteFile $fileName	'		{'
	WriteFile $fileName	'			"name": "Linux",'
	WriteFile $fileName	'			"intelliSenseMode": "clang-x64",'
	WriteFile $fileName	'			"includePath": ['
	WriteFile $fileName	'				"/usr/include/c++/5",'
	WriteFile $fileName	'				"/usr/include/x86_64-linux-gnu/c++/5",'
	WriteFile $fileName	'				"/usr/include/c++/5/backward",'
	WriteFile $fileName	'				"/usr/lib/gcc/x86_64-linux-gnu/5/include",'
	WriteFile $fileName	'				"/usr/local/include",'
	WriteFile $fileName	'				"/usr/lib/gcc/x86_64-linux-gnu/5/include-fixed",'
	WriteFile $fileName	'				"/usr/include/x86_64-linux-gnu",'
	WriteFile $fileName	'				"/usr/include",'
	WriteFile $fileName	'				"${workspaceRoot}"'
	WriteFile $fileName	'			]'
	WriteFile $fileName	'		}'
	WriteFile $fileName	'	]'
	WriteFile $fileName	'}'
fi
 
# 創建 vscode settings.json
fileName=.vscode/settings.json
if ! test -f $fileName ;then
	NewFile $fileName	'{'
	WriteFile $fileName	'	"files.exclude": {'
	WriteFile $fileName	'		"**/init-cpp-cmake.sh": true,'
	WriteFile $fileName	'		"**/build/release": true,'
	WriteFile $fileName	'		"**/build/release": true,'
	WriteFile $fileName	'	}'
	WriteFile $fileName	'}'
fi
```
