# 本地使用

rsync 可以作爲本地 cp 指令的 增強替代版

## -r 參數

本機可以使用 -r 將 檔案夾 拷貝到目標檔案夾下

```
rsync -r src dst
```

rsync 支持多個源 可以將多個源全部拷貝到目標檔案夾下

```
rsync -r src1 src2 dst
```

> 對於檔案的拷貝可以不使用 -r 參數

如果不想在 dst 目錄下創建 src 檔案夾 只想將 src下的 內容拷貝到 dst 下面 可以按照下面當方式寫

```
rsync -r src/ dst
rsync -r src1/ src2/ dst
```

## -a 參數

-r 參數只是遞歸拷貝檔案數據 如果要一起拷貝元信息(最後修改時間 檔案系統權限等) 需要使用 -a 參數。

因爲 rsync 默認使用檔案大小和最後修改時間判斷檔案是否有修改，故同步的話應該使用 -a 參數。

```
rsync -r src dst
rsync -a src1 src2 dst
```

## -n -v

如果向查看下 rsync 執行後會產生什麼後果 可以使用 -n 參數 即 --dry-run 的簡寫參數

使用 -v 參數 將 執行過程打印到控制檯

```
rsync -anv src/ dst
```

## --delete

默認情況下 rsync 只保證將源內容 拷貝到目標中，如果像將目標檔案夾下的 多於內容 刪除掉以成爲 源內容的鏡像 可以加上 ---delete 參數

```
rsync -av --delete src/ dst
rsync -av --delete src1/ src2/ dst
```

通常對於 --delete 不要使用 `src1/ src2/` 形式 因爲如果 src1 和 src2 下存在同名檔案 將只保留 src1 下的數據到目標 爲了避免問題應該使用 `src1 src2` 的形式

## -o 參數

-o 參數可以保留檔案原本的所有者與所屬組信息，通常備份都應該加上此參數，但是只有 root 執行 rsync 指令時 -o 參數才有效

```
rsync -aov --delete src/ dst
rsync -aov --delete src1/ src2/ dst
```
# 排除

rsync 支持在 同步數據時 排除指定的 檔案/檔案夾

## --exclude

--exclude 參數 用於指定要排除的 檔案/檔案夾

```
# 排除所有後綴爲的 .txt
rsync -av --exclude '*.txt' src/ dst

# 排除 file1.txt 和 dir1 檔案夾下的內容
rsync -av --exclude 'file1.txt' --exclude 'dir1/*' src/ dst
rsync -av --exclude={'file1.txt','dir1/*'} src/ dst

# 支持 從檔案讀取 exclude 規則 每個規則一行
rsync -av --exclude-from='exclude-file.txt' src/ dst
```

rsync 不會對src 和 dst 的排除檔案執行任何操作

## --include

--include 參數通常和 --exclude 配合使用 用來指定必須要同步的檔案

```
# 排除 .txt後綴之外的 所有 檔案/檔案夾
rsync -av --include='*.txt'--exclude='*' src/ dst
```

# 遠程同步

rsync 支持本地和遠程 同步 

## ssh 協議

rsync 支持 ssh 協議進行同步 地址寫 ssh 的形式即可

```
# 將本地同步到遠程
rsync -av src username@host:dst

# 將遠程 同步到本地
rsync -av username@host:src dst
```

實際上上述指令 省略了 -e ssh 當如果 需要附加額外的 ssh 參數 需要顯示指定 -e

```
# 將本地同步到遠程
rsync -av -e 'ssh -p 10022' src username@host:dst

# 將遠程 同步到本地
rsync -av -e 'ssh -p 10022' username@host:src dst
```

## rsync 協議

如果遠程安裝了 rsync 守衛進程 也可以使用 rsync 協議(默認 873 端口)

```
rsync -av src/ 192.168.122.32::module/dst
rsync -av src/ rsync://192.168.122.32/module/dst
```

上述 module 並非實際路徑而是 rsync 守衛進程指定的一個資源名稱

```
# 查看 module 列表
rsync rsync://192.168.122.32
```

# 增量備份

rsync 使用 --link-dest 可以指定一個基准以此來做增量備份，rsync 將數據源和 基準比較有變化的創建到目標目錄中，對於沒有變化的直接創建基準的硬連接

```
rsync -a --delete --link-dest /compare/path /source/path /target/path
```

# 其它參數

* -a --archive 同步元信息並且軟鏈接也會同步
* --append 指定檔案接着上次中斷的地方繼續傳輸
* --append-verify 類似 --append 但會對檔案進行效驗如果檔案發生變化則重新同步
* -b --backup 在刪除或更改已存在檔案時 將檔案改名備份 而不是刪除
* --backup-dir 指定 檔案備份時存放的檔案夾
* --bwlimit 指定帶寬限制 默認單位是 KB/s (--bwlimit=100)
* -c --checksum 設置rsync效驗方式。默認只檢查檔案大小和最後修改時間判斷是否發生變化
* --delete 將目標下多於的檔案刪除
* -e 指定 ssh 協議參數
* --exclude 指定要排除的同步內容
* --exclude-from 由檔案讀取 排除規則
* --existing --ignore-non-existing 不要同步目標檔案夾下不存在的內容
* -h --help 輸出人類幫助信息
* -i 輸出源和目標間的差異細節
* --ignore-existing 如果已存在的檔案 直接跳過不需要同步
* --include 指定要包含的內容
* --link-dest 指定增量備份 基準
* -m 不用同步空 檔案夾
* --max-size 設置傳輸的最大檔案大小 比如不超過200KB(--max-size='200k')
* --min-size 設置傳輸的最小檔案大小 比如不小於10KB(--min-size='10k')
* -n --dry-run 模擬執行
* -P 是 --progress 和 --partial 參數的簡寫
* --partial 參數允許恢復中斷的傳輸。不使用此參數時 rsync 會刪除傳輸到一半被打斷的檔案，一半需要和 --append 或--append-verify 一起使用
* --partial-dir 參數指定將傳輸到一半的檔案保存到一個臨時目錄
* --progress 顯示進度
* -r 遞歸子目錄
* --remove-source-files 同步成功後刪除 數據源
* --size-only 值同步大小有變化的
* --suffix 檔案備份時 添加的名稱 後綴 默認 ~
* -u --update 只同步更新的檔案 如果源的檔案更新時間比較舊則不更新
* -v 輸出細節
* -vv 輸出詳細的細節
* -vvv 輸出最詳細的細節
* --version 輸出 rsync 版本詳細
* -z 同步時壓縮數據