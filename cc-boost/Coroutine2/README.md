# [Coroutine2](https://www.boost.org/doc/libs/1_76_0/libs/coroutine2/doc/html/index.html)

```
#include <boost/coroutine2/all.hpp>
using namespace boost::coroutines2;
```
```
#info="cmake"
find_package(Boost
    1.76.0
    REQUIRED
    COMPONENTS 
        coroutine
)
```
* Author(s) -> Oliver Kowalke
* First Release -> 1.59.0
* C++ Standard Minimum Level -> 11
* Categories -> Concurrent Programming

coroutines2 提供了非對稱協程 每個協程都有自己完整的堆棧(Stackful Coroutines)

協程庫同時使用 boost::context 執行上下文切換 故同樣需要編譯和鏈接 context 庫， cmake 在查找coroutine時會自動加入堆 context 的依賴
# pull\_type push\_type

boost 提供了 兩種非對稱協程類型 pull_type 和 push_type

* pull\_type 接收一個帶有 push\_type 引用 的 仿函數，push\_type 重載了 operator(T) 用於將數據傳回給 pull\_type 並且切換上下文，當構造 pull\_type 時執行上下文會被切換到 push\_type
* pull\_type 則 提供了 get 返回當前值 以及 重載的 operator() 切換上下文
* pull\_type 提供了 coroutine<>::pull\_type::iterator 並且重載了 std::begin()/std::end() ，以便方便的使用 for 切換上下文和獲取數據
```
#include <iostream>
#include <boost/coroutine2/all.hpp>

int main(int argc, char *argv[])
{
    typedef boost::coroutines2::coroutine<int> coro_t;

    coro_t::pull_type source(
        [&](coro_t::push_type &sink)
        {
            int first = 1, second = 1;
            sink(first);
            sink(second);
            for (int i = 0; i < 8; ++i)
            {
                int third = first + second;
                first = second;
                second = third;
                sink(third);
            }
        });

    // while (source)
    // {
    //     std::cout << source.get() << " ";
    //     source();
    // }
    for (auto i : source)
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;
    return 0;
}
```

* push\_type 的構造類似 接收一個帶有 pull\_type 引用的 仿函數，push\_type 構造時不會切換上下文
* push\_type 接收的防函數 可能永遠不會執行到最後一行 因爲 push\_type 沒有將上下文切換回來 但棧內變量會被安全釋放 故應該利用 析構函數去釋放資源和執行 收尾工作

```
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <boost/coroutine2/all.hpp>

int main(int argc, char *argv[])
{
    typedef boost::coroutines2::coroutine<std::string> coro_t;

    struct FinalEOL
    {
        ~FinalEOL()
        {
            std::cout << std::endl;
        }
    };
    const int num = 5, width = 15;
    coro_t::push_type writer(
        [&](coro_t::pull_type &in)
        {
            // finish the last line when we leave by whatever means
            FinalEOL eol;
            // pull values from upstream, lay them out 'num' to a line
            for (;;)
            {
                for (int i = 0; i < num; ++i)
                {
                    // when we exhaust the input, stop
                    if (!in)
                        return;

                    std::cout << std::setw(width) << in.get();
                    // now that we've handled this item, advance to next
                    in();
                }
                // after 'num' items, line break
                std::cout << std::endl;
            }
        });

    std::vector<std::string> words{
        "peas", "porridge", "hot", "peas",
        "porridge", "cold", "peas", "porridge",
        "in", "the", "pot", "nine",
        "days", "old"};

    std::copy(begin(words), end(words), begin(writer));
    return 0;
}
```

# 異常

1. 在第一次調用 coroutine&lt;&gt;::push\_type::operator\(\) 前 pull\_type 的協程拋出的異常將由 pull\_type 的構造函數重新拋出
2. 在調用過 coroutine&lt;&gt;::push\_type::operator\(\) 之後 pull\_type 的協程拋出的異常將由 coroutine&lt;&gt;::pull\_type::operator\(\) 重新拋出 coroutine&lt;&gt;::pull\_type::get\(\) 不會拋出異常

異常函數不能阻止 detail::forced\_unwind 異常的傳播 否則 會導致堆棧展開失敗 故捕獲了異常的協程函數 需要重寫拋出 detail::forced\_unwind

```
try {
    // code that might throw
} catch(const boost::coroutines2::detail::forced_unwind&) {
    throw;
} catch(...) {
    // possibly not re-throw pending exception
}
```