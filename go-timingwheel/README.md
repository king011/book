# timingwheel

timingwheel 是一個 開源(MIT) 的 層級時間輪

* 官網 [http://russellluo.com/2018/10/golang-implementation-of-hierarchical-timing-wheels.html](http://russellluo.com/2018/10/golang-implementation-of-hierarchical-timing-wheels.html)
* 源碼 [https://github.com/RussellLuo/timingwheel](https://github.com/RussellLuo/timingwheel)

```
go get github.com/RussellLuo/timingwheel
```