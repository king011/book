# 模板變量

```
template.defaults.imports
```

通過 $imports 可以訪問 模板外部的全局變量與導入變量

```
template.defaults.imports.log = console.log;
```

```
<% $imports.log('hello world') %>
```

# 內置變量

* $data 傳入模板的數據
* $imports 外部導入的變量以及全局變量
* print 字符串輸出函數
* include 子模板載入函數
* extend 模板繼承導入函數
* block 模板聲明函數