# @alpha
* Standardization:	Discretionary
* Syntax kind:	Modifier

將 api 項的發佈爲 alpha。它用於告訴第三方人員，該 api 尚未發佈。可能會從公開發佈中刪除


```
/**
 * Represents a book in the catalog.
 * @public
 */
export class Book {
  /**
   * The title of the book.
   * @alpha
   */
  public get title(): string;

  /**
   * The author of the book.
   */
  public get author(): string;
};
```

# @beta

* Standardization:	Discretionary
* Syntax kind:	Modifier
* Synonyms:	@experimental

標記 api 處於發佈階段的 測試版，爲了收集反饋，它已經試驗性的發佈給第三方開發人員。API 不應該在生成中使用，因爲可能會被更改，不另行通知。api 可能會從公開版本中刪除，但可能將其包含在開發人員預覽版本中

```
/**
 * Represents a book in the catalog.
 * @public
 */
export class Book {
  /**
   * The title of the book.
   * @beta
   */
  public get title(): string;

  /**
   * The author of the book.
   */
  public get author(): string;
};
```

# @decorator

* Standardization:	Extended
* Syntax kind:	Block tag

裝飾器有時是 API 簽約的重要組成部分。但是 TypeScript 編譯器並不代表 API 使用的 .d.ts 輸出檔案中的裝飾器。@decorator 提供了一種解決方法，允許在檔案註釋中引用裝飾器表達式

```
class Book {
  /**
   * The title of the book.
   * @decorator `@jsonSerialized`
   * @decorator `@jsonFormat(JsonFormats.Url)`
   */
  @jsonSerialized
  @jsonFormat(JsonFormats.Url)
  public website: string;
}
```

# @deprecated

* Standardization:	Core
* Syntax kind:	Block tag

標記 api 不在受支持，可能會在未來的版本中被刪除。 @deprecated 標記後跟一段短句，用於描述推薦的替代方案。

```
/**
 * The base class for controls that can be rendered.
 *
 * @deprecated Use the new {@link Control} base class instead.
 */
export class VisualControl {
  . . .
}
```

# @defaultValue

* Standardization:	Extended
* Syntax kind:	Block tag

用於標記字段或屬性的默認值。此標籤只能與 class 或 interface 的成員字段或屬性一起使用

```
enum WarningStyle {
  DialogBox,
  StatusMessage,
  LogOnly
}

interface IWarningOptions {
  /**
   * Determines how the warning will be displayed.
   *
   * @remarks
   * See {@link WarningStyle| the WarningStyle enum} for more details.
   *
   * @defaultValue `WarningStyle.DialogBox`
   */
  warningStyle: WarningStyle;

  /**
   * Whether the warning can interrupt a user's current activity.
   * @defaultValue
   * The default is `true` unless
   *  `WarningStyle.StatusMessage` was requested.
   */
  cancellable?: boolean;

  /**
   * The warning message
   */
  message: string;
}
```

# @eventProperty
* Standardization:	Extended
* Syntax kind:	Modifier

當應用與 class 和 interface 的屬性時。表明該屬性返回事件處理程序可以附加到的事件對象。事件處理 API 是程序定義的，但通常具有 addHandler removeHandler 等成員函數。文檔工具可以在 **Events** 標題 而非 **Properties** 標題下顯示此內容

```
class MyClass {
  /**
    * This event is fired whenever the application navigates to a new page.
    * @eventProperty
    */
  public readonly navigatedEvent: FrameworkEvent<NavigatedEventArgs>;
}
```

# @example

* Standardization:	Extended
* Syntax kind:	Block tag

用於展示如何使用 API。它可能包含一個代碼示例。任何與 @example 在同行的文本都應該被解釋爲標題，否則文檔工具可以使用數字對其進行索引


下面例子生成的標題可能是 **Example** 和 **Example2**
```
/**
 * Adds two numbers together.
 * @example
 * Here's a simple example:
 * ```
 * // Prints "2":
 * console.log(add(1,1));
 * ```
 * @example
 * Here's an example with negative numbers:
 * ```
 * // Prints "0":
 * console.log(add(1,-1));
 * ```
 */
export function add(x: number, y: number): number {
}
```

下面例子生成的標題可能是 **Example: Parsing a basic JSON file**

```
/**
 * Parses a JSON file.
 *
 * @param path - Full path to the file.
 * @returns An object containing the JSON data.
 *
 * @example Parsing a basic JSON file
 *
 * # Contents of `file.json`
 * ```json
 * {
 *   "exampleItem": "text"
 * }
 * ```
 *
 * # Usage
 * ```ts
 * const result = parseFile("file.json");
 * ```
 *
 * # Result
 * ```ts
 * {
 *   exampleItem: 'text',
 * }
 * ```
 */
```

# @experimental

* Standardization:	Discretionary
* Syntax kind:	Modifier
* Synonyms:	@beta

與 @beta 語義相同，但由不支持 @alpha 發佈階段的工具使用

```
/**
 * Represents a book in the catalog.
 * @public
 */
export class Book {
  /**
   * The title of the book.
   * @experimental
   */
  public get title(): string;

  /**
   * The author of the book.
   */
  public get author(): string;
};
```

# @inheritDoc

* Standardization:	Extended
* Syntax kind:	Inline tag

用於通過另一個 API 項複製它來自動生成 API 項。內聯標記參數包含對其它項的引用，該項可能是不相關的 class 甚至從單獨的 NPM 包中導入

@inheritDoc 不會複製所有內容，僅僅複製如下內容

* summary section
* @remarks block
* @params blocks
* @typeParam blocks
* @returns block

@defaultValue @example  等其它標籤不會被複製，需要在 @inheritDoc 標籤之後顯示包含。當指定 @inheritDoc 標籤時，不能在註釋中指定摘要部分 和 @remarks 部分

```
import { Serializer } from 'example-library';

/**
 * An interface describing a widget.
 * @public
 */
export interface IWidget {
  /**
   * Draws the widget on the display surface.
   * @param x - the X position of the widget
   * @param y - the Y position of the widget
   */
  public draw(x: number, y: number): void;
}

/** @public */
export class Button implements IWidget {
  /** {@inheritDoc IWidget.draw} */
  public draw(x: number, y: number): void {
    . . .
  }

  /**
   * {@inheritDoc example-library#Serializer.writeFile}
   * @deprecated Use {@link example-library#Serializer.writeFile} instead.
   */
  public save(): void {
    . . .
  }
}
```

# @internal

* Standardization:	Discretionary
* Syntax kind:	Modifier

指定第三方開發人員不能使用的 api 項。該 api 可能會從公開發佈中刪除聲明。在一些實現中，可以允許某些指定的包使用內部 API 項，例如 這些包是同一產品的組件

```
/**
 * Represents a book in the catalog.
 * @public
 */
export class Book {
  /**
   * The title of the book.
   * @internal
   */
  public get _title(): string;

  /**
   * The author of the book.
   */
  public get author(): string;
};
```

# @label
* Standardization:	Core
* Syntax kind:	Inline tag

**{@label}** 用於標記聲明，以便 可以使用 TSDoc 聲明引用表示法中的選擇器來引用它

```
export interface Interface {
  /**
   * Shortest name:  {@link InterfaceL1.(:STRING_INDEXER)}
   * Full name:      {@link (InterfaceL1:interface).(:STRING_INDEXER)}
   *
   * {@label STRING_INDEXER}
   */
  [key: string]: number;

  /**
   * Shortest name:  {@link InterfaceL1.(:NUMBER_INDEXER)}
   * Full name:      {@link (InterfaceL1:interface).(:NUMBER_INDEXER)}
   *
   * {@label NUMBER_INDEXER}
   */
  [key: number]: number;

  /**
   * Shortest name:  {@link InterfaceL1.(:FUNCTOR)}
   * Full name:      {@link (InterfaceL1:interface).(:FUNCTOR)}
   *
   * {@label FUNCTOR}
   */
  (source: string, subString: string): boolean;

  /**
   * Shortest name:  {@link InterfaceL1.(:CONSTRUCTOR)}
   * Full name:      {@link (InterfaceL1:interface).(:CONSTRUCTOR)}
   *
   * {@label CONSTRUCTOR}
   */
  new (hour: number, minute: number);
}
```

# @link

* Standardization:	Core
* Syntax kind:	Inline tag

{@link} 用於創建指向文檔系統中其它頁面或一般互聯網 URL 的超鏈接。特別是，它支持引用 API 項的表達式

```
/**
 * Let's learn about the `{@link}` tag.
 *
 * @remarks
 *
 * Links can point to a URL: {@link https://github.com/microsoft/tsdoc}
 *
 * Links can point to an API item: {@link Button}
 *
 * You can optionally include custom link text: {@link Button | the Button class}
 *
 * Suppose the `Button` class is part of an external package.  In that case, we
 * can include the package name when referring to it:
 *
 * {@link my-control-library#Button | the Button class}
 *
 * The package name can include an NPM scope and import path:
 *
 * {@link @microsoft/my-control-library/lib/Button#Button | the Button class}
 *
 * The TSDoc standard calls this notation a "declaration reference".  The notation supports
 * references to many different kinds of TypeScript declarations.  This notation was originally
 * designed for use in `{@link}` and `{@inheritDoc}` tags, but you can also use it in your
 * own custom tags.
 *
 * For example, the `Button` can be part of a TypeScript namespace:
 *
 * {@link my-control-library#controls.Button | the Button class}
 *
 * We can refer to a member of the class:
 *
 * {@link controls.Button.render | the render() method}
 *
 * If a static and instance member have the same name, we can use a selector to distinguish them:
 *
 * {@link controls.Button.(render:instance) | the render() method}
 *
 * {@link controls.Button.(render:static) | the render() static member}
 *
 * This is also how we refer to the class's constructor:
 *
 * {@link controls.(Button:constructor) | the class constructor}
 *
 * Sometimes a name has special characters that are not a legal TypeScript identifier:
 *
 * {@link restProtocol.IServerResponse."first-name" | the first name property}
 *
 * Here is a fairly elaborate example where the function name is an ECMAScript 6 symbol,
 * and it's an overloaded function that uses a label selector (defined using the `{@label}`
 * TSDoc tag):
 *
 * {@link my-control-library#Button.([UISymbols.toNumberPrimitive]:OVERLOAD_1)
 * | the toNumberPrimitive() static member}
 *
 * See the TSDoc spec for more details about the "declaration reference" notation.
 */
```

# @override

* Standardization:	Extended
* Syntax kind:	Modifier

與 c# 或 java 中的 override 關鍵字具有相同的語義。對與成員函數或屬性，明確表示此定義正在覆蓋(即重新定義) 從基類繼承的定義。基類定義通常會被標記爲 @virtual

```
class Base {
  /** @virtual */
  public render(): void {
  }

  /** @sealed */
  public initialize(): void {
  }
}

class Child extends Base {
  /** @override */
  public render(): void;
}
```

# @packageDocumentation

* Standardization:	Core
* Syntax kind:	Modifier

用於描述整個 NPM 包的文檔註釋。@packageDocumentation 註釋位於作爲包入口點的 .d.ts 檔案中，它應該是該檔案中遇到的第一個 /\*\* 註釋。包含 @packageDocumentation 標籤的註釋絕對不應該用於描述單個 api 項

```
// Copyright (c) Example Company. All rights reserved. Licensed under the MIT license.

/**
 * A library for building widgets.
 *
 * @remarks
 * The `widget-lib` defines the {@link IWidget} interface and {@link Widget} class,
 * which are used to build widgets.
 *
 * @packageDocumentation
 */

/**
 * Interface implemented by all widgets.
 * @public
 */
export interface IWidget {
  /**
   * Draws the widget on the screen.
   */
  render(): void;
}
```

# @param

* Standardization:	Core
* Syntax kind:	Block tag

用於基類函數參數。@param 標記後跟參數名稱、連字符和描述。

```
/**
 * Returns the average of two numbers.
 *
 * @remarks
 * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
 *
 * @param x - The first input number
 * @param y - The second input number
 * @returns The arithmetic mean of `x` and `y`
 *
 * @beta
 */
function getAverage(x: number, y: number): number {
  return (x + y) / 2.0;
}
```

# @privateRemarks

* Standardization:	Core
* Syntax kind:	Block tag

開始一段不面向公衆的附加文檔內容。工具必須從 API 參考網站 生成的 .d.ts 檔案已經包含該內容的任何其它輸出中省略這整個部分

```
/**
 * The summary section should be brief. On a documentation web site,
 * it will be shown on a page that lists summaries for many different
 * API items.  On a detail page for a single item, the summary will be
 * shown followed by the remarks section (if any).
 *
 * @remarks
 *
 * The main documentation for an API item is separated into a brief
 * "summary" section optionally followed by an `@remarks` block containing
 * additional details.
 *
 * @privateRemarks
 *
 * The `@privateRemarks` tag starts a block of additional commentary that is not meant
 * for an external audience.  A documentation tool must omit this content from an
 * API reference web site.  It should also be omitted when generating a normalized
 * *.d.ts file.
 */
```

# @public

* Standardization:	Discretionary
* Syntax kind:	Modifier

只是 api 處於發佈階段的 公開狀態。它已經正式發佈給第三方開發者，並且其簽名保證是穩定。

```
/**
 * Represents a book in the catalog.
 * @public
 */
export class Book {
  /**
   * The title of the book.
   * @internal
   */
  public get _title(): string;

  /**
   * The author of the book.
   */
  public get author(): string;
};
```

# @readonly

* Standardization:	Extended
* Syntax kind:	Modifier

標記 api 項目爲只讀，即使 TypeScript 類型系統中可能另有說明。例如，假設一個 setter 函數始終拋出異常說明該屬性不能被賦值，在這種情況下可以添加 @readonly 修飾符，以便在文檔中顯示爲只讀

```
export class Book {
  /**
   * Technically property has a setter, but for documentation purposes it should
   * be presented as readonly.
   * @readonly
   */
  public get title(): string {
    return this._title;
  }

  public set title(value: string) {
    throw new Error('This property is read-only!');
  }
}
```

# @remarks

* Standardization:	Core
* Syntax kind:	Block tag

api 項目主要文檔被分成一個簡短的“摘要”部分，然後可以選擇更詳細的“備註”部分。在文檔網站上，索引頁面將僅顯示簡要的摘要，而詳細頁面將顯示摘要和備註。@remarks 塊標記結束“摘要”部分，並且開始文檔註釋的“備註”部分

# @returns

* Standardization:	Core
* Syntax kind:	Block tag

標記函數的返回值

```
/**
 * Returns the average of two numbers.
 *
 * @remarks
 * This method is part of the {@link core-library#Statistics | Statistics subsystem}.
 *
 * @param x - The first input number
 * @param y - The second input number
 * @returns The arithmetic mean of `x` and `y`
 *
 * @beta
 */
function getAverage(x: number, y: number): number {
  return (x + y) / 2.0;
}
```

# @sealed

* Standardization:	Extended
* Syntax kind:	Modifier

標記一個類不能被繼承，或者標記一個 函數/屬性 不能被子類覆蓋(重新定義)

文檔工具可以強制一致的應用 @virtual @override @sealed，但這不是 TSDoc 標準所要求的 

```
class Base {
  /** @virtual */
  public render(): void {
  }

  /** @sealed */
  public initialize(): void {
  }
}

class Child extends Base {
  /** @override */
  public render(): void;
}
```

# @see

* Standardization:	Extended
* Syntax kind:	Block tag

用於構建對 API 項或可能與當前項相關的其它資源的引用列表

```
/**
 * Parses a string containing a Uniform Resource Locator (URL).
 * @see {@link ParsedUrl} for the returned data structure
 * @see {@link https://tools.ietf.org/html/rfc1738|RFC 1738}
 * for syntax
 * @see your developer SDK for code samples
 * @param url - the string to be parsed
 * @returns the parsed result
 */
function parseURL(url: string): ParsedUrl;
```

@see 是一個塊標籤。每個塊都成爲引用列表中的一個項目。例如文檔系統可能會按如下方式呈現上述塊

```
`function parseURL(url: string): ParsedUrl;`

Parses a string containing a Uniform Resource Locator (URL).

## See Also
- ParsedUrl for the returned data structure
- RFC 1738 for syntax
- your developer SDK for code samples
```

# @throws

* Standardization:	Extended
* Syntax kind:	Block tag

用於標記函數或屬性可能引發的異常類型

應使用單獨的 @throws 來記錄每種異常類型。此標籤僅作爲參考，不限制拋出其它類型。

```
/**
 * Retrieves metadata about a book from the catalog.
 *
 * @param isbnCode - the ISBN number for the book
 * @returns the retrieved book object
 *
 * @throws {@link IsbnSyntaxError}
 * This exception is thrown if the input is not a valid ISBN number.
 *
 * @throws {@link book-lib#BookNotFoundError}
 * Thrown if the ISBN number is valid, but no such book exists in the catalog.
 *
 * @public
 */
function fetchBookByIsbn(isbnCode: string): Book;
```

# @typeParam
* Standardization:	Core
* Syntax kind:	Block tag

用於記錄模板參數

```
/**
 * Alias for array
 *
 * @typeParam T - Type of objects the list contains
 */
type List<T> = Array<T>;

/**
 * Wrapper for an HTTP Response
 * @typeParam B - Response body
 * @param <H> - Headers
 */
interface HttpResponse<B, H> {
    body: B;
    headers: H;
    statusCode: number;
}
```

# @virtual

* Standardization:	Extended
* Syntax kind:	Modifier

明確指定子類可以覆蓋此 函數/屬性

文檔工具可以強制一致的應用 @virtual @override @sealed，但這不是 TSDoc 標準所要求的 

```
class Base {
  /** @virtual */
  public render(): void {
  }

  /** @sealed */
  public initialize(): void {
  }
}

class Child extends Base {
  /** @override */
  public render(): void;
}
```