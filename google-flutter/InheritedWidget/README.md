# InheritedWidget

InheritedWidget 是一個 非常重要的 功能型 widget 可以高效的將數據 在 widget 樹中向下 傳遞 來實現 數據共享

flutter 正是通過 InheritedWidget 來實現的在 widget 間共享 Theme Locale 

# didChangeDependencies

在 StatefulWidget 的 State 對象中 會有個 didChangeDependencies 回調 它會在依賴發送變化時被 flutter 調用

這個 依賴這是 祖先 widget 中的 數據

# 使用 InheritedWidget

1. 從 InheritedWidget 派生一個 子類 例子中 爲 class ShareDataWidget

   ```dart
   // 從 InheritedWidget 派生
   class ShareDataWidget extends InheritedWidget {
     ShareDataWidget({@required this.data, Widget child}) : super(child: child);
   
     // 在子樹中 要共享的 數據
     final int data;
   
     // 定義一個便捷方法 讓子類獲取到 ShareDataWidget 實例
     static ShareDataWidget of(BuildContext context) {
       return context.inheritFromWidgetOfExactType(ShareDataWidget);
     }
   
     // 該回調返回 當 data 變化時 是否通知子樹 依賴變化
     @override
     bool updateShouldNotify(ShareDataWidget old) {
       // 如果 返回 ture 子widget 的 state.didChangeDependencies 會被調用
       //
       // 之後會 重建子 widget
       return old.data != data;
     }
   }
   ```

1. 創建 ShareDataWidget 到 佈局 並設置 共享 數據

1. 在ShareDataWidget 子樹中 調用 ShareDataWidget.of 獲取 ShareDataWidget 實例

![](assets/InheritedWidget.gif)

```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate,
      ],
      localeResolutionCallback: localeResolutionCallback,
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // 共享 數據
  int _counter = 0;
  void _incrementCounter() {
    // setState 會標記 _MyHomePageState dirty 從而 重新 build
    //
    // 於是 ShareDataWidget 被重建 所以 ShareDataWidget.data 也會被設置爲新值
    //
    // ShareDataWidget.data 改變 導致 ShareDataWidget.updateShouldNotify 返回true 依賴的 子樹也會被重新 build
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appName),
      ),
      body: ShareDataWidget(
        data: _counter,
        child: ListView(
          children: <Widget>[
            CardView(name: "card 1"),
            CardView(name: "card 2"),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter, //用戶單擊調用 函數 修改狀態
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

// 從 InheritedWidget 派生
class ShareDataWidget extends InheritedWidget {
  ShareDataWidget({@required this.data, Widget child}) : super(child: child);

  // 在子樹中 要共享的 數據
  final int data;

  // 定義一個便捷方法 讓子類獲取到 ShareDataWidget 實例
  static ShareDataWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(ShareDataWidget);
  }

  // 該回調返回 當 data 變化時 是否通知子樹 依賴變化
  @override
  bool updateShouldNotify(ShareDataWidget old) {
    // 如果 返回 ture 子widget 的 state.didChangeDependencies 會被調用
    //
    // 之後會 重建子 widget
    return old.data != data;
  }
}

class CardView extends StatefulWidget {
  CardView({this.name, Key key}) : super(key: key);
  final String name;
  @override
  _CardViewState createState() => _CardViewState();
}

class _CardViewState extends State<CardView> {
  @override
  Widget build(BuildContext context) {
    print("${widget.name} build");
    // 使用 InheritedWidget 中的共享数据
    return Text("${widget.name} = ${ShareDataWidget.of(context).data}");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // 依賴發生變化
    print("${widget.name} Dependencies change");
  }
}
```