# [Buffer](https://github.com/jerryscript-project/iotjs/blob/master/docs/api/IoT.js-API-Buffer.md)

class Buffer 提供二進制操作的能力，目前是一個純 es5 的兼容實現，但日後可能會使用使用 UInt8Array 重新實現

```
import Buffer from "buffer";

// 創建一個使用 0 填充，長度爲 10 的 Buffer
var buf1 = new Buffer(10)

// 使用二進制數組創建 Buffer
var buf2 = new Buffer([1, 2, 3])

// 由 UTF-8 字符串創建 Buffer [0x74, 0xc3, 0xa9, 0x73, 0x74]
var buf3 = new Buffer('tést')
```

# class Buffer

```
/**
 * 對 bytes 的包裝
 */
export class Buffer {
		/**
		 * @param s 
		 * @param encoding 
		 * @returns new Buffer(s, encoding).length
		 */
		static byteLength(s: string, encoding: 'utf8' | 'hex' = 'utf8'): number
		static concat(list: Array<Buffer>): Buffer
		static from(s: string, encoding: 'utf8' | 'hex' = 'utf8'): Buffer
		static from(data: ArrayLike<number> | Buffer, offset = 0, length?: number): Buffer
		static isBuffer(o: any): o is Buffer
		/**
		 * 創建 size 大小的 bytes，bytes 全部設置爲 0
		 * @param size 
		 */
		constructor(size: number)
		/**
		 * 創建 other 的副本
		 * @param other 
		 */
		constructor(other: Buffer)
		/**
		 * 由 bytes 數組創建 bytes
		 * @param data 
		 */
		constructor(data: ArrayLike<number>)
		/**
		 * 由文本創建 bytes
		 * @param text
		 * @param encoding
		 */
		constructor(text: string, encoding: 'utf8' | 'hex' = 'utf8')

		/**
		 * 返回 bytes 長度
		 */
		readonly length: number
		/**
		 * 與另外一個 Buffer 比較大小
		 * @param other 
		 */
		ompare(other: Buffer): number

		/**
		 * 將 buffer 存儲的 bytes 拷貝到 target 中
		 * @param target 
		 * @param targetStart 
		 * @param sourceStart 
		 * @param sourceEnd 
		 */
		copy(target: Buffer, targetStart = 0, sourceStart = 0, sourceEnd?: number)
		/**
		 * 比較是否相等
		 * @param other 
		 */
		equals(other: Buffer): boolean

		/**
		 * 
		 * 將 bytes 全部設置爲 value
		 * @param value 
		 */
		fill(value: number): Buffer
		/**
		 * 創建一個 bytes 切片的副本
		 * @param start 
		 * @param end 
		 */
		slice(start = 0, end?: number): Buffer
		/**
		 * 將 bytes 轉爲 utf8 字符串
		 * @param start 
		 * @param end 
		 */
		toString(start = 0, end?: number): string
		/**
		 * 將 bytes 轉爲 hex 字符串
		 * @param encoding 
		 */
		toString(encoding: 'hex')

		/**
		 * 將字符串 s 寫入到 bytes 中指定位置
		 * @param s 要寫入的內容
		 * @param offset 寫入偏移
		 * @param length 最多寫入長度
		 * @returns 返回實際寫入長度
		 */
		write(s: string, offset = 0, length?: number): number

		/**
		 * 寫入 1 字節
		 * @param value 
		 * @param offset 
		 * @param noAssert 
		 */
		writeUInt8(value: number, offset = 0, noAssert = false): number
		/**
		 * 寫入 2 字節
		 * @param value 
		 * @param offset 
		 * @param noAssert 
		 */
		writeUInt16LE(value: number, offset = 0, noAssert = false): number
		/**
		 * 寫入 4 字節
		 * @param value 
		 * @param offset 
		 * @param noAssert 
		 */
		writeUInt32LE(value: number, offset = 0, noAssert = false): number
		/**
		 * 讀取 1 字節
		 * @param offset 
		 * @param noAssert 
		 */
		readInt8(offset = 0, noAssert = false): number
		/**
		 * 讀取 1 字節
		 * @param offset 
		 * @param noAssert 
		 */
		readUInt8(offset = 0, noAssert = false): number
		/**
		 * 讀取 2 字節
		 * @param offset 
		 * @param noAssert 
		 */
		readUInt16LE(offset = 0, noAssert = false): number
}
```

讀取寫入字節時 官方文檔沒有說明字節序是大端/小端還是和平臺一樣，這對於編碼時可能會產生問題，正確的做法是先判斷平臺是大端還是小端，然後明確的依據平臺存儲方式來寫入大端/小端字節

```typescript
import Buffer from "buffer";
export interface ByteOrder {
    uint16(b: Buffer, offset?: number): number
    uint32(b: Buffer, offset?: number): number
    putUint16(value: number, b: Buffer, offset?: number): void
    putUint32(value: number, b: Buffer, offset?: number): void
    toString(): string
}
class littleEndian {
    constructor(public readonly name: string) { }
    uint16(b: Buffer, offset?: number): number {
        offset = offset ?? 0
        const a0 = b.readUInt8(offset)
        const a1 = b.readUInt8(offset + 1)
        return a0 | (a1 << 8)
    }
    uint32(b: Buffer, offset?: number): number {
        offset = offset ?? 0
        const a0 = b.readUInt8(offset)
        const a1 = b.readUInt8(offset + 1)
        const a2 = b.readUInt8(offset + 2)
        const a3 = b.readUInt8(offset + 3)
        return a0 | (a1 << 8) | (a2 << 16) | (a3 << 24)
    }
    putUint16(value: number, b: Buffer, offset?: number): void {
        offset = offset ?? 0
        const a0 = value & 0xff
        const a1 = (value >> 8) & 0xff
        b.writeUInt8(a1, offset + 1)
        b.writeUInt8(a0, offset)
    }
    putUint32(value: number, b: Buffer, offset?: number): void {
        offset = offset ?? 0
        const a0 = value & 0xff
        const a1 = (value >> 8) & 0xff
        const a2 = (value >> 16) & 0xff
        const a3 = (value >> 24) & 0xff
        b.writeUInt8(a3, offset + 3)
        b.writeUInt8(a2, offset + 2)
        b.writeUInt8(a1, offset + 1)
        b.writeUInt8(a0, offset)
    }
    toString(): string {
        return this.name
    }
}
class bigEndian {
    constructor(public readonly name: string) { }
    uint16(b: Buffer, offset?: number): number {
        offset = offset ?? 0
        const a0 = b.readUInt8(offset)
        const a1 = b.readUInt8(offset + 1)
        return (a0 << 8) | a1
    }
    uint32(b: Buffer, offset?: number): number {
        offset = offset ?? 0
        const a0 = b.readUInt8(offset)
        const a1 = b.readUInt8(offset + 1)
        const a2 = b.readUInt8(offset + 2)
        const a3 = b.readUInt8(offset + 3)
        return (a0 << 24) | (a1 << 16) | (a2 << 8) | a3
    }
    putUint16(value: number, b: Buffer, offset?: number): void {
        offset = offset ?? 0
        const a0 = value & 0xff
        const a1 = (value >> 8) & 0xff
        b.writeUInt8(a0, offset + 1)
        b.writeUInt8(a1, offset)
    }
    putUint32(value: number, b: Buffer, offset?: number): void {
        offset = offset ?? 0
        const a0 = value & 0xff
        const a1 = (value >> 8) & 0xff
        const a2 = (value >> 16) & 0xff
        const a3 = (value >> 24) & 0xff
        b.writeUInt8(a0, offset + 3)
        b.writeUInt8(a1, offset + 2)
        b.writeUInt8(a2, offset + 1)
        b.writeUInt8(a3, offset)
    }
    toString(): string {
        return this.name
    }
}
function _isLittleEndian(): boolean {
    const buf = new ArrayBuffer(2)
    const view = new DataView(buf)
    view.setUint16(0, 1, true)
    const arrs = new Int16Array(buf)
    return arrs[0] === 1
}

export const isLittleEndian = _isLittleEndian()
export const LittleEndian = isLittleEndian ? new littleEndian('LittleEndian') : new bigEndian('LittleEndian')
export const BigEndian = isLittleEndian ? new bigEndian('BigEndian') : new littleEndian('BigEndian')
```

# ArrayBuffer/DataView

雖然目前沒看到文檔，但測試發現 iotjs 支持了 標準的 ArrayBuffer/DataView，使用它們處理二進制似乎更加合適