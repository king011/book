# 自動完成文本 AutoCompleteTextView

[android.widget.AutoCompleteTextView](https://developer.android.com/reference/android/widget/AutoCompleteTextView) 是 EditText 的子類 爲 輸入文本 提供了 自動完成的功能

使用類似 EditText 不過提供了 setAdapter 函數 用來設置 自動完成 的候選文本

![](assets/AutoCompleteTextView.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center">


        <AutoCompleteTextView
            android:id="@+id/autoCompleteTextView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />

    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    static final private String[] COUNTRIES = new String[]{"kate","king","anna","anita"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AutoCompleteTextView view = findViewById(R.id.autoCompleteTextView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,COUNTRIES);
        view.setAdapter(adapter);
    }

}
```

| 屬性 | 含義 |
| -------- | -------- |
| android:completionHint     | 爲彈出的候選下拉菜單指定提示標題     |
| android:completionThreshold     | 用戶至少輸入幾個字符才顯示候選列表     |
| android:dropDownHeight     | 候選列表高度     |
| android:dropDownWidth     | 候選列表寬度     |
| android:dropDownHorizontalOffset	     | 候選文本間的 水平偏移     |
| android:dropDownVerticalOffset     | 候選文本間的 垂直偏移     |
| android:popupBackground     | 候選列表背景     |

# 進度條 ProgressBar

[android.widget.ProgressBar](https://developer.android.com/reference/android/widget/ProgressBar) 提供了多種 進度條

* void setProgress (int progress) 設置進度
* void incrementProgressBy (int diff) 增加/減少 進度

![](assets/ProgressBar.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center"
        android:padding="10dp">

        <GridLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            android:columnCount="2"
            android:useDefaultMargins="true"
            >
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="水平"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                style="@android:style/Widget.ProgressBar.Horizontal"
                android:progress="25"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="默認"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"/>

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="小圓"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                style="@android:style/Widget.ProgressBar.Small"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="大圓"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                style="@android:style/Widget.ProgressBar.Large"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="逆向 圓"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                style="@android:style/Widget.ProgressBar.Inverse"
                />

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="逆向 小圓"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                style="@android:style/Widget.ProgressBar.Small.Inverse"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="逆向 大圓"/>
            <ProgressBar
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                style="@android:style/Widget.ProgressBar.Large.Inverse"
                />

        </GridLayout>
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

| 屬性 | 含義 |
| -------- | -------- |
| android:max     | 進度條最大值     |
| android:progress     | 進度條當前值     |

# 滑塊 SeekBar

[android.widget.SeekBar](https://developer.android.com/reference/android/widget/SeekBar) 是ProgressBar的子孫類 提供了供用戶 拖動的滑塊

![](assets/SeekBar.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center">

        <SeekBar
            android:id="@+id/seekBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:progress="25"/>
        <TextView
            android:id="@+id/textView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="25"/>
        <TextView
            android:id="@+id/textViewTouch"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="none"/>

    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    static final private String[] COUNTRIES = new String[]{"kate","king","anna","anita"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SeekBar seekBar = findViewById(R.id.seekBar);
        final TextView textView = findViewById(R.id.textView);
        final TextView textViewTouch = findViewById(R.id.textViewTouch);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                textViewTouch.setText("touch");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textViewTouch.setText("none");
            }
        });
    }

}
```

# 星條評級 RatingBar

[android.widget.RatingBar](https://developer.android.com/reference/android/widget/RatingBar) 類似SeekBar 不過現在是以 星條表示

![](assets/RatingBar.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal"
        android:gravity="center">

        <RatingBar
            android:id="@+id/ratingBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:numStars="5"
            android:rating="2.5"/>
        <TextView
            android:id="@+id/textView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="2.5"/>

    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    static final private String[] COUNTRIES = new String[]{"kate","king","anna","anita"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RatingBar ratingBar = findViewById(R.id.ratingBar);
        final TextView textView = findViewById(R.id.textView);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                textView.setText(Float.toString(rating));
            }
        });
    }

}
```

| 屬性 | 含義 |
| -------- | -------- |
| android:isIndicator     | boolean 是否運行用戶 改變評級 默認 true     |
| android:numStars     | 星星數量     |
| android:rating     | 當前評級     |
| android:stepSize     | 步長 默認 0.5     |

> android:layout_width 屬性務必設置爲 wrap_content 
> 
> 否則 width 太小 無法完整顯示 星星 太大會 顯示出額外的 星星


# 滾動視圖 ScrollView

[android.widget.ScrollView](https://developer.android.com/reference/android/widget/ScrollView) 爲其子元素 添加了 滾動條

> ScrollView 中只能有一個子類 如果 需要放置多個 組件 可以 添加一個 佈局管理器作爲ScrollView的子類 將組件添加到 佈局管理器中
> 

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:orientation="vertical">
            <TextView
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:text="@string/text"/>
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center">
                <RatingBar
                    android:id="@+id/ratingBar"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:numStars="5"
                    android:rating="2.5"
                    />
                <TextView
                    android:id="@+id/textView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="2.5"/>
            </LinearLayout>
        </LinearLayout>
    </ScrollView>

</android.support.constraint.ConstraintLayout>
```

# 微調 Spinner

[android.widget.Spinner](https://developer.android.com/reference/android/widget/Spinner) 類似常見的下拉菜單

![](assets/Spinner.png)
![](assets/Spinner1.png)

```
#info="res/layout/activity_main.xml"
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center">
       <Spinner
           android:layout_width="wrap_content"
           android:layout_height="wrap_content"
           android:entries="@array/loves"
           />
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
#info="res/values/arrays.xml"
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string-array name="loves">
        <item>anita</item>
        <item>anna</item>
        <item>kate</item>
        <item>j0</item>
    </string-array>
</resources>
```


| 屬性 | 含義 | 
| -------- | -------- | 
| android:spinnerMode     | dialog(對話框 0) dropdown(下拉菜單 1 默認)    | 
| android:prompt     | dialog 模式時 如果 此值不爲空白 則顯示爲 對話框 標題     | 

除了使用 xml 也可以使用 java 指定 android:entries

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    static final private String[] COUNTRIES = new String[]{"kate","king","anna","anita"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,COUNTRIES);
        spinner.setAdapter(adapter);
    }

}
```

# 列表視圖 ListView

[android.widget.ListView](https://developer.android.com/reference/android/widget/ListView) 提供了列表視圖

![](assets/ListView.png)

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:gravity="center">
       <ListView
           android:id="@+id/listView"
           android:layout_width="wrap_content"
           android:layout_height="wrap_content" />
    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView view = findViewById(R.id.listView);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.loves,android.R.layout.simple_list_item_single_choice);
        view.setAdapter(adapter);

        // 啓用 單選
        view.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        // 監聽 item 點擊事件
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = parent.getItemAtPosition(position).toString();
                Log.i(TAG,"you love " + str);
            }
        });
    }

}
```

* simple\_list\_item\_1 普通文本
* simple\_list\_item\_2 普通文本(字體略大)
* simple\_list\_item\_checked 帶勾選按鈕
* simple\_list\_item\_multiple\_choice 帶複選框按鈕
* simple\_list\_item\_single\_choice 帶單選按鈕

> simple\_list\_item\_checked simple\_list\_item\_multiple\_choice simple\_list\_item\_single\_choice 只指定了 選框 外表 
>
> 是否允許 複選 需要使用 setChoiceMode函數/android:choiceMode屬性 進行設置
> 

## ListActivity

如果 Activity 只有 ListView 可以從 [android.app.ListActivity](https://developer.android.com/reference/android/app/ListActivity) 派生 此時無需 setContentView 只要 setListAdapter 設置 ListView 的 Adapter 即可

![](assets/ListActivity.png)

```
package com.king011.example;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.loves,android.R.layout.simple_list_item_multiple_choice);
        setListAdapter(adapter);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

}
```


# 選項卡 TabHost

[android.widget.TabHost](https://developer.android.com/reference/android/widget/TabHost) 提供了選項卡 功能 可以將 頁面 分割到不同的 選項卡中

![](assets/TabHost_anna.png) ![](assets/TabHost_anita.png)

```
#info="activity_main.xml"
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <TabHost
        android:id="@+id/tabHost"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:orientation="vertical">

            <TabWidget
                android:id="@android:id/tabs"
                android:layout_width="match_parent"
                android:layout_height="wrap_content" />

            <FrameLayout
                android:id="@android:id/tabcontent"
                android:layout_width="match_parent"
                android:layout_height="match_parent">
            </FrameLayout>
        </LinearLayout>
    </TabHost>
</android.support.constraint.ConstraintLayout>
```

```
#info="tab_anna.xml"
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/tabAnna"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="this is anna"/>

</LinearLayout>
```

```
#info="tab_anita.xml"
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/tabAnita"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="this is anita"/>

</LinearLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化 TabHost
        TabHost tabHost = findViewById(R.id.tabHost);
        tabHost.setup();

        // 初始化 視圖
        FrameLayout frameLayout = tabHost.getTabContentView();
        LayoutInflater inflater = LayoutInflater.from(this);
        inflater.inflate(R.layout.tab_anna,frameLayout);
        inflater.inflate(R.layout.tab_anita,frameLayout);

        // 添加 標籤
        tabHost.addTab(tabHost.newTabSpec("anna").setIndicator("安娜").setContent(R.id.tabAnna));
        tabHost.addTab(tabHost.newTabSpec("anita").setIndicator("梅豔芳").setContent(R.id.tabAnita));

    }

}
```

# 表格視圖 GridView

[android.widget.GridLayout](https://developer.android.com/reference/android/widget/GridLayout) 以表格的形式顯示 組件 通常用來顯示 一組 類似的 東西 比如android的 app 圖標

![](assets/GridView.png)

```
#info="activity_main.xml"
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <GridView
        android:id="@+id/gridView"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:stretchMode="columnWidth"
        android:numColumns="3"
        />

</android.support.constraint.ConstraintLayout>
```

```
#info="items.xml"
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:orientation="vertical"
    android:gravity="center">

    <ImageView
        android:id="@+id/image"
        android:layout_width="wrap_content"
        android:layout_height="100dp"
        android:scaleType="fitCenter"/>
    <TextView
        android:id="@+id/title"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:gravity="center"/>

</LinearLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // 爲適配器 創建項目
        int[] image = new int[]{
                R.drawable.charmander_1,R.drawable.charmander_2,
                R.drawable.charmander_3,R.drawable.charmander_4,
                R.drawable.charmander_5,R.drawable.charmander_6,
                R.drawable.charmander_7,R.drawable.charmander_8
        };
        List items = new ArrayList<Map<String,Object>>(image.length);
        for (int i=0;i<image.length;i++)
        {
            Map<String,Object> m = new HashMap<String,Object>();
            m.put("image",image[i]);
            m.put("title","charmander-" + Integer.toString(i+1));
            items.add(m);
        }

        // 創建適配器
        SimpleAdapter adapter = new SimpleAdapter(this,
                items,
                R.layout.items,
                new String[]{"title","image"},
                new int[]{R.id.title,R.id.image}
                );

        // 設置 適配器
        GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(adapter);
    }

}
```

| 屬性 | 含義 |
| -------- | -------- |
| android:columnWidth     | 列寬     |
| android:gravity     | 對齊方式     |
| android:horizontalSpacing     | 元素水平間距     |
| android:verticalSpacing     | 元素垂直間距     |
| android:numColumns     | 列數     |
| android:stretchMode     | 拉伸模式  none(不拉伸) spacingWidth(僅拉伸元素間距) columnWidth(僅拉伸表格本身) spacingWidthUniform(均勻拉伸元素間距)    |

如果 值顯示 圖像 沒有額外設置 可以實現 BaseAdapter

![](assets/BaseAdapter.png)

```
package com.king011.example;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // 爲適配器 創建項目
        final int[] image = new int[]{
                R.drawable.charmander_1,R.drawable.charmander_2,
                R.drawable.charmander_3,R.drawable.charmander_4,
                R.drawable.charmander_5,R.drawable.charmander_6,
                R.drawable.charmander_7,R.drawable.charmander_8
        };
        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return image.length;
            }

            @Override
            public Object getItem(int position) {
                return image[position];
            }

            @Override
            public long getItemId(int position) {
                return image[position];
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ImageView view;
                if(convertView == null){
                    view = new ImageView(MainActivity.this);
                    view.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }else{
                    view = (ImageView) convertView;
                }
                view.setImageResource(image[position]);
                return view;
            }
        };

        // 設置 適配器
        GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(adapter);
    }

}
```

# 圖像切換器 ImageSwitcher

[android.widget.ImageSwitcher](https://developer.android.com/reference/android/widget/ImageSwitcher) 可以在切換圖像時 爲切換添加 動畫特效

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        >
        <ImageSwitcher
            android:id="@+id/imageSwitcher"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_centerInParent="true"/>

        <Button
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="上一張"
            android:layout_alignParentBottom="true"
            android:onClick="onPrevious"/>
        <Button
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="下一張"
            android:layout_alignParentBottom="true"
            android:layout_alignParentRight="true"
            android:onClick="onNext"/>
    </RelativeLayout>


</android.support.constraint.ConstraintLayout>
```

```
package com.king011.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    // 定義 圖像 資源
    static final private int[] image = new int[]{
            R.drawable.charmander_1,R.drawable.charmander_2,
            R.drawable.charmander_3,R.drawable.charmander_4,
            R.drawable.charmander_5,R.drawable.charmander_6,
            R.drawable.charmander_7,R.drawable.charmander_8
    };
    // 當前顯示項目
    int index = 0;
    ImageSwitcher imageSwitcher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 設置 切換特效
        imageSwitcher = findViewById(R.id.imageSwitcher);
        imageSwitcher.setInAnimation(AnimationUtils.loadAnimation(this,android.R.anim.fade_in)); // 淡入
        imageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this,android.R.anim.fade_out)); // 淡出

        // 必須 設置 Factory 來創建 view
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView view = new ImageView(MainActivity.this);
                view.setScaleType(ImageView.ScaleType.FIT_CENTER);
                view.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
                return view;
            }
        });

        // 設置 要顯示的 圖像
        imageSwitcher.setImageResource(image[index]);
    }
    public void onPrevious(View view){
        if(index==0){
            index = image.length-1;
        }else{
            index--;
        }

        imageSwitcher.setImageResource(image[index]);
    }
    public void onNext(View view){
        if(index==image.length-1){
            index = 0;
        }else{
            index++;
        }
        imageSwitcher.setImageResource(image[index]);
    }

}
```