# [Sidebar](https://primeng.org/sidebar)

```
import {SidebarModule} from 'primeng/sidebar';
```

sidebar 是一個側邊欄

```
<p-sidebar [(visible)]="display">
    Content
</p-sidebar>

<p-button type="text" (click)="display = true" icon="pi pi-plus" label="Show"></p-button>
```

## Position

使用 **position** 屬性來設置側邊欄顯示位置

```
<p-sidebar [(visible)]="display" position="right">
    Content
</p-sidebar>
```

## Size

你可以使用一個固定值 或者 三個預設的值來設置側邊欄的大小

```

<p-sidebar [(visible)]="display" [style]="{width:'30em'}"></p-sidebar>

<p-sidebar [(visible)]="display" styleClass="p-sidebar-sm"></p-sidebar>
<p-sidebar [(visible)]="display" styleClass="p-sidebar-md"></p-sidebar>
<p-sidebar [(visible)]="display" styleClass="p-sidebar-lg"></p-sidebar>
```

## Animation Configuration

側邊欄在打開和隱藏時使用 **transitionOptions** 屬性來定義來過度動畫，默認值爲 150ms cubic-bezier(0, 0, 0.2, 1)，下面的例子將完全禁用動畫

```
<p-sidebar [(visible)]="display" [transitionOptions]="'0ms'"></p-sidebar>
```

## Full Screen

使用 **fullScreen** 屬性可以使用側邊欄全屏顯示

## Custom Content

```
<p-sidebar [(visible)]="display">
  <ng-template pTemplate="header">Header Content</ng-template>
  <ng-template pTemplate="content">Body Content</ng-template>
  <ng-template pTemplate="footer">Footer Content</ng-template>
</p-sidebar>
```

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| visible     | boolean     | false     | 是否顯示側邊欄     |
| position     | string     | left     | 側邊欄顯示位置: left, right, bottom and top.     |
| fullScreen     | boolean     | false     | 是否全屏顯示側邊欄     |
| appendTo     | any     | null     | Target element to attach the dialog, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| style     | string     | null     | Inline style of the component.     |
| styleClass     | string     | null     | Style class of the component.     |
| blockScroll     | boolean     | false     | 側邊欄處於活動狀態時是否阻止滾動 document     |
| baseZIndex     | number     | 0     | 用於分層的 zindex 值     |
| autoZIndex     | boolean     | true     | 是否自動管理分層     |
| modal     | boolean     | true     | 側邊欄是否以 modal 形式顯示     |
| dismissible     | boolean     | true     | 單擊蒙版時是否關閉側邊欄     |
| showCloseIcon     | boolean     | true     | 是否顯示關閉圖標     |
| transitionOptions     | string     | 150ms cubic-bezier(0, 0, 0.2, 1)     | 過渡動畫     |
| ariaCloseLabel     | string     | close     | 	Aria label of the close icon.     |
| closeOnEscape     | boolean     | true     | 按 Esc 鍵是否應該隱藏側邊欄     |

# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onShow     | event: Event object     | 顯示側邊欄時回調     |
| onHide     | event: Event object     | 隱藏側邊欄時回調     |
| visibleChange     | event: Event object     | 側邊欄可見狀態發生改變時回調     |

# Templates

| Name | Parameters | 
| -------- | -------- |
| header     | -     |
| content     | -     |
| footer     | -     |

# Styling

| Name | Element | 
| -------- | -------- | 
| p-sidebar     | Container element     | 
| p-sidebar-left     | Container element of left sidebar.     | 
| p-sidebar-right     | Container element of right sidebar.     | 
| p-sidebar-top     | 	Container element of top sidebar.     | 
| p-sidebar-bottom     | Container element of bottom sidebar.     | 
| p-sidebar-full     | Container element of a full screen sidebar.     | 
| p-sidebar-active     | 	Container element when sidebar is visible.     | 
| p-sidebar-close     | Close anchor element.     | 
| p-sidebar-sm     | Small sized sidebar.     | 
| p-sidebar-md     | Medium sized sidebar.     | 
| p-sidebar-lg     | Large sized sidebar.     | 
| p-sidebar-mask     | Modal layer of the sidebar.     | 

