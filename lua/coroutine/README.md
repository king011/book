# 協程

lua 提供了對協程的支持，所有相關操作都定義在 table **coroutine** 中

* coroutine.create(function) 創建一個協程，並使之掛起
* coroutine.status(co) 返回描述協程狀態的字符串
	* suspended 掛起
	* running 運行
	* dead 死亡
* coroutine.resume(co) 恢復協程運行
* coroutine.yield() 協程讓出 cpu 返回到 resume 調用處

```
local co =
    coroutine.create(
    function(a, b)
        print(a, b, 2)
        local a, b = coroutine.yield("a0", "b0")
        print(a, b, 4)

        return "a1", "b1"
    end
)
print(coroutine.status(co))

print(1)
print(coroutine.resume(co, "A0", "B0"))
print(3)
print(coroutine.resume(co, "A1", "B1"))
print()
print(coroutine.resume(co)) -- false	'cannot resume dead coroutine'
```
# resume yield

resume 會掛起當前進度運行協程，直到協程 返回或調用 yield，如果沒有錯誤 resume 第一個返回值爲 true 之後的返回值是 yield 傳回的值，如果出錯 resume 第一個返回值爲 false 第二個返回值爲錯誤描述字符串

resume 第一個之後的參數會傳遞給協程函數或 yield 的返回值 

