# example

```
package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

func main() {
	go client()
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	http.ListenAndServe(":8080", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ws, e := upgrader.Upgrade(w, r, nil)
		if e != nil {
			log.Println(e)
			return
		}
		defer ws.Close()
		for {
			t, r, e := ws.NextReader()
			if e != nil {
				break
			}
			w, e := ws.NextWriter(t)
			if e != nil {
				break
			}
			_, e = io.Copy(w, r)
			if e != nil {
				break
			}
			e = w.Close()
			if e != nil {
				break
			}
		}
	}))
}
func client() {
	time.Sleep(time.Second)
	ws, _, e := websocket.DefaultDialer.Dial(`ws://127.0.0.1:8080`, nil)
	if e != nil {
		log.Fatalln(e)
	}
	defer ws.Close()
	go func() {
		for {
			t, b, e := ws.ReadMessage()
			if e != nil {
				break
			}
			if t == websocket.BinaryMessage {
				fmt.Println(`<-`, string(b))
			}
		}
		ws.Close()
	}()
	for i := 0; i < 10; i++ {
		str := fmt.Sprint(`cerberus is an idea,`, i)
		fmt.Println(`->`, str)
		e = ws.WriteMessage(websocket.BinaryMessage, []byte(str))
		if e != nil {
			break
		}
		time.Sleep(time.Second)
	}
}
```