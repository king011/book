# systemd

linux kernerl 3 以上的版本 開始 提供了 新的 服務管理 機制 systemd

所有管理命令都集中到了 systemctl 中

命令形式
```sh
#info=false
systemctl [command] [unit]
```

## command

| 命令 | 功能 |
| -------- | -------- | 
| start     | 啓動服務     |
| stop     | 停止服務     |
| restart     | 重啓服務     |
| reload     | 重新加載設定     |
| enable     | 設置開啓啓動     |
| disable     | 設置開機不啓動     |
| status     | 顯示服務當前狀態     |
| is-active     | 返回服務是否處於 運行中     |
| is-enabled     | 返回服務是否設定開機啓動     |

## status
```sh
king@king-company ~ $ systemctl status cron
● cron.service - Regular background program processing daemon
   Loaded: loaded (/lib/systemd/system/cron.service; enabled; vendor preset: enabled)
   Active: active (running) since 五 2017-08-25 00:21:47 CST; 7h left
     Docs: man:cron(8)
 Main PID: 939 (cron)
    Tasks: 1
   Memory: 372.0K
      CPU: 10ms
   CGroup: /system.slice/cron.service
           └─939 /usr/sbin/cron -f

 8月 25 00:21:47 king-company systemd[1]: Started Regular background program processing daemon.
 8月 25 00:21:48 king-company cron[939]: (CRON) INFO (pidfile fd = 3)
 8月 25 00:21:48 king-company cron[939]: (CRON) INFO (Running @reboot jobs)
 8月 24 17:17:02 king-company CRON[5041]: pam_unix(cron:session): session opened for user root by (uid=0)
 8月 24 17:17:02 king-company CRON[5042]: (root) CMD (   cd / && run-parts --report /etc/cron.hourly)
 8月 24 17:17:02 king-company CRON[5041]: pam_unix(cron:session): session closed for user root
```

第二行 enabled 顯示 了 服務 是否開機運行

* enabled	開機運行
* disabled	不會開機運行
* static		不可以直接啓動 但可能被其它服務 啓動
* mask		無論如何都無法被啓動

第三行 active (running) 顯示了服務正在運行

* active (running)	正有一隻或多隻程式在運行中
* active (exited)	僅執行一次就正常結束的服務
* active (waiting)	正在執行中 但處於等待中
* inactive		服務目前沒有運行

> 如果 os 使用了 systemd 管理服務  
> 不要使用 使用 init.d/*** start (system V) 等舊式方式 管理服務 這樣 systemd 無法偵查到 服務狀態
>
> 如果os 還沒 提供 systemd的服務 管理 那麼你該 換個os 不要 cosplay 上個世紀的 環境
> 

# unit 分類

systemd 中 將 每個 服務 都作爲一個 unit 單元

針對 每個 unit 又進行了 分類 用於 標註 其 類別 通過 副檔 名稱即可知道

* multi-user.target 的分類爲 target 
* ssh.service 的分類爲 service 

| 副檔名 | 主要功能 |
| -------- | -------- |
| .service     | 一般服務類型     |
| .socket     | 	內部資料交換的 socket unit     |
| .target     | 	一群 unit 的 集合     |
| .mount<br>.automount     | 檔案系統 掛接服務     |
| .path     | 偵測特定檔案或目錄類型 如列印服務     |
| .timer     | 循環執行的服務     |

# 列舉系統所有服務
systemctl [command] [--type=TYPE] [--all]
* command:
    * list-units	(默認) 依據 unit 列出目前有啟動的 unit。若加上 --all 才會列出沒啟動的。
    * list-unit-files	依據 /usr/lib/systemd/system/ 內的檔案，將所有檔案列表說明。
* --type=TYPE	service socket target ...

```sh
#info="查看所有服務"
king@king-company ~ $ systemctl | head -5
UNIT                                                                                      LOAD   ACTIVE SUB       DESCRIPTION
proc-sys-fs-binfmt_misc.automount                                                         loaded active running   Arbitrary Executable File Formats File System Automount Point
sys-devices-pci0000:00-0000:00:01.0-0000:01:00.1-sound-card2.device                       loaded active plugged   GK208 HDMI/DP Audio Controller
sys-devices-pci0000:00-0000:00:02.0-backlight-acpi_video0.device                          loaded active plugged   /sys/devices/pci0000:00/0000:00:02.0/backlight/acpi_video0
sys-devices-pci0000:00-0000:00:03.0-sound-card0.device                                    loaded active plugged   Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller
```

> 加上 -all 才會顯示 沒啓動的 服務
> 

* UNIT	服務名稱
* LOAD	是否開機載入
* ACTIVE	目前狀態 
* DESCRIPTION	描述信息

# 查看服務依賴
systemctl list-dependencies [unit] [--reverse]

```sh
//查看 ssh 依賴的服務
systemctl list-dependencies ssh

//查詢誰使用了 ssh
systemctl list-dependencies ssh --reverse
```

# systemd 的 daemon 運作過程相關的目錄簡介
* /usr/lib/systemd/system/  
 官方提供的 軟體 服務配置目錄  
 (ubuntu 在 /lib/systemd/system/ )
 
* /run/systemd/system/  
  os 運行過程中 產生的 服務腳本 優先級 比 /usr/lib/systemd/system/ 高

* **/etc/systemd/system/**  
  系統管理員 配置的 服務 (類似 system 中的 /etc/rc.d/rc5.d/Sxx) 比 /run/systemd/system/ 優先

* /etc/sysconfig/*  
  服務配置檔案 (如 /etc/sysconfig/mysql/my.cnf)  
  (ubuntu 直接放在 /etc 下 如 /etc/mysql/my.cnf)

* /var/lib/  
  產生的資料 如 Mariadb 的資料庫 在 /var/lib/mysql 下

* /run/  
  暫存檔 如 PID file lock file 等

```sh
#info="查看需要 socket file 的 服務所在 檔案"
king@king-company ~/program/project/go/src/test_console $ systemctl list-sockets
LISTEN                          UNIT                            ACTIVATES
/dev/rfkill                     systemd-rfkill.socket           systemd-rfkill.service
/run/acpid.socket               acpid.socket                    acpid.service
/run/dmeventd-client            dm-event.socket                 dm-event.service
/run/dmeventd-server            dm-event.socket                 dm-event.service
/run/lvm/lvmetad.socket         lvm2-lvmetad.socket             lvm2-lvmetad.service
/run/lvm/lvmpolld.socket        lvm2-lvmpolld.socket            lvm2-lvmpolld.service
/run/systemd/fsck.progress      systemd-fsckd.socket            systemd-fsckd.service
/run/systemd/initctl/fifo       systemd-initctl.socket          systemd-initctl.service
/run/systemd/journal/dev-log    systemd-journald-dev-log.socket systemd-journald.service
/run/systemd/journal/socket     systemd-journald.socket         systemd-journald.service
/run/systemd/journal/stdout     systemd-journald.socket         systemd-journald.service
/run/systemd/journal/syslog     syslog.socket                   rsyslog.service
/run/udev/control               systemd-udevd-control.socket    systemd-udevd.service
/run/uuidd/request              uuidd.socket                    uuidd.service
/var/run/avahi-daemon/socket    avahi-daemon.socket             avahi-daemon.service
/var/run/cups/cups.sock         cups.socket                     cups.service
/var/run/dbus/system_bus_socket dbus.socket                     dbus.service
/var/run/docker.sock            docker.socket                   docker.service
audit 1                         systemd-journald-audit.socket   systemd-journald.service
kobject-uevent 1                systemd-udevd-kernel.socket     systemd-udevd.service
 
20 sockets listed.
Pass --all to see loaded but inactive sockets, too.
```

# 創建服務
每個服務 都是由一個 \*\.\* 檔案定義  
對於 red head 系列 /usr/lib/systemd/system/ 檔案夾下定義了 系統服務  
(ubuntu 系統服務在 /lib/systemd/system/ 下)  

一般 自定義 和第三方 服務 定義在 /etc/systemd/system/ 檔案夾下

```ini
#info="/etc/systemd/system/sshd.service "
[Unit]
Description=OpenBSD Secure Shell server
After=network.target auditd.service
ConditionPathExists=!/etc/ssh/sshd_not_to_be_run
 
[Service]
EnvironmentFile=-/etc/default/ssh
ExecStart=/usr/sbin/sshd -D $SSHD_OPTS
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure
RestartPreventExitStatus=255
Type=notify
 
[Install]
WantedBy=multi-user.target
Alias=sshd.service
```

# 服務配置檔案

* 使用 # 註釋
* 存在 三個段 [Unit] [Service] [Install] 用於描述服務
* 每個段落 使用 key=value 的方式 進行設置
* value如果有多個 項目 使用 空格隔開
* key值如果 重複 以最後一個 key值 作爲有效值(所以可以使用 key= 空白 來取消一個 key的 設置)

## Unit

Unit 定義了服務本身 的描述	


| key | 含義 | 
| -------- | -------- | 
| Description     | systemctrl status 等命名中顯示的 服務說明     | 
| Documentation     | 這個項目在提供管理員能夠進行進一步的文件查詢的功能！提供的文件可以是如下的資料<ul><li>Documentation=http://www....</li><li>Documentation=man:sshd(8)</li><li>Documentation=file:/etc/ssh/sshd_config</li></ul>     | 
| After     | 建議在After的服務啓動後才啓動此服務(並非強制要求)<br>After=network.target auditd.service     | 
| Before     | 建議在 啓動 Before指定的服務前 啓動本服務     | 
| Requires     | 類似 After 當現在是強制要求 如果 Requires 指定的服務沒啓動前 本服務不會被啓動     | 
| Wants     | 在啓動本服務後 最好還要啓動的服務 沒有強制要求     | 
| Conflicts     | 衝突檢查 如果 Conflicts 指定的服務啓動了 就不會啓動本服務 如果啓動了本服務 就不會啓動 Conflicts 指定的服務     | 

## Service

| key | 含義 |
| -------- | -------- | 
| Type     | 說明 deamon 啓動方式 <ul><li>simple	默認值 啓動後常駐記於憶體中</li><li>forking	ExecStart 啓動的程式 透過 spwans 延伸出其它子程式爲此 deamon 服務</li><li>oneshot	與simple類似 不過工作完畢後就結束 不會常駐記於憶體中</li><li>dbus	與simple類似 但必須取一個 D-Bus 名稱</li><li>idle	與simple類似 但必須要所有工作都順利執行完畢才會執行 通常在開機最後 執行</li></ul>     |
| EnvironmentFile     | 配置檔案     |
| ExecStart     | systemctl start 執行的命令     |
| ExecStop     | systemctl stop 執行的命令     |
| ExecReload     | systemctl reload 執行的命令     |
| Restart     | 服務退出時 是否需要 重啓服務     |
| RemainAfterExit     | RemainAfterExit=1 時 deamon 所屬的所有程式終止後 會再次啓動     |
| TimeoutSec     | 服務在 啓動/關閉 時 如因爲某些故障無法 正常啓動/關閉 等多久 執行 強制結束     |
| KillMode     | 可以是 process control-group none<ul><li>process	deamon 終止時 只會 終止 ExecStart 後接的指令</li><li>control-group	deamon 終止時 會終止 deamon啓動的其它 程式</li><li>none	不會 終止任何程式</li></ul>     |
| RestartSec     | Restart 設置需要重啓時 等待多久 啓動服務 默認 100ms(毫秒) 如果不寫單位默認單位爲秒     |
| User  | 以 指定用戶運行 默認 root  |

## Install

| key | 含義 |
| -------- | -------- |
| WantedBy     | 本服務 掛機到哪個 *.target 下<br>大部分都是 multi-user.target     | 
| Also     | 此服務 enable 時 也請將 Also 指定的服務 enable (依賴的服務)     | 
| Alias     | 進行一個連結的別名     | 

## Restart 取值

Restart 如下值之一 定義了服務在 非 systemctrl 指令使其停止時 如何重啓

* no		默認 不會重啓
* always		始終重啓
* on-success	服務正常退出(退出碼 == 0 或者收到 SIGHUP, SIGINT, SIGTERM, SIGPIPE 信號) 時重啓
* on-failure	服務非正常退出 時重啓
* on-abnormal
* on-watchdog
* on-abor

| 退出原因 | no | always | on-success | on-failure | on-abnormal | on-watchdog | on-abor |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| 正常退出     |      | yes     | yes     |      |      |      |      |
| 退出碼不爲0     |      | yes     |      | yes     |      |      |      |
| 被進程殺死     |      | yes     |      | yes     | yes     | yes     |      |
| systemd 操作超時     |      | yes     |      | yes     | yes     |      |      |
| 看門狗 超時     |      | yes     |      | yes     | yes     |      | yes     |


## Example

```ini
[Unit]
Description=go echo 服務器
After=network.target
 
[Service]
Type=simple
ExecStart=/home/king/program/project/go/src/test-echo/s/s
KillMode=process
Restart=on-failure
 
[Install]
WantedBy=multi-user.target
```

# Limit

systemd 的服務 需要在服務定義檔案中指定 limit

```
Directive        ulimit equivalent     Unit
LimitCPU=        ulimit -t             Seconds      
LimitFSIZE=      ulimit -f             Bytes
LimitDATA=       ulimit -d             Bytes
LimitSTACK=      ulimit -s             Bytes
LimitCORE=       ulimit -c             Bytes
LimitRSS=        ulimit -m             Bytes
LimitNOFILE=     ulimit -n             Number of File Descriptors 
LimitAS=         ulimit -v             Bytes
LimitNPROC=      ulimit -u             Number of Processes 
LimitMEMLOCK=    ulimit -l             Bytes
LimitLOCKS=      ulimit -x             Number of Locks 
LimitSIGPENDING= ulimit -i             Number of Queued Signals 
LimitMSGQUEUE=   ulimit -q             Bytes
LimitNICE=       ulimit -e             Nice Level 
LimitRTPRIO=     ulimit -r             Realtime Priority  
LimitRTTIME=     No equivalent
```

```
[Service]
LimitNOFILE=1048576
```