# go-redis

go-redis 是 redis 的 開源(BSD-2) client

* 源碼 [https://github.com/go-redis/redis](https://github.com/go-redis/redis)

```
go get github.com/go-redis/redis/v8
```

```
package main

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     `localhost:6379`, // 服務器地址
		Password: ``,               // 驗證密碼
		DB:       0,                // 使用的 數據庫
	})
	defer rdb.Close()

	// 設置 字符串
	ctx := context.Background()
	e := rdb.Set(ctx, `id`, 123, time.Second).Err()
	checkError(e)

	val, e := rdb.Get(ctx, `id`).Result()
	checkError(e)
	fmt.Println(val)
}
```