# nginx-rtmp-module

第三方的 nginx-rtmp-module 模塊 可以將 nginx 搭建成 流媒體 服務器 不過 需要 自行 編譯 nginx 和 nginx-rtmp-module 

# ubuntu18.04

1. 下載 nginx 源碼

    ```sh
		#info=false
    git clone https://github.com/nginx/nginx
    ```

1. 下載 nginx-rtmp-module 源碼

    ```sh
		#info=false
    git clone https://github.com/arut/nginx-rtmp-module
    ```

1. 安裝依賴

    ```sh
		#info=false
    sudo apt install gcc make libssl-dev libpcre3 libpcre3-dev zlib1g-dev -y
    ```

1. 編譯 nginx 和 nginx-rtmp-module 

    ```sh
    #info=false
    cd nginx
    ./auto/configure --add-module=../nginx-rtmp-module
    make
    sudo make install
    ```
		
> 源碼 安裝 默認安裝 到 /usr/local/nginx
> 
> 可執行檔在 /usr/local/nginx/sbin/nginx
> 


# 配置
```
#user  nobody;
worker_processes  1;  

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;

events {
    worker_connections  1024;
}

rtmp_auto_push on; 
rtmp_auto_push_reconnect 1s; 

# 定義 rtmp 服務
rtmp {
        # 定義一個 服務器
        server {
                # 監聽端口
                listen 1935;
                # application 定義一個 頻道
                application files {
                        live on; 
                        # play 播放 本地 檔案 指定 頻道根路徑
                        # 如果 有個 檔案 /var/www/rtmp/a.mp4 則 其 播放地址 爲 rtmp://xxx:1935/files/a.mp4
                        play /var/www/rtmp;
                }

                # 定義一個 頻道 播放 ffmpeg 推送的 流
                application ffmpeg {
                        live on;
                }
                # 定義一個 頻道 播放 hls
                # hls 將 流 切片 成檔案 存儲 以便使用 http 播放
                application hls {
                        live on;
                        hls on;
                        hls_path /tmp/hls; # hls 臨時檔案夾
                }
        }
}

# 定義 http 服務
http {
        server{
                listen  80;
                # 定義 http://xxx/hls/xxx 的支持 播放 rtmp://xxx:xx/hls/xxx
                location /hls {
                        types{
                                application/vnd.apple.mpegurl m3u8;
                                video/mp2t ts;
                        }
                        root /tmp/;
                        add_header Cache-Control no-cache;
                }
        }
}
```

```sh
# 播放 a.mp4 並且 推送到 rtmp://127.0.0.1:1935/ffmpeg/a 流
ffmpeg -re -i a.mp4 -vcodec copy -acodec copy -f flv "rtmp://127.0.0.1:1935/ffmpeg/a"

# 播放 a.mp4 並且 推送到 rtmp://127.0.0.1:1935/hls/a 流
ffmpeg -re -i a.mp4 -vcodec copy -acodec copy -f flv "rtmp://127.0.0.1:1935/hls/a"
```