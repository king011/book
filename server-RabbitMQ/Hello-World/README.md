# [基礎介紹](https://www.rabbitmq.com/tutorials/tutorial-one-go.html)

RabbitMQ 是一個消息代理 ，她接收並轉發消息。可以把RabbitMQ看着是郵局，發佈者將消息發送給郵局RabbitMQ, RabbitMQ將消息轉發給收信者

# Producer
![](assets/producer.webp)

生產意味着發送消息 一個發送消息的程序就是 生產者

# Queue 
![](assets/queue.webp)

隊列是RabbitMQ內部郵箱名稱 所有消息都需要通過 queue 流動 

隊列收到主機內存和磁盤限制 queue本質上是一個大消息緩衝區

多個生產者可以發送消息到一個隊列 多個消費者也可以從同一個隊列收取數據

# Consumer
![](assets/consumer.webp)

消費意味者 收取消息 一個接收消息的程序就是 消費者

# One to One
![](assets/python-one.webp)
如下顯示 一個生產者 一個消費者 模式

```
#info="producer"
package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-one`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}
	// 關閉連接
	defer c.Close()

	// 創建生產者
	producer(c)
}

func producer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()

	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}
	for i := 0; i < 10; i++ {
		// 發佈消息
		e = channel.Publish(
			``,
			q.Name,
			false,
			false,
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(strconv.Itoa(i)), //消息內容
			},
		)
		if e == nil {
			fmt.Println(`send ->`, i)
		} else {
			log.Println(e)
			break
		}
		time.Sleep(time.Second)
	}
}
```

```
#info="consumer"
package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

const (
	// ServerURL 定義 RabbitMQ 服務器 連接url
	ServerURL = `amqp://king:123456@127.0.0.1:5672/`
	// Name 定義 隊列名稱
	Name = `one-to-one`
)

func main() {
	// 連接 服務器
	c, e := amqp.Dial(ServerURL)
	if e != nil {
		log.Fatalln(e)
	}

	// 關閉連接
	defer c.Close()

	// 創建消費者
	consumer(c)
}
func consumer(c *amqp.Connection) {
	// 創建一個 頻道 用於 通信
	// channel 是 RabbitMQ 爲了重用tcp 而實現的邏輯 通信信道
	channel, e := c.Channel()
	if e != nil {
		log.Println(e)
		return
	}
	// 關閉 通道
	defer channel.Close()
	// 聲明一個 隊列 只有在隊列不存在時 才會創建
	q, e := channel.QueueDeclare(
		Name,  // 隊列名稱 如果爲空 服務器將返回一個 隨機的名字
		false, // 是否持久化
		false, // 是否在沒人使用時 自動刪除
		false, // 是否排他 如果爲true 則只能由聲明此隊列的Connection訪問且在連接關閉時將其刪除
		false, // 如果爲true則假定隊列在服務器聲明
		nil,   //参数
	)
	if e != nil {
		log.Println(e)
		return
	}

	// 創建一個 消費者 消費消息
	ch, e := channel.Consume(
		q.Name, // 隊列名稱
		``,
		true, // 自動回覆 ack
		false,
		false,
		false,
		nil,
	)
	if e != nil {
		log.Println(e)
		return
	}
	for msg := range ch {
		v, e0 := strconv.Atoi(string(msg.Body))
		fmt.Println(`recv <-`, v, e0)
	}
}
```