# 安裝運行

```
npm install art-template --save
```

```
#info="main.ts"
import { default as template, compile, render } from 'art-template'
import { join, normalize } from 'path'
function background() {
    return {
        name: "kate",
        test: (level: number) => {
            if (level > 10) {
                return 'A'
            }
            return 'B'
        }
    }
}
(function () {
    console.log('--- example template ---')
    const filename = normalize(join(__dirname, '..', 'template', 'a.art'))
    // 加載模板 僅僅 node 環境可用
    let t = template(filename, // 模板路徑
        // * undefined 編譯模板並返回 渲染函數
        // * object 編譯模板並渲染 返回渲染後的 字符串
        undefined,
    )
    // 渲染模板
    const ctx = background()
    console.log(t(ctx))
    ctx.name = 'anita'
    console.log(t(ctx))
})();
const source = "wellcome {{name}}.\nYour rating is {{test(1)}}.}}";
(function () {
    console.log('--- example compile ---')
    // 編譯 模板
    const t = compile(source)
    // 渲染 模板
    const ctx = background()
    console.log(t(ctx))
    ctx.name = 'anita'
    console.log(t(ctx))
})();
(function () {
    console.log('--- example render ---')
    // 編譯 並 渲染 模板
    console.log(render(source, background()))
})();
```