# 發佈訂閱

redis 提供了 發佈訂閱模式

* 發送者 發送消息 訂閱者 接收消息
* 訂閱者 可以 訂閱任意數量的頻道

| 命令 | 含義 |
| -------- | -------- |
| PSUBSCRIBE pattern [pattern ...]     | 訂閱一個或多個符合模式的頻道     |
| PUBSUB subcommand [argument [argument ...]]     | 查看訂閱與發佈系統狀態     |
| PUBLISH channel message     | 將消息發送到指定頻道     |
| PUNSUBSCRIBE [pattern [pattern ...]]     | 退訂符合模式的頻道     |
| SUBSCRIBE channel [channel ...]     | 訂閱頻道     |
| UNSUBSCRIBE [channel [channel ...]]     | 退訂頻道     |

