# interface nil == nil

對於 interface 包含兩個信息 reflect.Type 和 unsafe.Pointer 只有當兩者都爲 nil 時 其和nil值才相等

如果將 一個實現了此接口的 空指針 賦值給 interface 變量 此時 變量的 reflect.Type 不會 nil 所有此時變量 不等於 nil

```
package main

import (
	"fmt"
	"reflect"
)

type Animal interface {
}
type Cat struct {
}

func main() {
	var cat *Cat
	fmt.Println(reflect.TypeOf(cat), cat == nil) // *main.Cat true
	var a Animal
	fmt.Println(reflect.TypeOf(a), a == nil) // <nil> true
	var i interface{}
	i = a
	fmt.Println(reflect.TypeOf(i), i == nil) // <nil> true
	a = cat                                  // reflect.Type 被設置爲 *main.Cat
	fmt.Println(reflect.TypeOf(a), a == nil) // *main.Cat false
	a = i                                    // reflect.Type 重新被設置爲 nil
	fmt.Println(reflect.TypeOf(a), a == nil) // <nil> true
}
```

# glibc

從 go1.2 開始在 ubuntu22 中編譯的程序可能無法在 ubuntu22以下系統運行並提示 `not found glibc`。這是因爲go1.2 開始不在提供默認的 glic 預編譯庫而是使用系統自帶的，所以如果使用了 cgo 則會出現這樣的問題

解決方案是
1. 設置 `CGO_ENABLED=0` 禁用掉 cgo
2. 如果必須使用 cgo，只能在較低版本的系統上去進行編譯(這會鏈接較低版本的 glic，但這樣的兼容性完全由 glic 控制，它和普通的 c 程序享有同樣的兼容問題)

