# build

go build 指令用於編譯 go 代碼，build 支持了很多可選的編譯參數

## ldflags

使用 -ldflags 可以爲 linker 傳遞一些參數

* **-s** 禁用符號信息
* **-w** 禁用調試信息
* **-X**  package.name=value 覆蓋包中的變量

將 **-s -w** 傳遞給 linker 可以得到更小的輸出目標， 使用 **-X** 可以覆蓋包中的變量，通常可以用來自動設置編譯時間之類的

```
go build -ldflags "-s -w -X main.Version=v1.0.0"
```

```
#!/bin/bash

version=v1.0.0
unix=`date +%s`
build=`TZ='Asia/Shanghai' date '+%F %X'`

go build -ldflags "-s -w -X 'main.Version=$version' -X 'main.Build=Asia/Shanghai=$build unix=$unix'"
```