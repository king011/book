# [調試](https://docs.cocos.com/creator/2.1/manual/zh/publish/debug-jsb.html)


# android 調試

1. 使用 andord debug 模式 creator項目
2. 通過 usb 連接電腦和手機(或者啓動 android模擬器)
3. 打開 手機上的 usb 調試
4. 在設備上運行遊戲
5. 使用 adb 轉發調試端口 `adb forward tcp:5086 tcp:5086`
6. chrome  打開 chrome://inspect 進行調試