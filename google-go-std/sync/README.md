# [sync](https://pkg.go.dev/sync)

sync 包 提供了 其它程式中 常用的 同步 方法 如 互斥量 讀寫鎖 條件變量 ...

## Once

sync.Once 型別的 Do方法 用於 在多個goroutine中 只執行一次

```go
package main
 
import (
	"fmt"
	"sync"
)
 
var once sync.Once
 
func work(i int, ch chan int32) {
	once.Do(func() {
		fmt.Println("only once")
	})
 
	fmt.Println(i)
	ch <- 0
}
func main() {
	count := 5
	ch := make(chan int32, 4)
	for i := 0; i < count; i++ {
		go work(i, ch)
	}
 
	for count > 0 {//    
		<-ch
		count -= 1
	}
}
```

## Mutex
sync.Mutex 型別是一個 互斥量 
```txt
#info=false
//鎖定/解鎖
func (m *Mutex) Lock()
func (m *Mutex) Unlock()
```

```go
package main
 
import (
	"fmt"
	"sync"
	"time"
)
 
var mutex sync.Mutex
 
func work(ch chan int) {
	fmt.Println("work")
 
	mutex.Lock()
	fmt.Println("work ...")
	mutex.Unlock()
 
	ch <- 0
}
func main() {
	fmt.Println("Lock")
	mutex.Lock()
	fmt.Println("start goroutine")
	ch := make(chan int)
	go work(ch)
	time.Sleep(1000 * time.Millisecond)
	fmt.Println("Unlock")
	mutex.Unlock()
 
	<-ch
}
```

## RWMutex

sync.RWMutex 型別是一個讀寫鎖
```txt
#info=false
func (rw *RWMutex) Lock()
func (rw *RWMutex) RLock()
func (rw *RWMutex) RLocker() Locker
func (rw *RWMutex) RUnlock()
func (rw *RWMutex) Unlock()
```

# WaitGroup
WaitGroup 通常 用於 等待 goroutine 結束
其只提供了 3個 API

```txt
#info=false
// delta > 0 增加要等待的 數量 delta < 0 減少要等待的 數量  
func (wg *WaitGroup) Add(delta int)

// 相當於 Add(-1)  
func (wg *WaitGroup) Done()

// 等待 直到要 等待的數量為0時 返回  
func (wg *WaitGroup) Wait()
```

```go
package main
 
import (
	"log"
	"sync"
)
 
func main() {
	var wg sync.WaitGroup
	wg.Add(4)
	wg.Add(-2)
	wg.Add(-1)
	wg.Done()
	wg.Wait()
	log.Println("end")
}
```

```go
package main
 
import (
	"log"
	"sync"
)
 
func main() {
	var wg sync.WaitGroup
 
	// 增加一個 要等待的 goroutine
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func(i int) {
			log.Println("goroutine A", i)
 
			// 通知 一個 goroutine 結束
			wg.Done()
		}(i)
	}
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func(i int, wg *sync.WaitGroup) {
			log.Println("goroutine B", i)
 
			// 通知 一個 goroutine 結束
			wg.Done()
		}(i, &wg) //需要傳遞 WaitGroup 指針
	}
 
	// 等待 所有 goroutine 結束
	wg.Wait()
	log.Println("end")
}
```
> WaitGroup 一旦使用後 變不可copy 如果需要在函數間 傳遞 需要傳遞 指針
> 

# Cond
Cond
Cond 通過一個關聯的 Locker(通常是一個 *Mutex 或 *RWMutex) 實現了一個 條件變量

```go
#info=false
// 創建一個 Cond
func NewCond(l Locker) *Cond

// 等待 一個 通知 調用前 必須 Lock Locker
// Wait 會先自動 Unlock Locker 之後使用 調用 Wait的 goroutine 阻塞 直到收到一個 通知
// 當收到 通知 後 Wait 後嘗試 Lock Locker 如果 成功則 返回 否則 繼續阻塞 等待下個通知
func (c *Cond) Wait(){
	c.checker.check()
	t := runtime_notifyListAdd(&c.notify)
	c.L.Unlock()
	runtime_notifyListWait(&c.notify, t)
	c.L.Lock()
}

// 喚醒 一個 Wait 中的 goroutine
// 調用 Signal 的goroutine 可以 Lock Locker 也可以 不 Lock
func (c *Cond) Signal(){
	c.checker.check()
	runtime_notifyListNotifyOne(&c.notify)
}

// 喚醒 所有 Wait 中的 goroutine
// 調用 Broadcast 的goroutine 可以 Lock Locker 也可以 不 Lock
func (c *Cond) Broadcast(){
	c.checker.check()
	runtime_notifyListNotifyAll(&c.notify)
}
```

```go
package main
 
import (
	"log"
	"sync"
	"time"
)
 
func main() {
	var wg sync.WaitGroup
	wg.Add(2)
 
	cond := sync.NewCond(&sync.Mutex{})
 
	go func() {
		time.Sleep(time.Second)
		// 喚醒 等待的 goroutine
		log.Println("1")
		cond.Signal()
		wg.Done()
	}()
 
	go func() {
		//time.Sleep(time.Second)
		cond.L.Lock()
		log.Println("0")
		cond.Wait()
		log.Println("2")
		cond.L.Unlock()
 
		wg.Done()
	}()
 
	wg.Wait()
	log.Println("end")
}
```
## 在 Wait 之前 Signal
在 Wait 之前 Signal  
之後 在調用 Wait 時會發出 異常

此時可以 增加一個 變量 來表示 是否已經 Signal過了 如果 沒有 才Wait
```go
package main
 
import (
	"log"
	"sync"
	"time"
)
 
func main() {
	var wg sync.WaitGroup
	wg.Add(2)
 
	cond := sync.NewCond(&sync.Mutex{})
 
	condition := false
	go func() {
		cond.L.Lock()
		log.Println(0)
		// 喚醒 等待的 goroutine
		condition = true
		cond.Signal()
		log.Println(1)
		cond.L.Unlock()
 
		wg.Done()
	}()
 
	go func() {
		time.Sleep(time.Millisecond)
		cond.L.Lock()
		log.Println(2)
		for !condition {
			log.Println("wait")
			cond.Wait()
		}
		log.Println(3)
		cond.L.Unlock()
		wg.Done()
	}()
 
	wg.Wait()
	log.Println("end")
}
```
## 確認 是否存在 Wait
如果 在 Wait 前 Signal/Broadcast 則 之後都無法使用 Wait  
如果 需要多次 重用Wait過程 一個做法是 在 Signal/Broadcast 前 判斷 是否有 Wait存在

```go
package main
 
import (
	"log"
	"sync"
	"time"
)
 
func main() {
	var wg sync.WaitGroup
	wg.Add(2)
 
	cond := sync.NewCond(&sync.Mutex{})
 
	// 定義 工作 數量
	work := 0
	// 定義 存儲正在 wait 的 數量
	wait := 0
 
	go func() {
 
		for i := 0; i < 5; i++ {
			time.Sleep(time.Millisecond)
			cond.L.Lock()
			//增加 工作
			log.Println("add work")
			work++
			if wait != 0 {
				//存在 等待 goroutine 喚醒 她
				log.Println("signal work")
				cond.Signal()
			}
			cond.L.Unlock()
		}
		wg.Done()
	}()
 
	go func() {
		time.Sleep(time.Millisecond * 2)
		for i := 0; i < 5; i++ {
			cond.L.Lock()
			log.Println("get work")
			if work == 0 {
				// 沒有 工作 等待
				wait++
				log.Println("wait work")
				cond.Wait()
 
				// 減少 等待 計數
				wait--
			}
			// 處理工作
			log.Println("do work")
			work--
			cond.L.Unlock()
		}
 
		wg.Done()
	}()
 
	wg.Wait()
	log.Println("end")
}
```

## 多個 wait 
如果出現 類似多個 wait 且 被喚醒時 不能保證當前 條件 成立 爲此 應該在 wait 返回後 再次檢測條件(可能被其它 喚醒的wait 先處理導致 條件不成立)

```
c.L.Lock()
for !condition() {
    c.Wait()
}
// ... make use of condition ...
c.L.Unlock()
```


# Pool

sync.Pool 是 一個 臨時的 對象池

因爲是 goroutine 安全 且 go gc 本來就是 內存池 沒必要再來一個 內存池 故將 Pool 放到了 sync 中(我猜啦)

go 本身 提供了 gc 管理了 內存 Pool 是一個 臨時的 內存池 用來減少 gc 的壓力 

> Pool 保存的 對象 會在 下次 gc 時 被 全部 釋放掉

要使用 Pool 只需要 如下 三個 步驟

1. 創建一個 Pool 並且 並設置 New 方法
2. 調用 Get 獲取 對象(如果 對象池中 不存在  則使用 Pool 的 New 方法 創建 對象)
3. 調用 Put 將 對象 歸還到 Pool

使用 Pool 來 緩存 tcp recv bytes.Buffer 是個不錯的注意

```go
package sync_test

import (
    "bytes"
    "io"
    "os"
    "sync"
    "time"
)

var bufPool = sync.Pool{
    New: func() interface{} {
        // The Pool's New function should generally only return pointer
        // types, since a pointer can be put into the return interface
        // value without an allocation:
        return new(bytes.Buffer)
    },
}

// timeNow is a fake version of time.Now for tests.
func timeNow() time.Time {
    return time.Unix(1136214245, 0)
}

func Log(w io.Writer, key, val string) {
    b := bufPool.Get().(*bytes.Buffer)
    b.Reset()
    // Replace this with time.Now() in a real logger.
    b.WriteString(timeNow().UTC().Format(time.RFC3339))
    b.WriteByte(' ')
    b.WriteString(key)
    b.WriteByte('=')
    b.WriteString(val)
    w.Write(b.Bytes())
    bufPool.Put(b)
}

func ExamplePool() {
    Log(os.Stdout, "path", "/search?q=flowers")
    // Output: 2006-01-02T15:04:05Z path=/search?q=flowers
}
```

> Pool 是 goroutine 安全的 但 最好的情況下 不需要 加鎖  Pool 爲每個 P 分配了 子池 Get/Put 優先從 自己P的子池 中 Get/Put 此時 無需加鎖
> 

# sync/atomic

sync/atomic 包 提供了 對 一些數據的 原子操作 主要是

* int32
* int64
* uint32
* uint64
* unsafe.Pointer
* uintptr

| 函數 | 功能 | 返回值 |
| -------- | -------- | -------- |
| AddXXX     | 爲原子增加一個值     | 增加後的值     |
| LoadXXX     | 返回原子的值     | 原子的值     |
| StoreXXX     | 爲原子設置值     | 無返回值     |
| SwapXXX     | 爲原子設置值     | 返回原子的舊值     |
| CompareAndSwapXXX     | 如果原子值等於指定值 則爲其設置新值    | 返回是否設置了新值     |



## type Value

type Value 提供了 Load Store 函數 用於 原子的 存入 讀取 一個 interface{}


