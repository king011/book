# shelf

[shelf](https://pub.dev/packages/shelf) 庫 提供了一個 http 服務器架構 但是 shelf 未提供路由功能

[shelf_router](https://pub.dev/packages/shelf_router) 是一個 非google維護 由google開發者提供的 shelf 路由庫

```dart
#info="main.dart"

import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import './router.dart';

void main(List<String> args) async {
  final router = Router();

  /// 創建一個 /api 分組
  final r = router.group('api');

  /// 爲分組添加 通用 中間件
  r.use(logRequests());

  /// 分別註冊 /api 和 /api/ 路由
  r.get('', _responseAPI);
  r.get('/', _responseAPI);

  /// 註冊其它測試
  r.get('/json', _responseJSON); // 分隔符是可選的 如果沒有會自動添加
  r.get('html', _responseHTML);
  r.get('text', _responseText);
  r.get('xml', _responseXML);

  // 返回 檔案
  r.get('README.md', _responseFILE);

  // uri 參數
  r.get('<id>/<name>', _responseURI);
  r.get('download/<path|[^]*>', _responseDownload); // 匹配前綴模式

  var server = await io.serve(router.handler, InternetAddress.anyIPv6, 9000);
  print('Serving at http://${server.address.host}:${server.port}');
}

Response _responseAPI(Request request) {
  return Response.ok(
    'api test 默認',
    headers: {
      'content-type': MinType.text,
    },
  );
}

Response _responseJSON(Request request) {
  return Response.ok(
    '''
{
  "urls" : [
    "google.com",
    "book.king011.com"
  ]
}
''',
    headers: {
      'content-type': MinType.json,
    },
  );
}

Response _responseHTML(Request request) {
  return Response.ok(
    '''
<html>
  <header>
    <title>測試</title>
  </header>
  <body>
    cerberus is an idea
  </body>
</html>  
''',
    headers: {
      'content-type': MinType.html,
    },
  );
}

Response _responseText(Request request) {
  return Response.ok(
    '文本測試',
    headers: {
      'content-type': MinType.text,
    },
  );
}

Response _responseXML(Request request) {
  return Response.ok(
    '''<?xml version="1.0" encoding="UTF-8"?>
<root>
  <name>kate</name>
  <lv>2</lv>
  <urls>
    <url>google.com</url>
    <url>book.king011.com</url>
  </urls>
</root>
''',
    headers: {
      'content-type': MinType.xml,
    },
  );
}

Response _responseFILE(Request request) {
  final stream = File('README.md').openRead();
  return Response.ok(
    stream,
    headers: {
      'content-type': MinType.typeByExtension('.md'),
    },
  );
}

Response _responseURI(Request request) {
  final Map<String, String> params = request.context['shelf_router/params'];
  final id = Uri.decodeComponent(params['id'] ?? '');
  final name = Uri.decodeComponent(params['name'] ?? '');
  return Response.ok(
    'id=$id name=$name',
    headers: {
      'content-type': MinType.text,
    },
  );
}

Response _responseDownload(Request request) {
  final Map<String, String> params = request.context['shelf_router/params'];
  final path = Uri.decodeComponent(params['path'] ?? '');
  return Response.ok(
    'path=$path',
    headers: {
      'content-type': MinType.text,
    },
  );
}
```
# Router

shelf_router 未提供 分組功能 上面示例代碼則 使用了 重寫包裝後的 Router 和一些輔助函數 具體實現如下

```
#info="router.dart"

import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart' as shelf_router;

const minType = {
  '.htm': 'text/html; charset=utf-8',
  '.html': 'text/html; charset=utf-8',
  '.shtml': 'text/html; charset=utf-8',
  '.css': 'text/css; charset=utf-8',
  '.xml': 'text/xml; charset=utf-8',
  '.gif': 'image/gif',
  '.jpeg': 'image/jpeg',
  '.jpg': 'image/jpeg',
  '.js': 'application/javascript',
  '.mjs': 'application/javascript',
  '.atom': 'application/atom+xml',
  '.rss': 'application/rss+xml',

  //
  '.mml': 'text/mathml',
  '.txt': 'text/plain',
  '.jad': 'text/vnd.sun.j2me.app-descriptor',
  '.wml': 'text/vnd.wap.wml',
  '.htc': 'text/x-component',

  //
  '.png': 'image/png',
  '.tif': 'image/tiff',
  '.tiff': 'image/tiff',
  '.wbmp': 'image/vnd.wap.wbmp',
  '.ico': 'image/x-icon',
  '.jng': 'image/x-jng',
  '.bmp': 'image/x-ms-bmp',
  '.svg': 'image/svg+xml',
  '.svgz': 'image/svg+xml',
  '.webp': 'image/webp',

  //
  '.woff': 'application/font-woff ',
  '.jar': 'application/java-archive',
  '.war': 'application/java-archive',
  '.ear': 'application/java-archive',
  '.json': 'application/json',
  '.hqx': 'application/mac-binhex40',
  '.doc': 'application/msword',
  '.pdf': 'application/pdf',
  '.ps': 'application/postscript',
  '.eps': 'application/postscript',
  '.ai': 'application/postscript',
  '.rtf': 'application/rtf',
  '.m3u8': 'application/vnd.apple.mpegurl',
  '.wmlc': 'application/vnd.wap.wmlc',
  '.xls': 'application/vnd.ms-excel',
  '.eot': 'application/vnd.ms-fontobject',
  '.ppt': 'application/vnd.ms-powerpoint',
  '.kml': 'application/vnd.google-earth.kml+xml',
  '.kmz': 'application/vnd.google-earth.kmz',
  '.7z': 'application/x-7z-compressed',
  '.cco': 'application/x-cocoa',
  '.jardiff': 'application/x-java-archive-diff',
  '.jnlp': 'application/x-java-jnlp-file',
  '.run': 'application/x-makeself',
  '.pl': 'application/x-perl',
  '.pm': 'application/x-perl',
  '.prc': 'application/x-pilot',
  '.pdb': 'application/x-pilot',
  '.sea': 'application/x-sea',
  '.sit': 'application/x-stuffit',
  '.tcl': 'application/x-tcl',
  '.tk': 'application/x-tcl',
  '.xpi': 'application/x-xpinstall',
  '.xhtml': 'application/xhtml+xml',
  '.xspf': 'application/xspf+xml',
  '.rar': 'application/x-rar-compressed',
  '.rpm': 'application/x-redhat-package-manager',
  '.swf': 'application/x-shockwave-flash',
  '.zip': 'application/zip',
  '.der': 'application/x-x509-ca-cert',
  '.pem': 'application/x-x509-ca-cert',
  '.crt': 'application/x-x509-ca-cert',

  //
  '.bin': 'application/octet-stream',
  '.exe': 'application/octet-stream',
  '.dll': 'application/octet-stream',
  '.deb': 'application/octet-stream',
  '.dmg': 'application/octet-stream',
  '.iso': 'application/octet-stream',
  '.img': 'application/octet-stream',
  '.msi': 'application/octet-stream',
  '.msp': 'application/octet-stream',
  '.msm': 'application/octet-stream',
  '.wasm': 'application/wasm',

//
  '.docx':
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  '.pptx':
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  //
  '.mid': 'audio/midi',
  '.midi': 'audio/midi',
  '.kar': 'audio/midi',
  '.mp3': 'audio/mpeg',
  '.ogg': 'audio/ogg',
  '.m4a': 'audio/x-m4a',
  'ra': 'audio/x-realaudio ',
  //
  '.3gpp': 'video/3gpp',
  '.3gp': 'video/3gpp',
  '.ts': 'video/mp2t',
  '.mp4': 'video/mp4',
  '.mpeg': 'video/mpeg',
  '.mpg': 'video/mpeg',
  '.mov': 'video/quicktime',
  '.webm': 'video/webm',
  '.flv': 'video/x-flv',
  '.m4v': 'video/x-m4v',
  '.mng': 'video/x-mng',
  '.avi': 'video/x-msvideo',
  '.asx': 'video/x-ms-asf',
  '.asf': 'video/x-ms-asf',
  '.wmv': 'video/x-ms-wmv',
};

class MinType {
  const MinType._();
  static final xml = 'application/xml; charset=utf-8';
  static final text = 'text/plain; charset=utf-8';
  static final html = 'text/html; charset=utf-8';
  static final json = 'application/json; charset=utf-8';
  static final binary = 'application/octet-stream';
  static String typeByExtension(String ext) {
    ext = ext.toLowerCase();
    return minType[ext] ?? 'application/octet-stream';
  }
}

String _getPath(String baseURL, String relativePath) {
  if (relativePath.isEmpty) {
    return baseURL;
  }
  if (baseURL.endsWith('/')) {
    if (relativePath == '/') {
      return baseURL;
    }
    return '$baseURL$relativePath';
  } else {
    if (relativePath.startsWith('/')) {
      return '$baseURL$relativePath';
    }
    return '$baseURL/$relativePath';
  }
}

Handler _getHandler(
  Pipeline pipeline,
  Handler handler,
) {
  if (pipeline == null) {
    return handler;
  }
  return pipeline.addHandler(handler);
}

/// 包裝一下 shelf_router.Router 提供 分組功能 提供修正 未定義頁面 沒有返回 404 的問題
class Router {
  shelf_router.Router _router;
  final String baseURL;
  Pipeline _pipeline;
  Router({
    this.baseURL = '/',
    shelf_router.Router router,
  }) : assert(baseURL != null) {
    if (router == null) {
      _router = shelf_router.Router();
    } else {
      _router = router;
    }
  }
  Handler get handler {
    return (Request request) async {
      final response = await _router.handler(request);
      if (response == null) {
        return Response.notFound('page not found ${request.requestedUri.path}');
      }
      return response;
    };
  }

  Router use(Middleware middleware) {
    if (_pipeline == null) {
      _pipeline = Pipeline().addMiddleware(middleware);
    } else {
      _pipeline = _pipeline.addMiddleware(middleware);
    }
    return this;
  }

  Router group(String relativePath) {
    return Router(
      baseURL: _getPath(baseURL, relativePath),
      router: _router,
    );
  }

  void any(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('ANY  $path');
    _router.all(path, _getHandler(_pipeline, handler));
  }

  void get(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('GET  $path');
    _router.get(path, (req) {
      return _getHandler(_pipeline, handler)(req);
    });
  }

  void head(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('HEAD  $path');
    _router.head(path, _getHandler(_pipeline, handler));
  }

  void post(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('POST  $path');
    _router.post(path, _getHandler(_pipeline, handler));
  }

  void put(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('PUT  $path');
    _router.put(path, _getHandler(_pipeline, handler));
  }

  void delete(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('DELETE  $path');
    _router.delete(path, _getHandler(_pipeline, handler));
  }

  void connect(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('CONNECT  $path');
    _router.connect(path, _getHandler(_pipeline, handler));
  }

  void options(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('OPTIONS  $path');
    _router.options(path, _getHandler(_pipeline, handler));
  }

  void trace(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('TRACE  $path');
    _router.trace(path, _getHandler(_pipeline, handler));
  }

  void patch(String relativePath, Handler handler) {
    final path = _getPath(baseURL, relativePath);
    print('PATCH  $path');
    _router.patch(path, _getHandler(_pipeline, handler));
  }
}
```

# Middleware gzip
中間件 是一個 參數是Handler(上個處理器) 返回值是 Handler(當前中間件處理器) 的函數

另外 shelf 提供了 createMiddleware 函數 方便創建 中間件

```
typedef Middleware = Handler Function(Handler innerHandler);

Middleware createMiddleware(
    {FutureOr<Response> Function(Request) requestHandler,
    FutureOr<Response> Function(Response) responseHandler,
    FutureOr<Response> Function(dynamic error, StackTrace) errorHandler}) 
```

如下 例子演示了如何創建一個 提供 gzip 壓縮的 中間件

```
import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import './router.dart';

void main(List<String> args) async {
  final router = Router();

  /// 創建一個 /api 分組
  final r = router.group('api');

  /// 爲分組添加 通用 中間件
  r.use(logRequests());

  /// 分別註冊 /api 和 /api/ 路由
  r.get('', _responseAPI);

  /// 爲特定請求設置 中間件
  final gz = _gzipRequests();
  r.get('json', Pipeline().addMiddleware(gz).addHandler(_responseJSON));
  r.get('README.md', Pipeline().addMiddleware(gz).addHandler(_responseFILE));

  var server = await io.serve(router.handler, InternetAddress.anyIPv6, 9000);
  print('Serving at http://${server.address.host}:${server.port}');
}

Response _responseAPI(Request request) {
  return Response.ok(
    'api test 默認',
    headers: {
      'content-type': MinType.text,
    },
  );
}

Response _responseJSON(Request request) {
  return Response.ok(
    '''
{
  "urls" : [
    "google.com",
    "book.king011.com"
  ]
}
''',
    headers: {
      'content-type': MinType.json,
    },
  );
}

Response _responseFILE(Request request) {
  final stream = File('README.md').openRead();
  return Response.ok(
    stream,
    headers: {
      'content-type': MinType.typeByExtension('.md'),
    },
  );
}

Middleware _gzipRequests(
        {void Function(String message, bool isError) logger}) =>
    (innerHandler) {
      return (request) {
        return Future.sync(() => innerHandler(request)).then((response) {
          if (!shouldCompress(request, response)) {
            return response;
          }
          final gz = ZLibEncoder(gzip: true);
          var headers = {
            'content-encoding': 'gzip',
          }
            ..addAll(response.headers)
            ..remove('content-length')
            ..remove('accept-ranges');
          return Response(
            response.statusCode,
            body: gz.bind(response.read()),
            headers: headers,
            context: response.context,
          );
        }, onError: (error, StackTrace stackTrace) {
          throw error;
        });
      };
    };
const exclude = {
  '.png',
  '.gif',
  '.jpeg',
  '.jpg',
};
bool shouldCompress(Request request, Response response) {
  // 沒有 body 不用壓縮
  if (response.statusCode != HttpStatus.ok || response.contentLength == 0) {
    return false;
  }
  final contentLength = response.contentLength;
  if (contentLength is int) {
    // 已知長度 只壓縮大小在 [1k,10m] 的
    if (contentLength < 1024 || contentLength > 1024 * 1024 * 10) {
      return false;
    }
  }
  // 驗證 是否支持 gzip
  final encoding = request.headers['Accept-Encoding'];
  if (!encoding.contains('gzip')) {
    return false;
  } else if (request.headers['Connection']?.contains('Upgrade') ?? false) {
    return false;
  } else if (request.headers['Content-Type']?.contains('text/event-stream') ??
      false) {
    return false;
  }

  // 排除 一些 壓縮效果差的
  final path = request.requestedUri.path;
  final index = path.lastIndexOf('.');
  if (index < 0) {
    return false;
  }
  final ext = path.substring(index).toLowerCase();
  if (exclude.contains(ext)) {
    return false;
  }
  return true;
}
```

# asset
shelf 沒有提供很好的 檔案下載支持 雖然官方由提供 shelf\_static 但是 shelf\_static 不支持 分塊下載 如果需要支持 只有自己實現

並且 shelf_static 只支持檔案 無法支持 抽象的 將內存資源 作爲下載對象

```
#info="asset.dart"

import 'dart:async';
import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:http_parser/http_parser.dart';

abstract class Asset {
  /// 返回 指定位置的 讀取流
  Stream<List<int>> openRead([int start, int end]);

  /// 返回 資源 最後修改時間
  FutureOr<DateTime> get changed;

  /// 返回 資源大小
  FutureOr<int> get size;

  /// 返回資源 類型
  FutureOr<String> get contentType;
}

Handler createAssetHandler(Asset asset) {
  return (request) {
    return _handleAsset(request, asset);
  };
}

FutureOr<Response> _handleAsset(Request request, Asset asset) async {
  var ifModifiedSince = request.ifModifiedSince;

  /// lastModifiedHeader
  final changed = await asset.changed;
  final headers = {
    HttpHeaders.lastModifiedHeader: formatHttpDate(changed),
    HttpHeaders.acceptRangesHeader: 'bytes',
  };
  final contentType = await asset.contentType;
  if (contentType != null) {
    headers[HttpHeaders.contentTypeHeader] = contentType;
  }

  /// check notModified
  if (ifModifiedSince != null) {
    var fileChangeAtSecResolution = toSecondResolution(changed);
    if (!fileChangeAtSecResolution.isAfter(ifModifiedSince)) {
      return Response.notModified();
    }
  }

  /// range
  final size = await asset.size;
  List<HttpRange> ranges;
  if (size > 0) {
    try {
      ranges = parseRange(request, changed, size);
      if (sumRangesSize(ranges) > size) {
        /// 超過資源整體 大小 直接返回整個資源
        ranges = null;
      }
    } catch (e) {
      headers[HttpHeaders.contentRangeHeader] = 'bytes */$size';
      headers[HttpHeaders.contentTypeHeader] = 'text/plain; charset=utf-8';
      return Response(
        HttpStatus.requestedRangeNotSatisfiable,
        headers: headers,
        body: '$e',
      );
    }
  }
  final length = ranges?.length ?? 0;
  if (length == 1) {
    /// 返回單個塊
    final range = ranges[0];
    headers[HttpHeaders.contentRangeHeader] = range.contentRange(size);
    return Response(
      HttpStatus.partialContent,
      body: asset.openRead(range.start, range.end),
      headers: headers,
    );
  } else if (length > 1) {
    /// 暫不支持 返回多塊
    headers[HttpHeaders.contentRangeHeader] = 'bytes */$size';
    headers[HttpHeaders.contentTypeHeader] = 'text/plain; charset=utf-8';
    return Response(
      HttpStatus.requestedRangeNotSatisfiable,
      headers: headers,
      body: 'not supported multipart/byteranges',
    );
  }

  /// 200
  return Response.ok(asset.openRead(), headers: headers);
}

List<HttpRange> parseRange(Request request, DateTime changed, int size) {
  var s = request.headers['Range'];
  if (s?.isEmpty ?? true) {
    return null;
  }
  final ir = request.headers['If-Range'];
  if (ir?.isNotEmpty ?? false) {
    final IfRange = parseHttpDate(ir);
    var fileChangeAtSecResolution = toSecondResolution(changed);
    if (fileChangeAtSecResolution.isAfter(IfRange)) {
      return null;
    }
  }
  const b = 'bytes=';
  if (!s.startsWith(b)) {
    throw Exception('invalid range');
  }

  final ranges = <HttpRange>[];
  var noOverlap = false;
  final strs = s.substring('bytes='.length).split(',');
  for (var ra in strs) {
    ra = ra.trim();
    if (ra.isEmpty) {
      continue;
    }
    var i = ra.indexOf('-');
    if (i < 0) {
      throw Exception('invalid range');
    }
    var start = ra.substring(0, i).trim();
    var end = ra.substring(i + 1).trim();
    final r = HttpRange();
    if (start.isEmpty) {
      // If no start is specified, end specifies the
      // range start relative to the end of the file.
      i = int.parse(end);
      if (i > size) {
        i = size;
      }
      r.start = size - i;
      r.length = size - r.start;
    } else {
      i = int.parse(start);
      if (i < 0) {
        throw Exception('invalid range');
      }
      if (i >= size) {
        // If the range begins after the size of the content,
        // then it does not overlap.
        noOverlap = true;
        continue;
      }
      r.start = i;
      if (end.isEmpty) {
        // If no end is specified, range extends to end of the file.
        r.length = size - r.start;
      } else {
        i = int.parse(end);
        if (r.start > i) {
          throw Exception('invalid range');
        }
        if (i >= size) {
          i = size - 1;
        }
        r.length = i - r.start + 1;
      }
    }
    ranges.add(r);
  }
  if (noOverlap && (ranges.isEmpty ?? true)) {
    // The specified ranges did not overlap with the content.
    throw Exception('invalid range: failed to overlap');
  }

  return ranges;
}

int sumRangesSize(List<HttpRange> ranges) {
  var size = 0;
  ranges?.forEach((element) {
    size += element.length;
  });
  return size;
}

class HttpRange {
  int start;
  int length;

  int get end => start + length;
  String contentRange(int size) {
    return 'bytes $start-${start + length - 1}/$size';
  }
}

DateTime toSecondResolution(DateTime dt) {
  if (dt.millisecond == 0) return dt;
  return dt.subtract(Duration(milliseconds: dt.millisecond));
}

/// 本地檔案
class FileAsset implements Asset {
  final File file;
  FileAsset(this.file, {this.contentType}) : assert(file != null);

  /// 返回 指定位置的 讀取流
  @override
  Stream<List<int>> openRead([int start, int end]) => file.openRead(start, end);

  /// 返回 資源 最後修改時間
  @override
  FutureOr<DateTime> get changed => file.stat().then((stat) => stat.modified);

  /// 返回 資源大小
  @override
  FutureOr<int> get size => file.stat().then((stat) => stat.size);

  /// 返回資源 類型
  @override
  final String contentType;
}
```

```
#info="main.dart"

import 'dart:async';
import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_static/shelf_static.dart' as shelf_static;
import './router.dart';
import './asset.dart';

void main(List<String> args) async {
  final router = Router();

  /// 創建一個 /api 分組
  final r = router.group('api');

  /// 爲分組添加 通用 中間件
  r.use(logRequests());

  r.get('download/<path|[^]*>', _responseDownload);
  r.get('assets/<path|[^]*>', _responseAssets);

  var server = await io.serve(router.handler, InternetAddress.anyIPv6, 9000);
  print('Serving at http://${server.address.host}:${server.port}');
}

FutureOr<Response> _responseAssets(Request request) {
  final Map<String, String> params = request.context['shelf_router/params'];
  final path = Uri.decodeComponent(params['path'] ?? '');
  print('assets : $path');
  final index = path.lastIndexOf('.');
  String ext;
  if (index != -1) {
    ext = path.substring(index);
  }
  final handler = createAssetHandler(FileAsset(
    File(path),
    contentType: MinType.typeByExtension(ext),
  ));
  return handler(request);
}

FutureOr<Response> _responseDownload(Request request) {
  final Map<String, String> params = request.context['shelf_router/params'];
  final path = Uri.decodeComponent(params['path'] ?? '');
  print('download : $path');
  final handler = shelf_static.createFileHandler(
    path,
    url: request.url.path,
  );
  return handler(request);
}
```