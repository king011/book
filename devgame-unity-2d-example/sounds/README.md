# 聲音

在 unity 裏加入聲音非常簡單，但它在遊戲裏的確是非常重要的

# 尋找音樂和音效

Damien 最近的文章 [participated in an interesting subject on Stack Exchange](https://gamedev.stackexchange.com/questions/22525/how-should-a-one-man-team-do-game-audio) 就是討論這個主題的

在我們看來，一個遊戲開發者可以：
* 買聲音
* 聘用一個音樂人
* 從 sound banks 裏找免費的(像是 [FindSounds](https://www.findsounds.com/) 或 [Freesound](https://freesound.org/))
* 自己錄製
* 使用 [BFXR](http://www.bfxr.net/) 製作 chiptune (8-bit) 聲音(基於 [SFXR](http://drpetter.se/project_sfxr.html)，但使用網頁版本非常有用)

關於音樂，得看你的需求

* [Jamendo](https://www.jamendo.com/) 有大量的藝術家，但小心商業許可協議
* [Bosca Ceoil](http://distractionware.com/blog/2013/08/bosca-ceoil/) 是一個來自 Terry Cavanagh 製作音樂的簡單軟體

# 本教學使用的素材

* [player 射擊音效](https://pixelnest.io/tutorials/2d-game-unity/sounds/-sounds/shot_player.wav)
* [enemy 射擊音效](https://pixelnest.io/tutorials/2d-game-unity/sounds/-sounds/shot_enemy.wav)
* [爆炸音效](https://pixelnest.io/tutorials/2d-game-unity/sounds/-sounds/explosion.wav)
* [背景音樂](https://storage-new.newjamendo.com/download/track/730917/mp32/)

將這四個音樂檔案導入到 unity 的 Sounds 目錄裏

# 播放音樂

要播放音樂，簡單的將音樂資源拖入 **Hierarchy** 窗口即可

![](assets/music.png)

**注意** 當你做許多音樂測試時 **Mute**(靜音) 選項會很有用

# 播放音效

遊戲裏，音樂是一直播放的，而音效只在需要時觸發。

所以我們像之前的 **SpecialEffectsHelper** 一樣創建一個 **SoundEffectsHelper** 供其它腳本播放音效

```
#info="SoundEffectsHelper.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectsHelper : MonoBehaviour
{
    // 單例
    public static SoundEffectsHelper Instance;

    // 各種音效的 prefab
    public AudioClip explosionSound;
    public AudioClip playerShotSound;
    public AudioClip enemyShotSound;

    void Awake()
    {
        // Register the singleton
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of SoundEffectsHelper!");
        }
        Instance = this;
    }

    public void MakeExplosionSound()
    {
        MakeSound(explosionSound);
    }

    public void MakePlayerShotSound()
    {
        MakeSound(playerShotSound);
    }

    public void MakeEnemyShotSound()
    {
        MakeSound(enemyShotSound);
    }


    private void MakeSound(AudioClip originalClip)
    {
        // As it is not 3D audio clip, position doesn't matter.
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }
}
```

最後記得將腳本添加到 Scripts 節點 並設置好 prefab

![](assets/sound_script.png)

