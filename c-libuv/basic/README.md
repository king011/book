# Hello World

```
#include <iostream>
#include <uv.h>
int main(int argc, char *argv[])
{
    uv_loop_t loop;
    uv_loop_init(&loop);
    printf("Now quitting.\n");
    uv_run(&loop, UV_RUN_DEFAULT);
    uv_loop_close(&loop);
    return 0;
}
```

1. uv\_loop\_init 初始化 loop
2. uv\_run 運行 loop
3. uv\_loop\_close 釋放 loop

uv\_default\_loop() 函數會返回一個默認的 loop 如果程序中只有一個 loop 應該使用 此函數

```
#include <iostream>
#include <uv.h>
int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();
    printf("Now quitting.\n");
    uv_run(loop, UV_RUN_DEFAULT);
    return 0;
}
```

# [錯誤處理](http://docs.libuv.org/en/v1.x/errors.html)

在libuv中錯誤是負數常量，只要有狀態參數和API函數返回整數 負數意味着錯誤。

當接受回調的函數返回錯誤時，將永遠不會回調。

libuv 提供了幾個錯誤相關的函數：
* const char \*uv\_strerror(int err) 返回錯誤代碼的消息，當使用未知錯誤調用時它會泄漏幾個字節內存
* char \*\uv\_strerror\_r(int err, char \*buf, size_t buflen) 將以0結尾的錯誤消息最多 buflen 個字節 拷貝到 buf
* const char \*uv\_err\_name(int err) 返回錯誤代碼的名稱，當使用未知錯誤調用時它會泄漏幾個字節內存
* char \*uv\_err\_name\_r(int err, char \*buf, size_t buflen) 將以0結尾的錯誤名稱最多 buflen 個字節 拷貝到 buf
* int uv\_translate\_sys\_error(int sys_errno) 返回給定平臺相關錯誤代碼等效的 libuv 錯誤代碼(Unix 的POSIX平臺通常存儲在 errno 中的錯誤代碼，Windows上 GetLastError WSAGetLastError 返回)

# Handles Requests

libuv 工作在用於感興趣的事件上，這通常由句柄表示，命名爲 uv\_**TYPE**\_t，其中表示句柄用處

```
/* Handle types. */
typedef struct uv_loop_s uv_loop_t;
typedef struct uv_handle_s uv_handle_t;
typedef struct uv_dir_s uv_dir_t;
typedef struct uv_stream_s uv_stream_t;
typedef struct uv_tcp_s uv_tcp_t;
typedef struct uv_udp_s uv_udp_t;
typedef struct uv_pipe_s uv_pipe_t;
typedef struct uv_tty_s uv_tty_t;
typedef struct uv_poll_s uv_poll_t;
typedef struct uv_timer_s uv_timer_t;
typedef struct uv_prepare_s uv_prepare_t;
typedef struct uv_check_s uv_check_t;
typedef struct uv_idle_s uv_idle_t;
typedef struct uv_async_s uv_async_t;
typedef struct uv_process_s uv_process_t;
typedef struct uv_fs_event_s uv_fs_event_t;
typedef struct uv_fs_poll_s uv_fs_poll_t;
typedef struct uv_signal_s uv_signal_t;

/* Request types. */
typedef struct uv_req_s uv_req_t;
typedef struct uv_getaddrinfo_s uv_getaddrinfo_t;
typedef struct uv_getnameinfo_s uv_getnameinfo_t;
typedef struct uv_shutdown_s uv_shutdown_t;
typedef struct uv_write_s uv_write_t;
typedef struct uv_connect_s uv_connect_t;
typedef struct uv_udp_send_s uv_udp_send_t;
typedef struct uv_fs_s uv_fs_t;
typedef struct uv_work_s uv_work_t;
```

Handlers 代表一個長期的對象，比如一個 udp套接字用 uv\_udp\_t 表示。Requests 用來表示句柄上一個短暫的操作，比如用 uv\_udp\_send\_t 表示向 udp 寫入一次數據

uv\_TYPE\_init 函數用於初始化句柄：
```
int uv_TYPE_init(uv_loop_t *, uv_TYPE_t *)
```

# idle

idle 是空閒句柄，回調在事件循環的每一輪都會調用一次。

```
#include <iostream>
#include <uv.h>
void wait_for_a_while(uv_idle_t *handle)
{
    size_t counter = (size_t)(handle->data) + 1;
    handle->data = (void *)counter;
    printf("idle %ld\n", counter);
    if (counter >= 10)
    {
        uv_idle_stop(handle);
    }
}
int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();

    uv_idle_t idler;
    uv_idle_init(loop, &idler);
    idler.data = 0;
    uv_idle_start(&idler, wait_for_a_while);

    uv_run(loop, UV_RUN_DEFAULT);
    return 0;
}
```

所有 handle 都帶有一個 名稱爲 data 的 void\* 成員 用於在回調中傳遞用戶自定義數據

