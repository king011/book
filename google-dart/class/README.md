# 訪問 實例 方法

* 通過 \. 訪問
* 提供 \.\. 級聯 訪問
* 通過 \.? 在非null時 訪問

```dart
class Point {
  int x = 0;
  int y = 0;
  // 構造 函數 this.x=x this.y=y 的 語法糖 寫法
  Point(this.x, this.y);
  // 命名 構造函數
  Point.fromJson() : x = 100 {
    y = 200;
  }
  display() => print("x=$x\ny=$y");
}

main() {
  // 創建 實例
  var p0 = Point(1, 2);
  // 訪問 方法
  p0.display();
  p0.x = 10;

  // 級聯 訪問
  p0..display()..display();
  p0 = null;

  // 非 null 時 訪問
  p0?.display();

  // 命名 構造
  p0 = Point.fromJson();
  p0.display();
}
```

# 構造實例

* 通過 new 構造 函數 來 創建 實例 
* 在 dart2 中 new 關鍵字是 可選的
* 通過 const 可以 創建 實例常量 多個 常量 返回的 是相同實例

```dart
var p1 = Point(2, 2);
var p2 = Point.fromJson({'x': 1, 'y': 2});
```

```dart
var p1 = new Point(2, 2);
var p2 = new Point.fromJson({'x': 1, 'y': 2});
```

```dart
var a = const ImmutablePoint(1, 1);
var b = const ImmutablePoint(1, 1);

assert(identical(a, b)); // They are the same instance!
```

在 常量 上下文中 可以省略 構造前的 const

```dart
// Lots of const keywords here.
const pointAndLine = const {
  'point': const [const ImmutablePoint(0, 0)],
  'line': const [const ImmutablePoint(1, 10), const ImmutablePoint(-2, 11)],
};
```

```dart
// Only one const, which establishes the constant context.
const pointAndLine = {
  'point': [ImmutablePoint(0, 0)],
  'line': [ImmutablePoint(1, 10), ImmutablePoint(-2, 11)],
};
```

```dart
var a = const ImmutablePoint(1, 1); // Creates a constant
var b = ImmutablePoint(1, 1); // Does NOT create a constant

assert(!identical(a, b)); // NOT the same instance!
```

# runtimeType

實例的 runtimeType getter 會 返回 運行時 信息

```dart
main() {
  // 創建 實例
  var p0 = Point(1, 2);
  print("${p0.runtimeType}"); // Point
  p0 = null;
  print("${p0.runtimeType}"); // Null
}
```

# 實例變量

class 的所有 實例變量 默認都爲 null  並且 帶有 默認的 getters setters


# 構造 函數

如果沒有提供任何 構造函數 dart 默認 提供一個 空 實現的 構造 函數 

構造函數 可以 和 class 同名
```dart
class Point {
  num x, y;

  Point(num x, num y) {
    // There's a better way to do this, stay tuned.
    this.x = x;
    this.y = y;
  }
}
```

dart 提供了 語法糖 實現和上述一樣的 功能

```
class Point {
  num x, y;

  // Syntactic sugar for setting x and y
  // before the constructor body runs.
  Point(this.x, this.y);
}
```

dart 也支持 命名的 構造 函數 使用 class.名稱 定義 即可

```dart
class Point {
  num x, y;

  Point(this.x, this.y);

  // Named constructor
  Point.origin() {
    x = 0;
    y = 0;
  }
}
```

# 初始化 列表

dart 允許 在 構造 函數 中 使用 初始化 列表 來初始化 值 並且 可以 使用 assert 在 開發時 驗證 傳入 參數 範圍

```dart
class Point {
  int x = 0;
  int y = 0;

  //Point(this.x, this.y) : assert(x >= 0);
  Point(int x, y)
      : x = x,
        y = y,
        assert(x >= 0) {}
  @override
  String toString() {
    return "x=$x\ny=$y";
  }
}

main() {
  var p = Point(-1, 2);
  print("$p");
}
```

# 重定向 構造函數
dart 允許 重定向 構造函數

```dart
class Point {
  num x, y;

  // The main constructor for this class.
  Point(this.x, this.y);

  // Delegates to the main constructor.
  Point.alongXAxis(num x) : this(x, 0);
}
```

# 常量構造函數

如果向創建的對象 擁有不會改變 可以 使用 const final 使其成爲 編譯時常量

```dart
class ImmutablePoint {
  static final ImmutablePoint origin =
      const ImmutablePoint(0, 0);

  final num x, y;

  const ImmutablePoint(this.x, this.y);
}
```

# 工廠構造

如果 創建的 對象 不總是 新的 實例 比如 從緩存中 返回的 可以使用 factory 標明這是一個 工廠構造函數

```dart
class Logger {
  final String name;
  bool mute = false;

  // _cache is library-private, thanks to
  // the _ in front of its name.
  static final Map<String, Logger> _cache =
      <String, Logger>{};

  factory Logger(String name) {
    if (_cache.containsKey(name)) {
      return _cache[name];
    } else {
      final logger = Logger._internal(name);
      _cache[name] = logger;
      return logger;
    }
  }

  Logger._internal(this.name);

  void log(String msg) {
    if (!mute) print(msg);
  }
}
```

```dart
var logger = Logger('UI');
logger.log('Button clicked');
```

# Getters and setters

使用 set/get 關鍵字 可以 定義 Getters and setters

```dart
class Rectangle {
  num left, top, width, height;

  Rectangle(this.left, this.top, this.width, this.height);

  // Define two calculated properties: right and bottom.
  num get right => left + width;
  set right(num value) => left = value - width;
  num get bottom => top + height;
  set bottom(num value) => top = value - height;
}

void main() {
  var rect = Rectangle(3, 4, 20, 15);
  assert(rect.left == 3);
  rect.right = 12;
  assert(rect.left == -8);
}
```

# 虛函數 虛類

使用 abstract 定義一個 abstract class

不實現 函數體 定義 一個 abstract method

```dart
// This class is declared abstract and thus
// can't be instantiated.
abstract class AbstractContainer {
  // Define constructors, fields, methods...

  void updateChildren(); // Abstract method.
}
```

# 實現接口

每個 class 都 隱式定義了一個接口 該接口 包含 class 的所有 實例成員 和 方法

使用 關鍵字 implements 來實現 接口

```dart
// A person. The implicit interface contains greet().
class Person {
  // In the interface, but visible only in this library.
  final _name;

  // Not in the interface, since this is a constructor.
  Person(this._name);

  // In the interface.
  String greet(String who) => 'Hello, $who. I am $_name.';
}

// An implementation of the Person interface.
class Impostor implements Person {
  get _name => '';

  String greet(String who) => 'Hi $who. Do you know who I am?';
}

String greetBob(Person person) => person.greet('Bob');

void main() {
  print(greetBob(Person('Kathy')));
  print(greetBob(Impostor()));
}
```

# 派生 類
 使用 extends 從 超類 派生
 
```dart
 class Television {
  void turnOn() {
    _illuminateDisplay();
    _activateIrSensor();
  }
  // ···
}

class SmartTelevision extends Television {
  void turnOn() {
    super.turnOn();
    _bootNetworkInterface();
    _initializeMemory();
    _upgradeApps();
  }
  // ···
}
```

使用 @override 標籤 覆蓋 超類 方法

```dart
class SmartTelevision extends Television {
  @override
  void turnOn() {...}
  // ···
}
```

dart 支持 使用 operator 關鍵字 重載 操作符

<table class="table">
  <tbody>
    <tr>
      <td><span class="highlighter-rouge">&lt;</code></td>
      <td><span class="highlighter-rouge">+</code></td>
      <td><span class="highlighter-rouge">|</code></td>
      <td><span class="highlighter-rouge">[]</code></td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&gt;</code></td>
      <td><span class="highlighter-rouge">/</code></td>
      <td><span class="highlighter-rouge">^</code></td>
      <td><span class="highlighter-rouge">[]=</code></td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&lt;=</code></td>
      <td><span class="highlighter-rouge">~/</code></td>
      <td><span class="highlighter-rouge">&amp;</code></td>
      <td><span class="highlighter-rouge">~</code></td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">&gt;=</code></td>
      <td><span class="highlighter-rouge">*</code></td>
      <td><span class="highlighter-rouge">&lt;&lt;</code></td>
      <td><span class="highlighter-rouge">==</code></td>
    </tr>
    <tr>
      <td><span class="highlighter-rouge">–</code></td>
      <td><span class="highlighter-rouge">%</code></td>
      <td><span class="highlighter-rouge">&gt;&gt;</code></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>

```dart
class Vector {
  final int x, y;

  Vector(this.x, this.y);

  Vector operator +(Vector v) => Vector(x + v.x, y + v.y);
  Vector operator -(Vector v) => Vector(x - v.x, y - v.y);

  // Operator == and hashCode not shown. For details, see note below.
  // ···
}

void main() {
  final v = Vector(2, 3);
  final w = Vector(2, 2);

  assert(v + w == Vector(4, 5));
  assert(v - w == Vector(0, 1));
}
```

# enum

使用 enum 定義 枚舉

```dart
enum Color { red, green, blue }
```

# mixins

使用 with 組合 class

```dart
class Musician extends Performer with Musical {
  // ···
}

class Maestro extends Person
    with Musical, Aggressive, Demented {
  Maestro(String maestroName) {
    name = maestroName;
    canConduct = true;
  }
}
```

如果 class 只能 被 with 使用 用 mixin 替代 class 關鍵字

```dart
mixin Musical {
  bool canPlayPiano = false;
  bool canCompose = false;
  bool canConduct = false;

  void entertainMe() {
    if (canPlayPiano) {
      print('Playing piano');
    } else if (canConduct) {
      print('Waving hands');
    } else {
      print('Humming to self');
    }
  }
}
```
當一個 mixin 類想要組合其它 mixin 類時使用 **on** 關鍵字

```dart
mixin MusicalPerformer on Musician {
  // ···
}
```

# static

使用 static 定義 靜態 成員 和 方法

```dart
class Queue {
  static const initialCapacity = 16;
  // ···
}

void main() {
  assert(Queue.initialCapacity == 16);
}
```

```dart
import 'dart:math';

class Point {
  num x, y;
  Point(this.x, this.y);

  static num distanceBetween(Point a, Point b) {
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return sqrt(dx * dx + dy * dy);
  }
}

void main() {
  var a = Point(2, 2);
  var b = Point(4, 4);
  var distance = Point.distanceBetween(a, b);
  assert(2.8 < distance && distance < 2.9);
  print(distance);
}
```

# Callable classes

如果 想讓 實例 像 函數一樣 可以被 調用 實現 call 方法 即可

```dart
class WannabeFunction {
  call(String a, String b, String c) => '$a $b $c!';
}

main() {
  var wf = new WannabeFunction();
  var out = wf("Hi","there,","gang");
  print('$out');
}
```