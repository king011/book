# 庫

使用 import 引入 庫 dart 內置的 庫 名字空間爲 dart

pacakge 名字 空間 有 pub 提供

```dart
import 'dart:html';

import 'package:test/test.dart';
```

# 別名
dart 允許 在 import 時 指定一個 別名 以解決 多個庫中 標識符號 重名

```dart
import 'package:lib1/lib1.dart';
import 'package:lib2/lib2.dart' as lib2;

// Uses Element from lib1.
Element element1 = Element();

// Uses Element from lib2.
lib2.Element element2 = lib2.Element()
```

# import 部分
dart 運行 只 import 部分 標識符

```dart
// Import only foo.
import 'package:lib1/lib1.dart' show foo;

// Import all names EXCEPT foo.
import 'package:lib2/lib2.dart' hide foo;
```

# 延遲加載

dart 允許 延遲 加載 庫

1. 使用 deferred 聲明 import

	```dart
	import 'package:greetings/hello.dart' deferred as hello;
	```

1. 調用 loadLibrary 加載 庫

	```dart
	Future greet() async {
		await hello.loadLibrary();
		hello.printGreeting();
	}
	```
	
可以 多次 調用 loadLibrary dart 在後續 loadLibrary 中 直接 返回的 緩存