# 切換版本

如果有一些舊的 flutter 項目需要重新編譯，可能就需要切換 flutter 版本。flutter 使用 git 管理所以

1. 使用 tag 查看版本

	```
	git tag
	```
	
2. 使用 checkout 切換到目標版本


	```
	git checkout XXX
	```
	
3. 重新下載編譯依賴工具

	```
	flutter doctor
	```
	
當需要換回新版本時需要執行下述指令

```
# 切換到穩定版本
flutter channel stable

# 更新到最新版本
flutter upgrade
```