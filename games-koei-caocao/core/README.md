# 核心角色

曹操傳出場人物衆多，但廢物也很多，故需要挑選核心角色進行培養。

# 無可替代

基本沒有爭議，絕對的核心角色

1. 曹操 -> 主角必須出站
2. 司馬懿 -> 唯一騎馬軍師帶青龍寶玉，可沙暴全屏消弱敵人，可控制天氣爲火系策略提供使用保障
3. 賈詡 -> 道士人選帶玄武寶玉，和郭嘉屬性差不多，郭嘉可以換遁甲天數，故選賈詡比較划算
4. 荀攸 -> 風水師帶白虎寶玉，自帶防禦升檔到 B
5. 荀彧 -> 策士帶朱雀寶玉，主要法術輸出，需要 4 個統率果升檔到 A
6. 滿寵 -> 風水師太有用，回歸使核心角色多行動一會合，各種 buff 和 補給 故兩個都帶，只需要 1 個統率果使防禦升檔到 B
7. 許褚 -> 唯一賊兵，山路無響應，沒羽箭增加攻擊範圍，輸出 S ，防禦自動升檔到 A ，敏捷 B，士氣 S ，需要 11 個敏捷果使用爆發升檔到 A
8. 劉曄 -> 唯一炮兵，大範圍輸出，帶上金火灌炮攻擊還帶毒，需要 9 個武力果恢復攻擊到 S，9 個敏捷果升檔到B，耗果巨大但不吃過也輸出很高，並可以後期才出戰追等級很快前期用處也不大
9. 于禁 -> 步弓兵，自帶爆發升檔到 A，需要 7個統率果使防禦升檔到 A
10. 李典 -> 步兵，需要4個武力果，使攻擊升檔到 A
11. 夏侯惇 -> 騎兵，攻擊 S，防禦 A，自帶爆發升檔到 A，使用12個運氣果使士氣升檔到 A
12. 張遼 -> 騎兵，攻擊 S，防禦 A，自帶士氣升檔到 A，使用6個敏捷果使爆發升檔到 A

最終站可出場15人，可以從候補中選擇三個

# 可選角色
還有一些可選的角色

1. 張郃 -> 弓騎，需要 8 個敏捷果使爆發升檔到 A，11 個統率果使防禦升檔到 A，建議只吃敏捷果
2. 夏侯淵 -> 弓騎，需要 12 個敏捷果使爆發升檔到 A，5 個統率果使防禦升檔到 A，建議只吃統率果帶呂布弓
3. 貂蟬 -> 舞娘，自帶爲周圍友軍解除負面狀態，需要 13 個統率果使防禦升檔到 A，4 個運氣果使士氣升檔到 A
4. 楽進 -> 步兵，需要6個武力果，使攻擊升檔到 A，可考慮再加兩個運氣果使士氣升檔到 A

# [練級參考](http://xycq.online/forum/viewthread.php?tid=210792&extra=page%3D1%26amp%3Bfilter%3Ddigest)