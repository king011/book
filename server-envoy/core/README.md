# [config.core.v3.Address](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-address)

指定一個邏輯或物理地址，用於告訴 Envoy 在哪裏 bind/listen， connect 上游，查找管理服務器

```typescript
interface Address {
		socket_address?: SocketAddress
		pipe?: Pipe
		envoy_internal_address?: EnvoyInternalAddress
}
```

必須設置 **socket\_address**, **pipe**, **envoy\_internal\_address** 中的一個

## socket\_address

([config.core.v3.SocketAddress](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-socketaddress))

```
interface SocketAddress {
		/**
		 * @defaultValue 'TCP'
		 * 
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-enum-config-core-v3-socketaddress-protocol}
		 */
		protocol?: 'TCP' | 'UDP'
		/**
		 * listener 監聽地址，不允許空字符串可以使用 0.0.0.0 或 :: 指代任意地址
		 * 
		 * @remarks
		 * 
		 * 當在上游 BindConfig 中使用時，地址控制出站連接的源地址。 
		 * 對於集群，集群類型決定地址必須是 IP（STATIC 或 EDS 集群）還是由 DNS 解析的主機名（STRICT_DNS 或 LOGICAL_DNS 集群）。
		 * 可以通過 resolver_name 自定義地址解析。
		 */
		address: string
		/**
		 * uint32 的監聽端口號
		 * 
		 * port_value/named_port 必須設置其中一個值
		 */
		port_value?: number
		/**
		 * 這僅在下面指定了 resolver_name 且命名解析器能夠進行命名端口解析時才有效
		 * 
		 * port_value/named_port 必須設置其中一個值
		 */
		named_port?: string
		/**
		 * 自定義解析器的名稱，這必須已在 Envoy 註冊
		 * 
		 * @remarks
		 * 如果為空，則應用上下文相關的默認值。 
		 * 如果該地址是具體的 IP 地址，則不會發生任何解析。 
		 * 如果 address 是主機名，則應將其設置為解析而不是 DNS。 
		 * 使用 STRICT_DNS 或 LOGICAL_DNS 指定自定義解析器將在運行時產生錯誤。
		 */
		resolver_name?: string
		/**
		 * 當綁定到上面的 IPv6 地址時，這會啟用 IPv4 兼容性
		 * 
		 * @remarks
		 * 綁定到 :: 將允許 IPv4 和 IPv6 連接，將對等 IPv4 地址映射到 IPv6 空間，如 ::FFFF:<IPv4-address>。
		 */
		ipv4_compat?: boolean
}
```

## pipe

([config.core.v3.Pipe](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-pipe))

```typescript
interface Pipe {
	/**
	 * Unix Domain Socket path
	 * 
	 * @remarks
	 * 在 Linux 上，以 "@" 開頭的路徑將使用抽象命名空間。 
	 * Envoy 將開頭的 "@" 替換為空字節。 
	 * 以 "@" 開頭的路徑將導致在 Linux 以外的環境中出錯。
	 */
	path: string
	/**
	 * uuint32 的管道的模式。 不適用於抽象套接字。
	 */
	mode?: number
}
```

## envoy\_internal\_address

([config.core.v3.EnvoyInternalAddress](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/address.proto#envoy-v3-api-msg-config-core-v3-envoyinternaladdress)) 指定由內部偵聽器處理的用戶空間地址

```
interface EnvoyInternalAddress {
		/**
		 * 指定內部偵聽器的名稱
		 */
		server_listener_name: string
		/**
		 * 指定端點標識符以區分單個上游池中同一內部偵聽器的多個端點。 
		 * 僅在上游地址中用於跟踪對各個端點的更改。 
		 * 例如，這可以設置為目標內部偵聽器的最終目標 IP。
		 */
		endpoint_id?: string
}
```

# [config.core.v3.ConfigSource](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#config-core-v3-configsource)

listeners, clusters, routes, endpoints 的動態配置可以來自檔案系統(使用 inotify 進行監視檔案以更新) 或者  xDS API 源。

```typescript
interface ConfigSource {
		/**
		 * @deprecated use {@link path_config_source} instead 
		 */
		path?: string
		/**
		 * 本地文件系統路徑配置源
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-pathconfigsource config.core.v3.PathConfigSource}
		 */
		path_config_source?: PathConfigSource
		/**
		 * API 配置源
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-apiconfigsource config.core.v3.ApiConfigSource}
		 */
		api_config_source?: ApiConfigSource
		/**
		 * 設置後，ADS 將用於獲取資源。 使用引導程序配置中的 ADS API 配置源。
		 *  {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-aggregatedconfigsource config.core.v3.AggregatedConfigSource}
		 */
		ads?: AggregatedConfigSource

		/**
		 * 資源超時時間
		 * 
		 * @defaultValue 15000
		 * @remarks
		 * 指定此超時後，Envoy 將在初始化過程中等待此 xDS 訂閱上的第一個配置響應的指定時間不超過指定時間。
		 *  達到超時後，Envoy 將進入下一個初始化階段，即使第一個配置尚未交付。
		 *  計時器在 xDS API 訂閱開始時激活，並在第一次配置更新或出錯時解除。 
		 * 0 表示沒有超時 - Envoy 將無限期地等待第一個 xDS 配置（除非另一個超時適用）。 默認為 15 秒
		 * {@link https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration Duration}
		 */
		initial_fetch_timeout?: number
		/**
		 * xDS 資源的 API 版本。 
		 * 
		 * @defaultValue AUTO
		 * @remarks
		 * 這意味著客戶端將請求資源的類型 URL 以及客戶端將依次期望交付的資源類型。
		 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-enum-config-core-v3-apiversion config.core.v3.ApiVersion}
		 */
		resource_api_version?: ApiVersion
}
```

## path\_config\_source

([config.core.v3.PathConfigSource](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/config_source.proto#envoy-v3-api-msg-config-core-v3-pathconfigsource))

```typescript
interface PathConfigSource {
	/**
	 * 檔案系統上的路徑以獲取和監視配置更新。 為 secret 獲取配置時，還會監視證書和密鑰文件的更新。
	 */
	path: string
	/**
	 * 如果已配置，將監視此目錄的移動。 當移動到此目錄中的條目時，將重新加載路徑。 這在某些部署場景中是必需的。
	 * 
	 * @remarks
	 * 具體來說，如果嘗試使用 Kubernetes ConfigMap 加載 xDS 資源，則可能會使用以下配置： 
	 * 1. 將 xds.yaml 存儲在 ConfigMap 中。 
	 * 2. 將 ConfigMap 掛載到 /config_map/xds 
	 * 3. 配置 path 爲 /config_map/xds/xds.yaml 
	 * 4. 配置 watched_directory 爲 /config_map/xds
	 * 上述配置將確保 Envoy 監視擁有目錄以進行移動，這是由於 Kubernetes 在原子更新期間如何管理 ConfigMap 符號鏈接所必需
	 * {@link https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-watcheddirectory config.core.v3.WatchedDirectory}
	 */
	watched_directory?: WatchedDirectory
}
interface WatchedDirectory {
	/**
	 * 要監聽的檔案夾路徑
	 */
	path: string
}
```

# [config.core.v3.DataSource](https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/core/v3/base.proto#envoy-v3-api-msg-config-core-v3-datasource)

```typescript
interface DataSource {
	/**
	 * 本地文件系統數據源
	 */
	filename?: string
	/**
	 *  內聯的 bytes 數據源
	 */
	inline_bytes?: any
	/**
	 * 內聯字符串
	 */
	inline_string?: string
	/**
	 * 環境變量數據源
	 */
	environment_variable?: string
}
```