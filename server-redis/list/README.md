# 列表

* redis 列表是簡單的字符串 安裝插入順序排列
* 可以添加一個元素到列表頭部或尾部
* 一個列表最多可以包含 pow(2,32-1) 個元素(4294967295,超過40億)

| 命令 1 | 含義 |
| -------- | -------- |
| BLPOP key1 [key2 ] timeout     | pop front 並返回 pop 的值 如果沒有元素 一直等待直到超時     |
| BRPOP key1 [key2 ] timeout     | pop back 並返回 pop 的值 如果沒有元素 一直等待直到超時     |
| BRPOPLPUSH source destination timeout     | 從列表中彈出一個值，將彈出的元素插入到另外一個列表並返回。如果列表沒有元素 一直等待直到超時     |
| LINDEX key index     | 通過索引獲取列表元素     |
| LINSERT key BEFORE|AFTER pivot value     | 在列表元素前或後插入元素     |
| LLEN key     | 獲取列表長度     |
| LPOP key     | pop front 並返回 pop 的值      |
| LPUSH key value1 [value2]     | 將值 插入到列表頭部     |
| LPUSHX key value     | 將值插入到已經存在的列表頭部     |
| LRANGE key start stop     | 獲取列表指定範圍內的元素     |
| LREM key count value     | 移除列表元素     |
| LSET key index value     | 通過索引設置列表元素值     |
| LTRIM key start stop     | 將列表切取爲子區間     |
| RPOP key     | pop back 並返回 pop 的值     |
| RPOPLPUSH source destination     | 移除列表最後一個元素 並將該元素添加到另一個列表並返回     |
| RPUSH key value1 [value2]     | 在列表中添加值     |
| RPUSHX key value     | 爲已經存在的列表添加值     |
