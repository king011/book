# 編譯

只提供了 makefile 進行編譯，但你可以指定一些 宏 來確定要編譯的內容

```bash
# 編譯靜態庫
make CFLAGS="-DLTC_NO_TEST"
```

```bash
# 編譯動態庫
make -f makefile.shared
```

```bash
# 交叉編譯
make CC=arm-linux-gnueabihf-gcc
```

# headers

**src/headers** 中包含了所有頭文件請加入 include 搜索路徑中

```
#include <tomcrypt.h>
```
