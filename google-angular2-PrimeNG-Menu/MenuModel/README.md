# [MenuModel](https://primeng.org/menumodel)

PrimeNG 菜單組件共享一個公共的 API 來指定菜單項和子菜單

# MenuItem

api 的核心是 class **MenuItem**，它定義了各種選項，例如菜單中項目的 標籤 圖標 和 子項。下面的示例是帶有菜單組件的示例配置

```
import {MenuModule} from 'primeng/menu';
import {MenuItem} from 'primeng/api';
import {MegaMenuItem} from 'primeng/api';  //required when using MegaMenu
```

```
<p-menu [model]="items"></p-menu>
```

```
export class MenuDemo {

    items: MenuItem[];

    ngOnInit() {
        this.items = [{
            label: 'File',
            items: [
                {label: 'New', icon: 'pi pi-plus'},
                {label: 'Open', icon: 'pi pi-download'}
            ]
        },
        {
            label: 'Edit',
            items: [
                {label: 'Undo', icon: 'pi pi-refresh'},
                {label: 'Redo', icon: 'pi pi-repeat'}
            ]
        }];
    }
}
```

# MegaMenu

除了 MegaMenu 之外的所有菜單組件都使用 MenuItem[] 作爲模型，在 MegaMenu 根級項目的情況下應該是 MegaMenuItem[] 而嵌套的子菜單可以是 MenuItem[]

# Command
單擊菜單時會調用 command 指定的函數

```
export class MenuDemo {

    private items: MenuItem[];

    ngOnInit() {
        this.items = [{
            label: 'File',
            items: [
                {label: 'New', icon: 'pi pi-plus', command: (event) => {
                    //event.originalEvent: Browser event
                    //event.item: menuitem metadata
                }}
            ]
        }
    }
}
```
# Navigation

導航是使用外部鏈接的 **url** 屬性和內部鏈接的 **routerLink** 指定的。如果一個菜單項有一個活動的路由 class **p-menuitem-link-active** 會被指定。可以使用 MenuItem API 的 **routerLinkActiveOptions** 屬性配置活動路由鏈接

```
export class MenuDemo {

    private items: MenuItem[];

    ngOnInit() {
        this.items = [{
            label: 'File',
            items: [
                {label: 'New', icon: 'pi pi-plus', url: 'http://www.primefaces.org/primeng'},
                {label: 'Open', icon: 'pi pi-download', routerLink: ['/pagename']}
                {label: 'Recent Files', icon: 'pi pi-download', routerLink: ['/pagename'], queryParams: {'recent': 'true'}}
            ]
        }
    }
}
```

# Custom Content
菜單項的 label 也支 html。默認情況下，html 被轉義，設置 **escape** 爲 false 則不會轉義

```
export class MenuDemo {

    private items: MenuItem[];

    ngOnInit() {
        this.items = [{
            label: '<h2>File</h2>',
            escape: false,
            //...
        }
    }
}
```

# Properties of MenuItem

MenuItem 提供了如下屬性。但請注意，並非所有這些屬性都被菜單組件支持

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| id     | string     | null     | 元素唯一標識     |
| label     | string     | null     | 菜單項文本     |
| icon     | string     | null     | 菜單項圖標     |
| iconStyle     | object     | null     | 圖標的內聯 css style     |
| command     | function     | null     | 單擊回調函數     |
| url     | string     | null     | 導航到外部鏈接     |
| routerLink     | array     | null     | 使用 RouterLink 定義的內部鏈接     |
| routerLinkActiveOptions     | object     | null     | 配置活動路由     |
| queryParams     | object     | null     | 內部鏈接的查詢參數     |
| fragment     | string     | null     | 爲 URL 設置 fragment     |
| queryParamsHandling     | QueryParamsHandling     | null     | 如何處理路由鏈接中的查詢參數:<br> merge : 將新參數與當前參數合併<br> preserve:保留當前參數     |
| preserveFragment     | boolean     | false     | 如果爲true，下次導航保留 fragment     |
| skipLocationChange     | boolean     | null     | 如果爲true，導航不會將新狀態推入歷史記錄     |
| replaceUrl     | boolean     | null     | 如果爲true，導航同時替換歷史中的當前狀態     |
| state     | object     | null     | 可以傳遞給任何導航的開發人員自定義狀態     |
| items     | array     | null     | 子菜單項     |
| expanded     | boolean     | false     | 是否顯示子菜單     |
| disabled     | boolean     | false     | 如果爲true，禁用菜單項     |
| visible     | boolean     | true     | 菜單項的 dom 元素是否創建     |
| target     | string     | null     | 指定打開鏈接文檔的位置     |
| escape     | boolean     | true     | 是否轉義 label 的內容     |
| separator     | boolean     | false     | 將項目定義爲分隔符     |
| style     | object     | null     | 菜單項的內聯 css style     |
| styleClass     | string     | null     | 菜單項的 css class     |
| badge     | string     | null     | 一個小徽章     |
| badgeStyleClass     | string     | null     | 徽章的 css class     |
| title     | string     | null     | 菜單項的 Tooltip 文本     |
| automationId     | any     | null     | HTML data-\* 屬性的值     |
| tabindex     | string     | 0     | 指定項目的 Tab 鍵順序     |
| tooltipOptions     | TooltipOptions     | -     | 項目的 Tooltip 選項     |



