# [常用節點組件接口](http://docs.cocos.com/creator/manual/zh/scripting/basic-node-api.html)

[cc.Node](http://docs.cocos.com/creator/api/zh/classes/Node.html) 和 [cc.Component](http://docs.cocos.com/creator/api/zh/classes/Component.html) 定義了 Node 和 Component 的基本行爲

# 節點狀態 和 層級操作

在 Component 中 提供 this.node 可以訪問當前 Component 所掛接的 Node

## 激活關閉節點

設置 this.node.active = true/false 可以 激活或關閉 當前節點 

也可以在 creator上 的屬性檢查器中 勾選 是否 激活節點

## 更改父節點

更改父節點 可以使用 下面兩個 完全 等價的 寫法
```
this.node.parent = parentNode
```

```
this.node.removeFromParent(false);
parentNode.addChild(this.node);
```

> removeFromParent 傳入 true 會 清空 節點上 綁定的 事件 和 action ...

## 訪問子節點

* this.node.children 返回子節點
* this.node.childrenCount 返回中節點 數量

> 上述api只會返回 直接子節點 不會返回其它子孫節點
> 

# 變換節點

cocos 提供了豐富的 節點變換 位置 旋轉 縮放 尺寸

## 更改節點位置

以下寫法 等價

```
this.node.x = 100
this.node.y = 50
```

```
this.node.setPosition(100, 50)
this.node.setPosition(cc.v2(100, 50))
```

```
this.node.position = cc.v2(100, 50)
```

## 旋轉節點

以下寫法 等價

```
this.node.rotation = 90
```

```
this.node.setRotation(90)
```

## 縮放節點

以下寫法 等價

```
this.node.scaleX = 2
this.node.scaleY = 2
```

```
this.node.setScale(2) // scaleX
this.node.setScale(2, 2)
```

## 更改尺寸

以下寫法 等價

```
this.node.setContentSize(100, 100)
this.node.setContentSize(cc.size(100, 100))
```

```
this.node.width = 100
this.node.height = 100
```

## 更改錨點

```
this.node.anchorX = 1
this.node.anchorY = 0
```

```
this.node.setAnchorPoint(1, 0)
```

# 顏色和不透明度

```
this.node.color = cc.Color.RED
```

```
this.node.opacity = 128
```

# 常用組件接口 

cc.Component 是所有組件的基類

* this.node 返回組件掛接的節點
* this.enabled 是否每幀執行該組件的 update 方法 ,同時也控制渲染組件是否顯示
* update(dt) 組件成員方法 如果 this.enabled 爲 true 則每幀 都會執行此方法
* onLoad() 組件所在節點 初始化時(節點加載到節點樹時) 執行
* start() 會在組件第一次 update 之前執行 通常用於 需要在 所有組件 onLoad 初始化完畢後 執行的 邏輯