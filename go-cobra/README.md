# cobra
cobra 是一個 開源(Apache-2.0) 強大的 命令解析工具(支持 子命令)

docker 等項目 亦在使用 此項目 解析 命令

源碼 https://github.com/spf13/cobra

```go
go get -u github.com/spf13/cobra
```

# Example
```go
package main
 
import (
	"fmt"
	"github.com/spf13/cobra"
)
 
func main() {
	//創建 root 命令
	var name string
	var rootCmd = &cobra.Command{
		//root 命令名
		Use: "app",
		//簡短的 命令 說明
		Short: "測試 cobra",
		//詳細的 命令 說明
		Long: `孤 測試 cobra 庫用法的 試驗 代碼
	by king
	http://github.com/spf13/cobra`,
		//匹配到 命令時執行的 代碼
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("this is run by", name)
		},
	}
	//參數 解析 類似 標準庫的 flag
	//flags := rootCmd.PersistentFlags()
	flags := rootCmd.Flags() // 和PersistentFlags 的區別在於 PersistentFlags 為全局的 參數 使用子命令也可以使用
	flags.StringVarP(&name,
		"name", //和 標準庫 StringVar 區別在於 此參數 指定 長命令 --xxx
		"n",    //和 標準庫 StringVar 區別在於 此參數 指定 短命令 -x
		"king", "who run")
 
	//添加子命令
	rootCmd.AddCommand(&cobra.Command{
		//子命令 名稱
		Use: "debug",
		//為 命令 設置可選的 別名 (別名 可以是一個 任意長度的 字符串 數組)
		Aliases: []string{"d0", "d1"},
		Short:   "Short Debug",
		Long:    "Long Debug",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("this is debug")
		},
	})
 
	//路由 並執行 命令
	fmt.Println(rootCmd.Execute())
}
```