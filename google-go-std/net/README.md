# [net](https://pkg.go.dev/net)

net 包 提供了 網路基礎庫

# ip

net 包中包含了一些處理 ip 的實用工具

## IPMask

IPMask 是一個 bitmask，可用於操作 IP 地址 進行尋址與路由

```
type IPMask []byte

// 返回一個 ipv4 的掩碼 a.b.c.d
func IPv4Mask(a, b, c, d byte) IPMask

// 返回一個由二進制 全1 後緊跟 全0 的 掩碼，ones 指定 1 的數量，bits 指定 bit 長度，如果參數無效返回 nil
func CIDRMask(ones, bits int) IPMask

// CIDRMask 的逆向行爲，返回掩碼中前導有多少個1 以及 總的 bits，如果不是規範模式(由全1的前綴和全0的後綴組成,全1或全0 都是規範的) 返回 [0,0]
func (m IPMask) Size() (ones, bits int)

// 返回 16進制 的掩碼 字符串
func (m IPMask) String() string
```

## IP

IP 表示了一個 ipv4/ipv6 地址

```
type IP []byte

// 返回一個 ipv4 地址 a.b.c.d
func IPv4(a, b, c, d byte) IP

// 解析 ip 字符串，可以是 ipv4/ipv6
func ParseIP(s string) IP

// 查詢 host 的 ip
func LookupIP(host string) ([]IP, error)

// 返回 默認掩碼，只有 ipv4有默認掩碼，對於 ipv6 將始終返回 nil
func (ip IP) DefaultMask() IPMask

// 返回 ip 是否相等，ipv4 和 ipv6 的相同地址被認爲是相等的
func (ip IP) Equal(x IP) bool

// 返回是否是全球單播地址
//
// 全球单播地址的标识使用 RFC 1122、RFC 4632 和 RFC 4291 中定义的地址类型标识，但 IPv4 定向广播地址除外。 
// 即使 ip 在 IPv4 私有地址空间或本地 IPv6 单播地址空间中，它也会返回 true。
func (ip IP) IsGlobalUnicast() bool

// 返回地址是否是 interface-local 多播地址
func (ip IP) IsInterfaceLocalMulticast() bool

// 返回地址是否是 link-local 多播地址
func (ip IP) IsLinkLocalMulticast() bool

// 返回地址是否是 link-local 單播地址
func (ip IP) IsLinkLocalUnicast() bool

// 返回地址是否是環回地址
func (ip IP) IsLoopback() bool

// 返回地址是否是多播地址
func (ip IP) IsMulticast() bool

// 返回地址是否是私有地址
func (ip IP) IsPrivate() bool

// 返回 ip 是否爲未指定地址
// v4 "0.0.0.0"
// v6 "::"
func (ip IP) IsUnspecified() bool

// 返回 ip 字符串
func (ip IP) String() string

// 返回 與 mask 計算後的 ip
func (ip IP) Mask(mask IPMask) IP

// 轉換到 16 byte 的表示形式
func (ip IP) To16() IP
// 轉換到 4 byte 的表示形式，如果 ip 不是 v4 地址返回 nil
func (ip IP) To4() IP

func (ip IP) MarshalText() ([]byte, error)
func (ip *IP) UnmarshalText(text []byte) error
```

## IPNet

IPNet 代表了一個 ip 網路 IP+IPMask

```
type IPNet struct {
	IP   IP     // network number
	Mask IPMask // network mask
}

// 返回 ip 是否在被包含在此網路中
func (n *IPNet) Contains(ip IP) bool

// 返回網路名稱，固定字符串 "ip+net"
func (n *IPNet) Network() string

// 返回 CIDR 表示法的字符串 例如 "192.0.2.0/24" 或 "2001:db8::/48"
// 如果掩碼不是規範形式則返回 ip/mask.String() "198.51.100.0/c000ff00"
func (n *IPNet) String() string
```

## ParseCIDR

ParseCIDR 將解析 CIDR 表示的 ip 與掩碼例如 "192.0.2.1/24" 將返回 ip "192.168.2.1" 以及掩碼 "192.0.2.0/24"

```
func ParseCIDR(s string) (IP, *IPNet, error)
```

## JoinHostPort SplitHostPort

* JoinHostPort 將 host port 組成成 **"host:port"** 的形式，如果 host 中包含 **:** (ipv6) 則將其組合成爲 **"\[host\]:port"** 形式
* SplitHostPort 將 "host:port" "host%zone:port" "[host]:port" "[host%zone]:port" 拆分爲 **"host" "port"** 或 **"host%zone" "port"**
```
func JoinHostPort(host, port string) string
func SplitHostPort(hostport string) (host, port string, err error)
```
# udp

```
package main

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"sync"
)

func main() {
	var wait sync.WaitGroup
	wait.Add(2)
	ok := make(chan struct{})
	go func() {
		server(ok)
		wait.Done()
	}()
	<-ok
	go func() {
		client()
		wait.Done()
	}()
	wait.Wait()
}
func server(ok chan struct{}) {
	addr, e := net.ResolveUDPAddr("udp", ":8964")
	if e != nil {
		log.Fatalln(e)
	}
	// 監聽 udp 端口
	l, e := net.ListenUDP("udp", addr)
	if e != nil {
		log.Fatalln(e)
	}
	log.Println("server work :", addr)
	defer l.Close()
	close(ok)

	data := make([]byte, 1024)
	var remoteAddr *net.UDPAddr
	var n int
	for {
		// 接收數據
		n, remoteAddr, e = l.ReadFromUDP(data)
		if e != nil {
			log.Println(e)
			continue
		}
		fmt.Printf("[s] %v <- %v %s\n", addr, remoteAddr, data[:n])
		// 發送數據
		_, e = l.WriteToUDP(data[:n], remoteAddr)
		if e != nil {
			log.Println(e)
			continue
		}
		l.Write(data[:n])
		if bytes.Equal(data[:n], []byte(`quit`)) {
			break
		}
	}
}
func client() {
	addr, e := net.ResolveUDPAddr("udp", "127.0.0.1:8964")
	if e != nil {
		log.Fatalln(e)
	}
	// udp 不會建立連接 故無論服務器是否 監聽 都會返回成功
	c, e := net.DialUDP("udp", nil, addr)
	if e != nil {
		log.Fatalln(e)
	}
	ok := make(chan struct{})
	go read(c, ok)
	_, e = c.Write([]byte(`first data`))
	if e != nil {
		log.Fatalln(e)
	}
	_, e = c.Write([]byte(`quit`))
	if e != nil {
		log.Fatalln(e)
	}
	<-ok
}
func read(c *net.UDPConn, ok chan struct{}) {
	remoteAddr := c.RemoteAddr().String()
	localAddr := c.LocalAddr().String()

	data := make([]byte, 1024)
	var e error
	var n int
	for {
		n, e = c.Read(data)
		if e != nil {
			break
		}
		fmt.Printf("[c] %v <- %v %s\n", localAddr, remoteAddr, data[:n])
		if bytes.Equal(data[:n], []byte(`quit`)) {
			break
		}
	}
	c.Close()
	close(ok)
}
```