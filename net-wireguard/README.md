# WireGuard

WireGuard 是一個開源(GPL2)的 VPN 程式及協議，目的是提供比 IPsec 和 OpenVPN 更好的效能。2020年3月它被納入 Linux5.6 核心

WireGuard 僅使用 UDP 這是因爲 TCP-over-TCP 存在潛在的缺點。默認端口爲 UDP 51820

* 官網 [https://www.wireguard.com/](https://www.wireguard.com/)
* 源碼 [https://git.zx2c4.com/wireguard-linux](https://git.zx2c4.com/wireguard-linux)


# MTU 開銷

WireGuard 的開銷細分如下:

* 20 位元組的 IPv4 報頭或 40 位元組的 IPv6 報頭
* 8 位元組的 UDP 報頭
* 4 位元組的類型
* 4 位元組的金鑰索引
* 8 位元組的亂數
* N 位元組的加密資料
* 16 位元組的身分驗證標籤

# MTU 操作注意事項

假設傳輸 WireGuard 封包的底層網路保持 1500 位元組的 MTU，則將所有相關對等方的 WireGuard 介面組態為 1420 位元組的 MTU 是傳輸 IPv6 + IPv4 流量的理想選擇。但是，當僅承載傳統的 IPv4 流量時，WireGuard 介面的 MTU 可以設定為更高的 1440 位元組。

從操作角度和網路組態一致性來看，選擇為 WireGuard 介面組態 1420 MTU 是有利的。這種方法確保了一致性，並促進了將來為 WireGuard 對等方和介面啟用 IPv6 的更平穩過渡。

> 在某些情況下，例如，一個對等方位於 MTU 為 1500 位元組的網路之後，而另一個對等方位於LTE等無線網路之後，運營商通常會選擇遠低於 1420 位元組的 MTU。在這種情況下，主機的底層 IP 網路堆疊將對 UDP 封裝的封包進行分片並行送，但隧道內的包將保持一致，並且不需要分片，因為PMTUD將檢測對等方之間的 MTU（在本例中為 1420 位元組）並在對等方之間傳送固定大小的封包