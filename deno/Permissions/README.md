# [權限](https://deno.land/manual@v1.28.1/basics/permissions)
默認情況下,deno 需要明確指定才能訪問敏感資源

如果下面例子沒有目前指定 --allow-read 腳本將無法訪問檔案系統

```
deno run --allow-read mod.ts
```

# 權限列表

* **--allow-env=&lt;allow\-env&gt;** 運行設置和獲取環境變量,從 deno 1.9 開始可以指定一個可選的以 **,** 分隔的環境變量列表,以提供允許訪問的環境變量名稱列表
* **--allow-sys=&lt;allow\-sys&gt;&nbsp;** 運行訪問提供有關用戶操作系統信息的 API,例如 Deno.osRelease() 和 Deno.systemMemoryInfo()
* **--allow-hrtime** 允許高分辨率時間測量。高分辨率時間測量可用於定時攻擊和指紋識別
* **--allow-net=&lt;allow\-net&gt;** 允許訪問網路。你可以指定一個可選的以 **,** 分隔的 IP 地址或主機名列表(可選的帶有端口)，以提供允許訪問的網路地址列表
* **--allow-ffi** 允許加載動態庫。請注意，動態庫不在沙箱中運行，因此沒有與 deno 進程相同的安全限制。因此請謹慎使用，此外這是一個不穩定的特性
* **--allow-read=&lt;allow\-read&gt;** 允許讀取檔案系統。你可以指定一個可選的以 **,** 分隔的目錄或檔案列表，以提供允許訪問的檔案列表
* **--allow-run=&lt;allow\-run&gt;** 允許運行子進程。從 deno 1.9 開始，你可以指定一個可選的以 **,** 分隔的子進程列表，以提供允許的子進程列表。請注意，子進程不在沙箱中運行，因此沒有與 deno 進程相同的安全限制，請謹慎使用
* **--allow-write=&lt;allow\-write&gt;** 允許寫入檔案系統。你可以指定一個可選的以 **,** 分隔的目錄或檔案列表，以提供允許寫入的檔案列表
* **-A, --allow-all** 允許所有訪問，這將開放所有權限，請謹慎使用