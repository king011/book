# [UDP Receive Buffer Size](https://github.com/quic-go/quic-go/wiki/UDP-Receive-Buffer-Size)

```
echo 'net.core.rmem_max=2500000' >> /etc/sysctl.conf
sysctl --system
```