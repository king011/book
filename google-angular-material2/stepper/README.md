# MatStepperModule
MatStepperModule 提供了 一個 步進的 操作 分組
```ts
import {MatStepperModule} from '@angular/material/stepper';
```

1. 使用 mat-horizontal-stepper/mat-vertical-stepper 定義水平/垂直步進容器
2. 使用 mat-step 定義分組 label屬性指定 分組標題

```html
<mat-horizontal-stepper>
    <mat-step label="個人資訊">
        <h4>提供個人資訊</h4>
    </mat-step>
    <mat-step label="詳細問題">
        <h4>主要的問題內容</h4>
    </mat-step>
    <mat-step label="其他">
        <h4>其他問題</h4>
    </mat-step>
</mat-horizontal-stepper>
```
![](assets/01-mat-horizontal-stepper.gif)

```html
<mat-vertical-stepper>
    <mat-step label="個人資訊">
        <h4>提供個人資訊</h4>
    </mat-step>
    <mat-step label="詳細問題">
        <h4>主要的問題內容</h4>
    </mat-step>
    <mat-step label="其他">
        <h4>其他問題</h4>
    </mat-step>
</mat-vertical-stepper>
```
![](assets/02-mat-vertical-stepper.gif)

# matStepLabel
使用 matStepLabel 而非 label屬性 可以 設置複雜的 分組標題

```html
<mat-vertical-stepper>
  <mat-step>
    <ng-template matStepLabel>
      <u>個人資訊</u>
    </ng-template>
    <h4>提供個人資訊</h4>
  </mat-step>
  <mat-step>
    <ng-template matStepLabel>
      <em>詳細問題</em>
    </ng-template>
    <h4>主要的問題內容</h4>
  </mat-step>
  <mat-step label="其他">
    <h4>其他問題</h4>
  </mat-step>
</mat-vertical-stepper>
```

# 上一步/下一步
使用 matStepperNext/matStepperPrevious 指令 可以 提供一個 上一步/下一步 功能按鈕

```html
<mat-vertical-stepper>
  <mat-step>
    <ng-template matStepLabel>
      <u>個人資訊</u>
    </ng-template>
    <h4>提供個人資訊</h4>
    <button mat-button matStepperNext>前進到「詳細問題」</button>
  </mat-step>
  <mat-step>
    <ng-template matStepLabel>
      <em>詳細問題</em>
    </ng-template>
    <h4>主要的問題內容</h4>
    <button mat-button matStepperPrevious>回到「個人資訊」</button>
    <button mat-button matStepperNext>前進到「其他」</button>
  </mat-step>
  <mat-step label="其他">
    <h4>其他問題</h4>
    <button mat-button matStepperPrevious>回到「詳細問題」</button>
  </mat-step>
</mat-vertical-stepper>
```
![](assets/04-mat-step-button.gif)

# Linear