# [多配置檔案](https://docs.mamedev.org/advanced/multiconfig.html)

mame 有一個非常強大的配置系統，可以讓你爲每個遊戲 每個系統 甚至每個顯示器進行不同的配置調整，但需要仔細考慮好如何安排你的配置

# 配置優先順序

1. 通過命令行解析的參數將優先於任何 INI 檔案中的內容
2. **mame.ini** 被解析兩次，第一次可能會更改各種路徑設置，因此第二次會檢查新位置是否有有效的配置檔案(如果有，則使用該檔案更改配置)
3. **debug.ini** 用於啓用調試器。這是一個高級配置檔案，大多數人不想要使用和關心它
4. 屏幕方向 INI 檔案 (horizont.ini/vertical.ini)。例如吃豆人是豎屏的所以它會加載 **vertical.ini**。而街頭霸王 Alpha 是橫屏的，所以它會加載 **horizont.ini**。

	沒有顯示器 多個方向不同的顯示器或連接到插槽設備的顯示器系統通常會加載 **horizont.ini**
	
5. System type INI 檔案(arcade.ini console.ini computer.ini othersys.ini)。Pac-Man 和 Street Fighter Alpha 都是街機遊戲，所以這裡會加載 **arcade.ini**，而 Atari 2600 會加載 **console.ini**，因為它是家用遊戲機
6. Monitor type INI (vector.ini 用於矢量顯示器，raster.init 用於 CRT 光柵顯示器， lcd.ini 用於 LCD/EL/等離子矩陣顯示器)。Pac-Man 和 Street Fighter Alpha 使用光柵 CRT，因此 raster.ini 加載到此處，而 Tempest 使用矢量顯示器，因此 vector.ini 加載到此處。

	對於具有多種顯示器類型的系統，例如帶有 CRT 光柵顯示器和 LCD 矩陣顯示器的 House Mannequi 使用與第一個顯示器相關的 INI 檔案(本例子中爲  raster.ini).沒有顯示器或帶有其它類型顯示器的系統不會爲此步驟加載 INI 檔案
	
7. Driver source file INI。mame 將嘗試加載 **source/&lt;sourcefile&gt;.ini**，其中 sourcefile 是定義系統驅動程序的源代碼檔案的基本名稱。可以在命令行使用 mame-listsource &lt;pattern&gt; 找到系統的源檔案

	例如 Banpresto's Sailor Moon, Atlus's Dodonpachi, Nihon System's Dangun Feveron 都運行在類似的硬件上，並且定義在 cave.cpp 源檔案中，所以它們都會在此加載 source/cave.ini
	
8. BIOS set INI。例如 The Last Soldier 使用 Neo-Geo MVS BIOS 所以它會加載 neogeo.ini。不使用 BIOS 設置的系統不會爲此步驟加載 INI 檔案
9. 父系統 INI。例如 The Last Soldier 是 The Last Blade / Bakumatsu Roman - Gekka no Kenshi 的克隆，所以它將加載 lastblad.ini，父系統不會爲此步驟加載 ini 檔案
10. System INI。使用前面的示例，The Last Soldier 會加載 lastsold.ini