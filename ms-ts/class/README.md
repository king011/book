# 類
* ts 提供了 類的概念 使用關鍵字 **class** 定義 
* 使用 **public protected private** 定義成員 可訪問域 \(默認爲public\)
* 使用 **extends** 派生類 \(ts 不支持從多個基類 派生\)
* 使用 **readonly** 定義 只讀屬性
* 使用 **static** 定義靜態成員

```ts
class Animal {
    // 定義 靜態只讀 屬性
    static Type: string = "Animal";

    // 定義 私有 屬性 只有 Animal 可訪問
    private name: string = ""

    // 定義 保護屬性 可被 Animal 和 子類 訪問
    protected getName(): string {
        return this.name;
    }
    // 定義 構造函數
    constructor(name: string) {
        this.name = name
    }
    // 定義 公開屬性
    Eat() {
        console.log(this.name, "eat")
    }
}
class Cat extends Animal {
    Speak() {
        console.log(this.getName(), "喵~")
    }
}
class Dog extends Animal {
    constructor(name: string) {
        super("dog " + name)
    }
    // 覆蓋父類方法
    Eat() {
        super.Eat()
        console.log("dog eat")
    }
}
// js 是基於原型的 所以 static 只在 同型別間 共用 子類 和 父類 不共用
Cat.Type = "Cat"
console.log(Animal.Type) // Animal
console.log(Cat.Type) // Cat

// 實例化子類
const cat = new Cat("Garfield")
cat.Eat()
cat.Speak()

// 實例化子類
const dog = new Dog("Odie")
dog.Eat()
```
>  ts 提供了 關鍵字 **super** 用於訪問 父類方法
>  
> 在子類 構造函數中 必須 調用 super 完成 父類 初始化  
> (如果 子類沒有定義 構造 方法 則默認使用 父類構造方法)

# 存取器
js 支持 使用 **set get** 定義存取器 ts使用 方法相同 並且 子類同樣可以繼承 
```ts
class Animal {
    private name: string = ""
    private lv: number = 0
    set Name(name: string) {
        console.log("set", name)
        this.name = name
    }
    get Name(): string {
        console.log("get", this.name)
        return this.name
    }
}
class Cat extends Animal{
    constructor(){
        super()
        this.Name = "cat"
    }
}
const cat = new Cat()
console.log("this is a", cat.Name)
```

# 抽象類
使用 abstract 定義一個 抽象類

抽象類 不能被實例化 通常用來 爲子類提供 通用的 方法屬性

可以使用 abstract 定義 抽象方法 實現此類的 子類 必須實現此方法

```ts
abstract class Animal {
    protected name: string = ""
    abstract Eat():void
    abstract Speak():void
    constructor(name:string){
        this.name = name
        this.Speak()
    }
}
class Cat extends Animal{
    constructor(){
        super("cat")
    }
    Eat():void{
        console.log(this.name,"eat")
    }
    Speak():void{
        console.log("喵~")
    }
}

const cat = new Cat()
cat.Eat()
```