# Variables

scss 支持 使用 **$** 存取變量

Variables 支持 
* Numbers (可以帶上可選的單位)
* trings
* Booleans
* null (視爲空)
* Lists
* Maps 

```
#info="scss"
// 定義基本變量
$background: yellow;

// 定義 一個 list
$standard-border: 4px solid black;

// 定義 map
$theme: (
    color: red, 
    fontsize: 30px,
);

.box{
    // 取得 變量
    background-color: $background;
    // 取得 list
    border: $standard-border;
    // 取得 map 
    color: map-get($theme,color);
    font-size: map-get($theme,fontsize);
}
```
```
#info="html"
<p class="box">
  cat
</p>
<p class="box">
  dog
</p>
<p class="box">
  fish
</p>
```

# Nesting

scss 允許 嵌套子元素到父元素中 來降低 父元素的 重複書寫

```
#info="scss"
.parent {
  color: blue;
  .child {
    font-size: 12px;
  }
}
```
```
#info="轉譯後的css"
.parent {
  color: blue;
}

.parent .child {
    font-size: 12px;
}
```

```
#info="scss"
.parent {
  font : {
    family: Roboto, sans-serif;
    size: 12px;
    decoration: none;
  }
}
```
```
#info="轉譯後的css"
.parent {
  font-family: Roboto, sans-serif;
  font-size: 12px;
  font-decoration: none;
}
```

# Mixins

降低 pseudo 元素的 重複性 如：::before、::after、:hover，在 Sass 中使用 & 代表父元素

```
#info="scss"
.notecard{ 
  &:hover{
      @include transform (rotatey(-180deg));  
    }
  }
```
```
info="css"
.notecard:hover {
  transform: rotatey(-180deg);
}
```


重用群組的 CSS 例如跨瀏覽器的 prefix 使用 @include 加入群組
```
#info="scss"
@mixin backface-visibility {
  backface-visibility: hidden;
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility: hidden;
  -ms-backface-visibility: hidden;
  -o-backface-visibility: hidden;
}
.notecard {
  .front, .back {
    width: 100%;
    height: 100%;
    position: absolute;

    @include backface_visibility;
  }
}
```

```
#info="css"
.notecard .front, .notecard .back {
  width: 100%;
  height: 100%;
  position: absolute;

  backface-visibility: hidden;
  -webkit-backface-visibility: hidden; 
  -moz-backface-visibility: hidden;
  -ms-backface-visibility: hidden;
  -o-backface-visibility: hidden;
}
```

@mixin 也可以透過 @include 使用參數

```
@mixin backface-visibility($visibility:hidden) { //Add an argument
  backface-visibility: $visibility;
  -webkit-backface-visibility: $visibility;
  -moz-backface-visibility: $visibility;
  -ms-backface-visibility: $visibility;
  -o-backface-visibility: $visibility;
}

.front, .back {
    @include backface-visibility(hidden);
}
```
```
#info="使用 Lists 作爲參數"
@mixin stripes($direction, $width-percent, $stripe-color, $stripe-background: #FFF) {
  background: repeating-linear-gradient(
    $direction,
    $stripe-background,
    $stripe-background ($width-percent - 1),
    $stripe-color 1%,
    $stripe-background $width-percent
  );
}
```
```
#info="使用 Maps"
$college-ruled-style: ( 
    direction: to bottom,
    width-percent: 15%,
    stripe-color: blue,
    stripe-background: white
);

.definition {
      width: 100%;
      height: 100%;
			// 使用 ... 傳遞
      @include stripes($college-ruled-style...);
 }
```

scss 也允許使用 字符串 作爲 參數
```
#info="scss"
// 使用 #{$file} 接收
@mixin photo-content($file) {
  content: url(#{$file}.jpg); //string interpolation
  object-fit: cover;
}

.photo { 
  @include photo-content('titanosaur');
  width: 60%;
  margin: 0px auto; 
}
```

```
#info="css"
.photo { 
  content: url(titanosaur.jpg);
  width: 60%;
  margin: 0px auto; 
}
```

scss 允許 Nesting 和 Mixins 一起使用
```
@mixin hover-color($color) {
  		&:hover {
   		color: $color;
   	}
}

.word {
	@include hover-color(red);
}
```

# Functions

scss 內建了 一些 [Functions](http://sass-lang.com/documentation/Sass/Script/Functions.html) 可以簡單設定色相、漸層，例如：adjust-hue($color, $degrees)、fade-out

```
$lagoon-blue: fade-out(#62fdca, 0.5);
```

# Operations

scss 允許 透過加減乘除和取餘數等運算子可以方便運算所需的屬性值

```
$color: #010203 + #040506;
/*
	01 + 04 = 05
	02 + 05 = 07
	03 + 06 = 09
	color: #050709;
*/
```

```
width: $variable/6; //有效
line-height: (600px)/9; //有效
margin-left: 20-10 px/ 2; //有效
font-size: 10px/8px; //無效 錯誤寫法
```

也可以使用 @each 語法迭代 list 內容：

```
$list: (orange, purple, teal);

@each $item in $list {
  .#{$item} {
    background: $item;
  }
}
```

使用 @for 迭代，並加入條件判斷
```
@for $i from 1 through $total {
  .ray:nth-child(#{$i}) {
	background: adjust-hue(blue, $i * $step);
	// 
	width: if($i % 2 == 0, 300px, 350px);
 		margin-left: if($i % 2 == 0, 0px, 50px);
  }
}
```

# @include 引用：可以引入其他 Sass、SCSS 檔案

我們通常使用 Partials 去處理特定功能，方便管理和維護。以下是引用 \_variables.scss 檔案範例，其中檔名前的 **\_** 表示引用前要先 compile：

```
@import "variables";
```

# @extend 共用

```
#info="scss"
.lemonade {
  border: 1px yellow;
  background-color: #fdd;
}
.strawberry {
  @extend .lemonade;
  border-color: pink;
}
```

```
#info="css"
.lemonade, .strawberry {
  border: 1px yellow;
  background-color: #fdd;
}

.strawberry {
  @extend .lemonade;
  border-color: pink;
}
```

搭配 Placeholders 使用
```
#info="scss"
a%drink {
	font-size: 2em;
	background-color: $lemon-yellow;
}

.lemonade {
	@extend %drink;
	//more rules
}
```

```
#info="css"
a.lemonade {
   	font-size: 2em;
   	background-color: $lemon-yellow;
	}

.lemonade {
 		//more rules
}
```

# @mixin 和 @extend 比較
```
#info="scss"
@mixin no-variable {
  font-size: 12px;
  color: #FFF;
  opacity: .9;
}

%placeholder {
  font-size: 12px;
  color: #FFF;
  opacity: .9;
}

span {
  @extend %placeholder;
}

div {
  @extend %placeholder;
}

p {
  @include no-variable;
}

h1 {
  @include no-variable;
}
```

```
#info="css"
span, div{
  font-size: 12px;
  color: #FFF;
  opacity: .9;
}

p {
  font-size: 12px;
  color: #FFF;
  opacity: .9;
  //rules specific to ps
}

h1 {
  font-size: 12px;
  color: #FFF;
  opacity: .9;
  //rules specific to ps
}
```