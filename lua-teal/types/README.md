# 語法

[Teal](https://github.com/teal-language/tl/blob/master/docs/tutorial.md) 是 lua 的一種方言爲 lua 提供了類型檢查

 # 型別
 Teal 中有明確的型別
 
 * any
 * nil
 * boolean
 * integer
 * number
 * string
 * thread (coroutine)

integer 是 number 的子類型，其具體精度遵循 lua vm

你還可以使用類型構造函數聲明更多類型

* arrays - {number}, {{number}}
* tuples - {number, integer, string}
* maps - {string:boolean}
* functions - function(number, string): {number}, string

最後，必須使用名稱聲明和引用某些類型

* enum
* record
	* userdata
	* arrayrecord

```
#info="example"

-- 聲明了一個枚舉，其枚舉值包含兩個字符串
local enum State
    "open"
    "closed"
end

-- 聲明了一個包含 x y 字段的 table
local record Point
    x: number
    y: number
end

    
-- 聲明實現爲 userdata
local record File
    userdata
    status: function(): State
    close: function(File): boolean, string
end
    
-- 聲明一個 table 同時包含數組部分
local record TreeNode<T> 
    {TreeNode<T>}
    item: T
end
```