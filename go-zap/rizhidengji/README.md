# 日誌等級
zap 默認 提供了 多個級別的 日誌
* debug
* info
* warn
* error
* dpanic 如果處於開發模式 DPanicLevel 日誌會 產生 panic
* panic PanicLevel 等級日誌 會在 日誌記錄後 產生panic 即使 PanicLevel 被禁用
* fatal FatalLevel 等級的日誌 會在 日誌記錄後調用 os.Exit(1) 即使 FatalLevel 被禁用

# Check
zap 提供了 Check 函數 用於 檢查 指定日誌是否被 啓用 以便 在日誌 啓用時 才 執行 相關 代碼

```txt
#info=false
func (log *Logger) Check(lvl zapcore.Level, msg string) *zapcore.CheckedEntry
```

```go
logger := zap.NewExample()
defer logger.Sync()

if ce := logger.Check(zap.DebugLevel, "debugging"); ce != nil {
    // If debug-level log output isn't enabled or if zap's sampling would have
    // dropped this log entry, we don't allocate the slice that holds these
    // fields.
    ce.Write(
        zap.String("foo", "bar"),
        zap.String("baz", "quux"),
    )
}
```