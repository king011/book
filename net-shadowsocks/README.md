# shadowsocks

shadowsocks 一個使用 python c++ c# 等開發的 加密傳輸的 開源(Apachev2 部分組件使用 GPLv3/LGPL) socks5 代理 服務器/客戶端

官網 [https://shadowsocks.org/](https://shadowsocks.org/)

源碼 [https://github.com/shadowsocks](https://github.com/shadowsocks)

> Shadowsocks 的目的 主要是 爲了 突破網路封鎖 在某些 極端環境下 通過 深度包檢查(DPI) 可能會暴露 用戶 身份
> 
> 可以在Shadowsocks之後 在加上 tor
> 
