# Piwigo

Piwigo 是一個 php + mysql 實現的開源(GPL 2)相冊網站系統

強烈建議不要使用, bug 多的不是一點並且提交 [issue](https://github.com/Piwigo/Piwigo/issues/1773) 也等不到修復(本喵難以相信這是一個已經超過二十年的系統), 上傳視頻竟然可能導致視頻已經毀壞但提示你一切成功直到你播放時才發現珍貴的家庭視頻已經永遠毀滅了(本喵已經因損失了大量家庭影片, 本喵永遠也想不到 php 的程序員水平竟然這麼低, 本喵永遠不想再使用任何 php 相關的東西)

這個軟體在本喵看來簡直就是坑害使用者的毒瘤, 除非你的視頻或照片可以忍受隨機的隨時不會心疼否則不要再使用這個垃圾了(本喵使用中還遇到了其它 bug 但已經懶得去提 issue了, 反正提了也不會修復, 本喵已經放棄這個垃圾了, 本喵之前還專門爲它開發了 android tv app 來適配電視真他媽的浪費生命)

對於已經被坑害的用戶建議使用本喵寫的[腳本](https://github.com/powerpuffpenguin/deno-scripts?tab=readme-ov-file#piwigots)把相冊都下載下來存儲到其它系統中


* 官網 [https://piwigo.org/](https://piwigo.org/)
* 中文官網 [https://cn.piwigo.org/](https://cn.piwigo.org/)
