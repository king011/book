# runit program

runit 必須以 PID 1 在 Unix 中啓動，即作爲系統第一個進程啓動，對於 docker 容器就是 docker run 執行的進程

它分三個階段執行系統的 啓動 運行 關閉

## Stage 1
runit 運行 **/etc/runit/1** 並等待它的結束。系統的一次性任務在這裏執行。**/etc/runit/1** 擁有 **/dev/console** 的完全控制權，以便在一次性初始化任務失敗時能夠啓動緊急的 shell

如果 **/etc/runit/1** 崩潰或返回 100，runit 將跳過 階段2 和 階段 3

## Stage 2

runit 運行 **/etc/runit/2**，直到系統關閉才返回；如果它崩潰或返回 111，它將重新啓動

通常 /etc/runit/2 會啓動 runsvdir。runit 能夠在階段 2 處理 ctrl-alt-del 鍵盤請求

## Stage 3

如果 runit 被告知關閉系統，或者 階段 2 返回，如果階段 2 在運行終止階段 2 並且運行 **/etc/runit/3**。在此執行關閉或系統重啓相關的操作

如果 **/etc/runit/reboot** 存在且擁有執行權限，runit 會執行它

# ctrl-alt-del
如果 runit 接收到鍵盤 ctrl-alt-del 會檢查 **/etc/runit/ctrlaltdel** 如果存在且擁有執行權限，則 runit 運行 /etc/runit/ctrlaltdel，等待它終止，最後發送一個 CONT signal

# Signals

runit 只在階段 2 接收 signal

如果 runit 收到 CONT signal 會檢查如果 **/etc/runit/stopit** 存在 並且擁有執行權限，則通知 runit 關閉系統

如果 runit 收到一個 INT signal，就會觸發一個 ctrl-alt-del 鍵盤請求
