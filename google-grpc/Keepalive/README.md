# keepalive
```go
#info=false
import "google.golang.org/grpc/keepalive"
```

keepalive 包 提供了 客戶端/服務器 keepalive 相關設置

> 經測試某些版本 設置了 keepalive 服務器不能正常返回 ack 這將導致客戶端以爲連接斷開從而真的斷開連接，簡單來說就是這個功能可能存在不穩定的 bug 故通常不要使用，要用的話請謹慎測試沒有問題再開啓

# WithKeepaliveParams
客戶端 調用 grpc.WithKeepaliveParams 傳入 keepalive.ClientParameters 創建一個 連接選項

```go
// ClientParameters is used to set keepalive parameters on the client-side.
// These configure how the client will actively probe to notice when a
// connection is broken and send pings so intermediaries will be aware of the
// liveness of the connection. Make sure these parameters are set in
// coordination with the keepalive policy on the server, as incompatible
// settings can result in closing of connection.
type ClientParameters struct {
	// After a duration of this time if the client doesn't see any activity it
	// pings the server to see if the transport is still alive.
	//
	// 多久發送一次 ping 默認 infinity 永遠不會 發送 
	Time time.Duration // The current default value is infinity.
	// After having pinged for keepalive check, the client waits for a duration
	// of Timeout and if no activity is seen even after that the connection is
	// closed.
	//
	// ping 之後 經過 此時間 依然 沒有 active RPCs 就 關閉 Conn 默認 20秒
	Timeout time.Duration // The current default value is 20 seconds.
	// If true, client sends keepalive pings even with no active RPCs. If false,
	// when there are no active RPCs, Time and Timeout will be ignored and no
	// keepalive pings will be sent.
	//
	// 需要設置 爲 true 才會 發送 ping,只有在沒有 active RPCs 時 才會發送 ping
	PermitWithoutStream bool // false by default.
}
```

* 在go中 測試 實踐上是 結果 Time+Timeout 才 發送的 網路數據包
* 如果 ping 成功 會被視爲 一個 active RPCs 如果服務器 不運行 ping 則會 ping 失敗 有可能會 關閉 Conn
* 在 go中 測試 如果連續 ping 了 4 次 已經沒有 active RPCs 則會 關閉 Conn
* 在 active RPCs  活躍時 不會 發送 ping

所以 只有在 PermitWithoutStream 爲 true 且 Time 大於0時 客戶端 才會 發送 ping 包到  服務器

```
grpc.WithKeepaliveParams(keepalive.ClientParameters{
	Time:                time.Second * 10,
	PermitWithoutStream: true,
})
```

# KeepaliveEnforcementPolicy

服務器調用 KeepaliveEnforcementPolicy 傳入 keepalive.EnforcementPolicy 創建一個 服務器 選項 定義了 允許的 客戶端 keepalive 行爲

```go
// EnforcementPolicy is used to set keepalive enforcement policy on the
// server-side. Server will close connection with a client that violates this
// policy.
type EnforcementPolicy struct {
	// MinTime is the minimum amount of time a client should wait before sending
	// a keepalive ping.
	//
	// 運行的 最短 ping 間隔 默認 5 分鐘
	MinTime time.Duration // The current default value is 5 minutes.
	// If true, server allows keepalive pings even when there are no active
	// streams(RPCs). If false, and client sends ping when there are no active
	// streams, server will send GOAWAY and close the connection.
	//
	// 如果爲 true 允許 ping 否則 不允許
	PermitWithoutStream bool // false by default.
}
```

* 當服務器不允許ping 或 ping 太頻繁超過 MinTime 限制 服務器 會 返回ping失敗 此時 客戶端 不會認爲這個ping是 active RPCs

# KeepaliveParams

服務器調用 KeepaliveParams 傳入 ServerParameters 設置 如何 檢測 客戶端 狀態

```go
// ServerParameters is used to set keepalive and max-age parameters on the
// server-side.
type ServerParameters struct {
	// MaxConnectionIdle is a duration for the amount of time after which an
	// idle connection would be closed by sending a GoAway. Idleness duration is
	// defined since the most recent time the number of outstanding RPCs became
	// zero or the connection establishment.
	MaxConnectionIdle time.Duration // The current default value is infinity.
	// MaxConnectionAge is a duration for the maximum amount of time a
	// connection may exist before it will be closed by sending a GoAway. A
	// random jitter of +/-10% will be added to MaxConnectionAge to spread out
	// connection storms.
	MaxConnectionAge time.Duration // The current default value is infinity.
	// MaxConnectinoAgeGrace is an additive period after MaxConnectionAge after
	// which the connection will be forcibly closed.
	MaxConnectionAgeGrace time.Duration // The current default value is infinity.
	// After a duration of this time if the server doesn't see any activity it
	// pings the client to see if the transport is still alive.
	//
	// 類似 ClientParameters.Time 不過默認爲 2小時
	Time time.Duration // The current default value is 2 hours.
	// After having pinged for keepalive check, the server waits for a duration
	// of Timeout and if no activity is seen even after that the connection is
	// closed.
	//
	// 類似 ClientParameters.Timeout 默認 20秒
	Timeout time.Duration // The current default value is 20 seconds.
}
```