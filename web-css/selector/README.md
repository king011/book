# 選擇器

選擇器規定了 CSS 規則會被應用到哪些元素上，通常可以被劃分爲下列幾個類別

* 簡單選擇器(Simple selectors): 通過元素類型 class id 匹配一個或多個元素
* 屬性選擇器(Attribute selectors): 通過 屬性/屬性值 匹配一個或多個元素
* 組合器(Combinators): 組合多個選擇器共同匹配元素
* 多用選擇器(Multiple selectors): 這個思路是將以逗號分隔的多個選擇器放在一個 css 規則下面，以將一組聲明應用與這些選擇器選擇的所有元素
* 僞類(Pseudo-classes): 匹配處於確定狀態的一個或多個元素，比如鼠標指針懸停的元素，或當前 被選中/未被選中 的複選框，或元素是 DOM 樹中一個父節點的第一個子節點
* 僞元素(Pseudo-elements): 匹配處於相關的確定位置的一個或多個元素，例如每個段落的第一個字，或者某個元素之前生成的內容

# 簡單選擇器(Simple selectors)
如其名稱一樣，簡單選擇器提供了一些簡單好用的元素選取方法

## 類型選擇器

此類選擇器是一個 HTML標籤 名稱，不區分大小寫。他會選取所有同名的標籤元素。

```
/* 選取頁面所有 div 標籤 */
div {
		color: red;
}
```

## 類選擇器

類選擇器使用 **.** 前綴加上類名 選取所有包含此類名的元素

```
/* 選取頁面所有包含類名爲 button 的元素 */
.button {
		color: red;
}
```


## ID 選擇器

id選擇器使用 **#** 前綴加上 id名稱 選取被設置爲此 id 的元素，一個 id 在頁面中必須是唯一，如果有多個同名 id 行爲將是未定義的(不過目前大部分瀏覽器都是忽略第一個之外的其它同名id)

```
/* 選取 id 爲 view 的一個元素 */
#view {
		color: red;
}
```

## 通用選擇器

使用通用選擇器 **\***  選擇頁面中的所有元素，通常爲所有元素添加相同的樣式沒有價值，所以更常見的是與其它選擇器配合使用。

```
/* 選取頁面所有元素 */
* {
}
```

# 屬性選擇器(Attribute selectors)

根據元素的 屬性 和屬性值 來匹配元素

## 存在的值(Presence and value)

* [attr]: 選擇所有帶有 attr 屬性的元素，不論其值爲何
* [attr=val]: 僅選取 atrr 屬性 被設置爲 val 的所有元素
* [attr~=val]: 選擇 attr 屬性的值(以空格隔開多個值) 中有包含 val 值的元素(類似 class 選擇器 的工作模式)

## 子串值(Substring value)

* [attr|=val]: 選取 attr 屬性以 val 或 val- 開頭的元素
* [attr^=val]: 選取 attr 屬性以 val 開頭的元素
* [attr$=val]: 選取 attr 屬性以 val 結尾的元素
* [attr\*=val]: 選取 attr 屬性包含字符串 val 的元素

# 組合器(Combinators)

組合器允許將多個選擇器組合在一起，這允許在其它元素中選擇元素，或者與其它元素相鄰，有四種可用的類型

* 後代選擇器(空格): 允許選擇嵌套在另外一個元素中的元素
* 子選擇器(&gt;): 允許選擇嵌套在另外一個元素中的子元素
* 相鄰兄弟選擇器(+): 允許選擇一個元素的直接兄弟元素(也就是說在它旁邊，層次結構的同一層)
* 通用兄弟選擇器(~): 允許選擇一個元素的兄弟元素(在層次結構中相同級別，但不一定在它旁邊)

| Combinators | Select |
| -------- | -------- |
| A,B     | 匹配滿足 A 或 B 的任意元素     |
| A B     | 在匹配 A 的元素的子元素中 選取匹配 B 的元素     |
| A>B     | 在匹配 A 的元素的直接子元素中 選取匹配 B 的元素     |
| A+B     | 匹配A的直接兄弟節點B，既 A B 有相同父節點且 B是A的下一個節點     |
| A~B     | 匹配A的兄弟節點B，既 A B 有相同父節點     |

# 僞類(Pseudo-classes)

一個僞類是一個以 **:**(冒號) 作爲前綴的關鍵字，當希望樣式在特定狀態下才被呈現到元素時就可以在選擇器後面加上對應的僞類



| 選擇器 | 例子 | 例子含義 |
| -------- | -------- | -------- |
| :active     | a:active     | 選擇活動鏈接     |
| :checked     | input:checked     | 選擇每個被選中的 input 元素     |
| :default     | input:default     | 選擇默認的 input 元素     |
| :disabled     | input:disabled     | 選擇被禁用的 input 元素     |
| :empty     | p:empty     | 選擇沒有子元素和文本的所有 p 元素     |
| :enabled     | input:enabled     | 選擇每個啓用的 input 元素     |
| :first-child     | p:first-child     | 選擇屬於父元素的第一個子元素的每個 p 元素     |
| :first-of-type     | p:first-of-type     | 選擇屬於其父元素的首個 p 元素的每個 p 元素     |
| :focus     | input:focus     | 選取活動焦點的 input 元素     |
| :fullscreen     | :fullscreen     | 選擇處於全屏模式的元素     |
| :hover     | a:hover     | 選擇鼠標指針位於其上的鏈接     |
| :in-range     | input:in-range     | 選擇其值在指定範圍內的 input 元素     |
| :indeterminate     | input:indeterminate     | 選擇處於不確定狀態的 input 元素     |
| :invalid     | input:invalid     | 選擇具有無效值的所有 input 元素     |
| :lang(language)     | p:lang(it)     | 選擇 lang 屬性 等於 "it"(意大利) 的每個 p 元素     |
| :last-child     | p:last-child     | 選擇屬於其父元素最後一個子元素的 p 元素     |
| :last-of-type     | p:last-of-type     | 選擇屬於其父元素的最後一個 o 元素的每個 p 元素     |
| :link     | a:link     | 選擇所有未被訪問過的鏈接     |
| :not(selector)     | :not(p)     | 選擇非 p 的每個元素     |
| :nth-child(n)     | p:nth-child(2)     | 選擇屬於其父元素的第二個子元素的每個 p 元素     |
| :nth-last-child(n)     | p:nth-last-child(2)     | 同上但從最後一個子元素開始計數     |
| :nth-of-type(n)     | p:nth-of-type(2)     | 選擇屬於其父元素第二個 p 元素的 每個 p 元素     |
| :nth-last-of-type(n)     | p:nth-last-of-type(2)     | 同上，但從最後一個子元素開始計數     |
| :only-of-type     | p:only-of-type     | 選擇屬於其父元素唯一的 p 元素的每個 p 元素     |
| :only-child     | p:only-child     | 選擇屬於其父元素的唯一子元素的每個 p 元素     |
| :optional     | input:optional     | 選擇不帶 "required" 屬性的 input 元素     |
| :out-of-range     | input:out-of-range     | 選擇值超出指定範圍的 input 元素     |
| :read-only     | input:read-only     | 選擇已規定 readonly 屬性的 input 元素     |
| :read-write     | input:read-write     | 選擇未規定 readonly 屬性的 input 元素     |
| :required     | input:required     | 選擇已規定 "required" 屬性的 input 元素     |
| :root     | :root     | 選擇文檔的根元素     |
| :target     | #news:target     | 選擇當前活動的 #news 元素     |
| :valid     | input:valid     | 選擇帶有有效值的所有 input 元素     |
| :visited     | a:visited     | 選擇所有已訪問過的鏈接     |

# 僞元素(Pseudo-elements)
僞元素更僞類很像但又不同，使用**::**(雙冒號)作爲前綴，同樣是添加到選擇器後達到指定某個元素的某個部分

| 選擇器 | 例子 | 例子含義 |
| -------- | -------- | -------- |
| ::after | p::after | 在每個 p 的內容之後插入內容 |
| ::before | p::before | 在每個 p 的內容之前插入內容 |
| ::first-letter | p::first-letter | 選擇每個 p 元素的首字母 |
| ::first-line | p::first-line | 選擇每個 p 元素的首行 |
| ::placeholder | input::placeholder | 選取已規定 "placeholder" 屬性的 input 元素 |
| ::selection | ::selection | 選取用戶已選取的元素部分 |
