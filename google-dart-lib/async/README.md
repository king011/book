# dart:async
```dart
#info=false
import 'dart:async';
```

dart:async 提供了 異步編程 的工具

# Future

Future 類似 js 的Promise 代表一個未來完成的值 

可以通過 sucess 或 error 來 完成

其 提供了

* catchError 註冊失敗的錯誤處理
* then 來註冊 成功回調 ， 命名 參數 onError 也可以註冊錯誤處理
* whenComplete 註冊 完成回調
* asStream 轉換爲 Stream
* timeout 註冊一個 超時異常

如果 沒有註冊 錯誤回調 則發生錯誤時 會throw 異常

catchError 可以傳入一個 命名參數 test test 返回 false 則可以讓 catchError 忽略掉此異常 使用 異常 繼續向上傳遞

whenComplete 註冊的 回調 無論 成功還是失敗都會 被 回調

timeout 可以選擇 單獨註冊一個 超時異常處理回調 

Future 一旦定義 就會完成 無論是否有註冊 then 或 catchError

```
import 'dart:async';

main() {
  Future<int>(() => 123)
      .timeout(Duration(milliseconds: 100))
      .then((v) => print(v))
      .catchError((e) => print("error : $e"))
      .whenComplete(() => print("complete"));
}
```

## delayed

delayed 構造函數 可以創建一個 延遲執行的 Future

```
import 'dart:async';

main() {
  Future<int>.delayed(Duration(seconds: 1), () => 123)
      .timeout(Duration(milliseconds: 100))
      .then((v) => print(v))
      .catchError((e) => print("error : $e"))
      .whenComplete(() => print("complete"));
}
```

## error
error 構造一個以 異常完成 的 Future

```
import 'dart:async';

main() {
  Future<int>.error("test error")
      .timeout(Duration(milliseconds: 100))
      .then((v) => print(v))
      .catchError((e) => print("error : $e"))
      .whenComplete(() => print("complete"));
}
```

## static methods 

Future 提供了 幾個 靜態的輔助方法

* any 返回將要 第一個 完成的 Future 
* doWhile 重複執行操作 直到返回 false 可以同步也可以異步
* forEach 遍歷 Iterable 可以同步也可以異步
* wait 等待多個 Future 完成 並收集 多個 Future 的值

```
import 'dart:async';

main() {
  // 1
  // 2
  // 3
  int i = 0;
  Future.doWhile(() {
    ++i;
    print(i);
    return i % 3 != 0;
  });

  // 4
  // 5
  // 6
  Future.forEach([4, 5, 6], (v) => print(v));

  final futures = [
    Future<int>.delayed(Duration(milliseconds: 100), () => 123),
    Future<int>.delayed(Duration(milliseconds: 200), () => 456),
  ];
  // 123
  Future.any(futures).then((v) => print(v));

  // [123, 456]
  Future.wait(futures).then((v) => print(v));
}
```

## microtask

類似 默認構造函數 但 以 scheduleMicrotask 的微任務 運行

默認構造函數 以 Timer 運行

## sync

類似 默認構造函數 但 以 同步方式 運行

## value

相當於 sync 但直接以指定值 返回

```
import 'dart:async';

main() {
  Future<int>.value(123)
      .timeout(Duration(milliseconds: 100))
      .then((v) => print(v))
      .catchError((e) => print("error : $e"))
      .whenComplete(() => print("complete"));
}
```

# Completer

Completer 類似 Promise 中的 resolve, reject 可以 通過

* future 屬性返回 Completer 關聯的 Future
* complete 以成功值 完成 Future
* completeError 以異常 完成 Future
* isCompleted 返回 Future 是否完成

```
import 'dart:async';

main() {
  final completer = Completer<int>();
  completer.future.then((v) => print(v));
  Timer(Duration(seconds: 1), () => completer.complete(1));
}
```

如果 completer 已經完成 繼續調用 complete/completeError 將 引發異常

## sync
 使用同步方式 完成 completer

```
import 'dart:async';

main() {
  final completer = Completer<int>.sync();
  completer.future.then((v) => print(v));
  completer.complete(1);
}
```

# Timer
Timer 是一個 定時器 可以在指定的超時 時間後 執行一個回調函數

```dart
import 'dart:async';

// 聲明 main 爲 異步 函數
main() {
  const timeout = const Duration(seconds: 3);
  var t0 = Timer(timeout, () => print("timer 0"));
  Timer t1;
  t1 = new Timer(Duration(seconds: 1), () {
    // isActive 返回 timer 是否 有效
    print("${t0.isActive}"); // true
    t0.cancel(); // 取消 t0 調用
    print("${t0.isActive}"); // false

    print("timer 1 ${t1.isActive}"); // timer 1 false
  });
}
```

# StreamController&lt;T&gt; class

StreamController 是一個 控制器 可以 控制一個 Stream&lt;T&gt;

可以 向流 發送 數據/錯誤/完成 通常用來 創建可供他人監聽的 簡單流 並將 事件 推送到 此流

同時 允許 監測 流是否 粘貼 有訂閱者 並在其中任何一個 更改時 獲得回調

常用 方法
```dart
// 發送數據到 流中 通知聽總接收數據
void add(T event);

// 發送錯誤到流中 通知聽總錯誤
void addError(Object error, [StackTrace stackTrace]);

// 關閉流 通知 通知聽總完成
Future close();
```