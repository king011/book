# if
if 可以作爲表達式

```
fun main() {
    var a = 1
    var b = 2
    if (a < b) {
        println("small")
    } else {
        println("not small")
    }
    println(if (a < b) a else b)
}
```

分支的最後表達式 作爲 分支的返回值

# when

when 取代了 switch

```
fun main() {
    var x = 1
    when (x) {
        1 -> print("x == 1")
        2 -> print("x == 2")
        else -> { //除非編譯器能檢測到已處理所有可能分支否則需要 else
            print("x is neither 1 nor 2")
        }
    }
}
```

同樣 分支的最後表達式 作爲 分支的返回值
```
fun main() {
    println(
        when ("cerberus") {
            is String -> "string"
            else -> "not string"
        }
    )
}
```

# for

for 可以遍歷 迭代器

迭代器 是指 重載了
* iterator
* next
* hasNext
的對象

```
for (i in 1..3) {
    println(i)
}
for (i in 6 downTo 0 step 2) {
    println(i)
}
for (i in array.indices) {
    println(array[i])
}
for ((index, value) in array.withIndex()) {
    println("the element at $index is $value")
}
```

# while

```
while (x > 0) {
    x--
}

do {
  val y = retrieveData()
} while (y != null) 
```

# break continue
同 c

# 標籤 
kotlin 的表達式 前 都可以 使用 @ 添加 一個 標籤

break continue 可以 指定 要跳轉到的 標籤
```
package com.example

fun main() {
    loop@ for (x in 0..10) {
        for (y in 0..10) {
            if (x == 5 && y == 5) {
                // 跳出指定 標籤
                break@loop
            }
            if (y == 5) {
                // 跳轉到指定 標籤
                continue@loop
            }
            println("x=$x y=$y")
        }
    }
}
```

return 也支持 標籤 指示要返回的 函數

```
package com.example

fun find(value: Int, list: List<Int>): Int {
    println("find $value")

    list.forEachIndexed { i, v ->
        println("compare index $i $v")
        if (value == v) {
            return i
        }

    }

    return -1
}

fun count(value: Int, list: List<Int>): Int {
    println("count $value")

    var sum = 0
    list.forEachIndexed fea@ { i, v ->
        println("count index $i $v")
        if (value == v) {
            sum++
						// 內聯的 lambda 表達式 會直接返回到 count 的 調用者 此處用 標籤 指示 返回到 forEachIndexed
            return@fea
            //return@forEachIndexed
        }
        println("$v not match")
    }

    return sum
}

fun main() {
    println(find(2, listOf(1, 2, 3, 4, 5)))
    println(count(2, listOf(1, 2, 3, 2, 5)))
}

```

