# emby

emby 本來是一個開源且免費的多媒體串流工具但其流行後官方開始收費並對於免費版本不提供硬件加速編解碼，雖然編解碼的工作都是 ffmpeg 和顯卡在做，但 emby 還是將這一功能從免費版本中剔除了，並且將部分源碼設置爲閉源，沒有硬件加速 emby 實際上可用性大減，但如果不需要轉碼串流的化 emby 還是可以一用

* 官網 [https://emby.media/index.html](https://emby.media/index.html)
* 源碼 [https://github.com/MediaBrowser/Emby](https://github.com/MediaBrowser/Emby)