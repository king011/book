# [模塊](https://deno.land/manual@v1.28.1/basics/modules)

deno 使用 es6 模塊語法

* **import** 導入模塊可以是本地檔案也可以是一個遠程的 URL
* **export** 將模塊內的內容導出

deno 採用類似瀏覽器的模塊加載模式,所以你不能省略導入模塊的後綴名

# 模塊緩存

默認情況下緩存中的模塊將被重用而無需重新獲取或編譯它.如果你向重新獲取和編譯緩存,可以使用 cache --reload 來使用本地 DENO_DIR 緩存無效


重載所有緩存
```
deno cache --reload src/main.ts
```

重載特定緩存

```
deno cache --reload=https://deno.land/std@0.165.0 src/main.ts
deno cache --reload=https://deno.land/std@0.165.0/fs/copy.ts,https://deno.land/std@0.165.0/fmt/colors.ts src/main.ts
```

# 依賴完整性

deno 可以加載遠程模塊,並且模塊會被保存在緩存中,如果遠程模塊發生了變化,需要一些機制保證依賴完整以及正確

可以使用 --lock-write --lock=deno.lock 參數來創建一個 lock 檔案,它將記錄遠程模塊的 hash 以保證模塊完整

# Proxies
deno 會讀取環境變量 HTTP\_PROXY HTTPS\_PROXY NO\_PROXY 來確定是否使用代理下載模塊,這對西朝鮮等地區可以說是必不可少的

對於 Windows 平臺,如果沒有找到這些環境變量,deno 還會從註冊表中讀取代理設定

# 私有模塊和存儲庫

deno 使用 url 加載遠程模塊,使用你可以在模塊url中加入你的私有token

此外你可以設置環境變量 **DENO\_AUTH\_TOKENS** deno 會幫你在 url 中注入 token,多個令牌使用 ; 分隔

1. bearer token 格式 **{token}@{hostname[:port]}**
2. basic auth  格式 **{username}:{password}@{hostname[:port]}**

```
#info="一些例子"

DENO_AUTH_TOKENS=a1b2c3d4e5f6@deno.land

DENO_AUTH_TOKENS=username:password@deno.land

DENO_AUTH_TOKENS=a1b2c3d4e5f6@deno.land;f1e2d3c4b5a6@example.com:8080,username:password@deno.land
```

# Import Maps

deno 運行在一個 json 檔案中指定導入模塊的 url 映射別名,這似乎是目前唯一正確的 deno 處理複雜依賴的可行方案

```
#info="import_map.json"
{
  "imports": {
    "fmt/": "https://deno.land/std@0.165.0/fmt/"
  }
}
```
```
import { red } from "fmt/colors.ts";

console.log(red("hello world"));
```
# node module

目前 deno 支持了加載 node module

```
import express from "npm:express@^4.18";
```

# 依賴管理

目前 Deno 1.28.2 推薦的做法是創建一個 deps.ts 檔案，在裏面寫入所有的依賴並導出，項目則通過 deps.ts 間接導入依賴

```
#info="deps.ts"
export {
  add,
  multiply,
} from "https://x.nest.land/ramda@0.27.0/source/index.js";
```

這種做法存在幾個顯著的問題
1. 所有依賴名稱都暴露在 deps.ts 檔案中，如果多個依賴庫中有同名函數只能爲其設置別名(這不但麻煩而且將影響庫的使用體驗)
2. 一旦 import deps.ts 將引出所有依賴即使裏面可能某些依賴並沒有使用(並且會形成一個依賴黑洞以爲一旦有人 import 你的代碼也間接 import 了你的 deps.ts 而其他人可能只想使用你的部分功能但因爲 deps.ts 所有依賴都會被編碼)，這不但增加了編譯時間並且因爲 目前 deno 不支持 tree shaking (似乎從 1.5 開始說支持但本喵感覺 效果並不好) 也會增加編譯後的代碼大小


一個解決方案之一是使用 import_map.json 但這對項目開發可能適合，但若作爲一個庫或 命令行工具不太推薦，因爲 import_map.json 作爲第三方庫，可能會導致使用 你庫的 代碼無法正確執行，因爲 使用者沒有和你設置一樣的 import_map.json 導致 你代碼 import 的內容 deno 無法識別。此外作爲命令行工具 則也需要 使用者設置和你一樣的 import_map.json 導致工具使用成本增加


本喵考慮後使用了如下解決方案，使用一個 deps.ts 腳本但裏面不是直接的 export 而是記錄了依賴信息，執行這個腳本則在 deps 檔案夾下爲依賴生成單獨的 export 檔案

```
#info="deps.ts"
interface Dependency {
  name: string;
  url: string;
  mod: Array<string>;
}

function define(name: string, url: string, mod?: Array<string>): Dependency {
  return {
    name: name,
    url: url,
    mod: mod ?? [],
  };
}
async function deps(output: string, ...deps: Array<Dependency>) {
  if (output == "") {
    output = "./";
  } else if (Deno.build.os == "windows") {
    if (!output.endsWith("\\") && !output.endsWith("/")) {
      output += "\\";
    }
  } else if (!output.endsWith("/")) {
    output += "/";
  }

  for (const dep of deps) {
    console.log(`dependency: ${dep.name} from ${dep.url}`);
    const dir = `${output}${dep.name}`;
    await Deno.mkdir(dir, { recursive: true });

    if (dep.url.startsWith("npm:")) {
      console.log(` - mod.ts`);
      await Deno.writeTextFile(
        `${dir}/mod.ts`,
        `export { default } from "${dep.url}";`,
      );
      return;
    }

    for (const mode of dep.mod) {
      console.log(` - ${mode}`);
      const found = mode.lastIndexOf("/");
      if (found) {
        await Deno.mkdir(`${dir}/${mode.substring(0, found)}`, {
          recursive: true,
        });
      }
      await Deno.writeTextFile(
        `${dir}/${mode}`,
        `export * from "${dep.url}/${mode}";`,
      );
    }
  }
}

deps(
  "deps",
  define("std", "https://deno.land/std@0.167.0", [
    "log/mod.ts",
    "testing/asserts.ts",
  ]),
  define(
    "easyts",
    "https://deno.land/x/easyts@0.1.0",
    [
      "channel.ts",
      "exception.ts",
      "net/url/mod.ts",
    ],
  ),
  define(
    "luxon",
    "https://cdn.jsdelivr.net/npm/luxon@3.1.0/build/es6",
    [
      "luxon.js",
    ],
  ),
  define(
    "art-template",
    "npm:art-template@^4.13.2",
  ),
);
```

1. 上述代碼調用 deps 函數生成依賴項代碼
2. 調用 define 定義一個依賴項目
3. define 第一個參數是爲依賴取個名稱別名，第二個參數是 base url，第三個路徑是要使用的 模塊 path (相對 base url)