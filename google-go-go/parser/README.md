# [parser](https://pkg.go.dev/go/parser)

parser 包用於解析go源碼，並返回 AST 樹

```
package main

import (
	"fmt"
	"go/parser"
	"go/token"
	"log"
	"os"
)

func main() {
	src, e := os.ReadFile(`main.go`)
	if e != nil {
		log.Fatalln(e)
	}

	fset := token.NewFileSet() // positions are relative to fset
	// 解析源碼
	f, err := parser.ParseFile(fset,
		"main.go", src,
		parser.ImportsOnly, // 只解析 import 內容後就結束
	)
	if err != nil {
		log.Fatalln(err)
	}

	// 打印 imports
	for _, s := range f.Imports {
		fmt.Println(s.Path.Value)
	}
}
```

# ParseDir

ParseDir 用於解析檔案夾下的所有以 .go 結尾的檔案

```
func ParseDir(fset *token.FileSet, path string, filter func(fs.FileInfo) bool, mode Mode) (pkgs map[string]*ast.Package, first error)
```