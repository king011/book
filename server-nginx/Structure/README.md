# if

nginx提供了if語言 用於 控制結構 **不支持else**

字符串比較

* = 相等比較
* != 不等比較
* ~ 與指定正則匹配時返回 true 區分大小寫
* ~\*  與指定正則匹配時返回 true 不區分大小寫
* !~ 與指定正則不匹配時返回 true 區分大小寫
* !~* 與指定正則不匹配時返回 true 不區分大小寫

檔案目錄比較

* -f,!-f 判斷指定的路徑是否爲存在的檔案
* -d,!-d 判斷指定的路徑是否爲存在的目錄
* -e,!-e 判斷指定的路徑是否存在
* -x,!-x 判斷指定的路徑是否爲存在且可執行

```
server {
	listen 8080;
	set $contentType "text/plain;charset=utf-8";
	set $body "cerberus is an idea";
	if ($arg_type = json) {
		set $contentType "application/json;charset=utf-8";
		set $body '{"idea":"cerberus is an idea"}';
	}
	if ($arg_type = xml) {
		set $contentType "application/xml;charset=utf-8";
		set $body '<root>cerberus is an idea</root>';
	}
	add_header Content-Type $contentType;
	return 200 $body;
}
```

# include

include 指令可以將 指定檔案內容 複製到當前位置

```
include sites-enabled/*.conf;
include proxy.conf;
```

include 如果是相對路 徑都是相對 主配置檔案所在位置