# RabbitMQ

RabbitMQ 是一個使用Erlang實現的開源(MPL 2.0)的消息中間件 實現了 AMQP協議

生產者只需要將消息 發送到 RabbitMQ 消費者只需從RabbitMQ獲取消息 無需要知道 對方的存在從而實現了解偶 同時RabbitMQ 提供了多種消息派發機制來實現 消息單播 廣播 持久化消息等

* 官網 [https://www.rabbitmq.com/](https://www.rabbitmq.com/)
* 源碼 [https://github.com/rabbitmq/rabbitmq-server](https://github.com/rabbitmq/rabbitmq-server)
* 文檔 [https://www.rabbitmq.com/getstarted.html](https://www.rabbitmq.com/getstarted.html)
* 客戶端[https://www.rabbitmq.com/devtools.html](https://www.rabbitmq.com/devtools.html)