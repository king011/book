# 陣列

yaml支持兩種陣列寫法 

使用 - 加 空格

```yaml
- anita
- kate
- jolin
```

或者使用 \[...,...\] 形式

```yaml
[anita, kate, jolin]
```

# 雜湊

同樣支持兩種寫法

```yaml
- {name: kate,lv: 10}
- name: kate
  lv: 9
  age: it's secret
```

# 換行

使用 | 或 > 語法

```yaml
data: |
  第一行
  第二行
  第三行
```

```yaml
data: >
  第一行
  123
  
  第二行
  345
  
  第三行
  678
```
# 資料合併
yaml 允許使用 **&** 創建錨點 使用 **<<** 插入錨
```yaml
def: &base #創建一個錨
  age: 18
  lv: 1
beauty:
  - 
    <<: *base # 插入錨
    name: anita # 插入額外字段
    lv: 10 # 覆蓋錨
  - 
    <<: *base
    name: anita
```

# 強制型別

yaml 會自動判斷資料型別 也可以使用 **!!型別名** 強制指定

```yaml
a: 123                     # 整數
b: "123"                   # 字符串
c: 123.0                   # 浮點
d: !!float 123             # 浮點
e: !!str 123               # 字串
f: !!str Yes               # 字串
g: Yes                     # 布爾
h: Yes we have No bananas  # 字串
```