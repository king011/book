# docker

certbot 提供了 docker 鏡像方便來方便安裝與部署

## 安裝證書

```
docker run -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/www:/var/www" \
            certbot/certbot certonly --webroot -w /var/www/html -d example.com -d www.example.com
```

## 更新證書
```
docker run -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/www:/var/www" \
            certbot/certbot renew --dry-run
```

## 注意
docker 執行上述安裝與更新前請確保 /var/www 被作爲靜態網頁可以通過 80 端口訪問，例如你可以運行你 nginx 的鏡像
 
```
#info="docker-compose.yml"
version: '1'
services:
  nignx:
    image: nginx:1.23
    restart: always
    volumes:
      - ./default.conf:/etc/nginx/conf.d/default.conf:ro
      - /var/www:/var/www:ro
```

```
#info="default.conf"
server{
    listen 80 default;
    location /.well-known/ {
        alias /var/www/html/.well-known/;
    }
}
```

> alias 的前綴需要和 certbot certonly -w 參數指定的值匹配


# cloudflare 通配符證書

首先參照 [cloudflare](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/) 的說明創建一個 api token，爲此 token 設置好編輯 dns 的權限（創建後記得把 token 備份，後續無法再從 cloudflare 查詢到此值）

執行下述命令驗證 token 正常工作了(假設 API_TOKEN 變量存儲了你創建的的 api token )
```
curl -X GET "https://api.cloudflare.com/client/v4/user/tokens/verify" \
     -H "Authorization: Bearer $API_TOKEN" \
     -H "Content-Type:application/json"
```

執行下述命令獲取到 [zone id](https://api.cloudflare.com/#zone-list-zones) (假設 DOMAIN 變量存儲了你的主域名)

```
ZONE_ID=$(curl -X GET "https://api.cloudflare.com/client/v4/zones?name=$DOMAIN&status=active&page=1&per_page=20&order=status&direction=desc&match=all" \
     -H "Authorization: Bearer $API_TOKEN" \
     -H "Content-Type:application/json" | python -c "import sys,json;print(json.load(sys.stdin)['result'][0]['id'])")

echo "ZONE_ID=$ZONE_ID"
```

然後調用下述[接口](https://api.cloudflare.com/#dns-records-for-a-zone-create-dns-record)創建一個 dns 記錄(假設變量 CREATE\_DOMAIN 存儲來記錄名稱，CERTBOT\_VALIDATION 記錄了要解析到的值，ZONE\_ID 是上一步獲取到的 zone id)


```
RECORD_ID=$(curl -s -X POST "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records" \
     -H "Authorization: Bearer $API_TOKEN" \
     --data '{"type":"TXT","name":"'"$CREATE_DOMAIN"'","content":"'"$CERTBOT_VALIDATION"'","ttl":120}' \
             | python -c "import sys,json;print(json.load(sys.stdin)['result']['id'])")
echo "RECORD_ID=$RECORD_ID"
```

最後在不在需要時可以使用下述接口刪除 TXT 記錄

```
curl -s -X DELETE "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$RECORD_ID" \
    -H "Authorization: Bearer $API_TOKEN" \
    -H "Content-Type: application/json"
```

有了上述資料我們就可以爲 cloudflare 寫自動驗證的腳本
```
#!/bin/bash
export API_TOKEN="XXX"
certbot certonly \
--email zuiwuchang@gmail.com \
--agree-tos \
--preferred-challenges dns \
--server https://acme-v02.api.letsencrypt.org/directory \
--manual \
--manual-auth-hook /data/cloudflare_auth.sh \
--manual-cleanup-hook /data/cloudflare_cleanup.sh \
-d your_domain
```

```
#info="cloudflare_auth.sh"
#!/bin/bash

echo_error(){
    echo -en "\e[31m"
    echo "${@}"
    echo -en "\e[0m"
    exit 1
}
echo_info(){
    echo -en "\e[33m"
    echo "${@}"
    echo -en "\e[0m"
}
echo_success(){
    echo -en "\e[32m"
    echo "${@}"
    echo -en "\e[0m"
}
# 設置存儲請求的檔案夾路徑
CERTBOT_REQUEST_ID_DIR="/data/id/$CERTBOT_DOMAIN"

# 驗證 api token 已經設定
if [ "$API_TOKEN" == "" ];then
    echo_error "API_TOKEN not set"
fi

# 由 CERTBOT_DOMAIN 獲取 cloudflare 主域名
# CERTBOT_DOMAIN 會由 certbot 設定

# xx.yy CERTBOT_DOMAIN 本身就是主域名
DOMAIN=$(expr match "$CERTBOT_DOMAIN" '\([^\.]\+\.[^\.]\+\)$')
if [ "$DOMAIN" == "" ];then
    # XXX.xx.yy CERTBOT_DOMAIN 這是一個子域名
    DOMAIN=$(expr match "$CERTBOT_DOMAIN" '.*\.\([^\.]\+\.[^\.]\+\)$')
fi
if [ "$DOMAIN" == "" ];then
    echo_error "CERTBOT_DOMAIN invalid"
fi

echo_info "domain: $DOMAIN"
echo_info "certbot domain: $CERTBOT_DOMAIN"
echo_info "id dir: $CERTBOT_REQUEST_ID_DIR"
set -e
# 刪除 dns 記錄，以免存在多條歷史記錄導致驗證失敗
if [ -f "$CERTBOT_REQUEST_ID_DIR/ZONE_ID" ];then
    ZONE_ID=$(cat "$CERTBOT_REQUEST_ID_DIR/ZONE_ID")
    if [ -f "$CERTBOT_REQUEST_ID_DIR/RECORD_ID" ]; then
        RECORD_ID=$(cat "$CERTBOT_REQUEST_ID_DIR/RECORD_ID")
        if [[ "$ZONE_ID" != "" && "$RECORD_ID" != "" ]]; then
            curl -s -X DELETE "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$RECORD_ID" \
                -H "Authorization: Bearer $API_TOKEN" \
                -H "Content-Type: application/json"
        fi
        rm -f "$CERTBOT_REQUEST_ID_DIR/RECORD_ID"
    fi
    rm -f "$CERTBOT_REQUEST_ID_DIR/ZONE_ID"
fi

# 獲取 zone id
ZONE_EXTRA_PARAMS="status=active&page=1&per_page=20&order=status&direction=desc&match=all"
ZONE_ID=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$DOMAIN&$ZONE_EXTRA_PARAMS" \
    -H "Authorization: Bearer $API_TOKEN" \
    -H "Content-Type:application/json" | python -c "import sys,json;print(json.load(sys.stdin)['result'][0]['id'])")
if [ ! -n "${ZONE_ID}" ]; then
    echo_error "GET ZONE ID FAIL"
fi
echo_info "zone id: $ZONE_ID"

# 添加 dns 記錄
# CERTBOT_VALIDATION 會由 certbot 設定
RECORD_NAME="_acme-challenge.$CERTBOT_DOMAIN"
echo_info "dns name: $RECORD_NAME"
echo_info "dns content: $CERTBOT_VALIDATION"
RECORD_ID=$(curl -s -X POST "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records" \
    -H "Authorization: Bearer $API_TOKEN" \
    -H     "Content-Type: application/json" \
    --data '{"type":"TXT","name":"'"$RECORD_NAME"'","content":"'"$CERTBOT_VALIDATION"'","ttl":120}' \
            | python -c "import sys,json;print(json.load(sys.stdin)['result']['id'])")
if [ ! -n "${RECORD_ID}" ]; then
    echo_error "SET DNS FAIL"
fi
echo_info "record id: $RECORD_ID"

# 記錄 id，以便後續刪除 dns 記錄
mkdir -m 0700 "$CERTBOT_REQUEST_ID_DIR" -p
echo $RECORD_ID > "$CERTBOT_REQUEST_ID_DIR/RECORD_ID"
echo $ZONE_ID > "$CERTBOT_REQUEST_ID_DIR/ZONE_ID"

echo_success "dns success"
# 等待一會兒以便 dns 更改能夠有足夠的傳播時間
sleep 130
```

```
#info="cloudflare_cleanup.sh"
#!/bin/bash

echo_error(){
    echo -en "\e[31m"
    echo "${@}"
    echo -en "\e[0m"
    exit 1
}
echo_info(){
    echo -en "\e[33m"
    echo "${@}"
    echo -en "\e[0m"
}
# 設置存儲請求的檔案夾路徑
CERTBOT_REQUEST_ID_DIR="/data/id/$CERTBOT_DOMAIN"

# 驗證 api token 已經設定
if [ "$API_TOKEN" == "" ];then
    echo_error "API_TOKEN not set"
fi

# 由 CERTBOT_DOMAIN 獲取 cloudflare 主域名
# CERTBOT_DOMAIN 會由 certbot 設定

# xx.yy CERTBOT_DOMAIN 本身就是主域名
DOMAIN=$(expr match "$CERTBOT_DOMAIN" '\([^\.]\+\.[^\.]\+\)$')
if [ "$DOMAIN" == "" ];then
    # XXX.xx.yy CERTBOT_DOMAIN 這是一個子域名
    DOMAIN=$(expr match "$CERTBOT_DOMAIN" '.*\.\([^\.]\+\.[^\.]\+\)$')
fi
if [ "$DOMAIN" == "" ];then
    echo_error "CERTBOT_DOMAIN invalid"
fi

echo_info "domain: $DOMAIN"
echo_info "certbot domain: $CERTBOT_DOMAIN"
echo_info "id dir: $CERTBOT_REQUEST_ID_DIR"

set -e
# 刪除 dns 記錄
if [ -f "$CERTBOT_REQUEST_ID_DIR/ZONE_ID" ];then
    ZONE_ID=$(cat "$CERTBOT_REQUEST_ID_DIR/ZONE_ID")
    if [ -f "$CERTBOT_REQUEST_ID_DIR/RECORD_ID" ]; then
        RECORD_ID=$(cat "$CERTBOT_REQUEST_ID_DIR/RECORD_ID")
        if [[ "$ZONE_ID" != "" && "$RECORD_ID" != "" ]]; then
            curl -s -X DELETE "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$RECORD_ID" \
                -H "Authorization: Bearer $API_TOKEN" \
                -H "Content-Type: application/json"
        fi
        rm -f "$CERTBOT_REQUEST_ID_DIR/RECORD_ID"
    fi
    rm -f "$CERTBOT_REQUEST_ID_DIR/ZONE_ID"
fi
```