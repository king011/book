# 面向對象

lua 沒有提供面向對象的直接實現，但提供來一些工具可以供用戶自實現

* **\_\_index** 元方法除來可以是一個函數外也可以是一個 table，此時獲取不存在的 key 時會從 \_\_index 指定的 table 返回key的值
* **function TableName:FunctionName** 定義的函數會在作用域內包含一個 self 變量作爲調用傳入的第一個參數
* **TableName:FunctionName** 形式的函數調用會把當前 table 自動作爲第一個參數傳入 函數中

有了上面幾條規則，我們就可以利用 **\_\_index** 來模式面向對象中的類與派生等特性，利用 **:** 形式的函數定義來模仿成員函數定義，利用 **:** 形式的函數調用來調用成員函數

# types 

下面是本喵創建的類型系統

```
#info="types/types.lua"
local function classToString(self, class)
    if class then
        return class.toString(self)
    end
    return self.className
end
local function classIndex(class, self, key)
    -- 子類已經實現
    local v = class[key]
    if v then
        return v
    end
    -- 調用父類實現
    local super = self:super()
    if super then
        return super[key]
    end
end
local function classInstanceof(self, t)
    repeat
        local mt = getmetatable(self)
        if mt == t then
            return true
        end
        self = self:super()
    until not self
    return false
end
local function classIsClass(self, t)
    local mt = getmetatable(self)
    return mt == t
end
local function createClass(name)
    local class = {
        className = name,
        toString = classToString,
        instanceof = classInstanceof,
        isclass = classIsClass
    }
    function class:__tostring()
        return classToString(self, class)
    end
    function class:__index(key)
        return classIndex(class, self, key)
    end
    return class
end
local function extendsSuper(super, class, self)
    self.super = function()
        return super
    end
    setmetatable(self, class)
    return self
end

local Object = createClass("Object")
function Object:new()
    return extendsSuper(nil, Object, {})
end

local Error = createClass("Error")
function Error:new(message)
    return extendsSuper(
        Object:new(),
        Error,
        {
            message = message
        }
    )
end
function Error:toString()
    local message = self.message
    if message then
        return self.className .. ": " .. message
    end
    return self.className .. ": nil"
end

return {
    --- 創建一個新的 類
    createClass = createClass,
    -- 連接父類實現到子類
    extendsSuper = extendsSuper,
    -- 所有類別的基類
    Object = Object,
    -- 所有錯誤的基類
    Error = Error
}
```

types 模塊導出了 兩個輔助函數和兩個基類

**Object** 是作爲所有類型的基類，爲類型提供來基本的通用方法

* className: string 型別的一個字符串名稱，僅僅供給人類查看
* func toString(): string 返回型別的字符串值
* func instanceof(Type): boolean 返回實例是否是 Type 的 Type子孫類型 的實例
* func isclass(Type): boolean 返回實例是否是 Type 的實例
* func super(): Super 返回父類實例以供方法父類方法，Object 的 super 函數將返回 nil

**createClass** 函數接受一個字符串參數創建一個新的類型

**extendsSuper** 函數實現子類從父類的派生



## 單元測試
```
#info="types/types_test.lua"
local luaunit = require "luaunit.luaunit"
local types = require("types.types")
function TestObject()
    local o1 = types.Object:new()
    local o2 = types.Object:new()
    luaunit.assertIsTrue(getmetatable(o1) == getmetatable(o2))
    luaunit.assertIsTrue(o1 ~= o2)
    o1.className = "123"
    luaunit.assertIsTrue(o1:toString() == "123")
    luaunit.assertIsTrue(o2:toString() == "Object")
    luaunit.assertIsTrue(o1:__tostring() == "123")
    luaunit.assertIsTrue(o2:__tostring() == "Object")

    luaunit.assertIsTrue(o1:isclass(types.Object))
    luaunit.assertIsTrue(o2:isclass(types.Object))
end
function TestError()
    local o = types.Object:new()
    local e1 = types.Error:new("t1")
    local e2 = types.Error:new()
    luaunit.assertIsTrue(getmetatable(e1) == getmetatable(e2))
    luaunit.assertIsTrue(getmetatable(e1) ~= getmetatable(o))
    luaunit.assertIsTrue(e1 ~= e2)

    luaunit.assertIsTrue(o:instanceof(types.Object))
    luaunit.assertIsFalse(o:instanceof(types.Error))
    luaunit.assertIsTrue(e1:instanceof(types.Object))
    luaunit.assertIsTrue(e1:instanceof(types.Error))
    luaunit.assertIsTrue(e2:instanceof(types.Object))
    luaunit.assertIsTrue(e2:instanceof(types.Error))

    luaunit.assertIsFalse(o:isclass(types.Error))
    luaunit.assertIsTrue(e1:isclass(types.Error))
    luaunit.assertIsTrue(e2:isclass(types.Error))
    luaunit.assertIsFalse(e1:isclass(types.Object))
    luaunit.assertIsFalse(e2:isclass(types.Object))

    e2.className = "456"
    luaunit.assertIsTrue(o:toString() == "Object")
    luaunit.assertIsTrue(e1:toString() == "Error: t1")
    luaunit.assertIsTrue(e2:toString() == "456: nil")
    luaunit.assertIsTrue(e2:super():toString() == "Object")

    luaunit.assertIsTrue(o:__tostring() == "Object")
    luaunit.assertIsTrue(e1:__tostring() == "Error: t1")
    luaunit.assertIsTrue(e2:__tostring() == "456: nil")
end
```

# example

有了上面的 types 模塊要使用面向對象就稍微容易了一些

```
#info="types/types_test.lua"
local types = require "types.types"

-- -- 定義一個新的 動物類
local Animal = types.createClass("Animal")
-- 構造方法
function Animal:new()
    return types.extendsSuper(
        types.Object:new(), -- 構造父類
        Animal, -- 設置元表
        {
            name = "animal" -- 擴展屬性
        }
    )
end
function Animal:run()
    print(self:toString() .. " run")
end

-- 定義一個新的 貓咪類
local Cat = types.createClass("Cat")
function Cat:new()
    return types.extendsSuper(
        Animal:new(),
        Cat,
        {
            name = "Tom" -- 覆蓋屬性
        }
    )
end
function Cat:eat()
    print(self.name .. " eat")
end
function Cat:speak()
    -- 調用父類
    local super = self:super()
    print(string.format("my name is %s,my parent name is %s", self.name, super.name))
end

local a = Animal:new()
local c = Cat:new()
a:run()
c:run()
c:eat()
c:speak()

local c1 = Cat:new()
c1:super().name = "cc"
c1.name = "kk"
c:speak()
c1:speak()
```
