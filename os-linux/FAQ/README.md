# 雙系統 windows 時間錯誤
windows 默認 將硬件時間作為 本地時間

linux 默認將 硬件時間作為 utc 時間

將兩個系統 解讀硬件時間設置到一隻即可

修改 windows 註冊表
```
#info=false
HKEY_LOCAL_MACHINE/SYSTEM/CurrentControlSet/Control/TimeZoneInformation/
```
添加項目 RealTimeIsUniversal 類型 REG_QDWORD 值為1即可

修改linux
```
#info=false
# 将硬件時間作為 本地時間
timedatectl set-local-rtc 1 --adjust-system-clock
```