# 錯誤處理

可以使用 **?** 獲取上條指令的執行結果 如果非 0 則出現錯誤

```
#!/usr/bin/env bash

function checkError(){
    local code=$?
    if [[ $code != 0 ]];then
        echo "exit $code"
        exit $code
    fi
}
function error(){
    return 1
}
error
checkError
```

# set -e -x

* set -e 可以讓 bash 自動檢查 命令執行結果 如果非0 則 退出腳本
* set -x 可以打印 bash 執行詳情 通常用於調試
* set +X 取消某個同名的  -X 設定

可以使用 $- 變量來獲取設置的標記
```
if [[ $- == *e* ]];then
		echo "already set -e"
fi
```