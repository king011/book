# 重寫運算符

dart 允許 重寫運算符 

# == 和 hashCode
dart 默認 == 比較的 是否指向同個實例 如果要比較實例存儲的值 需要重寫 ==

對於 Set/Map 實現使用的 hashCode 查找 元素 如果多個 此 hashCode 存在 多個值 再使用 == 過濾 所以 通常 對於 重寫了== 的 class 應該重寫 hashCode 使 兩個 == 的 實例 返回i相同的 hashCode

如果 要比較 變量是否 指向同個 實例 應該使用 identical

```
class Point {
  int x;
  int y;
  Point({this.x, this.y});

  operator ==(other) {
    if (other is! Point) {
      return false;
    }
    return x == other.x && y == other.y;
  }

  int get hashCode {
    return x.hashCode ^ y.hashCode;
  }
}

void main() {
  final p0 = Point(x: 19, y: 89);
  final p1 = Point(x: 19, y: 89);
  final p2 = Point(x: 89, y: 19);
  final p = p0;
  print("p0 == p1 ${p0 == p1}"); // true
  print("p0 == p2 ${p0 == p2}"); // false
  print("identical(p0,p) ${identical(p0, p)}"); // true
  print("identical(p0,p1) ${identical(p0, p1)}"); // false
  print("identical(p0,p2) ${identical(p0, p2)}"); // false
  print("p0.hashCode ${p0.hashCode}"); // 74
  print("p1.hashCode ${p1.hashCode}"); // 74
  print("p2.hashCode ${p2.hashCode}"); // 74

  final m = {
    p0: "p0",
    p1: "p1",
    p2: "p2",
  };
  print(m[p0]); // p1
  print(m[p1]); // p1
  print(m[p2]); // p2
}
```

# clone
dart 沒有 重載的 = 使用 = 直接 淺拷貝 通常習慣使用 clone 來 創建深拷貝

```
class Point {
  int x;
  int y;
  Point({this.x, this.y});

  operator ==(other) {
    if (other is! Point) {
      return false;
    }
    return x == other.x && y == other.y;
  }

  int get hashCode {
    return x.hashCode ^ y.hashCode;
  }

  Point clone() {
    return Point(x: x, y: y);
  }
}

void main() {
  final p0 = Point(x: 19, y: 89);
  final p1 = null;

  var p2 = p0?.clone();
  print(identical(p0, p2)); // false
  print(p0 == p2); // true

  p2 = p1?.clone();
  print(identical(p1, p2)); // true 都是 null
  print(p1 == p2); // true
}
```