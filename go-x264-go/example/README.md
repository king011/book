# example

```
go get github.com/gen2brain/x264-go
```

```
package main

import (
	"image"
	_ "image/jpeg"
	"os"

	"github.com/gen2brain/x264-go"
)

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}
func loadImage(name string) image.Image {
	r, e := os.Open(name)
	checkError(e)

	img, _, e := image.Decode(r)
	r.Close()
	checkError(e)
	return img
}
func main() {
	// 編碼後的輸出目標
	w, e := os.Create("example.264")
	checkError(e)
	defer w.Close()

	// 創建 編碼器
	enc, e := x264.NewEncoder(w, &x264.Options{
		Width:     640,
		Height:    480,
		FrameRate: 25,
		Preset:    "veryfast",
		Tune:      "stillimage",
		Profile:   "baseline",
		LogLevel:  x264.LogDebug,
	})
	checkError(e)
	defer enc.Close()

	// 編碼數據源
	e = enc.Encode(loadImage(`a.jpg`))
	checkError(e)
	e = enc.Encode(loadImage(`b.jpg`))
	checkError(e)

	// 刷新緩存
	e = enc.Flush()
	checkError(e)
}
```

# 錄製屏幕
```
go get github.com/kbinani/screenshot
```

```
package main

import (
	"fmt"
	_ "image/jpeg"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gen2brain/x264-go"
	"github.com/kbinani/screenshot"
)

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	// 編碼後的輸出目標
	w, e := os.Create("example.264")
	checkError(e)
	defer w.Close()

	// 獲取屏幕尺寸
	bounds := screenshot.GetDisplayBounds(0)
	fmt.Println(bounds.Dx(), bounds.Dy())
	// 創建 編碼器
	enc, e := x264.NewEncoder(w, &x264.Options{
		Width:     bounds.Dx(),
		Height:    bounds.Dy(),
		FrameRate: 25,
		Tune:      "zerolatency",
		Preset:    "ultrafast",
		Profile:   "baseline",
		LogLevel:  x264.LogError,
	})
	checkError(e)
	defer enc.Close()

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	ticker := time.NewTicker(time.Second / time.Duration(25))

	start := time.Now()
	frame := 0

	for {
		select {
		case <-exit: //退出錄製
			e = enc.Flush()
			checkError(e)
			e = w.Close()
			checkError(e)
			log.Println(`exit`)
			return
		case <-ticker.C: // 錄製屏幕
			frame++
			log.Printf("frame: %v", frame)
			img, err := screenshot.CaptureRect(bounds)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err.Error())
				continue
			}

			e = enc.Encode(img)
			checkError(e)
			log.Printf("t: %v\n", time.Since(start))
			start = time.Now()
		}
	}
}
```