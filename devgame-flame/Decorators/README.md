# [Decorators](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html)

裝飾器是可以封裝某些視覺效果然後將這些視覺效果應用於一系列畫布繪製操作的類別。 裝飾器不是組件，但它們可以手動或透過 HasDecorator mixin 應用於組件。 同樣，裝飾器不是效果，儘管它們可用於實現某些效果。

Flame 中有一定數量的裝飾器可用，如果需要，添加自己的裝飾器也很簡單。 一旦 Flutter 在網路上完全支援基於著色器的裝飾器，我們計劃添加它們。

# [Flame built-in decorators](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#flame-built-in-decorators)

## [PaintDecorator.blur](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#paintdecorator-blur)

此裝飾器對底層組件套用高斯模糊。 X 和 Y 方向的模糊量可能不同，儘管這種情況並不常見。

```
final decorator = PaintDecorator.blur(3.0);
```

可能的用途：

* 柔和的陰影；
* 距離相機較遠或非常近的「失焦」物體；
* 運動模糊效果；
* 顯示彈出對話框時淡化/模糊內容；
* 角色喝醉時視力模糊。


## [PaintDecorator.grayscale](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#paintdecorator-grayscale)

裝飾器將底層影像轉換為灰色陰影，就好像它是一張黑白照片一樣。 此外，您還可以使影像半透明至所需的不透明度等級。

```
final decorator = PaintDecorator.grayscale(opacity: 0.5);
```

可能的用途：

* 向 NPC 申請，將他們變成石頭，或變成幽靈！
* 適用於某個場景，表示它是對過去的記憶；
* 黑白照片。

## [PaintDecorator.tint](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#paintdecorator-tint)

該裝飾器使用指定的顏色對底層圖像進行著色，就像透過有色玻璃觀看它一樣。 建議這個裝飾器使用的顏色是半透明的，這樣你就可以看到下圖的細節。

```
final decorator = PaintDecorator.tint(const Color(0xAAFF0000);
```

可能的用途：
* 受某些類型魔法影響的 NPC；
* 陰影中的物品/角色可以染成黑色；
* 將場景染成紅色以顯示嗜血，或角色生命值較低；
* 呈綠色表示角色中毒或生病；
* 在夜間將場景染成深藍色；

## [Rotate3DDecorator](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#rotate3ddecorator)

此裝飾器將 3D 旋轉應用於底層組件。 您可以指定旋轉角度、樞軸點和要套用的透視變形量。

裝飾器還提供 isFlipped 屬性，該屬性可讓您確定當前是從正面還是從背面查看組件。 如果您想要繪製正面和背面外觀不同的組件，這非常有用。

```
final decorator = Rotate3DDecorator(
  center: component.center,
  angleX: rotationAngle,
  perspective: 0.002,
);
```

可能的用途：
* 一張可以翻轉的卡片；
* 書中的頁數；
* 應用程式路由之間的轉換；
* 3d 掉落的顆粒，如雪花或樹葉。

## [Shadow3DDecorator](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#shadow3ddecorator)

此裝飾器在元件下方渲染陰影，就好像元件是站在平面上的 3D 物件一樣。 此效果最適合使用等距相機投影的遊戲。

此生成器產生的陰影非常靈活：您可以控制其角度、長度、不透明度、模糊等。有關該裝飾器具有哪些屬性及其含義的完整描述，請參閱類別文件。

```
final decorator = Shadow3DDecorator(
  base: Vector2(100, 150),
  angle: -1.4,
  xShift: 200,
  yScale: 1.5,
  opacity: 0.5,
  blur: 1.5,
);
```

此裝飾器的主要目的是為組件添加地面上的陰影。 主要限制是陰影是平坦的並且無法與環境互動。 例如，此裝飾器無法處理落在牆壁或其他垂直結構上的陰影。

# [Using decorators](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#using-decorators)

## [HasDecorator mixin](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#hasdecorator-mixin)

此組件混合添加了裝飾器屬性，該屬性最初為空。 如果將此屬性設為實際的裝飾器對象，則該裝飾器將在組件渲染期間套用其視覺效果。 為了消除這種視覺效果，只需將裝飾器屬性設為 null 即可。

## [PositionComponent](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#positioncomponent)

PositionComponent（以及所有衍生類別）已經具有裝飾器屬性，因此對於這些元件，不需要 HasDecorator mixin。

事實上，PositionComponent 使用其裝飾器來在螢幕上正確定位組件。 因此，您想要套用於 PositionComponent 的任何新裝飾器都需要連結（請參閱下面的多個裝飾器部分）。

如果您想為元件在螢幕上的定位方式建立替代邏輯，也可以取代 PositionComponent 的根裝飾器。

## [Multiple decorators](https://docs.flame-engine.org/1.16.0/flame/rendering/decorators.html#multiple-decorators)

可以將多個裝飾器同時套用到同一個元件：Decorator 類別支援連結。 也就是說，如果元件上有一個現有的裝飾器，並且您想要添加另一個裝飾器，那麼您可以呼叫component.decorator.addLast(newDecorator) – 這將在現有鏈的末尾添加新的裝飾器。 方法removeLast()可以稍後刪除該裝飾器。

多個裝飾器可以以這種方式連結。 例如，如果 A 是初始裝飾器，則 A.addLast(B) 後面可以跟 A.addLast(C) 或 B.addLast(C) – 在這兩種情況下，鏈 A -> B -> C 將被創建。 實際上，這意味著可以從根（通常是 component.decorator）開始操作整個鏈。