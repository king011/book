# [DateTime](https://moment.github.io/luxon/#/tour)

Luxon 中最重要的 class 是 DateTime。DateTime 表示特定的毫秒時間以及時區和語言環境

下面的例子表示的本地時間的 2017年5月15日8點30分鐘

```
import { DateTime } from "luxon";

const dt = DateTime.local(2017, 5, 15, 8, 30)
console.log(dt)
```

local 接受任意數據的參數最多到毫秒，此外 Luxon 還提供了多種方法來創建 DateTime

## now

如果要獲取當前時間則

```
DateTime.now()
```

實際上 DateTime.now() 是 DateTime.local() 的語法糖

## fromObject

你也可以從一個 object 創建 DateTime，與 DateTime 相同的屬性將被設置未指定的屬性將採用默認值。年份和月份默認值爲當前值，分鐘 秒 毫秒 默認爲 0

```
dt = DateTime.fromObject({day: 22, hour: 12 }, { zone: 'America/Los_Angeles', numberingSystem: 'beng'})
```

## fromISO

Luxon 包含很多解析功能，但最重要的是解析 [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) 字符串，因爲它們或多或少是日期和時間的標準格式

```
DateTime.fromISO("2017-05-15")          //=> May 15, 2017 at midnight
DateTime.fromISO("2017-05-15T08:30:00") //=> May 15, 2017 at 8:30
```

此外你可以參考[官方文檔](https://moment.github.io/luxon/#/parsing) 了解更多的解析格式以及自定義格式

# 使用 DateTimes

現在我們已經能夠創建 DateTimes 了，接下來看下如何使用它

## toString

toString 函數將其轉化爲 [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) 字符串

```
DateTime.now().toString(); //=> '2017-09-14T03:20:34.091-04:00'
```

## 獲取屬性

我們可以通過 getter 單獨獲取時間的組成部分。例如：

```
dt = DateTime.now();
dt.year     //=> 2017
dt.month    //=> 9
dt.day      //=> 14
dt.second   //=> 47
dt.weekday  //=> 4
```

其它的一些有趣的訪問器

```
dt.zoneName     //=> 'America/New_York'
dt.offset       //=> -240
dt.daysInMonth  //=> 30
```

## 格式化日期時間

你可能希望將 DateTime 輸出爲字符串以供機器或人類閱讀。Luxon 有很多工具可以做到這點，但其中最重要的是 toLocaleString 和 toISO 函數。

如果要格式化人類可讀的字符串，請使用 toLocaleString

```
dt.toLocaleString()      //=> '9/14/2017'
dt.toLocaleString(DateTime.DATETIME_MED) //=> 'September 14, 3:21 AM'
```

這在不同的語言環境中都能很好的工作。

如果你向讓另外一個程序讀取字符串，推薦使用 toISO:

```
dt.toISO() //=> '2017-09-14T03:21:47.070-04:00'
```

> toISO 是 toString 的語法糖，但 toISO 這個函數名更清除的表示了輸出個爲 ISO 8601


還支持一些自定義格式。你可以參考[官方文檔](https://moment.github.io/luxon/#/formatting)

# 改變 DateTime

DateTime 是不可變的，這意味着你不能修改它們，只能創建改變後的副本。

官方文檔或函數名可能會使用 "alter" "change" "set" 等詞彙，其意思都是創建具有不同屬性的新實例。

## 數據計算

```
import { DateTime } from "luxon";

var dt = DateTime.now();
dt.plus({ hours: 3, minutes: 2 }); // datetime + duration
dt.minus({ days: 7 }); // datetime - duration
dt.startOf('day'); // 返回當天開始
dt.endOf('hour'); // 返回當前小時的最後1分鐘(59)
```

## 設置

你可以通過 set 函數來直接創建覆蓋了指定屬性的新副本

```
var dt = DateTime.now();
dt.set({hour: 3}).hour   //=> 3
```

# 比較
DateTime 實現了 valueOf 使用可以直接使用 < > ==  等符號進行比較

# Intl
Luxon 提供了幾種不同的 Intl 功能，但最重要的是格式化：

```
var dt = DateTime.now();
var f = {month: 'long', day: 'numeric'};
dt.setLocale('fr').toLocaleString(f)      //=> '14 septembre'
dt.setLocale('en-GB').toLocaleString(f)   //=> '14 September'
dt.setLocale('en-US').toLocaleString(f)  //=> 'September 14'
dt.setLocale('zh-Hant').toLocaleString(f)  //=> '9月14日'
```

class Info 可以列出不同地區的月份或星期

```
import { Info } from "luxon";

Info.months('long', {locale: 'fr'}) //=> [ 'janvier', 'février', 'mars', 'avril', ... ]

Info.months('long', { locale: 'zh-Hant' }) //=> [ '1月', '2月', '3月', '4月', ... ]
Info.weekdays('long', { locale: 'zh-Hant' }) //=> [ '星期一', '星期二', '星期三', '星期四', ... ]
Info.weekdays('short', { locale: 'zh-Hant' }) //=> [ '週一', '週二', '週三', '週四', ... ]
```

# 時區

Luxon 支持時區。關於它有很大的篇幅，但簡而言之，你可以在特定區域中創建 DateTimes 並更改其時區

```
DateTime.fromObject({}, {zone: 'America/Los_Angeles'}); // now, but expressed in LA's local time
DateTime.now().setZone("America/Los_Angeles"); // same
```

Luxon 也支持 UTC

```
DateTime.utc(2017, 5, 15);
DateTime.utc(); // now, in UTC time zone
DateTime.now().toUTC();
DateTime.utc().toLocal();
```

# Duration

Duration 表示時間段，你可以通過語義化的 Object 來創建它

```
var dur = Duration.fromObject({ hours: 2, minutes: 7 });
```

然後可以在 DateTime 中通過各種 Math 函數進行計算

```
dt.plus(dur);
```

它們也有類似 Datetime 的 getters
```
dur.hours   //=> 2
dur.minutes //=> 7
dur.seconds //=> 0
```

和其它一些有趣的函數

```
dur.as('seconds') //=> 7620
dur.toObject()    //=> { hours: 2, minutes: 7 }
dur.toISO()       //=> 'PT2H7M'
```

# Intervals 日期段

Interval 是特定的日期時間段，例如“從現在到午夜”。它們實際上是形成端點的兩個 DateTime 的包裝器。你可以使用它們執行的操作：

```
now = DateTime.now();
later = DateTime.local(2020, 10, 12);
i = Interval.fromDateTimes(now, later);

i.length()                             //=> 97098768468
i.length('years')                //=> 3.0762420239726027
i.contains(DateTime.local(2019))       //=> true

i.toISO()       //=> '2017-09-14T04:07:11.532-04:00/2020-10-12T00:00:00.000-04:00'
i.toString()    //=> '[2017-09-14T04:07:11.532-04:00 – 2020-10-12T00:00:00.000-04:00)
```