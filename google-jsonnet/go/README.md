# go
google 官方對 jsonnet 提供了 golang 的支持  
需要 go1.8 及以上版本

源碼 [https://github.com/google/go-jsonnet](https://github.com/google/go-jsonnet)

```bash
go get github.com/google/go-jsonnet
```

# Example
```
#info="main.jsonnet"
//支持 單行註解
//支持 變量 定義
local version = "1.0.0";
local show = false;
local obj = {
    //使用 變量
    Version:version,
};
{
    /*
        支持多行註解
    */
    Person0:{
        //屬性名 可選擇 使用 ' " 擴起來 或 忽略
        Name:"Kate",
        "Full Name": self.Name +" beckinsale", //self 可調用 當前 屬性 別進行簡單的 運算
        'Lv':10,
        Array:[1,2,3],
        //最後的 , 可以選擇是否添加
        Who:"my love",
    },
    //複製一份配置
    Person1:self.Person0,
    //複製一份配置 並且 客製化
    Person2:self.Person0{
        //覆蓋 屬性 
        Name:"林青霞", //Full Name 也會變化
        //擴展屬性
        New:"新屬性",
    },
    Obj:obj,
    Description:|||
        支持多行
        文本
        \會被保留
    |||,
    File:@'\單行不轉義使用@和''輸出兩個''到json變爲一個',
}
```

```go
#info="main.go"
package main

import (
	"fmt"
	"log"

	"github.com/google/go-jsonnet"
)

func main() {
	// 初始化 解析器
	vm := jsonnet.MakeVM()

	// 解析 檔案
	filename := `main.jsonnet`
	str, e := vm.EvaluateFile(filename) // 	vm.EvaluateSnippet(filename, `{}`)
	if e != nil {
		log.Fatalln(e)
	}

	// 輸出 json
	fmt.Print(str)
}
```