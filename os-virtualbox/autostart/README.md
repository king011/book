# 開機啓動

如果使用 virtualbox 來搭建服務，開機自動啓動就必不可少，virtualbox本身沒有支持開機自動運行，但各個平臺可以利用系統工具來滿足這一需求

# linux systemd

對於支持 systemd 的 linux 系統可以很好的滿足需求，創建一個服務檔案到 **/etc/systemd/system/** 檔案夾下

```
#info="example.service"

[Unit]
Description=$MY_VM_NAME
After=network.target $VIRTUALBOX_SRV_NAME
Before=runlevel2.target shutdown.target

[Service]
User=$USERNAME
Group=$USERGROUP
Type=forking
Restart=no
TimeoutSec=5min
IgnoreSIGPIPE=no
KillMode=process
GuessMainPID=no
RemainAfterExit=yes

ExecStart=/usr/bin/VBoxManage startvm "$MY_VM_NAME" --type headless
ExecStop=/usr/bin/VBoxManage controlvm "$MY_VM_NAME" acpipowerbutton

[Install]
WantedBy=multi-user.target
```

上面有 4 個參數需要依據你的真實情況進行改寫

1. **$MY\_VM\_NAME** 這在 ExecStart/ExecStop 中用於指定要啓動的虛擬機名稱
2. **$VIRTUALBOX\_SRV\_NAME** virtualbox 驅動載入的服務名稱，不同的發佈平臺名稱不同(virtualbox.service 或者 vboxdrv.service)，可以使用 **ls /lib/systemd/system/ | egrep &#39;\(virtualbox\)|\(vbox\)&#39;**  進行查詢
3. **$USERNAME** 管理虛擬機的用戶名稱
4. **$USERGROUP** 管理虛擬機的用戶所屬群組
