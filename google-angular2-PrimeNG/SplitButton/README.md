# [SplitButton](https://primeng.org/splitbutton)

```
import {SplitButtonModule} from 'primeng/splitbutton';
```
SplitButton 將一組命令疊加在一個下拉菜單中供選擇，非下拉菜單則作爲默認按鈕命令

```
<p-splitButton label="Save" icon="pi pi-check" (onClick)="save()" [model]="items"></p-splitButton>
```

![](assets/1.png)

## Animation Configuration

**showTransitionOptions/hideTransitionOptions** 定義菜單顯示和隱藏的過渡動畫，下面的例子將完全禁用過渡動畫

```
<p-splitButton [showTransitionOptions]="'0ms'" [hideTransitionOptions]="'0ms'" label="Save" icon="pi pi-check" (onClick)="save()" [model]="items"></p-splitButton>
```

## MenuModel API

SplitButton 使用 menumodel api 來定義其條目，詳細信息請查看 [MenuModel API](https://primeng.org/menumodel)


## Severity

你可以爲按鈕指定不同的等級以顯示不同的顏色

* .p-button-secondary
* .p-button-success
* .p-button-info
* .p-button-warning
* .p-button-help
* .p-button-danger

```
<p-splitButton label="Save" icon="pi pi-check" [model]="items" styleClass="p-button-warning"></p-splitButton>
```

## Raised and Rounded Buttons

使用 class **p-button-raised** 來指定凸起按鈕，使用 class **p-button-rounded** 來指定圓角按鈕

```
<p-splitButton label="Save" icon="pi pi-check" [model]="items" styleClass="p-button-raised p-button-rounded"></p-splitButton>
```
# Events

| Name | Parameters | Description |
| -------- | -------- | -------- |
| onClick     | event: browser event     | 點擊按鈕回調     |
| onDropdownClick     | event: browser event     | 單擊下拉按鈕回調     |

# Properties

| Name | Type | Default | Description |
| -------- | -------- | -------- | -------- |
| label     | string     | null     | 按鈕文本     |
| icon     | string     | null     | 按鈕圖標     |
| iconPos     | string     | left     | 圖標在按鈕中的位置: left right     |
| style     | string     | null     | 組件內聯的 css style     |
| styleClass     | string     | null     | 組件的 css class     |
| menuStyle     | string     | null     | overlay 菜單的內聯 css style     |
| menuStyleClass     | string     | null     | overlay 菜單的 css class      |
| appendTo     | any     | null     | Target element to attach the overlay, valid values are "body" or a local ng-template variable of another element (note: use binding with brackets for template variables, e.g. [appendTo]="mydiv" for a div element having #mydiv as variable name).     |
| disabled     | boolean     | false     | 如果爲 true 則禁用菜單     |
| tabindex     | number     | null     | tab 索引     |
| dir     | string     | null     | 指示元素的方向     |
| showTransitionOptions     | string     | 225ms ease-out     | 菜單顯示的過渡動畫     |
| hideTransitionOptions     | string     | 195ms ease-in     | 菜單隱藏的過渡動畫     |
| expandAriaLabel     | string     | null     | 按鈕的 aria label     |

# Templates

| Name | Parameters |
| -------- | -------- |
| content     | -     |


# Styling

| Name | Element |
| -------- | -------- |
| p-splitbutton     | Container element.     |
| p-splitbutton-menubutton     | Dropdown button.     |
| p-menu     | Overlay menu.     |
