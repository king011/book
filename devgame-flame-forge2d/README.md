# forge2d

flame 提供了 flame\_forge2d 包用於實現物理引擎

```
flutter pub add flame_forge2d
```

```
dependencies:
  flame_forge2d: ^0.17.0
```

```
import 'package:flame_forge2d/flame_forge2d.dart';
```

* pub 倉庫 [https://pub.dev/packages/flame_forge2d](https://pub.dev/packages/flame_forge2d)
* 源碼 [https://github.com/flame-engine/flame/tree/main/packages/flame_forge2d](https://github.com/flame-engine/flame/tree/main/packages/flame_forge2d)