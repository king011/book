# [Sub Process](https://deno.land/api@v1.28.2?s=Deno.run)

Deno.run 函數提供了子進程的支持其接受一個 RunOptions 參數用於指定子進行信息

```
export interface RunOptions {
	// 子進程啓動的命令數組
	cmd: readonly string[] | [string | URL, ...string[]];
	// 子進程的工作路徑
	cwd?: string;
	// 子進行 環境變量 key-value 的 Map
	env?: Record<string, string>;
	// 子進程 stdout 默認繼承自父進程
	// * number 檔案資源 id, resource ID (_rid_) of an open file
	// * "inherit" 這是默認值，從父進程繼承
	// * "piped" 安排一個新的管道來連接父子進程
	// * "null" 流被忽略，相當於將流輸出到 /dev/nul
	stdout?: "inherit" | "piped" | "null" | number;
	stderr?: "inherit" | "piped" | "null" | number;
	stdin?: "inherit" | "piped" | "null" | number;
}
```

```
const p = Deno.run({
  cmd: [
    "echo",
    "1",
    "2",
  ],
});
// 等待子進程完成
const status = await p.status();
console.log(status.code, status.success);
// 清理子進程資源
await p.close();
```


# piped

使用 piped 可以將子進程輸出和父進程輸出進行隔離

```
const p = Deno.run({
  cmd: ["echo", "hello world"],
  stderr: "piped",
  stdout: "piped",
});
const [status, stdout, stderr] = await Promise.all([
  p.status(),
  p.output(),
  p.stderrOutput(),
]);
p.close();
```