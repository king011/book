# 語法簡介

1. cmake 由 ****(command) 和註解組成
2. 使用 **#** 作爲行開頭 註釋單行
3. command 使用 **()** 傳遞參數
4. 指令名稱 忽略大小寫
5. 變量只有 字符串 串列 兩種 區分大小寫(內建變量 全部大寫)

## 指令
```cmake
# 一個輸出 helloworld 的指令
message(helloworld)

# 打印錯誤 並且 終止 cmake繼續執行
message(FATAL_ERROR  "錯誤 測試")
```

## 變量
set(xxxName xxxVal) 設置變量
一般使用　${xxxName} 獲取變量

```cmake
set(var hello)
set(foo var)
 
message(${foo})
message(${${foo}})
```

## 字串與串列

將字串用 空白 或 ; 分隔表示串列
set(foo this is a list)
set(foo this;is;a;list)
上面兩個指令 完全 相同 將 foo 設置爲 [this,is,a,list] 的串列

如果需要包含 空格 ; 的字符串 可以使用 "" 將其擴起來
"" 中可以 使用 ${xxx} 類似bash取變量值
"" 中 的轉義符號 基本同 c

```cmake
set(name kate)
set(what "this is ${name}")
message(${what})
```

## boolean
cmake 中 字符串可以被 賦予 boolean (忽略大小寫)

以下認爲是 FALSE

* OFF
* FALSE
* N
* NO
* 0
* ""
* 沒指定值的變量
* NOTFOUND
* 以 -NOTFOUND 結尾的字串

以下認爲是 TRUE
* ON
* TRUE
* Y
* YE
* YES
* 1
* 其它不爲 FALSE 的字串

## 數學計算

由於 CMake 當中並沒有提供直接的數學運算子，所有的符號組合最終都會形成字串或串列。數學計算必須透過 math 指令解釋：

```cmake
math(EXPR var "1 + 2 * 3")
message("var = ${var}")
```

