# [docker](https://hub.docker.com/_/ubuntu)

docker 官方爲 ubuntu 提供了鏡像，ubuntu 鏡像可能是被使用的最多的鏡像之一，因爲 docker 的分層構建這意味着使用 ubuntu 作爲基礎鏡像能夠最大的減少整個 images 的體積

# tzdata

默認鏡像沒有安裝 tzdata，安裝後可以爲鏡像指定環境變量 TZ 來設定時區

```
apt install tzdata
```

```
TZ=Asia/Shanghai
```

# utf8
如果需要使用非 ascii 字符，通常需要定義如下環境變量

```
LANG=C.UTF-8
LC_ALL=C.UTF-8
```

# gosu

gosu 可以用於在容器中切換運行的子進程的 用戶，與 sudo 不同的是它不會使子進程使用一個新的pid，所以可以用它來使用 容器的後臺進程的pid保持爲 1
