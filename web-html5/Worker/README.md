# Worker

Web Worker 爲 js 提供了一個標準的多線程環境，你可以將一段 js 代碼放到單獨的檔案中，然後以 new Worker 來讓它在一個單獨的線程中執行

```
var worker = new Worker('work.js');
```

# 通信

在主線程和 worker 中可以使用 onmessage 和 postMessage 來互相傳遞與接收消息

```
const w = new Worker("worker.js", {
  type: "module",
});
w.onmessage = (evt) => {
  console.log(evt.data);
};
for (let i = 0; i < 10; i++) {
  w.postMessage(i);
}
```

```
#info="worker.js"

declare const self: {
  name: string;
  onmessage?: (e: MessageEvent) => void;
  close(): void;
  postMessage(message: any, transfer: Transferable[]): void;
  postMessage(message: any, options?: StructuredSerializeOptions): void;
};

self.onmessage = (evt: MessageEvent) => {
  self.postMessage(evt.data);
  if (evt.data == 9) {
    self.close();
  }
};
```

你也可以不用寫 self， worker 中的全局 this 本身就指向了 self

```
onmessage = (evt: MessageEvent) => {
  postMessage(evt.data);
  if (evt.data == 9) {
    close();
  }
};
```

# 傳二進制數據

主線程和 worker 間可以傳遞 二進制數據比如 File Blob ArrayBuffer 等，但會傳遞一個副本。這可能會引發性能問題，對此，可以使用 [Transferable Objects](https://www.w3.org/html/wg/drafts/html/master/infrastructure.html#transferable-objects),它直接將數據遞交給 worker，但此後主線程將不能再訪問 數據

```
worker.postMessage(arrayBuffer, [arrayBuffer]);
```
