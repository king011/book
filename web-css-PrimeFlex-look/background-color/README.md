# [background-color](https://www.primefaces.org/primeflex/backgroundcolor)

PrimeFlex 提供了很多背景顏色的 class，具體的顏色值由 theme 提供，顏色值太多這裏沒有列舉，你可以[在此](https://www.primefaces.org/primeflex/backgroundcolor)查看可選值和顏色預覽

![](assets/example.png)

```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container">
        <div class="surface-500 text-white font-bold border-round m-2 flex align-items-center justify-content-center" style="min-width: 200px; min-height: 100px">surface-500</div>
        <div class="bg-cyan-500 text-white font-bold border-round m-2 flex align-items-center justify-content-center" style="min-width: 200px; min-height: 100px">bg-cyan-500</div>
        <div class="bg-orange-500 text-white font-bold border-round m-2 flex align-items-center justify-content-center" style="min-width: 200px; min-height: 100px">bg-orange-500</div>
    </div>
</div>
```

## Pseudo States
你可以爲僞狀態在 color class 名稱前添加同名的前綴例如 focus: hover: active: 來爲這些狀態指定背景顏色

```
<div class="card">
    <div class="flex flex-wrap md:justify-content-between justify-content-center card-container">
        <div class="hover:surface-700 surface-500 text-white font-bold border-round m-2 flex align-items-center justify-content-center"
            style="min-width: 200px; min-height: 100px">hover:surface-700</div>
        <div class="hover:bg-cyan-700 bg-cyan-500 text-white font-bold border-round m-2 flex align-items-center justify-content-center"
            style="min-width: 200px; min-height: 100px">hover:bg-cyan-700</div>
        <div class="hover:bg-orange-700 bg-orange-500 text-white font-bold border-round m-2 flex align-items-center justify-content-center"
            style="min-width: 200px; min-height: 100px">hover:bg-orange-700</div>
    </div>
</div>
```

## Named Theme Color

每個 theme 都提供了建議用於某些表面的背景顏色，這些顏色被導出爲 css 變量，可以直接使用下述 class 來設置


| class | 建議 |
| -------- | -------- |
| surface-ground     | 地面顏色，例如 body 的背景色     |
| surface-section     | 內容部分的背景，例如一段文本的背景     |
| surface-card     | card 卡片的背景     |
| surface-overlay     | 覆蓋物的背景，比如模式對話框時將 body 背景色設置爲此值     |
| surface-border     | 分割內容的邊框顏色     |
| surface-hover     | 懸停狀態下的元素背景色     |

```
<div class="card surface-card hover:surface-hover border-round-md p-3" style="height: 400px;">
    <h2>這是一個 card</h2>
    <p class="surface-section border-round-sm p-2">
        card 被設置了 surface-card hover:surface-hover border-round
    </p>
</div>
```

