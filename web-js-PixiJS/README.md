# PixiJS

PixiJS 是一個開源 快速的 輕量級 2d 庫，可以在所有設備上運行。PixiJS 使用 WebGL 提供了硬件加速。

```
yarn add pixi.js@5.3.10
```

* 官網 [https://www.pixijs.com/](https://www.pixijs.com/)
* 源碼 [https://github.com/pixijs/pixijs](https://github.com/pixijs/pixijs)
* 文檔 [https://github.com/kittykatattack/learningPixi](https://github.com/kittykatattack/learningPixi)