# 獲取 執行檔 全路徑

```
package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func main() {
	path, e := exec.LookPath(os.Args[0])
	if e != nil {
		log.Fatalln(e)
	}
	path, e = filepath.Abs(path)
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println("exec :", path)
	fmt.Println("name :", filepath.Base(path))
	fmt.Println("ext  :", filepath.Ext(path))
	fmt.Println("dir  :", filepath.Dir(path))
}
```

ext 是帶了 **.** 的 比如 **.exe**
 