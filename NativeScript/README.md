# NativeScript

NativeScript 是2014年發佈的 一個 以 js 構建 android/ios 程式的 開源(Apache-2.0) 項目

* 官網 [http://www.nativescript.org/](http://www.nativescript.org/)
* 源碼 [https://github.com/NativeScript/NativeScript](https://github.com/NativeScript/NativeScript)

# 安裝

1. 安裝 nodejs

	```
	#info=false
	sudo apt install nodejs
	```
	
1. 安裝 NativeScript CLI

	```
	#info=false
	sudo npm install -g nativescript
	```

1. 在手機上安裝 **NativeScript Playground**