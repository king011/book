# adb

可以打開電視的開發者模式和調試功能，然後使用 adb 進行安裝

```
# 連接 局域網內的 android tv
adb connect 192.168.xxx.xxx

# 使用 adb 命令進行安裝
adb install -r xxx.apk
```