# [安裝](https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-angular-app-with-ngx-translate-new)

首先安裝代碼

* **@ngx-translate/core** 這是 ngx-translate 的核心實現
* **@ngx-translate/http-loader** 從網路加載翻譯，不從網路加載可以不用安裝

```
npm install @ngx-translate/core @ngx-translate/http-loader
```

然後在 **app.module.ts** 中 import ngx-translate

```
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// 如果要支持 AOT 需要定義一個函數，而不能使用 內聯的匿名函數
export function HttpLoaderFactory(http: HttpClient, locationStrategy: LocationStrategy): TranslateHttpLoader {
  return new TranslateHttpLoader(http, `${locationStrategy.getBaseHref()}/assets/i18n/`, '.json')
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // ngx-translate and the loader module
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: "en",// 指定默認語言
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory, // 使用 http 加載語言包
        deps: [HttpClient, LocationStrategy]
      }
    }),

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

# 設置語言

你可以註入 TranslateService 來設置默認語言和選擇應用要顯示的語言

```
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private translate: TranslateService) {
    translate.setDefaultLang("en")
    translate.use("en")
  }
}
```

你應該會看到瀏覽器報告 沒有找到 **/assets/i18n/en.json** 檔案，這是因爲 TranslateHttpLoader 默認從 **/assets/i18n/** 檔案進下加載 json 語言包

# 翻譯檔案

ngx-translate 使用 json 作爲翻譯檔案，它支持扁平化的定義

```
{
  "demo.title": "Translation demo",
  "demo.text": "This is a simple demonstration app for ngx-translate"
}
```

或者是嵌入式的定義

```
{
    "demo": {
        "title": "Translation demo",
        "text": "This is a simple demonstration app for ngx-translate"
    }
}
```

推薦使用嵌入式的定義方式，因爲它更具可讀性(注意不能混用兩種寫法)

# 使用翻譯

你可以在 html template 中使用 translate 管道來顯示翻譯內容，它也可以支持一個 Object 作爲翻譯的參數

```
<!--  使用管道來顯示翻譯 -->
<h2>{{'demo.title' | translate}}</h2>
<!-- 爲翻譯傳入參數 -->
<p>{{ 'demo.greeting' | translate:{'name':'Andreas'} }}</p>
```

```
{
    "demo": {
        "title": "Translation demo",
        "greeting": "Hello {{name}}!"
    }
}
```

TranslateService 服務提供來兩個方法用於在 ts 中使用翻譯

get 返回一個可觀察對象，它將在語言包加載完成後返回翻譯內容
```
translate.get('demo.greeting', {name: 'John'}).subscribe((res: string) => {
    console.log(res);
});
```

如果你 100% 確定語言包已經加載完成（比如通過靜態加載而不是 http 加載），可以使用 instant 返回翻譯內容

```
console.log(translate.instant('demo.greeting', {name: 'John'}));
```

你還可以使用 stream 方法來獲取翻譯內容，它返回一個可觀察對象，在語言包準備初次就緒和切換時返回翻譯內容

# 瀏覽器默認語言

你可以調用 TranslateService 提供的 getBrowserLang() 返回瀏覽器默認語言(比如 en)

或者調用 getBrowserCultureLang()  返回帶區域的默認語言名稱(比如 en-US 或者 en-GB)

# 子模塊

要在子模塊中使用翻譯需要 import TranslateModule 即可

```
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
  ],
  ...
})
export class SharedModule { }
```

# 修復故障

使用 TranslateLoader 加載語言包，可能會短暫的看到 翻譯 id 而非翻譯文本(因爲語言包還沒加載完成)，對此的一個解決方案是 將默認的語言包 靜態打包到項目這樣將可能看到短暫的默認語言而非翻譯id

修改 tsconfig.app.json 添加設定(允許 import json 檔案)
```
"compilerOptions": {
    ...
    "resolveJsonModule": true,
    "esModuleInterop": true
    ...
},
```

導入 json 並將它設置爲 默認語言
```
import defaultLanguage from "./../assets/i18n/en.json";

constructor(private translate: TranslateService) {
    translate.setTranslation('en', defaultLanguage);
    translate.setDefaultLang('en');
}
```

或者如果你的語言包很小，也可以選擇將所有語言包都靜態加載到項目，如果這樣做就不需要指定 loader 了

```
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
// import ngx-translate and the http loader
import { TranslateModule } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,

    // ngx-translate
    TranslateModule.forRoot(),

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

 另外本喵想到的不用靜態打包語言包的做法是可以在 app.component 裏面等待 TranslateService 加載完語言包，在此之前都顯示一個初始化頁面或者乾脆將整個頁面都隱藏