# [屬性參數](http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html)

使用 property 裝飾器 來指定 一個 creator 屬性

```

@ccclass("i18n-label")
export class Label {
    @property({
        type: cc.Label,
        displayName: 'i18n label',
    })
    label: cc.Label = null
    @property({
        displayName: 'i18n key',
    })
    key = ''
}

@ccclass
export default class NewClass extends cc.Component {

    @property({
        type: [Label],
    })
    labels: Array<Label> = []
		
}
```