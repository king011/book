# 嵌入式

可以使用 cmake 爲嵌入式 進行交叉編譯

```
# 設置目標平臺
set(CMAKE_SYSTEM_NAME Linux)
# 設置 編譯工具鏈
set(TOOLCHAIN_PATH /home/king/c/mipsel-mt-linux-gnu)
set(CMAKE_C_COMPILER ${TOOLCHAIN_PATH}/bin/mipsel-mt-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/bin/mipsel-mt-linux-gnu-g++)
```

# Find

cmake 提供了 多種 FIND_xxx 查找依賴 但默認從 當前主機查找 對於 嵌入式交叉編譯 不適合

可以指定 CMAKE\_FIND\_ROOT\_PATH 來限定 查找位置

```
set(CMAKE_FIND_ROOT_PATH path1 path2 path3 ...)

# NEVER 設置 program 查找不受 CMAKE_FIND_ROOT_PATH 限制
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# ONLY 只從 root目錄 查找 library include
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
```

> 除了 NEVER 和 ONLY 還可以設置 BOTH 優先從 CMAKE_FIND_ROOT_PATH 查找