# magic

emby 將硬件加速從免費版本中剔除基本上是逼迫用戶放棄使用或者購買會員，這本無可厚非但它前期卻憑藉開源累計了用戶和社區貢獻，現在看起來這實在是過河拆橋的利用開源社區。並且硬件加速 emby 沒有做任何工作，它只是調用了 ffmpeg 和顯卡來完成這一切。是的你付錢了 emby 就在調用 ffmpeg 時傳入參數告訴 ffmpeg 使用硬件加速，否則就通知 ffmpeg 不要使用硬件加速，本喵不清楚 emby 收到的會員費是否會贊助 ffmpeg 但估計不會吧！順便一提 [ffmpeg](https://ffmpeg.org/) 也是一個開源項目，只是從開源項目中吸血這讓本喵看起來覺得很有 emby 的風格。

對於這種無恥行爲本來應該直接換用 [jellyfin](https://jellyfin.org/) 但目前(2023-12) jellyfin 確實還存在效率和一些奇怪的小問題，簡單來說沒有 emby 好用。除了向 emby 付費另外一個方法就是對 emby 使用魔法攻擊。

下文探討了如何施展魔法，請勿將其用於商業目的，那顯然是違法的，後文只是魔法師的純技術研討。

目前 emby 4.7.14 會向 https://mb3admin.com 請求下述三個接口：

* /admin/service/registration/validate
* /admin/service/registration/getStatus
* /admin/service/registration/validateDevice

這些接口用於驗證服務器的會員信息，如果我們可以控制這三個接口的返回內容則可以騙過 emby 服務器直接使用高級功能。例如自己搭建一個 nginx 服務來響應這三個接口

```
location = /admin/service/registration/validateDevice {
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

    add_header Content-Type "application/json;charset=utf-8";
    add_header Cache-Control "max-age=31536000";
    return 200 '{"cacheExpirationDays": 365,"message": "Device Valid","resultCode": "GOOD"}';
}
location = /admin/service/registration/getStatus {
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';
    
    add_header Content-Type "application/json;charset=utf-8";
    add_header Cache-Control "max-age=31536000";
    return 200 '{"deviceStatus":"0","planType":"Lifetime","subscriptions":{}}';
}
location = /admin/service/registration/validate {
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';
    
    add_header Content-Type "application/json;charset=utf-8";
    add_header Cache-Control "max-age=31536000";
    return 200 '{"featId":"MBSupporter","registered":true,"expDate":"2100-01-01","key":114514}';
}
```

emby 使用了 .net 開發，你可以使用 [dySpy](https://github.com/dnSpy/dnSpy/releases) 來直接反編譯修改裏面的代碼，讓 emby 向你的 nginx 服務器請求這三個接口。再次提醒，不要用於商業行爲，未經允許的反編譯串改代碼是違法行爲，本喵只是使用 dySpy 打開查看了 emby 的部分代碼以研究此魔法的可行性並未對它進行實際的串改

另外後續內容並非本喵獨自分析完成，參考了 [https://blog.peos.cn/2022/08/12/emby467-emby47.html](https://blog.peos.cn/2022/08/12/emby467-emby47.html) ，本喵只是使用 dySpy 查看了文章中提及的內容並重新整理成冊(並且認爲確實能夠行之有效的實施)。

# docker

總共需要修改五個檔案，建議使用 docker 來部署，因爲比較方便

```
services:
  emby:
    image: linuxserver/emby:4.7.14
    restart: always
    ports:
      - 8096:8096
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Asia/Shanghai
      - LANG=en_US.UTF-8
    devices:
      - /dev/dri:/dev/dri
    volumes:
      - ./4.7.14/connectionmanager.js:/app/emby/dashboard-ui/modules/emby-apiclient/connectionmanager.js:ro
      - ./4.7.14/embypremiere.js:/app/emby/dashboard-ui/embypremiere/embypremiere.js:ro
      - ./4.7.14/Emby.Server.Implementations.dll:/app/emby/Emby.Server.Implementations.dll:ro
      - ./4.7.14/Emby.Web.dll:/app/emby/Emby.Web.dll:ro
      - ./4.7.14/MediaBrowser.Model.dll:/app/emby/MediaBrowser.Model.dll:ro
      - ./config:/config
      - ./data:/data
```

1. **image** 使用了 linuxserver 打包的鏡像，請明確指定版本，因爲不同版本的 emby 可能使用的 dll 庫不同需要分別施展不同的魔法
2. **devices** 映射了linux 驅動，用於硬件加速，詳情請查看 [linuxserver](https://hub.docker.com/r/linuxserver/emby) 說明
3. **/config** 檔案夾保存了鏡像的設定和各種 emby 需要持久化的內容
4. **/data** 檔案夾用於將你的媒體映射到 docker，應該依據實際情況修改
5. **./4.7.14/** 檔案夾下的五個檔案假設已經被釋放了魔法，用於替換原本的檔案

運行服務器後在 /web/index.html#!/embypremiere 頁面隨便輸入一個會員註冊代碼,保存後重啓服務器即可
## connectionmanager.js

這個檔案位於 **/app/emby/dashboard\-ui/modules/emby\-apiclient/connectionmanager\.js**，打開後搜索 **https://mb3admin.com/admin/service/registration/validateDevice** ，並將域名改成你準備好的域名即可

## embypremiere.js

這個檔案位於 **/app/emby/dashboard\-ui/embypremiere/embypremiere\.js**，打開後搜索 **https://mb3admin.com/admin/service/registration/getStatus** ，並將域名改成你準備好的域名即可

## Emby.Server.Implementations.dll

使用 dySpy 打開 Emby.Server.Implementations.dll

找到 Emby.Server.Implementations.Security 包的 class PluginSecurityManager 中的 常量 MBValidateUrl，右擊常量 **MBValidateUrl** 後選擇**編輯字段**

![](assets/0-f.png)

![](assets/0-f1.png)

然後在下述窗口中將**值**中的**域名**改成你準備好的域名

![](assets/0-w.png)


找到 Emby.Server.Implementations.Security 包的 class PluginSecurityManager 將代碼由 C# 顯示爲 IL 在第 1391 行有個指令 **ldstr "https://mb3admin.com/admin/service/registration/validate"** 右擊選擇 **編輯IL指令**

![](assets/0-1f.png)

然後將 ldstr 加載的**域名** 改成你準備好的域名

![](assets/0-1w.png)

## Emby.Web.dll

使用 dySpy 打開 Emby.Web.dll

將 **Emby\.Web\.dashboard\_ui\.modules\.emby\_apiclient\.connectionmanager\.js** 資源內容複製(右鍵菜單後保存)一份後刪除(右鍵菜單後刪除)

![](assets/1-f.png)

在複製副本裏面搜索 **https://mb3admin.com/admin/service/registration/validateDevice** 將 **域名**改成你準備好的域名，並且重新添加(右鍵菜單後創建文件資源)

![](assets/1-w.png)

## MediaBrowser.Model.dll

使用 dySpy 打開 MediaBrowser.Model.dll

找到 MediaBrowser.Model.Entities 包 class PluginSecurityInfo 的 **get\_IsMBSupporter** 方法，右鍵菜單後編輯方法

![](assets/2-f.png)

將 17 行的 get 函數返回值直接改成 **return true**

![](assets/2-w.png)

# 中間人攻擊

上文已知 emby 會向 mb3admin.com 發送 https 請求來驗證授權，其實我們可以使用中間人攻擊來將請求攔截到自己的網站而不用更改 emby 代碼。但 https 證書阻止了這一操作，我們必須要有 mb3admin.com 的證書才能成功冒充 mb3admin.com，好在服務器完全受我們控制，我們可以自己簽發一個證書，之後只需要把證書 添加到系統的受信列表即可。

使用下述指令創建一個 100年有效期的自簽證書
```
openssl req -new -newkey rsa:2048 -nodes -keyout my.key -out my.crt -x509 -days 36500 -subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master/CN=mb3admin.com"
```

```
networks:
  intranet:
    ipam:
      driver: default
      config:
        - subnet: "10.90.0.0/16"
services:
  nginx: # 僞造驗證服務器
    image: nginx:1.23
    restart: always
    volumes:
      - ./hack/4.7.14/emby.conf:/etc/nginx/conf.d/emby.conf:ro
      - ./hack/4.7.14/my.crt:/my.crt:ro
      - ./hack/4.7.14/my.key:/my.key:ro
    networks:
      intranet:
        ipv4_address: 10.90.0.2
  coredns: # 攔截 dns 對 https://mb3admin.com 發動中間人攻擊
    image: coredns/coredns:1.10.1
    restart: always
    volumes:
      - ./hack/4.7.14/Corefile:/Corefile:ro
    networks:
      intranet:
        ipv4_address: 10.90.0.3
  emby:
    image: linuxserver/emby:4.7.14
    restart: always
    ports:
      - 8096:8096
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Asia/Shanghai
      - LANG=en_US.UTF-8
    devices:
      - /dev/dri:/dev/dri
    volumes:
      # 替換容器內受信任的證書列表
      - ./hack/4.7.14/ca-certificates.crt:/app/emby/etc/ssl/certs/ca-certificates.crt:ro
      - ./hack/4.7.14/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt:ro
      # 攔截 web 客戶端請求
      - ./hack/4.7.14/index.html:/app/emby/dashboard-ui/index.html:ro
      # 存儲 emby 設定
      - ./config:/config
      # 自定義的媒體目錄
      - ./data:/data
    # 設置 dns 服務器
    dns: 10.90.0.3
    networks:
      intranet:
        ipv4_address: 10.90.0.4
```

## networks

networks 指定了一個 docker 網路，必須要自定義網路才可以爲 容器指定固定 ip

```
networks:
  intranet:
    ipam:
      driver: default
      config:
        - subnet: "10.90.0.0/16"
```

創建的虛擬網路使用 10.90.0.0/16 作爲網段

## nginx

nginx 用於提供一個 http 服務用於返回僞造的會員認證信息

```
services:
  nginx:
    image: nginx:1.23
    restart: always
    volumes:
      - ./hack/4.7.14/emby.conf:/etc/nginx/conf.d/emby.conf:ro
      - ./hack/4.7.14/my.crt:/my.crt:ro
      - ./hack/4.7.14/my.key:/my.key:ro
    networks:
      intranet:
        ipv4_address: 10.90.0.2
```

cert.crt 與 cert.key 是自簽的一個 https 證書， emby.conf 使用證書創建了一個 https 服務供 emby 訪問

```
#info="emby.conf"
server {
    listen  443 ssl http2;
    ssl_certificate /my.crt;
    ssl_certificate_key /my.key;

    location = /admin/service/registration/validateDevice {
        add_header Access-Control-Allow-Origin *;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

        add_header Content-Type "application/json;charset=utf-8";
        add_header Cache-Control "max-age=31536000";
        return 200 '{"cacheExpirationDays": 365,"message": "Device Valid","resultCode": "GOOD"}';
    }
    location = /admin/service/registration/getStatus {
        add_header Access-Control-Allow-Origin *;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

        add_header Content-Type "application/json;charset=utf-8";
        add_header Cache-Control "max-age=31536000";
        return 200 '{"deviceStatus":"0","planType":"Lifetime","subscriptions":{}}';
    }
    location = /admin/service/registration/validate {
        add_header Access-Control-Allow-Origin *;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

        add_header Content-Type "application/json;charset=utf-8";
        add_header Cache-Control "max-age=31536000";
        return 200 '{"featId":"MBSupporter","registered":true,"expDate":"2100-01-01","key":114514}';
    }
}
```

## coredns

coredns 是一個 dns 服務器，她劫持域名 mb3admin.com 到 僞造服務器

```
services:
  coredns:
    image: coredns/coredns:1.10.1
    restart: always
    volumes:
      - ./hack/4.7.14/Corefile:/Corefile:ro
    networks:
      intranet:
        ipv4_address: 10.90.0.3
```

```
#info="Corefile"
.:53 {
	cache
    hosts {
        10.90.0.2  mb3admin.com
        fallthrough
    }
}
```

## emby

emby 容器需要
1. 設置 dns 到 coredns，以便發動中間人攻擊
2. 用信任了自簽名 ca 的 ca-certificates.crt 替換掉容器中的 ca-certificates.crt 以便僞造的證書被信任
3. 用修改過的 index.html 替換原本的 index.html，其中攔截了網頁驗證授權

```
  emby:
    image: linuxserver/emby:4.7.14
    restart: always
    ports:
      - 8096:8096
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Asia/Shanghai
      - LANG=en_US.UTF-8
    devices:
      - /dev/dri:/dev/dri
    volumes:
      # 替換容器內受信任的證書列表
      - ./hack/4.7.14/ca-certificates.crt:/app/emby/etc/ssl/certs/ca-certificates.crt:ro
      - ./hack/4.7.14/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt:ro
      # 攔截 web 客戶端請求
      - ./hack/4.7.14/index.html:/app/emby/dashboard-ui/index.html:ro
      # 存儲 emby 設定
      - ./config:/config
      # 自定義的媒體目錄
      - ./data:/data
    # 設置 dns 服務器
    dns: 10.90.0.3
    networks:
      intranet:
        ipv4_address: 10.90.0.4
    depends_on:
        nginx:
            condition: service_started
        coredns:
            condition: service_started
```

```
#info="index.html"
<!DOCTYPE html>
<html class="preload" data-appmode="standalone">

<head>

    <meta name="viewport"
        content="viewport-fit=cover, width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="preload" href="modules/fonts/material-icons/LDItaoyNOAY6Uewc665JcIzCKsKc_M9flwmP_3.woff2" as="font"
        type="font/woff2" crossorigin>
    <link rel="manifest" href="manifest.json">
    <meta name="description" content="Emby Server">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">

    <meta http-equiv="X-UA-Compatibility" content="IE=Edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Emby">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta property="og:title" content="Emby">
    <meta property="og:site_name" content="Emby">
    <meta property="og:url" content="https://emby.media">
    <meta property="og:description" content="Energize your media.">
    <meta property="og:type" content="article">
    <meta property="fb:app_id" content="1618309211750238">
    <meta name="apple-itunes-app" content="app-id=992180193">
    <link rel="apple-touch-icon" href="images/icon-192x192.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/icon-152x152.png">
    <link rel="apple-touch-icon" sizes="192x192" href="images/icon-192x192.png">
    <link rel="apple-touch-icon" sizes="384x384" href="images/icon-384x384.png">
    <link rel="apple-touch-icon" sizes="512x512" href="images/icon-512x512.png">
    <link rel="apple-touch-startup-image" href="images/splash.png">
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="msapplication-TileImage" content="images/icon-144x144.png">
    <meta name="msapplication-TileColor" content="#333333">
    <meta name="theme-color" content="#43A047">

    <title>Emby</title>

    <style>
        .preload {
            background-color: #000;
        }

        .app-splash {
            background-image: url(modules/themes/logowhite.png);
            background-position: center top;
            background-repeat: no-repeat;
            background-size: contain;
            position: fixed;
            top: 30%;
            left: 30%;
            right: 30%;
            height: 20%;
            contain: strict;
            content-visibility: auto;
        }

        .hide {
            display: none !important;
        }

        @media (orientation: landscape) {

            .app-splash {
                left: 37.5%;
                right: 37.5%;
            }
        }
    </style>
<script>
    (function () {
        var rFetch = window.fetch;

        var map = {
            '/admin/service/registration/validateDevice': '{"cacheExpirationDays": 365,"message": "Device Valid","resultCode": "GOOD"}',
            '/admin/service/registration/validate': '{"featId":"MBSupporter","registered":true,"expDate":"2100-01-01","key":"114514"}',
            '/admin/service/registration/getStatus': '{"deviceStatus":"0","planType":"Lifetime","subscriptions":{}}',
        };

        var Resp = function (data) {
            return Promise.resolve({
                status: 200,
                text: function () {
                    return data;
                },
                json: function () {
                    return JSON.parse(data);
                },
            });
        };

        var fetch = function () {
            var url = arguments[0];
            if (typeof url === 'string') {
                var keys = Object.keys(map);
                for (var i = 0; i < keys.length; i++) {
                    var p = keys[i];
                    if (url.includes(p)) {
                        var data = map[p];
                        return Resp(data);
                    }
                }
            }
            return rFetch.apply(this, arguments);
        };
        window.fetch = fetch;
    })();
</script>
</head>

<body class="mainAnimatedPages skinBody">
    <div class="backdropContainer"></div>
    <div class="backgroundContainer"></div>
    <div class="mainDrawer hide focuscontainer-y padded-bottom-page" is="emby-scroller" data-horizontal="false"
        data-centerfocus="true" data-navcommands="card" data-bindheader="false" data-skipfocuswhenvisible="true">
        <div class="scrollSlider mainDrawerScrollSlider"></div>
    </div>
    <div class="skinHeader focuscontainer-x focuscontainer-up headroom flex align-items-center flex-grow headerTop">
        <div class="headerLeft headerSection">
        </div>
        <div class="headerMiddle headerSection sectionTabs">
        </div>
        <div class="headerRight headerSection focuscontainer-right">
        </div>
    </div>

    <div class="app-splash"></div>
    <script src="apploader.js" defer></script>
</body>

</html>
```

> 上述 73行到 111行的 script 是需要添加的代碼，它攔截了對授權 api 的請求並直接返回僞造的授權結果


# web

web 由於是加載服務器的資源，上述兩種對服務器釋放的魔法都已經 hack 了 web 加載的 js 代碼，故都可以直接獲取完整的會員功能

## 4.8.3

4.8.3 開始，服務器網頁資源是嵌入可執行程序程序中的無法直接替換，這樣可以使用 nginx 作爲 emby 的反向代理，直接爲 index.html 或 apploader.js 返回 magic 後的代碼

複製 apploader.js 對它施加 magic(在最前面添加如下代碼)

```
#info="apploader.js"
(function () {
    var rFetch = window.fetch;

    var map = {
        '/admin/service/registration/validateDevice': '{"cacheExpirationDays": 365,"message": "Device Valid","resultCode": "GOOD"}',
        '/admin/service/registration/validate': '{"featId":"MBSupporter","registered":true,"expDate":"2100-01-01","key":"114514"}',
        '/admin/service/registration/getStatus': '{"deviceStatus":"0","planType":"Lifetime","subscriptions":{}}',
    };

    var Resp = function (data) {
        return Promise.resolve({
            status: 200,
            text: function () {
                return data;
            },
            json: function () {
                return JSON.parse(data);
            },
        });
    };
    var keys = Object.keys(map);
    var fetch = function () {
        var url = arguments[0];
        if (typeof url === 'string') {
            for (var i = 0; i < keys.length; i++) {
                var p = keys[i];
                if (url.includes(p)) {
                    var data = map[p];
                    return Resp(data);
                }
            }
        }
        return rFetch.apply(this, arguments);
    };
    window.fetch = fetch;
})();
```


然後在 nginx 爲 emby 創建一個反向代理，並替換掉 apploader.js

```
#info="default.conf"
server {

    // 返回 magic 後的 代碼
    location /web/apploader.js {
        alias /4.8.3/apploader.js;
    }
    // 反代 emby
    location / {
        proxy_connect_timeout                   5s;                     
        proxy_send_timeout                      60s;                                  
        proxy_read_timeout                      60s;

        # version
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;

        # Allow websocket connections
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;

        proxy_set_header X-Real-IP              $remote_addr;                         

        proxy_set_header X-Forwarded-For        $remote_addr;

        proxy_set_header X-Forwarded-Host       $http_host;                      
        proxy_set_header X-Forwarded-Port       $server_port;                           
        proxy_set_header X-Forwarded-Proto      $scheme;                                  
        proxy_set_header X-Forwarded-Scheme     $scheme;                  

        proxy_set_header X-Scheme               $scheme;  

        # Pass the original X-Forwarded-For
        proxy_set_header X-Original-Forwarded-For $http_x_forwarded_for;
        proxy_pass http://10.90.0.4:8096; # 這裏地址改爲 emby 後端地址
    }
}
```

## 4.8.10

經測試 4.8.10 可以按照與 4.8.3 相同的方法進行施法

# android

從官網下載 [android](https://emby.media/emby-for-android.html) 或 [android tv](https://emby.media/emby-for-android-tv.html) 客戶端

1. 安裝一些魔法輔助工具

	```
	sudo apt install -y zipalign apksigner apktool
	```
	
2. 解包 apk

	```
	apktool d -r app-google-release.apk
	```
	
3. 對於 android 則修改 **assets/www/index.html** 檔案添加一個 script 標籤攔截會員認證(詳細代碼見上文中間人攻擊)

	對於 android tv 則全局搜索 https://mb3admin.com 並將域名替換爲僞造的服務域名
	
4. 重新打包破解的 apk

	```
	apktool b app-google-release -o tv.apk
	```

此時 apk 還不能安裝，因爲爲了安全 android 要求 apk 必須簽名

1. 如果沒有 jks 則使用下述指令創建一個

	```
	keytool -genkey -v -keyalg RSA -keysize 2048 -validity 36500 -keystore key.jks -alias key
	```

2. 對齊 apk 這樣app運行效率更高

	```
	zipalign -v -p 4 tv.apk tv-aligned.apk
	```

3. 簽名 apk (假設步驟1創建的 jks 密碼爲 123456)

	```
	apksigner sign --ks key.jks --ks-pass pass:123456  --out tv-final.apk tv-aligned.apk
	```

> 不施展魔法 android 可以播放但無法使用離線播放等功能，android tv 則連播放都不被允許

# linux

[linux](https://emby.media/emby-theater-linux.html) 版本同樣不施展魔法無法播放，攻擊方法和上文服務器中間人攻擊方法類似，但是桌面版本使用了 electron 編寫，electron 不會使用操作系統的證書信任鏈，你可以搜索如何添加 electron 信任鏈，此外本喵更推薦直接修改 electron 腳本

編輯 **/opt/emby-theater/electron/resources/app/main.js** 在檔案最上方添加如下代碼

```
(function(){
    var { app } = require('electron');
    app.commandLine.appendSwitch('host-rules', 'MAP mb3admin.com 127.0.0.1');
    // app.commandLine.appendSwitch('ignore-certificate-errors');
    // app.whenReady().then(() => {
    //     app.importCertificate({certificate:'/ca.crt'},()=>{});
    // })
    app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
        if(url.startsWith("https://mb3admin.com")){
            event.preventDefault();
            callback(true);
        }else{
            callback(false);
        }
    });
})();
```

* 第 3 行將服務器驗證地址 mb3admin.com 解析到 127.0.0.1，請將 127.0.0.1 修改成你的僞造驗證地址(如何搭建僞造服務器請參考上文 中間人攻擊中的 nginx 設定)
* 第  8 行直接忽略 https://mb3admin.com 的證書錯誤以便中間人攻擊能夠成功
	* 你也可以使用第 4 行的代碼，它直接忽略所有的證書錯誤，但這可能引發安全問題不建議
	* 你也可以使用第 5 行的代碼，它可以直接將你服務器僞造的證書添加到信任鏈，但是 importCertificate 只在 linux 下有效，其它平臺不支持(推薦在 linux 下使用此代碼)

# windows
[windows](https://emby.media/emby-theater.html) 版本同樣不施展魔法無法播放，它的施法方法和 linux 類似，只是它的 electron 代碼位置位於

```
C:\Users\Your_User_Name\AppData\Roaming\Emby-Theater\system\electronapp\main.js
```

* Your\_User\_Name 請修改爲你的 windows 用戶名 

# mac

本喵没有 [mac](https://emby.media/emby-theater-macos.html) 设备所以没有测试，但考虑到 linux/windows 都使用了 electron 编写，electron 也支持 mac 故猜测 mac 版本应该施法方式也和 linux/windows 一样