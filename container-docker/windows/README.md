# windows

雖然很難用然 windwos 提供的 wsl 爲 docker 在windows 上運行提供了支持

docker 會創建兩個子系統 

```
PS C:\Users\king> wsl -l -v
  NAME                   STATE           VERSION
* docker-desktop-data    Running         2
  docker-desktop         Running         2
```

# 修改鏡像存儲位置
docker-desktop-data 用於 存儲 鏡像 通常存儲在 **C:\Users\XXX\AppData\Local\Docker\wsl\data\ext4.vhdx**

將其導出到其它位置 註銷後重新導入即可

1. 停止 docker desktop 確保 docker-desktop-data 和 docker-desktop 處於未運行狀態

1. 導出 docker-desktop-data

	```
	wsl --export docker-desktop-data "D:\\docker-desktop-data.tar"
	```

1. 註銷 docker-desktop-data

	```
	wsl --unregister docker-desktop-data
	```
	
1. 創建一個檔案夾 用於存儲 鏡像(比如 **D:\wsl\docker**)

1. 將 docker-desktop-data.tar 導入 並註冊 同時設置 ext4.vhdx 存儲路徑

	```
	wsl --import docker-desktop-data "D:\\wsl\\docker" "D:\\docker-desktop-data.tar" --version 2
	```
	
# [wsl2](https://docs.docker.com/desktop/windows/wsl/#download)

1. 安裝 [wsl2](ms-wsl/install) 後，確保至少再安裝一個 linux 子系統
2. 從 docker 官網下載 [Docker Desktop For Windows](https://www.docker.com/) 並運行
3. 在 設置 > General 中勾選 Use the WSL 2 based engine 
		![](assets/docker-running.png)
4. 在 設置 > Resources > WSL INTEGRATION 中 啓用 docker
		![](assets/docker-dashboard.png)