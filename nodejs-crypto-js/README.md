# crypto-js

crypto-js 是js環境下的一個開源(MIT)加密解碼庫


* [源碼](https://github.com/brix/crypto-js)
* [文檔](https://cryptojs.gitbook.io/docs)

雖然它已宣佈停止維護，但依然很值得使用

1. 它停止維護的官方聲明是說現代瀏覽器和 nodejs 原生環境都有原生的 Crypto 模塊。但是目前 Crypto 原生模塊有一些算法並沒有實現，此外一些非 nodejs 和 瀏覽器環境下也沒有 Crypto 模塊
2. crypto-js 已有代碼足夠健壯，早在 node 流行前此項目就存在，經過衆多項目的測試使用，對於已有代碼除非明確爆出bug否則可以放心繼續使用
3. 在一些部分兼容 nodejs 但並非 nodejs 的環境下，使用 crypto-js 可能是不錯的選擇


```
npm install crypto-js
```

```
npm install --save @types/crypto-js
```