# hash

1. 必須調用 xxx\_init 初始化 hash\_state
2. 重複調用 xxx\_process 更新 hash 值到 hash\_state
3. 最後調用 xxx\_done 獲取計算得到的 hash 值

其中 xxx\_desc 是一個 struct ltc\_hash\_descriptor 其中包含了hash 的描述信息
* **name** 一個 c 字符串記錄了此算法的名稱
* **hashsize** 計算出的 hash 的字節長度
* **blocksize** hash 算法每次以多大塊進行計算，通常傳入這個數字的整數倍效率更高
* **init** xxx\_init 函數地址
* **process** xxx\_process 函數地址
* **done** xxx\_done 函數地址
# example
```
#include <tomcrypt.h>
#include <stdio.h>
void puts_hex(const unsigned char *s, int n)
{
    static unsigned char *hextable = "0123456789abcdef";
    int j = 0;
    for (int i = 0; i < n; i++)
    {
        unsigned char v = s[i];
        putc(hextable[v >> 4], stdout);
        putc(hextable[v & 0x0f], stdout);
        j += 2;
    }
    putc('\n', stdout);
}
int main(int argc, char *argv[])
{
    hash_state md;
    if (sha512_init(&md))
    {
        puts("sha512_init error");
        return 1;
    }
    if (sha512_process(&md, "ok", 2))
    {
        puts("sha512_init error");
        return 1;
    }
    unsigned char out[64] = {0};
    if (sha512_done(&md, out))
    {
        puts("sha512_init error");
        return 1;
    }
    puts_hex(out, 64);

    return 0;
}
```

```
#include <tomcrypt.h>
#include <stdio.h>
#include <stdlib.h>
void puts_hex(const unsigned char *s, int n)
{
    static unsigned char *hextable = "0123456789abcdef";
    int j = 0;
    for (int i = 0; i < n; i++)
    {
        unsigned char v = s[i];
        putc(hextable[v >> 4], stdout);
        putc(hextable[v & 0x0f], stdout);
        j += 2;
    }
    putc('\n', stdout);
}
int main(int argc, char *argv[])
{
    hash_state md;
    if (sha512_desc.init(&md))
    {
        puts("sha512_init error");
        return 1;
    }
    if (sha512_desc.process(&md, "ok", 2))
    {
        puts("sha512_init error");
        return 1;
    }
    unsigned char *out = malloc(sha512_desc.hashsize);
    if (!out)
    {
        puts("malloc error");
        return 1;
    }
    if (sha512_desc.done(&md, out))
    {
        puts("sha512_init error");
        return 1;
    }
    puts_hex(out, sha512_desc.hashsize);
    return 0;
}
```