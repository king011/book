# [display](https://www.primefaces.org/primeflex/display)

display 定義了幾個 class 用於表示頁面元素要如何顯示

| Class | Properties |
| -------- | -------- |
| hidden     | display: none;     |
| block     | display: block;     |
| inline     | display: inline;     |
| inline-block     | display: inline-block;     |
| flex     | display: flex;     |
| inline-flex     | display: inline-flex;     |

## hidden

設置了 class hidden 的元素不會顯示也不會分配頁面空間

![](assets/hide.png)

```

<div class="card">
    <div class="card-container blue-container flex align-items-center justify-content-start">
        <div class="hidden w-4rem h-4rem bg-blue-500 text-white font-bold p-4 border-round mr-3">1</div>
        <div class="w-4rem h-4rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center p-4 border-round mr-3">2</div>
        <div class="w-4rem h-4rem bg-blue-500 text-white font-bold flex align-items-center justify-content-center p-4 border-round">3</div>
    </div>
</div>
```

## block

設置了 class block 的元素從新的一行開始顯示

![](assets/block.png)
```
<div class="card">
    <div class="card-container yellow-container">
        <div class="block bg-yellow-500 font-bold text-center p-4 border-round mb-3">1</div>
        <div class="block bg-yellow-500 font-bold text-center p-4 border-round mb-3">2</div>
        <div class="block bg-yellow-500 font-bold text-center p-4 border-round mb-3">3</div>
    </div>
</div>
```

## inline

設置了 class inline 的元素不會在新行開始並根據需要分配寬度

![](assets/inline.png)

```
<div class="card py-6">
    <div class="card-container green-container p-0">
        <div class="inline w-4rem h-4rem bg-green-500 text-white font-bold text-center p-4 border-round mx-4">1</div>
        <div class="inline w-4rem h-4rem bg-green-500 text-white font-bold text-center p-4 border-round">2</div>
        <div class="inline w-4rem h-4rem bg-green-500 text-white font-bold text-center p-4 border-round mx-4">3</div>
    </div>
</div>
```

## inline-block
設置了 class inline-block 的元素類似 inline，但會遵守 width height top bottom padding margin 的屬性

![](assets/inline-block.png)

```
<div class="card">
    <div class="card-container purple-container">
        <div class="inline-block w-4rem h-4rem bg-purple-500 text-white font-bold text-center p-4 border-round">1</div>
        <div class="inline-block w-4rem h-4rem bg-purple-500 text-white font-bold text-center p-4 border-round mx-4">2</div>
        <div class="inline-block w-4rem h-4rem bg-purple-500 text-white font-bold text-center p-4 border-round">3</div>
    </div>
</div>
```

## flex
設置了 class flex 的元素會以 block level flex container 顯示

![](assets/flex.png)

```
<div class="card">
    <div class="flex card-container indigo-container">
        <div class="flex-1 h-4rem bg-indigo-500 text-white font-bold text-center p-4 border-round">1</div>
        <div class="flex-1 h-4rem bg-indigo-500 text-white font-bold text-center p-4 border-round mx-4">2</div>
        <div class="flex-1 h-4rem bg-indigo-500 text-white font-bold text-center p-4 border-round">3</div>
    </div>
</div>
```
## inline-flex
設置了 class flex 的元素會以 inline level flex container 顯示

![](assets/inline-flex.png)

```
<div class="card">
    <div class="inline-flex card-container orange-container">
        <div class="flex-1 h-4rem bg-orange-500 text-white font-bold text-center p-4 border-round">1</div>
        <div class="flex-1 h-4rem bg-orange-500 text-white font-bold text-center p-4 border-round mx-4">2</div>
        <div class="flex-1 h-4rem bg-orange-500 text-white font-bold text-center p-4 border-round">3</div>
    </div>
</div>
```

# 響應式佈局

可以爲上述 class 加上響應式前綴，例如 md:block 這將使用 **@media screen** 查詢屏幕寬度，當屏幕到達指定寬度時使用帶前綴的佈局而忽略未帶前綴的佈局

| 前綴 | 描述 | 測試定義 |
| -------- | -------- | -------- |
| sm     | 小屏幕，例如手機     | @media screen and (min-width: 576px)     |
| md     | 中等屏幕，例如平板     | @media screen and (min-width: 768px)     |
| lg     | 大屏幕，例如筆記本     | @media screen and (min-width: 992px)     |
| xl     | 大屏幕，例如顯示器     | @media screen and (min-width: 1200px)     |

```
<div class="card">
    <div class="card-container cyan-container inline-flex">
        <div class="hidden sm:inline-flex h-4rem bg-cyan-500 text-white font-bold align-items-center justify-content-center p-4 border-round mr-3">Hide on a small screen</div>
    </div>
</div>
```

優先順序是 xl > lg > md > sm > 未加前綴，安裝順序匹配屏幕寬度，匹配則使用此佈局否則匹配下個規則。

一個常用的情景是爲一個小的工具圖標指示 **hidden sm:xxx** 這樣這個小工具圖標只在手機端顯示，而爲一個大的工具欄指示 **xxx sm:hidden** 讓工具欄在手機端隱藏而在其它稍大屏幕中顯示

```
<div class="card">
    <div class="card-container cyan-container inline-flex">
        <div
            class="hidden sm:inline-flex h-4rem bg-cyan-500 text-white font-bold align-items-center justify-content-center p-4 border-round mr-3">
            Hide on a small screen</div>
    </div>
    <div class="card-container cyan-container inline-flex">
        <div
            class="inline-flex sm:hidden h-4rem bg-cyan-500 text-white font-bold align-items-center justify-content-center p-4 border-round mr-3">
            Hide on a not small screen</div>
    </div>
</div>
```