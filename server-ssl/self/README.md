# 自簽

可以使用 openssl 自己簽署證書，但自簽證書不會被系統信任除非你把它添加到系統信任列表，但對於測試或一些魔法操作自簽證書很有必要

# 自簽證書

對於單一的測試網站自簽證書是最簡單的方案

1. 創建私鑰

	```
	openssl genrsa -out my.key 2048
	```

2. 基於私鑰創建證書請求檔案

	```
	openssl req -new -key my.key -out my.csr -subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master/CN=example.com/CN=example2.com"
	```
	
	可以不傳入 -subj 則會以互動式輸入這些內容
	
	* **/C** Country 國家名稱 的兩個字母代碼，你可以[在此](https://www.iban.com/country-codes)查詢
	* **/ST** State or Province 省/州 名稱
	* **/L** Locality or City 地點或城市名稱
	* **/O** Organization or  Company 組織或公司名稱
	* **/OU** Organizational or section 組織單位或公司部門
	* **/CN** Common Name 通用名稱，例如域名或ip或證書所有者名稱
	
3. 使用私鑰和證書請求生成自簽名證書

	```
	openssl x509 -req -signkey my.key -days 365 -in my.csr -out my.crt
	```

上述步驟 1 和 2 可以合併爲一個指令

```
openssl req -new -newkey rsa:2048 -nodes -keyout my.key -out my.csr -subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master/CN=example.com"
```

上述3個指令也可以全部合併爲一個(這不會產生 csr 請求檔案)

```
openssl req -new -newkey rsa:2048 -nodes -keyout my.key -out my.crt -x509 -days 365 -subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master/CN=example.com"
```

# 信任證書

對於 windows 直接雙擊即可安裝並信任證書，對於 linux 可以編輯 **/etc/ssl/certs/ca-certificates.crt** 檔案將證書內容添加到檔案結尾即可

# 私有 ca

證書是一個信任鏈，可以將一個自簽證書作爲 ca，用這個 ca 去簽署其它證書，這樣只需要信任 ca 所有被它簽署的證書也會被信任

1. 生成一個自簽證書作爲 ca

	```
	openssl req -new -newkey rsa:2048 -nodes -keyout ca.key -out ca.crt -x509 -days 365 -subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master"
	```

2. 創建 key 和 csr

	```
	openssl req -new -newkey rsa:2048 -nodes -keyout my.key -out my.csr -subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master/CN=example.com"
	```

3. 使用 ca 簽署 csr

	```
	openssl x509 -req -CA ca.crt -CAkey ca.key -CAcreateserial -days 365 -in my.csr -out my.crt
	```
	
證書的序號必須唯一 **-CAcreateserial** 會創建一個隨機的序號並存儲到 \*\.srl 中，在下次簽署新的證書時應該使用 **-CAserial** 指定此檔案來替代 **-CAcreateserial**
```
openssl x509 -req -CA ca.crt -CAkey ca.key -CAserial ca.srl -days 365 -in my.csr -out my.crt
```

> -CAserial 從指定檔案中讀取序號將其+1作爲新序號，並將新序號存儲到原檔案

# android

目前(2024-09-25) android 要求必須使用 subjectAltName 指定域名，不再信任只使用 CN 指定域名的證書可以使用 addext 設置

```
openssl req -new \
	-newkey rsa:2048 -nodes \
	-keyout my.key \
	-out my.csr \
	-subj "/C=CN/ST=sichuan/L=chengdu/O=cerberus/OU=master/CN=example.com" \
    -addext "subjectAltName=DNS:example.com"
```