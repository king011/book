# urlencode

url 需要將一些字符轉義 http-parser 沒有提供 轉碼函數 需要依據規則自己實現

1. 對於字符 **a\-z&nbsp;A\-Z&nbsp;0\-9&nbsp;\.&nbsp;\-&nbsp;\*&nbsp;\_** 不編碼
2. 對於 空格 需要 編碼爲 **+**
3. 其它字節編碼爲 **%XY** 形式的3個字符串

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
char hexchar(unsigned char v)
{
    if (v < 10)
    {
        return '0' + v;
    }
    return 'A' + v - 10;
}
int urlencode(char *dst, int capacity, const char *src, int length)
{
    int offset = 0;
    for (int i = 0; i < length; i++)
    {
        char ch = src[i];
        if (' ' == ch)
        {
            if (dst)
            {
                if (offset >= capacity)
                {
                    return -1;
                }
                dst[offset++] = '+';
            }
            else
            {
                ++offset;
            }
        }
        else if ((ch >= 'A' && ch <= 'Z') ||
                 (ch >= 'a' && ch <= 'z') ||
                 (ch >= '0' && ch <= '9') ||
                 '.' == ch || '-' == ch || '*' == ch || '_' == ch)
        {
            if (dst)
            {
                if (offset >= capacity)
                {
                    return -1;
                }
                dst[offset++] = ch;
            }
            else
            {
                ++offset;
            }
        }
        else
        {
            if (dst)
            {
                if (offset + 2 >= capacity)
                {
                    return -1;
                }
                dst[offset++] = '%';
                unsigned char x = (unsigned char)ch >> 4;
                x = (unsigned char)ch & 0xF;
                dst[offset++] = hexchar((unsigned char)ch >> 4);
                dst[offset++] = hexchar((unsigned char)ch & 0xF);
            }
            else
            {
                offset += 3;
            }
        }
    }
    return offset;
}
// return -1 目標緩衝區太小
// return -2 未知格式
int urldecode(char *dst, int capacity, const char *src, int length)
{
    int offset = 0;
    for (int i = 0; i < length; i++)
    {
        char ch = src[i];
        if ('+' == ch)
        {
            if (dst)
            {
                if (offset >= capacity)
                {
                    return -1;
                }
                dst[offset++] = ' ';
            }
            else
            {
                ++offset;
            }
            continue;
        }
        else if ((ch >= 'A' && ch <= 'Z') ||
                 (ch >= 'a' && ch <= 'z') ||
                 (ch >= '0' && ch <= '9') ||
                 '.' == ch || '-' == ch || '*' == ch || '_' == ch)
        {
            if (dst)
            {
                if (offset >= capacity)
                {
                    return -1;
                }
                dst[offset++] = ch;
            }
            else
            {
                ++offset;
            }
            continue;
        }
        else if ('%' == ch && i + 2 < length)
        {
            unsigned char v0 = src[++i];
            if (v0 >= 'A' && v0 <= 'F')
            {
                v0 -= 'A' - 10;
            }
            else if (v0 >= '0' && v0 <= '9')
            {
                v0 -= '0';
            }
            else
            {
                // unknow format
                return -2;
            }
            unsigned char v1 = src[++i];
            if (v1 >= 'A' && v1 <= 'F')
            {
                v1 -= 'A' - 10;
            }
            else if (v1 >= '0' && v1 <= '9')
            {
                v1 -= '0';
            }
            else
            {
                // unknow format
                return -2;
            }
            if (dst)
            {
                v0 <<= 4;
                v0 |= v1;
                dst[offset++] = v0;
            }
            else
            {
                ++offset;
            }
            continue;
        }
        // unknow format
        return -2;
    }
    return offset;
}
void test_urldecode(const char *src, int length)
{
    int capacity = urldecode(NULL, 0, src, length);
    assert(capacity > 0);
    char *dst = malloc(capacity);
    int n = urldecode(dst, capacity, src, length);
    assert(n == capacity);
    printf("urldecode -> %.*s\n", capacity, dst);
    free(dst);
}
int main(void)
{
    // urlencode
    const char *src = "/api/v1/中文測試";
    int length = strlen(src);
    int capacity = urlencode(NULL, 0, src, length); // 計算目標緩衝區大小
    char *dst = malloc(capacity);
    int n = urlencode(dst, capacity, src, length);
    assert(n == capacity);
    printf("urlencode -> %.*s\n", capacity, dst);

    // urldecode
    test_urldecode(dst, n);
    free(dst);
    return 0;
}
```