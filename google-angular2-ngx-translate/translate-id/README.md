# 翻譯id

如何從項目中抽取帶翻譯的內容是一個挑戰，[官方教學](https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-angular-app-with-ngx-translate-new) 中提到了可以使用 ngx-translate-extract 來分析並抽取項目中的待翻譯內容，不過本喵並不贊同，因爲這存在幾個問題

1. ngx-translate-extract 依賴特定的標籤(比如 **\_** 函數)，來識別翻譯 id，這影響來直接對 TranslateService 服務的調用
2. html 模板和 TranslateService.get 等傳入的 翻譯id 是字符串，ide 無法對它們進行代碼補齊，標籤書寫錯誤也不會有錯誤提示


本喵的做法是創建一個腳本(比如 i18n.js)，在裏面定義要翻譯的 id，然後腳本自動更新語言包以保證語言包的一致，然後腳本在爲 angular 項目生成一個 i18n.ts 裏面包含了所有 翻譯 id 的索引以及註釋，現在不要通過字符串去調用 ngx-translate 而是傳遞 i18n.ts 裏面的索引這樣 ide 將給出代碼提示以及對翻譯id 寫錯時報出錯誤

```
#info="package.json"
{
  "scripts": {
    ...
    "i18n": "node i18n"
  },
	...
}
```

```
#info="i18n.js"
const fs = require('fs');
// 定義 語言包 輸出檔案夾
const dir = `${__dirname}/src/assets/i18n`
// 定義 語言包
const langs = [
    'en',
    'zh-Hant',
]
// 定義 翻譯 id key
const keyname = `${__dirname}/src/app/i18n.ts`

class Item {
    constructor(id, note) {
        this.id = id
        this.note = note
    }
}
function make(id, note) {
    return new Item(id, note)
}
class Group {

}
// 定義 翻譯 id 
const i18n = {
    demo: {
        title: make(null, '項目標題'),
        greeting: make('kk', '自定義的 翻譯 id'),
    },
    key: {
        __id: "k",
        title: null,
        greeting: make('g'),
    }
}
function copyI18n(i18n, dst, src) {
    for (const o in i18n) {
        if (!Object.hasOwnProperty.call(i18n, o)) {
            continue
        } else if (o.startsWith("__")) {
            continue
        }
        const v = i18n[o]
        if (v === null || v === undefined || v instanceof Item) {
            const id = v?.id ?? o
            let val = null
            if (src) {
                val = src[id]
            }
            dst[id] = val ?? null
        } else if (typeof v === "object") {
            let id = v.__id ?? ''
            if (id == '') {
                id = o
            }
            let s
            if (src) {
                s = src[id]
                if (typeof s !== "object") {
                    s = null
                }
            }
            dst[id] = copyI18n(v, {}, s)
        }
    }
    return dst
}
// 創建語言包
for (const lang of langs) {
    let old
    const filename = `${dir}/${lang}.json`
    try {
        old = JSON.parse(fs.readFileSync(filename, {
            encoding: "utf-8"
        }))
    } catch (e) {
        if (e.code !== 'ENOENT') {
            throw e
        }
        // not found
    }
    fs.writeFileSync(filename, JSON.stringify(copyI18n(i18n, {}, old), "", "\t"), {
        flag: "w"
    })
}
function generateKey(strs, obj, prefix, tab) {
    for (const o in obj) {
        if (!Object.hasOwnProperty.call(obj, o)) {
            continue
        } else if (o.startsWith("__")) {
            continue
        }

        const v = obj[o]
        if (v === null || v === undefined || v instanceof Item) {
            let id = v?.id ?? o
            id = prefix == '' ? id : prefix + '.' + id
            const note = v?.note ?? ''
            if (note != '') {
                strs.push(`${tab}/**`)
                const vals = note.split("\n")
                for (const val of vals) {
                    strs.push(`${tab}* ${val}`)
                }
                strs.push(`${tab}*/`)
            }
            strs.push(`${tab}${o}: ${JSON.stringify(id)},`)
        } else if (typeof v === "object") {
            let id = v.__id ?? ''
            if (id == '') {
                id = o
            }
            strs.push(`${tab}${o}: {`)
            generateKey(strs, v, prefix == "" ? id : `${prefix}.${id}`, tab + "\t")
            strs.push(`${tab}},`)
        }
    }
}
function generateKeys() {
    const strs = []
    generateKey(strs, i18n, "", "\t")
    return "export const i18n = {\n" + strs.join("\n") + "\n}"
}
// 創建源碼 key
fs.writeFileSync(keyname, generateKeys(), {
    flag: "w"
})
```
> \_\_id 可以定義分組的翻譯 id，make 第一個參數可以定義條目的翻譯 id，如果都不定義則使用默認值

執行下述代碼，更新語言包以及索引
```
npm run i18n
```

它會爲項目生成 **src/app/i18n.ts** 的索引檔案

```
#info="src/app/i18n.ts"
export const i18n = {
	demo: {
		/**
		* 項目標題
		*/
		title: "demo.title",
		/**
		* 自定義的 翻譯 id
		*/
		greeting: "demo.kk",
	},
	key: {
		title: "k.title",
		greeting: "k.g",
	},
}
```

當然這個方案也存在副作用，就是這個索引檔案(i18n.ts) 會被打包到發佈的代碼中，這會增加編譯產出的體積，但是本喵認爲對於整個項目來說增加的體積幾乎可以忽略不計