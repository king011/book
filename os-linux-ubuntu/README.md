# Ubuntu
Ubuntu 是一個玄酷的 linux 桌面系統 亦提供了 服務器版本的支持

Ubuntu 基於 Debian 和 GNOME 桌面

Ubuntu 每6個月發佈一個版本(18個月技術支持) 每24個月發佈一個LTS版本(60個月技術支持)


* [官網](https://ubuntu.com/)

# Ubuntu GNOME

Ubuntu GNOME 是一個基與 UBuntu 的發行版 (已被Ubuntu官方接納)  只是 其將 Ubuntu的 Unity 桌面 替換為了 Gnome 桌面

* [官網](http://ubuntugnome.org/)

# Mint

Linux Mint 基與 UBuntu 的發行版 兼容UBuntu軟體

* [官網](https://www.linuxmint.com/)