# quick start

```
import { Cron } from "https://deno.land/x/croner@5.3.5/src/croner.js";

new Cron("* * * * * *", () => {
  console.log("This will run every second.");
});
```

```
new Cron(pattern, fnOrOptions1opt, fnOrOptions2opt) → {Cron}
```

# pattern

pattern 可以是一個 Date 或者字符串，用於指定任務什麼時候執行，如果是 Date 或者是 ISO 8601 字符串並且在當前時間之後則它會被執行一次，否則字符串則按照 cron 規則執行

```
// ┌──────────────── (optional) second (0 - 59)
// │ ┌────────────── minute (0 - 59)
// │ │ ┌──────────── hour (0 - 23)
// │ │ │ ┌────────── day of month (1 - 31)
// │ │ │ │ ┌──────── month (1 - 12, JAN-DEC)
// │ │ │ │ │ ┌────── day of week (0 - 6, SUN-Mon) 
// │ │ │ │ │ │       (0 to 6 are Sunday to Saturday; 7 is Sunday, the same as 0)
// │ │ │ │ │ │
// * * * * * *
```

對 cron 規則進行了一些擴展修改

1. 可以在 Field 傳入 **?** 這樣對應的 Field 將使用當前時間的對應值進行填充
2. 使用 **L** 可以指代每月最後一天 
3. pattern 也可以傳入一個 Date 或 ISO 8601 字符串來創建只觸發一次的任務，並且你還可以在第二個參數則設置 timezone 來指定時區
4. coner 允許更改星期和日期的組合方式，默認情況下，在星期或日期匹配時觸發例如 "0 20 1 * MON" 將在每月第一天或每週一觸發。你可以在第二個參數中設置 legacyMode 爲 false 來要求在每月第一天且爲星期一時執行(legacyMode 的默認值在多個版本中適合不太一致，故最好明確設置這個屬性)




| Field | Required | Allowed values | Allowed special characters | Remarks |
| -------- | -------- | -------- | -------- | -------- |
| Seconds     | Optional     | 0-59     | * , - / ?     |      |
| Minutes     | Yes     | 0-59     | * , - / ?     |      |
| Hours     | Yes     | 0-23     | * , - / ?     |      |
| Day of Month     | Yes     | 1-31     | * , - / ? L     |      |
| Month     | Yes     | 1-12 or JAN-DEC     | * , - / ?     |      |
| Day of Week     | Yes     | 0-7 or SUN-MON     | * , - / ?     | 0-6 是星期天-星期六,7也代表星期天     |

## nickname

croner 爲一些常用規則提供了昵稱

| nickname | 映射規則 | 含義 |
| -------- | -------- | -------- |
| @yearly     | "0 0 1 1 \*"     | 每年執行一次     |
| @annually     | "0 0 1 1 \*"     | 每年執行一次     |
| @monthly     | "0 0 1 \* \*"     | 每月執行一次     |
| @weekly     | "0 0 \* \* 0"     | 每周星期天執行一次     |
| @daily     | "0 0 \* \* \*"     | 每天執行一次     |
| @hourly     | "0 \* \* \* \*"     | 每小時執行一次     |

# CronOptions

Cron 的第二/三 個參數需要分別是 CronOptions 或 (self:Cron,ctx:any)=>void


```
interface opts {
  /**
   * 是否暫停 cron
   */
  paused?: boolean;

  /**
   * 構造時忽略此參數，用於記錄 cron 是否將被殺死或已經被殺死
   */
  kill?: boolean;

  /**
   * 即時觸發函數拋出異常也就緒
   */
  catch?: boolean;
  /**
   * 最多執行多少次觸發函數
   */
  maxRuns?: number;

  /**
   * 兩次調用觸發函數的最短時間間隔
   */
  interval?: number;
  /**
   * 設置 cron 開始時間
   */
  startAt?: string | Date;
  /**
   * 設置 cron 自動結束時間
   */
  stopAt?: string | Date;

  /**
   * 設置使用的時區
   */
  timezone?: string;

  /**
   * 如果爲 true，星期和號數使用 OR 匹配，如果爲 false 使用 AND 匹配
   * @defaultValue true
   */
  legacyMode?: boolean;

  /**
   * 傳遞給 觸發函數的第二個參數
   */
  context?: any;
}
```

# find

croner 提供了一些 next 函數可用於找出下一個執行時間

```
import { Cron } from "https://deno.land/x/croner@5.3.5/src/croner.js";

// Find next month
const nextMonth = new Cron("@monthly").next(),
  nextSunday = new Cron("@weekly").next(),
  nextSat29feb = new Cron("0 0 0 29 2 6", { legacyMode: false }).next(),
  nextSunLastOfMonth = new Cron("0 0 0 L * 7", { legacyMode: false }).next();

console.log("First day of next month: " + nextMonth!.toLocaleDateString());
console.log("Next sunday: " + nextSunday!.toLocaleDateString());
console.log(
  "Next saturday at 29th of february: " + nextSat29feb!.toLocaleDateString(),
); // 2048-02-29
console.log(
  "Next month ending with a sunday: " +
    nextSunLastOfMonth!.toLocaleDateString(),
);
```

# 控制

你可以直接修改構造時傳入的 CronOptions 來控制 cron，但更推薦的做法是調用 Cron 提供的控制函數
```
import { Cron } from "https://deno.land/x/croner@5.3.5/src/croner.js";

const job = new Cron("* * * * * *", (self: Cron) => {
  console.log(
    "This will run every second. Pause on second 10. Resume on 15. And quit on 20.",
  );
  console.log("Current second: ", new Date().getSeconds());
  console.log("Previous run: " + self.previous());
  console.log("Next run: " + self.next());
});

new Cron("10 * * * * *", { maxRuns: 1 }, () => job.pause());
new Cron("15 * * * * *", { maxRuns: 1 }, () => job.resume());
new Cron("20 * * * * *", { maxRuns: 1 }, () => job.stop());
```

# 首次與最後一次

利用 previous 和 next 成員函數，可以判斷首次執行或最後一次執行

```
import { Cron } from "https://deno.land/x/croner@5.3.5/src/croner.js";

const job = new Cron("* * * * * *", (self: Cron) => {
  if (!self.previous()) {
    console.log("first");
  }
  console.log("job run");
  if (!self.next()) {
    console.log("last");
  }
}, {
  maxRuns: 3,
});
if (!job.next() && !job.previous()) {
  console.log("不會執行, cron 表達式的時間不存在或 maxRuns 爲 0 或只執行一次的 cron 時間已經過期");
}
```