# pprof

golang 提供了 runtime/pprof 包進行 性能分析

net/http/pprof 包對其封裝 並且 開發一個 web 接口

```
package main

import (
	"net/http"
	_ "net/http/pprof"
)

func main() {
	http.ListenAndServe("localhost:7000", nil)
}
```

訪問 [http://localhost:7000/debug/pprof/ ](http://localhost:7000/debug/pprof/ ) 即可打開web 分析頁面


# cpu
執行 go tool pprof http://localhost:7000/debug/pprof/profile\?seconds\=10 等待10秒後

即可 進入 cpu 分析 交互 模式 比如 輸入 top 10

會 輸出 佔用 cpu 最高的 10個 函數

# heap

執行 go tool pprof http://localhost:7000/debug/pprof/heap 用於 分析 堆內存分配

* -inuse_space：分析常駐內存
* -alloc_objects：分析臨時分配內存

# other

go tool pprof http://localhost:7000/debug/pprof/block

go tool pprof http://localhost:7000/debug/pprof/mutex
