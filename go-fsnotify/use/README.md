# 使用

```go
package main

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func main() {
	var wg sync.WaitGroup

	// 創建 監控器
	watcher, e := fsnotify.NewWatcher()
	if e != nil {
		log.Fatal(e)
	}

	// 設置要監視的 目錄
	e = watcher.Add("/home/king/project/go/src/test/console/tmp")
	if e != nil {
		log.Fatal(e)
	}

	// 運行 監視 goroutine
	wg.Add(1)
	go func() {
		fmt.Println("run monitor")
		var e error
		var ok bool
		var evt fsnotify.Event
		var info os.FileInfo
		keys := make(map[string]bool)
		for {
			select {
			case evt, ok = <-watcher.Events:
				if !ok {
					goto EXIT_M
				}
				log.Println(evt)
				if evt.Op&fsnotify.Create == fsnotify.Create {
					info, e = os.Stat(evt.Name)
					if e == nil {
						if info.IsDir() && !keys[evt.Name] {
							e = watcher.Add(evt.Name)
							if e == nil {
								keys[evt.Name] = true
								log.Println("add monitor", evt.Name)
							} else {
								log.Println(e)
							}
						}
					} else {
						log.Println(e)
					}
				} else if evt.Op&fsnotify.Remove == fsnotify.Remove {
					if keys[evt.Name] {
						watcher.Remove(evt.Name)
						delete(keys, evt.Name)
						log.Println("remove monitor", evt.Name)
					}
				} else if evt.Op&fsnotify.Rename == fsnotify.Rename {
					if keys[evt.Name] {
						watcher.Remove(evt.Name)
						delete(keys, evt.Name)
						log.Println("remove monitor", evt.Name)
					}
				}
			case e, ok = <-watcher.Errors:
				log.Println("err", ok)
				if !ok {
					goto EXIT_M
				}
				log.Println("error:", e)
			}
		}
	EXIT_M:
		fmt.Println("exit monitor")
		wg.Done()
	}()
	// 運行 signal goroutine
	ch := make(chan os.Signal, 2)
	signal.Notify(ch,
		os.Interrupt,
		syscall.SIGTERM)
	wg.Add(1)
	go func() {
		fmt.Println("run signal")
		for {
			sig := <-ch
			switch sig {
			case os.Interrupt:
				fmt.Println("ctrl + c")
				//do exit
				goto EXIT_S
			case syscall.SIGTERM:
				fmt.Println("kill")
				//do exit
				goto EXIT_S
			}
		}
	EXIT_S:
		fmt.Println("exit signal")
		watcher.Close()
		wg.Done()
	}()
	wg.Wait()
	fmt.Println("exit process")
}
```

> watcher 只會監控當前 目錄 不會監控 子目錄中的 變化
> 
> watcher 的函數 都是 goroutine 安全的
> 

# 監視 檔案
如果 watcher.Add 傳入的 是一個 檔案 而非 目錄 則 只對 此檔案變化 進行 監控
