# testify

golang 官方稱爲避免 程序員偷懶 單元測試 沒有提供 斷言 以強迫程序員處理所有錯誤

開源(MIT)的 testify 爲golang提供了 更簡單偷懶的 單元測試寫法

* 源碼 [https://github.com/stretchr/testify](https://github.com/stretchr/testify)

```
go get github.com/stretchr/testify/assert
```