# Google
由 Google 提供或 主導的 服務

# 常用產品
* Search
* [Chrome](google-chrome/0)
    * [AdGuard](google-chrome-AdGuard/0) 廣告屏蔽插件
    * [Tampermonkey](google-chrome-Tampermonkey/0) 用戶腳本
* [Noto](google-noto/0)

# 基礎組件
* [TCP BBR](google-bbr/0) TCP擁塞控制
* [Protocol Buffers](google-proto/0) 序列化
* [grpc](google-grpc/0) 遠程調用
* [jsonnet](google-jsonnet/0) 檔案格式
* [Material Design 3](google-md3/0)
* [Tesseract](google-Tesseract/0) OCR 文字識別引擎

# 算法
* [Brotli](google-Brotli/0) https response 壓縮

# 服務器

* Kubernetes
	* [kubeadm](google-kubeadm/0) 設置集羣
	* [Kubernetes](google-Kubernetes/0) 
	* [minikube](google-k8s-minikube/0)
	* [基礎](google-k8s-basics/0)
	* [對象管理](google-k8s-objects/0)
	* [服務 負載均衡 聯網](google-k8s-net/0)
	* [存儲](google-k8s-storage/0)
	* [FAQ](google-k8s-FAQ/0)

# tensorflow
* [tensorflow.js](google-tensorflow-js/0)
# golang
* google 提供
    * [go](google-go/0) 基礎介紹
    
    * [go](google-go-go/0) 標準庫 go package
    * [x 標準庫補充](google-go-x/0) 標準庫補充
        * [mobile](google-go-x-mobile/0) 移動平臺
    * [github.com/google](google-go-github/0) google團隊維護的go庫
* 標準庫
	* [標準庫](google-go-std/0) 標準庫介紹
	* [字符串](google-go-string/0) 字符串相關模塊
* 包管理
    * [gopkg.in](go-gopkg/0)
* 第三方庫
    * [cobra](go-cobra/0) 命令行解析
    * [otp](go-otp/0) 一次性密碼
    * [statik](go-statik/0) 靜態資源
    * [zap](go-zap/0) 日誌
    * [lumberjack](go-lumberjack/0) 日誌輪轉
    * [fsnotify](go-fsnotify/0) 檔案夾監控
    * [gomove](go-gomove/0) golang項目 改名工具
    * [rxgo](go-rxgo/0) rxgo ReactiveX 的go實現
    * [ants](go-ants/0) goroutine 池
    * [timingwheel](go-timingwheel/0) 時間輪
    * [h2non/bimg](go-h2non-bimg/0) 圖像處理
    * [gopsutil](go-gopsutil/0) 硬件信息
    * [base64Captcha](go-base64Captcha/0) 驗證碼
    * 測試
        * [github.com/stretchr/testify](go-testify/0) 單元測試
    * 網路相關
        * [github.com/xtaci/kcp-go](kcp-go/0) kcp-go
        * [github.com/lucas-clemente/quic-go](quic-go/0) quic 
        * [github.com/miekg/dns](miekg-dns/0) dns 庫
        * [github.com/gorilla/securecookie](go-securecookie/0) cookie 安全
        * [github.com/gorilla/websocket](go-gorilla-websocket/0) websocket
        * [github.com/dgrijalva/jwt-go](go-jwt-go/0) jwt
        * [github.com/gocolly/colly](go-colly/0) 爬蟲
        * github.com/chromedp/chromedp 爬蟲 Chrome DevTools Protocol
    * 數據庫
         * [gorm](go-gorm/0) 關係型數據庫 ORM
         * [go-xorm](go-xorm/0) 關係型數據庫 ORM
         * [bolt](go-bolt/0) key/value 數據庫
         * [DiDB](go-DiDB/0) 分佈式關係型數據庫
         * [etcd](go-etcd/0) 分佈式 key/value
         * [go-redis](go-redis/0) redis go client
    * ui
        * [wails](go-wails/0) 跨平臺 ui
        * [qt](go-qt/0) qt golang bind
        * [gocui](go-gocui/0) golang cui
    * 服務器
        * [gin](go-gin/0) http 服務器 
        * [go-kit](go-go-kit/0) 微服務框架
        * [leaf](go-leaf/0) 遊戲服務器
        * [rtsp-simple-server](go-rtsp-simple-server/0) rtsp 服務器
        * [gortsplib](go-gortsplib/0) rtsp
    * 時間相關
	     * [github.com/robfig/cron](go-cron/0) 定時任務
    * 編碼解碼
        * [jsoniter](go-jsoniter/0) json 
        * [github.com/gomarkdown/markdown](go-markdown/0) markdown
    * 視頻
        * [x264-go](go-x264-go/0) h264 編碼庫 cgo 
    * 音頻
        * [beep](go-beep/0) cgo 音樂播放器 
    * 嵌入腳本
        * [otto](go-otto/0) javascript 
        * [goja](go-goja/0) javascript
# dartlang
* dart 核心
    * [dart](google-dart/0)
    * [標準庫](google-dart-lib/0)
        * [google團隊](google-dart-google/0) 由google團隊維護的庫
    * [第三方庫](google-dart-libs/0)
    * [dio](google-dart-dio/0) http 客戶端
    * [angel-dart](google-dart-angel-dart/0) web 框架
* flutter
    *  [flutter](google-flutter/0)
    *  [flutter linux](google-flutter-linux/0)
    *  [flutter web](google-flutter-web/0)
    *  [flutter 插件](google-flutter-plugin/0)
    * [material](google-flutter-material/0)
    * [第三方庫](google-flutter-libs/0)
		   * [google團隊](google-flutter-google/0) 由google團隊維護的庫 
    * [TV](google-flutter-tv/0)
# c++
* [gtest](google-gtest/0)

# angularjs

* [angularjs](google-angularjs/0)

# angular
* [angular](google-angular/0)
* [angular-material2](google-angular-material2/0)
* 第三方庫
    * [PrimeNG](google-angular2-PrimeNG/0) 組件庫 
    * [ngx-translate](google-angular2-ngx-translate/0) i18n
    * [angular2-toaster](google-angular2-toaster/0) 頁面通知

# OS
* [android](google-android/0)
    * [常用軟體](google-android-apps/0) 
    * [emulator 模擬器](google-android-emulator/0) 
    * [android ui](google-android-ui/0)
    * [qt for android](qt-android/0)
    * [android 功能實現](google-android-features/0) 
* 手機
	* [Nexus 6](google-phone-Nexus-6/0) 
	* [Pixel](google-Pixel/0)