# Chart.js

一個開源(MIT) 的 js 圖表庫

* 官網 [https://www.chartjs.org/](https://www.chartjs.org/)
* 文檔 [https://www.chartjs.org/docs/latest/](https://www.chartjs.org/docs/latest/)
* 源碼 [https://github.com/chartjs/Chart.js](https://github.com/chartjs/Chart.js)

```
npm install chart.js
```