# 編譯

libevent 提供了完善的 cmake 編譯支持，故編譯相當容易

```
# 創建一個編譯目錄
mkdir build && cd build

# 編譯 libevent
cmake ../
	-DCMAKE_C_COMPILER=gcc
	-DCMAKE_BUILD_TYPE=Release
	-DEVENT__DISABLE_TESTS=ON
	-DEVENT__DISABLE_SAMPLES=ON
	-DEVENT__DISABLE_OPENSSL=ON
	-DEVENT__LIBRARY_TYPE=STATIC 
```

* **CMAKE\_C\_COMPILER** 指定了編譯器，對於交叉編譯可以指定否則可以忽略
* **EVENT\_\_DISABLE\_OPENSSL=ON** 禁用了 tls 相關功能，如果打開需要安裝 openssl
* **EVENT\_\_LIBRARY\_TYPE** 指定編譯靜態庫

如果成功會在 lib 目錄下生成

* **libevent\_core** 所有核心 event 和 buffer 功能。 含所有 event_base evbuffer bufferevent 和實用程序函數
* **libevent\_extra** 一些可能不需要的額外功能， HTTP DNS RPC
* **libevent** 歷史遺留包含了 libevent_core + libevent_extra 的功能，對於新的程式不應該再使用，日後可能會刪除這個產出
* **libevent\_pthreads** 使用 pthreads 實現了一些鎖和多線程相關功能，如果不需要這部分功能可以不用鏈接
* **libevent\_openssl** 使用 bufferevents 和 OpenSSL 爲網路通信提供了加密，如果不需要加密可以不鏈接這部分



