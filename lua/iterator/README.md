# 迭代器

lua 要求迭代器是一個函數，每次調用都返回集合的下一個元素，當函數點一個返回值是 nil 時，迭代結束

```
local arrs = {"a", "b", "c"}
print(pairs(arrs)) -- function table 0
for index, value in ipairs(arrs) do
    print(index .. "=" .. value)
end
```

for 用於迭代器時在 in 後需要傳入三個參數分別爲
* iterator 迭代器函數 
* container 狀態常量
* control 控制變量

1. 第一版 for 會調用 iterator(container,control)
2. 如果存在元素則 iterator 應該返回 control,Value，否則返回 nil
3. 如果返回 nil 循環結束，否則將 newControl,Value 設置給 in 之前的變量並執行循環體
4. 執行玩循環體後執行 iterator(container,newControl) 並重複步驟3

# 無狀態迭代器

無狀態迭代器指不保留任何狀態的迭代器，因此它可以避免創建閉包的額外花銷

內置的 ipairs 函數就會創建一個無狀態迭代器

```
-- 我們自己來實現無狀態的  迭代器
function iterator(table, i)
    i = i + 1
    local v = table[i]
    if v then
        -- 迭代器可以返回任意多個值，但只要第一個值爲 nil 就會結束 for 迭代
        return i, v, true, 123
    end
end

-- 我們自己實現 ipairs 替代內置的
function ipairs(table)
    return iterator, table, 0
end

local arrs = {"a", "b", nil, "c"}
-- in 之前可以定義任意多變量
for index, value, ok, flag in ipairs(arrs) do
    print(index .. "=" .. value, ok, flag) -- c 不會輸出因爲 for 在遇到迭代器返回 nil 就停止迭代
end
```

# 有狀態迭代器
有時迭代器需要保存一些狀態，這可以利用 table 或 閉包來實現

```
-- 我們自己實現 ipairs 替代內置的
function ipairs(table)
    local i = 0 -- 使用閉包來存儲 control
    -- 返回一個使用閉包的函數作爲 迭代器
    return function(table)
        i = i + 1
        local v = table[i]
        if v then
            return i, v
        end
    end, table
end

local arrs = {"a", "b", nil, "c"}
for index, value in ipairs(arrs) do
    print(index .. "=" .. value)
end
```

